/**
 * 
 */
package com.synapsis.dimer.codensa.service;

import java.util.List;

import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.service.CRUDService;

/**
 * @author dbraccio - Suivant
 *	09/04/2008
 */
public interface NodoRutaService extends CRUDService {

	public abstract List findByCriteriaSucursales(QueryFilter filter);
	
	public abstract int findByCriteriaSucursalesToPageable(QueryFilter filter);

	
}
