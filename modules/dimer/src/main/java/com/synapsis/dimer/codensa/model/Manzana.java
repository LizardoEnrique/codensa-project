package com.synapsis.dimer.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author dbraccio - Suivant
 *	26/03/2008
 */
public interface Manzana extends SynergiaBusinessObject {

	public abstract Long getIdNodo();

	public abstract void setIdNodo(Long idNodo);

	public abstract String getCodigoManzana();

	public abstract void setCodigoManzana(String codigoManzana);
	
}