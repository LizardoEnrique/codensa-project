/**
 *
 */
package com.synapsis.dimer.codensa.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.suivant.arquitectura.core.exception.IllegalDAOAccessException;
import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.integration.Module;
import com.suivant.arquitectura.core.model.BusinessObject;
import com.suivant.arquitectura.core.queryFilter.InQueryFilter;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.service.CRUDService;
import com.synapsis.synergia.core.service.impl.SynergiaAbstractCrudServiceImpl;

/**
 * @author dbraccio - Suivant 11/04/2008
 */
public class TipoRutaServiceImpl extends SynergiaAbstractCrudServiceImpl
		implements CRUDService {

	public TipoRutaServiceImpl(Module module) {
		super(module);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.synapsis.synergia.core.service.impl.SynergiaAbstractCrudServiceImpl#exists(com.suivant.arquitectura.core.queryFilter.QueryFilter)
	 */
	public Boolean exists(QueryFilter filter) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.synapsis.synergia.core.service.impl.SynergiaAbstractCrudServiceImpl#findByCriteria(com.suivant.arquitectura.core.queryFilter.QueryFilter)
	 */
	public List findByCriteria(QueryFilter filter)
			throws IllegalDAOAccessException {
		InQueryFilter qf = new InQueryFilter();
		qf.setAttributeName("codigo");
		List listqf = new ArrayList();
		listqf.add("FACT");
		listqf.add("REP");
		qf.setAttributeValue(listqf);
		return super.defaultFindByCriteria(qf);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.synapsis.synergia.core.service.impl.SynergiaAbstractCrudServiceImpl#findByCriteriaUnique(com.suivant.arquitectura.core.queryFilter.QueryFilter)
	 */
	public BusinessObject findByCriteriaUnique(QueryFilter filter)
			throws ObjectNotFoundException, IllegalDAOAccessException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.synapsis.synergia.core.service.impl.SynergiaAbstractCrudServiceImpl#isValidNewPrimaryKey(com.suivant.arquitectura.core.queryFilter.QueryFilter)
	 */
	public Boolean isValidNewPrimaryKey(QueryFilter filter) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.synapsis.synergia.core.service.impl.SynergiaAbstractCrudServiceImpl#removeByCriteria(com.suivant.arquitectura.core.queryFilter.QueryFilter)
	 */
	public Integer removeByCriteria(QueryFilter filter) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.suivant.arquitectura.core.service.CRUDService#findByCriteriaToPageable(com.suivant.arquitectura.core.queryFilter.QueryFilter)
	 */
	public int findByCriteriaToPageable(QueryFilter arg0) {
		InQueryFilter qf = new InQueryFilter();
		qf.setAttributeName("codigo");
		List listqf = new ArrayList();
		listqf.add("LECT");
		listqf.add("REP");
		qf.setAttributeValue(listqf);
		return super.countAll(qf);
	}

}
