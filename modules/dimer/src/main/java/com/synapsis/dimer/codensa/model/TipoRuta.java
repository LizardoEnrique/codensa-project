package com.synapsis.dimer.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author dbraccio - Suivant
 *	26/03/2008
 */
public interface TipoRuta extends SynergiaBusinessObject{

	//************************************************
	//**** getter and setter
	//************************************************
	public abstract String getCodigoInterno();

	public abstract void setCodigoInterno(String codigoInterno);

	public abstract String getCodigo();

	public abstract void setCodigo(String codigo);

	public abstract String getDescripcionTipoRuta();

	public abstract void setDescripcionTipoRuta(String descripcionTipoRuta);

	public abstract Integer getCantidadNiveles();

	public abstract void setCantidadNiveles(Integer cantidadNiveles);

	public abstract Long getIdTipoRutaComp();

	public abstract void setIdTipoRutaComp(Long idTipoRutaComp);

	public abstract Integer getCantidadNivelesComp();

	public abstract void setCantidadNivelesComp(Integer cantidadNivelesComp);

}