/**
 * 
 */
package com.synapsis.dimer.codensa.model.impl;

import com.synapsis.dimer.codensa.model.Manzana;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author dbraccio - Suivant 11/04/2008
 */
public class ManzanaImpl extends SynergiaBusinessObjectImpl implements Manzana {

	private Long idNodo;
	private String codigoManzana;
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.dimer.codensa.model.impl.Manzana#getIdNodo()
	 */
	public Long getIdNodo() {
		return idNodo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.dimer.codensa.model.impl.Manzana#setIdNodo(java.lang.Long)
	 */
	public void setIdNodo(Long idNodo) {
		this.idNodo = idNodo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.dimer.codensa.model.impl.Manzana#getCodigoManzana()
	 */
	public String getCodigoManzana() {
		return codigoManzana;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.dimer.codensa.model.impl.Manzana#setCodigoManzana(java.lang.String)
	 */
	public void setCodigoManzana(String codigoManzana) {
		this.codigoManzana = codigoManzana;
	}

}
