/**
 * $Id: TipoRutaImpl.java,v 1.1 2008/04/05 18:49:44 ar29261698 Exp $
 */
package com.synapsis.dimer.codensa.model.impl;
import com.synapsis.dimer.codensa.model.TipoRuta;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
/**
 * @author dbraccio - Suivant
 *	26/03/2008
 */
public class TipoRutaImpl extends SynergiaBusinessObjectImpl implements TipoRuta  {
	
	private String codigoInterno;
	private String codigo;
	private String descripcionTipoRuta;
	private Integer cantidadNiveles;
	private Long idTipoRutaComp;
	private Integer cantidadNivelesComp;
	//************************************************
	//**** getter and setter
	//************************************************
	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.ITipoRuta#getCodigoInterno()
	 */
	public String getCodigoInterno() {
		return codigoInterno;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.ITipoRuta#setCodigoInterno(java.lang.String)
	 */
	public void setCodigoInterno(String codigoInterno) {
		this.codigoInterno = codigoInterno;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.ITipoRuta#getCodigo()
	 */
	public String getCodigo() {
		return codigo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.ITipoRuta#setCodigo(java.lang.String)
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.ITipoRuta#getDescripcionTipoRuta()
	 */
	public String getDescripcionTipoRuta() {
		return descripcionTipoRuta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.ITipoRuta#setDescripcionTipoRuta(java.lang.String)
	 */
	public void setDescripcionTipoRuta(String descripcionTipoRuta) {
		this.descripcionTipoRuta = descripcionTipoRuta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.ITipoRuta#getCantidadNiveles()
	 */
	public Integer getCantidadNiveles() {
		return cantidadNiveles;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.ITipoRuta#setCantidadNiveles(java.lang.Integer)
	 */
	public void setCantidadNiveles(Integer cantidadNiveles) {
		this.cantidadNiveles = cantidadNiveles;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.ITipoRuta#getIdTipoRutaComp()
	 */
	public Long getIdTipoRutaComp() {
		return idTipoRutaComp;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.ITipoRuta#setIdTipoRutaComp(java.lang.Long)
	 */
	public void setIdTipoRutaComp(Long idTipoRutaComp) {
		this.idTipoRutaComp = idTipoRutaComp;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.ITipoRuta#getCantidadNivelesComp()
	 */
	public Integer getCantidadNivelesComp() {
		return cantidadNivelesComp;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.ITipoRuta#setCantidadNivelesComp(java.lang.Integer)
	 */
	public void setCantidadNivelesComp(Integer cantidadNivelesComp) {
		this.cantidadNivelesComp = cantidadNivelesComp;
	}
	
}
