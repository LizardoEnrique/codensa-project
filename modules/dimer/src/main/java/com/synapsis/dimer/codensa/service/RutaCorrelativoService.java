/**
 * $Id: RutaCorrelativoService.java,v 1.9 2008/10/03 22:03:35 ar29261698 Exp $
 */
package com.synapsis.dimer.codensa.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.service.CRUDService;
import com.synapsis.dimer.codensa.model.impl.RutaInfoImpl;

/**
 * @author dbraccio - Suivant 09/04/2008
 */
public interface RutaCorrelativoService extends CRUDService {

	public abstract List findByCriteriaAll(QueryFilter filter, String nroMedidor)
			throws ObjectNotFoundException, CloneNotSupportedException;

	/**
	 * retorna todas las rutas con los criterios enviados
	 * 
	 * @param filter
	 * @param nroMedidor
	 * @return
	 * @throws ObjectNotFoundException
	 * @throws CloneNotSupportedException
	 */
	public abstract int findByCriteriaAllToPageable(QueryFilter filter,
			String nroMedidor) throws ObjectNotFoundException,
			CloneNotSupportedException;

	/**
	 * retorna la lista de rutas que pueden ser seleccionadas para asignar
	 * 
	 * @param filter
	 * @return
	 */
	public abstract List findByCriteriaToAssign(QueryFilter filter);

	/**
	 * retorna la cantidad de rutas que pueden ser seleccionadas para asignar
	 * 
	 * @param filter
	 * @return
	 */
	public abstract int findByCriteriaToAssignToPageable(QueryFilter filter);

	/**
	 * actuializa el orden las rutas para el nodo seleccionado
	 * 
	 * @param rutasSelected
	 * @param rutaSelected
	 * @param tipoRuta
	 */
	public abstract void actualizarRutas(ArrayList rutasSelected,
			RutaInfoImpl rutaSelected, String tipoRuta);

	/**
	 * agrega las rutas seleccionadas al final de las rutas del nodo
	 * seleccionada
	 * 
	 * @param rutasSelected
	 * @param tipoRuta
	 * @param idNodo
	 */
	public abstract void agregarRutasAlFinal(ArrayList rutasSelected,
			String tipoRuta, Long idNodo);

	/**
	 * ejecuta un proC y retorna el archivo generado.
	 * 
	 * @param tipoRuta
	 * @param id_nodo
	 * @return
	 * @throws FileNotFoundException
	 * @throws ObjectNotFoundException
	 */
	public abstract FileInputStream exportFile(String tipoRuta, Long id_nodo)
			throws Exception;

	/**
	 * Se encarga de preparar la tabla para realizar las busquedas de rutas
	 * 
	 * @param idNodo
	 * @param tipoRuta
	 * @return
	 */
	public abstract String preparedSearch(Long idNodo, Long idTipoRuta);

}
