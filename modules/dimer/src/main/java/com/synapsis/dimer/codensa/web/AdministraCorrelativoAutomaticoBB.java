/**
 * $Id: AdministraCorrelativoAutomaticoBB.java,v 1.23 2012/05/10 13:21:36 syacac Exp $
 */
package com.synapsis.dimer.codensa.web;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.myfaces.custom.service.UIService;

import com.synapsis.commons.connector.model.ConnectorUtils;
import com.synapsis.commons.faces.messages.ErrorsManager;
import com.synapsis.commons.services.IServiceManager;
import com.synapsis.commons.services.ServiceHelper;
import com.synapsis.commons.services.model.IServiceRequest;
import com.synapsis.components.behaviour.Notifier;
import com.synapsis.dimer.codensa.model.NodoRuta;
import com.synapsis.dimer.codensa.model.TipoRuta;
import com.synapsis.nuc.codensa.model.impl.TipoServicioImpl;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;
import com.synapsis.synergia.presentation.model.NasServiceHelper;

/**
 * CU-Administra Correlativo Automatico
 * 
 * @author Paola Attadio
 * 
 */
public class AdministraCorrelativoAutomaticoBB implements Serializable {

	// listas de los combos
	private List tiposRutas;
	private List sucursales;
	private List zonas;
	private List ciclos;
	private List grupos;
	private List manzanas;
	private List tiposServicios;
	private SelectItem[] status;

	// campos seleccionados por el usuario
	private Long tipoRutaSelected;
	private Long sucursalSelected;
	private Long zonaSelected;
	private Long cicloSelected;
	private Long grupoSelected;
	private Long manzanaSelected;
	private String tipoServicioSelected;
	private String nroMedidor;
	private Boolean statusSelected;
	private String sessionId;

	private UIService rutaService;
	private UIService rutaServiceToAssign;

	// tabla para seleccionar las rutas
	private HtmlDataTable dataTableRutasToAssign;
	// tabla para seleccionar las rutas ya seleccionadas
	private HtmlDataTable dataTableRutas;

	// Ruta seleccionada
	private Object rutaSelected;
	// lista de rutas seleccionadas
	private List rutasSelected = new ArrayList();

	// propiedades staticas
	private static final String TIPO_RUTA_FACT = "FACT";
	private static final String TIPO_RUTA_CODIGO = "LECT-FACT";
	private static final String TIPO_RUTA_DESCRIPCION = "Lect-Fact";
	private static final String SERVICIO_ACTIVO = "S";
	private static final String SERVICIO_INACTIVO = "N";
	private static final String RUTA_EN_ESTADO_FACTURACION = "EnFacturacion";

	public void initClean() {
		FacesContext fc = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) fc
				.getExternalContext().getRequest();
		String clean = request.getParameter("action");
		if (StringUtils.equalsIgnoreCase(clean, "clean"))
			cleanFields();
	}

	// ********************************************************************
	// ********** actions para refrescar los combos con ajax
	// ********************************************************************
	// refresca el combo de sucursales y limpia los combos de zonas, ciclos,
	// grupos y manzanas
	public void refreshComboSucursales(ActionEvent event) {
		String submittedValue = ((UIInput) event.getComponent().getParent())
				.getSubmittedValue().toString();
		clean();
		if (StringUtils.isNotEmpty(submittedValue)) {
			Map map = new HashMap();
			map.put("activo", new Boolean(true));
			sucursales = (List) NasServiceHelper.getCollectionResponse(
					"nodoRutaService", "findByCriteriaSucursales", map);
		} else {
			sucursales = null;
		}
		zonas = null;
		ciclos = null;
		grupos = null;
		manzanas = null;
	}

	// refresca el combo de sucursales y limpia los combos de ciclos, grupos y
	// manzanas
	public void refreshComboZonas(ActionEvent event) {
		String submittedValue = ((UIInput) event.getComponent().getParent())
				.getSubmittedValue().toString();
		clean();
		if (StringUtils.isNotEmpty(submittedValue)) {
			Map map = new HashMap();
			map.put("idNodoPadre", new Long(submittedValue));
			map.put("activo", new Boolean(true));
			zonas = (List) NasServiceHelper.getCollectionResponse(
					"nodoRutaService", "findByCriteria", map);
		} else {
			zonas = null;
		}
		ciclos = null;
		grupos = null;
		manzanas = null;
	}

	// refresca el combo de ciclos y limpia el combo de grupos y manzanas
	public void refreshComboCiclos(ActionEvent event) {
		String submittedValue = ((UIInput) event.getComponent().getParent())
				.getSubmittedValue().toString();
		clean();
		if (StringUtils.isNotEmpty(submittedValue)) {
			Map map = new HashMap();
			map.put("idNodoPadre", new Long(submittedValue));
			map.put("activo", new Boolean(true));
			IServiceManager serviceManager = SynergiaApplicationContext
					.getServiceManager();
			IServiceRequest request = serviceManager.getModel(
					"nodoRutaService", "findByCriteria");
			request.setMaxRows(110);
			ConnectorUtils.fillDataModel(request.getParameters(), map);
			ciclos = (List) ServiceHelper.getResponse(serviceManager, request);
		} else {
			ciclos = null;
		}
		grupos = null;
		manzanas = null;
	}

	// refresca el combo de grupos y limpia el combo de manzanas
	public void refreshComboGrupos(ActionEvent event) {
		String submittedValue = ((UIInput) event.getComponent().getParent())
				.getSubmittedValue().toString();
		clean();
		if (StringUtils.isNotEmpty(submittedValue)) {
			Map map = new HashMap();
			map.put("idNodoPadre", new Long(submittedValue));
			map.put("activo", new Boolean(true));
			IServiceManager serviceManager = SynergiaApplicationContext
					.getServiceManager();
			IServiceRequest request = serviceManager.getModel(
					"nodoRutaService", "findByCriteria");
			request.setMaxRows(110);
			ConnectorUtils.fillDataModel(request.getParameters(), map);
			grupos = (List) ServiceHelper.getResponse(serviceManager, request);
		} else {
			grupos = null;
		}
		manzanas = null;
	}

	// refresce el combo de manzanas
	public void refreshComboManzanas(ActionEvent event) {
		String submittedValue = ((UIInput) event.getComponent().getParent())
				.getSubmittedValue().toString();
		clean();
		if (StringUtils.isNotEmpty(submittedValue)) {
			Map map = new HashMap();
			map.put("idNodo", new Long(submittedValue));
			map.put("activo", new Boolean(true));
			IServiceManager serviceManager = SynergiaApplicationContext
					.getServiceManager();
			IServiceRequest request = serviceManager.getModel("manzanaService",
					"findByCriteria");
			request.setMaxRows(110);
			ConnectorUtils.fillDataModel(request.getParameters(), map);
			manzanas = (List) ServiceHelper
					.getResponse(serviceManager, request);
		} else {
			manzanas = null;
		}
	}

	// ************************************************************************
	// **** actions ejecutadas con el submit del formulario
	// ************************************************************************
	// realiza la busqueda de rutas
	public void search() {
		// previo a realizar la busqueda limpio todo
		clean();
		Map map = new HashMap();
		map.put("idNodo", getGrupoSelected());
		map.put("idTipoRuta", getTipoRutaSelected());
		sessionId = (String)NasServiceHelper.getResponse("rutaCorrelativoService", "preparedSearch", map);
		getRutaServiceToAssign().execute(true);
		getRutaService().execute(true);
	}

	// atualizo los seleccionados
	public void actualizar() {
		try {
			if (!isValidDataToActualizar())
				return;
			if (enProcesoFacturacion())
				return;
			Map map = new HashMap();
			map.put("rutaSelected", getRutaSelected());
			map.put("rutasSelected", getRutasSelected());
			map.put("tipoRuta", getTipoRuta(tipoRutaSelected));
			NasServiceHelper.invoke("rutaCorrelativoService",
					"actualizarRutas", map);
			// limpio las tablas
			getRutasSelected().clear();
			setRutaSelected(null);
			dataTableRutas.setFirst(0);
			dataTableRutasToAssign.setFirst(0);
			// realizo nuevamente la busqueda
			search();
			// pongo un mensaje
			Notifier
					.addMessage("administra.correlativo.automatico.message.update");
		} catch (Exception e) {
			e.printStackTrace();
			Notifier
					.addMessage("administra.correlativo.automatico.message.excepcion.actualizar");
		}
	}

	// agrego los seleccionados al final
	public void agregarAlFinal() {
		try {
			if (!isValidDataToAddAtEnd())
				return;
			if (enProcesoFacturacion())
				return;
			Map map = new HashMap();
			map.put("rutasSelected", getRutasSelected());
			map.put("tipoRuta", getTipoRuta(tipoRutaSelected));
			map.put("idNodo", this.grupoSelected);
			NasServiceHelper.invoke("rutaCorrelativoService",
					"agregarRutasAlFinal", map);
			// limpio las tablas
			getRutasSelected().clear();
			setRutaSelected(null);
			dataTableRutas.setFirst(0);
			dataTableRutasToAssign.setFirst(0);
			// realizo nuevamente la busqueda
			search();
			Notifier
					.addMessage("administra.correlativo.automatico.message.agregar.al.final");
		} catch (Exception e) {
			Notifier
					.addMessage("administra.correlativo.automatico.message.excepcion.actualizar");
		}
	}

	// retorna el archivo generado
	public void exportFile() {
		try {
			Map map = new HashMap();
			map.put("tipoRuta", getTipoRuta(tipoRutaSelected));
			map.put("idNodo", this.grupoSelected);
			InputStream in = (InputStream) NasServiceHelper.getResponse(
					"rutaCorrelativoService", "exportFile", map);
			FacesContext fc = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) fc
					.getExternalContext().getResponse();
			String filename = "prueba.csv";
			String contentType = "application/cvs";
			response.setContentType(contentType);
			response.setHeader("Content-disposition", "attachment; filename="
					+ filename);
			int bit = 256;
			ServletOutputStream outs = response.getOutputStream();
			while ((bit) >= 0) {
				bit = in.read();
				outs.write(bit);
			}
			outs.close();
			in.close();
			fc.responseComplete();
		} catch (Exception e) {
			Notifier
					.addMessage("administra.correlativo.automatico.message.excepcion.exportar");
		}

	}

	// limpia todas las colecciones
	public void cleanFields() {
		tipoRutaSelected = null;
		sucursales = null;
		zonas = null;
		ciclos = null;
		grupos = null;
		manzanas = null;
		tipoServicioSelected = null;
		nroMedidor = null;
		clean();
	}

	// limpia los componentes de servicio, la lista y el seleccionado
	public void clean() {
		if (getRutaService() != null)
			getRutaService().setValue(null);
		if (getRutaServiceToAssign() != null)
			getRutaServiceToAssign().setValue(null);
		if (getRutaSelected() != null) {
			getRutasSelected().clear();
			setRutaSelected(null);
		}
		if (dataTableRutas != null)
			dataTableRutas.setFirst(0);
		if (dataTableRutasToAssign != null)
			dataTableRutasToAssign.setFirst(0);
	}

	// aplica filtros a la segunda tabla
	public void addFilterStatus(ActionEvent event) {
		String submittedValue = ((UIInput) event.getComponent().getParent())
				.getSubmittedValue().toString();
		if (!StringUtils.equalsIgnoreCase(submittedValue, ""))
			this.statusSelected = new Boolean(submittedValue);
		else
			this.statusSelected = null;
		getRutaService().execute(true);
	}

	// aplica filtros a la segunda tabla
	public void addFilterMedidor(ActionEvent event) {
		String submittedValue = ((UIInput) event.getComponent().getParent())
				.getSubmittedValue().toString();
		if (!StringUtils.equalsIgnoreCase(submittedValue.trim(), ""))
			this.setNroMedidor(submittedValue);
		else
			this.nroMedidor = "";
		getRutaService().execute(true);
	}

	// aplica filtros a la segunda tabla
	public void addFilterTipoServicio(ActionEvent event) {
		String submittedValue = ((UIInput) event.getComponent().getParent())
				.getSubmittedValue().toString();
		if (!StringUtils.equalsIgnoreCase(submittedValue, ""))
			this.setTipoServicioSelected(submittedValue);
		else
			this.tipoServicioSelected = "";
		getRutaService().execute(true);
	}

	// aplica filtros a la segunda tabla
	public void addFilterManzana(ActionEvent event) {
		String submittedValue = ((UIInput) event.getComponent().getParent())
				.getSubmittedValue().toString();
		if (!StringUtils.equalsIgnoreCase(submittedValue, ""))
			this.setManzanaSelected(new Long(submittedValue));
		else
			this.setManzanaSelected(null);
		getRutaService().execute(true);
	}

	// *******************************************************
	// *** metodos privados
	// *******************************************************
	private boolean enProcesoFacturacion() {
		Map map = new HashMap();
		map.put("idNodo", this.sucursalSelected);
		map.put("estado", RUTA_EN_ESTADO_FACTURACION);
		map.put("codigoCiclo", this.getCodigoCiclo());
		Integer i = (Integer) NasServiceHelper.getResponse(
				"calendarioFacturacionService", "countAll", map);
		if (i.intValue() == 0)
			return false;
		Notifier
				.addMessage("administra.correlativo.automatico.message.ruta.en.proceso.de.facturacion");
		return true;
	}

	private boolean isValidDataToActualizar() {
		boolean errores = false;
		if (getRutaSelected() == null) {
			errores = true;
			ErrorsManager
					.addError("Falta seleccionar la Ruta donde se agregaran las seleccionada");
		}
		if (getRutasSelected().isEmpty()) {
			errores = true;
			ErrorsManager
					.addError("Falta seleccionar las Rutas para reasignar correlativo");
		}
		return !errores;
	}

	private boolean isValidDataToAddAtEnd() {
		boolean errores = false;
		if (getRutasSelected().isEmpty()) {
			errores = true;
			ErrorsManager
					.addError("Falta seleccionar las Rutas para agregar al final del Grupo");
		}
		return !errores;
	}

	// inicia la coleccion de tipos de servicios que se muestran en el combo
	private void initTiposServicios() {
		List result = new ArrayList();
		TipoServicioImpl element0 = new TipoServicioImpl();
		element0.setId("Cuenta");
		element0.setDescripTipoServicio("Cuenta");
		TipoServicioImpl element1 = new TipoServicioImpl();
		element1.setId("Macromedidor");
		element1.setDescripTipoServicio("Macromedidor");
		result.add(element0);
		result.add(element1);
		tiposServicios = result;
	}

	// reemplaza en la collection el string de Fact por los tres Lectura
	private void replaceFACT() {
		Iterator it = tiposRutas.iterator();
		while (it.hasNext()) {
			TipoRuta t = (TipoRuta) it.next();
			if (StringUtils.equalsIgnoreCase(t.getCodigo(), TIPO_RUTA_FACT)) {
				t.setCodigo(TIPO_RUTA_CODIGO);
				t.setDescripcionTipoRuta(TIPO_RUTA_DESCRIPCION);
			}
		}
	}

	// retorna el tipo de ruta
	private String getTipoRuta(Long idTipoRuta) {
		Iterator it = tiposRutas.iterator();
		while (it.hasNext()) {
			TipoRuta t = (TipoRuta) it.next();
			if (t.getId().longValue() == idTipoRuta.longValue()) {
				if (StringUtils.equalsIgnoreCase(t.getCodigo(),
						TIPO_RUTA_CODIGO))
					return TIPO_RUTA_FACT;
				return t.getCodigo();
			}
		}
		return TIPO_RUTA_FACT;
	}

	private String getCodigoCiclo() {
		String cod_ciclo = "";
		Long id_cod_ciclo = getCicloSelected();
		Iterator it = this.getCiclos().iterator();
		while (it.hasNext()) {
			NodoRuta nodoRuta = (NodoRuta) it.next();
			if (nodoRuta.getId().longValue() == id_cod_ciclo.longValue())
				return nodoRuta.getCodigoNodo();
		}
		return cod_ciclo;
	}

	private void fillStatusList() {
		status = new SelectItem[2];
		status[0] = new SelectItem(new Boolean(true), "S");
		status[1] = new SelectItem(new Boolean(false), "N");
	}

	// *******************************************************
	// **** getter and setter
	// *******************************************************
	public List getTiposRutas() {
		if (tiposRutas != null)
			replaceFACT();
		return tiposRutas;
	}

	public void setTiposRutas(List tiposRutas) {
		this.tiposRutas = tiposRutas;
	}

	public List getSucursales() {
		return sucursales;
	}

	public void setSucursales(List sucursales) {
		this.sucursales = sucursales;
	}

	public List getZonas() {
		return zonas;
	}

	public void setZonas(List zonas) {
		this.zonas = zonas;
	}

	public List getCiclos() {
		return ciclos;
	}

	public void setCiclos(List ciclos) {
		this.ciclos = ciclos;
	}

	public List getGrupos() {
		return grupos;
	}

	public void setGrupos(List grupos) {
		this.grupos = grupos;
	}

	public Long getTipoRutaSelected() {
		return tipoRutaSelected;
	}

	public void setTipoRutaSelected(Long tipoRutaSelected) {
		this.tipoRutaSelected = tipoRutaSelected;
	}

	public Long getSucursalSelected() {
		return sucursalSelected;
	}

	public void setSucursalSelected(Long sucursalSelected) {
		this.sucursalSelected = sucursalSelected;
	}

	public Long getZonaSelected() {
		return zonaSelected;
	}

	public void setZonaSelected(Long zonaSelected) {
		this.zonaSelected = zonaSelected;
	}

	public Long getCicloSelected() {
		return cicloSelected;
	}

	public void setCicloSelected(Long cicloSelected) {
		this.cicloSelected = cicloSelected;
	}

	public Long getGrupoSelected() {
		return grupoSelected;
	}

	public void setGrupoSelected(Long grupoSelected) {
		this.grupoSelected = grupoSelected;
	}

	public UIService getRutaService() {
		return rutaService;
	}

	public void setRutaService(UIService rutaService) {
		this.rutaService = rutaService;
	}

	public UIService getRutaServiceToAssign() {
		return rutaServiceToAssign;
	}

	public void setRutaServiceToAssign(UIService rutaServiceToAssign) {
		this.rutaServiceToAssign = rutaServiceToAssign;
	}

	public Object getRutaSelected() {
		return rutaSelected;
	}

	public void setRutaSelected(Object rutaSelected) {
		this.rutaSelected = rutaSelected;
	}

	public List getRutasSelected() {
		return rutasSelected;
	}

	public void setRutasSelected(List rutasSelected) {
		this.rutasSelected = rutasSelected;
	}

	public List getManzanas() {
		return manzanas;
	}

	public void setManzanas(List manzanas) {
		this.manzanas = manzanas;
	}

	public Long getManzanaSelected() {
		return manzanaSelected;
	}

	public void setManzanaSelected(Long manzanaSelected) {
		this.manzanaSelected = manzanaSelected;
	}

	public List getTiposServicios() {
		if (tiposServicios == null)
			initTiposServicios();
		return tiposServicios;
	}

	public void setTiposServicios(List tiposServicios) {
		this.tiposServicios = tiposServicios;
	}

	public String getTipoServicioSelected() {
		return tipoServicioSelected;
	}

	public void setTipoServicioSelected(String tipoServicioSelected) {
		this.tipoServicioSelected = tipoServicioSelected;
	}

	public String getNroMedidor() {
		return nroMedidor;
	}

	public void setNroMedidor(String nroMedidor) {
		this.nroMedidor = nroMedidor;
	}

	public HtmlDataTable getDataTableRutasToAssign() {
		return dataTableRutasToAssign;
	}

	public void setDataTableRutasToAssign(HtmlDataTable dataTableRutasToAssign) {
		this.dataTableRutasToAssign = dataTableRutasToAssign;
	}

	public HtmlDataTable getDataTableRutas() {
		return dataTableRutas;
	}

	public void setDataTableRutas(HtmlDataTable dataTableRutas) {
		this.dataTableRutas = dataTableRutas;
	}

	public Boolean getStatusSelected() {
		return statusSelected;
	}

	public void setStatusSelected(Boolean statusSelected) {
		this.statusSelected = statusSelected;
	}

	public SelectItem[] getStatus() {
		if (status == null)
			fillStatusList();
		return status;
	}

	public void setStatus(SelectItem[] status) {
		this.status = status;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	
}
