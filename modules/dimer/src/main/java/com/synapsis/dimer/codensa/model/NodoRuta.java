package com.synapsis.dimer.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author dbraccio - Suivant
 *	26/03/2008
 */
public interface NodoRuta extends SynergiaBusinessObject{

	// ************************************************
	// ****** getter and setter
	// ***********************************************
	public abstract TipoRuta getTipoRuta();

	public abstract void setTipoRuta(TipoRuta tipoRuta);

	public abstract String getCodigoNodo();

	public abstract void setCodigoNodo(String codigoNodo);

	public abstract Long getIdNodoPadre();

	public abstract void setIdNodoPadre(Long idNodoPadre);

	public abstract String getDescripcionNodo();

	public abstract void setDescripcionNodo(String descripcionNodo);

	public abstract Boolean getActivo();

	public abstract void setActivo(Boolean activo);

	public abstract String getCodigoNodoRuta();

	public abstract void setCodigoNodoRuta(String codigoNodoRuta);

	public abstract Long getIdNivelRuta() ;

	public abstract void setIdNivelRuta(Long idNivelRuta) ;

}