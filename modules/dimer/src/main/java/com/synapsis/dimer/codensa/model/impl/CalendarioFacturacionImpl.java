/**
 * 
 */
package com.synapsis.dimer.codensa.model.impl;

import com.synapsis.dimer.codensa.model.CalendarioFacturacion;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author dbraccio - Suivant
 *	26/06/2008
 */
public class CalendarioFacturacionImpl extends SynergiaBusinessObjectImpl implements CalendarioFacturacion {

	private String estado;
	private Long idNodo;
	private String codigoCiclo;
	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.CalendarioFacturacion#getEstado()
	 */
	public String getEstado() {
		return estado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.CalendarioFacturacion#setEstado(java.lang.String)
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.CalendarioFacturacion#getIdNodo()
	 */
	public Long getIdNodo() {
		return idNodo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.CalendarioFacturacion#setIdNodo(java.lang.Long)
	 */
	public void setIdNodo(Long idNodo) {
		this.idNodo = idNodo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.CalendarioFacturacion#getCodigoCiclo()
	 */
	public String getCodigoCiclo() {
		return codigoCiclo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.CalendarioFacturacion#setCodigoCiclo(java.lang.Long)
	 */
	public void setCodigoCiclo(String codigoCiclo) {
		this.codigoCiclo = codigoCiclo;
	}
	
	
	
	
}
