/**
 * 
 */
package com.synapsis.dimer.codensa.service.impl;

import java.util.List;

import com.suivant.arquitectura.core.exception.IllegalDAOAccessException;
import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.integration.Module;
import com.suivant.arquitectura.core.model.BusinessObject;
import com.suivant.arquitectura.core.queryFilter.CompositeQueryFilter;
import com.suivant.arquitectura.core.queryFilter.IsNullQueryFilter;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.synapsis.dimer.codensa.service.NodoRutaService;
import com.synapsis.synergia.core.service.impl.SynergiaAbstractCrudServiceImpl;

/**
 * @author dbraccio - Suivant
 *	09/04/2008
 */
public class NodoRutaServiceImpl extends SynergiaAbstractCrudServiceImpl
		implements NodoRutaService {

	/**
	 * default constructor
	 * @param module
	 */
	public NodoRutaServiceImpl(Module module) {
		super(module);
	}

	public Boolean exists(QueryFilter filter) {
		// do nothing
		return null;
	}

	public List findByCriteria(QueryFilter filter)
			throws IllegalDAOAccessException {
		return super.defaultFindByCriteria(filter);
	}

	public BusinessObject findByCriteriaUnique(QueryFilter filter)
			throws ObjectNotFoundException, IllegalDAOAccessException {
		//do nothing
		return null;
	}

	public Boolean isValidNewPrimaryKey(QueryFilter filter) {
		//do nothing
		return null;
	}

	public Integer removeByCriteria(QueryFilter filter) {
		return null;
	}

	

	public int findByCriteriaToPageable(QueryFilter filter) {
		return super.countAll(filter);
	}
	
	public List findByCriteriaSucursales(QueryFilter filter) {
		QueryFilter isNullQueryFilter = new IsNullQueryFilter("idNodoPadre");
		CompositeQueryFilter cqf = new CompositeQueryFilter();
		cqf.addFilter(filter);
		cqf.addFilter(isNullQueryFilter);
		return super.defaultFindByCriteria(cqf);
	}

	public int findByCriteriaSucursalesToPageable(QueryFilter filter) {
		QueryFilter isNullQueryFilter = new IsNullQueryFilter("idNodoPadre");
		CompositeQueryFilter cqf = new CompositeQueryFilter();
		cqf.addFilter(filter);
		cqf.addFilter(isNullQueryFilter);
		return super.countAll(cqf);
	}

}
