/**
 *
 */
package com.synapsis.dimer.codensa.model.impl;

import com.synapsis.dimer.codensa.model.RutaInfo;
import com.synapsis.synergia.core.model.impl.SynergiaPersistentObjectImpl;

/**
 * @author dbraccio - Suivant 26/03/2008
 */
public class RutaInfoImpl extends SynergiaPersistentObjectImpl implements
		RutaInfo {

	private Long idTipoRuta;
	private Long idNodo;
	private String codigoRuta;
	private Long idManzana;
	private Boolean reasignarCorrelativo;
	private String codigoManzana;
	private String nroServicio;
	private String tipoServicio;
	private String direccion;
	private String nroComponente;
	private String correlativo;
	private String anomalia;
	private String ciclo;
	private Boolean activo;
	private String sessionId;

	// **********************************************
	// ******** getter and setter
	// *********************************************

	/*
	 * (non-Javadoc)
	 *
	 * @see com.synapsis.dimer.codensa.model.impl.Ruta#getIdTipoRuta()
	 */
	public Long getIdTipoRuta() {
		return idTipoRuta;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.synapsis.dimer.codensa.model.impl.Ruta#setIdTipoRuta(java.lang.Long)
	 */
	public void setIdTipoRuta(Long idTipoRuta) {
		this.idTipoRuta = idTipoRuta;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.synapsis.dimer.codensa.model.impl.Ruta#getIdNodo()
	 */
	public Long getIdNodo() {
		return idNodo;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.synapsis.dimer.codensa.model.impl.Ruta#setIdNodo(java.lang.Long)
	 */
	public void setIdNodo(Long idNodo) {
		this.idNodo = idNodo;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.synapsis.dimer.codensa.model.impl.Ruta#getCodigoRuta()
	 */
	public String getCodigoRuta() {
		return codigoRuta;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.synapsis.dimer.codensa.model.impl.Ruta#setCodigoRuta(java.lang.String)
	 */
	public void setCodigoRuta(String codigoRuta) {
		this.codigoRuta = codigoRuta;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.synapsis.dimer.codensa.model.impl.Ruta#getIdManzana()
	 */
	public Long getIdManzana() {
		return idManzana;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.synapsis.dimer.codensa.model.impl.Ruta#setIdManzana(java.lang.Integer)
	 */
	public void setIdManzana(Long idManzana) {
		this.idManzana = idManzana;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.synapsis.dimer.codensa.model.impl.Ruta#getReasignarCorrelativo()
	 */
	public Boolean getReasignarCorrelativo() {
		return reasignarCorrelativo;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.synapsis.dimer.codensa.model.impl.Ruta#setReasignarCorrelativo(java.lang.Boolean)
	 */
	public void setReasignarCorrelativo(Boolean reasignarCorrelativo) {
		this.reasignarCorrelativo = reasignarCorrelativo;
	}

	public String getCodigoManzana() {
		return codigoManzana;
	}

	public void setCodigoManzana(String codigoManzana) {
		this.codigoManzana = codigoManzana;
	}

	public String getNroServicio() {
		return nroServicio;
	}

	public void setNroServicio(String nroServicio) {
		this.nroServicio = nroServicio;
	}

	public String getTipoServicio() {
		return tipoServicio;
	}

	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNroComponente() {
		return nroComponente;
	}

	public void setNroComponente(String nroComponente) {
		this.nroComponente = nroComponente;
	}

	public String getCorrelativo() {
		return correlativo;
	}

	public void setCorrelativo(String correlativo) {
		this.correlativo = correlativo;
	}

	public String getEntityName() {
		return null;
	}

	public String getAnomalia() {
		return anomalia;
	}

	public void setAnomalia(String anomalia) {
		this.anomalia = anomalia;
	}

	public String getCiclo() {
		return ciclo;
	}

	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	


}
