package com.synapsis.dimer.codensa.model;

public interface CalendarioFacturacion {

	public abstract String getEstado();

	public abstract void setEstado(String estado);

	public abstract Long getIdNodo();

	public abstract void setIdNodo(Long idNodo);

	public abstract String getCodigoCiclo();

	public abstract void setCodigoCiclo(String codigoCiclo);

}