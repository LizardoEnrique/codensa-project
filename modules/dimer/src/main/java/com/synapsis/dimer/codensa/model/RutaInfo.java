package com.synapsis.dimer.codensa.model;

import com.synapsis.synergia.core.model.SynergiaPersistentObject;

/**
 * @author dbraccio - Suivant 09/04/2008
 */

public interface RutaInfo extends SynergiaPersistentObject {

	public abstract Long getIdTipoRuta();

	public abstract void setIdTipoRuta(Long idTipoRuta);

	public abstract Long getIdNodo();

	public abstract void setIdNodo(Long idNodo);

	public abstract String getCodigoRuta();

	public abstract void setCodigoRuta(String codigoRuta);

	public abstract Long getIdManzana();

	public abstract void setIdManzana(Long idManzana);

	public abstract Boolean getReasignarCorrelativo();

	public abstract void setReasignarCorrelativo(Boolean reasignarCorrelativo);

	public abstract String getCodigoManzana();

	public abstract void setCodigoManzana(String codigoManzana);

	public abstract String getNroServicio();

	public abstract void setNroServicio(String nroServicio);

	public abstract String getTipoServicio();

	public abstract void setTipoServicio(String tipoServicio);

	public abstract String getDireccion();

	public abstract void setDireccion(String direccion);

	public abstract String getNroComponente();

	public abstract void setNroComponente(String nroComponente);

	public abstract String getCorrelativo();

	public abstract void setCorrelativo(String correlativo);

	public abstract String getAnomalia();

	public abstract void setAnomalia(String anomalia);

	public abstract String getCiclo();

	public abstract void setCiclo(String ciclo);

	public abstract Boolean getActivo();

	public abstract void setActivo(Boolean activo);
	
	public abstract String getSessionId() ;

	public abstract void setSessionId(String sessionId) ;

}