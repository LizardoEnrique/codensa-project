/**
 *
 */
package com.synapsis.dimer.codensa.service.impl;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.lang.StringUtils;

import com.suivant.arquitectura.core.cache.IServiceInvalidateCache;
import com.suivant.arquitectura.core.cache.InvalidateMessage;
import com.suivant.arquitectura.core.integration.Module;
import com.suivant.arquitectura.core.service.IStoredProcedureService;
import com.suivant.arquitectura.core.storedProcedure.StoredProcedureCall;
import com.synapsis.dimer.codensa.model.RutaInfo;
import com.synapsis.dimer.codensa.model.impl.RutaInfoImpl;
import com.synapsis.dimer.codensa.service.RutaService;
import com.synapsis.synergia.core.service.impl.SynergiaServiceImpl;

/**
 * @author dbraccio - Suivant 09/04/2008
 */
public class RutaServiceImpl extends SynergiaServiceImpl implements RutaService {

	// servicio que ejecuta el procedure
	private IStoredProcedureService storedProcedureService;
	//servicio de incvalidacion de cache
	private IServiceInvalidateCache serviceInvalidateCache;
	
	public void updateTablesToSearch(Long idTipoRuta, Long idNodo, String sessionId){
		StoredProcedureCall call = new StoredProcedureCall(
			"{call PRE_CONSULTA_CORRELATIVO(?,?,?)}");
			call.addParameter(1, idNodo);
			call.addParameter(2, idTipoRuta);
			call.addParameter(3, sessionId);
			storedProcedureService.executeStoredProcedure(call);
	}

	/**
	 *  ejecuta el pl de actualizacion de las rutas
	 */
	public void updateOrderUbiRuta(ArrayList rutasSelected,
			RutaInfoImpl rutaSelected, String tipoRuta) {
		StoredProcedureCall call = new StoredProcedureCall(
				"{call UBI_UPDATE_ORDER_UBI_RUTA(?,?,?,?)}");
		call.addParameter(1, rutaSelected.getIdNodo());
		call.addParameter(2, tipoRuta);
		call.addParameter(3, rutaSelected.getCodigoRuta());
		call.addParameter(4, getCodRutasSelected(rutasSelected));
		storedProcedureService.executeStoredProcedure(call);
		serviceInvalidateCache.registerInvalidateMessage(getInvalidateMessage());
	}

	/**
	 * ejecuta el pl que agrega al final las rutas seleccionadas
	 */
	public void updateUbiRutaAlFinal(ArrayList rutasSelected, String tipoRuta, Long idNodo) {
		StoredProcedureCall call = new StoredProcedureCall(
				"{call UBI_UPDATE_UBI_RUTA_AL_FINAL(?,?,?)}");
		call.addParameter(1, idNodo);
		call.addParameter(2, tipoRuta);
		call.addParameter(3, getCodRutasSelected(rutasSelected));
		storedProcedureService.executeStoredProcedure(call);
		serviceInvalidateCache.registerInvalidateMessage(getInvalidateMessage());
	}

	private InvalidateMessage getInvalidateMessage(){
		return InvalidateMessage.getInvalidateMassiveMessageForUpdate("Ruta");
	}
	/**
	 * retorna los codigos de las rutas seleccionadas
	 * @param rutasSelected
	 * @return
	 */
	private String getCodRutasSelected(ArrayList rutasSelected) {
		StringBuffer rutas = new StringBuffer();
		Iterator it = rutasSelected.iterator();
		while (it.hasNext()) {
			RutaInfo rutaInfo = (RutaInfo) it.next();
			rutas.append("'").append(rutaInfo.getCodigoRuta()).append("',");
		}
		// saco la coma del final
		String r = StringUtils.removeEnd(rutas.toString(), ",");
		return r;
	}

	
	
	/**
	 * default constructor
	 * 
	 * @param module
	 */
	public RutaServiceImpl(Module module) {
		super(module);
	}

	// *************************************************************
	// ********* getter and setter
	// ************************************************************
	public IStoredProcedureService getStoredProcedureService() {
		return storedProcedureService;
	}

	public void setStoredProcedureService(
			IStoredProcedureService storedProcedureService) {
		this.storedProcedureService = storedProcedureService;
	}

	public IServiceInvalidateCache getServiceInvalidateCache() {
		return serviceInvalidateCache;
	}

	public void setServiceInvalidateCache(
			IServiceInvalidateCache serviceInvalidateCache) {
		this.serviceInvalidateCache = serviceInvalidateCache;
	}

	
}
