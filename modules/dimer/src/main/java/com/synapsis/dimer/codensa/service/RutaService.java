/**
 *
 */
package com.synapsis.dimer.codensa.service;

import java.util.ArrayList;

import com.suivant.arquitectura.core.service.Service;
import com.synapsis.dimer.codensa.model.impl.RutaInfoImpl;

/**
 * @author dbraccio - Suivant 09/04/2008
 */
public interface RutaService extends Service {

	/**
	 * servicio que se encarga de preparar la tabla para realizar las busquedas
	 * 
	 * @param tipoRuta
	 * @param idNodo
	 * @param sessionId
	 */
	public abstract void updateTablesToSearch(Long idTipoRuta, Long idNodo,
			String sessionId);

	/**
	 * servicio que actualiza el orden las rutas
	 * 
	 * @param rutasSelected
	 * @param rutaSelected
	 * @param tipoRuta
	 */
	public abstract void updateOrderUbiRuta(ArrayList rutasSelected,
			RutaInfoImpl rutaSelected, String tipoRuta);

	/**
	 * servicio que pone al final las rutas seleccionadas
	 * 
	 * @param rutasSelected
	 * @param tipoRuta
	 * @param idNodo
	 */
	public abstract void updateUbiRutaAlFinal(ArrayList rutasSelected,
			String tipoRuta, Long idNodo);

}
