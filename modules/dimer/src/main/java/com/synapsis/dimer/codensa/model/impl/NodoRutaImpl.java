/**
 * 
 */
package com.synapsis.dimer.codensa.model.impl;

import com.synapsis.dimer.codensa.model.NodoRuta;
import com.synapsis.dimer.codensa.model.TipoRuta;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author dbraccio - Suivant 26/03/2008
 */
public class NodoRutaImpl extends SynergiaBusinessObjectImpl implements NodoRuta {

	//private NivelRuta nivelRuta;
	private Long idNivelRuta;
	//private Long idTipoRuta;
	private TipoRuta tipoRuta;
	private String codigoNodo;
	private Long idNodoPadre;
	private String descripcionNodo;
	private Boolean activo;
	private String codigoNodoRuta;

	// ************************************************
	// ****** getter and setter
	// ***********************************************
	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.NodoRuta#getCodigoNodo()
	 */
	public String getCodigoNodo() {
		return codigoNodo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.NodoRuta#setCodigoNodo(java.lang.String)
	 */
	public void setCodigoNodo(String codigoNodo) {
		this.codigoNodo = codigoNodo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.NodoRuta#getIdNodoPadre()
	 */
	public Long getIdNodoPadre() {
		return idNodoPadre;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.NodoRuta#setIdNodoPadre(java.lang.Long)
	 */
	public void setIdNodoPadre(Long idNodoPadre) {
		this.idNodoPadre = idNodoPadre;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.NodoRuta#getDescripcionNodo()
	 */
	public String getDescripcionNodo() {
		return descripcionNodo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.NodoRuta#setDescripcionNodo(java.lang.String)
	 */
	public void setDescripcionNodo(String descripcionNodo) {
		this.descripcionNodo = descripcionNodo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.NodoRuta#getActivo()
	 */
	public Boolean getActivo() {
		return activo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.NodoRuta#setActivo(java.lang.Boolean)
	 */
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.NodoRuta#getCodigoNodoRuta()
	 */
	public String getCodigoNodoRuta() {
		return codigoNodoRuta;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.dimer.codensa.model.impl.NodoRuta#setCodigoNodoRuta(java.lang.String)
	 */
	public void setCodigoNodoRuta(String codigoNodoRuta) {
		this.codigoNodoRuta = codigoNodoRuta;
	}

	public Long getIdNivelRuta() {
		return idNivelRuta;
	}

	public void setIdNivelRuta(Long idNivelRuta) {
		this.idNivelRuta = idNivelRuta;
	}

	public TipoRuta getTipoRuta() {
		return tipoRuta;
	}

	public void setTipoRuta(TipoRuta tipoRuta) {
		this.tipoRuta = tipoRuta;
	}

	

	
}
