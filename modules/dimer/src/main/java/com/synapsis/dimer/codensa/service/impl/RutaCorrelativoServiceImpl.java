/**
 * $Id: RutaCorrelativoServiceImpl.java,v 1.16 2008/10/03 22:03:35 ar29261698 Exp $
 */
package com.synapsis.dimer.codensa.service.impl;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.suivant.arquitectura.core.context.DAOLocator;
import com.suivant.arquitectura.core.exception.ExporterException;
import com.suivant.arquitectura.core.exception.IllegalDAOAccessException;
import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.integration.Module;
import com.suivant.arquitectura.core.model.BusinessObject;
import com.suivant.arquitectura.core.queryFilter.CompositeQueryFilter;
import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;
import com.suivant.arquitectura.core.queryFilter.GreaterThanQueryFilter;
import com.suivant.arquitectura.core.queryFilter.OrderQuery;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.queryFilter.SimpleQueryFilter;
import com.suivant.arquitectura.core.service.CRUDService;
import com.suivant.arquitectura.core.service.IServiceDownloadFile;
import com.suivant.arquitectura.core.service.IStoredProcedureService;
import com.suivant.arquitectura.core.storedProcedure.StoredProcedureCall;
import com.suivant.arquitectura.core.storedProcedure.StoredProcedureResponse;
import com.synapsis.commons.faces.messages.ErrorsManager;
import com.synapsis.dimer.codensa.model.RutaInfo;
import com.synapsis.dimer.codensa.model.TipoRuta;
import com.synapsis.dimer.codensa.model.impl.RutaInfoImpl;
import com.synapsis.dimer.codensa.model.impl.TipoRutaImpl;
import com.synapsis.dimer.codensa.service.RutaCorrelativoService;
import com.synapsis.dimer.codensa.service.RutaService;
import com.synapsis.nuc.codensa.model.Path;
import com.synapsis.synergia.core.service.impl.SynergiaAbstractCrudServiceImpl;

/**
 * @author dbraccio - Suivant 09/04/2008
 */
public class RutaCorrelativoServiceImpl extends SynergiaAbstractCrudServiceImpl
		implements RutaCorrelativoService {

	/**
	 * servicio que realiza la ejecucion de los pl de actualizacion de las rutas
	 */
	private RutaService rutaService;

	/**
	 * servicio que obtiene archivos
	 */
	private IServiceDownloadFile serviceDownloadFile;

	/**
	 * servicio para realizar la ejecucion del proc
	 */
	private IStoredProcedureService storedProcedureService;

	/**
	 * servicio para obtener un objeto Path
	 */
	private CRUDService pathService;

	private static final String pathToSearch = "PathExtraerInfoRuta";

	/**
	 * 
	 * @param module
	 */
	public RutaCorrelativoServiceImpl(Module module) {
		super(module);
	}

	public Boolean exists(QueryFilter filter) {
		return super.defaultExists(filter);
	}

	/**
	 * retorna una lista de rutas
	 */
	public List findByCriteria(QueryFilter filter)
			throws IllegalDAOAccessException {
		return defaultFindByCriteria(filter);
	}

	public BusinessObject findByCriteriaUnique(QueryFilter filter)
			throws ObjectNotFoundException, IllegalDAOAccessException {
		return super.defaultFindByCriteriaUnique(filter);
	}

	public Boolean isValidNewPrimaryKey(QueryFilter filter) {
		return super.defaultIsValidNewPrimaryKey(filter);
	}

	public Integer removeByCriteria(QueryFilter filter) {
		return super.defaultRemoveByCriteria(filter);
	}

	public int findByCriteriaToPageable(QueryFilter arg0) {
		return super.countAll(arg0);
	}

	/**
	 * retorna una lista de rutas ordenada
	 */
	public List findByCriteriaAll(QueryFilter filter, String nroMedidor)
			throws ObjectNotFoundException, CloneNotSupportedException {
		
		if (!this.addNroMedidorFilter(filter, nroMedidor)) {
			return Collections.EMPTY_LIST;
		}

		OrderQuery order = new OrderQuery();
		order.setAttributeName("correlativo");
		((CompositeQueryFilter) filter).addOrder(order);
		return defaultFindByCriteria(filter);
	}

	/**
	 * retorna la cantidad de rutas
	 * 
	 * @throws ObjectNotFoundException
	 * @throws CloneNotSupportedException
	 * @throws IllegalDAOAccessException
	 */
	public int findByCriteriaAllToPageable(QueryFilter filter, String nroMedidor)
			throws ObjectNotFoundException, CloneNotSupportedException {
		
		if (!this.addNroMedidorFilter(filter, nroMedidor)) {
			return 0;
		}
		
		return super.countAll(filter);
	}

	/**
	 * Si se informa un nroMedidor, se agrega el filtro correspondiente. Si al
	 * intentar agregarlo no se encuentra una Ruta para el nroMedidor, devuelve
	 * false y loguea el error a trav�s de ErrorsManager. En el resto de los
	 * casos devuelve true.
	 * 
	 * @param filter
	 * @param nroMedidor
	 * @return
	 */
	private boolean addNroMedidorFilter(QueryFilter filter, String nroMedidor) {
		if (StringUtils.isNotEmpty(nroMedidor)) {
			SimpleQueryFilter sq = new EqualQueryFilter("nroComponente",
					nroMedidor);
			((CompositeQueryFilter) filter).addFilter(sq);

			try {
				RutaInfo rutaInfo = (RutaInfo) findByCriteriaUnique(filter);
				((CompositeQueryFilter) filter).getFiltersList().remove(sq);

				SimpleQueryFilter q = new GreaterThanQueryFilter("correlativo",
						rutaInfo.getCorrelativo(), true);
				((CompositeQueryFilter) filter).addFilter(q);
			} catch (ObjectNotFoundException e) {
				ErrorsManager
						.addError("No se encontr� el Medidor nro. '" + nroMedidor + "' en la Ruta especificada");
				return false;
			}
		}

		return true;
	}

	/**
	 * retorna una lista de Rutas ordenadas para ser asignadas, ordenado por
	 * aquellas que no han sido asignadas aun
	 */
	public List findByCriteriaToAssign(QueryFilter filter) {
		OrderQuery order = new OrderQuery();
		order.setAttributeName("reasignarCorrelativo");
		order.setOrderAsc(false);
		OrderQuery order1 = new OrderQuery();
		order1.setAttributeName("correlativo");
		CompositeQueryFilter cqf = new CompositeQueryFilter();
		cqf.addOrder(order);
		cqf.addOrder(order1);
		cqf.addFilter(filter);
		cqf.setMaxResults(filter.getMaxResults());
		cqf.setPositionStart(filter.getPositionStart());
		return super.defaultFindByCriteria(cqf);
	}

	/**
	 * este metodo es ejecutado automaticamente por la grilla previo a dibujarse
	 */
	public int findByCriteriaToAssignToPageable(QueryFilter filter) {
		return super.countAll(filter);
	}

	/**
	 * realiza la actualizacion de las rutas seleccionadas previamente a la ruta
	 * seleccionada
	 */
	public void actualizarRutas(ArrayList rutasSelected,
			RutaInfoImpl rutaSelected, String tipoRuta) {
		rutaService.updateOrderUbiRuta(rutasSelected, rutaSelected, tipoRuta);
	}

	/**
	 * agrega las rutas seleccionas al final del grupo
	 */
	public void agregarRutasAlFinal(ArrayList rutasSelected, String tipoRuta,
			Long idNodo) {
		rutaService.updateUbiRutaAlFinal(rutasSelected, tipoRuta, idNodo);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.dimer.codensa.service.RutaCorrelativoService#getExportedFile(java.lang.String,
	 *      java.lang.Long)
	 */
	public FileInputStream exportFile(String tipoRuta, Long id_nodo)
			throws Exception {
		QueryFilter filterTP = new EqualQueryFilter("codigo", tipoRuta);
		TipoRuta tipoRutaObject = (TipoRuta) DAOLocator.getInstance().getDAO(
				TipoRutaImpl.class, this.getModule()).findByCriteriaUnique(
				filterTP);
		String fileName = "rutas-" + new Date().getTime();

		// armo el call del proc
		StoredProcedureCall storedProcedureCall = new StoredProcedureCall(
				"DISExtraerInfoRuta");
		storedProcedureCall.addParameter(1, tipoRutaObject.getId().toString());
		storedProcedureCall.addParameter(2, id_nodo.toString());
		storedProcedureCall.addParameter(3, fileName);

		// primero ejecuto el proc
		StoredProcedureResponse response = storedProcedureService
				.executeExternalStoredProcedure(storedProcedureCall);
		String responseString = (String) response.getOriginalResponse();
		if (!responseString.startsWith("OK"))
			throw new ExporterException(
					"El stored procedure retorno un error al ejecutarse");
		// obtengo el path donde deja el archivo generado
		QueryFilter filter = new EqualQueryFilter("key", pathToSearch);
		Path path = (Path) pathService.findByCriteriaUnique(filter);
		// luego obtengo el archivo de
		return serviceDownloadFile.getFile(path.getPath(), fileName);
	}
	
	public String preparedSearch(Long idNodo, Long idTipoRuta){ 
		String sessionId = String.valueOf( new Date().toString());
		rutaService.updateTablesToSearch(idTipoRuta, idNodo, sessionId);
		return sessionId;
	}
	

	// **************************************************************************
	// *** getter and setter
	// *************************************************************************

	public RutaService getRutaService() {
		return rutaService;
	}

	public void setRutaService(RutaService rutaService) {
		this.rutaService = rutaService;
	}

	public IServiceDownloadFile getServiceDownloadFile() {
		return serviceDownloadFile;
	}

	public void setServiceDownloadFile(IServiceDownloadFile serviceDownloadFile) {
		this.serviceDownloadFile = serviceDownloadFile;
	}

	public IStoredProcedureService getStoredProcedureService() {
		return storedProcedureService;
	}

	public void setStoredProcedureService(
			IStoredProcedureService storedProcedureService) {
		this.storedProcedureService = storedProcedureService;
	}

	public CRUDService getPathService() {
		return pathService;
	}

	public void setPathService(CRUDService pathService) {
		this.pathService = pathService;
	}

}
