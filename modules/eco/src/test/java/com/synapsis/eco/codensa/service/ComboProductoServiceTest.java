/**
 * 
 */
package com.synapsis.eco.codensa.service;

import com.suivant.arquitectura.core.model.PersistentObject;
import com.synapsis.eco.codensa.EcoCodensaFinderService;
import com.synapsis.eco.codensa.model.combo.impl.ComboProductoImpl;

/**
 * @author ar29261698
 * 
 */
public class ComboProductoServiceTest extends EcoCodensaFinderService {

	protected Class getBeanClass() {
		return ComboProductoImpl.class;
	}


	protected String getServiceName() {
		return "comboProductoServiceFinder";
	}

}
