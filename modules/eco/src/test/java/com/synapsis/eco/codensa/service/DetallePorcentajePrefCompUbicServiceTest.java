/**
 * 
 */
package com.synapsis.eco.codensa.service;

import com.suivant.arquitectura.core.model.PersistentObject;
import com.synapsis.eco.codensa.EcoCodensaFinderService;
import com.synapsis.eco.codensa.model.impl.DetallePorcentajePrefCompUbicImpl;

/**
 * @author ar29261698
 * 
 */
public class DetallePorcentajePrefCompUbicServiceTest extends
		EcoCodensaFinderService {

	protected Class getBeanClass() {
		return DetallePorcentajePrefCompUbicImpl.class;
	}


	protected String getServiceName() {
		return "detallePorcentajePrefCompUbicServiceFinder";
	}

}
