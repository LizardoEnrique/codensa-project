/*$Id: EcoCodensaTestSuite.java,v 1.10 2007/06/06 16:53:13 ar27816695 Exp $*/
package com.synapsis.eco.codensa;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.synapsis.eco.codensa.service.ComboEstadoEncargosServiceTest;
import com.synapsis.eco.codensa.service.ComboLineaNegociosServiceTest;
import com.synapsis.eco.codensa.service.ComboPlanServiceTest;
import com.synapsis.eco.codensa.service.ComboProductoServiceTest;
import com.synapsis.eco.codensa.service.ComboSocioNegociosServiceTest;
import com.synapsis.eco.codensa.service.DetallePorcentajePrefCompUbicServiceTest;
import com.synapsis.eco.codensa.service.MockServiceForTest;
import com.synapsis.eco.codensa.service.ReporteCargaVentaServiceTest;
import com.synapsis.eco.codensa.service.ReporteCarteraMorososServiceTest;
import com.synapsis.eco.codensa.service.ReporteFacturacionServiceTest;
import com.synapsis.eco.codensa.service.ReportePrefCompraUbicServiceTest;
import com.synapsis.eco.codensa.service.ReporteRecaudoServiceTest;

/**
 * Aca deben ir agregando los TestCases que definan
 * 
 * @autor: chino
 * @date: 26/02/2007
 */

public class EcoCodensaTestSuite {
	public static String SPRING_TEST_CONFIG =  "META-INF/eco/spring/*.xml" ;

	public static Test suite() {
		TestSuite suite = new TestSuite();
		suite.addTestSuite(MockServiceForTest.class);
		suite.addTestSuite(ReporteCargaVentaServiceTest.class);
		suite.addTestSuite(ReporteCarteraMorososServiceTest.class);
		suite.addTestSuite(ReporteFacturacionServiceTest.class);
		suite.addTestSuite(ReportePrefCompraUbicServiceTest.class);
		suite.addTestSuite(ReporteRecaudoServiceTest.class);
		suite.addTestSuite(ComboEstadoEncargosServiceTest.class);
		suite.addTestSuite(ComboLineaNegociosServiceTest.class);
		suite.addTestSuite(ComboPlanServiceTest.class);
		suite.addTestSuite(ComboProductoServiceTest.class);
		suite.addTestSuite(ComboSocioNegociosServiceTest.class);
		suite.addTestSuite(DetallePorcentajePrefCompUbicServiceTest.class);
		return suite;
	}

}
