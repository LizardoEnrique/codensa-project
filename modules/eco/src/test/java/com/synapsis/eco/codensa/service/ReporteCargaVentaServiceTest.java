/**
 * 
 */
package com.synapsis.eco.codensa.service;

import com.suivant.arquitectura.core.model.PersistentObject;
import com.synapsis.eco.codensa.EcoCodensaFinderService;
import com.synapsis.eco.codensa.model.impl.ReporteCargaVentasImpl;

/**
 * @author ar29261698
 *
 */
public class ReporteCargaVentaServiceTest extends EcoCodensaFinderService {

	protected Class getBeanClass() {
		return ReporteCargaVentasImpl.class;
	}

	protected String getServiceName() {
		return "reporteCargaVentasServiceFinder";
	}

}
