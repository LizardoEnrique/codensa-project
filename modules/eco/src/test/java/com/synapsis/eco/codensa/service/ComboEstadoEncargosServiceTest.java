/**
 * 
 */
package com.synapsis.eco.codensa.service;

import com.suivant.arquitectura.core.model.PersistentObject;
import com.synapsis.eco.codensa.EcoCodensaFinderService;
import com.synapsis.eco.codensa.model.combo.impl.ComboEstadoEncargosImpl;

/**
 * @author ar29261698
 * 
 */
public class ComboEstadoEncargosServiceTest extends EcoCodensaFinderService {


	protected Class getBeanClass() {
		return ComboEstadoEncargosImpl.class;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see com.suivant.arquitectura.support.tests.service.SpringCRUDServiceTestCase#getServiceName()
	 */
	protected String getServiceName() {
		return "comboEstadoEncargosServiceFinder";
	}

}
