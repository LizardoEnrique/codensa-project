/**
 * 
 */
package com.synapsis.eco.codensa.service;

import com.suivant.arquitectura.core.model.PersistentObject;
import com.synapsis.eco.codensa.EcoCodensaFinderService;
import com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbicImpl;

/**
 * @author ar29261698
 * 
 */
public class ReportePrefCompraUbicServiceTest extends EcoCodensaFinderService {

	protected Class getBeanClass() {
		return ReportePrefCompraUbicImpl.class;
	}


	protected String getServiceName() {
		return "reportePrefCompraUbicServiceFinder";
	}

}
