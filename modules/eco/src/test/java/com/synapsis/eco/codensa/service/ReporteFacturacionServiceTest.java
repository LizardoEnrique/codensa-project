/**
 * 
 */
package com.synapsis.eco.codensa.service;

import com.suivant.arquitectura.core.model.PersistentObject;
import com.synapsis.eco.codensa.EcoCodensaFinderService;
import com.synapsis.eco.codensa.model.impl.ReporteFacturacionImpl;

/**
 * @author ar29261698
 * 
 */
public class ReporteFacturacionServiceTest extends EcoCodensaFinderService {

	protected Class getBeanClass() {
		return ReporteFacturacionImpl.class;
	}

	protected String getServiceName() {
		return "reporteFacturacionServiceFinder";
	}

}
