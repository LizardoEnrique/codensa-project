/**
 * 
 */
package com.synapsis.eco.codensa.service;

import com.suivant.arquitectura.core.model.PersistentObject;
import com.synapsis.eco.codensa.EcoCodensaFinderService;
import com.synapsis.eco.codensa.model.combo.impl.ComboLineaNegociosImpl;

/**
 * @author ar29261698
 * 
 */
public class ComboLineaNegociosServiceTest extends EcoCodensaFinderService {

	protected Class getBeanClass() {
		return ComboLineaNegociosImpl.class;
	}

	protected String getServiceName() {
		return "comboLineaNegociosServiceFinder";
	}

}
