/**
 * 
 */
package com.synapsis.eco.codensa.service;

import com.suivant.arquitectura.core.model.PersistentObject;
import com.synapsis.eco.codensa.EcoCodensaFinderService;
import com.synapsis.eco.codensa.model.combo.impl.ComboPlanImpl;

/**
 * @author ar29261698
 * 
 */
public class ComboPlanServiceTest extends EcoCodensaFinderService {


	protected Class getBeanClass() {
		return ComboPlanImpl.class;
	}



	protected String getServiceName() {
		return "comboPlanServiceFinder";
	}

}
