/*$Id: EcoCodensaFinderService.java,v 1.2 2007/04/05 15:36:51 ar29261698 Exp $*/
package com.synapsis.eco.codensa;

import com.suivant.arquitectura.support.tests.service.SpringFinderServiceTestCase;

/**
 * @autor: chino
 * @date: 26/02/2007
 */
public abstract class EcoCodensaFinderService extends SpringFinderServiceTestCase {


    public EcoCodensaFinderService() {
        super(EcoCodensaTestSuite.SPRING_TEST_CONFIG);
    }
}
