/**
 * 
 */
package com.synapsis.eco.codensa.service;

import com.suivant.arquitectura.core.model.PersistentObject;
import com.synapsis.eco.codensa.EcoCodensaFinderService;
import com.synapsis.eco.codensa.model.combo.impl.ComboSocioNegociosImpl;

/**
 * @author ar29261698
 * 
 */
public class ComboSocioNegociosServiceTest extends EcoCodensaFinderService {

	protected Class getBeanClass() {
		return ComboSocioNegociosImpl.class;
	}

	protected String getServiceName() {
		return "comboSocioNegociosServiceFinder";
	}

}
