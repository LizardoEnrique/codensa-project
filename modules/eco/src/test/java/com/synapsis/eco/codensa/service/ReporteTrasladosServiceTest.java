/**
 * 
 */
package com.synapsis.eco.codensa.service;

import com.suivant.arquitectura.core.model.PersistentObject;
import com.synapsis.eco.codensa.EcoCodensaFinderService;
import com.synapsis.eco.codensa.model.impl.ReporteTrasladosImpl;

/**
 * @author ar29261698
 * 
 */
public class ReporteTrasladosServiceTest extends EcoCodensaFinderService {



	protected Class getBeanClass() {
		return ReporteTrasladosImpl.class;
	}

	protected String getServiceName() {
		return "reporteTrasladosServiceFinder";
	}

}
