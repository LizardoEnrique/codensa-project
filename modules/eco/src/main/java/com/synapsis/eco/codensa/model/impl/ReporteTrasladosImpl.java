/**
 * $id: Clase[ReporteCargaVentasImpl.java], Autor[ar31059727], Time[28/02/2007-10:03:20]
 */
package com.synapsis.eco.codensa.model.impl;

import java.util.Date;

import com.synapsis.eco.codensa.model.ReporteTraslados;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * BO ReporteCargaVentasImpl.
 * 
 * @author ar26942422
 */
public class ReporteTrasladosImpl extends SynergiaBusinessObjectImpl implements
		ReporteTraslados {
	private static final long serialVersionUID = 1L;

	/**
	 * Linea de Negocio
	 */
	private String linNeg;

	/**
	 * Socio de Negocio
	 */
	private String socioNeg;

	/**
	 * Producto
	 */
	private String producto;

	/**
	 * Plan de Negocio
	 */
	private String planNeg;

	/**
	 * Fecha de Traslado
	 */
	private Date fechaTraslado;

	/**
	 * Numero de Cuenta Anterior
	 */
	private Long numCtaAnt;

	/**
	 * Numero de Cuenta Nueva
	 */
	private Long numCtaNueva;

	/**
	 * Valor de Cuota de Encargo de Cobranza
	 */
	private Double valCuotaEncCob;

	/**
	 * Numero de Cuotas Facturadas
	 */
	private Long numCuotasFact;

	/**
	 * Numero de Cuotas Pendientes de Facturar
	 */
	private Long numCuotasPendFact;

	/**
	 * Numero de Cuotas Recaudadas
	 */
	private Long numCuotasRec;

	/**
	 * Retorna la fecha de traslado.
	 * 
	 * @return the fechaTraslado Date
	 */
	public Date getFechaTraslado() {
		return fechaTraslado;
	}

	/**
	 * Setea la fecha de traslado.
	 * 
	 * @param fechaTraslado
	 *            the fechaTraslado to set
	 */
	public void setFechaTraslado(Date fechaTraslado) {
		this.fechaTraslado = fechaTraslado;
	}

	/**
	 * Retorna la linea de negocio.
	 * 
	 * @return the linNeg String
	 */
	public String getLinNeg() {
		return linNeg;
	}

	/**
	 * Setea la linea de negocio.
	 * 
	 * @param linNeg
	 *            the linNeg to set
	 */
	public void setLinNeg(String linNeg) {
		this.linNeg = linNeg;
	}

	/**
	 * @return the numCtaNueva
	 */
	public Long getNumCtaNueva() {
		return numCtaNueva;
	}

	/**
	 * @param numCtaNueva
	 *            the numCtaNueva to set
	 */
	public void setNumCtaNueva(Long numCtaNueva) {
		this.numCtaNueva = numCtaNueva;
	}

	/**
	 * Retorna el numero de la cuenta anterior.
	 * 
	 * @return the numCtaAnt Long
	 */
	public Long getNumCtaAnt() {
		return numCtaAnt;
	}

	/**
	 * Setea el numero de la cuenta nueva.
	 * 
	 * @param numCtaAnt
	 *            the numCtaAnt to set
	 */
	public void setNumCtaAnt(Long numCtaAnt) {
		this.numCtaAnt = numCtaAnt;
	}

	/**
	 * Retorna el numero de cuotas facturadas.
	 * 
	 * @return the numCuotasFact
	 */
	public Long getNumCuotasFact() {
		return numCuotasFact;
	}

	/**
	 * Setea el numero de cuotas facturadas.
	 * 
	 * @param numCuotasFact
	 *            the numCuotasFact to set
	 */
	public void setNumCuotasFact(Long numCuotasFact) {
		this.numCuotasFact = numCuotasFact;
	}

	/**
	 * Retorna el numero de cuotas pendientes de facturar.
	 * 
	 * @return the numCuotasPendFact
	 */
	public Long getNumCuotasPendFact() {
		return numCuotasPendFact;
	}

	/**
	 * Setea el numero de cuotas pendientes de facturar.
	 * 
	 * @param numCuotasPendFact
	 *            the numCuotasPendFact to set
	 */
	public void setNumCuotasPendFact(Long numCuotasPendFact) {
		this.numCuotasPendFact = numCuotasPendFact;
	}

	/**
	 * Retorna el numero de cuotas recaudadas.
	 * 
	 * @return the numCuotasRec
	 */
	public Long getNumCuotasRec() {
		return numCuotasRec;
	}

	/**
	 * Setea el numero de cuotas recaudadas.
	 * 
	 * @param numCuotasRec
	 *            the numCuotasRec to set
	 */
	public void setNumCuotasRec(Long numCuotasRec) {
		this.numCuotasRec = numCuotasRec;
	}

	/**
	 * Retorna el plan de negocio.
	 * 
	 * @return the planNeg
	 */
	public String getPlanNeg() {
		return planNeg;
	}

	/**
	 * Setea el plan de negocio.
	 * 
	 * @param planNeg
	 *            the planNeg to set
	 */
	public void setPlanNeg(String planNeg) {
		this.planNeg = planNeg;
	}

	/**
	 * Retorna el producto.
	 * 
	 * @return the producto
	 */
	public String getProducto() {
		return producto;
	}

	/**
	 * Setea el producto.
	 * 
	 * @param producto
	 *            the producto to set
	 */
	public void setProducto(String producto) {
		this.producto = producto;
	}

	/**
	 * Retorna el socio de negocio.
	 * 
	 * @return the socioNeg
	 */
	public String getSocioNeg() {
		return socioNeg;
	}

	/**
	 * Setea el socio de negocio.
	 * 
	 * @param socioNeg
	 *            the socioNeg to set
	 */
	public void setSocioNeg(String socioNeg) {
		this.socioNeg = socioNeg;
	}

	/**
	 * Retorna el valor de la cuota de encargo de cobranza.
	 * 
	 * @return the valCuotaEncCob
	 */
	public Double getValCuotaEncCob() {
		return valCuotaEncCob;
	}

	/**
	 * Setea el valor de la cuota de encargo de cobranza.
	 * 
	 * @param valCuotaEncCob
	 *            the valCuotaEncCob to set
	 */
	public void setValCuotaEncCob(Double valCuotaEncCob) {
		this.valCuotaEncCob = valCuotaEncCob;
	}
	
	/**
	 * C�digo del socio de negocio del servicio de encargo de cobranza.
	 */
	public Long codSocioNeg;
	
	/**
	 * C�digo correspondiente a la L�nea de Negocio del servicio de encargo de cobranza.
	 */
	public Long codLinNeg;
	
	/**
	 * C�digo correspondiente al plan del servicio de encargo de cobranza.
	 */
	public Long codPlanNeg;
	
	/**
	 * Retorna el c�digo correspondiente al producto del servicio de encargo de cobranza.
	 */
	public Long codProducto;

	/**
	 * Retorna el c�digo correspondiente a la L�nea de Negocio del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodLinNeg() {
		return codLinNeg;
	}

	/**
	 * Setea el c�digo de l�nea de negocio del servicio de encargo de cobranza
	 * 
	 * @param codLineaNeg Long
	 */
	public void setCodLinNeg(Long codLinNeg) {
		this.codLinNeg = codLinNeg;
	}
	
	/**
	 * Retorna el c�digo correspondiente al plan del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodPlanNeg() {
		return codPlanNeg;
	}

	/**
	 * Setea el c�digo de plan de negocio del servicio de encargo de cobranza
	 * 
	 * @param codPlanNeg Long
	 */
	public void setCodPlanNeg(Long codPlanNeg) {
		this.codPlanNeg = codPlanNeg;
	}

	/**
	 * Retorna el c�digo correspondiente al producto del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodProducto() {
		return codProducto;
	}
	
	/**
	 * Setea el c�digo de producto del servicio de encargo de cobranza
	 * 
	 * @param codProducto Long
	 */
	public void setCodProducto(Long codProducto) {
		this.codProducto = codProducto;
	}

	/**
	 * Retorna el c�digo del socio de negocio del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodSocioNeg() {
		return codSocioNeg;
	}

	/**
	 * Setea el c�digo de socio de negocio del servicio de encargo de cobranza
	 * 
	 * @param codSocioNeg Long
	 */
	public void setCodSocioNeg(Long codSocioNeg) {
		this.codSocioNeg = codSocioNeg;
	}
}