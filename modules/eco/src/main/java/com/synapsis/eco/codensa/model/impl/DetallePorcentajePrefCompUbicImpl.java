/**
 * $id: Clase[ReporteCargaVentasImpl.java], Autor[ar31059727], Time[28/02/2007-10:03:20]
 */
package com.synapsis.eco.codensa.model.impl;

import com.synapsis.eco.codensa.model.DetallePorcentajePrefCompUbic;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * BO DetallePorcentajePrefCompUbicImpl.
 * 
 * @author ar26942422
 */
public class DetallePorcentajePrefCompUbicImpl extends
		SynergiaBusinessObjectImpl implements DetallePorcentajePrefCompUbic {
	private static final long serialVersionUID = 1L;

	private Long idServicio;

	private String cargo;

	private String impuesto;

	private Long porcentaje;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.DetallePorcentajePrefCompUbic#getCargo()
	 */
	public String getCargo() {
		return cargo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.DetallePorcentajePrefCompUbic#setCargo(java.lang.String)
	 */
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.DetallePorcentajePrefCompUbic#getImpuesto()
	 */
	public String getImpuesto() {
		return impuesto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.DetallePorcentajePrefCompUbic#setImpuesto(java.lang.Double)
	 */
	public void setImpuesto(String impuesto) {
		this.impuesto = impuesto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.DetallePorcentajePrefCompUbic#getPorcentaje()
	 */
	public Long getPorcentaje() {
		return porcentaje;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.DetallePorcentajePrefCompUbic#setPorcentaje(java.lang.Double)
	 */
	public void setPorcentaje(Long porcentaje) {
		this.porcentaje = porcentaje;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.DetallePorcentajePrefCompUbic#getServicio()
	 */
	public Long getIdServicio() {
		return idServicio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.DetallePorcentajePrefCompUbic#setServicio(java.lang.String)
	 */
	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}
}