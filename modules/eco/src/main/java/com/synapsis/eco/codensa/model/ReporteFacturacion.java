package com.synapsis.eco.codensa.model;

import java.util.Date;

public interface ReporteFacturacion {

	/**
	 * @return the fechaCargue
	 */
	public Date getFechaCargue();

	/**
	 * @param fechaCargue the fechaCargue to set
	 */
	public void setFechaCargue(Date fechaCargue);

	/**
	 * @return the fechaUltFac
	 */
	public Date getFechaUltFac();

	/**
	 * @param fechaUltFac the fechaUltFac to set
	 */
	public void setFechaUltFac(Date fechaUltFac);

	/**
	 * @return the fecPriVto
	 */
	public Date getFecPriVto();

	/**
	 * @param fecPriVto the fecPriVto to set
	 */
	public void setFecPriVto(Date fecPriVto);

	/**
	 * @return the fecSdoVto
	 */
	public Date getFecSdoVto();

	/**
	 * @param fecSdoVto the fecSdoVto to set
	 */
	public void setFecSdoVto(Date fecSdoVto);

	/**
	 * @return the linNeg
	 */
	public String getLinNeg();

	/**
	 * @param linNeg the linNeg to set
	 */
	public void setLinNeg(String linNeg);

	/**
	 * @return the numCta
	 */
	public Long getNumCta();

	/**
	 * @param numCta the numCta to set
	 */
	public void setNumCta(Long numCta);

	/**
	 * @return the numDocTit
	 */
	public String getNumDocTit();

	/**
	 * @param numDocTit the numDocTit to set
	 */
	public void setNumDocTit(String numDocTit);

	/**
	 * @return the numIdentificador
	 */
	public Long getNumIdentificador();

	/**
	 * @param numIdentificador the numIdentificador to set
	 */
	public void setNumIdentificador(Long numIdentificador);

	/**
	 * @return the numServicio
	 */
	public Long getNumServicio();

	/**
	 * @param numServicio the numServicio to set
	 */
	public void setNumServicio(Long numServicio);

	/**
	 * @return the numTotCuotasEnc
	 */
	public Long getNumTotCuotasEnc();

	/**
	 * @param numTotCuotasEnc the numTotCuotasEnc to set
	 */
	public void setNumTotCuotasEnc(Long numTotCuotasEnc);

	/**
	 * @return the numUltCuotaFac
	 */
	public Long getNumUltCuotaFac();

	/**
	 * @param numUltCuotaFac the numUltCuotaFac to set
	 */
	public void setNumUltCuotaFac(Long numUltCuotaFac);

	/**
	 * @return the numUltimaFac
	 */
	public Long getNumUltimaFac();

	/**
	 * @param numUltimaFac the numUltimaFac to set
	 */
	public void setNumUltimaFac(Long numUltimaFac);

	/**
	 * @return the planNeg
	 */
	public String getPlanNeg();

	/**
	 * @param planNeg the planNeg to set
	 */
	public void setPlanNeg(String planNeg);

	/**
	 * @return the producto
	 */
	public String getProducto();

	/**
	 * @param producto the producto to set
	 */
	public void setProducto(String producto);

	/**
	 * @return the socioNeg
	 */
	public String getSocioNeg();

	/**
	 * @param socioNeg the socioNeg to set
	 */
	public void setSocioNeg(String socioNeg);

	/**
	 * @return the tipoDocTit
	 */
	public String getTipoDocTit();

	/**
	 * @param tipoDocTit the tipoDocTit to set
	 */
	public void setTipoDocTit(String tipoDocTit);

	/**
	 * @return the valCuotaEnc
	 */
	public Double getValCuotaEnc();

	/**
	 * @param valCuotaEnc the valCuotaEnc to set
	 */
	public void setValCuotaEnc(Double valCuotaEnc);

	/**
	 * @return the valFacEnc
	 */
	public Double getValFacEnc();

	/**
	 * @param valFacEnc the valFacEnc to set
	 */
	public void setValFacEnc(Double valFacEnc);

	/**
	 * @return the valFacImpEnc
	 */
	public Double getValFacImpEnc();

	/**
	 * @param valFacImpEnc the valFacImpEnc to set
	 */
	public void setValFacImpEnc(Double valFacImpEnc);

	/**
	 * @return the valFacIntMora
	 */
	public Double getValFacIntMora();

	/**
	 * @param valFacIntMora the valFacIntMora to set
	 */
	public void setValFacIntMora(Double valFacIntMora);

	/**
	 * @return the valTotFacEnc
	 */
	public Double getValTotFacEnc();

	/**
	 * @param valTotFacEnc the valTotFacEnc to set
	 */
	public void setValTotFacEnc(Double valTotFacEnc);

	/**
	 * @return the valTotFacImpEnc
	 */
	public Double getValTotFacImpEnc();

	/**
	 * @param valTotFacImpEnc the valTotFacImpEnc to set
	 */
	public void setValTotFacImpEnc(Double valTotFacImpEnc);

	/**
	 * @return the valTotFacIntMora
	 */
	public Double getValTotFacIntMora();

	/**
	 * @param valTotFacIntMora the valTotFacIntMora to set
	 */
	public void setValTotFacIntMora(Double valTotFacIntMora);
	
	/**
	 * Retorna el c�digo del socio de negocio del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodSocioNeg();
	
	/**
	 * Retorna el c�digo correspondiente a la L�nea de Negocio del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodLinNeg();
	
	/**
	 * Retorna el c�digo correspondiente al plan del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodPlanNeg();
	
	/**
	 * Retorna el c�digo correspondiente al producto del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodProducto();
	
	/**
	 * Setea el c�digo del socio de negocio del servicio de encargo de cobranza.
	 * @param Long codSocioNeg
	 */
	public void setCodSocioNeg(Long codSocioNeg);
	
	/**
	 * Setea el c�digo correspondiente a la L�nea de Negocio del servicio de encargo de cobranza.
	 * @param Long codLinNeg
	 */
	public void setCodLinNeg(Long codLinNeg);
	
	/**
	 * Setea el c�digo correspondiente al plan del servicio de encargo de cobranza.
	 * @param Long codPlanNeg
	 */
	public void setCodPlanNeg(Long codPlanNeg);
	
	/**
	 * Setea el c�digo correspondiente al producto del servicio de encargo de cobranza.
	 * @param Long codProducto
	 */
	public void setCodProducto(Long codProducto);
	
	/**
	 * @return the sucursalRf
	 */
	public String getSucursalRf();

	/**
	 * @param sucursalRf the sucursalRf to set
	 */
	public void setSucursalRf(String sucursalRf);


	/**
	 * @return the zonaRf
	 */
	public String getZonaRf();

	/**
	 * @param zonaRf the zonaRf to set
	 */
	public void setZonaRf(String zonaRf);

	/**
	 * @return the grupoRf
	 */
	public String getGrupoRf();

	/**
	 * @param grupoRf the grupoRf to set
	 */
	public void setGrupoRf(String grupoRf);


	/**
	 * @return the cicloRf
	 */
	public String getCicloRf();

	/**
	 * @param cicloRf the cicloRf to set
	 */
	public void setCicloRf(String cicloRf);

}