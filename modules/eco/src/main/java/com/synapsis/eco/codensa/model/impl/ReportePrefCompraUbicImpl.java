package com.synapsis.eco.codensa.model.impl;

import java.util.Date;

import com.synapsis.eco.codensa.model.ReportePrefCompraUbic;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * BO ReportePrefCompraUbicImpl.
 * 
 * @author ar26942422
 */
public class ReportePrefCompraUbicImpl extends SynergiaBusinessObjectImpl
		implements ReportePrefCompraUbic {
	private static final long serialVersionUID = 1L;

	/**
	 * Linea de Negocio
	 */
	private String linNeg;

	/**
	 * Socio de Negocio
	 */
	private String socioNeg;

	/**
	 * Producto
	 */
	private String producto;

	/**
	 * Plan de Negocio
	 */
	private String planNeg;

	private Long numCta;

	private Long numServicio;

	private Long numIdentificador;

	private String tipoDocTit;

	private String numDocTit;

	private Date fechaCargue;

	private String numCuotasEnc;

	private Double valCuotaEnc;

	private String valTotalEnc;

	private Long numCuotasFacNoRec;

	private Date fecUltFac;

	private String estadoEnc;

	private String estratoSe;

	private String sucursalRf;

	private String zonaRf;

	private String cicloRf;

	private String grupoRf;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#getCicloRf()
	 */
	public String getCicloRf() {
		return cicloRf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#setCicloRf(java.lang.String)
	 */
	public void setCicloRf(String cicloRf) {
		this.cicloRf = cicloRf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#getEstadoEnc()
	 */
	public String getEstadoEnc() {
		return estadoEnc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#setEstadoEnc(java.lang.String)
	 */
	public void setEstadoEnc(String estadoEnc) {
		this.estadoEnc = estadoEnc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#getEstratoSe()
	 */
	public String getEstratoSe() {
		return estratoSe;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#setEstratoSe(java.lang.String)
	 */
	public void setEstratoSe(String estratoSe) {
		this.estratoSe = estratoSe;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#getFechaCargue()
	 */
	public Date getFechaCargue() {
		return fechaCargue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#setFechaCargue(java.util.Date)
	 */
	public void setFechaCargue(Date fechaCargue) {
		this.fechaCargue = fechaCargue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#getFecUltFac()
	 */
	public Date getFecUltFac() {
		return fecUltFac;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#setFecUltFac(java.util.Date)
	 */
	public void setFecUltFac(Date fecUltFac) {
		this.fecUltFac = fecUltFac;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#getGrupoRf()
	 */
	public String getGrupoRf() {
		return grupoRf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#setGrupoRf(java.lang.String)
	 */
	public void setGrupoRf(String grupoRf) {
		this.grupoRf = grupoRf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#getLinNeg()
	 */
	public String getLinNeg() {
		return linNeg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#setLinNeg(java.lang.String)
	 */
	public void setLinNeg(String linNeg) {
		this.linNeg = linNeg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#getNumCta()
	 */
	public Long getNumCta() {
		return numCta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#setNumCta(java.lang.Long)
	 */
	public void setNumCta(Long numCta) {
		this.numCta = numCta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#getNumCuotasEnc()
	 */
	public String getNumCuotasEnc() {
		return numCuotasEnc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#setNumCuotasEnc(java.lang.Long)
	 */
	public void setNumCuotasEnc(String numCuotasEnc) {
		this.numCuotasEnc = numCuotasEnc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#getNumCuotasFacNoRec()
	 */
	public Long getNumCuotasFacNoRec() {
		return numCuotasFacNoRec;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#setNumCuotasFacNoRec(java.lang.Long)
	 */
	public void setNumCuotasFacNoRec(Long numCuotasFacNoRec) {
		this.numCuotasFacNoRec = numCuotasFacNoRec;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#getNumDocTit()
	 */
	public String getNumDocTit() {
		return numDocTit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#setNumDocTit(java.lang.String)
	 */
	public void setNumDocTit(String numDocTit) {
		this.numDocTit = numDocTit;
	}

	/**
	 * @return the numIdentificador
	 */
	public Long getNumIdentificador() {
		return numIdentificador;
	}

	/**
	 * @param numIdentificador
	 *            the numIdentificador to set
	 */
	public void setNumIdentificador(Long numIdentificador) {
		this.numIdentificador = numIdentificador;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#getNumServicio()
	 */
	public Long getNumServicio() {
		return numServicio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#setNumServicio(java.lang.Long)
	 */
	public void setNumServicio(Long numServicio) {
		this.numServicio = numServicio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#getPlanNeg()
	 */
	public String getPlanNeg() {
		return planNeg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#setPlanNeg(java.lang.String)
	 */
	public void setPlanNeg(String planNeg) {
		this.planNeg = planNeg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#getProducto()
	 */
	public String getProducto() {
		return producto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#setProducto(java.lang.String)
	 */
	public void setProducto(String producto) {
		this.producto = producto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#getSocioNeg()
	 */
	public String getSocioNeg() {
		return socioNeg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#setSocioNeg(java.lang.String)
	 */
	public void setSocioNeg(String socioNeg) {
		this.socioNeg = socioNeg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#getSucursalRf()
	 */
	public String getSucursalRf() {
		return sucursalRf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#setSucursalRf(java.lang.String)
	 */
	public void setSucursalRf(String sucursalRf) {
		this.sucursalRf = sucursalRf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#getTipoDocTit()
	 */
	public String getTipoDocTit() {
		return tipoDocTit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#setTipoDocTit(java.lang.String)
	 */
	public void setTipoDocTit(String tipoDocTit) {
		this.tipoDocTit = tipoDocTit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#getValCuotaEnc()
	 */
	public Double getValCuotaEnc() {
		return valCuotaEnc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#setValCuotaEnc(java.lang.Double)
	 */
	public void setValCuotaEnc(Double valCuotaEnc) {
		this.valCuotaEnc = valCuotaEnc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#getValTotalEnc()
	 */
	public String getValTotalEnc() {
		return valTotalEnc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#setValTotalEnc(java.lang.Double)
	 */
	public void setValTotalEnc(String valTotalEnc) {
		this.valTotalEnc = valTotalEnc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#getZonaRf()
	 */
	public String getZonaRf() {
		return zonaRf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReportePrefCompraUbic#setZonaRf(java.lang.String)
	 */
	public void setZonaRf(String zonaRf) {
		this.zonaRf = zonaRf;
	}

	/**
	 * C�digo de la Linea de Negocio
	 */
	private Long codLinNeg;

	/**
	 * C�digo del Socio de Negocio
	 */
	private Long codSocioNeg;

	/**
	 * C�digo del Producto
	 */
	private Long codProducto;

	/**
	 * C�digo del Plan de Negocio
	 */
	private Long codPlanNeg;

	/* (non-Javadoc)
	 * @see com.synapsis.eco.codensa.model.impl.dummyinterfaz#getCodLinNeg()
	 */
	public Long getCodLinNeg() {
		return codLinNeg;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.eco.codensa.model.impl.dummyinterfaz#setCodLinNeg(java.lang.Long)
	 */
	public void setCodLinNeg(Long codLinNeg) {
		this.codLinNeg = codLinNeg;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.eco.codensa.model.impl.dummyinterfaz#getCodPlanNeg()
	 */
	public Long getCodPlanNeg() {
		return codPlanNeg;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.eco.codensa.model.impl.dummyinterfaz#setCodPlanNeg(java.lang.Long)
	 */
	public void setCodPlanNeg(Long codPlanNeg) {
		this.codPlanNeg = codPlanNeg;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.eco.codensa.model.impl.dummyinterfaz#getCodProducto()
	 */
	public Long getCodProducto() {
		return codProducto;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.eco.codensa.model.impl.dummyinterfaz#setCodProducto(java.lang.Long)
	 */
	public void setCodProducto(Long codProducto) {
		this.codProducto = codProducto;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.eco.codensa.model.impl.dummyinterfaz#getCodSocioNeg()
	 */
	public Long getCodSocioNeg() {
		return codSocioNeg;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.eco.codensa.model.impl.dummyinterfaz#setCodSocioNeg(java.lang.Long)
	 */
	public void setCodSocioNeg(Long codSocioNeg) {
		this.codSocioNeg = codSocioNeg;
	}
}