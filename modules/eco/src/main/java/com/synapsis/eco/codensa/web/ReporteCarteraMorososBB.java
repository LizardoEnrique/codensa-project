package com.synapsis.eco.codensa.web;

import java.util.Date;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.model.SelectItem;

import org.apache.myfaces.custom.service.UIService;

import com.suivant.arquitectura.presentation.utils.ComboFactory;
import com.synapsis.nuc.codensa.utils.DateConverterUtils;

/**
 * Backing Bean para la vista de CUECO025: Reporte de Carga de Ventas.
 * 
 * @author ar31059727
 * @author dbraccio - assert solutions
 */
public class ReporteCarteraMorososBB implements BackingBeansConstants {

	private static final long serialVersionUID = 1L;

	/* Property para los combos */
	private SelectItem[] comboLineaNegocios;
	private SelectItem[] comboSocioNegocios;
	private SelectItem[] comboProducto;
	private SelectItem[] comboPlan;

	/*
	 * property de los filtros de busqueda
	 */
	private Date fechaCargue;
	private Date fechaCargueHasta;
	private Long codLinNeg;
	private Long codSocioNeg;
	private Long codProducto;
	private Long codPlanNeg;
	private Long numCuotasFactNpMora;
	// fecha formateada para pasarla al filtro
	private Date fechaCargueHastaFormatted;

	// componente que realiza la busqueda
	private UIService finderService;
	private HtmlDataTable tableResults;

	/* Realiza el refresco de la pagina cuando se ejecuta la consulta */
	public void search() {
		getFinderService().execute(true);
		getTableResults().setFirst(0);
	}

	/*
	 * Getters de los combos
	 */
	public SelectItem[] getComboLineaNegocios() {
		if (comboLineaNegocios == null) {
			comboLineaNegocios = ComboFactory.makeCombo(
					SRV_COMBO_LINEA_NEGOCIOS, EMPTY_QUERY_FILTER, DESCRIPCION,
					CODIGO, null, new Long(-1));
		}
		return comboLineaNegocios;
	}

	public SelectItem[] getComboSocioNegocios() {
		if (comboSocioNegocios == null) {
			comboSocioNegocios = ComboFactory.makeCombo(
					SRV_COMBO_SOCIO_NEGOCIOS, EMPTY_QUERY_FILTER, DESCRIPCION,
					CODIGO, null, new Long(-1));
		}
		return comboSocioNegocios;
	}

	public SelectItem[] getComboProducto() {
		if (comboProducto == null) {
			comboProducto = ComboFactory
					.makeCombo(SRV_COMBO_PRODUCTO, EMPTY_QUERY_FILTER,
							DESCRIPCION, CODIGO, null, new Long(-1));
		}
		return comboProducto;
	}

	public SelectItem[] getComboPlan() {
		if (comboPlan == null) {
			comboPlan = ComboFactory
					.makeCombo(SRV_COMBO_PLAN, EMPTY_QUERY_FILTER, DESCRIPCION,
							CODIGO, null, new Long(-1));
		}
		return comboPlan;
	}

	/*
	 * Setters para los combos para que sean persistidos
	 */
	public void setComboLineaNegocios(SelectItem[] comboLineaNegocios) {
		this.comboLineaNegocios = comboLineaNegocios;
	}

	public void setComboPlan(SelectItem[] comboPlan) {
		this.comboPlan = comboPlan;
	}

	public void setComboProducto(SelectItem[] comboProducto) {
		this.comboProducto = comboProducto;
	}

	public void setComboSocioNegocios(SelectItem[] comboSocioNegocios) {
		this.comboSocioNegocios = comboSocioNegocios;
	}

	/**
	 * getter and setter de los demas properties
	 */
	public Date getFechaCargue() {
		return fechaCargue;
	}

	public void setFechaCargue(Date fechaCargue) {
		this.fechaCargue = fechaCargue;
	}

	public Date getFechaCargueHasta() {
		return fechaCargueHasta;
	}

	public void setFechaCargueHasta(Date fechaCargueHasta) {
		this.fechaCargueHasta = fechaCargueHasta;
	}

	public Long getCodLinNeg() {
		return codLinNeg;
	}

	public void setCodLinNeg(Long codLinNeg) {
		this.codLinNeg = codLinNeg;
	}

	public Long getCodSocioNeg() {
		return codSocioNeg;
	}

	public void setCodSocioNeg(Long codSocioNeg) {
		this.codSocioNeg = codSocioNeg;
	}

	public Long getCodProducto() {
		return codProducto;
	}

	public void setCodProducto(Long codProducto) {
		this.codProducto = codProducto;
	}

	public Long getCodPlanNeg() {
		return codPlanNeg;
	}

	public void setCodPlanNeg(Long codPlanNeg) {
		this.codPlanNeg = codPlanNeg;
	}

	public Long getNumCuotasFactNpMora() {
		return numCuotasFactNpMora;
	}

	public void setNumCuotasFactNpMora(Long numCuotasFactNpMora) {
		this.numCuotasFactNpMora = numCuotasFactNpMora;
	}

	public Date getFechaCargueHastaFormatted() {
		return DateConverterUtils
				.getDateFormatddmmyyyy(fechaCargueHasta);
	}

	public void setFechaCargueHastaFormatted(Date fechaCargueHastaFormatted) {
		this.fechaCargueHastaFormatted = fechaCargueHastaFormatted;
	}

	public UIService getFinderService() {
		return finderService;
	}

	public void setFinderService(UIService finderService) {
		this.finderService = finderService;
	}

	public HtmlDataTable getTableResults() {
		return tableResults;
	}

	public void setTableResults(HtmlDataTable tableResults) {
		this.tableResults = tableResults;
	}

}
