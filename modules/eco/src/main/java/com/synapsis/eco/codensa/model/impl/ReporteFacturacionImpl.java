package com.synapsis.eco.codensa.model.impl;

import java.util.Date;

import com.synapsis.eco.codensa.model.ReporteFacturacion;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * BO ReporteFacturacionImpl.
 * 
 * @author ar26942422
 */
public class ReporteFacturacionImpl extends SynergiaBusinessObjectImpl
		implements ReporteFacturacion {
	private static final long serialVersionUID = 1L;

	/**
	 * Linea de Negocio
	 */
	private String linNeg;

	/**
	 * Socio de Negocio
	 */
	private String socioNeg;

	/**
	 * Producto
	 */
	private String producto;

	private String planNeg;

	private Long numCta;

	private Long numServicio;

	private Long numIdentificador;

	private String tipoDocTit;

	private String numDocTit;

	private Date fechaCargue;

	private Date fechaUltFac;

	private Long numUltimaFac;

	private Double valFacEnc;

	private Double valFacIntMora;

	private Double valFacImpEnc;

	private Double valTotFacEnc;

	private Double valTotFacIntMora;

	private Double valTotFacImpEnc;

	private Long numTotCuotasEnc;

	private Long numUltCuotaFac;

	private Double valCuotaEnc;

	private Date fecPriVto;

	private Date fecSdoVto;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#getFechaCargue()
	 */
	public Date getFechaCargue() {
		return fechaCargue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#setFechaCargue(java.util.Date)
	 */
	public void setFechaCargue(Date fechaCargue) {
		this.fechaCargue = fechaCargue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#getFechaUltFac()
	 */
	public Date getFechaUltFac() {
		return fechaUltFac;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#setFechaUltFac(java.util.Date)
	 */
	public void setFechaUltFac(Date fechaUltFac) {
		this.fechaUltFac = fechaUltFac;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#getFecPriVto()
	 */
	public Date getFecPriVto() {
		return fecPriVto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#setFecPriVto(java.util.Date)
	 */
	public void setFecPriVto(Date fecPriVto) {
		this.fecPriVto = fecPriVto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#getFecSdoVto()
	 */
	public Date getFecSdoVto() {
		return fecSdoVto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#setFecSdoVto(java.util.Date)
	 */
	public void setFecSdoVto(Date fecSdoVto) {
		this.fecSdoVto = fecSdoVto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#getLinNeg()
	 */
	public String getLinNeg() {
		return linNeg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#setLinNeg(java.lang.String)
	 */
	public void setLinNeg(String linNeg) {
		this.linNeg = linNeg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#getNumCta()
	 */
	public Long getNumCta() {
		return numCta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#setNumCta(java.lang.Long)
	 */
	public void setNumCta(Long numCta) {
		this.numCta = numCta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#getNumDocTit()
	 */
	public String getNumDocTit() {
		return numDocTit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#setNumDocTit(java.lang.String)
	 */
	public void setNumDocTit(String numDocTit) {
		this.numDocTit = numDocTit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#getNumIdentificador()
	 */
	public Long getNumIdentificador() {
		return numIdentificador;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#setNumIdentificador(java.lang.Long)
	 */
	public void setNumIdentificador(Long numIdentificador) {
		this.numIdentificador = numIdentificador;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#getNumServicio()
	 */
	public Long getNumServicio() {
		return numServicio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#setNumServicio(java.lang.Long)
	 */
	public void setNumServicio(Long numServicio) {
		this.numServicio = numServicio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#getNumTotCuotasEnc()
	 */
	public Long getNumTotCuotasEnc() {
		return numTotCuotasEnc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#setNumTotCuotasEnc(java.lang.Long)
	 */
	public void setNumTotCuotasEnc(Long numTotCuotasEnc) {
		this.numTotCuotasEnc = numTotCuotasEnc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#getNumUltCuotaFac()
	 */
	public Long getNumUltCuotaFac() {
		return numUltCuotaFac;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#setNumUltCuotaFac(java.lang.Long)
	 */
	public void setNumUltCuotaFac(Long numUltCuotaFac) {
		this.numUltCuotaFac = numUltCuotaFac;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#getNumUltimaFac()
	 */
	public Long getNumUltimaFac() {
		return numUltimaFac;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#setNumUltimaFac(java.lang.Long)
	 */
	public void setNumUltimaFac(Long numUltimaFac) {
		this.numUltimaFac = numUltimaFac;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#getPlanNeg()
	 */
	public String getPlanNeg() {
		return planNeg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#setPlanNeg(java.lang.String)
	 */
	public void setPlanNeg(String planNeg) {
		this.planNeg = planNeg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#getProducto()
	 */
	public String getProducto() {
		return producto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#setProducto(java.lang.String)
	 */
	public void setProducto(String producto) {
		this.producto = producto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#getSocioNeg()
	 */
	public String getSocioNeg() {
		return socioNeg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#setSocioNeg(java.lang.String)
	 */
	public void setSocioNeg(String socioNeg) {
		this.socioNeg = socioNeg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#getTipoDocTit()
	 */
	public String getTipoDocTit() {
		return tipoDocTit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#setTipoDocTit(java.lang.String)
	 */
	public void setTipoDocTit(String tipoDocTit) {
		this.tipoDocTit = tipoDocTit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#getValCuotaEnc()
	 */
	public Double getValCuotaEnc() {
		return valCuotaEnc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#setValCuotaEnc(java.lang.Double)
	 */
	public void setValCuotaEnc(Double valCuotaEnc) {
		this.valCuotaEnc = valCuotaEnc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#getValFacEnc()
	 */
	public Double getValFacEnc() {
		return valFacEnc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#setValFacEnc(java.lang.Double)
	 */
	public void setValFacEnc(Double valFacEnc) {
		this.valFacEnc = valFacEnc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#getValFacImpEnc()
	 */
	public Double getValFacImpEnc() {
		return valFacImpEnc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#setValFacImpEnc(java.lang.Double)
	 */
	public void setValFacImpEnc(Double valFacImpEnc) {
		this.valFacImpEnc = valFacImpEnc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#getValFacIntMora()
	 */
	public Double getValFacIntMora() {
		return valFacIntMora;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#setValFacIntMora(java.lang.Double)
	 */
	public void setValFacIntMora(Double valFacIntMora) {
		this.valFacIntMora = valFacIntMora;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#getValTotFacEnc()
	 */
	public Double getValTotFacEnc() {
		return valTotFacEnc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#setValTotFacEnc(java.lang.Double)
	 */
	public void setValTotFacEnc(Double valTotFacEnc) {
		this.valTotFacEnc = valTotFacEnc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#getValTotFacImpEnc()
	 */
	public Double getValTotFacImpEnc() {
		return valTotFacImpEnc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#setValTotFacImpEnc(java.lang.Double)
	 */
	public void setValTotFacImpEnc(Double valTotFacImpEnc) {
		this.valTotFacImpEnc = valTotFacImpEnc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#getValTotFacIntMora()
	 */
	public Double getValTotFacIntMora() {
		return valTotFacIntMora;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteFacturacion#setValTotFacIntMora(java.lang.Double)
	 */
	public void setValTotFacIntMora(Double valTotFacIntMora) {
		this.valTotFacIntMora = valTotFacIntMora;
	}

	/**
	 * C�digo del socio de negocio del servicio de encargo de cobranza.
	 */
	public Long codSocioNeg;
	
	/**
	 * C�digo correspondiente a la L�nea de Negocio del servicio de encargo de cobranza.
	 */
	public Long codLinNeg;
	
	/**
	 * C�digo correspondiente al plan del servicio de encargo de cobranza.
	 */
	public Long codPlanNeg;
	
	/**
	 * Retorna el c�digo correspondiente al producto del servicio de encargo de cobranza.
	 */
	public Long codProducto;

	/**
	 * Retorna el c�digo correspondiente a la L�nea de Negocio del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodLinNeg() {
		return codLinNeg;
	}

	/**
	 * Setea el c�digo de l�nea de negocio del servicio de encargo de cobranza
	 * 
	 * @param codLineaNeg Long
	 */
	public void setCodLinNeg(Long codLinNeg) {
		this.codLinNeg = codLinNeg;
	}
	
	/**
	 * Retorna el c�digo correspondiente al plan del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodPlanNeg() {
		return codPlanNeg;
	}

	/**
	 * Setea el c�digo de plan de negocio del servicio de encargo de cobranza
	 * 
	 * @param codPlanNeg Long
	 */
	public void setCodPlanNeg(Long codPlanNeg) {
		this.codPlanNeg = codPlanNeg;
	}

	/**
	 * Retorna el c�digo correspondiente al producto del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodProducto() {
		return codProducto;
	}
	
	/**
	 * Setea el c�digo de producto del servicio de encargo de cobranza
	 * 
	 * @param codProducto Long
	 */
	public void setCodProducto(Long codProducto) {
		this.codProducto = codProducto;
	}

	/**
	 * Retorna el c�digo del socio de negocio del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodSocioNeg() {
		return codSocioNeg;
	}

	/**
	 * Setea el c�digo de socio de negocio del servicio de encargo de cobranza
	 * 
	 * @param codSocioNeg Long
	 */
	public void setCodSocioNeg(Long codSocioNeg) {
		this.codSocioNeg = codSocioNeg;
	}
	
	private String sucursalRf;

	private String zonaRf;

	private String cicloRf;

	private String grupoRf;

	public String getCicloRf() {
		return cicloRf;
	}

	public void setCicloRf(String cicloRf) {
		this.cicloRf = cicloRf;
	}

	public String getGrupoRf() {
		return grupoRf;
	}

	public void setGrupoRf(String grupoRf) {
		this.grupoRf = grupoRf;
	}

	public String getSucursalRf() {
		return sucursalRf;
	}

	public void setSucursalRf(String sucursalRf) {
		this.sucursalRf = sucursalRf;
	}

	public String getZonaRf() {
		return zonaRf;
	}

	public void setZonaRf(String zonaRf) {
		this.zonaRf = zonaRf;
	}
}