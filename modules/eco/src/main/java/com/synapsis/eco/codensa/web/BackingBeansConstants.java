/**
 * $id: Clase[BackingBeansConstants.java], Autor[ar31059727], Time[28/02/2007-10:15:27]
 */
package com.synapsis.eco.codensa.web;

/**
 * Clase de soporte con las constantes utilizadas desde los Backing Beans.
 * 
 * @author ar31059727
 */
public interface BackingBeansConstants {
	// Nombre de servicio de cada Combo.
	public static final String SRV_COMBO_LINEA_NEGOCIOS = "comboLineaNegociosServiceFinder";

	public static final String SRV_COMBO_SOCIO_NEGOCIOS = "comboSocioNegociosServiceFinder";

	public static final String SRV_COMBO_PRODUCTO = "comboProductoServiceFinder";

	public static final String SRV_COMBO_PLAN = "comboPlanServiceFinder";

	public static final String SRV_COMBO_TIPO_DOCUMENTO = "ecoComboTipoDocumentoServiceFinder";

	public static final String SRV_COMBO_ESTADO_ENCARGOS = "comboEstadoEncargosServiceFinder";

	public static final String SRV_COMBO_SUCURSAL_RF = "comboSucursalRFServiceFinder";

	public static final String SRV_COMBO_ZONA_RF = "comboZonaRFServiceFinder";

	public static final String SRV_COMBO_CICLO_RF = "comboCicloRFServiceFinder";

	public static final String SRV_COMBO_GRUPO_RF = "comboGrupoRFServiceFinder";

	public static final String CODIGO = "codigo";

	public static final String DESCRIPCION = "descripcion";

	public static final String EMPTY_QUERY_FILTER = "ecoEmptyQueryFilter"; 
}