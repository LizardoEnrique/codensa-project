package com.synapsis.eco.codensa.web;

import javax.faces.model.SelectItem;

import com.suivant.arquitectura.presentation.model.ListableBB;
import com.suivant.arquitectura.presentation.utils.ComboFactory;
import com.synapsis.integration.codensa.backingBeans.helpers.DataTableFactoryRefresh;

/**
 * Backing Bean para la vista de CUECO047: Reporte de Preferencia de Compra por
 * Ubicación.
 * 
 * @author ar26942422
 */
public class ReportePrefCompraUbicBB extends ListableBB implements
		BackingBeansConstants {
	private static final long serialVersionUID = 1L;

	/* Nombres de componentes y filtros a utilizar */
	private final static String nombreComponenteReporteCarga = "reportePrefCompraUbicForm:reportePrefCompraUbicTable";

	private final static String nombreQueryFilterReporteCarga = "reportePrefCompraUbicQueryFilter";

	/* Property para los combos */
	private SelectItem[] comboLineaNegocios;

	private SelectItem[] comboSocioNegocios;

	private SelectItem[] comboProducto;

	private SelectItem[] comboPlan;

	private SelectItem[] comboEstadoEncargos;

	/* Getters */
	public SelectItem[] getComboLineaNegocios() {
		if (comboLineaNegocios == null) {
			comboLineaNegocios = ComboFactory.makeCombo(
					SRV_COMBO_LINEA_NEGOCIOS, EMPTY_QUERY_FILTER, DESCRIPCION,
					CODIGO, null, new Long(-1));
		}
		return comboLineaNegocios;
	}

	public SelectItem[] getComboSocioNegocios() {
		if (comboSocioNegocios == null) {
			comboSocioNegocios = ComboFactory.makeCombo(
					SRV_COMBO_SOCIO_NEGOCIOS, EMPTY_QUERY_FILTER, DESCRIPCION,
					CODIGO, null, new Long(-1));
		}
		return comboSocioNegocios;
	}

	public SelectItem[] getComboProducto() {
		if (comboProducto == null) {
			comboProducto = ComboFactory
					.makeCombo(SRV_COMBO_PRODUCTO, EMPTY_QUERY_FILTER,
							DESCRIPCION, CODIGO, null, new Long(-1));
		}
		return comboProducto;
	}

	public SelectItem[] getComboPlan() {
		if (comboPlan == null) {
			comboPlan = ComboFactory
					.makeCombo(SRV_COMBO_PLAN, EMPTY_QUERY_FILTER, DESCRIPCION,
							CODIGO, null, new Long(-1));
		}
		return comboPlan;
	}

	public SelectItem[] getComboEstadoEncargos() {
		if (comboEstadoEncargos == null) {
			comboEstadoEncargos = ComboFactory.makeCombo(
					SRV_COMBO_ESTADO_ENCARGOS, EMPTY_QUERY_FILTER, DESCRIPCION,
					CODIGO);
		}
		return comboEstadoEncargos;
	}

	/* Realiza el refresco de la pagina cuando se ejecuta la consulta */
	public void reportePrefCompraUbicQueryFilter() {
		DataTableFactoryRefresh.makeDataTable(nombreComponenteReporteCarga,
				nombreQueryFilterReporteCarga, getServiceName(), getMetadata());
	}

	/* Setters para los combos para que sean persistidos */
	public void setComboEstadoEncargos(SelectItem[] comboEstadoEncargos) {
		this.comboEstadoEncargos = comboEstadoEncargos;
	}

	public void setComboLineaNegocios(SelectItem[] comboLineaNegocios) {
		this.comboLineaNegocios = comboLineaNegocios;
	}

	public void setComboPlan(SelectItem[] comboPlan) {
		this.comboPlan = comboPlan;
	}

	public void setComboProducto(SelectItem[] comboProducto) {
		this.comboProducto = comboProducto;
	}

	public void setComboSocioNegocios(SelectItem[] comboSocioNegocios) {
		this.comboSocioNegocios = comboSocioNegocios;
	}
}