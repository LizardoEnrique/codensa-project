package com.synapsis.eco.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * Interface del BO ReporteCargaVentasImpl.
 * 
 * @author ar31059727
 */
public interface ReporteCargaVentas extends SynergiaBusinessObject {

	/**
	 * @return the cargoCap
	 */
	public String getCargoCap();

	/**
	 * @param cargoCap
	 *            the cargoCap to set
	 */
	public void setCargoCap(String cargoCap);

	/**
	 * @return the cicloRf
	 */
	public String getCicloRf();

	/**
	 * @param cicloRf
	 *            the cicloRf to set
	 */
	public void setCicloRf(String cicloRf);

	/**
	 * @return the clase
	 */
	public String getClase();

	/**
	 * @param clase
	 *            the clase to set
	 */
	public void setClase(String clase);

	/**
	 * @return the claseSe
	 */
	public String getClaseSe();

	/**
	 * @param claseSe
	 *            the claseSe to set
	 */
	public void setClaseSe(String claseSe);

	/**
	 * @return the codClase
	 */
	public Long getCodClase();

	/**
	 * @param codClase
	 *            the codClase to set
	 */
	public void setCodClase(Long codClase);

	/**
	 * @return the codLinNeg
	 */
	public Long getCodLinNeg();

	/**
	 * @param codLinNeg
	 *            the codLinNeg to set
	 */
	public void setCodLinNeg(Long codLinNeg);

	/**
	 * @return the codPlan
	 */
	public Long getCodPlan();

	/**
	 * @param codPlan
	 *            the codPlan to set
	 */
	public void setCodPlan(Long codPlan);

	/**
	 * @return the codSocio
	 */
	public Long getCodSocio();

	/**
	 * @param codSocio
	 *            the codSocio to set
	 */
	public void setCodSocio(Long codSocio);

	/**
	 * @return the codTipoDocTit
	 */
	public Long getCodTipoDocTit();

	/**
	 * @param codTipoDocTit
	 *            the codTipoDocTit to set
	 */
	public void setCodTipoDocTit(Long codTipoDocTit);

	/**
	 * @return the estado
	 */
	public String getEstado();

	/**
	 * @param estado
	 *            the estado to set
	 */
	public void setEstado(String estado);

	/**
	 * @return the estratoSe
	 */
	public String getEstratoSe();

	/**
	 * @param estratoSe
	 *            the estratoSe to set
	 */
	public void setEstratoSe(String estratoSe);

	/**
	 * @return the fechaInicio
	 */
	public Date getFechaInicio();

	/**
	 * @param fechaInicio
	 *            the fechaInicio to set
	 */
	public void setFechaInicio(Date fechaInicio);

	/**
	 * @return the fecUltCambEst
	 */
	public Date getFecUltCambEst();

	/**
	 * @param fecUltCambEst
	 *            the fecUltCambEst to set
	 */
	public void setFecUltCambEst(Date fecUltCambEst);

	/**
	 * @return the grupoRf
	 */
	public String getGrupoRf();

	/**
	 * @param grupoRf
	 *            the grupoRf to set
	 */
	public void setGrupoRf(String grupoRf);

	/**
	 * @return the linNeg
	 */
	public String getLinNeg();

	/**
	 * @param linNeg
	 *            the linNeg to set
	 */
	public void setLinNeg(String linNeg);

	/**
	 * @return the moneda
	 */
	public String getMoneda();

	/**
	 * @param moneda
	 *            the moneda to set
	 */
	public void setMoneda(String moneda);

	/**
	 * @return the motivoFin
	 */
	public String getMotivoFin();

	/**
	 * @param motivoFin
	 *            the motivoFin to set
	 */
	public void setMotivoFin(String motivoFin);

	/**
	 * @return the nombreTit
	 */
	public String getNombreTit();

	/**
	 * @param nombreTit
	 *            the nombreTit to set
	 */
	public void setNombreTit(String nombreTit);

	/**
	 * @return the numCuenta
	 */
	public Long getNumCuenta();

	/**
	 * @param numCuenta
	 *            the numCuenta to set
	 */
	public void setNumCuenta(Long numCuenta);

	/**
	 * @return the numCuotas
	 */
	public Long getNumCuotas();

	/**
	 * @param numCuotas
	 *            the numCuotas to set
	 */
	public void setNumCuotas(Long numCuotas);

	/**
	 * @return the numDocTit
	 */
	public String getNumDocTit();

	/**
	 * @param numDocTit
	 *            the numDocTit to set
	 */
	public void setNumDocTit(String numDocTit);

	/**
	 * @return the numEco
	 */
	public Long getNumEco();

	/**
	 * @param numEco
	 *            the numEco to set
	 */
	public void setNumEco(Long numEco);

	/**
	 * @return the numIdTit
	 */
	public Long getNumIdTit();

	/**
	 * @param numIdTit
	 *            the numIdTit to set
	 */
	public void setNumIdTit(Long numIdTit);

	/**
	 * @return the plan
	 */
	public String getPlan();

	/**
	 * @param plan
	 *            the plan to set
	 */
	public void setPlan(String plan);

	/**
	 * @return the socio
	 */
	public String getSocio();

	/**
	 * @param socio
	 *            the socio to set
	 */
	public void setSocio(String socio);

	/**
	 * @return the subClaseSe
	 */
	public String getSubClaseSe();

	/**
	 * @param subClaseSe
	 *            the subClaseSe to set
	 */
	public void setSubClaseSe(String subClaseSe);

	/**
	 * @return the sucursalRf
	 */
	public String getSucursalRf();

	/**
	 * @param sucursalRf
	 *            the sucursalRf to set
	 */
	public void setSucursalRf(String sucursalRf);

	/**
	 * @return the tipoDocTit
	 */
	public String getTipoDocTit();

	/**
	 * @param tipoDocTit
	 *            the tipoDocTit to set
	 */
	public void setTipoDocTit(String tipoDocTit);

	/**
	 * @return the valCuotaEnc
	 */
	public Double getValCuotaEnc();

	/**
	 * @param valCuotaEnc
	 *            the valCuotaEnc to set
	 */
	public void setValCuotaEnc(Double valCuotaEnc);

	/**
	 * @return the valTotalEncCob
	 */
	public Double getValTotalEncCob();

	/**
	 * @param valTotalEncCob
	 *            the valTotalEncCob to set
	 */
	public void setValTotalEncCob(Double valTotalEncCob);

	/**
	 * @return the zonaRf
	 */
	public String getZonaRf();

	/**
	 * @param zonaRf
	 *            the zonaRf to set
	 */
	public void setZonaRf(String zonaRf);

}