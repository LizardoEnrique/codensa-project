package com.synapsis.eco.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * Interface del BO ReporteCarteraMorososImpl.
 * 
 * @author ar26942422
 */
public interface ReporteCarteraMorosos extends SynergiaBusinessObject  {

	/**
	 * Retorna el ciclo de la ruta de facturaci�n 
	 * @return String
	 */
	public String getCicloRf();

	/**
	 * Setea el valor al ciclo del a ruta de facturaci�n
	 * @param cicloRf String
	 */
	public void setCicloRf(String cicloRf);

	/**
	 * Retorna la clase de servicio del servicio el�ctrico
	 * @return String
	 */
	public String getClaseSe();

	/**
	 * Setea la clase de servicio del servicio el�ctrico
	 * @param claseSe String
	 */
	public void setClaseSe(String claseSe);

	/**
	 * Retorna la cantidad de d�as de atraso, la misma surge de la resta
	 * de la fecha actual con la fecha de la cuota m�s antigua no pagada.
	 * @return Long
	 */
	public Long getDiasAtraso();

	/**
	 * Setea la cantidad de d�as de atraso en el pago de las cuotas.
	 * @param diasAtraso Long
	 */
	public void setDiasAtraso(Long diasAtraso);

	/**
	 * Retorna el estrato de servicio el�ctrico
	 * @return String
	 */
	public String getEstratoSe();

	/**
	 * Setea el estrato de servicio el�ctrico
	 * @param estratoSe String
	 */
	public void setEstratoSe(String estratoSe);

	/**
	 * Retorna la fecha del cargue.
	 * @return Date
	 */
	public Date getFechaCargue();
	
	/**
	 * Setea la fecha del cargue.
	 * @param fechaCargue Date
	 */
	public void setFechaCargue(Date fechaCargue);

	/**
	 * Retorna el grupo de ruta de facturaci�n.
	 * @return String
	 */
	public String getGrupoRf();

	/**
	 * Setea el grupo de ruta de facturaci�n.
	 * @param grupoRf String
	 */
	public void setGrupoRf(String grupoRf);

	/**
	 * Retorna la L�nea de Negocio del servicio de encargo de cobranza.
	 * @return String
	 */
	public String getLinNeg();

	
	
	/**
	 * Setea la L�nea de Negocio del servicio de encargo de cobranza.
	 * @param linNeg String
	 */
	public void setLinNeg(String linNeg);

	/**
	 * Retorna el Numero de Cuenta del servicio de encargo de cobranza..
	 * @return Long
	 */
	public Long getNumCta();

	/**
	 * Setea el Numero de Cuenta del servicio de encargo de cobranza.
	 * @param numCta Long
	 */
	public void setNumCta(Long numCta);

	/**
	 * Retorna el Numero de Cuotas del encargo de cobranza.
	 * @return Long
	 */
	public Long getNumCuotasEncCob();

	/**
	 * Setea el Numero de Cuotas del encargo de cobranza.
	 * @param numCuotasEncCob Long
	 */
	public void setNumCuotasEncCob(Long numCuotasEncCob);

	/**
	 * Retorna el numero de cuotas facturadas.
	 * @return Long
	 */
	public Long getNumCuotasFact();

	/**
	 * Setea el numero de cuotas facturadas.
	 * @param numCuotasFact Long
	 */
	public void setNumCuotasFact(Long numCuotasFact);

	/**
	 * Retorna el numero de cuotas facturadas no pagadas en mora.
	 * @return Long
	 */
	public Long getNumCuotasFactNpMora();

	/**
	 * Setea el numero de cuotas facturadas no pagadas en mora.
	 * @param numCuotasFactNpMora Long
	 */
	public void setNumCuotasFactNpMora(Long numCuotasFactNpMora);

	/**
	 * Retorna el numero de cuotas facturadas no pagadas en plazo.
	 * @return Long
	 */
	public Long getNumCuotasFactNpPlazo();

	/**
	 * Setea el numero de cuotas facturadas no pagadas en plazo.
	 * @param numCuotasFactNpPlazo Long
	 */
	public void setNumCuotasFactNpPlazo(Long numCuotasFactNpPlazo);

	/**
	 * Retorna el numero de cuotas pagadas.
	 * @return Long
	 */
	public Long getNumCuotasPg();

	/**
	 * Setea el numero de cuotas pagadas.
	 * @param numCuotasPg Long
	 */
	public void setNumCuotasPg(Long numCuotasPg);

	/**
	 * Retorna el numero de documento del titular.
	 * @return String
	 */
	public String getNumDocTit();

	/**
	 * Setea el numero de documento del titular. 
	 * @param numDocTit String
	 */
	public void setNumDocTit(String numDocTit);

	/**
	 * Retorna el numero de identificador.
	 * @return Long
	 */
	public Long getNumIdentificador();

	/**
	 * Setea el numero de identificador.
	 * @param numIdentificador Long
	 */
	public void setNumIdentificador(Long numIdentificador);

	/**
	 * Retorna el numero de servicio.
	 * @return Long
	 */
	public Long getNumServicio();

	/**
	 * Setea el numero de servicio.
	 * @param numServicio Long
	 */
	public void setNumServicio(Long numServicio);

	/**
	 * Retorna el plan de negocio del servicio de encargo de cobranza.
	 * @return String
	 */
	public String getPlanNeg();

	/**
	 * Setea el plan de negocio del servicio de encargo de cobranza.
	 * @param planNeg
	 */
	public void setPlanNeg(String planNeg);

	/**
	 * Retorna el producto del servicio de encargo de cobranza.
	 * @return String
	 */
	public String getProducto();

	/**
	 * Setea el producto del servicio de encargo de cobranza.
	 * @param producto String
	 */
	public void setProducto(String producto);

	/**
	 * Retorna el socio de negocio del servicio de encargo de cobranza.
	 * @return String
	 */
	public String getSocioNeg();

	
	
	/**
	 * Setea el socio de negocio del servicio de encargo de cobranza.
	 * @param socioNeg String
	 */
	public void setSocioNeg(String socioNeg);

	/**
	 * Retorna la subclase de servicio del servicio el�ctrico.
	 * @return String
	 */
	public String getSubclaseSe();

	/**
	 * Setea la subclase de servicio del servicio el�ctrico.
	 * @param subclaseSe String
	 */
	public void setSubclaseSe(String subclaseSe);

	/**
	 * Retorna la sucursal de ruta de facturacion.
	 * @return String
	 */
	public String getSucursalRf();

	/**
	 * Setea la sucursal de ruta de facturacion.
	 * @param sucursalRf String
	 */
	public void setSucursalRf(String sucursalRf);

	/**
	 * Retorna el tipo de documento del titular del servicio de encargo de cobranza.
	 * @return String
	 */
	public String getTipoDocTit();

	/**
	 * Setea el tipo de documento del titular del servicio de encargo de cobranza.
	 * @param tipoDocTit String
	 */
	public void setTipoDocTit(String tipoDocTit);

	/**
	 * Retorna el valor de la cuota de encargo de cobranza.
	 * @return Double
	 */
	public Double getValCuotaEncCob();

	/**
	 * Setea el valor de la cuota de encargo de cobranza.
	 * @param valCuotaEncCob Double
	 */
	public void setValCuotaEncCob(Double valCuotaEncCob);

	/**
	 * Retorna el valor de las cuotas facturadas.
	 * @return Double
	 */
	public Double getValCuotaFact();

	/**
	 * Setea el valor de las cuotas facturadas.
	 * @param valCuotaFact Double
	 */
	public void setValCuotaFact(Double valCuotaFact);

	/**
	 * Retorna el valor de las cuotas facturadas no pagadas en mora.
	 * @return Double
	 */
	public Double getValCuotaFactNpMora();

	/**
	 * Setea el valor de las cuotas facturadas no pagadas en mora.
	 * @param valCuotaFactNpMora Double
	 */
	public void setValCuotaFactNpMora(Double valCuotaFactNpMora);

	/**
	 * Retorna el valor de las cuotas facturadas no pagadas en plazo.
	 * @return Double
	 */
	public Double getValCuotaFactNpPlazo();

	/**
	 * Setea el valor de las cuotas facturadas no pagadas en plazo.
	 * @param valCuotaFactNpPlazo Double
	 */
	public void setValCuotaFactNpPlazo(Double valCuotaFactNpPlazo);

	/**
	 * Retorna el valor de las cuotas pagadas.
	 * @return Double
	 */
	public Double getValCuotaPg();

	/**
	 * Setea el valor de las cuotas pagadas.
	 * @param valCuotaPg Double
	 */
	public void setValCuotaPg(Double valCuotaPg);

	/**
	 * Retorna el valor del encargo de cobranza.
	 * @return Double
	 */
	public Double getValEncCob();

	/**
	 * Setea el valor del encargo de cobranza.
	 * @param valEncCob Double
	 */
	public void setValEncCob(Double valEncCob);

	/**
	 * Retorna el valor de los impuestos facturados.
	 * @return Double
	 */
	public Double getValImpFact();

	/**
	 * Setea el valor de los impuestos facturados.
	 * @param valImpFact Double
	 */
	public void setValImpFact(Double valImpFact);

	/**
	 * Retorna el valor de los impuestos facturados no pagados en mora.
	 * @return Double
	 */
	public Double getValImpFactNpMora();

	/**
	 * Setea el valor de los impuestos facturados no pagados en mora.
	 * @param valImpFactNpMora Double
	 */
	public void setValImpFactNpMora(Double valImpFactNpMora);

	/**
	 * Retorna el valor de los impuestos facturados no pagados en plazo.
	 * @return Double
	 */
	public Double getValImpFactNpPlazo();

	/**
	 * Setea el valor de los impuestos facturados no pagados en plazo.
	 * @param valImpFactNpPlazo Double
	 */
	public void setValImpFactNpPlazo(Double valImpFactNpPlazo);
	
	/**
	 * Retorna el valor de los impuestos pagados.
	 * @return Double
	 */
	public Double getValImpPg();

	/**
	 * Setea el valor de los impuestos pagados.
	 * @param valImpPg Double
	 */
	public void setValImpPg(Double valImpPg);

	/**
	 * Retorna el valor de los intereses de mora facturados.
	 * @return Double
	 */
	public Double getValIntMoraFact();

	/**
	 * Setea el valor de los intereses de mora facturados.
	 * @param valIntMoraFact Double
	 */
	public void setValIntMoraFact(Double valIntMoraFact);

	/**
	 * Retorna el valor de los intereses de mora facturados no pagados en mora.
	 * @return Double
	 */
	public Double getValIntMoraFactNpMora();

	/**
	 * Setea el valor de los intereses de mora facturados no pagados en mora.
	 * @param valIntMoraFactNpMora Double
	 */
	public void setValIntMoraFactNpMora(Double valIntMoraFactNpMora);

	/**
	 * Retorna el valor de los intereses de mora facturados no pagados en plazo.
	 * @return Double
	 */
	public Double getValIntMoraFactNpPlazo();

	/**
	 * Setea el valor de los intereses de mora facturados no pagados en plazo.
	 * @param valIntMoraFactNpPlazo Double
	 */	
	public void setValIntMoraFactNpPlazo(Double valIntMoraFactNpPlazo);

	/**
	 * Retorna el valor de los intereses de mora pagados.
	 * @return Double
	 */
	public Double getValIntMoraPg();

	/**
	 * Setea el valor de los intereses de mora pagados.
	 * @param valIntMoraPg Double
	 */
	public void setValIntMoraPg(Double valIntMoraPg);

	/**
	 * Retorna la zona de la ruta de facturacion.
	 * @return String
	 */
	public String getZonaRf();

	/**
	 * Setea la zona de la ruta de facturacion.
	 * @param zonaRf String
	 */
	public void setZonaRf(String zonaRf);

	/**
	 * Retorna el c�digo del socio de negocio del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodSocioNeg();
	
	/**
	 * Retorna el c�digo correspondiente a la L�nea de Negocio del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodLinNeg();
	
	/**
	 * Retorna el c�digo correspondiente al plan del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodPlanNeg();
	
	/**
	 * Retorna el c�digo correspondiente al producto del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodProducto();
	
	/**
	 * Setea el c�digo del socio de negocio del servicio de encargo de cobranza.
	 * @param Long codSocioNeg
	 */
	public void setCodSocioNeg(Long codSocioNeg);
	
	/**
	 * Setea el c�digo correspondiente a la L�nea de Negocio del servicio de encargo de cobranza.
	 * @param Long codLinNeg
	 */
	public void setCodLinNeg(Long codLinNeg);
	
	/**
	 * Setea el c�digo correspondiente al plan del servicio de encargo de cobranza.
	 * @param Long codPlanNeg
	 */
	public void setCodPlanNeg(Long codPlanNeg);
	
	/**
	 * Setea el c�digo correspondiente al producto del servicio de encargo de cobranza.
	 * @param Long codProducto
	 */
	public void setCodProducto(Long codProducto);
	
}