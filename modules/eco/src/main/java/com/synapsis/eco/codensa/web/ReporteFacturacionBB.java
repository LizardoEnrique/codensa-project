package com.synapsis.eco.codensa.web;

import java.util.Date;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.model.SelectItem;

import org.apache.myfaces.custom.service.UIService;

import com.suivant.arquitectura.presentation.utils.ComboFactory;
import com.synapsis.nuc.codensa.utils.DateConverterUtils;

/**
 * Backing Bean para la vista de CUECO044: Reporte de Facturación.
 * 
 * @author ar26942422
 */
public class ReporteFacturacionBB implements BackingBeansConstants {

	private static final long serialVersionUID = 1L;

	/* Property para los combos */
	private SelectItem[] comboLineaNegocios;
	private SelectItem[] comboSocioNegocios;
	private SelectItem[] comboProducto;
	private SelectItem[] comboPlan;

	// filtros para realizar la busqueda
	private Date fechaUltFac;
	private Date fechaUltFacHasta;
	private String sucursalRf;
	private String zonaRf;
	private String cicloRf;
	private String grupoRf;
	private Long codLinNeg;
	private Long codSocioNeg;
	private Long codProducto;
	private Long codPlanNeg;
	// fecha formateada
	private Date fechaUltFacHastaFormatted;

	// componente que realiza la busqueda
	private UIService finderService;
	private HtmlDataTable tableResults;

	/* Realiza la busqueda */
	public void search() {
		getFinderService().execute(true);
		getTableResults().setFirst(0);
	}

	/*
	 * Getters de los combos
	 */
	public SelectItem[] getComboLineaNegocios() {
		if (comboLineaNegocios == null) {
			comboLineaNegocios = ComboFactory.makeCombo(
					SRV_COMBO_LINEA_NEGOCIOS, EMPTY_QUERY_FILTER, DESCRIPCION,
					CODIGO, null, new Long(-1));
		}
		return comboLineaNegocios;
	}

	public SelectItem[] getComboSocioNegocios() {
		if (comboSocioNegocios == null) {
			comboSocioNegocios = ComboFactory.makeCombo(
					SRV_COMBO_SOCIO_NEGOCIOS, EMPTY_QUERY_FILTER, DESCRIPCION,
					CODIGO, null, new Long(-1));
		}
		return comboSocioNegocios;
	}

	public SelectItem[] getComboProducto() {
		if (comboProducto == null) {
			comboProducto = ComboFactory
					.makeCombo(SRV_COMBO_PRODUCTO, EMPTY_QUERY_FILTER,
							DESCRIPCION, CODIGO, null, new Long(-1));
		}
		return comboProducto;
	}

	public SelectItem[] getComboPlan() {
		if (comboPlan == null) {
			comboPlan = ComboFactory
					.makeCombo(SRV_COMBO_PLAN, EMPTY_QUERY_FILTER, DESCRIPCION,
							CODIGO, null, new Long(-1));
		}
		return comboPlan;
	}

	/*
	 * Setters para los combos para que sean persistidos
	 */
	public void setComboLineaNegocios(SelectItem[] comboLineaNegocios) {
		this.comboLineaNegocios = comboLineaNegocios;
	}

	public void setComboPlan(SelectItem[] comboPlan) {
		this.comboPlan = comboPlan;
	}

	public void setComboProducto(SelectItem[] comboProducto) {
		this.comboProducto = comboProducto;
	}

	public void setComboSocioNegocios(SelectItem[] comboSocioNegocios) {
		this.comboSocioNegocios = comboSocioNegocios;
	}

	public Date getFechaUltFac() {
		return fechaUltFac;
	}

	public void setFechaUltFac(Date fechaUltFac) {
		this.fechaUltFac = fechaUltFac;
	}

	public Date getFechaUltFacHasta() {
		return fechaUltFacHasta;
	}

	public void setFechaUltFacHasta(Date fechaUltFacHasta) {
		this.fechaUltFacHasta = fechaUltFacHasta;
	}

	public String getSucursalRf() {
		return sucursalRf;
	}

	public void setSucursalRf(String sucursalRf) {
		this.sucursalRf = sucursalRf;
	}

	public String getZonaRf() {
		return zonaRf;
	}

	public void setZonaRf(String zonaRf) {
		this.zonaRf = zonaRf;
	}

	public String getCicloRf() {
		return cicloRf;
	}

	public void setCicloRf(String cicloRf) {
		this.cicloRf = cicloRf;
	}

	public String getGrupoRf() {
		return grupoRf;
	}

	public void setGrupoRf(String grupoRf) {
		this.grupoRf = grupoRf;
	}

	public Long getCodLinNeg() {
		return codLinNeg;
	}

	public void setCodLinNeg(Long codLinNeg) {
		this.codLinNeg = codLinNeg;
	}

	public Long getCodSocioNeg() {
		return codSocioNeg;
	}

	public void setCodSocioNeg(Long codSocioNeg) {
		this.codSocioNeg = codSocioNeg;
	}

	public Long getCodProducto() {
		return codProducto;
	}

	public void setCodProducto(Long codProducto) {
		this.codProducto = codProducto;
	}

	public Long getCodPlanNeg() {
		return codPlanNeg;
	}

	public void setCodPlanNeg(Long codPlanNeg) {
		this.codPlanNeg = codPlanNeg;
	}

	public Date getFechaUltFacHastaFormatted() {
		return DateConverterUtils.getDateFormatddmmyyyy(fechaUltFacHasta);
	}

	public void setFechaUltFacHastaFormatted(Date fechaUltFacHastaFormatted) {
		this.fechaUltFacHastaFormatted = fechaUltFacHastaFormatted;
	}

	public UIService getFinderService() {
		return finderService;
	}

	public void setFinderService(UIService finderService) {
		this.finderService = finderService;
	}

	public HtmlDataTable getTableResults() {
		return tableResults;
	}

	public void setTableResults(HtmlDataTable tableResults) {
		this.tableResults = tableResults;
	}

}