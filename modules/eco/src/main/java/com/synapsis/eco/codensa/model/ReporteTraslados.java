package com.synapsis.eco.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ReporteTraslados extends SynergiaBusinessObject {

	/**
	 * Retorna la fecha de traslado.
	 * 
	 * @return the fechaTraslado Date
	 */
	public Date getFechaTraslado();

	/**
	 * Setea la fecha de traslado.
	 * 
	 * @param fechaTraslado
	 *            the fechaTraslado to set
	 */
	public void setFechaTraslado(Date fechaTraslado);

	/**
	 * Retorna la linea de negocio.
	 * 
	 * @return the linNeg String
	 */
	public String getLinNeg();

	/**
	 * Setea la linea de negocio.
	 * 
	 * @param linNeg
	 *            the linNeg to set
	 */
	public void setLinNeg(String linNeg);

	/**
	 * Retorna el numero de la cuenta nueva.
	 * 
	 * @return the numCtaNueva Long
	 */
	public Long getNumCtaNueva();

	/**
	 * Setea el numero de la cuenta nueva.
	 * 
	 * @param numCtaNueva
	 *            the numCtaNueva to set
	 */
	public void setNumCtaNueva(Long numCtaNueva);

	/**
	 * Retorna el numero de la cuenta anterior.
	 * 
	 * @return the numCtaAnt Long
	 */
	public Long getNumCtaAnt();

	/**
	 * Setea el numero de la cuenta nueva.
	 * 
	 * @param numCtaAnt
	 *            the numCtaAnt to set
	 */
	public void setNumCtaAnt(Long numCtaAnt);

	/**
	 * Retorna el numero de cuotas facturadas.
	 * 
	 * @return the numCuotasFact
	 */
	public Long getNumCuotasFact();

	/**
	 * Setea el numero de cuotas facturadas.
	 * 
	 * @param numCuotasFact
	 *            the numCuotasFact to set
	 */
	public void setNumCuotasFact(Long numCuotasFact);

	/**
	 * Retorna el numero de cuotas pendientes de facturar.
	 * 
	 * @return the numCuotasPendFact
	 */
	public Long getNumCuotasPendFact();

	/**
	 * Setea el numero de cuotas pendientes de facturar.
	 * 
	 * @param numCuotasPendFact
	 *            the numCuotasPendFact to set
	 */
	public void setNumCuotasPendFact(Long numCuotasPendFact);

	/**
	 * Retorna el numero de cuotas recaudadas.
	 * 
	 * @return the numCuotasRec
	 */
	public Long getNumCuotasRec();

	/**
	 * Setea el numero de cuotas recaudadas.
	 * 
	 * @param numCuotasRec
	 *            the numCuotasRec to set
	 */
	public void setNumCuotasRec(Long numCuotasRec);

	/**
	 * Retorna el plan de negocio.
	 * 
	 * @return the planNeg
	 */
	public String getPlanNeg();

	/**
	 * Setea el plan de negocio.
	 * 
	 * @param planNeg
	 *            the planNeg to set
	 */
	public void setPlanNeg(String planNeg);

	/**
	 * Retorna el producto.
	 * 
	 * @return the producto
	 */
	public String getProducto();

	/**
	 * Setea el producto.
	 * 
	 * @param producto
	 *            the producto to set
	 */
	public void setProducto(String producto);

	/**
	 * Retorna el socio de negocio.
	 * 
	 * @return the socioNeg
	 */
	public String getSocioNeg();

	/**
	 * Setea el socio de negocio.
	 * 
	 * @param socioNeg
	 *            the socioNeg to set
	 */
	public void setSocioNeg(String socioNeg);

	/**
	 * Retorna el valor de la cuota de encargo de cobranza.
	 * 
	 * @return the valCuotaEncCob
	 */
	public Double getValCuotaEncCob();

	/**
	 * Setea el valor de la cuota de encargo de cobranza.
	 * 
	 * @param valCuotaEncCob
	 *            the valCuotaEncCob to set
	 */
	public void setValCuotaEncCob(Double valCuotaEncCob);
	
	/**
	 * Retorna el c�digo del socio de negocio del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodSocioNeg();
	
	/**
	 * Retorna el c�digo correspondiente a la L�nea de Negocio del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodLinNeg();
	
	/**
	 * Retorna el c�digo correspondiente al plan del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodPlanNeg();
	
	/**
	 * Retorna el c�digo correspondiente al producto del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodProducto();
	
	/**
	 * Setea el c�digo del socio de negocio del servicio de encargo de cobranza.
	 * @param Long codSocioNeg
	 */
	public void setCodSocioNeg(Long codSocioNeg);
	
	/**
	 * Setea el c�digo correspondiente a la L�nea de Negocio del servicio de encargo de cobranza.
	 * @param Long codLinNeg
	 */
	public void setCodLinNeg(Long codLinNeg);
	
	/**
	 * Setea el c�digo correspondiente al plan del servicio de encargo de cobranza.
	 * @param Long codPlanNeg
	 */
	public void setCodPlanNeg(Long codPlanNeg);
	
	/**
	 * Setea el c�digo correspondiente al producto del servicio de encargo de cobranza.
	 * @param Long codProducto
	 */
	public void setCodProducto(Long codProducto);

}