/**
 * $id: Clase[ReporteCargaVentasImpl.java], Autor[ar31059727], Time[28/02/2007-10:03:20]
 */
package com.synapsis.eco.codensa.model.impl;

import java.util.Date;

import com.synapsis.eco.codensa.model.ReporteRecaudo;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * BO ReporteCargaVentasImpl.
 * 
 * @author ar26942422
 */
public class ReporteRecaudoImpl extends SynergiaBusinessObjectImpl implements
		ReporteRecaudo {
	private static final long serialVersionUID = 1L;

	/**
	 * Linea de Negocio
	 */
	private String linNeg;

	/**
	 * Socio de Negocio
	 */
	private String socioNeg;

	/**
	 * Producto
	 */
	private String producto;

	/**
	 * Plan de Negocio
	 */
	private String planNeg;

	/**
	 * Numero de Cuenta
	 */
	private Long numCta;

	/**
	 * Numero de Servicio de Encargo de Cobranza
	 */
	private Long numServicio;

	/**
	 * Numero de Identificador
	 */
	private Long numIdentificador;

	/**
	 * Tipo de Documento del Titular
	 */
	private String tipoDocTit;

	/**
	 * Numero de Documento del Titular
	 */
	private String numDocTit;

	/**
	 * Numero de Factura
	 */
	private Long numFactura;

	/**
	 * Fecha Real de Pago
	 */
	private Date fechaRealPago;

	/**
	 * Fecha de Proceso
	 */
	private Date fechaProceso;

	/**
	 * Numero de Cuotas de Encargo de Cobranza
	 */
	private Long numCuotasEncCob;

	/**
	 * Cantidad de Cuotas Recaudadas
	 */
	private Long cantCuotasRec;

	/**
	 * Valor de la cuota de servicio de Encargo de Cobranza
	 */
	private Double valCuotaEncCob;

	/**
	 * Valor total del recaudo referente a las cuotas
	 */
	private Double valTotRecCuotas;

	/**
	 * Valor de recaudo intereses de mora del Encargo de Cobranza.
	 */
	private Double valRecIntMoraEncCob;

	/**
	 * Valor de impuestos recaudado del Encargo de Cobranza
	 */
	private Double valImpRecEncCob;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#getCantCuotasRec()
	 */
	public Long getCantCuotasRec() {
		return cantCuotasRec;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#setCantCuotasRec(java.lang.Long)
	 */
	public void setCantCuotasRec(Long cantCuotasRec) {
		this.cantCuotasRec = cantCuotasRec;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#getFechaProceso()
	 */
	public Date getFechaProceso() {
		return fechaProceso;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#setFechaProceso(java.util.Date)
	 */
	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#getFechaRealPago()
	 */
	public Date getFechaRealPago() {
		return fechaRealPago;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#setFechaRealPago(java.util.Date)
	 */
	public void setFechaRealPago(Date fechaRealPago) {
		this.fechaRealPago = fechaRealPago;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#getLinNeg()
	 */
	public String getLinNeg() {
		return linNeg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#setLinNeg(java.lang.String)
	 */
	public void setLinNeg(String linNeg) {
		this.linNeg = linNeg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#getNumCta()
	 */
	public Long getNumCta() {
		return numCta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#setNumCta(java.lang.Long)
	 */
	public void setNumCta(Long numCta) {
		this.numCta = numCta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#getNumCuotasEncCob()
	 */
	public Long getNumCuotasEncCob() {
		return numCuotasEncCob;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#setNumCuotasEncCob(java.lang.Long)
	 */
	public void setNumCuotasEncCob(Long numCuotasEncCob) {
		this.numCuotasEncCob = numCuotasEncCob;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#getNumDocTit()
	 */
	public String getNumDocTit() {
		return numDocTit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#setNumDocTit(java.lang.String)
	 */
	public void setNumDocTit(String numDocTit) {
		this.numDocTit = numDocTit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#getNumIdentificador()
	 */
	public Long getNumIdentificador() {
		return numIdentificador;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#setNumIdentificador(java.lang.Long)
	 */
	public void setNumIdentificador(Long numIdentificador) {
		this.numIdentificador = numIdentificador;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#getNumServicio()
	 */
	public Long getNumServicio() {
		return numServicio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#setNumServicio(java.lang.Long)
	 */
	public void setNumServicio(Long numServicio) {
		this.numServicio = numServicio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#getPlanNeg()
	 */
	public String getPlanNeg() {
		return planNeg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#setPlanNeg(java.lang.String)
	 */
	public void setPlanNeg(String planNeg) {
		this.planNeg = planNeg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#getProducto()
	 */
	public String getProducto() {
		return producto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#setProducto(java.lang.String)
	 */
	public void setProducto(String producto) {
		this.producto = producto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#getSocioNeg()
	 */
	public String getSocioNeg() {
		return socioNeg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#setSocioNeg(java.lang.String)
	 */
	public void setSocioNeg(String socioNeg) {
		this.socioNeg = socioNeg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#getTipoDocTit()
	 */
	public String getTipoDocTit() {
		return tipoDocTit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#setTipoDocTit(java.lang.String)
	 */
	public void setTipoDocTit(String tipoDocTit) {
		this.tipoDocTit = tipoDocTit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#getValCuotaEncCob()
	 */
	public Double getValCuotaEncCob() {
		return valCuotaEncCob;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#setValCuotaEncCob(java.lang.Double)
	 */
	public void setValCuotaEncCob(Double valCuotaEncCob) {
		this.valCuotaEncCob = valCuotaEncCob;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#getValImpRecEncCob()
	 */
	public Double getValImpRecEncCob() {
		return valImpRecEncCob;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#setValImpRecEncCob(java.lang.Double)
	 */
	public void setValImpRecEncCob(Double valImpRecEncCob) {
		this.valImpRecEncCob = valImpRecEncCob;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#getValRecIntMoraEncCob()
	 */
	public Double getValRecIntMoraEncCob() {
		return valRecIntMoraEncCob;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#setValRecIntMoraEncCob(java.lang.Double)
	 */
	public void setValRecIntMoraEncCob(Double valRecIntMoraEncCob) {
		this.valRecIntMoraEncCob = valRecIntMoraEncCob;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#getValTotRecCuotas()
	 */
	public Double getValTotRecCuotas() {
		return valTotRecCuotas;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteRecaudo#setValTotRecCuotas(java.lang.Double)
	 */
	public void setValTotRecCuotas(Double valTotRecCuotas) {
		this.valTotRecCuotas = valTotRecCuotas;
	}

	/**
	 * @return the numFactura
	 */
	public Long getNumFactura() {
		return numFactura;
	}

	/**
	 * @param numFactura
	 *            the numFactura to set
	 */
	public void setNumFactura(Long numFactura) {
		this.numFactura = numFactura;
	}
	
	/**
	 * C�digo del socio de negocio del servicio de encargo de cobranza.
	 */
	public Long codSocioNeg;
	
	/**
	 * C�digo correspondiente a la L�nea de Negocio del servicio de encargo de cobranza.
	 */
	public Long codLinNeg;
	
	/**
	 * C�digo correspondiente al plan del servicio de encargo de cobranza.
	 */
	public Long codPlanNeg;
	
	/**
	 * Retorna el c�digo correspondiente al producto del servicio de encargo de cobranza.
	 */
	public Long codProducto;

	/* (non-Javadoc)
	 * @see com.synapsis.eco.codensa.model.impl.dummyintefaz#getCodLinNeg()
	 */
	public Long getCodLinNeg() {
		return codLinNeg;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.eco.codensa.model.impl.dummyintefaz#setCodLinNeg(java.lang.Long)
	 */
	public void setCodLinNeg(Long codLinNeg) {
		this.codLinNeg = codLinNeg;
	}
	
	/* (non-Javadoc)
	 * @see com.synapsis.eco.codensa.model.impl.dummyintefaz#getCodPlanNeg()
	 */
	public Long getCodPlanNeg() {
		return codPlanNeg;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.eco.codensa.model.impl.dummyintefaz#setCodPlanNeg(java.lang.Long)
	 */
	public void setCodPlanNeg(Long codPlanNeg) {
		this.codPlanNeg = codPlanNeg;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.eco.codensa.model.impl.dummyintefaz#getCodProducto()
	 */
	public Long getCodProducto() {
		return codProducto;
	}
	
	/* (non-Javadoc)
	 * @see com.synapsis.eco.codensa.model.impl.dummyintefaz#setCodProducto(java.lang.Long)
	 */
	public void setCodProducto(Long codProducto) {
		this.codProducto = codProducto;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.eco.codensa.model.impl.dummyintefaz#getCodSocioNeg()
	 */
	public Long getCodSocioNeg() {
		return codSocioNeg;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.eco.codensa.model.impl.dummyintefaz#setCodSocioNeg(java.lang.Long)
	 */
	public void setCodSocioNeg(Long codSocioNeg) {
		this.codSocioNeg = codSocioNeg;
	}
	
	/**
	 * C�digo de Tipo de Documento del Titular
	 */
	public Long codTipoDocTit;

	/* (non-Javadoc)
	 * @see com.synapsis.eco.codensa.model.impl.dummyintefaz#getCodTipoDocTit()
	 */
	public Long getCodTipoDocTit() {
		return codTipoDocTit;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.eco.codensa.model.impl.dummyintefaz#setCodTipoDocTit(java.lang.Long)
	 */
	public void setCodTipoDocTit(Long codTipoDocTit) {
		this.codTipoDocTit = codTipoDocTit;
	}
}