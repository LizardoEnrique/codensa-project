/**
 * $id: Clase[ReporteCargaVentasBB.java], Autor[ar31059727], Time[28/02/2007-10:11:57]
 */
package com.synapsis.eco.codensa.web;

import java.util.Date;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.model.SelectItem;

import org.apache.myfaces.custom.service.UIService;

import com.suivant.arquitectura.presentation.utils.ComboFactory;
import com.synapsis.nuc.codensa.utils.DateConverterUtils;

/**
 * Backing Bean para la vista de CUECO025: Reporte de Carga de Ventas.
 * 
 * @author ar31059727
 * @author dbraccio - assert solutions
 */
public class ReporteCargaVentasBB implements BackingBeansConstants {

	private static final long serialVersionUID = 1L;

	// List de los combos
	private SelectItem[] comboLineaNegocios;
	private SelectItem[] comboSocioNegocios;
	private SelectItem[] comboProducto;
	private SelectItem[] comboPlan;
	private SelectItem[] comboTipoDocumento;

	// filtros para realizar las busquedas
	private Date fechaInicio;
	private Date fechaInicio2;
	private Long codLinNeg;
	private Long codSocio;
	private Long codClase;
	private Long codPlan;
	private Long numCuenta;
	private String codTipoDocTit;
	private String numDocTit;
	private Date fechaInicioFormattedHasta;


	// componente que realiza la busqueda
	private UIService finderService;
	private HtmlDataTable tableResults;

	/* Realiza la busqueda */
	public void search() {
		getFinderService().execute(true);
		getTableResults().setFirst(0);
	}

	/**
	 * Getters de los combos ya cargados
	 */
	public SelectItem[] getComboLineaNegocios() {
		if (comboLineaNegocios == null) {
			comboLineaNegocios = ComboFactory.makeCombo(
					SRV_COMBO_LINEA_NEGOCIOS, EMPTY_QUERY_FILTER, DESCRIPCION,
					CODIGO, null, new Long(-1));
		}
		return comboLineaNegocios;
	}

	public SelectItem[] getComboSocioNegocios() {
		if (comboSocioNegocios == null) {
			comboSocioNegocios = ComboFactory.makeCombo(
					SRV_COMBO_SOCIO_NEGOCIOS, EMPTY_QUERY_FILTER, DESCRIPCION,
					CODIGO, null, new Long(-1));
		}
		return comboSocioNegocios;
	}

	public SelectItem[] getComboProducto() {
		if (comboProducto == null) {
			comboProducto = ComboFactory
					.makeCombo(SRV_COMBO_PRODUCTO, EMPTY_QUERY_FILTER,
							DESCRIPCION, CODIGO, null, new Long(-1));
		}
		return comboProducto;
	}

	public SelectItem[] getComboPlan() {
		if (comboPlan == null) {
			comboPlan = ComboFactory
					.makeCombo(SRV_COMBO_PLAN, EMPTY_QUERY_FILTER, DESCRIPCION,
							CODIGO, null, new Long(-1));
		}
		return comboPlan;
	}

	public SelectItem[] getComboTipoDocumento() {
		if (comboTipoDocumento == null) {
			comboTipoDocumento = ComboFactory.makeCombo(
					SRV_COMBO_TIPO_DOCUMENTO, EMPTY_QUERY_FILTER, DESCRIPCION,
					CODIGO, null, "");
		}
		return comboTipoDocumento;
	}

	/* Setters para los combos para que sean persistidos */
	public void setComboLineaNegocios(SelectItem[] comboLineaNegocios) {
		this.comboLineaNegocios = comboLineaNegocios;
	}

	public void setComboPlan(SelectItem[] comboPlan) {
		this.comboPlan = comboPlan;
	}

	public void setComboProducto(SelectItem[] comboProducto) {
		this.comboProducto = comboProducto;
	}

	public void setComboSocioNegocios(SelectItem[] comboSocioNegocios) {
		this.comboSocioNegocios = comboSocioNegocios;
	}

	public void setComboTipoDocumento(SelectItem[] comboTipoDocumento) {
		this.comboTipoDocumento = comboTipoDocumento;
	}

	// *****************************************************
	// getter and setter para los parametros del filtro
	// ****************************************************
	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaInicio2() {
		return fechaInicio2;
	}

	public void setFechaInicio2(Date fechaInicio2) {
		this.fechaInicio2 = fechaInicio2;
	}

	public Long getCodLinNeg() {
		return codLinNeg;
	}

	public void setCodLinNeg(Long codLinNeg) {
		this.codLinNeg = codLinNeg;
	}

	public Long getCodSocio() {
		return codSocio;
	}

	public void setCodSocio(Long codSocio) {
		this.codSocio = codSocio;
	}

	public Long getCodClase() {
		return codClase;
	}

	public void setCodClase(Long codClase) {
		this.codClase = codClase;
	}

	public Long getCodPlan() {
		return codPlan;
	}

	public void setCodPlan(Long codPlan) {
		this.codPlan = codPlan;
	}

	public Long getNumCuenta() {
		return numCuenta;
	}

	public void setNumCuenta(Long numCuenta) {
		this.numCuenta = numCuenta;
	}

	public String getCodTipoDocTit() {
		return codTipoDocTit;
	}

	public void setCodTipoDocTit(String codTipoDocTit) {
		this.codTipoDocTit = codTipoDocTit;
	}

	public String getNumDocTit() {
		return numDocTit;
	}

	public void setNumDocTit(String numDocTit) {
		this.numDocTit = numDocTit;
	}

	public UIService getFinderService() {
		return finderService;
	}

	public void setFinderService(UIService finderService) {
		this.finderService = finderService;
	}

	public HtmlDataTable getTableResults() {
		return tableResults;
	}

	public void setTableResults(HtmlDataTable tableResults) {
		this.tableResults = tableResults;
	}

	public Date getFechaInicioFormattedHasta() {
		return DateConverterUtils.getDateFormatddmmyyyy(fechaInicio2);
	}

	public void setFechaInicioFormattedHasta(Date fechaInicioFormattedHasta) {
		this.fechaInicioFormattedHasta = fechaInicioFormattedHasta;
	}

}