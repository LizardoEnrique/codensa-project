/**
 * $id: Clase[ReporteCargaVentasImpl.java], Autor[ar31059727], Time[28/02/2007-10:03:20]
 */
package com.synapsis.eco.codensa.model.impl;

import java.util.Date;

import com.synapsis.eco.codensa.model.ReporteCarteraMorosos;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * BO ReporteCargaVentasImpl.
 * 
 * @author ar26942422
 */
public class ReporteCarteraMorososImpl extends SynergiaBusinessObjectImpl
		implements ReporteCarteraMorosos {
	private static final long serialVersionUID = 1L;

	/**
	 * Linea de Negocio
	 */
	private String linNeg;

	/**
	 * Socio de Negocio
	 */
	private String socioNeg;

	/**
	 * Producto
	 */
	private String producto;

	/**
	 * Plan de Negocio
	 */
	private String planNeg;

	/**
	 * Numero de Cuenta
	 */
	private Long numCta;

	/**
	 * Numero de Servicio
	 */
	private Long numServicio;

	/**
	 * Numero de Identificador
	 */
	private Long numIdentificador;

	/**
	 * Tipo de Documento del Titular
	 */
	private String tipoDocTit;

	/**
	 * Numero de Documento del Titular
	 */
	private String numDocTit;

	/**
	 * Fecha del Cargue
	 */
	private Date fechaCargue;

	/**
	 * Valor de Encargo de Cobranza
	 */
	private Double valEncCob;

	/**
	 * Numero de Cuotas de Encargo de Cobranza
	 */
	private Long numCuotasEncCob;

	/**
	 * Numero de Cuotas Facturadas
	 */
	private Long numCuotasFact;

	/**
	 * Numero de Cuotas Facturadas No Pagadas en Plazo
	 */
	private Long numCuotasFactNpPlazo;

	/**
	 * Numero de Cuotas Facturadas No Pagadas en Mora
	 */
	private Long numCuotasFactNpMora;

	/**
	 * Numero de Cuotas Pagadas
	 */
	private Long numCuotasPg;

	/**
	 * Valor de Cuotas de Encargo de Cobranza
	 */
	private Double valCuotaEncCob;

	/**
	 * Valor de Cuotas Facturadas
	 */
	private Double valCuotaFact;

	/**
	 * Valor de Cuotas Facturadas No Pagadas En Plazo
	 */
	private Double valCuotaFactNpPlazo;

	/**
	 * Valor de Cuotas Facturadas No Pagadas En Mora
	 */
	private Double valCuotaFactNpMora;

	/**
	 * Valor de cuotas Pagadas
	 */
	private Double valCuotaPg;

	/**
	 * Valor de intereses de mora facturados
	 */
	private Double valIntMoraFact;

	/**
	 * Valor de intereses de mora facturados no pagados en plazo
	 */
	private Double valIntMoraFactNpPlazo;

	/**
	 * Valor de intereses de mora facturados no pagados en mora.
	 */
	private Double valIntMoraFactNpMora;

	/**
	 * Valor de intereses de mora pagados
	 */
	private Double valIntMoraPg;

	/**
	 * Valor de impuestos facturados
	 */
	private Double valImpFact;

	/**
	 * Valor de impuestos facturados no pagados en plazo
	 */
	private Double valImpFactNpPlazo;

	/**
	 * Valor de impuestos facturados no pagados en mora
	 */
	private Double valImpFactNpMora;

	/**
	 * Valor de impuestos pagados.
	 */
	private Double valImpPg;

	/**
	 * Dias de Atraso
	 */
	private Long diasAtraso;

	/**
	 * Estrato de Servicio Electrico
	 */
	private String estratoSe;

	/**
	 * Clase de Servicio Electrico
	 */
	private String claseSe;

	/**
	 * Subclase de Servicio Electrico
	 */
	private String subclaseSe;

	/**
	 * Sucursal de Ruta de Facturacion
	 */
	private String sucursalRf;

	/**
	 * Zona de Ruta de Facturacion
	 */
	private String zonaRf;

	/**
	 * Ciclo de Ruta de Facturacion
	 */
	private String cicloRf;

	/**
	 * Grupo de Ruta de Facturacion
	 */
	private String grupoRf;

	/**
	 * Retorna el ciclo de la ruta de facturaci�n
	 * 
	 * @return String cicloRf
	 */
	public String getCicloRf() {
		return cicloRf;
	}

	/**
	 * Setea el valor al ciclo del a ruta de facturaci�n
	 * 
	 * @param cicloRf
	 *            String
	 */
	public void setCicloRf(String cicloRf) {
		this.cicloRf = cicloRf;
	}

	/**
	 * Retorna la clase de servicio del servicio el�ctrico
	 * 
	 * @return String claseSe
	 */
	public String getClaseSe() {
		return claseSe;
	}

	/**
	 * Setea la clase de servicio del servicio el�ctrico
	 * 
	 * @param claseSe
	 *            String
	 */
	public void setClaseSe(String claseSe) {
		this.claseSe = claseSe;
	}

	/**
	 * Retorna la cantidad de d�as de atraso, la misma surge de la resta de la
	 * fecha actual con la fecha de la cuota m�s antigua no pagada.
	 * 
	 * @return Long diasAtraso
	 */
	public Long getDiasAtraso() {
		return diasAtraso;
	}

	/**
	 * Setea la cantidad de d�as de atraso en el pago de las cuotas.
	 * 
	 * @param diasAtraso
	 *            Long
	 */
	public void setDiasAtraso(Long diasAtraso) {
		this.diasAtraso = diasAtraso;
	}

	/**
	 * Retorna el estrato de servicio el�ctrico
	 * 
	 * @return String estratoSe
	 */
	public String getEstratoSe() {
		return estratoSe;
	}

	/**
	 * Setea el estrato de servicio el�ctrico
	 * 
	 * @param estratoSe
	 *            String
	 */
	public void setEstratoSe(String estratoSe) {
		this.estratoSe = estratoSe;
	}

	/**
	 * Retorna la fecha del cargue.
	 * 
	 * @return Date fechaCargue
	 */
	public Date getFechaCargue() {
		return fechaCargue;
	}

	/**
	 * Setea la fecha del cargue.
	 * 
	 * @param fechaCargue
	 *            Date
	 */
	public void setFechaCargue(Date fechaCargue) {
		this.fechaCargue = fechaCargue;
	}

	/**
	 * Retorna el grupo de ruta de facturaci�n.
	 * 
	 * @return String grupoRf
	 */
	public String getGrupoRf() {
		return grupoRf;
	}

	/**
	 * Setea el grupo de ruta de facturaci�n.
	 * 
	 * @param grupoRf
	 *            String
	 */
	public void setGrupoRf(String grupoRf) {
		this.grupoRf = grupoRf;
	}

	/**
	 * Retorna la L�nea de Negocio del servicio de encargo de cobranza.
	 * 
	 * @return String linNeg
	 */

	public String getLinNeg() {
		return linNeg;
	}

	/**
	 * Setea la L�nea de Negocio del servicio de encargo de cobranza.
	 * 
	 * @param linNeg
	 *            String
	 */
	public void setLinNeg(String linNeg) {
		this.linNeg = linNeg;
	}

	/**
	 * Retorna el Numero de Cuenta del servicio de encargo de cobranza..
	 * 
	 * @return Long numCta
	 */
	public Long getNumCta() {
		return numCta;
	}

	/**
	 * Setea el Numero de Cuenta del servicio de encargo de cobranza.
	 * 
	 * @param numCta
	 *            Long
	 */
	public void setNumCta(Long numCta) {
		this.numCta = numCta;
	}

	/**
	 * Retorna el Numero de Cuotas del encargo de cobranza.
	 * 
	 * @return Long
	 */
	public Long getNumCuotasEncCob() {
		return numCuotasEncCob;
	}

	/**
	 * Setea el Numero de Cuotas del encargo de cobranza.
	 * 
	 * @param numCuotasEncCob
	 *            Long
	 */
	public void setNumCuotasEncCob(Long numCuotasEncCob) {
		this.numCuotasEncCob = numCuotasEncCob;
	}

	/**
	 * Retorna el numero de cuotas facturadas.
	 * 
	 * @return Long numCuotasFact
	 */
	public Long getNumCuotasFact() {
		return numCuotasFact;
	}

	/**
	 * Setea el numero de cuotas facturadas.
	 * 
	 * @param numCuotasFact
	 *            Long
	 */
	public void setNumCuotasFact(Long numCuotasFact) {
		this.numCuotasFact = numCuotasFact;
	}

	/**
	 * Retorna el numero de cuotas facturadas no pagadas en mora.
	 * 
	 * @return Long numCuotasFactNpMora
	 */
	public Long getNumCuotasFactNpMora() {
		return numCuotasFactNpMora;
	}

	/**
	 * Setea el numero de cuotas facturadas no pagadas en mora.
	 * 
	 * @param numCuotasFactNpMora
	 *            Long
	 */
	public void setNumCuotasFactNpMora(Long numCuotasFactNpMora) {
		this.numCuotasFactNpMora = numCuotasFactNpMora;
	}

	/**
	 * Retorna el numero de cuotas facturadas no pagadas en plazo.
	 * 
	 * @return Long numCuotasFactNpPlazo
	 */
	public Long getNumCuotasFactNpPlazo() {
		return numCuotasFactNpPlazo;
	}

	/**
	 * Setea el numero de cuotas facturadas no pagadas en plazo.
	 * 
	 * @param numCuotasFactNpPlazo
	 *            Long
	 */
	public void setNumCuotasFactNpPlazo(Long numCuotasFactNpPlazo) {
		this.numCuotasFactNpPlazo = numCuotasFactNpPlazo;
	}

	/**
	 * Retorna el numero de cuotas pagadas.
	 * 
	 * @return Long numCuotasPg
	 */
	public Long getNumCuotasPg() {
		return numCuotasPg;
	}

	/**
	 * Setea el numero de cuotas pagadas.
	 * 
	 * @param numCuotasPg
	 *            Long
	 */
	public void setNumCuotasPg(Long numCuotasPg) {
		this.numCuotasPg = numCuotasPg;
	}

	/**
	 * Retorna el numero de documento del titular.
	 * 
	 * @return String numDocTit
	 */
	public String getNumDocTit() {
		return numDocTit;
	}

	/**
	 * Setea el numero de documento del titular.
	 * 
	 * @param numDocTit
	 *            String
	 */
	public void setNumDocTit(String numDocTit) {
		this.numDocTit = numDocTit;
	}

	/**
	 * Retorna el numero de identificador.
	 * 
	 * @return Long numIdentificador
	 */
	public Long getNumIdentificador() {
		return numIdentificador;
	}

	/**
	 * Setea el numero de identificador.
	 * 
	 * @param numIdentificador
	 *            Long
	 */
	public void setNumIdentificador(Long numIdentificador) {
		this.numIdentificador = numIdentificador;
	}

	/**
	 * Retorna el numero de servicio.
	 * 
	 * @return Long numServicio
	 */
	public Long getNumServicio() {
		return numServicio;
	}

	/**
	 * Setea el numero de servicio.
	 * 
	 * @param numServicio
	 *            Long
	 */
	public void setNumServicio(Long numServicio) {
		this.numServicio = numServicio;
	}

	/**
	 * Retorna el plan de negocio del servicio de encargo de cobranza.
	 * 
	 * @return String planNeg
	 */
	public String getPlanNeg() {
		return planNeg;
	}

	/**
	 * Setea el plan de negocio del servicio de encargo de cobranza.
	 * 
	 * @param planNeg
	 *            String
	 */
	public void setPlanNeg(String planNeg) {
		this.planNeg = planNeg;
	}

	/**
	 * Retorna el producto del servicio de encargo de cobranza.
	 * 
	 * @return String producto
	 */
	public String getProducto() {
		return producto;
	}

	/**
	 * Setea el producto del servicio de encargo de cobranza.
	 * 
	 * @param producto
	 *            String
	 */
	public void setProducto(String producto) {
		this.producto = producto;
	}

	/**
	 * Retorna el socio de negocio del servicio de encargo de cobranza.
	 * 
	 * @return String socioNeg
	 */
	public String getSocioNeg() {
		return socioNeg;
	}

	/**
	 * Setea el socio de negocio del servicio de encargo de cobranza.
	 * 
	 * @param socioNeg
	 *            String
	 */
	public void setSocioNeg(String socioNeg) {
		this.socioNeg = socioNeg;
	}

	/**
	 * Retorna la subclase de servicio del servicio el�ctrico.
	 * 
	 * @return String subclaseSe
	 */
	public String getSubclaseSe() {
		return subclaseSe;
	}

	/**
	 * Setea la subclase de servicio del servicio el�ctrico.
	 * 
	 * @param subclaseSe
	 *            String
	 */
	public void setSubclaseSe(String subclaseSe) {
		this.subclaseSe = subclaseSe;
	}

	/**
	 * Retorna la sucursal de ruta de facturacion.
	 * 
	 * @return String sucursalRf
	 */
	public String getSucursalRf() {
		return sucursalRf;
	}

	/**
	 * Setea la sucursal de ruta de facturacion.
	 * 
	 * @param sucursalRf
	 *            String
	 */
	public void setSucursalRf(String sucursalRf) {
		this.sucursalRf = sucursalRf;
	}

	/**
	 * Retorna el tipo de documento del titular del servicio de encargo de
	 * cobranza.
	 * 
	 * @return String tipoDocTit
	 */
	public String getTipoDocTit() {
		return tipoDocTit;
	}

	/**
	 * Setea el tipo de documento del titular del servicio de encargo de
	 * cobranza.
	 * 
	 * @param tipoDocTit
	 *            String
	 */
	public void setTipoDocTit(String tipoDocTit) {
		this.tipoDocTit = tipoDocTit;
	}

	/**
	 * Retorna el valor de la cuota de encargo de cobranza.
	 * 
	 * @return Double valCuotaEncCob
	 */
	public Double getValCuotaEncCob() {
		return valCuotaEncCob;
	}

	/**
	 * Setea el valor de la cuota de encargo de cobranza.
	 * 
	 * @param valCuotaEncCob
	 *            Double
	 */
	public void setValCuotaEncCob(Double valCuotaEncCob) {
		this.valCuotaEncCob = valCuotaEncCob;
	}

	/**
	 * Retorna el valor de las cuotas facturadas.
	 * 
	 * @return Double valCuotaFact
	 */
	public Double getValCuotaFact() {
		return valCuotaFact;
	}

	/**
	 * Setea el valor de las cuotas facturadas.
	 * 
	 * @param valCuotaFact
	 *            Double
	 */
	public void setValCuotaFact(Double valCuotaFact) {
		this.valCuotaFact = valCuotaFact;
	}

	/**
	 * Retorna el valor de las cuotas facturadas no pagadas en mora.
	 * 
	 * @return Double valCuotaFactNpMora
	 */
	public Double getValCuotaFactNpMora() {
		return valCuotaFactNpMora;
	}

	/**
	 * Setea el valor de las cuotas facturadas no pagadas en mora.
	 * 
	 * @param valCuotaFactNpMora
	 *            Double
	 */
	public void setValCuotaFactNpMora(Double valCuotaFactNpMora) {
		this.valCuotaFactNpMora = valCuotaFactNpMora;
	}

	/**
	 * Retorna el valor de las cuotas facturadas no pagadas en plazo.
	 * 
	 * @return Double valCuotaFactNpPlazo
	 */
	public Double getValCuotaFactNpPlazo() {
		return valCuotaFactNpPlazo;
	}

	/**
	 * Setea el valor de las cuotas facturadas no pagadas en plazo.
	 * 
	 * @param valCuotaFactNpPlazo
	 *            Double
	 */
	public void setValCuotaFactNpPlazo(Double valCuotaFactNpPlazo) {
		this.valCuotaFactNpPlazo = valCuotaFactNpPlazo;
	}

	/**
	 * Retorna el valor de las cuotas pagadas.
	 * 
	 * @return Double valCuotaPg
	 */
	public Double getValCuotaPg() {
		return valCuotaPg;
	}

	/**
	 * Setea el valor de las cuotas pagadas.
	 * 
	 * @param valCuotaPg
	 *            Double
	 */
	public void setValCuotaPg(Double valCuotaPg) {
		this.valCuotaPg = valCuotaPg;
	}

	/**
	 * Retorna el valor del encargo de cobranza.
	 * 
	 * @return Double valEncCob
	 */
	public Double getValEncCob() {
		return valEncCob;
	}

	/**
	 * Setea el valor del encargo de cobranza.
	 * 
	 * @param valEncCob
	 *            Double
	 */
	public void setValEncCob(Double valEncCob) {
		this.valEncCob = valEncCob;
	}

	/**
	 * Retorna el valor de los impuestos facturados.
	 * 
	 * @return Double valImpFact
	 */
	public Double getValImpFact() {
		return valImpFact;
	}

	/**
	 * Setea el valor de los impuestos facturados.
	 * 
	 * @param valImpFact
	 *            Double
	 */
	public void setValImpFact(Double valImpFact) {
		this.valImpFact = valImpFact;
	}

	/**
	 * Retorna el valor de los impuestos facturados no pagados en mora.
	 * 
	 * @return Double valImpFactNpMora
	 */
	public Double getValImpFactNpMora() {
		return valImpFactNpMora;
	}

	/**
	 * Setea el valor de los impuestos facturados no pagados en mora.
	 * 
	 * @param valImpFactNpMora
	 *            Double
	 */
	public void setValImpFactNpMora(Double valImpFactNpMora) {
		this.valImpFactNpMora = valImpFactNpMora;
	}

	/**
	 * Retorna el valor de los impuestos facturados no pagados en plazo.
	 * 
	 * @return Double valImpFactNpPlazo
	 */
	public Double getValImpFactNpPlazo() {
		return valImpFactNpPlazo;
	}

	/**
	 * Setea el valor de los impuestos facturados no pagados en plazo.
	 * 
	 * @param valImpFactNpPlazo
	 *            Double
	 */
	public void setValImpFactNpPlazo(Double valImpFactNpPlazo) {
		this.valImpFactNpPlazo = valImpFactNpPlazo;
	}

	/**
	 * Retorna el valor de los impuestos pagados.
	 * 
	 * @return Double valImpPg
	 */
	public Double getValImpPg() {
		return valImpPg;
	}

	/**
	 * Setea el valor de los impuestos pagados.
	 * 
	 * @param valImpPg
	 *            Double
	 */
	public void setValImpPg(Double valImpPg) {
		this.valImpPg = valImpPg;
	}

	/**
	 * Retorna el valor de los intereses de mora facturados.
	 * 
	 * @return Double valIntMoraFact
	 */
	public Double getValIntMoraFact() {
		return valIntMoraFact;
	}

	/**
	 * Setea el valor de los intereses de mora facturados.
	 * 
	 * @param valIntMoraFact
	 *            Double
	 */
	public void setValIntMoraFact(Double valIntMoraFact) {
		this.valIntMoraFact = valIntMoraFact;
	}

	/**
	 * Retorna el valor de los intereses de mora facturados no pagados en mora.
	 * 
	 * @return Double valIntMoraFactNpMora
	 */
	public Double getValIntMoraFactNpMora() {
		return valIntMoraFactNpMora;
	}

	/**
	 * Setea el valor de los intereses de mora facturados no pagados en mora.
	 * 
	 * @param valIntMoraFactNpMora
	 *            Double
	 */
	public void setValIntMoraFactNpMora(Double valIntMoraFactNpMora) {
		this.valIntMoraFactNpMora = valIntMoraFactNpMora;
	}

	/**
	 * Retorna el valor de los intereses de mora facturados no pagados en plazo.
	 * 
	 * @return Double valIntMoraFactNpPlazo
	 */
	public Double getValIntMoraFactNpPlazo() {
		return valIntMoraFactNpPlazo;
	}

	/**
	 * Setea el valor de los intereses de mora facturados no pagados en plazo.
	 * 
	 * @param valIntMoraFactNpPlazo
	 *            Double
	 */
	public void setValIntMoraFactNpPlazo(Double valIntMoraFactNpPlazo) {
		this.valIntMoraFactNpPlazo = valIntMoraFactNpPlazo;
	}

	/**
	 * Retorna el valor de los intereses de mora pagados.
	 * 
	 * @return Double valIntMoraPg
	 */
	public Double getValIntMoraPg() {
		return valIntMoraPg;
	}

	/**
	 * Setea el valor de los intereses de mora pagados.
	 * 
	 * @param valIntMoraPg
	 *            Double
	 */
	public void setValIntMoraPg(Double valIntMoraPg) {
		this.valIntMoraPg = valIntMoraPg;
	}

	/**
	 * Retorna la zona de la ruta de facturacion.
	 * 
	 * @return String zonaRf
	 */
	public String getZonaRf() {
		return zonaRf;
	}

	/**
	 * Setea la zona de la ruta de facturacion.
	 * 
	 * @param zonaRf
	 *            String
	 */
	public void setZonaRf(String zonaRf) {
		this.zonaRf = zonaRf;
	}

	/**
	 * C�digo del socio de negocio del servicio de encargo de cobranza.
	 */
	public Long codSocioNeg;
	
	/**
	 * C�digo correspondiente a la L�nea de Negocio del servicio de encargo de cobranza.
	 */
	public Long codLinNeg;
	
	/**
	 * C�digo correspondiente al plan del servicio de encargo de cobranza.
	 */
	public Long codPlanNeg;
	
	/**
	 * Retorna el c�digo correspondiente al producto del servicio de encargo de cobranza.
	 */
	public Long codProducto;

	/**
	 * Retorna el c�digo correspondiente a la L�nea de Negocio del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodLinNeg() {
		return codLinNeg;
	}

	/**
	 * Setea el c�digo de l�nea de negocio del servicio de encargo de cobranza
	 * 
	 * @param codLineaNeg Long
	 */
	public void setCodLinNeg(Long codLinNeg) {
		this.codLinNeg = codLinNeg;
	}
	
	/**
	 * Retorna el c�digo correspondiente al plan del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodPlanNeg() {
		return codPlanNeg;
	}

	/**
	 * Setea el c�digo de plan de negocio del servicio de encargo de cobranza
	 * 
	 * @param codPlanNeg Long
	 */
	public void setCodPlanNeg(Long codPlanNeg) {
		this.codPlanNeg = codPlanNeg;
	}

	/**
	 * Retorna el c�digo correspondiente al producto del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodProducto() {
		return codProducto;
	}
	
	/**
	 * Setea el c�digo de producto del servicio de encargo de cobranza
	 * 
	 * @param codProducto Long
	 */
	public void setCodProducto(Long codProducto) {
		this.codProducto = codProducto;
	}

	/**
	 * Retorna el c�digo del socio de negocio del servicio de encargo de cobranza.
	 * @return Long
	 */
	public Long getCodSocioNeg() {
		return codSocioNeg;
	}

	/**
	 * Setea el c�digo de socio de negocio del servicio de encargo de cobranza
	 * 
	 * @param codSocioNeg Long
	 */
	public void setCodSocioNeg(Long codSocioNeg) {
		this.codSocioNeg = codSocioNeg;
	}
}