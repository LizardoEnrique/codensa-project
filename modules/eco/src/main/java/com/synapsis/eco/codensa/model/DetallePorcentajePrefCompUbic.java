package com.synapsis.eco.codensa.model;

public interface DetallePorcentajePrefCompUbic {

	public String getCargo();

	public void setCargo(String cargo);

	public String getImpuesto();

	public void setImpuesto(String impuesto);

	public Long getPorcentaje();

	public void setPorcentaje(Long porcentaje);

	public Long getIdServicio();

	public void setIdServicio(Long idServicio);

}