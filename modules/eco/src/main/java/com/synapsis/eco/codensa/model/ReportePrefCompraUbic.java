package com.synapsis.eco.codensa.model;

import java.util.Date;

public interface ReportePrefCompraUbic {

	/**
	 * @return the cicloRf
	 */
	public String getCicloRf();

	/**
	 * @param cicloRf the cicloRf to set
	 */
	public void setCicloRf(String cicloRf);

	/**
	 * @return the estadoEnc
	 */
	public String getEstadoEnc();

	/**
	 * @param estadoEnc the estadoEnc to set
	 */
	public void setEstadoEnc(String estadoEnc);

	/**
	 * @return the estratoSe
	 */
	public String getEstratoSe();

	/**
	 * @param estratoSe the estratoSe to set
	 */
	public void setEstratoSe(String estratoSe);

	/**
	 * @return the fechaCargue
	 */
	public Date getFechaCargue();

	/**
	 * @param fechaCargue the fechaCargue to set
	 */
	public void setFechaCargue(Date fechaCargue);

	/**
	 * @return the fecUltFac
	 */
	public Date getFecUltFac();

	/**
	 * @param fecUltFac the fecUltFac to set
	 */
	public void setFecUltFac(Date fecUltFac);

	/**
	 * @return the grupoRf
	 */
	public String getGrupoRf();

	/**
	 * @param grupoRf the grupoRf to set
	 */
	public void setGrupoRf(String grupoRf);

	/**
	 * @return the linNeg
	 */
	public String getLinNeg();

	/**
	 * @param linNeg the linNeg to set
	 */
	public void setLinNeg(String linNeg);

	/**
	 * @return the numCta
	 */
	public Long getNumCta();

	/**
	 * @param numCta the numCta to set
	 */
	public void setNumCta(Long numCta);

	/**
	 * @return the numCuotasEnc
	 */
	public String getNumCuotasEnc();

	/**
	 * @param numCuotasEnc the numCuotasEnc to set
	 */
	public void setNumCuotasEnc(String numCuotasEnc);

	/**
	 * @return the numCuotasFacNoRec
	 */
	public Long getNumCuotasFacNoRec();

	/**
	 * @param numCuotasFacNoRec the numCuotasFacNoRec to set
	 */
	public void setNumCuotasFacNoRec(Long numCuotasFacNoRec);

	/**
	 * @return the numDocTit
	 */
	public String getNumDocTit();

	/**
	 * @param numDocTit the numDocTit to set
	 */
	public void setNumDocTit(String numDocTit);

	/**
	 * @return the numIdentificador
	 */
	public Long getNumIdentificador();

	/**
	 * @param numIdentificador the numIdentificador to set
	 */
	public void setNumIdentificador(Long numIdentificador);

	/**
	 * @return the numServicio
	 */
	public Long getNumServicio();

	/**
	 * @param numServicio the numServicio to set
	 */
	public void setNumServicio(Long numServicio);

	/**
	 * @return the planNeg
	 */
	public String getPlanNeg();

	/**
	 * @param planNeg the planNeg to set
	 */
	public void setPlanNeg(String planNeg);

	/**
	 * @return the producto
	 */
	public String getProducto();

	/**
	 * @param producto the producto to set
	 */
	public void setProducto(String producto);

	/**
	 * @return the socioNeg
	 */
	public String getSocioNeg();

	/**
	 * @param socioNeg the socioNeg to set
	 */
	public void setSocioNeg(String socioNeg);

	/**
	 * @return the sucursalRf
	 */
	public String getSucursalRf();

	/**
	 * @param sucursalRf the sucursalRf to set
	 */
	public void setSucursalRf(String sucursalRf);

	/**
	 * @return the tipoDocTit
	 */
	public String getTipoDocTit();

	/**
	 * @param tipoDocTit the tipoDocTit to set
	 */
	public void setTipoDocTit(String tipoDocTit);

	/**
	 * @return the valCuotaEnc
	 */
	public Double getValCuotaEnc();

	/**
	 * @param valCuotaEnc the valCuotaEnc to set
	 */
	public void setValCuotaEnc(Double valCuotaEnc);

	/**
	 * @return the valTotalEnc
	 */
	public String getValTotalEnc();

	/**
	 * @param valTotalEnc the valTotalEnc to set
	 */
	public void setValTotalEnc(String valTotalEnc);

	/**
	 * @return the zonaRf
	 */
	public String getZonaRf();

	/**
	 * @param zonaRf the zonaRf to set
	 */
	public void setZonaRf(String zonaRf);

	/**
	 * @return Long codLinNeg
	 */
	public Long getCodLinNeg();

	/**
	 * @param codLinNeg Long
	 */
	public void setCodLinNeg(Long codLinNeg);

	/**
	 * @return Long codPlanNeg
	 */
	public Long getCodPlanNeg();

	/**
	 * @param codPlanNeg Long
	 */
	public void setCodPlanNeg(Long codPlanNeg);


	/**
	 * @return Long codProducto
	 */
	public Long getCodProducto();

	/**
	 * @param codProducto Long
	 */
	public void setCodProducto(Long codProducto);


	/**
	 * @return Long codSocioNeg
	 */
	public Long getCodSocioNeg();

	/**
	 * @param codSocioNeg Long
	 */
	public void setCodSocioNeg(Long codSocioNeg);
}