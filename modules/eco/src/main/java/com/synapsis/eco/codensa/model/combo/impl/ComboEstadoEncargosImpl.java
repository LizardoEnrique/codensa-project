package com.synapsis.eco.codensa.model.combo.impl;

import com.synapsis.eco.codensa.model.combo.ComboConCodigoString;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * BO propio del Combo de Estado de Encargos.
 * 
 * @author ar26942422
 */
public class ComboEstadoEncargosImpl extends SynergiaBusinessObjectImpl
		implements ComboConCodigoString {
	private static final long serialVersionUID = 1L;

	private String codigo;

	private String descripcion;

	public ComboEstadoEncargosImpl() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.combo.impl.ComboLineaNegocio#getCodigo()
	 */
	public String getCodigo() {
		return codigo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.combo.impl.ComboLineaNegocio#setCodigo(java.lang.String)
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.combo.impl.ComboLineaNegocio#getDescripcion()
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.combo.impl.ComboLineaNegocio#setDescripcion(java.lang.String)
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}