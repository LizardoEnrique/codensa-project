package com.synapsis.eco.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ReporteRecaudo extends SynergiaBusinessObject{

	/**
	 * @return the cantCuotasRec
	 */
	public Long getCantCuotasRec();

	/**
	 * @param cantCuotasRec the cantCuotasRec to set
	 */
	public void setCantCuotasRec(Long cantCuotasRec);

	/**
	 * @return the fechaProceso
	 */
	public Date getFechaProceso();

	/**
	 * @param fechaProceso the fechaProceso to set
	 */
	public void setFechaProceso(Date fechaProceso);

	/**
	 * @return the fechaRealPago
	 */
	public Date getFechaRealPago();

	/**
	 * @param fechaRealPago the fechaRealPago to set
	 */
	public void setFechaRealPago(Date fechaRealPago);

	/**
	 * @return the linNeg
	 */
	public String getLinNeg();

	/**
	 * @param linNeg the linNeg to set
	 */
	public void setLinNeg(String linNeg);

	/**
	 * @return the numCta
	 */
	public Long getNumCta();

	/**
	 * @param numCta the numCta to set
	 */
	public void setNumCta(Long numCta);

	/**
	 * @return the numCuotasEncCob
	 */
	public Long getNumCuotasEncCob();

	/**
	 * @param numCuotasEncCob the numCuotasEncCob to set
	 */
	public void setNumCuotasEncCob(Long numCuotasEncCob);

	/**
	 * @return the numDocTit
	 */
	public String getNumDocTit();

	/**
	 * @param numDocTit the numDocTit to set
	 */
	public void setNumDocTit(String numDocTit);

	/**
	 * @return the numIdentificador
	 */
	public Long getNumIdentificador();

	/**
	 * @param numIdentificador the numIdentificador to set
	 */
	public void setNumIdentificador(Long numIdentificador);

	/**
	 * @return the numServicio
	 */
	public Long getNumServicio();

	/**
	 * @param numServicio the numServicio to set
	 */
	public void setNumServicio(Long numServicio);

	/**
	 * @return the planNeg
	 */
	public String getPlanNeg();

	/**
	 * @param planNeg the planNeg to set
	 */
	public void setPlanNeg(String planNeg);

	/**
	 * @return the producto
	 */
	public String getProducto();

	/**
	 * @param producto the producto to set
	 */
	public void setProducto(String producto);

	/**
	 * @return the socioNeg
	 */
	public String getSocioNeg();

	/**
	 * @param socioNeg the socioNeg to set
	 */
	public void setSocioNeg(String socioNeg);

	/**
	 * @return the tipoDocTit
	 */
	public String getTipoDocTit();

	/**
	 * @param tipoDocTit the tipoDocTit to set
	 */
	public void setTipoDocTit(String tipoDocTit);

	/**
	 * @return the valCuotaEncCob
	 */
	public Double getValCuotaEncCob();

	/**
	 * @param valCuotaEncCob the valCuotaEncCob to set
	 */
	public void setValCuotaEncCob(Double valCuotaEncCob);

	/**
	 * @return the valImpRecEncCob
	 */
	public Double getValImpRecEncCob();

	/**
	 * @param valImpRecEncCob the valImpRecEncCob to set
	 */
	public void setValImpRecEncCob(Double valImpRecEncCob);

	/**
	 * @return the valRecIntMoraEncCob
	 */
	public Double getValRecIntMoraEncCob();

	/**
	 * @param valRecIntMoraEncCob the valRecIntMoraEncCob to set
	 */
	public void setValRecIntMoraEncCob(Double valRecIntMoraEncCob);

	/**
	 * @return the valTotRecCuotas
	 */
	public Double getValTotRecCuotas();

	/**
	 * @param valTotRecCuotas the valTotRecCuotas to set
	 */
	public void setValTotRecCuotas(Double valTotRecCuotas);

	/**
	 * 
	 * @return numFactura
	 */
	public Long getNumFactura();
	
	/**
	 * @param numFactura
	 */
	public void setNumFactura(Long numFactura);
	
	/**
	 * @return the codLinNeg
	 */
	public Long getCodLinNeg();

	/**
	 * @param codLinNeg
	 */
	public void setCodLinNeg(Long codLinNeg);

	/**
	 * @return the codPlanNeg
	 */
	public Long getCodPlanNeg();

	/**
	 * @param codPlanNeg
	 */
	public void setCodPlanNeg(Long codPlanNeg);

	/**
	 * @return the codProducto
	 */
	public Long getCodProducto();

	/**
	 * @param codProducto
	 */
	public void setCodProducto(Long codProducto);

	/**
	 * @return the codSocioNeg
	 */
	public Long getCodSocioNeg();

	/**
	 * @param codSocioNeg
	 */
	public void setCodSocioNeg(Long codSocioNeg);
	
	/**
	 * @return the codTipoDocTit
	 */
	public Long getCodTipoDocTit();

	/**
	 * @param codTipoDocTit
	 */
	public void setCodTipoDocTit(Long codTipoDocTit);
}