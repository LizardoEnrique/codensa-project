/**
 * $id: Clase[ReporteCargaVentasImpl.java], Autor[ar31059727], Time[28/02/2007-10:03:20]
 */
package com.synapsis.eco.codensa.model.impl;

import java.util.Date;

import com.synapsis.eco.codensa.model.ReporteCargaVentas;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * BO ReporteCargaVentasImpl.
 * 
 * @author ar31059727
 */
public class ReporteCargaVentasImpl extends SynergiaBusinessObjectImpl
		implements ReporteCargaVentas {
	private static final long serialVersionUID = 1L;

	private Date fechaInicio;

	private Long numEco;

	private Long codLinNeg;

	private String linNeg;

	private Long codClase;

	private String clase;

	private Long codPlan;

	private String plan;

	private Long codSocio;

	private String socio;

	private Long numCuenta;

	private Long numIdTit;

	private String nombreTit;

	private Long codTipoDocTit;

	private String tipoDocTit;

	private String numDocTit;

	private String cargoCap;

	private String moneda;

	private Long numCuotas;

	private String estado;

	private Date fecUltCambEst;

	private String motivoFin;

	private Double valCuotaEnc;

	private Double valTotalEncCob;

	private String estratoSe;

	private String claseSe;

	private String subClaseSe;

	private String sucursalRf;

	private String zonaRf;

	private String cicloRf;

	private String grupoRf;

	public ReporteCargaVentasImpl() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getCargoCap()
	 */
	public String getCargoCap() {
		return cargoCap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setCargoCap(java.lang.String)
	 */
	public void setCargoCap(String cargoCap) {
		this.cargoCap = cargoCap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getCicloRf()
	 */
	public String getCicloRf() {
		return cicloRf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setCicloRf(java.lang.String)
	 */
	public void setCicloRf(String cicloRf) {
		this.cicloRf = cicloRf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getClase()
	 */
	public String getClase() {
		return clase;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setClase(java.lang.String)
	 */
	public void setClase(String clase) {
		this.clase = clase;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getClaseSe()
	 */
	public String getClaseSe() {
		return claseSe;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setClaseSe(java.lang.Long)
	 */
	public void setClaseSe(String claseSe) {
		this.claseSe = claseSe;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getCodClase()
	 */
	public Long getCodClase() {
		return codClase;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setCodClase(java.lang.Long)
	 */
	public void setCodClase(Long codClase) {
		this.codClase = codClase;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getCodLinNeg()
	 */
	public Long getCodLinNeg() {
		return codLinNeg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setCodLinNeg(java.lang.Long)
	 */
	public void setCodLinNeg(Long codLinNeg) {
		this.codLinNeg = codLinNeg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getCodPlan()
	 */
	public Long getCodPlan() {
		return codPlan;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setCodPlan(java.lang.Long)
	 */
	public void setCodPlan(Long codPlan) {
		this.codPlan = codPlan;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getCodSocio()
	 */
	public Long getCodSocio() {
		return codSocio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setCodSocio(java.lang.Long)
	 */
	public void setCodSocio(Long codSocio) {
		this.codSocio = codSocio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getCodTipoDocTit()
	 */
	public Long getCodTipoDocTit() {
		return codTipoDocTit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setCodTipoDocTit(java.lang.Long)
	 */
	public void setCodTipoDocTit(Long codTipoDocTit) {
		this.codTipoDocTit = codTipoDocTit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getEstado()
	 */
	public String getEstado() {
		return estado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setEstado(java.lang.String)
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getEstratoSe()
	 */
	public String getEstratoSe() {
		return estratoSe;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setEstratoSe(java.lang.String)
	 */
	public void setEstratoSe(String estratoSe) {
		this.estratoSe = estratoSe;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getFechaInicio()
	 */
	public Date getFechaInicio() {
		return fechaInicio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setFechaInicio(java.util.Date)
	 */
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getFecUltCambEst()
	 */
	public Date getFecUltCambEst() {
		return fecUltCambEst;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setFecUltCambEst(java.util.Date)
	 */
	public void setFecUltCambEst(Date fecUltCambEst) {
		this.fecUltCambEst = fecUltCambEst;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getGrupoRf()
	 */
	public String getGrupoRf() {
		return grupoRf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setGrupoRf(java.lang.String)
	 */
	public void setGrupoRf(String grupoRf) {
		this.grupoRf = grupoRf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getLinNeg()
	 */
	public String getLinNeg() {
		return linNeg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setLinNeg(java.lang.String)
	 */
	public void setLinNeg(String linNeg) {
		this.linNeg = linNeg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getMoneda()
	 */
	public String getMoneda() {
		return moneda;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setMoneda(java.lang.String)
	 */
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getMotivoFin()
	 */
	public String getMotivoFin() {
		return motivoFin;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setMotivoFin(java.lang.String)
	 */
	public void setMotivoFin(String motivoFin) {
		this.motivoFin = motivoFin;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getNombreTit()
	 */
	public String getNombreTit() {
		return nombreTit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setNombreTit(java.lang.String)
	 */
	public void setNombreTit(String nombreTit) {
		this.nombreTit = nombreTit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getNumCuenta()
	 */
	public Long getNumCuenta() {
		return numCuenta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setNumCuenta(java.lang.Long)
	 */
	public void setNumCuenta(Long numCuenta) {
		this.numCuenta = numCuenta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getNumCuotas()
	 */
	public Long getNumCuotas() {
		return numCuotas;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setNumCuotas(java.lang.Long)
	 */
	public void setNumCuotas(Long numCuotas) {
		this.numCuotas = numCuotas;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getNumDocTit()
	 */
	public String getNumDocTit() {
		return numDocTit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setNumDocTit(java.lang.String)
	 */
	public void setNumDocTit(String numDocTit) {
		this.numDocTit = numDocTit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getNumEco()
	 */
	public Long getNumEco() {
		return numEco;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setNumEco(java.lang.Long)
	 */
	public void setNumEco(Long numEco) {
		this.numEco = numEco;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getNumIdTit()
	 */
	public Long getNumIdTit() {
		return numIdTit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setNumIdTit(java.lang.Long)
	 */
	public void setNumIdTit(Long numIdTit) {
		this.numIdTit = numIdTit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getPlan()
	 */
	public String getPlan() {
		return plan;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setPlan(java.lang.String)
	 */
	public void setPlan(String plan) {
		this.plan = plan;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getSocio()
	 */
	public String getSocio() {
		return socio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setSocio(java.lang.String)
	 */
	public void setSocio(String socio) {
		this.socio = socio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getSubClaseSe()
	 */
	public String getSubClaseSe() {
		return subClaseSe;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setSubClaseSe(java.lang.Long)
	 */
	public void setSubClaseSe(String subClaseSe) {
		this.subClaseSe = subClaseSe;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getSucursalRf()
	 */
	public String getSucursalRf() {
		return sucursalRf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setSucursalRf(java.lang.String)
	 */
	public void setSucursalRf(String sucursalRf) {
		this.sucursalRf = sucursalRf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getTipoDocTit()
	 */
	public String getTipoDocTit() {
		return tipoDocTit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setTipoDocTit(java.lang.String)
	 */
	public void setTipoDocTit(String tipoDocTit) {
		this.tipoDocTit = tipoDocTit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getValCuotaEnc()
	 */
	public Double getValCuotaEnc() {
		return valCuotaEnc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setValCuotaEnc(java.lang.Double)
	 */
	public void setValCuotaEnc(Double valCuotaEnc) {
		this.valCuotaEnc = valCuotaEnc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getValTotalEncCob()
	 */
	public Double getValTotalEncCob() {
		return valTotalEncCob;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setValTotalEncCob(java.lang.Double)
	 */
	public void setValTotalEncCob(Double valTotalEncCob) {
		this.valTotalEncCob = valTotalEncCob;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#getZonaRf()
	 */
	public String getZonaRf() {
		return zonaRf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.impl.ReporteCargaVentas#setZonaRf(java.lang.String)
	 */
	public void setZonaRf(String zonaRf) {
		this.zonaRf = zonaRf;
	}
}