package com.synapsis.eco.codensa.web;

import java.util.Date;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.model.SelectItem;

import org.apache.myfaces.custom.service.UIService;

import com.suivant.arquitectura.core.queryFilter.CompositeQueryFilter;
import com.suivant.arquitectura.core.queryFilter.NullQueryFilter;
import com.suivant.arquitectura.core.queryFilter.OrderQuery;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.presentation.utils.ComboFactory;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.synapsis.nuc.codensa.utils.DateConverterUtils;

/**
 * Backing Bean para la vista de CUECO046: Reporte de Traslados.
 * 
 * @author ar26942422
 * @author dbraccio
 */
public class ReporteTrasladosBB implements BackingBeansConstants {

	private static final long serialVersionUID = 1L;

	/* Property para los combos */
	private SelectItem[] comboLineaNegocios;
	private SelectItem[] comboSocioNegocios;
	private SelectItem[] comboProducto;
	private SelectItem[] comboPlan;
	private OrderQuery orderComboLineaNegocio = new OrderQuery();

	// properties de los filtros de entrada
	private Date fechaDesde;
	private Date fechaHasta;
	private Long lineaNegocio;
	private Long socioNegocio;
	private Long tipoProducto;
	private Long plan;

	// esto es un patch
	private Date fechaHastaFormatted;

	// componente que realiza la busqueda
	private UIService finderService;
	private HtmlDataTable tableResults;

	/* Realiza el refresco de la pagina cuando se ejecuta la consulta */
	public void search() {
		getFinderService().execute(true);
		getTableResults().setFirst(0);
	}

	/* Getters de los combos */
	public SelectItem[] getComboLineaNegocios() {
		if (comboLineaNegocios == null) {
			comboLineaNegocios = ComboFactory.makeCombo(
					SRV_COMBO_LINEA_NEGOCIOS, getEmptyQueryFilter(),
					DESCRIPCION, CODIGO, null, new Long(-1));
		}
		return comboLineaNegocios;
	}

	public SelectItem[] getComboSocioNegocios() {
		if (comboSocioNegocios == null) {
			comboSocioNegocios = ComboFactory.makeCombo(
					SRV_COMBO_SOCIO_NEGOCIOS, getEmptyQueryFilter(),
					DESCRIPCION, CODIGO, null, new Long(-1));
		}
		return comboSocioNegocios;
	}

	public SelectItem[] getComboProducto() {
		if (comboProducto == null) {
			comboProducto = ComboFactory.makeCombo(SRV_COMBO_PRODUCTO,
					getEmptyQueryFilter(), DESCRIPCION, CODIGO, null, new Long(
							-1));
		}
		return comboProducto;
	}

	public SelectItem[] getComboPlan() {
		if (comboPlan == null) {
			comboPlan = ComboFactory.makeCombo(SRV_COMBO_PLAN,
					getEmptyQueryFilter(), DESCRIPCION, CODIGO, null, new Long(
							-1));
		}
		return comboPlan;
	}

	private QueryFilter getEmptyQueryFilter() {
		NullQueryFilter nqf = (NullQueryFilter) VariableResolverUtils
				.getObject(EMPTY_QUERY_FILTER);
		orderComboLineaNegocio.setAttributeName("descripcion");
		CompositeQueryFilter cqf = new CompositeQueryFilter();
		cqf.addFilter(nqf);
		cqf.addOrder(orderComboLineaNegocio);
		cqf.setMaxResults(150);
		return cqf;
	}

	/*
	 * Getter and setter
	 */
	public void setComboLineaNegocios(SelectItem[] comboLineaNegocios) {
		this.comboLineaNegocios = comboLineaNegocios;
	}

	public void setComboPlan(SelectItem[] comboPlan) {
		this.comboPlan = comboPlan;
	}

	public void setComboProducto(SelectItem[] comboProducto) {
		this.comboProducto = comboProducto;
	}

	public void setComboSocioNegocios(SelectItem[] comboSocioNegocios) {
		this.comboSocioNegocios = comboSocioNegocios;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public Date getFechaHastaFormatted() {
		return DateConverterUtils.getDateFormatddmmyyyy(fechaHasta);
	}

	public void setFechaHastaFormatted(Date fechaHastaFormatted) {
		this.fechaHastaFormatted = fechaHastaFormatted;
	}

	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Long getLineaNegocio() {
		return lineaNegocio;
	}

	public void setLineaNegocio(Long lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}

	public Long getSocioNegocio() {
		return socioNegocio;
	}

	public void setSocioNegocio(Long socioNegocio) {
		this.socioNegocio = socioNegocio;
	}

	public Long getTipoProducto() {
		return tipoProducto;
	}

	public void setTipoProducto(Long tipoProducto) {
		this.tipoProducto = tipoProducto;
	}

	public Long getPlan() {
		return plan;
	}

	public void setPlan(Long plan) {
		this.plan = plan;
	}

	public UIService getFinderService() {
		return finderService;
	}

	public void setFinderService(UIService finderService) {
		this.finderService = finderService;
	}

	public HtmlDataTable getTableResults() {
		return tableResults;
	}

	public void setTableResults(HtmlDataTable tableResults) {
		this.tableResults = tableResults;
	}

}