/**
 * $id: Clase[ComboLineaNegocio.java], Autor[ar31059727], Time[28/02/2007-11:13:36]
 */
package com.synapsis.eco.codensa.model.combo;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * Interface de todos los combo.
 * 
 * @author ar31059727
 */
public interface Combo extends SynergiaBusinessObject {

	/**
	 * @return the codigo
	 */
	public Long getCodigo();

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo(Long codigo);

	/**
	 * @return the descripcion
	 */
	public String getDescripcion();

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion);

}