/**
 * $id: Clase[ComboLineaNegocio.java], Autor[ar31059727], Time[28/02/2007-11:13:36]
 */
package com.synapsis.eco.codensa.model.combo.impl;

import com.synapsis.eco.codensa.model.combo.Combo;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * BO propio del Combo de Linea de Negocios.
 * 
 * @author ar31059727
 */
public class ComboLineaNegociosImpl extends SynergiaBusinessObjectImpl
		implements Combo {
	private static final long serialVersionUID = 1L;

	private Long codigo;

	private String descripcion;

	public ComboLineaNegociosImpl() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.combo.impl.ComboLineaNegocio#getCodigo()
	 */
	public Long getCodigo() {
		return codigo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.combo.impl.ComboLineaNegocio#setCodigo(java.lang.String)
	 */
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.combo.impl.ComboLineaNegocio#getDescripcion()
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.combo.impl.ComboLineaNegocio#setDescripcion(java.lang.String)
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}