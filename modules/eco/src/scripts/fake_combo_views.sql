-- CUECO025
CREATE OR REPLACE VIEW ECO_LINEA_NEGOCIO_COMBO_V (
  id,
  id_empresa,
  codigo,
  descripcion
) 
AS SELECT
  t01.id_linea_negocio,
  t01.id_empresa,
  t01.codigo,
  t01.descripcion
FROM ECO_LINEA_NEGOCIO t01
WHERE t01.activo = 'S';

CREATE OR REPLACE VIEW ECO_SOCIO_NEGOCIO_COMBO_V (
  id,
  id_empresa,
  codigo,
  descripcion
) 
AS SELECT
  t01.id_empresa_socio,
  t01.id_empresa,
  t01.code,
  t01.description
FROM eco_empresa_socio t01
WHERE t01.active = 'S';

CREATE OR REPLACE VIEW ECO_PRODUCTO_COMBO_V (
  id,
  id_empresa,
  codigo,
  descripcion
) 
AS SELECT
  t01.id_clases_eco,
  t01.id_empresa,
  t01.code,
  t01.description
FROM eco_clases_eco t01
WHERE t01.active = 'S';

CREATE OR REPLACE VIEW ECO_PLAN_COMBO_V (
  id,
  id_empresa,
  codigo,
  descripcion
) 
AS SELECT
  t01.id_plan_neg,
  t01.id_empresa,
  t01.codigo,
  t01.descripcion
FROM eco_plan_neg t01

CREATE OR REPLACE VIEW ECO_TIPO_DOCUMENTO_COMBO_V (
  id,
  id_empresa,
  codigo,
  descripcion
) 
AS SELECT
  t01.id_tip_doc_persona,
  t01.id_empresa,
  t01.cod_interno,
  t01.cod_tip_doc_persona
FROM nuc_tip_doc_persona t01