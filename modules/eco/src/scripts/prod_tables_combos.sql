PROMPT CREACION DE VISTA ECO_COD_ECO_PREF_COMP_UB_V
CREATE OR REPLACE VIEW ECO_COD_ECO_PREF_COMP_UB_V (
ID,
ID_EMPRESA,
COD_LIN_NEG,
LIN_NEG,
COD_SOCIO_NEG,
SOCIO_NEG,
COD_PRODUCTO,
PRODUCTO,
COD_PLAN_NEG,
PLAN_NEG,
NUM_CTA,
NUM_SERVICIO,
NUM_IDENTIFICADOR,
TIPO_DOC_TIT,
NUM_DOC_TIT,
FECHA_CARGUE,
NUM_CUOTAS_ENC_COB,
VAL_CUOTA_ENC_COB,
VAL_TOT_ENC,
NUM_CUOTAS_FACT_NO_REC,
FECHA_ULT_FACT,
ESTADO_ENCARGO,
ESTRATO_SE,
SUCURSAL_RF,
ZONA_RF,
CICLO_RF,
GRUPO_RF
) AS
SELECT
ROWNUM, EMP.ID_EMPRESA,
LIN.ID_LINEA_NEGOCIO, LIN.DESCRIPCION LINEA, SOCIO.ID_EMPRESA_SOCIO, SOCIO.DESCRIPTION SOCIO, CLAS.ID_CLASES_ECO,
CLAS.DESCRIPTION PRODUCTO, PLA.ID_PLAN_NEG,
PLA.DESCRIPCION PLAN, CUENTA.NRO_CUENTA CUENTA,SER.NRO_SERVICIO SERVICIO,
ECO.NRO_ID_VENTA ID_VENTA, TDOC.COD_TIP_DOC_PERSONA TIPO_DOC, PERS.NRO_DOCTO_IDENT NRO_DOC,
SER.FEC_INICIO FECHA_CARGUE,
(SELECT DECODE(COUNT(ID_FINANCIACION),0,NULL,COUNT(ID_FINANCIACION))
FROM FIN_CUOTA CUOT WHERE CUOT.ID_FINANCIACION = ECO.ID_FINANCIACION) AS NUM_CUOTAS,
DECODE(NVL(NCTA.CAPITAL,0),0,ECO.MONTO_CUOTA,NCTA.CAPITAL) VALOR_CUOTA,
(SELECT DECODE(SUM (NCTA.CAPITAL) + SUM(FCTA.CAPITAL_NO_AFECTO_INTERES),
0, NULL,SUM (NCTA.CAPITAL) + SUM(FCTA.CAPITAL_NO_AFECTO_INTERES))
FROM FIN_CUOTA FCTA, NUC_CUOTA NCTA WHERE NCTA.ID_CUOTA = FCTA.ID_CUOTA
AND ECO.ID_FINANCIACION = FCTA.ID_FINANCIACION ) AS VALOR_ECO,
(SELECT COUNT(IPAGO.ID_PAGO) FROM NUC_ITEM_DOC DOC, NUC_CARGO CARGO, REC_ITEM_PAGO IPAGO
WHERE DOC.ID_CARGO = CARGO.ID_CARGO
AND CARGO.ID_TIP_CARGO = 1051
AND DOC.ID_SERVICIO = ECO.ID_SERVICIO
AND DOC.SALDO > 0
AND DOC.ID_ITEM_DOC = IPAGO.ID_ITEM_DOC)
AS CANT_CTAS_NO_REC,
(SELECT MAX(DOCU.FEC_DOCUMENTO)
FROM NUC_DOCUMENTO DOCU, ECO_ENCARGO_COB ECO, NUC_ITEM_DOC DOC
WHERE ECO.ID_SERVICIO = DOC.ID_SERVICIO
AND DOC.ID_DOCUMENTO = DOCU.ID_DOCUMENTO) AS FECHA_FACT,
DECODE(WKF.ID_STATE,'Eliminado','Finalizado','Finalizado','Terminado',WKF.ID_STATE) ESTADO,
EST.DESCRIPCION ESTRATO ,
MOROSIDAD.F_SUCURSAL SUCURSAL,
MOROSIDAD.F_ZONA ZONA,
MOROSIDAD.F_CICLO CICLO,
MOROSIDAD.F_GRUPO GRUPO
FROM
NUC_SERVICIO SER, ECO_ENCARGO_COB ECO, ECO_CLASES_ECO CLAS, ECO_PLAN_NEG PLA,
ECO_EMPRESA_SOCIO SOCIO, ECO_REL_SOC_CLASECO RELA, ECO_LINEA_NEGOCIO LIN,
NUC_CUENTA CUENTA, ECO_TITULAR TITU, NUC_PERSONA PERS, NUC_TIP_DOC_PERSONA TDOC,
FIN_TIPO_FINANC TFIN, NUC_CARGO CAR, WKF_WORKFLOW WKF, NUC_CUOTA NCTA,
FIN_CUOTA FCTA, SRV_ELECTRICO SE, NUC_SERVICIO SER2,
SRV_ESTRATOSOC EST, NUC_EMPRESA EMP
WHERE
SER.ID_SERVICIO = ECO.ID_SERVICIO
AND
ECO.ID_PLAN_NEGOCIO = PLA.ID_PLAN_NEG
AND
PLA.ID_REL_SOC_CLASECO = RELA.ID_REL_SOC_CLASECO
AND
RELA.ID_CLASES_ECO = CLAS.ID_CLASES_ECO
AND
RELA.ID_EMPRESA_SOCIO = SOCIO.ID_EMPRESA_SOCIO
AND
LIN.ID_LINEA_NEGOCIO = CLAS.ID_LINEA_NEGOCIO
AND
CUENTA.ID_CUENTA = SER.ID_CUENTA
AND
SER.ID_SERVICIO = TITU.ID_SERVICIO_ECO
AND
TITU.ID_PERSONA = PERS.ID_PERSONA
AND
TDOC.ID_TIP_DOC_PERSONA = PERS.ID_TIP_DOC_PERSONA
AND
TFIN.ID_TIPO_FINANC = PLA.ID_TIPO_FINANC
AND
CAR.ID_CARGO = TFIN.ID_CARGO_CAPITAL
AND
ECO.ID_WORKFLOW = WKF.ID_WORKFLOW
AND
ECO.ID_FINANCIACION = FCTA.ID_FINANCIACION(+)
AND
NCTA.ID_CUOTA(+) = FCTA.ID_CUOTA
AND
NVL(NCTA.NUMERO_CUOTA,0)  < 2
AND PLA.ID_PLAN_NEG = ECO.ID_PLAN_NEGOCIO
AND ECO.ID_SERVICIO = SER.ID_SERVICIO
AND SER2.ID_CUENTA = SER.ID_CUENTA
AND SER2.TIPO      = 'ELECTRICO'
AND SE.ID_SERVICIO = SER2.ID_SERVICIO
AND SE.ID_ESTRATOSOC = EST.ID_ESTRATOSOC
AND EMP.COD_PARTITION = 'COD'
AND SER.ID_EMPRESA = EMP.ID_EMPRESA
AND PLA.ID_EMPRESA = EMP.ID_EMPRESA
AND SER2.ID_EMPRESA = EMP.ID_EMPRESA
AND WKF.ID_EMPRESA = EMP.ID_EMPRESA
AND MOROSIDAD.F_CALCULAR_RUTA(SER2.ID_SERVICIO) = 'S'
ORDER BY SER.FEC_INICIO, SER.NRO_SERVICIO
/
PROMPT CREACION DE VISTA eco_cod_eco_porc_pcub_v
CREATE OR REPLACE VIEW eco_cod_eco_porc_pcub_v (
id,
id_empresa,
id_servicio,
cargo,
impuesto,
porcentaje
) AS
SELECT rownum,emp.id_empresa, eco.id_servicio,
cargo.descripcion CARGO, imp.descripcion IMPUESTO, porc.valor PORCENTAJE
FROM fac_cargo_impuesto impcar, fac_impuesto imp, nuc_empresa emp, fac_val_imp_per porc,
nuc_cargo cargo, eco_encargo_cob eco, fin_tipo_financ tfin, eco_plan_neg plan
WHERE cargo.id_cargo (+) = impcar.id_cargo
AND impcar.id_impuesto = imp.id_impuesto
AND imp.id_impuesto = porc.id_impuesto
AND impcar.id_empresa = emp.id_empresa
AND imp.id_empresa = emp.id_empresa
AND eco.id_plan_negocio = plan.id_plan_neg
AND plan.id_tipo_financ = tfin.id_tipo_financ
AND tfin.id_cargo_capital = cargo.id_cargo
AND emp.id_empresa = cargo.id_empresa
ORDER BY eco.id_servicio , impcar.id_impuesto

/
PROMPT CREACION DE VISTA ECO_COD_ECO_FACTURACION_V
CREATE OR REPLACE VIEW ECO_COD_ECO_FACTURACION_V (
ID,
ID_EMPRESA,
COD_LIN_NEG,
LIN_NEG,
COD_SOCIO_NEG,
SOCIO_NEG,
COD_PRODUCTO,
PRODUCTO,
COD_PLAN_NEG,
PLAN_NEG,
NUM_CTA,
NUM_SERVICIO,
NUM_IDENTIFICADOR,
TIPO_DOC_TIT,
NUM_DOC_TIT,
FECHA_CARGUE,
FECHA_ULT_FACT,
NUM_ULTIMA_FACT,
VAL_FACT_ENC_COB,
VAL_FACT_INT_MORA,
VAL_FACT_IMP_ENC_COB,
VAL_TOT_FACT_ENC_COB,
VAL_TOT_FACT_INT_MORA,
VAL_TOT_FACT_IMP_ENC,
NUM_TOT_CUOTAS_ENC_COB,
NUM_ULT_CUOTA_FACT,
VAL_CUOTA_ENC_COB,
FECHA_PRI_VTO,
FECHA_SDO_VTO,
SUCURSAL_RF,
ZONA_RF,
CICLO_RF,
GRUPO_RF
) AS
SELECT
rownum,
emp.id_empresa,
lin.id_linea_negocio,
lin.descripcion AS LINEA,
socio.id_empresa_socio,
socio.description AS SOCIO,
clas.id_clases_eco,
clas.description AS PRODUCTO,
pla.id_plan_neg,
pla.descripcion AS PLAN,
cuenta.nro_cuenta AS CUENTA,
ser.nro_servicio AS SERVICIO,
eco.nro_id_venta AS IDENT_VENTA,
tdoc.cod_tip_doc_persona AS TIPO_DOC,
pers.nro_docto_ident AS NRO_DOC,
ser.fec_inicio AS FECHA_CARGUE,
(SELECT MAX(docu.fec_documento)
FROM nuc_documento docu,nuc_item_doc doc
WHERE eco.id_servicio = doc.id_servicio
AND doc.id_documento = docu.id_documento)
AS FECHA_FACT,
(SELECT MAX(docu.nro_documento)
FROM nuc_documento docu, nuc_item_doc doc
WHERE eco.id_servicio = doc.id_servicio
AND doc.id_documento = docu.id_documento)
AS NRO_ULT_FACT,
(SELECT doc.monto
FROM nuc_item_doc doc,nuc_cargo cargo,nuc_documento docu
WHERE doc.id_cargo = cargo.id_cargo
AND cargo.id_tip_cargo = 1051
AND doc.id_servicio = eco.id_servicio
AND doc.id_documento = docu.id_documento
AND docu.fec_documento = (SELECT MAX(docu.fec_documento)
FROM nuc_documento docu,nuc_item_doc doc
WHERE eco.id_servicio = doc.id_servicio
AND doc.id_documento = docu.id_documento))
as VALOR_CTA_ECO,
(SELECT SUM(doc.monto)
FROM nuc_item_doc doc,nuc_cargo cargo,nuc_documento docu
WHERE doc.id_cargo IN(148,150,151,152,153,157)
AND cargo.id_tip_cargo = 1045
AND doc.id_servicio = eco.id_servicio
AND doc.id_documento = docu.id_documento
AND docu.fec_documento = (SELECT MAX(docu.fec_documento)
FROM nuc_documento docu,nuc_item_doc doc
WHERE eco.id_servicio = doc.id_servicio
AND doc.id_documento = docu.id_documento))
AS INT_MORA_ULT_FACT,
(SELECT sum(doc.monto)
FROM nuc_item_doc doc,nuc_cargo cargo,nuc_documento docu
WHERE doc.id_cargo = cargo.id_cargo
AND cargo.id_tip_cargo = 1045
AND doc.id_servicio = eco.id_servicio
AND doc.id_documento = docu.id_documento
AND docu.fec_documento = (SELECT MAX(docu.fec_documento)
FROM nuc_documento docu,nuc_item_doc doc
WHERE eco.id_servicio = doc.id_servicio
AND doc.id_documento = docu.id_documento))
AS IMP_ULT_FACT,
(SELECT SUM(doc.monto)
FROM nuc_item_doc doc,nuc_documento docu
WHERE doc.id_servicio = eco.id_servicio
AND doc.id_documento = docu.id_documento
AND docu.fec_documento = (SELECT MAX(docu.fec_documento)
FROM nuc_documento docu,nuc_item_doc doc
WHERE eco.id_servicio = doc.id_servicio
AND doc.id_documento = docu.id_documento))
AS TOTAL_ULT_FACT,
(SELECT DECODE(COUNT(id_financiacion),0,NULL,COUNT(id_financiacion))
FROM fin_cuota cuot WHERE cuot.id_financiacion = eco.id_financiacion)
AS TOTAL_CTAS_ECO,
(SELECT SUM (doc.monto)
FROM nuc_item_doc doc,nuc_cargo cargo
WHERE doc.id_cargo = cargo.id_cargo
AND cargo.id_tip_cargo = 1051
AND doc.id_servicio = eco.id_servicio)
AS TOTAL_FACTURADO,
(SELECT SUM (doc.monto)
FROM nuc_item_doc doc,nuc_cargo cargo
WHERE doc.id_cargo in (148,150,151,152,153,157)
AND doc.id_servicio = eco.id_servicio)
AS TOTAL_FACT_INT_MORA,
(SELECT SUM (doc.monto)
FROM nuc_item_doc doc,nuc_cargo cargo
WHERE doc.id_cargo = cargo.id_cargo
AND cargo.id_tip_cargo = 1045
AND doc.id_servicio = eco.id_servicio)
AS TOTAL_FACT_IMP,
(SELECT MAX(cuot.numero_cuota)
FROM fin_cuota finc,nuc_cuota cuot
WHERE eco.id_financiacion = finc.id_financiacion
AND finc.id_cuota = cuot.id_cuota
AND (cuot.cuota_state = 'Facturada' OR cuot.cuota_state = 'Facturando'))
AS NRO_ULT_CTA_FACT,
(SELECT Nvl(docu.fec_vencimiento,'')
FROM nuc_documento docu,nuc_item_doc doc
WHERE eco.id_servicio = doc.id_servicio
AND doc.id_documento = docu.id_documento
AND docu.fec_documento = (SELECT MAX(docu.fec_documento)
FROM nuc_documento docu,nuc_item_doc doc
WHERE eco.id_servicio = doc.id_servicio
AND doc.id_documento = docu.id_documento))
AS PRIMER_VENC,
(SELECT NVL(DOCU.FEC_SEGUNDO_VENC,'')
FROM nuc_documento docu,nuc_item_doc doc
WHERE eco.id_servicio = doc.id_servicio
AND doc.id_documento = docu.id_documento
AND docu.fec_documento = (SELECT MAX(docu.fec_documento)
FROM nuc_documento docu,nuc_item_doc doc
WHERE eco.id_servicio = doc.id_servicio
AND doc.id_documento = docu.id_documento))
AS SEGUNDO_VENC,
morosidad.f_sucursal SUCURSAL,
morosidad.f_zona ZONA,
morosidad.f_ciclo CICLO,
morosidad.f_grupo GRUPO
FROM
nuc_servicio ser,
nuc_empresa emp,
eco_encargo_cob eco,
eco_clases_eco clas,
eco_plan_neg pla,
eco_empresa_socio socio,
eco_rel_soc_claseco rela,
eco_linea_negocio lin,
nuc_cuenta cuenta,
eco_titular titu,
nuc_persona pers,
nuc_tip_doc_persona tdoc,
fin_tipo_financ tfin,
nuc_cargo car,
srv_electrico se,
nuc_servicio ser2
WHERE
ser.id_servicio = eco.id_servicio AND eco.id_plan_negocio = pla.id_plan_neg
AND pla.id_rel_soc_claseco = rela.id_rel_soc_claseco
AND rela.id_clases_eco = clas.id_clases_eco
AND rela.id_empresa_socio = socio.id_empresa_socio
AND lin.id_linea_negocio = clas.id_linea_negocio
AND cuenta.id_cuenta = ser.id_cuenta
AND ser.id_servicio = titu.id_servicio_eco
AND titu.id_persona = pers.id_persona
AND tdoc.id_tip_doc_persona = pers.id_tip_doc_persona
AND tfin.id_tipo_financ = pla.id_tipo_financ
AND car.id_cargo = tfin.id_cargo_capital
AND ser2.id_cuenta = ser.id_cuenta
AND ser2.tipo = 'ELECTRICO'
AND se.id_servicio = ser2.id_servicio
AND emp.cod_partition = 'COD'
AND ser.id_empresa = emp.id_empresa
AND ser2.id_empresa = emp.id_empresa
AND pla.id_empresa = emp.id_empresa
AND morosidad.f_calcular_ruta(ser2.id_servicio) = 'S'
ORDER BY ser.nro_servicio, ser.fec_inicio

/
PROMPT CREACION DE VISTA ECO_COD_ECO_RECAUDO_V
CREATE OR REPLACE VIEW ECO_COD_ECO_RECAUDO_V (
ID,
ID_EMPRESA,
COD_LIN_NEG,
LIN_NEG,
COD_SOCIO_NEG,
SOCIO_NEG,
COD_PRODUCTO,
PRODUCTO,
COD_PLAN_NEG,
PLAN_NEG,
NUM_CTA,
NUM_SERVICIO,
NUM_IDENTIFICADOR,
COD_TIPO_DOC_TIT,
TIPO_DOC_TIT,
NUM_DOC_TIT,
NUM_FACTURA,
FECHA_REAL_PAGO,
FECHA_PROCESO,
NUM_CUOTAS_ENC_COB,
CANT_CUOTAS_REC,
VAL_CUOTA_ENC_COB,
VAL_TOTAL_REC_CUOTAS,
VAL_REC_INT_MOR_ENC_COB,
VAL_IMP_REC_ENC_COB
) AS
SELECT
ROWNUM,
EMP.ID_EMPRESA,
LINEA.ID_LINEA_NEGOCIO,
LINEA.DESCRIPCION LINEA,
SOCIO.ID_EMPRESA_SOCIO,
SOCIO.DESCRIPTION SOCIO,
CLASE.ID_CLASES_ECO,
CLASE.DESCRIPTION CLASE,
PLAN_NEG.ID_PLAN_NEG,
PLAN_NEG.DESCRIPCION PLAN,
CUENTA.NRO_CUENTA CUENTA,
SERVICIO.NRO_SERVICIO SERVICIO,
ECO.NRO_ID_VENTA ID_VTA,
TDOC.ID_TIP_DOC_PERSONA,
TDOC.COD_TIP_DOC_PERSONA TIPO_DOC,
PERSONA.NRO_DOCTO_IDENT NRO_DOC,
(SELECT DOCU.NRO_DOCUMENTO
FROM NUC_DOCUMENTO DOCU, ECO_ENCARGO_COB ECO, NUC_ITEM_DOC DOC
WHERE ECO.ID_SERVICIO = DOC.ID_SERVICIO
AND DOC.ID_DOCUMENTO = DOCU.ID_DOCUMENTO
AND DOC.SALDO <= 0)
AS NRO_FACT,
(SELECT LOTE.FECHA_PAGO
FROM REC_LOTE_PAGO LOTE, NUC_ITEM_DOC DOC, REC_ITEM_PAGO PAGO, REC_PAGO RPAGO, ECO_ENCARGO_COB ECO,
NUC_DOCUMENTO DOCU
WHERE DOC.ID_ITEM_DOC = PAGO.ID_ITEM_DOC
AND RPAGO.ID_PAGO = PAGO.ID_PAGO
AND RPAGO.ID_LOTE = LOTE.ID_LOTE
AND DOC.ID_SERVICIO = ECO.ID_SERVICIO
AND DOCU.ID_DOCUMENTO = DOC.ID_DOCUMENTO
AND DOCU.NRO_DOCUMENTO IN (SELECT DOCU.NRO_DOCUMENTO
FROM NUC_DOCUMENTO DOCU, ECO_ENCARGO_COB ECO, NUC_ITEM_DOC DOC
WHERE ECO.ID_SERVICIO = DOC.ID_SERVICIO
AND DOC.ID_DOCUMENTO = DOCU.ID_DOCUMENTO))
AS FECHA_REAL_PAGO,
(SELECT LOTE.FECHA_PROCESO
FROM REC_LOTE_PAGO LOTE, NUC_ITEM_DOC DOC, REC_ITEM_PAGO PAGO, REC_PAGO RPAGO, ECO_ENCARGO_COB ECO,
NUC_DOCUMENTO DOCU
WHERE DOC.ID_ITEM_DOC = PAGO.ID_ITEM_DOC
AND RPAGO.ID_PAGO = PAGO.ID_PAGO
AND RPAGO.ID_LOTE = LOTE.ID_LOTE
AND DOC.ID_SERVICIO = ECO.ID_SERVICIO
AND DOCU.ID_DOCUMENTO = DOC.ID_DOCUMENTO
AND DOCU.NRO_DOCUMENTO IN (SELECT DOCU.NRO_DOCUMENTO
FROM NUC_DOCUMENTO DOCU, ECO_ENCARGO_COB ECO, NUC_ITEM_DOC DOC
WHERE ECO.ID_SERVICIO = DOC.ID_SERVICIO
AND DOC.ID_DOCUMENTO = DOCU.ID_DOCUMENTO))
AS FECHA_PROCESO,
(SELECT DECODE(COUNT(ID_FINANCIACION),0,NULL,COUNT(ID_FINANCIACION))
FROM FIN_CUOTA CUOT WHERE CUOT.ID_FINANCIACION = ECO.ID_FINANCIACION) AS NUM_CUOTAS,
(SELECT COUNT (IPAGO.ID_PAGO)
FROM NUC_ITEM_DOC DOC, ECO_ENCARGO_COB ECO, NUC_CARGO CARGO, REC_ITEM_PAGO IPAGO
WHERE DOC.ID_CARGO = CARGO.ID_CARGO
AND CARGO.ID_TIP_CARGO = 1051
AND DOC.ID_SERVICIO = ECO.ID_SERVICIO
AND DOC.SALDO <= 0
AND DOC.ID_ITEM_DOC = IPAGO.ID_ITEM_DOC)
AS CANT_CUOTAS_REC,
DECODE(NVL(NCTA.CAPITAL,0),0,ECO.MONTO_CUOTA,NCTA.CAPITAL) VALOR_CUOTA,
(SELECT SUM(IPAGO.MONTO)
FROM NUC_ITEM_DOC DOC, ECO_ENCARGO_COB ECO, NUC_CARGO CARGO, REC_ITEM_PAGO IPAGO
WHERE DOC.ID_CARGO = CARGO.ID_CARGO
AND CARGO.ID_TIP_CARGO = 1051
AND DOC.ID_SERVICIO = ECO.ID_SERVICIO
AND DOC.SALDO <= 0
AND DOC.ID_ITEM_DOC = IPAGO.ID_ITEM_DOC)
AS VALOR_CUOTAS_REC,
(SELECT SUM (IPAGO.MONTO)
FROM NUC_ITEM_DOC DOC, ECO_ENCARGO_COB ECO, NUC_CARGO CARGO, REC_ITEM_PAGO IPAGO
WHERE DOC.ID_CARGO = CARGO.ID_CARGO
AND CARGO.COD_CARGO IN (SELECT VALOR FROM FAC_CONFIGURACION WHERE PARAMETRO = 'CARGO INTERES')
AND DOC.ID_SERVICIO = ECO.ID_SERVICIO
AND DOC.SALDO < = 0
AND DOC.ID_ITEM_DOC = IPAGO.ID_ITEM_DOC)
AS VALOR_INT_MORA_REC,
(SELECT SUM (IPAGO.MONTO)
FROM NUC_ITEM_DOC DOC, ECO_ENCARGO_COB ECO, NUC_CARGO CARGO, REC_ITEM_PAGO IPAGO
WHERE DOC.ID_CARGO = CARGO.ID_CARGO
AND CARGO.ID_TIP_CARGO = 1045
AND DOC.ID_SERVICIO = ECO.ID_SERVICIO
AND DOC.SALDO < = 0
AND DOC.ID_ITEM_DOC = IPAGO.ID_ITEM_DOC)
AS VALOR_IMP_REC
FROM
ECO_LINEA_NEGOCIO LINEA,
ECO_EMPRESA_SOCIO SOCIO,
ECO_CLASES_ECO CLASE,
ECO_PLAN_NEG PLAN_NEG,
NUC_CUENTA CUENTA,
NUC_SERVICIO SERVICIO,
ECO_ENCARGO_COB ECO,
NUC_TIP_DOC_PERSONA TDOC,
NUC_PERSONA PERSONA,
NUC_EMPRESA EMP,
ECO_REL_SOC_CLASECO RELA,
ECO_TITULAR TITU,
FIN_TIPO_FINANC TFIN,
NUC_CARGO CAR,
NUC_CUOTA NCTA,
FIN_CUOTA FCTA
WHERE LINEA.ID_LINEA_NEGOCIO = CLASE.ID_LINEA_NEGOCIO
AND CLASE.ID_CLASES_ECO = RELA.ID_CLASES_ECO
AND PLAN_NEG.ID_REL_SOC_CLASECO = RELA.ID_REL_SOC_CLASECO
AND RELA.ID_EMPRESA_SOCIO = SOCIO.ID_EMPRESA_SOCIO
AND SERVICIO.ID_SERVICIO = ECO.ID_SERVICIO
AND ECO.ID_PLAN_NEGOCIO = PLAN_NEG.ID_PLAN_NEG
AND CUENTA.ID_CUENTA = SERVICIO.ID_CUENTA
AND ECO.ID_SERVICIO = TITU.ID_SERVICIO_ECO
AND TITU.ID_PERSONA = PERSONA.ID_PERSONA
AND TDOC.ID_TIP_DOC_PERSONA = PERSONA.ID_TIP_DOC_PERSONA
AND TFIN.ID_TIPO_FINANC = PLAN_NEG.ID_TIPO_FINANC
AND CAR.ID_CARGO = TFIN.ID_CARGO_CAPITAL
AND ECO.ID_FINANCIACION = FCTA.ID_FINANCIACION(+)
AND NCTA.ID_CUOTA(+) = FCTA.ID_CUOTA
AND NVL(NCTA.NUMERO_CUOTA,0)  < 2
AND EMP.COD_PARTITION = 'COD'
AND SERVICIO.ID_EMPRESA = EMP.ID_EMPRESA
AND PLAN_NEG.ID_EMPRESA = EMP.ID_EMPRESA
ORDER BY FECHA_REAL_PAGO, NRO_FACT

/
PROMPT CREACION DE VISTA ECO_COD_ECO_TRASLADOS_V
CREATE OR REPLACE VIEW ECO_COD_ECO_TRASLADOS_V (
ID,
ID_EMPRESA,
COD_LIN_NEG,
LIN_NEG,
COD_SOCIO_NEG,
SOCIO_NEG,
COD_PRODUCTO,
PRODUCTO,
COD_PLAN_NEG,
PLAN_NEG,
FECHA_TRASLADO,
NUM_CTA_ANT,
NUM_CTA_NUEVA,
VAL_CUOTA_ENC_COB,
NUM_CUOTAS_FACT,
NUM_CUOTAS_PEND_FACT,
NUM_CUOTAS_REC
) AS
SELECT
ROWNUM, EMP.ID_EMPRESA,
LIN.ID_LINEA_NEGOCIO,
LIN.DESCRIPCION LINEA,
SOCIO.ID_EMPRESA_SOCIO,
SOCIO.DESCRIPTION SOCIO,
CLAS.ID_CLASES_ECO,
CLAS.DESCRIPTION PRODUCTO,
PLA.ID_PLAN_NEG,
PLA.DESCRIPCION PLAN,
ECO.FECHA_TRASLADO FECHA_TRASLADO,
ECO.NRO_CUENTA_ORIGEN NRO_CTA_ANTERIOR,
CUENTA.NRO_CUENTA NRO_CTA_NUEVA,
DECODE(NVL(NCTA.CAPITAL,0),0,ECO.MONTO_CUOTA,NVL(NCTA.CAPITAL,0)) VALOR_CUOTA,
(SELECT DECODE (COUNT(FCTA.ID_FINANCIACION),0,NULL,COUNT(FCTA.ID_FINANCIACION))
FROM NUC_CUOTA NCUOT, FIN_CUOTA FCUOTA
WHERE (NCUOT.CUOTA_STATE = 'Facturada' OR NCUOT.CUOTA_STATE = 'Facturando')
AND NCUOT.ID_CUOTA = FCUOTA.ID_CUOTA AND FCUOTA.ID_FINANCIACION = ECO.ID_FINANCIACION)
AS NUM_CUOTAS_FACT,
(SELECT DECODE (COUNT(FCUOTA.ID_FINANCIACION),0,NULL,COUNT(FCUOTA.ID_FINANCIACION))
FROM NUC_CUOTA NCUOT, FIN_CUOTA FCUOTA
WHERE NCUOT.CUOTA_STATE = 'NoFacturada'
AND NCUOT.ID_CUOTA = FCUOTA.ID_CUOTA AND FCUOTA.ID_FINANCIACION = ECO.ID_FINANCIACION)
AS NUM_CUOTAS_NO_FACT,
(SELECT DECODE (COUNT(IPAGO.ID_PAGO),0,0,COUNT(IPAGO.ID_PAGO))
FROM NUC_ITEM_DOC DOC, NUC_CARGO CARGO, REC_ITEM_PAGO IPAGO, ECO_ENCARGO_COB ECO
WHERE DOC.ID_CARGO = CARGO.ID_CARGO
AND CARGO.ID_TIP_CARGO = 1051
AND DOC.ID_SERVICIO = ECO.ID_SERVICIO
AND DOC.SALDO < = 0
AND DOC.ID_ITEM_DOC = IPAGO.ID_ITEM_DOC)
AS CANT_CTAS_REC
FROM
NUC_SERVICIO SER, ECO_ENCARGO_COB ECO, ECO_CLASES_ECO CLAS, ECO_PLAN_NEG PLA,
ECO_EMPRESA_SOCIO SOCIO, ECO_REL_SOC_CLASECO RELA, ECO_LINEA_NEGOCIO LIN,
NUC_CUENTA CUENTA, FIN_TIPO_FINANC TFIN, WKF_WORKFLOW WKF, NUC_CUOTA NCTA,
FIN_CUOTA FCTA, NUC_EMPRESA EMP
WHERE
SER.ID_SERVICIO = ECO.ID_SERVICIO
AND
ECO.ID_PLAN_NEGOCIO = PLA.ID_PLAN_NEG
AND
PLA.ID_REL_SOC_CLASECO = RELA.ID_REL_SOC_CLASECO
AND
RELA.ID_CLASES_ECO = CLAS.ID_CLASES_ECO
AND
RELA.ID_EMPRESA_SOCIO = SOCIO.ID_EMPRESA_SOCIO
AND
LIN.ID_LINEA_NEGOCIO = CLAS.ID_LINEA_NEGOCIO
AND
CUENTA.ID_CUENTA = SER.ID_CUENTA
AND
TFIN.ID_TIPO_FINANC = PLA.ID_TIPO_FINANC
AND
ECO.ID_WORKFLOW = WKF.ID_WORKFLOW
AND
ECO.ID_FINANCIACION = FCTA.ID_FINANCIACION(+)
AND
NCTA.ID_CUOTA(+) = FCTA.ID_CUOTA
AND
NVL(NCTA.NUMERO_CUOTA,0)  < 2
AND
ECO.ID_SERVICIO = SER.ID_SERVICIO
AND
EMP.COD_PARTITION = 'COD'
AND
EMP.ID_EMPRESA = SER.ID_EMPRESA
ORDER BY ECO.FECHA_TRASLADO, ECO.ID_SERVICIO

/
PROMPT CREACION DE VISTA ECO_COD_ECO_CARTERA_MOROSOS_V
CREATE OR REPLACE VIEW ECO_COD_ECO_CARTERA_MOROSOS_V (
ID,
ID_EMPRESA,
COD_LIN_NEG,
LIN_NEG,
COD_SOCIO_NEG,
SOCIO_NEG,
COD_PRODUCTO,
PRODUCTO,
COD_PLAN_NEG,
PLAN_NEG,
NUM_CTA,
NUM_SERVICIO,
NUM_IDENTIFICADOR,
TIPO_DOC_TIT,
NUM_DOC_TIT,
FECHA_CARGUE,
VAL_ENC_COB,
NUM_CUOTAS_ENC_COB,
NUM_CUOTAS_FACT,
NUM_CUOTAS_NP_PLAZO,
NUM_CUOTAS_NP_MORA,
NUM_CUOTAS_PG,
VAL_CUOTA_ENC_COB,
VAL_CUOTAS_FACT,
VAL_CUOTAS_FACT_NP_PLAZO,
VAL_CUOTAS_FACT_NP_MORA,
VAL_CUOTAS_PG,
VAL_INT_MORA_FACT,
VAL_INT_MORA_FACT_NP_PLAZO,
VAL_INT_MORA_FACT_NP_MORA,
VAL_INT_MORA_PG,
VAL_IMP_FACT,
VAL_IMP_FACT_NP_PLAZO,
VAL_IMP_FACT_NP_MORA,
VAL_IMP_PAGADO,
DIAS_ATRASO,
ESTRATO_SE,
CLASE_SE,
SUBCLASE_SE,
SUCURSAL_RF,
ZONA_RF,
CICLO_RF,
GRUPO_RF
) AS
SELECT rownum,
emp.id_empresa,
lin.id_linea_negocio,
lin.descripcion LINEA,
socio.id_empresa_socio,
socio.description SOCIO,
clas.id_clases_eco,
clas.description PRODUCTO,
pla.id_plan_neg,
pla.descripcion PLAN,
cuenta.nro_cuenta CUENTA,
ser.nro_servicio SERVICIO,
eco.nro_id_venta ID_VTA,
tdoc.cod_tip_doc_persona TIPO_DOC,
pers.nro_docto_ident NRO_DOC,
ser.fec_inicio FECHA_CARGUE,
decode(nvl(ncta.capital,   0),   0,   eco.monto_cuota,   ncta.capital) valor_cuota,
(SELECT decode(COUNT(id_financiacion),    0,    NULL,    COUNT(id_financiacion))
FROM fin_cuota cuot
WHERE cuot.id_financiacion = eco.id_financiacion)
AS
NUM_CUOTAS,
(SELECT COUNT(DOC.id_servicio)
FROM nuc_item_doc DOC, eco_encargo_cob eco, nuc_cargo cargo
WHERE DOC.id_cargo = cargo.id_cargo
AND cargo.id_tip_cargo = 1051
AND DOC.id_servicio = eco.id_servicio) AS NRO_CUOTAS_FACTURADAS,
(SELECT COUNT(DOC.id_servicio)
FROM nuc_item_doc DOC,eco_encargo_cob	eco, nuc_cargo cargo,	nuc_documento	docu
WHERE DOC.saldo	>	0
AND docu.fec_vencimiento > sysdate
AND DOC.id_servicio	=	eco.id_servicio
AND DOC.id_cargo = cargo.id_cargo
AND cargo.id_tip_cargo = 1051) AS NO_PAGADAS_EN_PLAZO,
(SELECT COUNT(DOC.id_servicio)
FROM nuc_item_doc DOC,eco_encargo_cob	eco, nuc_cargo cargo,	nuc_documento	docu
WHERE DOC.saldo	>	0
AND docu.fec_vencimiento < sysdate
AND DOC.id_servicio	=	eco.id_servicio
AND DOC.id_cargo = cargo.id_cargo
AND cargo.id_tip_cargo = 1051) AS NO_PAGADAS_EN_MORA,
(SELECT COUNT(DOC.id_servicio)
FROM nuc_item_doc DOC,eco_encargo_cob eco, nuc_cargo cargo
WHERE DOC.saldo <= 0
AND DOC.id_servicio = eco.id_servicio
AND cargo.id_tip_cargo = 1051) AS CUOTAS_PAGADAS,
(SELECT decode(SUM(ncta.capital) + SUM(fcta.capital_no_afecto_interes),    0,    NULL,    SUM(ncta.capital) + SUM(fcta.capital_no_afecto_interes))
FROM fin_cuota fcta,    nuc_cuota ncta
WHERE ncta.id_cuota = fcta.id_cuota
AND eco.id_financiacion = fcta.id_financiacion) AS
VALOR_ECO,
(SELECT SUM(DOC.monto)
FROM nuc_item_doc DOC, eco_encargo_cob eco, nuc_cargo cargo
WHERE DOC.id_cargo = cargo.id_cargo
AND cargo.id_tip_cargo = 1051
AND DOC.id_servicio = eco.id_servicio)  AS VALOR_CUOTAS_FACT,
(SELECT SUM(DOC.monto)
FROM nuc_item_doc DOC, eco_encargo_cob eco, nuc_cargo cargo, nuc_documento docu
WHERE DOC.id_cargo = cargo.id_cargo
AND cargo.id_tip_cargo = 1051
AND DOC.id_servicio = eco.id_servicio
AND DOC.id_documento = docu.id_documento
AND docu.fec_vencimiento > sysdate
AND DOC.saldo > 0 ) AS VALOR_NO_PAGADAS_EN_PLAZO,
(SELECT SUM(DOC.monto)
FROM nuc_item_doc DOC, eco_encargo_cob eco, nuc_cargo cargo, nuc_documento docu
WHERE DOC.id_cargo = cargo.id_cargo
AND cargo.id_tip_cargo = 1051
AND DOC.id_servicio = eco.id_servicio
AND DOC.id_documento = docu.id_documento
AND docu.fec_vencimiento < sysdate
AND DOC.saldo > 0 )  AS VALOR_NO_PAGADAS_EN_MORA,
(SELECT SUM(DOC.monto)
FROM nuc_item_doc DOC, eco_encargo_cob eco, nuc_cargo cargo
WHERE DOC.id_cargo = cargo.id_cargo
AND cargo.id_tip_cargo = 1051
AND DOC.id_servicio = eco.id_servicio
AND DOC.saldo < = 0) AS VALOR_CUOTAS_PAGADAS,
(SELECT SUM(DOC.monto)
FROM nuc_item_doc DOC, eco_encargo_cob eco
WHERE DOC.id_cargo IN (148 ,150, 151 , 152, 153, 157)
AND DOC.id_servicio = eco.id_servicio) AS INTERESES_MORA,
(SELECT SUM(DOC.monto)
FROM nuc_item_doc DOC, eco_encargo_cob eco, nuc_documento docu
WHERE DOC.id_cargo IN (148 ,150, 151 , 152, 153, 157)
AND DOC.id_servicio = eco.id_servicio
AND DOC.id_documento = docu.id_documento
AND DOC.saldo > 0
AND docu.fec_vencimiento > = sysdate) AS INT_MORA_NO_PAGADOS_EN_PLAZO,
(SELECT SUM(DOC.monto)
FROM nuc_item_doc DOC, eco_encargo_cob eco, nuc_documento docu
WHERE DOC.id_cargo IN (148 ,150, 151 , 152, 153, 157)
AND DOC.id_servicio = eco.id_servicio
AND DOC.id_documento = docu.id_documento
AND DOC.saldo > 0
AND docu.fec_vencimiento < sysdate) AS INT_MORA_NO_PAGADOS_EN_MORA,
(SELECT SUM(DOC.monto)
FROM nuc_item_doc DOC, eco_encargo_cob eco
WHERE DOC.id_cargo IN (148 ,150, 151 , 152, 153, 157)
AND DOC.id_servicio = eco.id_servicio
AND DOC.saldo <= 0 )
AS INT_MORA_PAGADOS,
(SELECT SUM(DOC.monto)
FROM nuc_item_doc DOC,eco_encargo_cob eco, nuc_cargo cargo
WHERE DOC.id_cargo = cargo.id_cargo
AND cargo.id_tip_cargo = 1045
AND DOC.id_servicio = eco.id_servicio) AS IMPUESTOS_FACTURADOS,
(SELECT SUM(DOC.monto)
FROM nuc_item_doc DOC,eco_encargo_cob eco, nuc_cargo cargo, nuc_documento docu
WHERE DOC.id_cargo = cargo.id_cargo
AND cargo.id_tip_cargo = 1045
AND DOC.saldo > 0
AND docu.fec_vencimiento > = sysdate
AND DOC.id_servicio = eco.id_servicio) AS IMP_FACT_NO_PAGADOS_EN_PLAZO,
(SELECT SUM(DOC.monto)
FROM nuc_item_doc DOC,eco_encargo_cob eco, nuc_cargo cargo, nuc_documento docu
WHERE DOC.id_cargo = cargo.id_cargo
                              AND cargo.id_tip_cargo = 1045
                               AND DOC.saldo > 0
                              AND docu.fec_vencimiento < sysdate
                               AND DOC.id_servicio = eco.id_servicio) AS IMP_FACT_NO_PAGADOS_EN_MORA,
                                (SELECT SUM(DOC.monto)
                                FROM nuc_item_doc DOC,eco_encargo_cob eco, nuc_cargo cargo
                                WHERE DOC.id_cargo = cargo.id_cargo
                                AND cargo.id_tip_cargo = 1045
                                 AND DOC.id_servicio = eco.id_servicio
                                AND DOC.saldo < = 0 ) AS IMPUESTO_PAGADO,
                                  (SELECT decode((sysdate -MIN(docu.fec_vencimiento)) -ABS(sysdate -MIN(docu.fec_vencimiento)),    0,    to_number(TRUNC(sysdate -MIN(docu.fec_vencimiento))),    0)
                                   FROM nuc_item_doc DOC, eco_encargo_cob eco, nuc_documento docu
                                  WHERE eco.id_servicio = DOC.id_servicio
                                  AND DOC.id_documento = docu.id_documento
                                  AND DOC.saldo > 0)
                                  AS DIAS_DE_RETRASO,
                                      est.descripcion estrato,    se.id_categoria clase,    se.id_subcategoria subclase,    morosidad.f_sucursal sucursal,    morosidad.f_zona zona,    morosidad.f_ciclo ciclo,    morosidad.f_grupo grupo
                                   FROM nuc_servicio ser,    eco_encargo_cob eco,    eco_clases_eco clas,    eco_plan_neg pla,    eco_empresa_socio socio,    eco_rel_soc_claseco rela,    eco_linea_negocio lin,    nuc_cuenta cuenta,    eco_titular titu,    nuc_persona pers,    nuc_tip_doc_persona tdoc,    fin_tipo_financ tfin,    nuc_moneda mon,    nuc_coti_moneda coti,    wkf_workflow wkf,    nuc_cuota ncta,    fin_cuota fcta,    srv_electrico se,    nuc_servicio ser2,    srv_estratosoc est,    nuc_empresa emp
                                   WHERE ser.id_servicio = eco.id_servicio
                                   AND eco.id_plan_negocio = pla.id_plan_neg
                                   AND pla.id_rel_soc_claseco = rela.id_rel_soc_claseco
                                   AND rela.id_clases_eco = clas.id_clases_eco
                                   AND rela.id_empresa_socio = socio.id_empresa_socio
                                   AND lin.id_linea_negocio = clas.id_linea_negocio
                                   AND cuenta.id_cuenta = ser.id_cuenta
                                   AND ser.id_servicio = titu.id_servicio_eco
                                   AND titu.id_persona = pers.id_persona
                                   AND tdoc.id_tip_doc_persona = pers.id_tip_doc_persona
                                   AND tfin.id_tipo_financ = pla.id_tipo_financ
                                   AND tfin.id_tipo_magnitud = coti.id_coti_moneda
                                   AND coti.id_moneda = mon.id_moneda
                                   AND eco.id_workflow = wkf.id_workflow
                                   AND eco.id_financiacion = fcta.id_financiacion(+)
                                   AND ncta.id_cuota(+) = fcta.id_cuota
                                   AND nvl(ncta.numero_cuota,    0) < 2
                                   AND pla.id_plan_neg = eco.id_plan_negocio
                                   AND eco.id_servicio = ser.id_servicio
                                   AND ser2.id_cuenta = ser.id_cuenta
                                   AND ser2.tipo = 'ELECTRICO'
                                   AND se.id_servicio = ser2.id_servicio
                                   AND se.id_estratosoc = est.id_estratosoc
                                   AND emp.cod_partition = 'COD'
                                   AND ser.id_empresa = emp.id_empresa
                                   AND pla.id_empresa = emp.id_empresa
                                   AND ser2.id_empresa = emp.id_empresa
                                   AND wkf.id_empresa = emp.id_empresa
                                   AND morosidad.f_calcular_ruta(ser2.id_servicio) = 'S'
                                   ORDER BY ser.fec_inicio,    ser.nro_servicio
/
PROMPT CREACION DE VISTA ECO_COD_ECO_CARGA_VENTAS_V
CREATE OR REPLACE VIEW ECO_COD_ECO_CARGA_VENTAS_V (
  ID,
  ID_EMPRESA,
  FECHA_INICIO,
  NUMERO_ECO,
  COD_LIN_NEG,
  LIN_NEG,
  COD_PRODUCTO,
  PRODUCTO,
  COD_PLAN_NEG,
  PLAN_NEG,
  COD_SOCIO_NEG,
  SOCIO_NEG,
  NUM_CTA,
  NUM_ID_TIT,
  NOMBRE_TIT,
  COD_TIPO_DOC_TIT,
  TIPO_DOC_TIT,
  NUM_DOC_TIT,
  CARGO_CAP,
  MONEDA,
  NUM_CUOTAS,
  ESTADO,
  FEC_ULT_CAMB_EST,
  MOTIVO_FIN,
  VAL_CUOTA_ENC,
  VAL_TOTAL_ENC_COB,
  ESTRATO_SE,
  CLASE_SE,
  SUBCLASE_SE,
  SUCURSAL_RF,
  ZONA_RF,
  CICLO_RF,
  GRUPO_RF
) AS
SELECT
ROWNUM, EMP.ID_EMPRESA, SER.FEC_INICIO FECHA_CARGUE, SER.NRO_SERVICIO SERVICIO, LIN.ID_LINEA_NEGOCIO,LIN.DESCRIPCION LINEA,
CLAS.ID_CLASES_ECO, CLAS.DESCRIPTION PRODUCTO, PLA.ID_PLAN_NEG,PLA.DESCRIPCION PLAN, SOCIO.ID_EMPRESA_SOCIO,
SOCIO.DESCRIPTION SOCIO,
CUENTA.NRO_CUENTA, ECO.NRO_ID_VENTA ID_VENTA, PERS.NOMBRE TITULAR,
TDOC.ID_TIP_DOC_PERSONA,TDOC.COD_TIP_DOC_PERSONA TIPO_DOC, PERS.NRO_DOCTO_IDENT NRO_DOC,
CAR.DESCRIPCION CARGO_CAP, MON.DES_MONEDA MONEDA,
(SELECT DECODE(COUNT(ID_FINANCIACION),0,NULL,COUNT(ID_FINANCIACION))
FROM FIN_CUOTA CUOT WHERE CUOT.ID_FINANCIACION = ECO.ID_FINANCIACION) AS NUM_CUOTAS,
DECODE(WKF.ID_STATE,'Eliminado','Finalizado','Finalizado','Terminado',WKF.ID_STATE) ESTADO,
(SELECT MAX(AUDI.FECHA_EJECUCION)
FROM FWK_AUDITEVENT AUDI
WHERE (AUDI.USECASE = 'Eliminar'
       OR AUDI.USECASE = 'Caducar'
       OR AUDI.USECASE = 'Finalizar'
       OR AUDI.USECASE = 'Terminar')
  AND AUDI.ID_FK = SER.ID_SERVICIO) ULTIMO_ESTADO,
MOTI.DESCRIPCION MOTIVO_FIN,
DECODE(NVL(NCTA.CAPITAL,0),0,ECO.MONTO_CUOTA,NCTA.CAPITAL) VALOR_CUOTA,
(SELECT DECODE(SUM (NCTA.CAPITAL) + SUM(FCTA.CAPITAL_NO_AFECTO_INTERES),
0, NULL,SUM (NCTA.CAPITAL) + SUM(FCTA.CAPITAL_NO_AFECTO_INTERES))
FROM FIN_CUOTA FCTA, NUC_CUOTA NCTA WHERE NCTA.ID_CUOTA = FCTA.ID_CUOTA
AND ECO.ID_FINANCIACION = FCTA.ID_FINANCIACION ) AS VALOR_ECO,
EST.DESCRIPCION ESTRATO, SE.ID_CATEGORIA CLASE, SE.ID_SUBCATEGORIA SUBCLASE,
MOROSIDAD.F_SUCURSAL SUCURSAL,
MOROSIDAD.F_ZONA ZONA,
MOROSIDAD.F_CICLO CICLO,
MOROSIDAD.F_GRUPO GRUPO
FROM
NUC_SERVICIO SER, ECO_ENCARGO_COB ECO, ECO_CLASES_ECO CLAS, ECO_PLAN_NEG PLA,
ECO_EMPRESA_SOCIO SOCIO, ECO_REL_SOC_CLASECO RELA, ECO_LINEA_NEGOCIO LIN,
NUC_CUENTA CUENTA, ECO_TITULAR TITU, NUC_PERSONA PERS, NUC_TIP_DOC_PERSONA TDOC,
ECO_MOTIVO_BAJA MOTI, FIN_TIPO_FINANC TFIN, NUC_CARGO CAR, NUC_MONEDA MON,
NUC_COTI_MONEDA COTI, WKF_WORKFLOW WKF, NUC_CUOTA NCTA, FIN_CUOTA FCTA, SRV_ELECTRICO SE,
NUC_SERVICIO SER2, SRV_ESTRATOSOC EST, NUC_EMPRESA EMP
WHERE
SER.ID_SERVICIO = ECO.ID_SERVICIO
AND
ECO.ID_PLAN_NEGOCIO = PLA.ID_PLAN_NEG
AND
PLA.ID_REL_SOC_CLASECO = RELA.ID_REL_SOC_CLASECO
AND
RELA.ID_CLASES_ECO = CLAS.ID_CLASES_ECO
AND
RELA.ID_EMPRESA_SOCIO = SOCIO.ID_EMPRESA_SOCIO
AND
LIN.ID_LINEA_NEGOCIO = CLAS.ID_LINEA_NEGOCIO
AND
CUENTA.ID_CUENTA = SER.ID_CUENTA
AND
SER.ID_SERVICIO = TITU.ID_SERVICIO_ECO
AND
TITU.ID_PERSONA = PERS.ID_PERSONA
AND
TDOC.ID_TIP_DOC_PERSONA = PERS.ID_TIP_DOC_PERSONA
AND
TFIN.ID_TIPO_FINANC = PLA.ID_TIPO_FINANC
AND
CAR.ID_CARGO = TFIN.ID_CARGO_CAPITAL
AND
ECO.ID_MOTIVO_BAJA = MOTI.ID_MOTIVO_BAJA (+)
AND
TFIN.ID_TIPO_MAGNITUD = COTI.ID_COTI_MONEDA
AND
COTI.ID_MONEDA = MON.ID_MONEDA
AND
ECO.ID_WORKFLOW = WKF.ID_WORKFLOW
AND
ECO.ID_FINANCIACION = FCTA.ID_FINANCIACION(+)
AND
NCTA.ID_CUOTA(+) = FCTA.ID_CUOTA
AND
NVL(NCTA.NUMERO_CUOTA,0)  < 2
AND PLA.ID_PLAN_NEG = ECO.ID_PLAN_NEGOCIO
AND ECO.ID_SERVICIO = SER.ID_SERVICIO
AND SER2.ID_CUENTA = SER.ID_CUENTA
AND SER2.TIPO      = 'ELECTRICO'
AND SE.ID_SERVICIO = SER2.ID_SERVICIO
AND SE.ID_ESTRATOSOC = EST.ID_ESTRATOSOC
AND EMP.COD_PARTITION = 'COD'
AND SER.ID_EMPRESA = EMP.ID_EMPRESA
AND PLA.ID_EMPRESA = EMP.ID_EMPRESA
AND SER2.ID_EMPRESA = EMP.ID_EMPRESA
AND WKF.ID_EMPRESA = EMP.ID_EMPRESA
AND MOROSIDAD.F_CALCULAR_RUTA(SER2.ID_SERVICIO) = 'S'
ORDER BY SER.FEC_INICIO, SER.NRO_SERVICIO

/
PROMPT CREACION DE VISTA ECO_COD_ECO_LINEA_NEG_COMBO_V
CREATE OR REPLACE VIEW ECO_COD_ECO_LINEA_NEG_COMBO_V (
ID,
ID_EMPRESA,
CODIGO,
DESCRIPCION
)
AS SELECT
T01.ID_LINEA_NEGOCIO,
T01.ID_EMPRESA,
T01.ID_LINEA_NEGOCIO,
T01.DESCRIPCION
FROM ECO_LINEA_NEGOCIO T01
WHERE T01.ACTIVO = 'S'

/
PROMPT CREACION DE VISTA ECO_COD_ECO_SOCIO_NEG_COMBO_V
CREATE OR REPLACE VIEW ECO_COD_ECO_SOCIO_NEG_COMBO_V (
ID,
ID_EMPRESA,
CODIGO,
DESCRIPCION
)
AS SELECT
T01.ID_EMPRESA_SOCIO,
T01.ID_EMPRESA,
T01.ID_EMPRESA_SOCIO,
T01.DESCRIPTION
FROM ECO_EMPRESA_SOCIO T01
WHERE T01.ACTIVE = 'S'

/
PROMPT CREACION DE VISTA ECO_COD_ECO_PRODUCTO_COMBO_V
CREATE OR REPLACE VIEW ECO_COD_ECO_PRODUCTO_COMBO_V (
ID,
ID_EMPRESA,
CODIGO,
DESCRIPCION
)
AS SELECT
T01.ID_CLASES_ECO,
T01.ID_EMPRESA,
T01.ID_CLASES_ECO,
T01.DESCRIPTION
FROM ECO_CLASES_ECO T01
WHERE T01.ACTIVE = 'S'

/
PROMPT CREACION DE VISTA ECO_COD_ECO_PLAN_COMBO_V
CREATE OR REPLACE VIEW ECO_COD_ECO_PLAN_COMBO_V (
ID,
ID_EMPRESA,
CODIGO,
DESCRIPCION
)
AS SELECT
T01.ID_PLAN_NEG,
T01.ID_EMPRESA,
T01.ID_PLAN_NEG,
T01.DESCRIPCION
FROM ECO_PLAN_NEG T01

/
PROMPT CREACION DE VISTA ECO_COD_ECO_ESTADO_ENC_COMBO_V
CREATE OR REPLACE VIEW ECO_COD_ECO_ESTADO_ENC_COMBO_V (
ID,
ID_EMPRESA,
CODIGO,
DESCRIPCION
) AS
((SELECT
1000,
1053,
'Activo',
'Activo'
FROM DUAL)
UNION (SELECT
1001,
1053,
'Finalizado',
'Finalizado'
FROM DUAL)
UNION(SELECT
1002,
1053,
'Caducado',
'Caducado'
FROM DUAL)
UNION(SELECT
1003,
1053,
'Terminado',
'Terminado'
FROM DUAL)
UNION(SELECT
2000,
1546,
'Activo',
'Activo'
FROM DUAL)
UNION (SELECT
2001,
1546,
'Finalizado',
'Finalizado'
FROM DUAL)
UNION(SELECT
2002,
1546,
'Caducado',
'Caducado'
FROM DUAL)
UNION(SELECT
2003,
1546,
'Terminado',
'Terminado'
FROM DUAL))

/
PROMPT CREACION DE VISTA ECO_COD_ECO_TIPO_DOC_COMBO_V
CREATE OR REPLACE VIEW ECO_COD_ECO_TIPO_DOC_COMBO_V (
ID,
ID_EMPRESA,
CODIGO,
DESCRIPCION
)
AS SELECT
T01.ID_TIP_DOC_PERSONA,
T01.ID_EMPRESA,
T01.ID_TIP_DOC_PERSONA,
T01.COD_TIP_DOC_PERSONA
FROM NUC_TIP_DOC_PERSONA T01
/