-- CU045
SELECT
  linea.id_linea_negocio,
  linea.descripcion,
  socio.id_empresa_socio,
  socio.description,
  clase.id_clases_eco,
  clase.description,
  plan_neg.id_plan_neg,
  plan_neg.descripcion,
  cuenta.nro_cuenta,
  servicio.nro_servicio,
  eco.nro_id_venta,
  tdoc.id_tip_doc_persona,
  tdoc.cod_tip_doc_persona,
  persona.nro_docto_ident,
  -- Numero de ultima factura
  (
    SELECT docu.nro_documento
    FROM nuc_documento docu, eco_encargo_cob eco, nuc_item_doc doc
    WHERE eco.id_servicio = doc.id_servicio
    AND doc.id_documento = docu.id_documento
    AND doc.saldo <= 0
   ) AS nro_fact,
  -- De acuerdo a 10 ir con el id_item_doc
  (
    SELECT lote.fecha_pago
    FROM rec_lote_pago lote, nuc_item_doc doc, rec_item_pago pago, rec_pago rpago, eco_encargo_cob eco
    WHERE doc.id_item_doc = pago.id_item_doc
    AND rpago.id_pago = pago.id_pago
    AND rpago.id_lote = lote.id_lote
    AND doc.id_servicio = eco.id_servicio
    AND doc.saldo <= 0
  ) AS fec_real_pago,
  -- De acuerdo a 10 ir con el id_item_doc
  (
    SELECT lote.fecha_proceso
    FROM rec_lote_pago lote, nuc_item_doc doc, rec_item_pago pago, rec_pago rpago, eco_encargo_cob eco
    WHERE doc.id_item_doc = pago.id_item_doc
    AND rpago.id_pago = pago.id_pago
    AND rpago.id_lote = lote.id_lote
    AND doc.id_servicio = eco.id_servicio
    AND doc.saldo <= 0
  ) AS fec_proceso,
  -- De acuerdo a 10 ir con el id_item_doc                     
  (
    SELECT DECODE(COUNT(id_financiacion), 0, NULL, COUNT(id_financiacion)) 
    FROM fin_cuota cuota
    WHERE cuota.id_financiacion = eco.id_financiacion
  ) AS num_cuotas,
  -- Cantidad de cuotas recaudadas
  (
    SELECT COUNT (ipago.id_pago)
    FROM nuc_item_doc doc, eco_encargo_cob eco, nuc_cargo cargo, rec_item_pago ipago
    WHERE doc.id_cargo = cargo.id_cargo
    AND cargo.id_tip_cargo = 1051
    AND doc.id_servicio = eco.id_servicio
    AND doc.saldo <= 0 
    AND doc.id_item_doc = ipago.id_item_doc
  ) AS cant_ctas_rec,
  -- Valor de cuota ECO
  (
    SELECT DECODE(SUM (ncta.capital) + SUM(fcta.capital_no_afecto_interes), 0, NULL,SUM (ncta.capital) + SUM(fcta.capital_no_afecto_interes))
    FROM eco_encargo_cob eco, fin_cuota fcta, nuc_cuota ncta WHERE ncta.id_cuota = fcta.id_cuota
    AND eco.id_financiacion = fcta.id_financiacion
  ) AS valor_cuota,
  -- Valor total cuotas recaudadas
  (
    SELECT SUM (ipago.monto)
    FROM nuc_item_doc doc, eco_encargo_cob eco, nuc_cargo cargo, rec_item_pago ipago
    WHERE doc.id_cargo = cargo.id_cargo
    AND cargo.id_tip_cargo = 1051
    AND doc.id_servicio = eco.id_servicio
    AND doc.saldo <= 0
    AND doc.id_item_doc = ipago.id_item_doc
   ) AS valor_ctas_rec
FROM
  eco_linea_negocio linea,
  eco_empresa_socio socio,
  eco_clases_eco clase,
  eco_plan_neg plan_neg,
  nuc_cuenta cuenta,
  nuc_servicio servicio,
  eco_encargo_cob eco,
  nuc_tip_doc_persona tdoc,
  nuc_persona persona;