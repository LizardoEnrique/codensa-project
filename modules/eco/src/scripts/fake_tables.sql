-- CUECO025
CREATE TABLE ZZ_CUECO025 (
  id                  NUMBER(18) NOT NULL,
  id_empresa          NUMBER(18) NOT NULL,
  lin_neg             VARCHAR2(50) NOT NULL,
  socio_neg           VARCHAR2(50) NOT NULL,
  producto            VARCHAR2(50) NOT NULL,
  plan_neg            VARCHAR2(50) NOT NULL,
  fecha_cargue        DATE NOT NULL,
  num_servicio        NUMBER(10) NOT NULL,
  num_id_vta          NUMBER(10) NOT NULL,
  num_cta             NUMBER(10) NOT NULL,
  tipo_doc_tit        VARCHAR2(3) NOT NULL,
  num_doc_tit         VARCHAR2(50) NOT NULL,
  nombre_tit          VARCHAR2(50) NOT NULL,
  val_enc_cob         NUMBER(19,4) NOT NULL,
  num_cuotas_enc_cob  NUMBER(10) NOT NULL,
  val_cuota_enc_cob   NUMBER(19,4) NOT NULL,
  cargo_cap           NUMBER(19,4) NOT NULL,
  fec_ult_camb_est    DATE NOT NULL,
  estado              VARCHAR2(50) NOT NULL,
  motivo_fin          VARCHAR2(50) NOT NULL,
  estrato_se          VARCHAR2(50) NOT NULL,
  clase_se            VARCHAR2(50) NOT NULL,
  subclase_se         VARCHAR2(50) NOT NULL,
  sucursal_rf         VARCHAR2(50) NOT NULL,
  zona_rf             VARCHAR2(50) NOT NULL,
  ciclo_rf            VARCHAR2(50) NOT NULL,
  grupo_rf            VARCHAR2(50) NOT NULL
)

ALTER TABLE ZZ_CUECO025
  ADD CONSTRAINT ZZ_ECO025_PK PRIMARY KEY (
    id
  )
  USING INDEX

INSERT INTO zz_cuecO025 (
         id, id_empresa, lin_neg, socio_neg, producto, plan_neg,
         fecha_cargue, num_servicio, num_id_vta, num_cta, tipo_doc_tit,
         num_doc_tit, nombre_tit, val_enc_cob, num_cuotas_enc_cob,
         val_cuota_enc_cob, cargo_cap, fec_ult_camb_est, estado,
         motivo_fin, estrato_se, clase_se, subclase_se, sucursal_rf,
         zona_rf, ciclo_rf, grupo_rf)
VALUES ( 1, 1000, 'linea de negocio', 'socio de negocio', 'producto',
        'plan de negocio', '2007-02-26', 1, 2, 13000, 'dni', '123456789',
        'victor', 1000, 10, 100, 1200, '2007-06-12', 'estado', 'motivo',
        'estrato', 'clase', 'subclase', 'sucursal', 'zona', 'ciclo', 'grupo');

INSERT INTO zz_cuecO025 (
         id, id_empresa, lin_neg, socio_neg, producto, plan_neg,
         fecha_cargue, num_servicio, num_id_vta, num_cta, tipo_doc_tit,
         num_doc_tit, nombre_tit, val_enc_cob, num_cuotas_enc_cob,
         val_cuota_enc_cob, cargo_cap, fec_ult_camb_est, estado,
         motivo_fin, estrato_se, clase_se, subclase_se, sucursal_rf,
         zona_rf, ciclo_rf, grupo_rf)
VALUES ( 2, 1053, 'linea ', 'socio', 'producto de negocio',
        'plan', '2007-02-28', 2, 3, 130238, 'dni', '12356498',
        'raul sanz bla', 1020, 12, 120, 1212, '2007-06-25', 'estado1', 'motivo1',
        'estrato1', 'clase1', 'subclase1', 'sucursal1', 'zona1', 'ciclo1', 'grupo1');