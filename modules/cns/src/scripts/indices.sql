
-- clientes aic atenciones 
CREATE INDEX AIC_CONTACTO_DIST_IX1 ON AIC_CONTACTO_DIST (ID_CUENTA);

-- clientes aic temas contactos 
CREATE INDEX AIC_TEM_CONT_DIST_IX1 ON AIC_TEM_CONT_DIST(ID_CONTACTO_DIST);

-- clientes iac observaciones
CREATE INDEX AIC_OBS_AT_DIST_IX1 ON AIC_OBS_AT_DIST(ID_ATENCION);

--indices para performance de consulta de cuentas
CREATE INDEX NASPERF_NUCITEMDOC_IDX1 ON NUC_ITEM_DOC 
(ID_SERVICIO) 
LOGGING 
TABLESPACE SYNDESA 
PCTFREE 10 
INITRANS 2 
MAXTRANS 255 
STORAGE ( 
            INITIAL 64K 
            MINEXTENTS 1 
            MAXEXTENTS 2147483645 
            PCTINCREASE 0 
            BUFFER_POOL DEFAULT 
           ) 
NOPARALLEL; 

--indices para performance de consulta de cuentas
CREATE INDEX NASPERF_NUCSERVICIO_IDX1 ON NUC_SERVICIO 
(ID_CUENTA) 
LOGGING 
TABLESPACE SYNDESA 
PCTFREE 10 
INITRANS 2 
MAXTRANS 255 
STORAGE ( 
            INITIAL 64K 
            MINEXTENTS 1 
            MAXEXTENTS 2147483645 
            PCTINCREASE 0 
            BUFFER_POOL DEFAULT 
           ) 
NOPARALLEL; 

--indices para performance de consulta de cuentas
CREATE INDEX NASPERF_REPCONFIGREP_IDX1 ON REP_CONFIG_REP( ID_SERVICIO);

--indices para performance de consulta de cuentas
CREATE INDEX NASPERF_SRVELECTRICO_IDX1 ON SRV_ELECTRICO(ID_ESTRATOSOC);
