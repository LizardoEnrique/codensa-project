/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaComprobantePago;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author syalri
 *
 */
public class ConsultaComprobantePagoImpl extends SynergiaBusinessObjectImpl implements ConsultaComprobantePago {
	
	private static final long serialVersionUID = 1L;
	
	private String tipoServicio;
	private Long nroServicio;
	private String tipoComprobante;
	private Long nroComprobante;
	private Long valorComprobante;
	private Date fechaHoraEmision;
	private Long nroCuenta;
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaComprobantePago#getFechaHoraEmicion()
	 */
	public Date getFechaHoraEmision() {
		return fechaHoraEmision;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaComprobantePago#setFechaHoraEmicion(java.util.Date)
	 */
	public void setFechaHoraEmision(Date fechaHoraEmision) {
		this.fechaHoraEmision = fechaHoraEmision;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaComprobantePago#getNroComprobante()
	 */
	public Long getNroComprobante() {
		return nroComprobante;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaComprobantePago#setNroComprobante(java.lang.Long)
	 */
	public void setNroComprobante(Long nroComprobante) {
		this.nroComprobante = nroComprobante;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaComprobantePago#getNroServicio()
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaComprobantePago#setNroServicio(java.lang.Long)
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaComprobantePago#getTipoComprobante()
	 */
	public String getTipoComprobante() {
		return tipoComprobante;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaComprobantePago#setTipoComprobante(java.lang.String)
	 */
	public void setTipoComprobante(String tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaComprobantePago#getTipoServicio()
	 */
	public String getTipoServicio() {
		return tipoServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaComprobantePago#setTipoServicio(java.lang.String)
	 */
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaComprobantePago#getValorComprobante()
	 */
	public Long getValorComprobante() {
		return valorComprobante;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaComprobantePago#setValorComprobante(java.lang.Long)
	 */
	public void setValorComprobante(Long valorComprobante) {
		this.valorComprobante = valorComprobante;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}   

}
