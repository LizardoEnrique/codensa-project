package com.synapsis.cns.codensa.model.impl;

import java.util.Set;

import com.synapsis.cns.codensa.model.ProductoClasificadosCodensa;

/**
 * Producto Clasificados Codensa para mostrar en el Reporte de Impresion de Anexo de Factura (CNS003)
 * 
 * @author jhack
 */
public class ProductoClasificadosCodensaImpl extends ServicioFinancieroECOImpl implements ProductoClasificadosCodensa {
	private static final long serialVersionUID = 1L;
	
	// Items
	Set itemsClasificadosCodensa;

	public Set getItemsClasificadosCodensa() {
		return itemsClasificadosCodensa;
	}

	public void setItemsClasificadosCodensa(Set itemsClasificadosCodensa) {
		this.itemsClasificadosCodensa = itemsClasificadosCodensa;
	}

}