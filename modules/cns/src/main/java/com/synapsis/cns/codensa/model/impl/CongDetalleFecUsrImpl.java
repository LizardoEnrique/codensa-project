package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.cns.codensa.model.CongDetalleFecUsr;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class CongDetalleFecUsrImpl extends SynergiaBusinessObjectImpl implements
		CongDetalleFecUsr {

	private Long nroCuenta;
	
	private Long nroSaldo;

	private String usuario;
	
	private String usuarioNombre;
	
	private Date fechaIntervencion;
	
	private String horaIntervencion;
	
	private String estado;
	
	private String tiempoIntervencionUsuario;
	
	private Long idOrden;
	
	//nuevo campo acta 4
	private String observacion;

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the usuarioNombre
	 */
	public String getUsuarioNombre() {
		return usuarioNombre;
	}

	/**
	 * @param usuarioNombre the usuarioNombre to set
	 */
	public void setUsuarioNombre(String usuarioNombre) {
		this.usuarioNombre = usuarioNombre;
	}

	/**
	 * @return the fechaIntervencion
	 */
	public Date getFechaIntervencion() {
		return fechaIntervencion;
	}

	/**
	 * @param fechaIntervencion the fechaIntervencion to set
	 */
	public void setFechaIntervencion(Date fechaIntervencion) {
		this.fechaIntervencion = fechaIntervencion;
	}

	/**
	 * @return the horaIntervencion
	 */
	public String getHoraIntervencion() {
		return horaIntervencion;
	}

	/**
	 * @param horaIntervencion the horaIntervencion to set
	 */
	public void setHoraIntervencion(String horaIntervencion) {
		this.horaIntervencion = horaIntervencion;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the tiempoIntervencionUsuario
	 */
	public String getTiempoIntervencionUsuario() {
		return tiempoIntervencionUsuario;
	}

	/**
	 * @param tiempoIntervencionUsuario the tiempoIntervencionUsuario to set
	 */
	public void setTiempoIntervencionUsuario(String tiempoIntervencionUsuario) {
		this.tiempoIntervencionUsuario = tiempoIntervencionUsuario;
	}

	/**
	 * @return the observacion
	 */
	public String getObservacion() {
		return observacion;
	}

	/**
	 * @param observacion the observacion to set
	 */
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	/**
	 * @return the nroSaldo
	 */
	public Long getNroSaldo() {
		return nroSaldo;
	}

	/**
	 * @param nroSaldo the nroSaldo to set
	 */
	public void setNroSaldo(Long nroSaldo) {
		this.nroSaldo = nroSaldo;
	}

	public Long getIdOrden() {
		return idOrden;
	}

	public void setIdOrden(Long idOrden) {
		this.idOrden = idOrden;
	}

	
	

}