package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.DatosTitular;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class DatosTitularImpl extends SynergiaBusinessObjectImpl implements
		DatosTitular {
	private static final long serialVersionUID = 1L;

	/* leyenda 1 */
	private Long nroCuenta;

	private Long nroServicio;

	private String nombreB;

	private String documentoB;

	private String telefonoB;

	private String email;

	private String rutaLec;

	private String dirPredio;

	private String distrito;

	private String dirDomicilio;

	private Long idPersona;
	
	private String barrioLocalizacion;

	/**
	 * @return the idPersona
	 */
	public Long getIdPersona() {
		return idPersona;
	}

	/**
	 * @param idPersona the idPersona to set
	 */
	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	/**
	 * @return the dirDomicilio
	 */
	public String getDirDomicilio() {
		return dirDomicilio;
	}

	/**
	 * @param dirDomicilio
	 *            the dirDomicilio to set
	 */
	public void setDirDomicilio(String dirDomicilio) {
		this.dirDomicilio = dirDomicilio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#getDirPredio()
	 */
	public String getDirPredio() {
		return dirPredio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#setDirPredio(java.lang.String)
	 */
	public void setDirPredio(String dirPredio) {
		this.dirPredio = dirPredio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#getDistrito()
	 */
	public String getDistrito() {
		return distrito;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#setDistrito(java.lang.String)
	 */
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#getDocumentoB()
	 */
	public String getDocumentoB() {
		return documentoB;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#setDocumentoB(java.lang.String)
	 */
	public void setDocumentoB(String documentoB) {
		this.documentoB = documentoB;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#getEmail()
	 */
	public String getEmail() {
		return email;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#setEmail(java.lang.String)
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#getNombreB()
	 */
	public String getNombreB() {
		return nombreB;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#setNombreB(java.lang.String)
	 */
	public void setNombreB(String nombreB) {
		this.nombreB = nombreB;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#getNroServicio()
	 */
	public Long getNroServicio() {
		return nroServicio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#setNroServicio(java.lang.Long)
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#getRutaLec()
	 */
	public String getRutaLec() {
		return rutaLec;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#setRutaLec(java.lang.String)
	 */
	public void setRutaLec(String rutaLec) {
		this.rutaLec = rutaLec;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#getTelefonoB()
	 */
	public String getTelefonoB() {
		return telefonoB;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#setTelefonoB(java.lang.String)
	 */
	public void setTelefonoB(String telefonoB) {
		this.telefonoB = telefonoB;
	}

	public String getBarrioLocalizacion() {
		return barrioLocalizacion;
	}

	public void setBarrioLocalizacion(String barrioLocalizacion) {
		this.barrioLocalizacion = barrioLocalizacion;
	}
}
