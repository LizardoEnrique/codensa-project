package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface ConsultaHistoriaEstados {

	/**
	 * @return the estado
	 */
	public String getEstado();

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado);

	/**
	 * @return the fechaModif
	 */
	public Date getFechaModif();

	/**
	 * @param fechaModif the fechaModif to set
	 */
	public void setFechaModif(Date fechaModif);

	/**
	 * @return the usuario
	 */
	public String getUsuario();

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario);

}