package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaServiciosElectricosByNroTransformador;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaServiciosElectricosByNroTransformadorImpl extends SynergiaBusinessObjectImpl implements ConsultaServiciosElectricosByNroTransformador {


	private Long servicio;
	private String transformador;
	private Long cuenta;
	private String titularCuenta;
	private String tipoServicio;
	private String mercado;
	private String estado;
	private String rutaFacturacion;
	private String direccion;
	private String municipio;
	private String tarifa;
	private String fecAsociacion;
	private Date fecDesasociacion;	
	private Date fechaProceso;

	
	public String getTransformador() {
		return transformador;
	}
	public void setTransformador(String transformador) {
		this.transformador = transformador;
	}
	public Long getCuenta() {
		return cuenta;
	}
	public void setCuenta(Long cuenta) {
		this.cuenta = cuenta;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getFecAsociacion() {
		return fecAsociacion;
	}
	public void setFecAsociacion(String fecAsociacion) {
		this.fecAsociacion = fecAsociacion;
	}
	public String getMercado() {
		return mercado;
	}
	public void setMercado(String mercado) {
		this.mercado = mercado;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getRutaFacturacion() {
		return rutaFacturacion;
	}
	public void setRutaFacturacion(String rutaFacturacion) {
		this.rutaFacturacion = rutaFacturacion;
	}
	public Long getServicio() {
		return servicio;
	}
	public void setServicio(Long servicio) {
		this.servicio = servicio;
	}
	public String getTarifa() {
		return tarifa;
	}
	public void setTarifa(String tarifa) {
		this.tarifa = tarifa;
	}
	public String getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	public String getTitularCuenta() {
		return titularCuenta;
	}
	public void setTitularCuenta(String titularCuenta) {
		this.titularCuenta = titularCuenta;
	}
	public Date getFechaProceso() {
		return fechaProceso;
	}
	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}
	public Date getFecDesasociacion() {
		return fecDesasociacion;
	}
	public void setFecDesasociacion(Date fecDesasociacion) {
		this.fecDesasociacion = fecDesasociacion;
	}
}
