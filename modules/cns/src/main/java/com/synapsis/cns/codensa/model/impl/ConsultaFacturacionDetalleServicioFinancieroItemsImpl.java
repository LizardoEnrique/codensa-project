/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaFacturacionDetalleServicioConvenioItems;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 * 
 */
public class ConsultaFacturacionDetalleServicioFinancieroItemsImpl extends
SynergiaBusinessObjectImpl implements ConsultaFacturacionDetalleServicioConvenioItems {
	

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Date periodoFact;
	private String codItem;
	private String descItem;
	private Long valFacItem;
	private Long idServicio;
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the periodoFact
	 */
	public Date getPeriodoFact() {
		return periodoFact;
	}
	/**
	 * @param periodoFact the periodoFact to set
	 */
	public void setPeriodoFact(Date periodoFact) {
		this.periodoFact = periodoFact;
	}
	/**
	 * @return the codItem
	 */
	public String getCodItem() {
		return codItem;
	}
	/**
	 * @param codItem the codItem to set
	 */
	public void setCodItem(String codItem) {
		this.codItem = codItem;
	}
	/**
	 * @return the descItem
	 */
	public String getDescItem() {
		return descItem;
	}
	/**
	 * @param descItem the descItem to set
	 */
	public void setDescItem(String descItem) {
		this.descItem = descItem;
	}
	/**
	 * @return the valFacItem
	 */
	public Long getValFacItem() {
		return valFacItem;
	}
	/**
	 * @param valFacItem the valFacItem to set
	 */
	public void setValFacItem(Long valFacItem) {
		this.valFacItem = valFacItem;
	}
	public Long getIdServicio() {
		return idServicio;
	}
	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}
	
}