package com.synapsis.cns.codensa.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.suivant.arquitectura.core.queryFilter.SimpleQueryFilter;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.synapsis.synergia.presentation.model.NasServiceHelper;

/**
 * Clase que contiene el c�digo repetido que ten�an los BB de la Consulta Principal.<br>
 * Contiene una lista de resultados que ser� la Grilla y un objectResult, que es el objeto que se encontr� en
 * la DB (el cual se a�ade a la lista)
 * 
 * @author egrande
 */
public class ConsultaPrincipalUniqueBB {

	private List listResults = new ArrayList();
	private Object objectResult;


	/**
	 * Busca mediante un findByCriteriaUnique, pero NO tira excepci�n si no encuentra el objeto.<br>
	 * <li>1) Tiro count(1) para saber si existe el objeto
	 * <li>2) Si existe, lo busco
	 * 
	 * @param serviceName
	 */
	protected void findObject(String serviceName) {
		try {
			SimpleQueryFilter qf = (SimpleQueryFilter) VariableResolverUtils
					.getObject("numeroCuentaQueryFilter");
			Long nroCuenta = (Long) qf.getAttributeValue();
			Map m = new HashMap();
			m.put("nroCuenta", nroCuenta);
			
			Boolean exists = (Boolean) NasServiceHelper.getResponse(
				serviceName, "exists",
				m);

			if (exists != null && exists.booleanValue()) {
				objectResult = NasServiceHelper.getResponse(
					serviceName, "findByCriteriaUnique", m);
				listResults.add(objectResult);				
			}

		} catch (Exception e) {
			objectResult = null;
			listResults.clear();
		}
	}
	
	public List getListResults() {
		return listResults;
	}


	public void setListResults(List listResults) {
		this.listResults = listResults;
	}


	public Object getObjectResult() {
		return objectResult;
	}

	public void setObjectResult(Object objectResult) {
		this.objectResult = objectResult;
	}
}
