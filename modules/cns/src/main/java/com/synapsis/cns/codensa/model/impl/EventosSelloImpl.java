package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.EventosSello;

/**
 * 
 * @author dBraccio
 *29/11/2006
 */
public class EventosSelloImpl extends SynergiaBusinessObjectImpl 
	implements EventosSello {
    // Fields    
	
   
	private static final long serialVersionUID = 1L;
	
	private Long idComponente; //este luego no deber�a sea el id clave
    private Long nroCuenta;   
    private Long nroSello;
    private String colorSello;
    private String tipoSello;    
    private String estadoInstalacionSello;
    private Date fechaInstalacion;
    private Date fechaRetiro;
	private String marcaMedidor;
	private String modeloMedidor;
	private String numeroMedidor;
	private String localizacionSello;
	private String nombreArea;
	private String usuario;
	private String nombreInstalador;
	private String nombreContratistaInstalador;
	private String nombreRetiro;
	private String nombreContratistaRetiro;
	private String estadoSello;
	
	
	public String getColorSello() {
		return colorSello;
	}
	public void setColorSello(String colorSello) {
		this.colorSello = colorSello;
	}
	public String getEstadoInstalacionSello() {
		return estadoInstalacionSello;
	}
	public void setEstadoInstalacionSello(String estadoInstalacionSello) {
		this.estadoInstalacionSello = estadoInstalacionSello;
	}
	public String getEstadoSello() {
		return estadoSello;
	}
	public void setEstadoSello(String estadoSello) {
		this.estadoSello = estadoSello;
	}
	public Date getFechaInstalacion() {
		return fechaInstalacion;
	}
	public void setFechaInstalacion(Date fechaInstalacion) {
		this.fechaInstalacion = fechaInstalacion;
	}
	public Date getFechaRetiro() {
		return fechaRetiro;
	}
	public void setFechaRetiro(Date fechaRetiro) {
		this.fechaRetiro = fechaRetiro;
	}
	public Long getIdComponente() {
		return idComponente;
	}
	public void setIdComponente(Long idComponente) {
		this.idComponente = idComponente;
	}
	
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	
	public String getLocalizacionSello() {
		return localizacionSello;
	}
	public void setLocalizacionSello(String localizacionSello) {
		this.localizacionSello = localizacionSello;
	}
	public String getMarcaMedidor() {
		return marcaMedidor;
	}
	public void setMarcaMedidor(String marcaMedidor) {
		this.marcaMedidor = marcaMedidor;
	}
	public String getModeloMedidor() {
		return modeloMedidor;
	}
	public void setModeloMedidor(String modeloMedidor) {
		this.modeloMedidor = modeloMedidor;
	}
	public String getNombreArea() {
		return nombreArea;
	}
	public void setNombreArea(String nombreArea) {
		this.nombreArea = nombreArea;
	}
	public String getNombreContratistaInstalador() {
		return nombreContratistaInstalador;
	}
	public void setNombreContratistaInstalador(String nombreContratistaInstalador) {
		this.nombreContratistaInstalador = nombreContratistaInstalador;
	}
	public String getNombreContratistaRetiro() {
		return nombreContratistaRetiro;
	}
	public void setNombreContratistaRetiro(String nombreContratistaRetiro) {
		this.nombreContratistaRetiro = nombreContratistaRetiro;
	}
	public String getNombreInstalador() {
		return nombreInstalador;
	}
	public void setNombreInstalador(String nombreInstalador) {
		this.nombreInstalador = nombreInstalador;
	}
	public String getNombreRetiro() {
		return nombreRetiro;
	}
	public void setNombreRetiro(String nombreRetiro) {
		this.nombreRetiro = nombreRetiro;
	}
	public Long getNroSello() {
		return nroSello;
	}
	public void setNroSello(Long nroSello) {
		this.nroSello = nroSello;
	}
	public String getNumeroMedidor() {
		return numeroMedidor;
	}
	public void setNumeroMedidor(String numeroMedidor) {
		this.numeroMedidor = numeroMedidor;
	}
	public String getTipoSello() {
		return tipoSello;
	}
	public void setTipoSello(String tipoSello) {
		this.tipoSello = tipoSello;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
   
}
