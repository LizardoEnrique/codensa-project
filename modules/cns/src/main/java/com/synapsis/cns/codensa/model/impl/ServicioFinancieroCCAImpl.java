package com.synapsis.cns.codensa.model.impl;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.collections.Closure;
import org.apache.commons.collections.CollectionUtils;

import com.synapsis.cns.codensa.model.ItemServicioFinancieroCCA;
import com.synapsis.cns.codensa.model.ServicioFinancieroCCA;

/**
 * Producto Credito Facil Codensa para mostrar en el Reporte de Impresion de Anexo de Factura (CNS003)
 * 
 * @author jhack
 */
public class ServicioFinancieroCCAImpl extends ServicioFinancieroImpl implements ServicioFinancieroCCA{
	private static final long serialVersionUID = 1L;

	Long 	nroServicio;
	String 	titular;
	String  tipoProducto;
	
	// Items
	Set itemsCCA;

	public String getTitular() {
		// Obtenerlo de los items ...
		return titular;
	}
	public Long getNroServicio() {
		return nroServicio;
	}
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	public Set getItemsCCA() {
		return itemsCCA;
	}
	public void setItemsCCA(Set itemsCCA) {
		this.itemsCCA = itemsCCA;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	
	
	public BigDecimal getPagoMinimo(){
		// Lo obtenemos de los items ...
		BigDecimal result = new BigDecimal("0");
		Iterator itemsCCAIterator = itemsCCA.iterator();
		while (itemsCCAIterator.hasNext()){
			ItemServicioFinancieroCCA itemServicioFinancieroCCA = (ItemServicioFinancieroCCA) itemsCCAIterator.next();
			if (itemServicioFinancieroCCA.getPagoMinimo() != null){
				result.add(itemServicioFinancieroCCA.getPagoMinimo());
			}
		}		
		return result;
	}
	
	public BigDecimal getSaldoEnMora() {
		// Lo obtenemos de los items ...
		BigDecimal result = new BigDecimal("0");
		Iterator itemsCCAIterator = itemsCCA.iterator();
		while (itemsCCAIterator.hasNext()){
			ItemServicioFinancieroCCA itemServicioFinancieroCCA = (ItemServicioFinancieroCCA) itemsCCAIterator.next();
			if (itemServicioFinancieroCCA.getSaldoEnMora() != null){
				result.add(itemServicioFinancieroCCA.getSaldoEnMora());
			}
		}		
		return result;
	}
	
	public BigDecimal getInteresPorMora() {
		// Lo obtenemos de los items ...
		BigDecimal result = new BigDecimal("0");
		Iterator itemsCCAIterator = itemsCCA.iterator();
		while (itemsCCAIterator.hasNext()){
			ItemServicioFinancieroCCA itemServicioFinancieroCCA = (ItemServicioFinancieroCCA) itemsCCAIterator.next();
			if (itemServicioFinancieroCCA.getInteresPorMora() != null){
				result.add(itemServicioFinancieroCCA.getInteresPorMora());
			}
		}		
		return result;
	}
	
	public BigDecimal getPagoTotal() {
		// Lo obtenemos de los items ...
		BigDecimal result = new BigDecimal("0");
		Iterator itemsCCAIterator = itemsCCA.iterator();
		while (itemsCCAIterator.hasNext()){
			ItemServicioFinancieroCCA itemServicioFinancieroCCA = (ItemServicioFinancieroCCA) itemsCCAIterator.next();
			if (itemServicioFinancieroCCA.getPagoTotal() != null){
				result.add(itemServicioFinancieroCCA.getPagoTotal());
			}
		}		
		return result;
	}
	
	
	public Double getTasaInteres() {
		// Lo obtenemos de los items ...
		double result = 0;
		Iterator itemsCCAIterator = itemsCCA.iterator();
		while (itemsCCAIterator.hasNext()){
			ItemServicioFinancieroCCA itemServicioFinancieroCCA = (ItemServicioFinancieroCCA) itemsCCAIterator.next();
			if (itemServicioFinancieroCCA.getTasaInteres() != null){
				result = result + itemServicioFinancieroCCA.getTasaInteres().doubleValue();
			}
		}		
		return new Double(result);

	}
	
	
	public Long getCupoTotal() {
		// Lo obtenemos de los items ...
		long result = 0;
		Iterator itemsCCAIterator = itemsCCA.iterator();
		while (itemsCCAIterator.hasNext()){
			ItemServicioFinancieroCCA itemServicioFinancieroCCA = (ItemServicioFinancieroCCA) itemsCCAIterator.next();
			if (itemServicioFinancieroCCA.getCupoTotal() != null){
				result = result + itemServicioFinancieroCCA.getCupoTotal().longValue();
			}
		}		
		return new Long(result);
	}
	
	public Long getCupoDisponible() {
		// Lo obtenemos de los items ...
		long result = 0;
		Iterator itemsCCAIterator = itemsCCA.iterator();
		while (itemsCCAIterator.hasNext()){
			ItemServicioFinancieroCCA itemServicioFinancieroCCA = (ItemServicioFinancieroCCA) itemsCCAIterator.next();
			if (itemServicioFinancieroCCA.getCupoDisponible() != null){
				result = result + itemServicioFinancieroCCA.getCupoDisponible().longValue();
			}
		}		
		return new Long(result);
	}
	public String getTipoProducto() {
		return tipoProducto;
	}
	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}
}