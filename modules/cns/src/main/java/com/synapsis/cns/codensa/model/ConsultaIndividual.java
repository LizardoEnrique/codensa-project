package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.Empresa;
import com.synapsis.synergia.core.model.SynergiaBusinessObject;


public interface ConsultaIndividual extends SynergiaBusinessObject {

	public Long getId();
	public void setId(Long id);

	public Empresa getEmpresa();
	public void setEmpresa(Empresa empresa);

	public String getAccion();
	public void setAccion(String accion);
	
	public String getCiclo();
	public void setCiclo(String ciclo);
	
	public String getEstadoSrvElectrico();
	public void setEstadoSrvElectrico(String estadoSrvElectrico);
	
	public Date getFechaModificacion();
	public void setFechaModificacion(Date fechaModificacion);
	
	public String getIpTerminal();	
	public void setIpTerminal(String ipTerminal);
	
	public String getNombreCliente();
	public void setNombreCliente(String nombreCliente);
	
	public Long getNroCuenta();
	public void setNroCuenta(Long nroCuenta);
	
	public Long getNroSrvElectrico();
	public void setNroSrvElectrico(Long nroSrvElectrico);

	public Long getNroTransformador();
	public void setNroTransformador(Long nroTransformador);

	public String getRolRealizoModificacion();
	public void setRolRealizoModificacion(String rolRealizoModificacion);

	public String getSucursal();
	public void setSucursal(String sucursal);

	public String getTipoModificacion();
	public void setTipoModificacion(String tipoModificacion);
	
	public String getTransDistribucion();
	public void setTransDistribucion(String transDistribucion);

}
