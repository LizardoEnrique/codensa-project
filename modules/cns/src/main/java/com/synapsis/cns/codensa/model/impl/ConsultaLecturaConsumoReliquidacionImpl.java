/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaLecturaConsumoReliquidacion;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author dbraccio
 *
 *CU 006
 */
public class ConsultaLecturaConsumoReliquidacionImpl extends
		SynergiaBusinessObjectImpl implements ConsultaLecturaConsumoReliquidacion{
	private Long idCuenta;
	private Long nroCuenta;
	private Long idServicio;
	private Long nroServicio;
	private Long idMedidor;
	private String numeroMedidor;
	private Date fechaInicialReliquidacion;
	private Date fechaFinalReliquidacion;
	private String periodosLiquidados;
	private Double valorReliquidacionConsumo;
	private Double valorReliquidacionSubsidios;
	private Double valorReliquidacionContribucion;
	private String correlativoFacturacion;
	private Double consumoFacturadoActivaFP;
	private Double consumoReliquiadoActivaFP;
	private Double diferenciaConsumoActivaFP;
	private Double consumoFacturadoActivaHP;
	private Double consumoReliquiadoActivaHP;
	private Double diferenciaConsumoActivaHP;
	private Double consumoFacturadoActivaXP;
	private Double consumoReliquiadoActivaXP;
	private Double diferenciaConsumoActivaXP;
	private Double consumoFacturadoReactivaFP;
	private Double consumoReliquiadoReactivaFP;
	private Double diferenciaConsumoReactivaFP;
	private Double consumoFacturadoReactivaHP;
	private Double consumoReliquiadoReactivaHP;
	private Double diferenciaConsumoReactivaHP;
	private Double consumoFacturadoReactivaXP;
	private Double consumoReliquiadoReactivaXP;
	private Double diferenciaConsumoReactivaXP;
	
	
	public Long getIdCuenta() {
		return idCuenta;
	}
	public Long getIdServicio() {
		return idServicio;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public Long getNroServicio() {
		return nroServicio;
	}
	
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	public Double getConsumoFacturadoActivaFP() {
		return consumoFacturadoActivaFP;
	}
	public Double getConsumoFacturadoActivaHP() {
		return consumoFacturadoActivaHP;
	}
	public Double getConsumoFacturadoActivaXP() {
		return consumoFacturadoActivaXP;
	}
	public Double getConsumoFacturadoReactivaFP() {
		return consumoFacturadoReactivaFP;
	}
	public Double getConsumoFacturadoReactivaHP() {
		return consumoFacturadoReactivaHP;
	}
	public Double getConsumoFacturadoReactivaXP() {
		return consumoFacturadoReactivaXP;
	}
	public Double getConsumoReliquiadoActivaFP() {
		return consumoReliquiadoActivaFP;
	}
	public Double getConsumoReliquiadoActivaHP() {
		return consumoReliquiadoActivaHP;
	}
	public Double getConsumoReliquiadoActivaXP() {
		return consumoReliquiadoActivaXP;
	}
	public Double getConsumoReliquiadoReactivaFP() {
		return consumoReliquiadoReactivaFP;
	}
	public Double getConsumoReliquiadoReactivaHP() {
		return consumoReliquiadoReactivaHP;
	}
	public Double getConsumoReliquiadoReactivaXP() {
		return consumoReliquiadoReactivaXP;
	}
	public String getCorrelativoFacturacion() {
		return correlativoFacturacion;
	}
	public Double getDiferenciaConsumoActivaFP() {
		return diferenciaConsumoActivaFP;
	}
	public Double getDiferenciaConsumoActivaHP() {
		return diferenciaConsumoActivaHP;
	}
	public Double getDiferenciaConsumoActivaXP() {
		return diferenciaConsumoActivaXP;
	}
	public Double getDiferenciaConsumoReactivaFP() {
		return diferenciaConsumoReactivaFP;
	}
	public Double getDiferenciaConsumoReactivaHP() {
		return diferenciaConsumoReactivaHP;
	}
	public Double getDiferenciaConsumoReactivaXP() {
		return diferenciaConsumoReactivaXP;
	}
	public Date getFechaFinalReliquidacion() {
		return fechaFinalReliquidacion;
	}
	public Date getFechaInicialReliquidacion() {
		return fechaInicialReliquidacion;
	}
	public String getPeriodosLiquidados() {
		return periodosLiquidados;
	}
	public Double getValorReliquidacionConsumo() {
		return valorReliquidacionConsumo;
	}
	public Double getValorReliquidacionContribucion() {
		return valorReliquidacionContribucion;
	}
	public Double getValorReliquidacionSubsidios() {
		return valorReliquidacionSubsidios;
	}
	public void setConsumoFacturadoActivaFP(Double consumoFacturadoActivaFP) {
		this.consumoFacturadoActivaFP = consumoFacturadoActivaFP;
	}
	public void setConsumoFacturadoActivaHP(Double consumoFacturadoActivaHP) {
		this.consumoFacturadoActivaHP = consumoFacturadoActivaHP;
	}
	public void setConsumoFacturadoActivaXP(Double consumoFacturadoActivaXP) {
		this.consumoFacturadoActivaXP = consumoFacturadoActivaXP;
	}
	public void setConsumoFacturadoReactivaFP(Double consumoFacturadoReactivaFP) {
		this.consumoFacturadoReactivaFP = consumoFacturadoReactivaFP;
	}
	public void setConsumoFacturadoReactivaHP(Double consumoFacturadoReactivaHP) {
		this.consumoFacturadoReactivaHP = consumoFacturadoReactivaHP;
	}
	public void setConsumoFacturadoReactivaXP(Double consumoFacturadoReactivaXP) {
		this.consumoFacturadoReactivaXP = consumoFacturadoReactivaXP;
	}
	public void setConsumoReliquiadoActivaFP(Double consumoReliquiadoActivaFP) {
		this.consumoReliquiadoActivaFP = consumoReliquiadoActivaFP;
	}
	public void setConsumoReliquiadoActivaHP(Double consumoReliquiadoActivaHP) {
		this.consumoReliquiadoActivaHP = consumoReliquiadoActivaHP;
	}
	public void setConsumoReliquiadoActivaXP(Double consumoReliquiadoActivaXP) {
		this.consumoReliquiadoActivaXP = consumoReliquiadoActivaXP;
	}
	public void setConsumoReliquiadoReactivaFP(Double consumoReliquiadoReactivaFP) {
		this.consumoReliquiadoReactivaFP = consumoReliquiadoReactivaFP;
	}
	public void setConsumoReliquiadoReactivaHP(Double consumoReliquiadoReactivaHP) {
		this.consumoReliquiadoReactivaHP = consumoReliquiadoReactivaHP;
	}
	public void setConsumoReliquiadoReactivaXP(Double consumoReliquiadoReactivaXP) {
		this.consumoReliquiadoReactivaXP = consumoReliquiadoReactivaXP;
	}
	public void setCorrelativoFacturacion(String correlativoFacturacion) {
		this.correlativoFacturacion = correlativoFacturacion;
	}
	public void setDiferenciaConsumoActivaFP(Double diferenciaConsumoActivaFP) {
		this.diferenciaConsumoActivaFP = diferenciaConsumoActivaFP;
	}
	public void setDiferenciaConsumoActivaHP(Double diferenciaConsumoActivaHP) {
		this.diferenciaConsumoActivaHP = diferenciaConsumoActivaHP;
	}
	public void setDiferenciaConsumoActivaXP(Double diferenciaConsumoActivaXP) {
		this.diferenciaConsumoActivaXP = diferenciaConsumoActivaXP;
	}
	public void setDiferenciaConsumoReactivaFP(Double diferenciaConsumoReactivaFP) {
		this.diferenciaConsumoReactivaFP = diferenciaConsumoReactivaFP;
	}
	public void setDiferenciaConsumoReactivaHP(Double diferenciaConsumoReactivaHP) {
		this.diferenciaConsumoReactivaHP = diferenciaConsumoReactivaHP;
	}
	public void setDiferenciaConsumoReactivaXP(Double diferenciaConsumoReactivaXP) {
		this.diferenciaConsumoReactivaXP = diferenciaConsumoReactivaXP;
	}
	public void setFechaFinalReliquidacion(Date fechaFinalReliquidacion) {
		this.fechaFinalReliquidacion = fechaFinalReliquidacion;
	}
	public void setFechaInicialReliquidacion(Date fechaInicialReliquidacion) {
		this.fechaInicialReliquidacion = fechaInicialReliquidacion;
	}
	public void setPeriodosLiquidados(String periodosLiquidados) {
		this.periodosLiquidados = periodosLiquidados;
	}
	public void setValorReliquidacionConsumo(Double valorReliquidacionConsumo) {
		this.valorReliquidacionConsumo = valorReliquidacionConsumo;
	}
	public void setValorReliquidacionContribucion(
			Double valorReliquidacionContribucion) {
		this.valorReliquidacionContribucion = valorReliquidacionContribucion;
	}
	public void setValorReliquidacionSubsidios(Double valorReliquidacionSubsidios) {
		this.valorReliquidacionSubsidios = valorReliquidacionSubsidios;
	}
	public Long getIdMedidor() {
		return idMedidor;
	}
	public void setIdMedidor(Long idMedidor) {
		this.idMedidor = idMedidor;
	}
	public String getNumeroMedidor() {
		return numeroMedidor;
	}
	public void setNumeroMedidor(String numeroMedidor) {
		this.numeroMedidor = numeroMedidor;
	}
	

	
}
