/**
 * 
 */
package com.synapsis.cns.codensa.model.combo.impl;

import com.synapsis.cns.codensa.model.combo.ConsultaProductosCCA;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaProductosCCAImpl extends SynergiaBusinessObjectImpl implements ConsultaProductosCCA {
	   private String descripcion;
	   private Long idProducto;

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Long getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}         

	
	
	
}
