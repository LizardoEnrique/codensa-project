package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.cns.codensa.model.FacturaCabecera;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class FacturaCabeceraImpl extends SynergiaBusinessObjectImpl implements
		FacturaCabecera {

	private Long nroCuenta;
		
	private Long nroFactura;

	private String nit;

	private String ofPpalCod;

	private Date fechaExpedicion;

	private Date periodoFacIni;

	private Date periodoFacFin;

	private String clienteNombre;

	private String clienteDireccion;

	private String clienteTel;

	private String clienteBarrio;

	private String estratoSocioeco;

	private String tipoSrv;

	private String rutaLectura;

	private String cargaContratada;

	private Double nivelTension;

	private String nroMedidor_1;

	private String nroMedidor_2;

	private String nroCircuito;

	private Date vtoPagoOportuno;

	private Date vtoAvisoSuspension;

	private String codigoBarras;

	public Long getNroFactura() {
		return nroFactura;
	}

	public void setNroFactura(Long nroFactura) {
		this.nroFactura = nroFactura;
	}

	public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public String getOfPpalCod() {
		return ofPpalCod;
	}

	public void setOfPpalCod(String ofPpalCod) {
		this.ofPpalCod = ofPpalCod;
	}

	public Date getFechaExpedicion() {
		return fechaExpedicion;
	}

	public void setFechaExpedicion(Date fechaExpedicion) {
		this.fechaExpedicion = fechaExpedicion;
	}

	public Date getPeriodoFacIni() {
		return periodoFacIni;
	}

	public void setPeriodoFacIni(Date periodoFacIni) {
		this.periodoFacIni = periodoFacIni;
	}

	public Date getPeriodoFacFin() {
		return periodoFacFin;
	}

	public void setPeriodoFacFin(Date periodoFacFin) {
		this.periodoFacFin = periodoFacFin;
	}

	public String getClienteNombre() {
		return clienteNombre;
	}

	public void setClienteNombre(String clienteNombre) {
		this.clienteNombre = clienteNombre;
	}

	public String getClienteDireccion() {
		return clienteDireccion;
	}

	public void setClienteDireccion(String clienteDireccion) {
		this.clienteDireccion = clienteDireccion;
	}

	public String getClienteTel() {
		return clienteTel;
	}

	public void setClienteTel(String clienteTel) {
		this.clienteTel = clienteTel;
	}

	public String getClienteBarrio() {
		return clienteBarrio;
	}

	public void setClienteBarrio(String clienteBarrio) {
		this.clienteBarrio = clienteBarrio;
	}

	public String getEstratoSocioeco() {
		return estratoSocioeco;
	}

	public void setEstratoSocioeco(String estratoSocioeco) {
		this.estratoSocioeco = estratoSocioeco;
	}

	public String getTipoSrv() {
		return tipoSrv;
	}

	public void setTipoSrv(String tipoSrv) {
		this.tipoSrv = tipoSrv;
	}

	public String getRutaLectura() {
		return rutaLectura;
	}

	public void setRutaLectura(String rutaLectura) {
		this.rutaLectura = rutaLectura;
	}

	public String getCargaContratada() {
		return cargaContratada;
	}

	public void setCargaContratada(String cargaContratada) {
		this.cargaContratada = cargaContratada;
	}

	public Double getNivelTension() {
		return nivelTension;
	}

	public void setNivelTension(Double nivelTension) {
		this.nivelTension = nivelTension;
	}

	public String getNroMedidor_1() {
		return nroMedidor_1;
	}

	public void setNroMedidor_1(String nroMedidor_1) {
		this.nroMedidor_1 = nroMedidor_1;
	}

	public String getNroMedidor_2() {
		return nroMedidor_2;
	}

	public void setNroMedidor_2(String nroMedidor_2) {
		this.nroMedidor_2 = nroMedidor_2;
	}

	public String getNroCircuito() {
		return nroCircuito;
	}

	public void setNroCircuito(String nroCircuito) {
		this.nroCircuito = nroCircuito;
	}

	public Date getVtoPagoOportuno() {
		return vtoPagoOportuno;
	}

	public void setVtoPagoOportuno(Date vtoPagoOportuno) {
		this.vtoPagoOportuno = vtoPagoOportuno;
	}

	public Date getVtoAvisoSuspension() {
		return vtoAvisoSuspension;
	}

	public void setVtoAvisoSuspension(Date vtoAvisoSuspension) {
		this.vtoAvisoSuspension = vtoAvisoSuspension;
	}

	public String getCodigoBarras() {
		return codigoBarras;
	}

	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}

	public Long getNroCuenta() {
		return this.nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	
}