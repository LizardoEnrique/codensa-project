/**
 * 
 */
package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author dBraccio
 *
 * 16/11/2006
 */
public interface CaracteristicasTransformador extends SynergiaBusinessObject {
	
	public String getEstadoTransformador();
	public void setEstadoTransformador(String estadoTransformador);
	public Date getFechaConexion();
	public void setFechaConexion(Date fechaConexion) ;
	public String getNivelReferencia() ;
	public void setNivelReferencia(String nivelReferencia);
	public Long getNroCuenta() ;
	public void setNroCuenta(Long nroCuenta);
	public Long getNroServicio() ;
	public void setNroServicio(Long nroServicio) ;
	public String getNumeroTransformador() ;
	public void setNumeroTransformador(String numeroTransformador);
	public String getPropiedadTransformador();
	public void setPropiedadTransformador(String propiedadTransformador);
	public String getTipoRed() ;
	public void setTipoRed(String tipoRed) ;
	public String getTipoTransformador() ;
	public void setTipoTransformador(String tipoTransformador) ;

}
