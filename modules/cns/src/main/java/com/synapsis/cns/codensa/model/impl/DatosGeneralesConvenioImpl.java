package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.DatosGeneralesConvenio;

public class DatosGeneralesConvenioImpl extends SynergiaBusinessObjectImpl 
	implements DatosGeneralesConvenio {
	
	private Long idConvenio;
	private Long nroConvenio;
	private String tipoConvenio;
	private Long nroServicio;
	private String deudaActual;
	private String estadoActual;
	private Date fechaCreacion;
	private Date fechaFinalizacion;
	private String opcion;
	private String deudaInicial;
	private String valorCuotaInicial;
	private String valorCuotaMensual;
	private String motivo;
	private Integer cuotasOtorgadas;
	private Integer cuotasFacturadas;
	private Long cuotasAtrasadas;
	private String saldo;
	private Date fechaSiniestro;
	private String periodoGracia;
	private String montoCondonado;
	private Float tasaInteresAplicada;
	private Float tasaInteresActual;
	private Long nroCuenta;
	private Long cuotasPendientes;

	
	
	
	public Long getCuotasAtrasadas() {
		return cuotasAtrasadas;
	}
	public void setCuotasAtrasadas(Long cuotasAtrasadas) {
		this.cuotasAtrasadas = cuotasAtrasadas;
	}
	public Integer getCuotasFacturadas() {
		return cuotasFacturadas;
	}
	public void setCuotasFacturadas(Integer cuotasFacturadas) {
		this.cuotasFacturadas = cuotasFacturadas;
	}
	public Integer getCuotasOtorgadas() {
		return cuotasOtorgadas;
	}
	public void setCuotasOtorgadas(Integer cuotasOtorgadas) {
		this.cuotasOtorgadas = cuotasOtorgadas;
	}
	public String getDeudaActual() {
		return deudaActual;
	}
	public void setDeudaActual(String deudaActual) {
		this.deudaActual = deudaActual;
	}
	public String getDeudaInicial() {
		return deudaInicial;
	}
	public void setDeudaInicial(String deudaInicial) {
		this.deudaInicial = deudaInicial;
	}
	public String getEstadoActual() {
		return estadoActual;
	}
	public void setEstadoActual(String estadoActual) {
		this.estadoActual = estadoActual;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public Date getFechaFinalizacion() {
		return fechaFinalizacion;
	}
	public void setFechaFinalizacion(Date fechaFinalizacion) {
		this.fechaFinalizacion = fechaFinalizacion;
	}
	public Date getFechaSiniestro() {
		return fechaSiniestro;
	}
	public void setFechaSiniestro(Date fechaSiniestro) {
		this.fechaSiniestro = fechaSiniestro;
	}
	public String getMontoCondonado() {
		return montoCondonado;
	}
	public void setMontoCondonado(String montoCondonado) {
		this.montoCondonado = montoCondonado;
	}
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public String getOpcion() {
		return opcion;
	}
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}
	public String getPeriodoGracia() {
		return periodoGracia;
	}
	public void setPeriodoGracia(String periodoGracia) {
		this.periodoGracia = periodoGracia;
	}
	public String getSaldo() {
		return saldo;
	}
	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}
	public Float getTasaInteresActual() {
		return tasaInteresActual;
	}
	public void setTasaInteresActual(Float tasaInteresActual) {
		this.tasaInteresActual = tasaInteresActual;
	}
	public Float getTasaInteresAplicada() {
		return tasaInteresAplicada;
	}
	public void setTasaInteresAplicada(Float tasaInteresAplicada) {
		this.tasaInteresAplicada = tasaInteresAplicada;
	}
	public String getValorCuotaInicial() {
		return valorCuotaInicial;
	}
	public void setValorCuotaInicial(String valorCuotaInicial) {
		this.valorCuotaInicial = valorCuotaInicial;
	}
	public String getValorCuotaMensual() {
		return valorCuotaMensual;
	}
	public void setValorCuotaMensual(String valorCuotaMensual) {
		this.valorCuotaMensual = valorCuotaMensual;
	}
	public Long getIdConvenio() {
		return idConvenio;
	}
	public void setIdConvenio(Long idConvenio) {
		this.idConvenio = idConvenio;
	}
	public Long getNroConvenio() {
		return nroConvenio;
	}
	public void setNroConvenio(Long nroConvenio) {
		this.nroConvenio = nroConvenio;
	}
	public Long getNroServicio() {
		return nroServicio;
	}
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	public String getTipoConvenio() {
		return tipoConvenio;
	}
	public void setTipoConvenio(String tipoConvenio) {
		this.tipoConvenio = tipoConvenio;
	}
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public Long getCuotasPendientes() {
		return cuotasPendientes;
	}
	public void setCuotasPendientes(Long cuotasPendientes) {
		this.cuotasPendientes = cuotasPendientes;
	}

	
	
}