package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


/**
 * @author m3.MarioRoss - 14/11/2006
 * refactor dBraccio	- 29/11/2006
 */
public interface DatosMedidor extends SynergiaBusinessObject {
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#getFechaDescarga()
	 */
	public Date getFechaDescarga();

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#setFechaDescarga(java.util.Date)
	 */
	public void setFechaDescarga(Date fechaDescarga);

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#getMarcaMedidor()
	 */
	public String getMarcaMedidor();

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#setMarcaMedidor(java.lang.String)
	 */
	public void setMarcaMedidor(String marcaMedidor);

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#getModeloMedidor()
	 */
	public String getModeloMedidor();

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#setModeloMedidor(java.lang.String)
	 */
	public void setModeloMedidor(String modeloMedidor);

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#getNumeroMedidor()
	 */
	public String getNumeroMedidor();

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#setNumeroMedidor(java.lang.String)
	 */
	public void setNumeroMedidor(String numeroMedidor);

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#getTipoMedidor()
	 */
	public String getTipoMedidor();

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#setTipoMedidor(java.lang.String)
	 */
	public void setTipoMedidor(String tipoMedidor);

	/**
	 * @return the lecturaActivaFP
	 */
	public Long getLecturaActivaFP();

	/**
	 * @param lecturaActivaFP the lecturaActivaFP to set
	 */
	public void setLecturaActivaFP(Long lecturaActivaFP);

	/**
	 * @return the lecturaActivaHP
	 */
	public Long getLecturaActivaHP();

	/**
	 * @param lecturaActivaHP the lecturaActivaHP to set
	 */
	public void setLecturaActivaHP(Long lecturaActivaHP);

	/**
	 * @return the lecturaActivaXP
	 */
	public Long getLecturaActivaXP();

	/**
	 * @param lecturaActivaXP the lecturaActivaXP to set
	 */
	public void setLecturaActivaXP(Long lecturaActivaXP);

	/**
	 * @return the lecturaReactivaFP
	 */
	public Long getLecturaReactivaFP();

	/**
	 * @param lecturaReactivaFP the lecturaReactivaFP to set
	 */
	public void setLecturaReactivaFP(Long lecturaReactivaFP);

	/**
	 * @return the lecturaReactivaHP
	 */
	public Long getLecturaReactivaHP();

	/**
	 * @param lecturaReactivaHP the lecturaReactivaHP to set
	 */
	public void setLecturaReactivaHP(Long lecturaReactivaHP);

	/**
	 * @return the lecturaReactivaXP
	 */
	public Long getLecturaReactivaXP();

	/**
	 * @param lecturaReactivaXP the lecturaReactivaXP to set
	 */
	public void setLecturaReactivaXP(Long lecturaReactivaXP);
	/**
	 * @return the idOrden
	 */
	public Long getIdOrden();
	/**
	 * @param idOrden the idOrden to set
	 */
	public void setIdOrden(Long idOrden);
}