package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaDeudaConvenios;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.1 $ $Date: 2007/02/28 21:47:47 $
 * 
 */
public class ConsultaDeudaConveniosImpl extends SynergiaBusinessObjectImpl
		implements ConsultaDeudaConvenios {

	/**
	 * Comment for <code>deuda</code>
	 */
	private Double deuda;

	/**
	 * Comment for <code>antiguedadDeuda</code>
	 */
	private Long antiguedadDeuda;

	/**
	 * Comment for <code>saldosEnergia</code>
	 */
	private Long saldosEnergia;

	/**
	 * Comment for <code>totalCuotasOtorgadas</code>
	 */
	private Long totalCuotasOtorgadas;

	/**
	 * Comment for <code>totalCuotasFacturadas</code>
	 */
	private Long totalCuotasFacturadas;

	/**
	 * Comment for <code>valorDeudaCondonada</code>
	 */
	private Double valorDeudaCondonada;

	/**
	 * Comment for <code>indicadorDeudaPendiente</code>
	 */
	private String indicadorDeudaPendiente;

	/**
	 * Comment for <code>estadoCobranza</code>
	 */
	private String estadoCobranza;

	/**
	 * @return the antiguedadDeuda
	 */
	public Long getAntiguedadDeuda() {
		return this.antiguedadDeuda;
	}

	/**
	 * @param antiguedadDeuda
	 *            the antiguedadDeuda to set
	 */
	public void setAntiguedadDeuda(Long antiguedadDeuda) {
		this.antiguedadDeuda = antiguedadDeuda;
	}

	/**
	 * @return the deuda
	 */
	public Double getDeuda() {
		return this.deuda;
	}

	/**
	 * @param deuda
	 *            the deuda to set
	 */
	public void setDeuda(Double deuda) {
		this.deuda = deuda;
	}

	/**
	 * @return the estadoCobranza
	 */
	public String getEstadoCobranza() {
		return this.estadoCobranza;
	}

	/**
	 * @param estadoCobranza
	 *            the estadoCobranza to set
	 */
	public void setEstadoCobranza(String estadoCobranza) {
		this.estadoCobranza = estadoCobranza;
	}

	/**
	 * @return the indicadorDeudaPendiente
	 */
	public String getIndicadorDeudaPendiente() {
		return this.indicadorDeudaPendiente;
	}

	/**
	 * @param indicadorDeudaPendiente
	 *            the indicadorDeudaPendiente to set
	 */
	public void setIndicadorDeudaPendiente(String indicadorDeudaPendiente) {
		this.indicadorDeudaPendiente = indicadorDeudaPendiente;
	}

	/**
	 * @return the saldosEnergia
	 */
	public Long getSaldosEnergia() {
		return this.saldosEnergia;
	}

	/**
	 * @param saldosEnergia
	 *            the saldosEnergia to set
	 */
	public void setSaldosEnergia(Long saldosEnergia) {
		this.saldosEnergia = saldosEnergia;
	}

	/**
	 * @return the totalCuotasFacturadas
	 */
	public Long getTotalCuotasFacturadas() {
		return this.totalCuotasFacturadas;
	}

	/**
	 * @param totalCuotasFacturadas
	 *            the totalCuotasFacturadas to set
	 */
	public void setTotalCuotasFacturadas(Long totalCuotasFacturadas) {
		this.totalCuotasFacturadas = totalCuotasFacturadas;
	}

	/**
	 * @return the totalCuotasOtorgadas
	 */
	public Long getTotalCuotasOtorgadas() {
		return this.totalCuotasOtorgadas;
	}

	/**
	 * @param totalCuotasOtorgadas
	 *            the totalCuotasOtorgadas to set
	 */
	public void setTotalCuotasOtorgadas(Long totalCuotasOtorgadas) {
		this.totalCuotasOtorgadas = totalCuotasOtorgadas;
	}

	/**
	 * @return the valorDeudaCondonada
	 */
	public Double getValorDeudaCondonada() {
		return this.valorDeudaCondonada;
	}

	/**
	 * @param valorDeudaCondonada
	 *            the valorDeudaCondonada to set
	 */
	public void setValorDeudaCondonada(Double valorDeudaCondonada) {
		this.valorDeudaCondonada = valorDeudaCondonada;
	}

}