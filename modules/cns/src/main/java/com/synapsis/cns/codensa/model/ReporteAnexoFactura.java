package com.synapsis.cns.codensa.model;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

/**
 * Interfaz del reporte de duplicado de anexo de factura - Servicios Financieros
 *    
 * @author jhack
 */
public interface ReporteAnexoFactura {
	public Long getNroCuenta();
	public void setNroCuenta(Long nroCuenta);
	public String getDireccion();
	public void setDireccion(String direccion);
	public String getTelefono();
	public void setTelefono(String telefono);
	public Date getPeriodoInicio();
	public void setPeriodoInicio(Date periodoInicio);
	public Date getPeriodoFin();
	public void setPeriodoFin(Date periodoFin);
	public String getRutaReparto();
	public void setRutaReparto(String rutaReparto);
	public Collection getServiciosCCA();
	public void setServiciosCCA(Collection serviciosCCA);
	public Collection getServiciosECO();
	public void setServiciosECO(Collection serviciosECO);
}
