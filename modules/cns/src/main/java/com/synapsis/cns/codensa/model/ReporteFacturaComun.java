package com.synapsis.cns.codensa.model;

import java.util.Collection;
import java.util.Set;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ReporteFacturaComun extends SynergiaBusinessObject {

	public String getBarrioCliente();

	public void setDigitoVerificadorDoc(Long digitoVerificadorDoc);

	public Long getDigitoVerificadorDoc();

	public Long getDigitoVerificadorCta();

	public void setDigitoVerificadorCta(Long digitoVerificadorCta);

	public void setBarrioCliente(String barrio);

	public Double getCargaContratada();

	public void setCargaContratada(Double cargaContratada);

	public String getCodigoDeBarras();

	public void setCodigoDeBarras(String codigoDeBarras);

	public String getCodigoDeBarrasNro();

	public void setCodigoDeBarrasNro(String codigoDeBarras);

	public Double getConsumoPeriodoActual();

	public void setConsumoPeriodoActual(Double consumoPeriodoActual);

	public String getDireccionCliente();

	public void setDireccionCliente(String direccion);

	public String getDireccionOficinaPrincipal();

	public void setDireccionOficinaPrincipal(String direccionOficinaPrincipal);

	public String getIndicadorRepartoEspecial();

	public void setIndicadorRepartoEspecial(String direccionRepartoEspecial);

	public String getGrupoC();
	
	public void setGrupoC(String grupoC);
	
	public String getEstratoSocioeconomico();

	public void setEstratoSocioeconomico(String estratoSocioeconomico);

	public String getMunicipioCliente();

	public void setMunicipioCliente(String municipioCliente);

	public String getNit();

	public void setNit(String nit);

	public Long getNivelTension();

	public void setNivelTension(Long nivelTension);

	public String getNombreCliente();

	public void setNombreCliente(String nombreCliente);

	public String getNumeroCircuito();

	public void setNumeroCircuito(String numeroCircuito);

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public String getNumeroMedidor1();

	public void setNumeroMedidor1(String numeroMedidor1);

	public String getNumeroMedidor2();

	public void setNumeroMedidor2(String numeroMedidor2);

	public Long getNumeroServicio();

	public void setNumeroServicio(Long numeroServicio);

	public String getPeriodoFacturadoActual();

	public void setPeriodoFacturadoActual(String periodoFacturadoActual);

	public String getRutaLectura();

	public void setRutaLectura(String rutaLectura);

	public String getTelefonoCliente();

	public void setTelefonoCliente(String telefonoCliente);

	public String getTipoServicioInfoTecnica();

	public void setTipoServicioInfoTecnica(String tipoServicioInfoTecnica);

	public Set getTiposServicioCliente();

	public void setTiposServicioCliente(Set tiposServicioCliente);

	public Long getNroFactura();

	public void setNroFactura(Long nroFactura);


	public String getInformacionInteres();

	public void setInformacionInteres(String informacionInteres);

	public void setTarifaParaFacturacionActual_EAFP(String tarifaParaFacturacionActual_EAFP);

	public String getTarifaParaFacturacionActual_EAFP();

	public void setTarifaParaFacturacionActual_ERFP(String tarifaParaFacturacionActual_ERFP);

	public String getTarifaParaFacturacionActual_ERFP();

	public void setTarifaParaFacturacionActual_EAHP(String tarifaParaFacturacionActual_EAHP);

	public String getTarifaParaFacturacionActual_EAHP();

	public void setTarifaParaFacturacionActual_ERHP(String tarifaParaFacturacionActual_ERHP);

	public String getTarifaParaFacturacionActual_ERHP();

	public void setLecturaActual_EAFP(Double lecturaActual);

	public Double getLecturaActual_EAFP();

	public void setLecturaActual_ERFP(Double lecturaActual_ERFP);

	public Double getLecturaActual_ERFP();

	public void setLecturaActual_EAHP(Double lecturaActual_EAHP);

	public Double getLecturaActual_EAHP();

	public void setLecturaActual_ERHP(Double lecturaActual_ERHP);

	public Double getLecturaActual_ERHP();

	public void setLecturaAnterior_EAFP(Double lecturaAnterior_EAFP);

	public Double getLecturaAnterior_EAFP();

	public void setLecturaAnterior_ERFP(Double lecturaAnterior_ERFP);

	public Double getLecturaAnterior_ERFP();

	public void setLecturaAnterior_EAHP(Double lecturaAnterior_EAHP);

	public Double getLecturaAnterior_EAHP();

	public void setLecturaAnterior_ERHP(Double lecturaAnterior_ERHP);

	public Double getLecturaAnterior_ERHP();

	public void setDiferenciaEntreLecturaActualYAnterior_EAFP(Double diferenciaEntreLecturaActualYAnterior_EAFP);

	public Double getDiferenciaEntreLecturaActualYAnterior_EAFP();

	public void setDiferenciaEntreLecturaActualYAnterior_ERFP(Double diferenciaEntreLecturaActualYAnterior_ERFP);

	public Double getDiferenciaEntreLecturaActualYAnterior_ERFP();

	public void setDiferenciaEntreLecturaActualYAnterior_EAHP(Double diferenciaEntreLecturaActualYAnterior_EAHP);

	public Double getDiferenciaEntreLecturaActualYAnterior_EAHP();

	public void setDiferenciaEntreLecturaActualYAnterior_ERHP(Double diferenciaEntreLecturaActualYAnterior_ERHP);

	public Double getDiferenciaEntreLecturaActualYAnterior_ERHP();

	public void setFactorLiquidacion_EAFP(Double factorLiquidacion_EAFP);

	public Double getFactorLiquidacion_EAFP();

	public void setFactorLiquidacion_ERFP(Double factorLiquidacion_ERFP);

	public Double getFactorLiquidacion_ERFP();

	public void setFactorLiquidacion_EAHP(Double factorLiquidacion_EAHP);

	public Double getFactorLiquidacion_EAHP();

	public void setFactorLiquidacion_ERHP(Double factorLiquidacion_ERHP);

	public Double getFactorLiquidacion_ERHP();

	public void setTotalConsumoKwh_EAFP(Double totalConsumoKwh_EAFP);

	public Double getTotalConsumoKwh_EAFP();

	public void setTotalConsumoKwh_ERFP(Double totalConsumoKwh_ERFP);

	public Double getTotalConsumoKwh_ERFP();

	public void setTotalConsumoKwh_EAHP(Double totalConsumoKwh_EAHP);

	public Double getTotalConsumoKwh_EAHP();

	public void setTotalConsumoKwh_ERHP(Double totalConsumoKwh_ERHP);

	public Double getTotalConsumoKwh_ERHP();

	public String getDesMarca();

	public String getDuplicado();

	public Collection getConsumosUltimos6Meses();

	public void setConsumosUltimos6Meses(Collection consumosUltimos6Meses);

	public Collection getCargos();

	public void setCargos(Collection cargos);

	public Collection getCargosDescuento();

	public void setCargosDescuento(Collection cargosDescuento);

	public Collection getCargosOtrosServPort();

	public void setCargosOtrosServPort(Collection cargosOtrosServPort);

	public Collection getOtrosCargos();

	public void setOtrosCargos(Collection otrosCargos);

	public Collection getCargosServicioElectrico();

	public void setCargosServicioElectrico(Collection cargosServicioElectrico);

	public Collection getConsumos();

	public void setConsumos(Collection consumos);
	
	/***********************************
	 * 25/09/2017 - Lizardo Mamani - Ayesa
	************************************/
	public Collection getConsumosServicioAseo();
	public void setConsumosServicioAseo(Collection consumosServicioAseo);
}
