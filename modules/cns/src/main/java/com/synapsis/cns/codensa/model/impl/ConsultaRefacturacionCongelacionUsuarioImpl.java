/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaRefacturacionCongelacionUsuario;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author dbraccio
 * 
 * CU 016
 *
 */
public class ConsultaRefacturacionCongelacionUsuarioImpl extends
		SynergiaBusinessObjectImpl implements ConsultaRefacturacionCongelacionUsuario{

	private Long idCuenta;
	private Long nroCuenta;
	private Long idOperacion;
	private Long nroOperacion;
	private String analistaQueDebeAprobar;
	private String analistaAprobador;
	private Date fechaAprobacion;
	private Date horaAprobacion;
	private String analistaQueElimino;
	private Date fechaEliminacion;
	private Date horaEliminacion;
	private String usuariosIntervinientes;
	private Date fechaIntervencion;
	private String accionQueEjecuto;
	private Double tiempoIntervencion;
	
	public String getAccionQueEjecuto() {
		return accionQueEjecuto;
	}
	public String getAnalistaAprobador() {
		return analistaAprobador;
	}
	public String getAnalistaQueDebeAprobar() {
		return analistaQueDebeAprobar;
	}
	public String getAnalistaQueElimino() {
		return analistaQueElimino;
	}
	public Date getFechaAprobacion() {
		return fechaAprobacion;
	}
	public Date getFechaEliminacion() {
		return fechaEliminacion;
	}
	public Date getFechaIntervencion() {
		return fechaIntervencion;
	}
	public Date getHoraAprobacion() {
		return horaAprobacion;
	}
	public Date getHoraEliminacion() {
		return horaEliminacion;
	}
	public Long getIdCuenta() {
		return idCuenta;
	}
	public Long getIdOperacion() {
		return idOperacion;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public Long getNroOperacion() {
		return nroOperacion;
	}
	public Double getTiempoIntervencion() {
		return tiempoIntervencion;
	}
	public String getUsuariosIntervinientes() {
		return usuariosIntervinientes;
	}
	public void setAccionQueEjecuto(String accionQueEjecuto) {
		this.accionQueEjecuto = accionQueEjecuto;
	}
	public void setAnalistaAprobador(String analistaAprobador) {
		this.analistaAprobador = analistaAprobador;
	}
	public void setAnalistaQueDebeAprobar(String analistaQueDebeAprobar) {
		this.analistaQueDebeAprobar = analistaQueDebeAprobar;
	}
	public void setAnalistaQueElimino(String analistaQueElimino) {
		this.analistaQueElimino = analistaQueElimino;
	}
	public void setFechaAprobacion(Date fechaAprobacion) {
		this.fechaAprobacion = fechaAprobacion;
	}
	public void setFechaEliminacion(Date fechaEliminacion) {
		this.fechaEliminacion = fechaEliminacion;
	}
	public void setFechaIntervencion(Date fechaIntervencion) {
		this.fechaIntervencion = fechaIntervencion;
	}
	public void setHoraAprobacion(Date horaAprobacion) {
		this.horaAprobacion = horaAprobacion;
	}
	public void setHoraEliminacion(Date horaEliminacion) {
		this.horaEliminacion = horaEliminacion;
	}
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	public void setIdOperacion(Long idOperacion) {
		this.idOperacion = idOperacion;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public void setNroOperacion(Long nroOperacion) {
		this.nroOperacion = nroOperacion;
	}
	public void setTiempoIntervencion(Double tiempoIntervencion) {
		this.tiempoIntervencion = tiempoIntervencion;
	}
	public void setUsuariosIntervinientes(String usuariosIntervinientes) {
		this.usuariosIntervinientes = usuariosIntervinientes;
	}
	
	
	
	
}
