/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaOtrasLecturaConsumo;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author dbraccio
 *
 */
public class ConsultaOtrasLecturaConsumoImpl extends SynergiaBusinessObjectImpl 
	implements ConsultaOtrasLecturaConsumo{
	private static final long serialVersionUID = 1L;

	private Long idMedidor;
	private Long nroCuenta;
	private Long idCuenta;
	private Long idServicio;
	private Date fechaLecturaAnterior;
	private Date fechaLecturaActual;
	private String periodoFacturacion;
	private Double lecturaTerrenoReactivaHP;
	private Double lecturaFacturaReactivaHP;
	private Double lecturaTerrenoReactivaXP;
	private Double lecturaFacturaReactivaXP;
	private Double consumoRealReactivaHP;
	private Double consumoFacturadoReactivaHP;
	private Double consumoRealReactivaXP;
	private Double consumoFacturadoReactivaXP;
	private String codigoAnomaliaLecturaTerreno;
	private String codigoAnomaliaLecturaFacturacion;
	private String descripcionAnomaliaLecturaFacturacion;
	private String ubicacionMedidor;
	private String estadoMedidor;
	private Long nroServicio;
	
	//nuevos atributos solicitados en el acta
	private String evento;
	private String tipoLectura;
	
	private Double lecturaTerrenoReactivaFP;		
	private Double lecturaFacturaReactivaFP;
	private Double consumoRealReactivaFP;
	private Double consumoFacturadoReactivaFP;
	
	
	
	public Double getConsumoFacturadoReactivaHP() {
		return consumoFacturadoReactivaHP;
	}
	public Double getConsumoFacturadoReactivaXP() {
		return consumoFacturadoReactivaXP;
	}
	public Double getConsumoRealReactivaHP() {
		return consumoRealReactivaHP;
	}
	public Double getConsumoRealReactivaXP() {
		return consumoRealReactivaXP;
	}
	public String getDescripcionAnomaliaLecturaFacturacion() {
		return descripcionAnomaliaLecturaFacturacion;
	}
	public String getEstadoMedidor() {
		return estadoMedidor;
	}
	public Date getFechaLecturaActual() {
		return fechaLecturaActual;
	}
	public Date getFechaLecturaAnterior() {
		return fechaLecturaAnterior;
	}
	public Long getIdCuenta() {
		return idCuenta;
	}
	public Long getIdServicio() {
		return idServicio;
	}
	public Double getLecturaFacturaReactivaHP() {
		return lecturaFacturaReactivaHP;
	}
	public Double getLecturaFacturaReactivaXP() {
		return lecturaFacturaReactivaXP;
	}
	public Double getLecturaTerrenoReactivaHP() {
		return lecturaTerrenoReactivaHP;
	}
	public Double getLecturaTerrenoReactivaXP() {
		return lecturaTerrenoReactivaXP;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public String getPeriodoFacturacion() {
		return periodoFacturacion;
	}
	public String getUbicacionMedidor() {
		return ubicacionMedidor;
	}
	
	public Long getNroServicio() {
		return nroServicio;
	}
	
	public String getCodigoAnomaliaLecturaFacturacion() {
		return codigoAnomaliaLecturaFacturacion;
	}
	public String getCodigoAnomaliaLecturaTerreno() {
		return codigoAnomaliaLecturaTerreno;
	}
	
	public void setCodigoAnomaliaLecturaFacturacion(
			String codigoAnomaliaLecturaFacturacion) {
		this.codigoAnomaliaLecturaFacturacion = codigoAnomaliaLecturaFacturacion;
	}
	public void setCodigoAnomaliaLecturaTerreno(String codigoAnomaliaLecturaTerreno) {
		this.codigoAnomaliaLecturaTerreno = codigoAnomaliaLecturaTerreno;
	}
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	public void setConsumoFacturadoReactivaHP(Double consumoFacturadoReactivaHP) {
		this.consumoFacturadoReactivaHP = consumoFacturadoReactivaHP;
	}
	public void setConsumoFacturadoReactivaXP(Double consumoFacturadoReactivaXP) {
		this.consumoFacturadoReactivaXP = consumoFacturadoReactivaXP;
	}
	public void setConsumoRealReactivaHP(Double consumoRealReactivaHP) {
		this.consumoRealReactivaHP = consumoRealReactivaHP;
	}
	public void setConsumoRealReactivaXP(Double consumoRealReactivaXP) {
		this.consumoRealReactivaXP = consumoRealReactivaXP;
	}
	public void setDescripcionAnomaliaLecturaFacturacion(
			String descripcionAnomaliaLecturaFacturacion) {
		this.descripcionAnomaliaLecturaFacturacion = descripcionAnomaliaLecturaFacturacion;
	}
	public void setEstadoMedidor(String estadoMedidor) {
		this.estadoMedidor = estadoMedidor;
	}
	public void setFechaLecturaActual(Date fechaLecturaActual) {
		this.fechaLecturaActual = fechaLecturaActual;
	}
	public void setFechaLecturaAnterior(Date fechaLecturaAnterior) {
		this.fechaLecturaAnterior = fechaLecturaAnterior;
	}
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}
	public void setLecturaFacturaReactivaHP(Double lecturaFacturaReactivaHP) {
		this.lecturaFacturaReactivaHP = lecturaFacturaReactivaHP;
	}
	public void setLecturaFacturaReactivaXP(Double lecturaFacturaReactivaXP) {
		this.lecturaFacturaReactivaXP = lecturaFacturaReactivaXP;
	}
	public void setLecturaTerrenoReactivaHP(Double lecturaTerrenoReactivaHP) {
		this.lecturaTerrenoReactivaHP = lecturaTerrenoReactivaHP;
	}
	public void setLecturaTerrenoReactivaXP(Double lecturaTerrenoReactivaXP) {
		this.lecturaTerrenoReactivaXP = lecturaTerrenoReactivaXP;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public void setPeriodoFacturacion(String periodoFacturacion) {
		this.periodoFacturacion = periodoFacturacion;
	}
	public void setUbicacionMedidor(String ubicacionMedidor) {
		this.ubicacionMedidor = ubicacionMedidor;
	}
	public String getEvento() {
		return evento;
	}
	public void setEvento(String evento) {
		this.evento = evento;
	}
	public String getTipoLectura() {
		return tipoLectura;
	}
	public void setTipoLectura(String tipoLectura) {
		this.tipoLectura = tipoLectura;
	}
	public Double getConsumoFacturadoReactivaFP() {
		return consumoFacturadoReactivaFP;
	}
	public void setConsumoFacturadoReactivaFP(Double consumoFacturadoReactivaFP) {
		this.consumoFacturadoReactivaFP = consumoFacturadoReactivaFP;
	}
	public Double getConsumoRealReactivaFP() {
		return consumoRealReactivaFP;
	}
	public void setConsumoRealReactivaFP(Double consumoRealReactivaFP) {
		this.consumoRealReactivaFP = consumoRealReactivaFP;
	}
	public Double getLecturaFacturaReactivaFP() {
		return lecturaFacturaReactivaFP;
	}
	public void setLecturaFacturaReactivaFP(Double lecturaFacturaReactivaFP) {
		this.lecturaFacturaReactivaFP = lecturaFacturaReactivaFP;
	}
	public Double getLecturaTerrenoReactivaFP() {
		return lecturaTerrenoReactivaFP;
	}
	public void setLecturaTerrenoReactivaFP(Double lecturaTerrenoReactivaFP) {
		this.lecturaTerrenoReactivaFP = lecturaTerrenoReactivaFP;
	}
	public Long getIdMedidor() {
		return idMedidor;
	}
	public void setIdMedidor(Long idMedidor) {
		this.idMedidor = idMedidor;
	}
	
}
