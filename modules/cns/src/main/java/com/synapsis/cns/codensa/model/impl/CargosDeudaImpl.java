package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.CargosDeuda;

public class CargosDeudaImpl extends SynergiaBusinessObjectImpl
		implements CargosDeuda {
		
	private Long codigoCargo;
	private String nombreCargo;
	private Long valor;
	private Long unidad;
	private Long numeroUnidades;
	private String usuarioGenerador;
	private String nombreUsuarioGenerador;
	private Date fechaCreacion;
	private Long numeroDocumentoAsociado;
	
	public Long getCodigoCargo() {
		return codigoCargo;
	}
	public void setCodigoCargo(Long codigoCargo) {
		this.codigoCargo = codigoCargo;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getNombreCargo() {
		return nombreCargo;
	}
	public void setNombreCargo(String nombreCargo) {
		this.nombreCargo = nombreCargo;
	}
	public String getNombreUsuarioGenerador() {
		return nombreUsuarioGenerador;
	}
	public void setNombreUsuarioGenerador(String nombreUsuarioGenerador) {
		this.nombreUsuarioGenerador = nombreUsuarioGenerador;
	}
	public Long getNumeroDocumentoAsociado() {
		return numeroDocumentoAsociado;
	}
	public void setNumeroDocumentoAsociado(Long numeroDocumentoAsociado) {
		this.numeroDocumentoAsociado = numeroDocumentoAsociado;
	}
	public Long getNumeroUnidades() {
		return numeroUnidades;
	}
	public void setNumeroUnidades(Long numeroUnidades) {
		this.numeroUnidades = numeroUnidades;
	}
	public Long getUnidad() {
		return unidad;
	}
	public void setUnidad(Long unidad) {
		this.unidad = unidad;
	}
	public String getUsuarioGenerador() {
		return usuarioGenerador;
	}
	public void setUsuarioGenerador(String usuarioGenerador) {
		this.usuarioGenerador = usuarioGenerador;
	}
	public Long getValor() {
		return valor;
	}
	public void setValor(Long valor) {
		this.valor = valor;
	}
	
	
}