package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


/**
 * @author m3.MarioRoss - 14/11/2006
 * refactor dBraccio	- 29/11/2006
 */
public interface EventosTransformador extends SynergiaBusinessObject {

	public Date getFechaInstalacion();
	
	public void setFechaInstalacion(Date fechaInstalacion);
		
	public String getMarcaTransformador();
	
	public void setMarcaTransformador(String marcaTransformador);
	
	public String getModeloTransformador();
	
	public void setModeloTransformador(String modeloTransformador);
	
	public Long getNroCuenta();
	
	public void setNroCuenta(Long nroCuenta);
	
	public String getNumeroTransformador();
	
	public void setNumeroTransformador(String numeroTransformador);
	
	public String getTipoEvento();
	
	public void setTipoEvento(String tipoEvento);

	public String getNumeroPCR() ;
	
	public void setNumeroPCR(String numeroPCR);
    
	public Date getFechaRetiro();
	
	public void setFechaRetiro(Date fechaRetiro);
	public Date getFechaModificacion();
	public void setFechaModificacion(Date fechaModificacion);
	
}