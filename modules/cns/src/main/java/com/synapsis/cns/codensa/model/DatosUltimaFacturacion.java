package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface DatosUltimaFacturacion extends SynergiaBusinessObject {

	/**
	 * @return the factura
	 */
	public abstract Long getFactura();

	/**
	 * @param factura the factura to set
	 */
	public abstract void setFactura(Long factura);

	/**
	 * @return the fechaFactura
	 */
	public abstract Date getFechaFactura();

	/**
	 * @param fechaFactura the fechaFactura to set
	 */
	public abstract void setFechaFactura(Date fechaFactura);

	/**
	 * @return the fechaVencimiento
	 */
	public abstract Date getFechaVencimiento();

	/**
	 * @param fechaVencimiento the fechaVencimiento to set
	 */
	public abstract void setFechaVencimiento(Date fechaVencimiento);

	/**
	 * @return the nroCuenta
	 */
	public abstract Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public abstract void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroServicio
	 */
	public abstract Long getNroServicio();

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public abstract void setNroServicio(Long nroServicio);

	/**
	 * @return the saldoAnterior
	 */
	public abstract String getSaldoAnterior();

	/**
	 * @param saldoAnterior the saldoAnterior to set
	 */
	public abstract void setSaldoAnterior(String saldoAnterior);

	/**
	 * @return the totalDocumento
	 */
	public abstract String getTotalDocumento();

	/**
	 * @param totalDocumento the totalDocumento to set
	 */
	public abstract void setTotalDocumento(String totalDocumento);

	
	public abstract String getTipoLiquidacion();
	public abstract void setTipoLiquidacion(String tipoLiquidacion);
	public abstract String getTipoEsquemaFacturacion();
	public abstract void setTipoEsquemaFacturacion(String tipoEsquemaFacturacion);
	
}