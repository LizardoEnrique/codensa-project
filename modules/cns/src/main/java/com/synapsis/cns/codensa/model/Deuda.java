/**
 * $Id: Deuda.java,v 1.6 2010/05/26 18:32:07 ar29302553 Exp $
 */
package com.synapsis.cns.codensa.model;

/**
 * @author Paola Attadio
 * @verasion $Revision: 1.6 $
 */
import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface Deuda extends SynergiaBusinessObject {

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public Long getIdItemDoc();

	public void setIdItemDoc(Long idItemDoc);

	public String getCodCargo();

	public void setCodCargo(String codCargo);

	public String getDescCargo();

	public void setDescCargo(String descCargo);

	public Double getValorCargo();

	public void setValorCargo(Double valorCargo);

	public Double getSaldoCargo();

	public void setSaldoCargo(Double saldoCargo);

	public String getCorrelativoFacturacion();

	public void setCorrelativoFacturacion(String correlativoFacturacion);

	public Date getFecFactura();

	public void setFecFactura(Date fecFactura);

	public String getPeriodoFacturacion();

	public void setPeriodoFacturacion(String periodoFacturacion);

	public Long getNroDocumento();

	public void setNroDocumento(Long nroDocumento);
	
	public String getOrigenDeuda();

	public void setOrigenDeuda(String origenDeuda);
	
	public Long getNroServicio();
	
	public void setNroServicio(Long nroServicio);
}