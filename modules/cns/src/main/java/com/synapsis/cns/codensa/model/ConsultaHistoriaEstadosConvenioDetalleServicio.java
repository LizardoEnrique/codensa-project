package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface ConsultaHistoriaEstadosConvenioDetalleServicio {

	/**
	 * @return the causalNegacion
	 */
	public String getCausalNegacion();

	/**
	 * @param causalNegacion the causalNegacion to set
	 */
	public void setCausalNegacion(String causalNegacion);

	/**
	 * @return the cicloFacturacion
	 */
	public String getCicloFacturacion();

	/**
	 * @param cicloFacturacion the cicloFacturacion to set
	 */
	public void setCicloFacturacion(String cicloFacturacion);

	/**
	 * @return the diasMora
	 */
	public Long getDiasMora();

	/**
	 * @param diasMora the diasMora to set
	 */
	public void setDiasMora(Long diasMora);

	/**
	 * @return the estrato
	 */
	public String getEstrato();

	/**
	 * @param estrato the estrato to set
	 */
	public void setEstrato(String estrato);

	/**
	 * @return the fechaCompraProducto
	 */
	public Date getFechaCompraProducto();

	/**
	 * @param fechaCompraProducto the fechaCompraProducto to set
	 */
	public void setFechaCompraProducto(Date fechaCompraProducto);

	/**
	 * @return the fechaSolicitudCredito
	 */
	public Date getFechaSolicitudCredito();

	/**
	 * @param fechaSolicitudCredito the fechaSolicitudCredito to set
	 */
	public void setFechaSolicitudCredito(Date fechaSolicitudCredito);

	/**
	 * @return the lineaNegocio
	 */
	public String getLineaNegocio();

	/**
	 * @param lineaNegocio the lineaNegocio to set
	 */
	public void setLineaNegocio(String lineaNegocio);

	/**
	 * @return the marcaProducto
	 */
	public String getMarcaProducto();

	/**
	 * @param marcaProducto the marcaProducto to set
	 */
	public void setMarcaProducto(String marcaProducto);

	/**
	 * @return the montoAprobado
	 */
	public Long getMontoAprobado();

	/**
	 * @param montoAprobado the montoAprobado to set
	 */
	public void setMontoAprobado(Long montoAprobado);

	/**
	 * @return the montoDisponible
	 */
	public Long getMontoDisponible();

	/**
	 * @param montoDisponible the montoDisponible to set
	 */
	public void setMontoDisponible(Long montoDisponible);

	/**
	 * @return the nombreSolicitante
	 */
	public String getNombreSolicitante();

	/**
	 * @param nombreSolicitante the nombreSolicitante to set
	 */
	public void setNombreSolicitante(String nombreSolicitante);

	/**
	 * @return the nroCuotasFacturaras
	 */
	public String getNroCuotasFacturaras();

	/**
	 * @param nroCuotasFacturaras the nroCuotasFacturaras to set
	 */
	public void setNroCuotasFacturaras(String nroCuotasFacturaras);

	/**
	 * @return the nroCuotasFaltanetes
	 */
	public String getNroCuotasFaltanetes();

	/**
	 * @param nroCuotasFaltanetes the nroCuotasFaltanetes to set
	 */
	public void setNroCuotasFaltanetes(String nroCuotasFaltanetes);

	/**
	 * @return the nroCuotasOriginales
	 */
	public String getNroCuotasOriginales();

	/**
	 * @param nroCuotasOriginales the nroCuotasOriginales to set
	 */
	public void setNroCuotasOriginales(String nroCuotasOriginales);

	/**
	 * @return the nroIdentificacion
	 */
	public Long getNroIdentificacion();

	/**
	 * @param nroIdentificacion the nroIdentificacion to set
	 */
	public void setNroIdentificacion(Long nroIdentificacion);

	/**
	 * @return the periodosAtrasados
	 */
	public String getPeriodosAtrasados();

	/**
	 * @param periodosAtrasados the periodosAtrasados to set
	 */
	public void setPeriodosAtrasados(String periodosAtrasados);

	/**
	 * @return the productoCodensaHogar
	 */
	public String getProductoCodensaHogar();

	/**
	 * @param productoCodensaHogar the productoCodensaHogar to set
	 */
	public void setProductoCodensaHogar(String productoCodensaHogar);

	/**
	 * @return the puntoVentaCompra
	 */
	public String getPuntoVentaCompra();

	/**
	 * @param puntoVentaCompra the puntoVentaCompra to set
	 */
	public void setPuntoVentaCompra(String puntoVentaCompra);

	/**
	 * @return the resultadoEstudio
	 */
	public String getResultadoEstudio();

	/**
	 * @param resultadoEstudio the resultadoEstudio to set
	 */
	public void setResultadoEstudio(String resultadoEstudio);

	/**
	 * @return the saldoCapitalServicio
	 */
	public Long getSaldoCapitalServicio();

	/**
	 * @param saldoCapitalServicio the saldoCapitalServicio to set
	 */
	public void setSaldoCapitalServicio(Long saldoCapitalServicio);

	/**
	 * @return the saldoInteresCorriente
	 */
	public Long getSaldoInteresCorriente();

	/**
	 * @param saldoInteresCorriente the saldoInteresCorriente to set
	 */
	public void setSaldoInteresCorriente(Long saldoInteresCorriente);

	/**
	 * @return the saldoInteresMora
	 */
	public Long getSaldoInteresMora();

	/**
	 * @param saldoInteresMora the saldoInteresMora to set
	 */
	public void setSaldoInteresMora(Long saldoInteresMora);

	/**
	 * @return the socioRealizadaCompra
	 */
	public String getSocioRealizadaCompra();

	/**
	 * @param socioRealizadaCompra the socioRealizadaCompra to set
	 */
	public void setSocioRealizadaCompra(String socioRealizadaCompra);

	/**
	 * @return the tasaInteresOrigen
	 */
	public Long getTasaInteresOrigen();

	/**
	 * @param tasaInteresOrigen the tasaInteresOrigen to set
	 */
	public void setTasaInteresOrigen(Long tasaInteresOrigen);

	/**
	 * @return the valorCapitalMora
	 */
	public Long getValorCapitalMora();

	/**
	 * @param valorCapitalMora the valorCapitalMora to set
	 */
	public void setValorCapitalMora(Long valorCapitalMora);

	/**
	 * @return the valorInteresesMora
	 */
	public Long getValorInteresesMora();

	/**
	 * @param valorInteresesMora the valorInteresesMora to set
	 */
	public void setValorInteresesMora(Long valorInteresesMora);

	/**
	 * @return the valorOriginalServicio
	 */
	public Long getValorOriginalServicio();

	/**
	 * @param valorOriginalServicio the valorOriginalServicio to set
	 */
	public void setValorOriginalServicio(Long valorOriginalServicio);

}