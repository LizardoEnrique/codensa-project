package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaAnomalia extends SynergiaBusinessObject {

	/**
	 * @return the codigo
	 */
	public String getCodigo();

	/**
	 * @return the descripcion
	 */
	public String getDescripcion();

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo);

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion);

}