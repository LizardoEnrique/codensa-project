package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


/**
 * @author m3.MarioRoss - 14/11/2006
 * refactor dBraccio	- 29/11/2006
 */
public interface InformacionMedidor extends SynergiaBusinessObject {

	public Date getFechaEvaluacion();

	public void setFechaEvaluacion(Date fechaEvaluacion);

	public Date getFechaRetiro();

	public void setFechaRetiro(Date fechaRetiro);

	public Long getIdMedidor();

	public void setIdMedidor(Long idMedidor);

	public String getMarcaMedidor();

	public void setMarcaMedidor(String marcaMedidor);

	public String getModeloMedidor();

	public void setModeloMedidor(String modeloMedidor);

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public Long getNumeroInspeccion();

	public void setNumeroInspeccion(Long numeroInspeccion);

	public String getNumeroMedidor();

	public void setNumeroMedidor(String numeroMedidor);
	

}