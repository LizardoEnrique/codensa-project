package com.synapsis.cns.codensa.model.impl.buscaCuenta;

import com.synapsis.cns.codensa.model.buscaCuenta.BuscaCuentaTipoNumeroIdenBeneficiario;

public class BuscaCuentaTipoNumeroIdenBeneficiarioImpl extends BuscaCuentaImpl
		implements BuscaCuentaTipoNumeroIdenBeneficiario {

	private String numero;
	private String tipo;
	private Long idTipo;

	public Long getIdTipo() {
		return idTipo;
	}

	public void setIdTipo(Long idTipo) {
		this.idTipo = idTipo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaTipoNumeroIdenBeneficiario#getNumero()
	 */
	public String getNumero() {
		return numero;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaTipoNumeroIdenBeneficiario#setNumero(java.lang.Long)
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}