/**
 * 
 */
package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author dbraccio
 *
 */
public interface ConsultaRefacturacionAjusteUsuario extends
		SynergiaBusinessObject {

	public String getAccionEjecutada() ;
	public String getFechaIntervencion();
	public Long getIdAjuste() ;
	public Long getIdCuenta() ;
	public String getNombreUsuario();
	public Long getNroAjuste() ;
	public Long getNroCuenta() ;
	public String getTiempoIntervencion();
	public String getUsuario() ;

}
