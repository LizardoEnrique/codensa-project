/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaExpedienteEnvioSSPD;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaExpedienteEnvioSSPDImpl extends SynergiaBusinessObjectImpl implements ConsultaExpedienteEnvioSSPD {
	private Date fechaEnvio;
	private String tipoEntrega;	
	private String tipoRadicacion;	
	private Long numeroRadicacion;	
	private Long numeroRadicacionDestino;	
	private Date fechaEntrega;	
	private Double valorReclamacion;	
	private Long numeroRadicacionConcede;
	private Long nroExpediente;
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioSSPD#getFechaEntrega()
	 */
	public Date getFechaEntrega() {
		return fechaEntrega;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioSSPD#setFechaEntrega(java.util.Date)
	 */
	public void setFechaEntrega(Date fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioSSPD#getFechaEnvio()
	 */
	public Date getFechaEnvio() {
		return fechaEnvio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioSSPD#setFechaEnvio(java.util.Date)
	 */
	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioSSPD#getNumeroRadicacion()
	 */
	public Long getNumeroRadicacion() {
		return numeroRadicacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioSSPD#setNumeroRadicacion(java.lang.Long)
	 */
	public void setNumeroRadicacion(Long numeroRadicacion) {
		this.numeroRadicacion = numeroRadicacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioSSPD#getNumeroRadicacionConcede()
	 */
	public Long getNumeroRadicacionConcede() {
		return numeroRadicacionConcede;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioSSPD#setNumeroRadicacionConcede(java.lang.Long)
	 */
	public void setNumeroRadicacionConcede(Long numeroRadicacionConcede) {
		this.numeroRadicacionConcede = numeroRadicacionConcede;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioSSPD#getNumeroRadicacionDestino()
	 */
	public Long getNumeroRadicacionDestino() {
		return numeroRadicacionDestino;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioSSPD#setNumeroRadicacionDestino(java.lang.Long)
	 */
	public void setNumeroRadicacionDestino(Long numeroRadicacionDestino) {
		this.numeroRadicacionDestino = numeroRadicacionDestino;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioSSPD#getTipoEntrega()
	 */
	public String getTipoEntrega() {
		return tipoEntrega;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioSSPD#setTipoEntrega(java.lang.String)
	 */
	public void setTipoEntrega(String tipoEntrega) {
		this.tipoEntrega = tipoEntrega;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioSSPD#getTipoRadicacion()
	 */
	public String getTipoRadicacion() {
		return tipoRadicacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioSSPD#setTipoRadicacion(java.lang.String)
	 */
	public void setTipoRadicacion(String tipoRadicacion) {
		this.tipoRadicacion = tipoRadicacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioSSPD#getValorReclamacion()
	 */
	public Double getValorReclamacion() {
		return valorReclamacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioSSPD#setValorReclamacion(java.lang.Double)
	 */
	public void setValorReclamacion(Double valorReclamacion) {
		this.valorReclamacion = valorReclamacion;
	}
	/**
	 * @return the nroExpediente
	 */
	public Long getNroExpediente() {
		return nroExpediente;
	}
	/**
	 * @param nroExpediente the nroExpediente to set
	 */
	public void setNroExpediente(Long nroExpediente) {
		this.nroExpediente = nroExpediente;
	}

}
