/**
 * 
 */
package com.synapsis.cns.codensa.web;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;

import org.apache.commons.lang.StringUtils;
import org.apache.myfaces.shared_impl.util.MessageUtils;
import org.springframework.dao.IncorrectResultSizeDataAccessException;

import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.model.BusinessObject;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.service.FinderService;
import com.suivant.arquitectura.presentation.model.ListableBB;
import com.suivant.arquitectura.presentation.utils.JSFUtils;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.synapsis.cns.codensa.model.DetalleCargos;
import com.synapsis.cns.codensa.web.filters.CustomQueryFilter;
import com.synapsis.commons.faces.messages.ErrorsManager;
import com.synapsis.synergia.core.components.dataTable.ScrollableList;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;

/**
 * @author ar30557486 CNS 019
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.13 $ $Date: 2014/04/16 17:58:46 $
 * 
 */
public class ConsultaDeudaCastigosItemsCargosBB extends ListableBB {

	/**
	 * Comment for <code>SERVICE_CONSULTA_DEUDA</code>
	 */
	// private final static String SERVICE_CONSULTA_DEUDA = "consultaDeudaServiceFinderMock";
	private final static String SERVICE_CONSULTA_DEUDA = "deudaTotalesServiceFinder";

	/**
	 * Comment for <code>SERVICE_CONSULTA_DEUDA_PAGOS</code>
	 */
	// private final static String SERVICE_CONSULTA_DEUDA_PAGOS = "consultaDeudaPagosServiceFinderMock";
	private final static String SERVICE_CONSULTA_DEUDA_PAGOS = "deudaUltimoPagoServiceFinder";

	/**
	 * Comment for <code>SERVICE_CONSULTA_DEUDA_CONSUMOS</code>
	 */
	// private final static String SERVICE_CONSULTA_DEUDA_CONSUMOS = "consultaDeudaConsumosServiceFinderMock";
	private final static String SERVICE_CONSULTA_DEUDA_CONSUMOS = "deudaDatosServElectServiceFinder";

	/**
	 * Comment for <code>SERVICE_CONSULTA_DEUDA_CONVENIOS</code>
	 */
	// private final static String SERVICE_CONSULTA_DEUDA_CONVENIOS =
	// "consultaDeudaConveniosServiceFinderMock";
	private final static String SERVICE_CONSULTA_DEUDA_CONVENIOS = "deudaDatosConveniosServiceFinder";

	private final static String SERVICE_CONSULTA_DEUDA_MESES_SIN_LECTURAS = "consultaDeudaMesesSinLecturasServiceFinder";

	/**
	 * Comment for <code>SERVICE_CONSULTA_DEUDA_CONVENIOS_CONCEPTOS</code>
	 */
	// private final static String SERVICE_CONSULTA_DEUDA_CONVENIOS_CONCEPTOS =
	// "consultaDeudaConveniosConceptosServiceFinderMock";
	private final static String SERVICE_CONSULTA_DEUDA_CONVENIOS_CONCEPTOS = "deudaOtrosConveniosServiceFinder";

	/**
	 * Comment for <code>SERVICE_CONSULTA_DEUDA_CODENSA_HOGAR</code>
	 */
	private final static String SERVICE_CONSULTA_DEUDA_CODENSA_HOGAR = "consultaDeudaCodensaHogarFinder";

	/**
	 * Comment for <code>SERVICE_CONSULTA_CASTIGO_CARTERA</code>
	 */
	// private final static String SERVICE_CONSULTA_CASTIGO_CARTERA = "consultaCastigoCarteraFinderMock";
	private final static String SERVICE_CONSULTA_CASTIGO_CARTERA = "deudaResumenDatosCastigoCarteraServiceFinder";

	/**
	 * Comment for <code>SERVICE_CONSULTA_CASTIGO_CARTERA</code>
	 */
	private final static String SERVICE_CONSULTA_DEUDA_INFORMACION_REFACTURACION = "consultaDeudaInformacionRefacturacionServiceFinder";

	/**
	 * Comment for <code>consultaDeuda</code>
	 */
	private BusinessObject consultaDeuda;

	/**
	 * Comment for <code>consultaDeudaPagos</code>
	 */
	private BusinessObject consultaDeudaPagos;

	/**
	 * Comment for <code>consultaDeudaConsumos</code>
	 */
	private BusinessObject consultaDeudaConsumos;

	/**
	 * Comment for <code>consultaDeudaConvenios</code>
	 */
	private BusinessObject consultaDeudaConvenios;

	private BusinessObject consultaDeudaMesesSinLecturas;

	/**
	 * Comment for <code>consultaDeudaConveniosConceptos</code>
	 */
	private BusinessObject consultaDeudaConveniosConceptos;

	/**
	 * Comment for <code>consultaDeudaCodensaHogar</code>
	 */
	private BusinessObject consultaDeudaCodensaHogar;

	/**
	 * Comment for <code>consultaCastigoCartera</code>
	 */
	private BusinessObject consultaCastigoCartera;

	/**
	 * Comment for <code>consultaDeudaInformacionRefacturacion</code>
	 */
	private BusinessObject consultaDeudaInformacionRefacturacion;

	/**
	 * @param serviceNname
	 * @return
	 */
	private BusinessObject directUniqueCall(String serviceNname) {
		String a = StringUtils.substringBefore(serviceNname, "Service");

		try {
			return this.getService(serviceNname).findByCriteriaUnique(this.getQueryFilter("numeroCuentaQueryFilter"));
		}
		catch (ObjectNotFoundException e) {
			ErrorsManager.addError(e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null,
				MessageUtils.getMessage("error.plain", new Object[] { e.getMessage() + " " + a }));
			return null;
		}
		catch (IncorrectResultSizeDataAccessException idae) {
			idae.printStackTrace();
			ErrorsManager.addError(idae.getMessage());
			FacesContext.getCurrentInstance().addMessage(null,
				MessageUtils.getMessage("error.plain", new Object[] { idae.getMessage() + " " + a }));
			return null;
		}
		catch (Throwable t) {
			t.printStackTrace();
			ErrorsManager.addError(t.getMessage());
			FacesContext.getCurrentInstance().addMessage(null,
				MessageUtils.getMessage("error.plain", new Object[] { t.getMessage() + " " + a }));

			return null;

		}

		/**
		 * public String buscarAction() { try {
		 * this.setBusinessObject(getService().findByCriteriaUnique(queryFilter)); } catch
		 * (ObjectNotFoundException oe) { this.setBusinessObject(null); // Fix, si la busqueda se realizada 2
		 * veces quedaba cacheado ErrorsManager.addErrorMessage(oe.getMessage()); } return
		 * getDynamicOutcome(); }
		 */
	}

	// emilz @ code xD

	private DetalleCargos detalleCargos;

	private String detalleDeudaServiceName;

	public String doSearchAction() {
		this.getDetalleDeudaServiceName();
		QueryFilter filter = null;
		String serviceName = ((CustomQueryFilter) filter).getServiceName();
		this.fillDataTable("detalleCargos", filter, serviceName);
		return "detalleCargos";
	}

	private void fillDataTable(String dataTableId, QueryFilter queryFilter, String serviceName) {
		HtmlDataTable dataTable = (HtmlDataTable) JSFUtils.getComponentFromTree(dataTableId);
		if (dataTable != null) {
			dataTable.setValue(new ScrollableList(queryFilter, serviceName));
			dataTable.setFirst(0);
		}
	}

	public void fillData() {
		this.consultaDeudaPagos = this.directUniqueCall(SERVICE_CONSULTA_DEUDA_PAGOS);
		this.consultaDeuda = this.directUniqueCall(SERVICE_CONSULTA_DEUDA);
		this.consultaDeudaConsumos = this.directUniqueCall(SERVICE_CONSULTA_DEUDA_CONSUMOS);
		this.consultaDeudaConvenios = this.directUniqueCall(SERVICE_CONSULTA_DEUDA_CONVENIOS);
		this.consultaDeudaMesesSinLecturas = this.directUniqueCall(SERVICE_CONSULTA_DEUDA_MESES_SIN_LECTURAS);
	}

	public void fillData2() {
		this.consultaDeudaConveniosConceptos = this.directUniqueCall(SERVICE_CONSULTA_DEUDA_CONVENIOS_CONCEPTOS);
		this.consultaDeudaCodensaHogar = this.directUniqueCall(SERVICE_CONSULTA_DEUDA_CODENSA_HOGAR);
		this.consultaCastigoCartera = this.directUniqueCall(SERVICE_CONSULTA_CASTIGO_CARTERA);
		this.consultaDeudaInformacionRefacturacion = this
			.directUniqueCall(SERVICE_CONSULTA_DEUDA_INFORMACION_REFACTURACION);

	}

	/**
	 * @param serviceFinder
	 * @return
	 */
	private FinderService getService(String serviceFinder) {
		return (FinderService) SynergiaApplicationContext.findService(serviceFinder);
	}

	/**
	 * @param name
	 * @return
	 */
	private QueryFilter getQueryFilter(String name) {
		return (QueryFilter) VariableResolverUtils.getObject(name);
	}

	/**
	 * @return the detalleCargos
	 */
	public DetalleCargos getDetalleCargos() {
		return detalleCargos;
	}

	/**
	 * @param detalleCargos the detalleCargos to set
	 */
	public void setDetalleCargos(DetalleCargos detalleCargos) {
		this.detalleCargos = detalleCargos;
	}

	/**
	 * @return the detalleDeudaServiceName
	 */
	public String getDetalleDeudaServiceName() {
		return detalleDeudaServiceName;
	}

	/**
	 * @param detalleDeudaServiceName the detalleDeudaServiceName to set
	 */
	public void setDetalleDeudaServiceName(String detalleDeudaServiceName) {
		this.detalleDeudaServiceName = detalleDeudaServiceName;
	}

	/**
	 * @return the consultaDeuda
	 */
	public BusinessObject getConsultaDeuda() {
		return this.consultaDeuda;
	}

	/**
	 * @param consultaDeuda the consultaDeuda to set
	 */
	public void setConsultaDeuda(BusinessObject consultaDeuda) {
		this.consultaDeuda = consultaDeuda;
	}

	/**
	 * @return the consultaDeudaPagos
	 */
	public BusinessObject getConsultaDeudaPagos() {
		return this.consultaDeudaPagos;
	}

	/**
	 * @param consultaDeudaPagos the consultaDeudaPagos to set
	 */
	public void setConsultaDeudaPagos(BusinessObject consultaDeudaPagos) {
		this.consultaDeudaPagos = consultaDeudaPagos;
	}

	/**
	 * @return the consultaDeudaConsumos
	 */
	public BusinessObject getConsultaDeudaConsumos() {
		return this.consultaDeudaConsumos;
	}

	/**
	 * @param consultaDeudaConsumos the consultaDeudaConsumos to set
	 */
	public void setConsultaDeudaConsumos(BusinessObject consultaDeudaConsumos) {
		this.consultaDeudaConsumos = consultaDeudaConsumos;
	}

	/**
	 * @return the consultaDeudaConvenios
	 */
	public BusinessObject getConsultaDeudaConvenios() {
		return this.consultaDeudaConvenios;
	}

	/**
	 * @param consultaDeudaConvenios the consultaDeudaConvenios to set
	 */
	public void setConsultaDeudaConvenios(BusinessObject consultaDeudaConvenios) {
		this.consultaDeudaConvenios = consultaDeudaConvenios;
	}

	public BusinessObject getConsultaDeudaMesesSinLecturas() {
		return this.consultaDeudaMesesSinLecturas;
	}

	public void setConsultaDeudaMesesSinLecturas(BusinessObject consultaDeudaMesesSinLecturas) {
		this.consultaDeudaMesesSinLecturas = consultaDeudaMesesSinLecturas;
	}

	/**
	 * @return the consultaDeudaConveniosConceptos
	 */
	public BusinessObject getConsultaDeudaConveniosConceptos() {
		return this.consultaDeudaConveniosConceptos;
	}

	/**
	 * @param consultaDeudaConveniosConceptos the consultaDeudaConveniosConceptos to set
	 */
	public void setConsultaDeudaConveniosConceptos(BusinessObject consultaDeudaConveniosConceptos) {
		this.consultaDeudaConveniosConceptos = consultaDeudaConveniosConceptos;
	}

	/**
	 * @return the consultaDeudaCodensaHogar
	 */
	public BusinessObject getConsultaDeudaCodensaHogar() {
		return this.consultaDeudaCodensaHogar;
	}

	/**
	 * @param consultaDeudaCodensaHogar the consultaDeudaCodensaHogar to set
	 */
	public void setConsultaDeudaCodensaHogar(BusinessObject consultaDeudaCodensaHogar) {
		this.consultaDeudaCodensaHogar = consultaDeudaCodensaHogar;
	}

	/**
	 * @return the consultaCastigoCartera
	 */
	public BusinessObject getConsultaCastigoCartera() {
		return this.consultaCastigoCartera;
	}

	/**
	 * @param consultaCastigoCartera the consultaCastigoCartera to set
	 */
	public void setConsultaCastigoCartera(BusinessObject consultaCastigoCartera) {
		this.consultaCastigoCartera = consultaCastigoCartera;
	}

	/**
	 * @return the consultaDeudaInformacionRefacturacion
	 */
	public BusinessObject getConsultaDeudaInformacionRefacturacion() {
		return this.consultaDeudaInformacionRefacturacion;
	}

	/**
	 * @param consultaDeudaInformacionRefacturacion the consultaDeudaInformacionRefacturacion to set
	 */
	public void setConsultaDeudaInformacionRefacturacion(BusinessObject consultaDeudaInformacionRefacturacion) {
		this.consultaDeudaInformacionRefacturacion = consultaDeudaInformacionRefacturacion;
	}
}