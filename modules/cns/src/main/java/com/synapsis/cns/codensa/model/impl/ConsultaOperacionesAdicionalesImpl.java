/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaOperacionesAdicionales;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */

public class ConsultaOperacionesAdicionalesImpl extends SynergiaBusinessObjectImpl implements ConsultaOperacionesAdicionales {
    private String tipoOperAdicional;
    private String codigoCargo;
    private String descripCargo;
    private String tipoCargo;
    private String tipoMoneda;
    private Date fechaCotizacion;
    private Double valor;
    private String estado;
    private String usuarioRegistro;
    private String observaciones;
    private Long nroCuenta;
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaOperacionesAdicionales#getCodigoCargo()
	 */
	public String getCodigoCargo() {
		return codigoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaOperacionesAdicionales#setCodigoCargo(java.lang.String)
	 */
	public void setCodigoCargo(String codigoCargo) {
		this.codigoCargo = codigoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaOperacionesAdicionales#getDescripCargo()
	 */
	public String getDescripCargo() {
		return descripCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaOperacionesAdicionales#setDescripCargo(java.lang.String)
	 */
	public void setDescripCargo(String descripCargo) {
		this.descripCargo = descripCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaOperacionesAdicionales#getEstado()
	 */
	public String getEstado() {
		return estado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaOperacionesAdicionales#setEstado(java.lang.String)
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaOperacionesAdicionales#getFechaCotizacion()
	 */
	public Date getFechaCotizacion() {
		return fechaCotizacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaOperacionesAdicionales#setFechaCotizacion(java.util.Date)
	 */
	public void setFechaCotizacion(Date fechaCotizacion) {
		this.fechaCotizacion = fechaCotizacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaOperacionesAdicionales#getObservaciones()
	 */
	public String getObservaciones() {
		return observaciones;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaOperacionesAdicionales#setObservaciones(java.lang.String)
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaOperacionesAdicionales#getTipoCargo()
	 */
	public String getTipoCargo() {
		return tipoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaOperacionesAdicionales#setTipoCargo(java.lang.String)
	 */
	public void setTipoCargo(String tipoCargo) {
		this.tipoCargo = tipoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaOperacionesAdicionales#getTipoMoneda()
	 */
	public String getTipoMoneda() {
		return tipoMoneda;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaOperacionesAdicionales#setTipoMoneda(java.lang.String)
	 */
	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaOperacionesAdicionales#getTipoOperAdicional()
	 */
	public String getTipoOperAdicional() {
		return tipoOperAdicional;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaOperacionesAdicionales#setTipoOperAdicional(java.lang.String)
	 */
	public void setTipoOperAdicional(String tipoOperAdicional) {
		this.tipoOperAdicional = tipoOperAdicional;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaOperacionesAdicionales#getUsuarioRegistro()
	 */
	public String getUsuarioRegistro() {
		return usuarioRegistro;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaOperacionesAdicionales#setUsuarioRegistro(java.lang.String)
	 */
	public void setUsuarioRegistro(String usuarioRegistro) {
		this.usuarioRegistro = usuarioRegistro;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaOperacionesAdicionales#getValor()
	 */
	public Double getValor() {
		return valor;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaOperacionesAdicionales#setValor(java.lang.Double)
	 */
	public void setValor(Double valor) {
		this.valor = valor;
	}
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

}
