package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaDeudaConsumos;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.1 $ $Date: 2007/02/28 21:47:47 $
 * 
 */
public class ConsultaDeudaConsumosImpl extends SynergiaBusinessObjectImpl
		implements ConsultaDeudaConsumos {

	/**
	 * Comment for <code>antiguedadDeuda</code>
	 */
	private Long antiguedadDeuda;

	/**
	 * Comment for <code>ultimoValorConsumoKw</code>
	 */
	private Long ultimoValorConsumoKw;

	/**
	 * Comment for <code>ultimoValorConsumoPesos</code>
	 */
	private Double ultimoValorConsumoPesos;

	/**
	 * Comment for <code>fechaFacturaUltimoConsumo</code>
	 */
	private Date fechaFacturaUltimoConsumo;

	/**
	 * Comment for <code>promedioConsumoUltimosMeses</code>
	 */
	private Integer promedioConsumoUltimosMeses;

	/**
	 * Comment for <code>estadoCobranza</code>
	 */
	private String estadoCobranza;

	/**
	 * @return the antiguedadDeuda
	 */
	public Long getAntiguedadDeuda() {
		return this.antiguedadDeuda;
	}

	/**
	 * @param antiguedadDeuda
	 *            the antiguedadDeuda to set
	 */
	public void setAntiguedadDeuda(Long antiguedadDeuda) {
		this.antiguedadDeuda = antiguedadDeuda;
	}

	/**
	 * @return the estadoCobranza
	 */
	public String getEstadoCobranza() {
		return this.estadoCobranza;
	}

	/**
	 * @param estadoCobranza
	 *            the estadoCobranza to set
	 */
	public void setEstadoCobranza(String estadoCobranza) {
		this.estadoCobranza = estadoCobranza;
	}

	/**
	 * @return the fechaFacturaUltimoConsumo
	 */
	public Date getFechaFacturaUltimoConsumo() {
		return this.fechaFacturaUltimoConsumo;
	}

	/**
	 * @param fechaFacturaUltimoConsumo
	 *            the fechaFacturaUltimoConsumo to set
	 */
	public void setFechaFacturaUltimoConsumo(Date fechaFacturaUltimoConsumo) {
		this.fechaFacturaUltimoConsumo = fechaFacturaUltimoConsumo;
	}

	/**
	 * @return the promedioConsumoUltimosMeses
	 */
	public Integer getPromedioConsumoUltimosMeses() {
		return this.promedioConsumoUltimosMeses;
	}

	/**
	 * @param promedioConsumoUltimosMeses
	 *            the promedioConsumoUltimosMeses to set
	 */
	public void setPromedioConsumoUltimosMeses(
			Integer promedioConsumoUltimosMeses) {
		this.promedioConsumoUltimosMeses = promedioConsumoUltimosMeses;
	}

	/**
	 * @return the ultimoValorConsumoKw
	 */
	public Long getUltimoValorConsumoKw() {
		return this.ultimoValorConsumoKw;
	}

	/**
	 * @param ultimoValorConsumoKw
	 *            the ultimoValorConsumoKw to set
	 */
	public void setUltimoValorConsumoKw(Long ultimoValorConsumoKw) {
		this.ultimoValorConsumoKw = ultimoValorConsumoKw;
	}

	/**
	 * @return the ultimoValorConsumoPesos
	 */
	public Double getUltimoValorConsumoPesos() {
		return this.ultimoValorConsumoPesos;
	}

	/**
	 * @param ultimoValorConsumoPesos
	 *            the ultimoValorConsumoPesos to set
	 */
	public void setUltimoValorConsumoPesos(Double ultimoValorConsumoPesos) {
		this.ultimoValorConsumoPesos = ultimoValorConsumoPesos;
	}

}