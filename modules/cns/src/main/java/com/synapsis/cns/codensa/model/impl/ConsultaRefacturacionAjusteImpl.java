/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaRefacturacionAjuste;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author dbraccio
 * CU 016
 *
 */
public class ConsultaRefacturacionAjusteImpl extends 
	SynergiaBusinessObjectImpl implements ConsultaRefacturacionAjuste {

	private Long nroCuenta;
	private Long idCuenta;
	
	private Long nroAjuste;
	private Long nroOrdenRefacturacion;
	private String servicioAjustado;
	private Double valorTotalAjustado;
	private Double cantidadKWAjustados;
	private Double valorAPagarDespuesReajuste;
	private Date fechaVencimientoPagoAjuste;
	private Date fechaYHoraRealizacion;
	private String usuarioRealizador;
	private String nombreUsuarioRealizador;
	private Date fechaYHoraAprobacion;
	private String usuariosAprobadores;
	private String nombresUsuariosAprobadores;
	
	//para el caso de eliminacion
	private Date fechaEliminacion;
	private String usuarioEliminador;
	private String nombreUsuarioEliminador;
	private String observacionesEliminacion;

	//para el detalle 
	private String areaSolicitante;
	private String areaResponsable;
	private String clasificacionAjuste;
	private String submotivoAjuste;
	private Double tiempoTranscurridoHastaAprobacion;
	
	

	public Double getCantidadKWAjustados() {
		return cantidadKWAjustados;
	}
	public Date getFechaEliminacion() {
		return fechaEliminacion;
	}
	public Date getFechaVencimientoPagoAjuste() {
		return fechaVencimientoPagoAjuste;
	}
	public Date getFechaYHoraAprobacion() {
		return fechaYHoraAprobacion;
	}
	public Date getFechaYHoraRealizacion() {
		return fechaYHoraRealizacion;
	}
	public Long getIdCuenta() {
		return idCuenta;
	}
	public String getNombresUsuariosAprobadores() {
		return nombresUsuariosAprobadores;
	}
	public String getNombreUsuarioEliminador() {
		return nombreUsuarioEliminador;
	}
	public String getNombreUsuarioRealizador() {
		return nombreUsuarioRealizador;
	}
	public Long getNroAjuste() {
		return nroAjuste;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public Long getNroOrdenRefacturacion() {
		return nroOrdenRefacturacion;
	}
	public String getObservacionesEliminacion() {
		return observacionesEliminacion;
	}
	public String getServicioAjustado() {
		return servicioAjustado;
	}
	public String getUsuarioEliminador() {
		return usuarioEliminador;
	}
	public String getUsuarioRealizador() {
		return usuarioRealizador;
	}
	public String getUsuariosAprobadores() {
		return usuariosAprobadores;
	}
	public Double getValorAPagarDespuesReajuste() {
		return valorAPagarDespuesReajuste;
	}
	public Double getValorTotalAjustado() {
		return valorTotalAjustado;
	}
	public String getAreaResponsable() {
		return areaResponsable;
	}
	public String getAreaSolicitante() {
		return areaSolicitante;
	}
	public String getClasificacionAjuste() {
		return clasificacionAjuste;
	}
	public String getSubmotivoAjuste() {
		return submotivoAjuste;
	}
	public Double getTiempoTranscurridoHastaAprobacion() {
		return tiempoTranscurridoHastaAprobacion;
	}
	public void setAreaResponsable(String areaResponsable) {
		this.areaResponsable = areaResponsable;
	}
	public void setAreaSolicitante(String areaSolicitante) {
		this.areaSolicitante = areaSolicitante;
	}
	public void setCantidadKWAjustados(Double cantidadKWAjustados) {
		this.cantidadKWAjustados = cantidadKWAjustados;
	}
	public void setClasificacionAjuste(String clasificacionAjuste) {
		this.clasificacionAjuste = clasificacionAjuste;
	}
	public void setFechaEliminacion(Date fechaEliminacion) {
		this.fechaEliminacion = fechaEliminacion;
	}
	public void setFechaVencimientoPagoAjuste(Date fechaVencimientoPagoAjuste) {
		this.fechaVencimientoPagoAjuste = fechaVencimientoPagoAjuste;
	}
	public void setFechaYHoraAprobacion(Date fechaYHoraAprobacion) {
		this.fechaYHoraAprobacion = fechaYHoraAprobacion;
	}
	public void setFechaYHoraRealizacion(Date fechaYHoraRealizacion) {
		this.fechaYHoraRealizacion = fechaYHoraRealizacion;
	}
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	public void setNombresUsuariosAprobadores(String nombresUsuariosAprobadores) {
		this.nombresUsuariosAprobadores = nombresUsuariosAprobadores;
	}
	public void setNombreUsuarioEliminador(String nombreUsuarioEliminador) {
		this.nombreUsuarioEliminador = nombreUsuarioEliminador;
	}
	public void setNombreUsuarioRealizador(String nombreUsuarioRealizador) {
		this.nombreUsuarioRealizador = nombreUsuarioRealizador;
	}
	public void setNroAjuste(Long nroAjuste) {
		this.nroAjuste = nroAjuste;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public void setNroOrdenRefacturacion(Long nroOrdenRefacturacion) {
		this.nroOrdenRefacturacion = nroOrdenRefacturacion;
	}
	public void setObservacionesEliminacion(String observacionesEliminacion) {
		this.observacionesEliminacion = observacionesEliminacion;
	}
	public void setServicioAjustado(String servicioAjustado) {
		this.servicioAjustado = servicioAjustado;
	}
	public void setSubmotivoAjuste(String submotivoAjuste) {
		this.submotivoAjuste = submotivoAjuste;
	}
	public void setTiempoTranscurridoHastaAprobacion(
			Double tiempoTranscurridoHastaAprobacion) {
		this.tiempoTranscurridoHastaAprobacion = tiempoTranscurridoHastaAprobacion;
	}
	public void setUsuarioEliminador(String usuarioEliminador) {
		this.usuarioEliminador = usuarioEliminador;
	}
	public void setUsuarioRealizador(String usuarioRealizador) {
		this.usuarioRealizador = usuarioRealizador;
	}
	public void setUsuariosAprobadores(String usuariosAprobadores) {
		this.usuariosAprobadores = usuariosAprobadores;
	}
	public void setValorAPagarDespuesReajuste(Double valorAPagarDespuesReajuste) {
		this.valorAPagarDespuesReajuste = valorAPagarDespuesReajuste;
	}
	public void setValorTotalAjustado(Double valorTotalAjustado) {
		this.valorTotalAjustado = valorTotalAjustado;
	}
	
	
	
	
	
}
