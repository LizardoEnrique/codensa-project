package com.synapsis.cns.codensa.model.buscaCuenta;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface BuscaCuentaNombreApellidoTitular extends SynergiaBusinessObject{

	/**
	 * @return the apellido
	 */
	public String getApellido();

	/**
	 * @return the nombre
	 */
	public String getNombre();

	/**
	 * @param apellido the apellido to set
	 */
	public void setApellido(String apellido);

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre);
	
	/**
	 * @return the apellidoMaterno
	 */
	public String getApellidoMaterno();
	/**
	 * @param apellidoMaterno the apellidoMaterno to set
	 */
	public void setApellidoMaterno(String apellidoMaterno);

}