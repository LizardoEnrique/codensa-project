package com.synapsis.cns.codensa.web;

/**
 * @author ccamba
 * 
 */
public class CUOrigenCertificaConveniosBB extends
		GeneracionAutomaticaContactoKendariBB {

	protected String getCasoUsoOrigen() {
		return this.getParamEncryptedAndEncoded("CNS032");
	}

}
