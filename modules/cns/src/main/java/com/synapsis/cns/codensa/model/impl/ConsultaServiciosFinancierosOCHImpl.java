package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaServiciosFinancierosOCH;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaServiciosFinancierosOCHImpl extends SynergiaBusinessObjectImpl implements ConsultaServiciosFinancierosOCH{

	  public String tipoServicioFinanciero;	
	  public Long nroServicioFinanciero;	  
	  public String nombreTitular;	  
	  public String nroIdentificacionSocio;	  
	  public String estado;	  
	  public Long nroServicio;	
	  public String lineaNegocio;	  
	  public String tipoProducto;	  
	  public String marcaProducto;	  
	  public Date fechaCompraProducto;	  
	  public Date fechaActivacionServicio;	  
	  public String nombreSocio;	  
	  public String sucursalSocio;	  
	  public String solicitanteCredito;	  
	  public String nroIdSolicitante;	  
	  public String resultadoEstadoCredito;	  
	  public Long montoAprobado;	  
	  public Long montoDisponible;	  
	  public Date fechaSolicitudCredito;	  
	  public String causalNegacion;
	  
	public String getCausalNegacion() {
		return causalNegacion;
	}
	public void setCausalNegacion(String causalNegacion) {
		this.causalNegacion = causalNegacion;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Date getFechaActivacionServicio() {
		return fechaActivacionServicio;
	}
	public void setFechaActivacionServicio(Date fechaActivacionServicio) {
		this.fechaActivacionServicio = fechaActivacionServicio;
	}
	public Date getFechaCompraProducto() {
		return fechaCompraProducto;
	}
	public void setFechaCompraProducto(Date fechaCompraProducto) {
		this.fechaCompraProducto = fechaCompraProducto;
	}
	public Date getFechaSolicitudCredito() {
		return fechaSolicitudCredito;
	}
	public void setFechaSolicitudCredito(Date fechaSolicitudCredito) {
		this.fechaSolicitudCredito = fechaSolicitudCredito;
	}
	public String getLineaNegocio() {
		return lineaNegocio;
	}
	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}
	public String getMarcaProducto() {
		return marcaProducto;
	}
	public void setMarcaProducto(String marcaProducto) {
		this.marcaProducto = marcaProducto;
	}
	public Long getMontoAprobado() {
		return montoAprobado;
	}
	public void setMontoAprobado(Long montoAprobado) {
		this.montoAprobado = montoAprobado;
	}
	public Long getMontoDisponible() {
		return montoDisponible;
	}
	public void setMontoDisponible(Long montoDisponible) {
		this.montoDisponible = montoDisponible;
	}
	public String getNombreSocio() {
		return nombreSocio;
	}
	public void setNombreSocio(String nombreSocio) {
		this.nombreSocio = nombreSocio;
	}
	public String getNombreTitular() {
		return nombreTitular;
	}
	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}
	public String getNroIdentificacionSocio() {
		return nroIdentificacionSocio;
	}
	public void setNroIdentificacionSocio(String nroIdentificacionSocio) {
		this.nroIdentificacionSocio = nroIdentificacionSocio;
	}
	public String getNroIdSolicitante() {
		return nroIdSolicitante;
	}
	public void setNroIdSolicitante(String nroIdSolicitante) {
		this.nroIdSolicitante = nroIdSolicitante;
	}
	public Long getNroServicio() {
		return nroServicio;
	}
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	public Long getNroServicioFinanciero() {
		return nroServicioFinanciero;
	}
	public void setNroServicioFinanciero(Long nroServicioFinanciero) {
		this.nroServicioFinanciero = nroServicioFinanciero;
	}
	public String getResultadoEstadoCredito() {
		return resultadoEstadoCredito;
	}
	public void setResultadoEstadoCredito(String resultadoEstadoCredito) {
		this.resultadoEstadoCredito = resultadoEstadoCredito;
	}
	public String getSolicitanteCredito() {
		return solicitanteCredito;
	}
	public void setSolicitanteCredito(String solicitanteCredito) {
		this.solicitanteCredito = solicitanteCredito;
	}
	public String getSucursalSocio() {
		return sucursalSocio;
	}
	public void setSucursalSocio(String sucursalSocio) {
		this.sucursalSocio = sucursalSocio;
	}
	public String getTipoProducto() {
		return tipoProducto;
	}
	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}
	public String getTipoServicioFinanciero() {
		return tipoServicioFinanciero;
	}
	public void setTipoServicioFinanciero(String tipoServicioFinanciero) {
		this.tipoServicioFinanciero = tipoServicioFinanciero;
	}
}
