/**
 * $Id: DetalleConsultaRefacturacionesAjustesBB.java,v 1.16 2008/05/18 20:26:37 ar26557682 Exp $
 */
package com.synapsis.cns.codensa.web;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlForm;

import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.queryFilter.SimpleQueryFilter;
import com.suivant.arquitectura.core.service.FinderService;
import com.suivant.arquitectura.presentation.model.ListableBB;
import com.suivant.arquitectura.presentation.utils.JSFUtils;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.synapsis.cns.codensa.model.RefacAprob;
import com.synapsis.cns.codensa.model.RefacPend;
import com.synapsis.commons.faces.messages.ErrorsManager;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;
import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * 
 * @author Paola Attadio
 * 
 */
public class DetalleConsultaRefacturacionesAjustesBB extends ListableBB {

	private SynergiaBusinessObject businessObject2;
	private String serviceNameBOBalanceAdjust;
	private QueryFilter queryFilterForBalanceAdjust;
	private SynergiaBusinessObject businessObject1;
	
	private SynergiaBusinessObject businessObject3;
	
	private SynergiaBusinessObject businessObject4;
	

	private String serviceNameBoSaldos;
	private QueryFilter queryFilterForSaldos;

	private String serviceNameBOMovimientosRefactAdjust;	
	private QueryFilter queryFilterForMovimientosRefactAdjust;

	
	/**
	 * Invocado por el on-load para cargar los datos del segundo tab
	 *
	 */
	public void buscarBalanceAdjustAction() {
		try {
			searchBusiness();
		} catch (ObjectNotFoundException e) {
			//ErrorsManager.addInfoError(e.getMessage());
			//e.printStackTrace();
		}
	}
	
	public void buscarSaldosAdjustAction() {
		try {
			searchBusiness4();
		} catch (ObjectNotFoundException e) {
			//ErrorsManager.addInfoError(e.getMessage());
			//e.printStackTrace();
		}
	}

	/**
	 * 
	 * @throws ObjectNotFoundException
	 */
	private void searchBusiness() throws ObjectNotFoundException {
		QueryFilter queryFilter = (QueryFilter) VariableResolverUtils.getObject("numeroAjusteQueryFilter");
		
		setBusinessObject3((SynergiaBusinessObject) getService(getServiceNameBOBalanceAdjust()).findByCriteriaUnique(queryFilter));				
	}
	
	/**
	 * 
	 * @throws ObjectNotFoundException
	 */
	private void searchBusiness4() throws ObjectNotFoundException {
		QueryFilter queryFilter = (QueryFilter) VariableResolverUtils.getObject("numeroAjusteQueryFilter");
		
		setBusinessObject4((SynergiaBusinessObject) getService("refacSaldosServiceFinder").findByCriteriaUnique(queryFilter));				
	}

	public void searchMovimientoRefacAjuste() {

		try {
			searchMovimientoRefacAjusteImpl();
		} catch (ObjectNotFoundException e) {
			ErrorsManager.addInfoError(e.getMessage());
		}
	}

	
	private void searchMovimientoRefacAjusteImpl() throws ObjectNotFoundException {

		setBusinessObject1((SynergiaBusinessObject) getService(getServiceNameBOMovimientosRefactAdjust()).findByCriteriaUnique(getQueryFilterForMovimientosRefactAdjust()));
		QueryFilter queryFilter = (QueryFilter) VariableResolverUtils.getObject("idOrdenQueryFilter");
		//consultaMovimientosDisposicionLegalDAO
		setBusinessObject2((SynergiaBusinessObject) this.getService("consultaMovDispLegalServiceFinder").findByCriteriaUnique(queryFilter));		
	}

	
	/** 
	 * Devuleve un servicio pidiendoselo al SynergiaApplicacontext
	 */
	private FinderService getService(String service) {
		return (FinderService) SynergiaApplicationContext
				.findService(service);
	}

	/**
	 * 
	 * @return
	 */
	public QueryFilter getQueryFilterForBalanceAdjust() {
		return queryFilterForBalanceAdjust;
	}

	/**
	 * 
	 * @param queryFilterForBalanceAdjust
	 */
	public void setQueryFilterForBalanceAdjust(
			QueryFilter queryFilterForBalanceAdjust) {
		this.queryFilterForBalanceAdjust = queryFilterForBalanceAdjust;
	}

	/**
	 * 
	 * @return
	 */
	public String getServiceNameBOBalanceAdjust() {
		return serviceNameBOBalanceAdjust;
	}

	/**
	 * 
	 * @param serviceNameBOBalanceAdjust
	 */
	public void setServiceNameBOBalanceAdjust(String serviceNameBOBalanceAdjust) {
		this.serviceNameBOBalanceAdjust = serviceNameBOBalanceAdjust;
	}

	/**
	 * 
	 * @param businessObject2
	 */
	public void setBusinessObject2(SynergiaBusinessObject businessObject2) {
		this.businessObject2 = businessObject2;
	}

	/**
	 * 
	 * @return
	 */
	public SynergiaBusinessObject getBusinessObject1() {
		return businessObject1;
	}

	/**
	 * 
	 * @param businessObject1
	 */
	public void setBusinessObject1(SynergiaBusinessObject businessObject1) {
		this.businessObject1 = businessObject1;
	}

	/**
	 * 
	 * @return
	 */
	public SynergiaBusinessObject getBusinessObject2() {
		return businessObject2;
	}

	/**
	 * 
	 * @return
	 */
	public String getServiceNameBoSaldos() {
		return serviceNameBoSaldos;
	}

	/**
	 * 
	 * @param serviceNameBOBalanceAdjust
	 */
	public void setServiceNameBoSaldos(String ServiceNameBoSaldos) {
		this.serviceNameBoSaldos = ServiceNameBoSaldos;
	}

	/**
	 * 
	 * @return
	 */
	public QueryFilter getQueryFilterForSaldos() {
		return queryFilterForSaldos;
	}

	/**
	 * 
	 * @param queryFilterForSaldos
	 */
	public void setQueryFilterForSaldos(QueryFilter queryFilterForSaldos) {
		this.queryFilterForSaldos = queryFilterForSaldos;
	}

	public SynergiaBusinessObject getBusinessObject3() {
		return businessObject3;
	}

	public void setBusinessObject3(SynergiaBusinessObject businessObject3) {
		this.businessObject3 = businessObject3;
	}
	
	
	
	public String getServiceNameBOMovimientosRefactAdjust() {
		return serviceNameBOMovimientosRefactAdjust;
	}

	public void setServiceNameBOMovimientosRefactAdjust(
			String serviceNameBOMovimientosRefactAdjust) {
		this.serviceNameBOMovimientosRefactAdjust = serviceNameBOMovimientosRefactAdjust;
	}

	public QueryFilter getQueryFilterForMovimientosRefactAdjust() {
		return queryFilterForMovimientosRefactAdjust;
	}

	public void setQueryFilterForMovimientosRefactAdjust(
			QueryFilter queryFilterForMovimientosRefactAdjust) {
		this.queryFilterForMovimientosRefactAdjust = queryFilterForMovimientosRefactAdjust;
	}

	public SynergiaBusinessObject getBusinessObject4() {
		return businessObject4;
	}

	public void setBusinessObject4(SynergiaBusinessObject businessObject4) {
		this.businessObject4 = businessObject4;
	}
	
	
	
}
