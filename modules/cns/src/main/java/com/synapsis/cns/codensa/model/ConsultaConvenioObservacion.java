package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaConvenioObservacion extends SynergiaBusinessObject {
	
	public Long getNroCuenta();
	public void setNroCuenta(Long nroCuenta);
	public String getDescripcionObservacion();
	public void setDescripcionObservacion(String descripcionObservacion);
	public Long getNroConvenio();
	public void setNroConvenio(Long nroConvenio);
	public Long getTipoObservacion();
	public void setTipoObservacion(Long tipoObservacion);
}