package com.synapsis.cns.codensa.model;


/**
 * Interfaz del producto financiero Suscripciones Codensa, para el reporte de duplicado de anexo de factura (Servicios Financieros)
 *    
 * @author jhack
 */
public interface ProductoSuscripcionesCodensa extends ServicioFinancieroECO{


}
