package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaOrdenesInspeccion;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaOrdenesInspeccionImpl extends SynergiaBusinessObjectImpl implements ConsultaOrdenesInspeccion {
	
	private Long idOrden;
	private Long nroCuenta;
	private Long nroServicio;
	private String clase;
	private String subClase;
	private String estrato;
	private String nroOrden;
	private String tipoInspeccion;
	private String estadoOrden;
	private String tipoResultado;
	private Date fechaCreacion;
	private Date fechaFinEjecucion;
	private Date fechaInicioEjecucion;
	private String nroComponente;
	private Double lecturaActivaFP;
	private Double lecturaActivaHP;
	private Double lecturaReactivaFP;
	private Double lecturaReactivaHP;
	private String observacionesInspeccion;
	
	public Long getIdOrden() {
		return idOrden;
	}
	
	public void setIdOrden(Long idOrden) {
		this.idOrden = idOrden;
	}
	
	public Long getNroCuenta() {
		return nroCuenta;
	}
	
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	
	public Long getNroServicio() {
		return nroServicio;
	}
	
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	
	public String getClase() {
		return clase;
	}
	
	public void setClase(String clase) {
		this.clase = clase;
	}
	
	public String getSubClase() {
		return subClase;
	}
	
	public void setSubClase(String subClase) {
		this.subClase = subClase;
	}
	
	public String getEstrato() {
		return estrato;
	}
	
	public void setEstrato(String estrato) {
		this.estrato = estrato;
	}
	
	public String getNroOrden() {
		return nroOrden;
	}
	
	public void setNroOrden(String nroOrden) {
		this.nroOrden = nroOrden;
	}
	
	public String getTipoInspeccion() {
		return tipoInspeccion;
	}
	
	public void setTipoInspeccion(String tipoInspeccion) {
		this.tipoInspeccion = tipoInspeccion;
	}
	
	public String getEstadoOrden() {
		return estadoOrden;
	}
	
	public void setEstadoOrden(String estadoOrden) {
		this.estadoOrden = estadoOrden;
	}
	
	public String getTipoResultado() {
		return tipoResultado;
	}
	
	public void setTipoResultado(String tipoResultado) {
		this.tipoResultado = tipoResultado;
	}
	
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	public Date getFechaFinEjecucion() {
		return fechaFinEjecucion;
	}
	
	public void setFechaFinEjecucion(Date fechaFinEjecucion) {
		this.fechaFinEjecucion = fechaFinEjecucion;
	}
	
	public Date getFechaInicioEjecucion() {
		return fechaInicioEjecucion;
	}
	
	public void setFechaInicioEjecucion(Date fechaInicioEjecucion) {
		this.fechaInicioEjecucion = fechaInicioEjecucion;
	}
	
	public String getNroComponente() {
		return nroComponente;
	}
	
	public void setNroComponente(String nroComponente) {
		this.nroComponente = nroComponente;
	}
	
	public Double getLecturaActivaFP() {
		return lecturaActivaFP;
	}
	
	public void setLecturaActivaFP(Double lecturaActivaFP) {
		this.lecturaActivaFP = lecturaActivaFP;
	}
	
	public Double getLecturaActivaHP() {
		return lecturaActivaHP;
	}
	
	public void setLecturaActivaHP(Double lecturaActivaHP) {
		this.lecturaActivaHP = lecturaActivaHP;
	}
	
	public Double getLecturaReactivaFP() {
		return lecturaReactivaFP;
	}
	
	public void setLecturaReactivaFP(Double lecturaReactivaFP) {
		this.lecturaReactivaFP = lecturaReactivaFP;
	}
	
	public Double getLecturaReactivaHP() {
		return lecturaReactivaHP;
	}
	
	public void setLecturaReactivaHP(Double lecturaReactivaHP) {
		this.lecturaReactivaHP = lecturaReactivaHP;
	}
	
	public String getObservacionesInspeccion() {
		return observacionesInspeccion;
	}
	
	public void setObservacionesInspeccion(String observacionesInspeccion) {
		this.observacionesInspeccion = observacionesInspeccion;
	}	
}
