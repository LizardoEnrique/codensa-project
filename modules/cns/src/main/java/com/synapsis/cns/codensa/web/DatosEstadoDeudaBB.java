package com.synapsis.cns.codensa.web;

import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.model.BusinessObject;
import com.suivant.arquitectura.core.service.FinderService;
import com.suivant.arquitectura.presentation.model.ViewBB;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;

public class DatosEstadoDeudaBB extends ViewBB {
	private BusinessObject bobResumenDeuda;

	private BusinessObject bobEstadoServicio;

	private String serviceNameBOResumenDeuda;

	private String serviceNameBOEstadoServicio;

	/**
	 * @return the bobEstadoServicio
	 */
	public BusinessObject getBobEstadoServicio() {
		return bobEstadoServicio;
	}

	/**
	 * @param bobEstadoServicio
	 *            the bobEstadoServicio to set
	 */
	public void setBobEstadoServicio(BusinessObject bobEstadoServicio) {
		this.bobEstadoServicio = bobEstadoServicio;
	}

	/**
	 * @return the bobResumenDeuda
	 */
	public BusinessObject getBobResumenDeuda() {
		return bobResumenDeuda;
	}

	/**
	 * @param bobResumenDeuda
	 *            the bobResumenDeuda to set
	 */
	public void setBobResumenDeuda(BusinessObject bobResumenDeuda) {
		this.bobResumenDeuda = bobResumenDeuda;
	}

	/**
	 * @return the serviceNameBOEstadoServicio
	 */
	public String getServiceNameBOEstadoServicio() {
		return serviceNameBOEstadoServicio;
	}

	/**
	 * @param serviceNameBOEstadoServicio
	 *            the serviceNameBOEstadoServicio to set
	 */
	public void setServiceNameBOEstadoServicio(
			String serviceNameBOEstadoServicio) {
		this.serviceNameBOEstadoServicio = serviceNameBOEstadoServicio;
	}

	/**
	 * @return the serviceNameBOResumenDeuda
	 */
	public String getServiceNameBOResumenDeuda() {
		return serviceNameBOResumenDeuda;
	}

	/**
	 * @param serviceNameBOResumenDeuda
	 *            the serviceNameBOResumenDeuda to set
	 */
	public void setServiceNameBOResumenDeuda(String serviceNameBOResumenDeuda) {
		this.serviceNameBOResumenDeuda = serviceNameBOResumenDeuda;
	}

	private void searchBusiness() {
		try {
			setBobResumenDeuda(getService(getServiceNameBOResumenDeuda())
					.findByCriteriaUnique(getQueryFilter()));
		} catch (ObjectNotFoundException e) {
			// ErrorsManager.addInfoMessage(e.getMessage());
		}
		try {
			setBobEstadoServicio(getService(getServiceNameBOEstadoServicio())
					.findByCriteriaUnique(getQueryFilter()));
		} catch (ObjectNotFoundException e) {
			// ErrorsManager.addInfoMessage(e.getMessage());
		}

	}

	public void buscarActionEstadoDeuda() {
		searchBusiness();
	}

	private FinderService getService(String serviceFinder) {
		return (FinderService) SynergiaApplicationContext
				.findService(serviceFinder);
	}

}
