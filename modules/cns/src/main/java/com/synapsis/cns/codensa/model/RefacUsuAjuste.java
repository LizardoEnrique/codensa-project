package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface RefacUsuAjuste extends SynergiaBusinessObject {

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public String getNroAjuste();

	public void setNroAjuste(String nroAjuste);

	public String getNombreUsuarios();

	public void setNombreUsuarios(String nombreUsuarios);

	public Date getFechaIntervencion();

	public void setFechaIntervencion(Date fechaIntervencion);

	public String getAccionEjecutada();

	public void setAccionEjecutada(String accionEjecutada);

	public Double getTiemposIntervUsuarios();

	public void setTiemposIntervUsuarios(Double tiemposIntervUsuarios);

	public String getObs();

	public void setObs(String obs);

}