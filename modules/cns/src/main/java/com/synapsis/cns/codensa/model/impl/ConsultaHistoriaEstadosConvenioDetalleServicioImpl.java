/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaHistoriaEstadosConvenioDetalleServicio;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 *
 */
public class ConsultaHistoriaEstadosConvenioDetalleServicioImpl extends SynergiaBusinessObjectImpl implements ConsultaHistoriaEstadosConvenioDetalleServicio {

	private static final long serialVersionUID = 1L;
	
	private Long nroIdentificacion;
	private String nombreSolicitante;
	private String resultadoEstudio;
	private Long montoAprobado;
	private Long montoDisponible;
	private Date fechaSolicitudCredito;
	private String causalNegacion;
	private String estrato;
	private String cicloFacturacion;
	private String lineaNegocio;
	private String productoCodensaHogar;
	private String marcaProducto;
	private Date fechaCompraProducto;
	private Long valorOriginalServicio;
	private String socioRealizadaCompra;
	private String puntoVentaCompra;
	private String nroCuotasOriginales;
	private String nroCuotasFacturaras;
	private String nroCuotasFaltanetes;
	private Long tasaInteresOrigen;
	private Long saldoCapitalServicio;
	private Long saldoInteresCorriente;
	private Long saldoInteresMora;
	private String periodosAtrasados;
	private Long diasMora;
	private Long valorCapitalMora;
	private Long valorInteresesMora;
	
	
	
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getCausalNegacion()
	 */
	public String getCausalNegacion() {
		return causalNegacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setCausalNegacion(java.lang.String)
	 */
	public void setCausalNegacion(String causalNegacion) {
		this.causalNegacion = causalNegacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getCicloFacturacion()
	 */
	public String getCicloFacturacion() {
		return cicloFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setCicloFacturacion(java.lang.String)
	 */
	public void setCicloFacturacion(String cicloFacturacion) {
		this.cicloFacturacion = cicloFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getDiasMora()
	 */
	public Long getDiasMora() {
		return diasMora;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setDiasMora(java.lang.Long)
	 */
	public void setDiasMora(Long diasMora) {
		this.diasMora = diasMora;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getEstrato()
	 */
	public String getEstrato() {
		return estrato;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setEstrato(java.lang.String)
	 */
	public void setEstrato(String estrato) {
		this.estrato = estrato;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getFechaCompraProducto()
	 */
	public Date getFechaCompraProducto() {
		return fechaCompraProducto;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setFechaCompraProducto(java.util.Date)
	 */
	public void setFechaCompraProducto(Date fechaCompraProducto) {
		this.fechaCompraProducto = fechaCompraProducto;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getFechaSolicitudCredito()
	 */
	public Date getFechaSolicitudCredito() {
		return fechaSolicitudCredito;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setFechaSolicitudCredito(java.util.Date)
	 */
	public void setFechaSolicitudCredito(Date fechaSolicitudCredito) {
		this.fechaSolicitudCredito = fechaSolicitudCredito;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getLineaNegocio()
	 */
	public String getLineaNegocio() {
		return lineaNegocio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setLineaNegocio(java.lang.String)
	 */
	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getMarcaProducto()
	 */
	public String getMarcaProducto() {
		return marcaProducto;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setMarcaProducto(java.lang.String)
	 */
	public void setMarcaProducto(String marcaProducto) {
		this.marcaProducto = marcaProducto;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getMontoAprobado()
	 */
	public Long getMontoAprobado() {
		return montoAprobado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setMontoAprobado(java.lang.Long)
	 */
	public void setMontoAprobado(Long montoAprobado) {
		this.montoAprobado = montoAprobado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getMontoDisponible()
	 */
	public Long getMontoDisponible() {
		return montoDisponible;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setMontoDisponible(java.lang.Long)
	 */
	public void setMontoDisponible(Long montoDisponible) {
		this.montoDisponible = montoDisponible;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getNombreSolicitante()
	 */
	public String getNombreSolicitante() {
		return nombreSolicitante;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setNombreSolicitante(java.lang.String)
	 */
	public void setNombreSolicitante(String nombreSolicitante) {
		this.nombreSolicitante = nombreSolicitante;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getNroCuotasFacturaras()
	 */
	public String getNroCuotasFacturaras() {
		return nroCuotasFacturaras;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setNroCuotasFacturaras(java.lang.String)
	 */
	public void setNroCuotasFacturaras(String nroCuotasFacturaras) {
		this.nroCuotasFacturaras = nroCuotasFacturaras;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getNroCuotasFaltanetes()
	 */
	public String getNroCuotasFaltanetes() {
		return nroCuotasFaltanetes;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setNroCuotasFaltanetes(java.lang.String)
	 */
	public void setNroCuotasFaltanetes(String nroCuotasFaltanetes) {
		this.nroCuotasFaltanetes = nroCuotasFaltanetes;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getNroCuotasOriginales()
	 */
	public String getNroCuotasOriginales() {
		return nroCuotasOriginales;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setNroCuotasOriginales(java.lang.String)
	 */
	public void setNroCuotasOriginales(String nroCuotasOriginales) {
		this.nroCuotasOriginales = nroCuotasOriginales;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getNroIdentificacion()
	 */
	public Long getNroIdentificacion() {
		return nroIdentificacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setNroIdentificacion(java.lang.Long)
	 */
	public void setNroIdentificacion(Long nroIdentificacion) {
		this.nroIdentificacion = nroIdentificacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getPeriodosAtrasados()
	 */
	public String getPeriodosAtrasados() {
		return periodosAtrasados;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setPeriodosAtrasados(java.lang.String)
	 */
	public void setPeriodosAtrasados(String periodosAtrasados) {
		this.periodosAtrasados = periodosAtrasados;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getProductoCodensaHogar()
	 */
	public String getProductoCodensaHogar() {
		return productoCodensaHogar;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setProductoCodensaHogar(java.lang.String)
	 */
	public void setProductoCodensaHogar(String productoCodensaHogar) {
		this.productoCodensaHogar = productoCodensaHogar;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getPuntoVentaCompra()
	 */
	public String getPuntoVentaCompra() {
		return puntoVentaCompra;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setPuntoVentaCompra(java.lang.String)
	 */
	public void setPuntoVentaCompra(String puntoVentaCompra) {
		this.puntoVentaCompra = puntoVentaCompra;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getResultadoEstudio()
	 */
	public String getResultadoEstudio() {
		return resultadoEstudio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setResultadoEstudio(java.lang.String)
	 */
	public void setResultadoEstudio(String resultadoEstudio) {
		this.resultadoEstudio = resultadoEstudio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getSaldoCapitalServicio()
	 */
	public Long getSaldoCapitalServicio() {
		return saldoCapitalServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setSaldoCapitalServicio(java.lang.Long)
	 */
	public void setSaldoCapitalServicio(Long saldoCapitalServicio) {
		this.saldoCapitalServicio = saldoCapitalServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getSaldoInteresCorriente()
	 */
	public Long getSaldoInteresCorriente() {
		return saldoInteresCorriente;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setSaldoInteresCorriente(java.lang.Long)
	 */
	public void setSaldoInteresCorriente(Long saldoInteresCorriente) {
		this.saldoInteresCorriente = saldoInteresCorriente;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getSaldoInteresMora()
	 */
	public Long getSaldoInteresMora() {
		return saldoInteresMora;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setSaldoInteresMora(java.lang.Long)
	 */
	public void setSaldoInteresMora(Long saldoInteresMora) {
		this.saldoInteresMora = saldoInteresMora;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getSocioRealizadaCompra()
	 */
	public String getSocioRealizadaCompra() {
		return socioRealizadaCompra;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setSocioRealizadaCompra(java.lang.String)
	 */
	public void setSocioRealizadaCompra(String socioRealizadaCompra) {
		this.socioRealizadaCompra = socioRealizadaCompra;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getTasaInteresOrigen()
	 */
	public Long getTasaInteresOrigen() {
		return tasaInteresOrigen;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setTasaInteresOrigen(java.lang.Long)
	 */
	public void setTasaInteresOrigen(Long tasaInteresOrigen) {
		this.tasaInteresOrigen = tasaInteresOrigen;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getValorCapitalMora()
	 */
	public Long getValorCapitalMora() {
		return valorCapitalMora;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setValorCapitalMora(java.lang.Long)
	 */
	public void setValorCapitalMora(Long valorCapitalMora) {
		this.valorCapitalMora = valorCapitalMora;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getValorInteresesMora()
	 */
	public Long getValorInteresesMora() {
		return valorInteresesMora;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setValorInteresesMora(java.lang.Long)
	 */
	public void setValorInteresesMora(Long valorInteresesMora) {
		this.valorInteresesMora = valorInteresesMora;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#getValorOriginalServicio()
	 */
	public Long getValorOriginalServicio() {
		return valorOriginalServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicio#setValorOriginalServicio(java.lang.Long)
	 */
	public void setValorOriginalServicio(Long valorOriginalServicio) {
		this.valorOriginalServicio = valorOriginalServicio;
	}
}