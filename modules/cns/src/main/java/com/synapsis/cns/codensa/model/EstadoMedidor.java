package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author m3.MarioRoss - 14/11/2006
 * refactor dBraccio	- 29/11/2006
 */
public interface EstadoMedidor extends SynergiaBusinessObject {

	public Long getActivaFP();

	public void setActivaFP(Long activaFP);

	public Long getActivaHP();

	public void setActivaHP(Long activaHP);

	public Long getActivaXP();

	public void setActivaXP(Long activaXP);

	public Long getConsecutivoFactura();

	public void setConsecutivoFactura(Long consecutivoFactura);

	public Long getDecimalesMedidor();

	public void setDecimalesMedidor(Long decimalesMedidor);

	public Long getEnterosMedidor();

	public void setEnterosMedidor(Long enterosMedidor);

	public Date getFechaCambioSistema();

	public void setFechaCambioSistema(Date fechaCambioSistema);

	public Date getFechaCambioTerreno();

	public void setFechaCambioTerreno(Date fechaCambioTerreno);

	public Long getIdMedidor();

	public void setIdMedidor(Long idMedidor);

	public String getMedida();

	public void setMedida(String medida);

	public String getMotivo();

	public void setMotivo(String motivo);

	public String getNombreUsuario();

	public void setNombreUsuario(String nombreUsuario);

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public String getObservaciones();

	public void setObservaciones(String observaciones);

	public Long getReactivaFP();

	public void setReactivaFP(Long reactivaFP);

	public Long getReactivaHP();

	public void setReactivaHP(Long reactivaHP);

	public Long getReactivaXP();

	public void setReactivaXP(Long reactivaXP);

	public String getUsuario();

	public void setUsuario(String usuario);


}