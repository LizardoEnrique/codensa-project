/**
 * 
 */
package com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada;

import com.synapsis.cns.codensa.model.buscaCuenta.avanzada.BuscaCuentaAvanzadaMunicipio;
import com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaImpl;

/**
 * @author Emiliano Arango (ar30557486)
 * 
 */
public class BuscaCuentaAvanzadaMunicipioImpl extends BuscaCuentaImpl implements BuscaCuentaAvanzadaMunicipio {

	private String municipio;
	private Long sucursal;
	private Long ciclo;
	private Long grupo;
	private String manzana;
	private Long zona;
	private Long idManzana;
	private Long idMunicipio;
	private Long departamento;
	private String correlativo;
	
	/**
	 * @return the ciclo
	 */
	public Long getCiclo() {
		return ciclo;
	}
	/**
	 * @param ciclo the ciclo to set
	 */
	public void setCiclo(Long ciclo) {
		this.ciclo = ciclo;
	}
	/**
	 * @return the departamento
	 */
	public Long getDepartamento() {
		return departamento;
	}
	/**
	 * @param departamento the departamento to set
	 */
	public void setDepartamento(Long departamento) {
		this.departamento = departamento;
	}
	/**
	 * @return the grupo
	 */
	public Long getGrupo() {
		return grupo;
	}
	/**
	 * @param grupo the grupo to set
	 */
	public void setGrupo(Long grupo) {
		this.grupo = grupo;
	}
	/**
	 * @return the idManzana
	 */
	public Long getIdManzana() {
		return idManzana;
	}
	/**
	 * @param idManzana the idManzana to set
	 */
	public void setIdManzana(Long idManzana) {
		this.idManzana = idManzana;
	}
	/**
	 * @return the idMunicipio
	 */
	public Long getIdMunicipio() {
		return idMunicipio;
	}
	/**
	 * @param idMunicipio the idMunicipio to set
	 */
	public void setIdMunicipio(Long idMunicipio) {
		this.idMunicipio = idMunicipio;
	}
	/**
	 * @return the manzana
	 */
	public String getManzana() {
		return manzana;
	}
	/**
	 * @param manzana the manzana to set
	 */
	public void setManzana(String manzana) {
		this.manzana = manzana;
	}
	/**
	 * @return the municipio
	 */
	public String getMunicipio() {
		return municipio;
	}
	/**
	 * @param municipio the municipio to set
	 */
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	/**
	 * @return the sucursal
	 */
	public Long getSucursal() {
		return sucursal;
	}
	/**
	 * @param sucursal the sucursal to set
	 */
	public void setSucursal(Long sucursal) {
		this.sucursal = sucursal;
	}
	/**
	 * @return the zona
	 */
	public Long getZona() {
		return zona;
	}
	/**
	 * @param zona the zona to set
	 */
	public void setZona(Long zona) {
		this.zona = zona;
	}
	public String getCorrelativo() {
		return correlativo;
	}
	public void setCorrelativo(String correlativo) {
		this.correlativo = correlativo;
	}	
	
	
}