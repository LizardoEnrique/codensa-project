/**
 * $Id: ConsultaLecturaConsumoBB.java,v 1.15 2008/08/20 18:28:15 ar18817018 Exp $ 
 */
package com.synapsis.cns.codensa.web;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.event.ActionEvent;

import org.apache.myfaces.custom.tabbedpane.HtmlPanelTabbedPane;

import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.model.BusinessObject;
import com.suivant.arquitectura.core.queryFilter.CompositeQueryFilter;
import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.service.FinderService;
import com.suivant.arquitectura.presentation.model.ListableBB;
import com.suivant.arquitectura.presentation.utils.JSFUtils;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.synapsis.commons.faces.messages.ErrorsManager;
import com.synapsis.synergia.core.components.dataTable.ScrollableList;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;
import com.synapsis.synergia.core.integration.ListableData;

/**
 * 
 * @author Paola Attadio
 *
 */
public class ConsultaLecturaConsumoBB extends ListableBB {
	
	/**objeto que hace referencia a datos generales */
	BusinessObject businessObject1;
	String serviceNameBOAuditHead;
	QueryFilter queryFilterForAudit;
	
	public ConsultaLecturaConsumoBB() {
		EqualQueryFilter queryFilter = (EqualQueryFilter) VariableResolverUtils
		.getObject("numeroCuentaQueryFilter");
		Object parameter = JSFUtils.getParameter("nroCuenta");
		if (parameter != null) {
			Long nroCuentaaa = new Long(parameter.toString());
			queryFilter.setAttributeValue(nroCuentaaa);
		}		
	}	
	

	public void buscarAuditHeader(){
		try {
			searchBusiness();
		} catch (ObjectNotFoundException e) {
			ErrorsManager.addInfoError(e.getMessage());
		}
	}
	
	private void searchBusiness() throws ObjectNotFoundException {			
		setBusinessObject1(getService(getServiceNameBOAuditHead()
				).findByCriteriaUnique(getQueryFilterForAudit()));
	}
	
	public void viewDetailsTab1(ActionEvent event) {
				
		// retain the idMedidor specified ...
		String idMedidor = (String) JSFUtils.getParameter("idMedidor");

		EqualQueryFilter queryFilter = (EqualQueryFilter) 
			VariableResolverUtils.getObject("medidorConsumoQueryFilter");
		queryFilter.setAttributeValue(new Long(idMedidor));

		CompositeQueryFilter queryCompositeFilter = (CompositeQueryFilter)
			VariableResolverUtils.getObject("consultaConsumoQueryFilter");
		queryCompositeFilter.getFiltersList().add(queryFilter);

		HtmlDataTable dataTable = (HtmlDataTable) 
			JSFUtils.getComponentFromTree("form1:tab1:subview1:table1");
		
		if (dataTable != null) {
			dataTable.setValue(new ScrollableList(queryCompositeFilter,
					((ListableData) super.getMultiList().getData().get("consultaLecturaConsumo"))
							.getServiceName(), 
							((ListableData) super.getMultiList().getData().get("consultaLecturaConsumo")).getMetadata()));
			dataTable.setFirst(0);
		}
		
		HtmlPanelTabbedPane tab =(HtmlPanelTabbedPane) 
			JSFUtils.getComponentFromTree("form1:_id12");
		tab.setSelectedIndex(0);
	}
	
	public void viewDetailsTab5(ActionEvent event) {
		// retain the idMedidor specified ...
		String idMedidor = (String) JSFUtils.getParameter("idMedidor");

		EqualQueryFilter queryFilter = (EqualQueryFilter) 
			VariableResolverUtils.getObject("medidorConsumoQueryFilter");
		queryFilter.setAttributeValue(new Long(idMedidor));

		CompositeQueryFilter queryCompositeFilter = (CompositeQueryFilter)
			VariableResolverUtils.getObject("consultaConsumoQueryFilter");
		queryCompositeFilter.getFiltersList().add(queryFilter);

		HtmlDataTable dataTable = (HtmlDataTable) 
			JSFUtils.getComponentFromTree("form1:tab5:table5");
		
		if (dataTable != null) {
			dataTable.setValue(new ScrollableList(queryCompositeFilter,
					((ListableData) super.getMultiList().getData().get("consultaLecturaConsumoReliquidacion"))
							.getServiceName(),
				    ((ListableData)	super.getMultiList().getData().get("consultaLecturaConsumoReliquidacion"))
							.getMetadata()));
			dataTable.setFirst(0);
		}
		
		HtmlPanelTabbedPane tab =(HtmlPanelTabbedPane) JSFUtils.getComponentFromTree("form1:_id12");
		tab.setSelectedIndex(4);
	}

	public BusinessObject getBusinessObject1() {
		return businessObject1;
	}

	public void setBusinessObject1(BusinessObject businessObject1) {
		this.businessObject1 = businessObject1;
	}

	public String getServiceNameBOAuditHead() {
		return serviceNameBOAuditHead;
	}

	public void setServiceNameBOAuditHead(String serviceNameBOAuditHead) {
		this.serviceNameBOAuditHead = serviceNameBOAuditHead;
	}

	//	me devuelve un servicio pidiendoselo al SynergiaApplicacontext
	private FinderService getService(String serviceFinder){
		return (FinderService)SynergiaApplicationContext.findService(
				serviceFinder);
	}

	public QueryFilter getQueryFilterForAudit() {
		return queryFilterForAudit;
	}

	public void setQueryFilterForAudit(QueryFilter queryFilterForAudit) {
		this.queryFilterForAudit = queryFilterForAudit;
	}
	
	

}