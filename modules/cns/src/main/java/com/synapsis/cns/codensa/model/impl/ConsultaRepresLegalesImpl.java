/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaRepresLegales;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author AR18799631
 *
 */
public class ConsultaRepresLegalesImpl extends SynergiaBusinessObjectImpl implements ConsultaRepresLegales {

	private Long nroCuenta;
	private Long nroServicio;
	private String representante;
	private String documento;
	private String telefono;
	private String email;
	private String direccion;
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaRepresLegales#getDireccion()
	 */
	public String getDireccion() {
		return direccion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaRepresLegales#setDireccion(java.lang.String)
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaRepresLegales#getDocumento()
	 */
	public String getDocumento() {
		return documento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaRepresLegales#setDocumento(java.lang.String)
	 */
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaRepresLegales#getEmail()
	 */
	public String getEmail() {
		return email;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaRepresLegales#setEmail(java.lang.String)
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaRepresLegales#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaRepresLegales#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaRepresLegales#getNroServicio()
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaRepresLegales#setNroServicio(java.lang.Long)
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaRepresLegales#getRepresentante()
	 */
	public String getRepresentante() {
		return representante;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaRepresLegales#setRepresentante(java.lang.String)
	 */
	public void setRepresentante(String representante) {
		this.representante = representante;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaRepresLegales#getTelefono()
	 */
	public String getTelefono() {
		return telefono;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaRepresLegales#setTelefono(java.lang.String)
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

}
