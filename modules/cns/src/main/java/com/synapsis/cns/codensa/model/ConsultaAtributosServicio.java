package com.synapsis.cns.codensa.model;

public interface ConsultaAtributosServicio {

	/**
	 * @return the clienteAfianzado
	 */
	public String getClienteAfianzado();

	/**
	 * @param clienteAfianzado the clienteAfianzado to set
	 */
	public void setClienteAfianzado(String clienteAfianzado);

	/**
	 * @return the clienteFiador
	 */
	public String getClienteFiador();

	/**
	 * @param clienteFiador the clienteFiador to set
	 */
	public void setClienteFiador(String clienteFiador);

	/**
	 * @return the cobranzaJudicial
	 */
	public String getCobranzaJudicial();

	/**
	 * @param cobranzaJudicial the cobranzaJudicial to set
	 */
	public void setCobranzaJudicial(String cobranzaJudicial);

	/**
	 * @return the debitoAutBancos
	 */
	public String getDebitoAutBancos();

	/**
	 * @param debitoAutBancos the debitoAutBancos to set
	 */
	public void setDebitoAutBancos(String debitoAutBancos);

	/**
	 * @return the demandaJudicial
	 */
	public String getDemandaJudicial();

	/**
	 * @param demandaJudicial the demandaJudicial to set
	 */
	public void setDemandaJudicial(String demandaJudicial);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio();

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio);

	/**
	 * @return the tieneKit
	 */
	public String getTieneKit();

	/**
	 * @param tieneKit the tieneKit to set
	 */
	public void setTieneKit(String tieneKit);

	/**
	 * @return the tip
	 */
	public String getTip();

	/**
	 * @param tip the tip to set
	 */
	public void setTip(String tip);

}