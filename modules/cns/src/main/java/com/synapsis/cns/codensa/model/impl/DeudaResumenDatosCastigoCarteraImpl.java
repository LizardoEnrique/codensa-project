package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.cns.codensa.model.DeudaResumenDatosCastigoCartera;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class DeudaResumenDatosCastigoCarteraImpl extends
		SynergiaBusinessObjectImpl implements DeudaResumenDatosCastigoCartera {

	private Long nroCuenta;
	
	private Date fechaGeneracionDeuda;

	private String usuarioNombreIngresoCastigoDeuda;

	private String usuarioNombreIngresoDeuda;

	private String valorTotalCastigado;
	
	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}	

	public Date getFechaGeneracionDeuda() {
		return fechaGeneracionDeuda;
	}

	public void setFechaGeneracionDeuda(Date fechaGeneracionDeuda) {
		this.fechaGeneracionDeuda = fechaGeneracionDeuda;
	}

	public String getUsuarioNombreIngresoCastigoDeuda() {
		return usuarioNombreIngresoCastigoDeuda;
	}

	public void setUsuarioNombreIngresoCastigoDeuda(String usuarioNombreIngresoCastigoDeuda) {
		this.usuarioNombreIngresoCastigoDeuda = usuarioNombreIngresoCastigoDeuda;
	}

	public String getUsuarioNombreIngresoDeuda() {
		return usuarioNombreIngresoDeuda;
	}

	public void setUsuarioNombreIngresoDeuda(String usuarioNombreIngresoDeuda) {
		this.usuarioNombreIngresoDeuda = usuarioNombreIngresoDeuda;
	}

	public String getValorTotalCastigado() {
		return valorTotalCastigado;
	}

	public void setValorTotalCastigado(String valorTotalCastigado) {
		this.valorTotalCastigado = valorTotalCastigado;
	}
}