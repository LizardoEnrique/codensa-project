/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.DatosBeneficiario;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 * 
 */
public class DatosBeneficiarioImpl extends SynergiaBusinessObjectImpl implements
		DatosBeneficiario {

	private static final long serialVersionUID = 1L;

	/* leyenda 1 */
	private Long nroCuenta;
	private Long nroServicio;
	private String nombreB;
	private String documentoB;
	private String telefonoB;
	private String email;
	private String rutaLec;
	private String dirPredio;
	private String urbaniza;
	private String distrito;
	private Long idPersona;
	private String municipio;
	private Long idBeneficiario;
	private String correlativoConjunto;
	private boolean tieneCorrelativoConjunto;
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#getDirPredio()
	 */
	public String getDirPredio() {
		return dirPredio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#setDirPredio(java.lang.String)
	 */
	public void setDirPredio(String dirPredio) {
		this.dirPredio = dirPredio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#getDistrito()
	 */
	public String getDistrito() {
		return distrito;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#setDistrito(java.lang.String)
	 */
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#getDocumentoB()
	 */
	public String getDocumentoB() {
		return documentoB;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#setDocumentoB(java.lang.String)
	 */
	public void setDocumentoB(String documentoB) {
		this.documentoB = documentoB;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#getEmail()
	 */
	public String getEmail() {
		return email;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#setEmail(java.lang.String)
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#getNombreB()
	 */
	public String getNombreB() {
		return nombreB;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#setNombreB(java.lang.String)
	 */
	public void setNombreB(String nombreB) {
		this.nombreB = nombreB;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#getNroServicio()
	 */
	public Long getNroServicio() {
		return nroServicio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#setNroServicio(java.lang.Long)
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#getRutaLec()
	 */
	public String getRutaLec() {
		return rutaLec;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#setRutaLec(java.lang.String)
	 */
	public void setRutaLec(String rutaLec) {
		this.rutaLec = rutaLec;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#getTelefonoB()
	 */
	public String getTelefonoB() {
		return telefonoB;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#setTelefonoB(java.lang.String)
	 */
	public void setTelefonoB(String telefonoB) {
		this.telefonoB = telefonoB;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#getUrbaniza()
	 */
	public String getUrbaniza() {
		return urbaniza;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.edelnor.model.impl.DatosPersona#setUrbaniza(java.lang.String)
	 */
	public void setUrbaniza(String urbaniza) {
		this.urbaniza = urbaniza;
	}

	/**
	 * @return the idPersona
	 */
	public Long getIdPersona() {
		return idPersona;
	}

	/**
	 * @param idPersona
	 *            the idPersona to set
	 */
	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	/**
	 * @return the id_beneficiario
	 */
	public Long getIdBeneficiario() {
		return idBeneficiario;
	}

	/**
	 * @param id_beneficiario
	 *            the id_beneficiario to set
	 */
	public void setIdBeneficiario(Long idBeneficiario) {
		this.idBeneficiario = idBeneficiario;
	}

	/**
	 * @return the correlativoConjunto
	 */
	public String getCorrelativoConjunto() {
		return correlativoConjunto;
	}

	/**
	 * @param correlativoConjunto the correlativoConjunto to set
	 */
	public void setCorrelativoConjunto(String correlativoConjunto) {
		this.correlativoConjunto = correlativoConjunto;
	}

	/**
	 * @return the tieneCorrelativoConjunto
	 */
	public boolean isTieneCorrelativoConjunto() {
		return this.getCorrelativoConjunto() == null ? false : true;
	}


}