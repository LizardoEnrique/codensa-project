package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.EstadoMedidor;

public class EstadoMedidorImpl extends SynergiaBusinessObjectImpl 
	implements EstadoMedidor {
    // Fields
	
    private Long nroCuenta;
	private Date fechaCambioTerreno;
	private Long enterosMedidor;
	private Long decimalesMedidor;
    private String medida;
    private Long activaFP;
    private Long reactivaFP;
    private Long activaHP;
    private Long reactivaHP;
    private Long activaXP;
    private Long reactivaXP;
    private Long consecutivoFactura;
    private String usuario;
    private String nombreUsuario;
    private String motivo;   
	private Date fechaCambioSistema;
	private String observaciones;

    private Long idMedidor;

	public Long getActivaFP() {
		return activaFP;
	}

	public void setActivaFP(Long activaFP) {
		this.activaFP = activaFP;
	}

	public Long getActivaHP() {
		return activaHP;
	}

	public void setActivaHP(Long activaHP) {
		this.activaHP = activaHP;
	}

	public Long getActivaXP() {
		return activaXP;
	}

	public void setActivaXP(Long activaXP) {
		this.activaXP = activaXP;
	}

	public Long getConsecutivoFactura() {
		return consecutivoFactura;
	}

	public void setConsecutivoFactura(Long consecutivoFactura) {
		this.consecutivoFactura = consecutivoFactura;
	}

	public Long getDecimalesMedidor() {
		return decimalesMedidor;
	}

	public void setDecimalesMedidor(Long decimalesMedidor) {
		this.decimalesMedidor = decimalesMedidor;
	}

	public Long getEnterosMedidor() {
		return enterosMedidor;
	}

	public void setEnterosMedidor(Long enterosMedidor) {
		this.enterosMedidor = enterosMedidor;
	}

	public Date getFechaCambioSistema() {
		return fechaCambioSistema;
	}

	public void setFechaCambioSistema(Date fechaCambioSistema) {
		this.fechaCambioSistema = fechaCambioSistema;
	}

	public Date getFechaCambioTerreno() {
		return fechaCambioTerreno;
	}

	public void setFechaCambioTerreno(Date fechaCambioTerreno) {
		this.fechaCambioTerreno = fechaCambioTerreno;
	}

	public Long getIdMedidor() {
		return idMedidor;
	}

	public void setIdMedidor(Long idMedidor) {
		this.idMedidor = idMedidor;
	}

	public String getMedida() {
		return medida;
	}

	public void setMedida(String medida) {
		this.medida = medida;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Long getReactivaFP() {
		return reactivaFP;
	}

	public void setReactivaFP(Long reactivaFP) {
		this.reactivaFP = reactivaFP;
	}

	public Long getReactivaHP() {
		return reactivaHP;
	}

	public void setReactivaHP(Long reactivaHP) {
		this.reactivaHP = reactivaHP;
	}

	public Long getReactivaXP() {
		return reactivaXP;
	}

	public void setReactivaXP(Long reactivaXP) {
		this.reactivaXP = reactivaXP;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

   
}
