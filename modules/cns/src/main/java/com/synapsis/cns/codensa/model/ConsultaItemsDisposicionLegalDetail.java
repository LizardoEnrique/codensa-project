package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.Empresa;
import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaItemsDisposicionLegalDetail extends SynergiaBusinessObject {

	public Long getCodigoItem();
	public void setCodigoItem(Long codigoItem);
	
	public Double getConsumoKW();
	public void setConsumoKW(Double consumoKW);
	public String getDescItem();
	public void setDescItem(String descItem);
	public Empresa getEmpresa();
	public void setEmpresa(Empresa empresa);
	public String getFecPeriodo();
	public void setFecPeriodo(String fecPeriodo);
	public Long getId();
	public void setId(Long id);
	public Long getIdCargo();
	public void setIdCargo(Long idCargo);
	public String getCargoDispLegal();
	public void setCargoDispLegal(String cargoDispLegal);
	public Double getValorAfterAjuste();
	public void setValorAfterAjuste(Double valorAfterAjuste);
	public Double getValorAjuste();
	public void setValorAjuste(Double valorAjuste);
	public Double getValorBeforeAjuste();
	public void setValorBeforeAjuste(Double valorBeforeAjuste);

}
