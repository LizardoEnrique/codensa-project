package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaServiciosECO extends SynergiaBusinessObject {
	
	  public String getTipoServicioFinanciero();
	  public void setTipoServicioFinanciero(String tipoServicioFinanciero);
	  public Long getNroServicioFinanciero();
	  public void setNroServicioFinanciero(Long nroServicioFinanciero);
	  public String getNombreTitular();
	  public void setNombreTitular (String nombreTitular);
	  public String getNroIdentificacionSocio();
	  public void setNroIdentificacionSocio(String nroIdentificacionSocio);
	  public String getTipoIdentificacionTitular();
	  public void setTipoIdentificacionTitular(String tipoIdentificacionTitular);
	  public String getNumeroIdentificacionTitular();
	  public void setNumeroIdentificacionTitular(String numeroIdentificacionTitular);
	  public String getLineaNegocio();
	  public void setLineaNegocio(String lineaNegocio);
	  public Long getNumeroECO();
	  public void setNumeroECO(Long numeroECO);
	  public String getNumeroIdECO();
	  public void setNumeroIdECO(String numeroIdECO);
	  public String getSocio();
	  public void setSocio(String socio);
	  public String getProducto();
	  public void setProducto(String producto);
	  public String getPlan();
	  public void setPlan(String plan);
	  public Long getNumeroCuotas();
	  public void setNumeroCuotas(Long numeroCuotas);
	  public Long getValorCuotas();
	  public void setValorCuotas(Long valorCuotas);
	  public String getIndicadorSaldoAnterior();
	  public void setIndicadorSaldoAnterior(String indicadorSaldoAnterior);
	  public Date getFechaActivacion();
	  public void setFechaActivacion(Date fechaActivacion);
	  public Long getNumeroDeCuotasFacturadas();
	  public void setNumeroDeCuotasFacturadas(Long numeroDeCuotasFacturadas);
	  public String getNumeroDeCuotasFaltantes();
	  public void setNumeroDeCuotasFaltantes(String numeroDeCuotasFaltantes);
	  public Long getSaldoECO();
	  public void setSaldoECO(Long saldoECO);
	  public Long getSaldoInteresMora();
	  public void setSaldoInteresMora(Long saldoInteresMora);
	  public Long getPeriodosAtrasados();
	  public void setPeriodosAtrasados(Long periodosAtrasados);
	  public Long getDiasMora();
	  public void setDiasMora(Long diasMora);
	  public Long getValorCuotasMora();
	  public void setValorCuotasMora(Long valorCuotasMora);

}
