package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * Impresión duplicado factura - Tipos de Servicio del Cliente
 * 
 * @author adambrosio
 */
public interface TipoServicio extends SynergiaBusinessObject {
	public Long getNroCuenta();
	public void setNroCuenta(Long nroFactura);
	public Long getNroServicio();
	public void setNroServicio(Long nroServicio);
	public String getTipoServicio();
	public void setTipoServicio(String tipoServicio);
}
