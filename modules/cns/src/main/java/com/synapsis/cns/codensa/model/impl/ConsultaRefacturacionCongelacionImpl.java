/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaRefacturacionCongelacion;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author dbraccio
 *
 *CU 016
 */
public class ConsultaRefacturacionCongelacionImpl extends
		SynergiaBusinessObjectImpl implements ConsultaRefacturacionCongelacion{

	private Long idCuenta;
	private Long nroCuenta;
	private Long idOperacion;
	private Long nroOperacion;
	private Double valorDeOperacionSaldoDisputa;
	private Double kwCongeladosODescongelados;
	private Date fechaRealizacion;
	private String usuarioRealizador;
	private Date fechaAprobacion;
	private String usuariosAprobadores;
	
	//para las pendientes
	private String usuarioQueDebeAprobar;
	
	//para las eliminadas
	private Date fechaEliminacion;
	private String usuarioEliminador;
	
	//para el detalle
	private String tipoOperacion;
	private String areaSolicitante;
	private String cicloDeLaCuenta;
	private String analistaRealizador;
	private Long nroOrden;
	private String clasificacion;
	private String condicionFinalDeCuenta;
	private Date fechaCiclo;
	private Double tiempoCongelacionTransitoria;
	private Double tiempoTranscurrido;
	private Date fechaVencimiento;
	private Date horaRealizacion;
	
	public String getAnalistaRealizador() {
		return analistaRealizador;
	}
	public String getAreaSolicitante() {
		return areaSolicitante;
	}
	public String getCicloDeLaCuenta() {
		return cicloDeLaCuenta;
	}
	public String getClasificacion() {
		return clasificacion;
	}
	public String getCondicionFinalDeCuenta() {
		return condicionFinalDeCuenta;
	}
	public Date getFechaAprobacion() {
		return fechaAprobacion;
	}
	public Date getFechaCiclo() {
		return fechaCiclo;
	}
	public Date getFechaEliminacion() {
		return fechaEliminacion;
	}
	public Date getFechaRealizacion() {
		return fechaRealizacion;
	}
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}
	public Date getHoraRealizacion() {
		return horaRealizacion;
	}
	public Long getIdCuenta() {
		return idCuenta;
	}
	public Double getKwCongeladosODescongelados() {
		return kwCongeladosODescongelados;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public Long getNroOperacion() {
		return nroOperacion;
	}
	public Long getNroOrden() {
		return nroOrden;
	}
	public Double getTiempoCongelacionTransitoria() {
		return tiempoCongelacionTransitoria;
	}
	public Double getTiempoTranscurrido() {
		return tiempoTranscurrido;
	}
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	public String getUsuarioEliminador() {
		return usuarioEliminador;
	}
	public String getUsuarioQueDebeAprobar() {
		return usuarioQueDebeAprobar;
	}
	public String getUsuarioRealizador() {
		return usuarioRealizador;
	}
	public String getUsuariosAprobadores() {
		return usuariosAprobadores;
	}
	public Double getValorDeOperacionSaldoDisputa() {
		return valorDeOperacionSaldoDisputa;
	}
	
	public Long getIdOperacion() {
		return idOperacion;
	}
	public void setIdOperacion(Long idOperacion) {
		this.idOperacion = idOperacion;
	}
	public void setAnalistaRealizador(String analistaRealizador) {
		this.analistaRealizador = analistaRealizador;
	}
	public void setAreaSolicitante(String areaSolicitante) {
		this.areaSolicitante = areaSolicitante;
	}
	public void setCicloDeLaCuenta(String cicloDeLaCuenta) {
		this.cicloDeLaCuenta = cicloDeLaCuenta;
	}
	public void setClasificacion(String clasificacion) {
		this.clasificacion = clasificacion;
	}
	public void setCondicionFinalDeCuenta(String condicionFinalDeCuenta) {
		this.condicionFinalDeCuenta = condicionFinalDeCuenta;
	}
	public void setFechaAprobacion(Date fechaAprobacion) {
		this.fechaAprobacion = fechaAprobacion;
	}
	public void setFechaCiclo(Date fechaCiclo) {
		this.fechaCiclo = fechaCiclo;
	}
	public void setFechaEliminacion(Date fechaEliminacion) {
		this.fechaEliminacion = fechaEliminacion;
	}
	public void setFechaRealizacion(Date fechaRealizacion) {
		this.fechaRealizacion = fechaRealizacion;
	}
	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public void setHoraRealizacion(Date horaRealizacion) {
		this.horaRealizacion = horaRealizacion;
	}
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	public void setKwCongeladosODescongelados(Double kwCongeladosODescongelados) {
		this.kwCongeladosODescongelados = kwCongeladosODescongelados;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public void setNroOperacion(Long nroOperacion) {
		this.nroOperacion = nroOperacion;
	}
	public void setNroOrden(Long nroOrden) {
		this.nroOrden = nroOrden;
	}
	public void setTiempoCongelacionTransitoria(Double tiempoCongelacionTransitoria) {
		this.tiempoCongelacionTransitoria = tiempoCongelacionTransitoria;
	}
	public void setTiempoTranscurrido(Double tiempoTranscurrido) {
		this.tiempoTranscurrido = tiempoTranscurrido;
	}
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	public void setUsuarioEliminador(String usuarioEliminador) {
		this.usuarioEliminador = usuarioEliminador;
	}
	public void setUsuarioQueDebeAprobar(String usuarioQueDebeAprobar) {
		this.usuarioQueDebeAprobar = usuarioQueDebeAprobar;
	}
	public void setUsuarioRealizador(String usuarioRealizador) {
		this.usuarioRealizador = usuarioRealizador;
	}
	public void setUsuariosAprobadores(String usuariosAprobadores) {
		this.usuariosAprobadores = usuariosAprobadores;
	}
	public void setValorDeOperacionSaldoDisputa(Double valorDeOperacionSaldoDisputa) {
		this.valorDeOperacionSaldoDisputa = valorDeOperacionSaldoDisputa;
	}
	
	
	
	
	
	
	
	
}
