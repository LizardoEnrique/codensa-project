/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaRefacturacionCongelacionAmpliacion;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author dbraccio
 *
 *CU 016
 */
public class ConsultaRefacturacionCongelacionAmpliacionImpl extends
		SynergiaBusinessObjectImpl implements ConsultaRefacturacionCongelacionAmpliacion {

	private Long idCuenta;
	private Long nroCuenta;
	private Long idOperacion;
	private Long nroOperacion;
	private Date fechaAmpliacion;
	private String soporteAmpliacion;
	private String observacionAmpliacion;
	
	public Date getFechaAmpliacion() {
		return fechaAmpliacion;
	}
	public Long getIdCuenta() {
		return idCuenta;
	}
	public Long getIdOperacion() {
		return idOperacion;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public Long getNroOperacion() {
		return nroOperacion;
	}
	public String getObservacionAmpliacion() {
		return observacionAmpliacion;
	}
	public String getSoporteAmpliacion() {
		return soporteAmpliacion;
	}
	public void setFechaAmpliacion(Date fechaAmpliacion) {
		this.fechaAmpliacion = fechaAmpliacion;
	}
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	public void setIdOperacion(Long idOperacion) {
		this.idOperacion = idOperacion;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public void setNroOperacion(Long nroOperacion) {
		this.nroOperacion = nroOperacion;
	}
	public void setObservacionAmpliacion(String observacionAmpliacion) {
		this.observacionAmpliacion = observacionAmpliacion;
	}
	public void setSoporteAmpliacion(String soporteAmpliacion) {
		this.soporteAmpliacion = soporteAmpliacion;
	}
	
	
	
}
