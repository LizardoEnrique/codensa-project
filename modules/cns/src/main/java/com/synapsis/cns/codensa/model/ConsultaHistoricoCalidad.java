package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.Empresa;
import com.synapsis.synergia.core.model.SynergiaBusinessObject;
/**
 * 
 * @author lcanas
 */
public interface ConsultaHistoricoCalidad extends SynergiaBusinessObject {

	public String getNroTransformador();
	public void setNroTransformador(String nroTransformador);

	public Long getDuracion();
	public void setDuracion(Long duracion);

	public Date getFechaIncidencia();
	public void setFechaIncidencia(Date fechaIncidencia);

	public String getReferencia();
	public void setReferencia(String referencia);

	public Empresa getEmpresa();
	public void setEmpresa(Empresa empresa);

	public Long getIdCalidadIndiv();
	public void setIdCalidadIndiv(Long idCalidadIndiv);

}
