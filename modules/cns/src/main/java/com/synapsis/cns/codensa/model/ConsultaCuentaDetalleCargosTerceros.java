package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaCuentaDetalleCargosTerceros extends SynergiaBusinessObject {

	/**
	 * @return the cargo
	 */
	public String getCargo();

	/**
	 * @param cargo the cargo to set
	 */
	public void setCargo(String cargo);

	/**
	 * @return the codigoCargo
	 */
	public String getCodigoCargo();

	/**
	 * @param codigoCargo the codigoCargo to set
	 */
	public void setCodigoCargo(String codigoCargo);

	/**
	 * @return the empresa
	 */
	public String getEmpresaDet();

	/**
	 * @param empresa the empresa to set
	 */
	public void setEmpresaDet(String empresa);

	/**
	 * @return the fechaProceso
	 */
	public Date getFechaProceso();

	/**
	 * @param fechaProceso the fechaProceso to set
	 */
	public void setFechaProceso(Date fechaProceso);

	/**
	 * @return the identificador
	 */
	public String getIdentificador();

	/**
	 * @param identificador the identificador to set
	 */
	public void setIdentificador(String identificador);



	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the Valor
	 */
	public Long getValor();

	/**
	 * @param Valor the Valor to set
	 */
	public void setValor(Long Valor);

	/**
	 * @return the producto
	 */
	public String getProducto();

	/**
	 * @param producto the producto to set
	 */
	public void setProducto(String producto);

}