package com.synapsis.cns.codensa.web;

/**
 * @author dbraccio - assert solutions
 * @egrande REFACTOR: creo ConsultaPrincipalUniqueBB (13/08/2009)
 * 
 * 14/05/2008
 */
public class ConsultaOrdenCorteBB extends ConsultaPrincipalUniqueBB {

	public void buscarConsultaOrdenCorte() {
		findObject("corteService");
	}

}
