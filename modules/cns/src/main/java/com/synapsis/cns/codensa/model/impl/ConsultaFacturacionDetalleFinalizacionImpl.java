/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.ConsultaFacturacionDetalleFinalizacion;

/**
 * @author ar30557486
 * 
 */
public class ConsultaFacturacionDetalleFinalizacionImpl extends
SynergiaBusinessObjectImpl implements ConsultaFacturacionDetalleFinalizacion {

	private String periodo;

	private Date fechaFacturacion;
	
	private Date fechaCreacion;	

	private Long numeroEncargoConvenio;

	private Double tasaInteresConvenio;

	private Double valorCuota;

	private Double interesCuota;

	private Integer numeroCuotas;

	private Integer vecesFacturado;
	
	private Integer cuotasPendientes;

	private Double interesMora;

	private Date fechaAlta;
	
	private String tipoServicio;	

	private String nombreCliente;

	private String numeroDocumento;

	private String identificacion;

	private String itemsFacturados;
	
	private Long nroCuenta;
		
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getFechaAlta()
	 */
	public Date getFechaAlta() {
		return fechaAlta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setFechaAlta(java.util.Date)
	 */
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getFechaFacturacion()
	 */
	public Date getFechaFacturacion() {
		return fechaFacturacion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setFechaFacturacion(java.util.Date)
	 */
	public void setFechaFacturacion(Date fechaFacturacion) {
		this.fechaFacturacion = fechaFacturacion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getIdentificacion()
	 */
	public String getIdentificacion() {
		return identificacion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setIdentificacion(java.lang.String)
	 */
	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getInteresCuota()
	 */
	public Double getInteresCuota() {
		return interesCuota;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setInteresCuota(java.lang.Integer)
	 */
	public void setInteresCuota(Double interesCuota) {
		this.interesCuota = interesCuota;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getInteresMora()
	 */
	public Double getInteresMora() {
		return interesMora;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setInteresMora(java.lang.Integer)
	 */
	public void setInteresMora(Double interesMora) {
		this.interesMora = interesMora;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getItemsFacturados()
	 */
	public String getItemsFacturados() {
		return itemsFacturados;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setItemsFacturados(java.lang.String)
	 */
	public void setItemsFacturados(String itemsFacturados) {
		this.itemsFacturados = itemsFacturados;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getNombreCliente()
	 */
	public String getNombreCliente() {
		return nombreCliente;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setNombreCliente(java.lang.String)
	 */
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getNumeroCuotas()
	 */
	public Integer getNumeroCuotas() {
		return numeroCuotas;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setNumeroCuotas(java.lang.Integer)
	 */
	public void setNumeroCuotas(Integer numeroCuotas) {
		this.numeroCuotas = numeroCuotas;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getNumeroDocumento()
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setNumeroDocumento(java.lang.Integer)
	 */
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getNumeroEncargoConvenio()
	 */
	public Long getNumeroEncargoConvenio() {
		return numeroEncargoConvenio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setNumeroEncargoConvenio(java.lang.Integer)
	 */
	public void setNumeroEncargoConvenio(Long numeroEncargoConvenio) {
		this.numeroEncargoConvenio = numeroEncargoConvenio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getPeriodo()
	 */
	public String getPeriodo() {
		return periodo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setPeriodo(java.util.Date)
	 */
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getTasaInteresConvenio()
	 */
	public Double getTasaInteresConvenio() {
		return tasaInteresConvenio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setTasaInteresConvenio(java.lang.Integer)
	 */
	public void setTasaInteresConvenio(Double tasaInteresConvenio) {
		this.tasaInteresConvenio = tasaInteresConvenio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getValorCuota()
	 */
	public Double getValorCuota() {
		return valorCuota;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setValorCuota(java.lang.Integer)
	 */
	public void setValorCuota(Double valorCuota) {
		this.valorCuota = valorCuota;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getVecesFacturado()
	 */
	public Integer getVecesFacturado() {
		return vecesFacturado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setVecesFacturado(java.lang.Integer)
	 */
	public void setVecesFacturado(Integer vecesFacturado) {
		this.vecesFacturado = vecesFacturado;
	}

	/**
	 * @return the cuotasPendientes
	 */
	public Integer getCuotasPendientes() {
		return cuotasPendientes;
	}

	/**
	 * @param cuotasPendientes the cuotasPendientes to set
	 */
	public void setCuotasPendientes(Integer cuotasPendientes) {
		this.cuotasPendientes = cuotasPendientes;
	}

	/**
	 * @return the tipoServicio
	 */
	public String getTipoServicio() {
		return tipoServicio;
	}

	/**
	 * @param tipoServicio the tipoServicio to set
	 */
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	/**
	 * @return the fechaCreacion
	 */
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	/**
	 * @param fechaCreacion the fechaCreacion to set
	 */
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
}