package com.synapsis.cns.codensa.model.impl;

import java.util.Set;

import com.synapsis.cns.codensa.model.ProductoSeguroCodensa;

/**
 * Producto Seguros Codensa para mostrar en el Reporte de Impresion de Anexo de Factura (CNS003)
 * 
 * @author jhack
 */
public class ProductoSeguroCodensaImpl extends ServicioFinancieroECOImpl implements ProductoSeguroCodensa {
	private static final long serialVersionUID = 1L;
	
	// Items
	Set itemsSegurosCodensa;

	public Set getItemsSegurosCodensa() {
		return itemsSegurosCodensa;
	}

	public void setItemsSegurosCodensa(Set itemsSegurosCodensa) {
		this.itemsSegurosCodensa = itemsSegurosCodensa;
	}

}