package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.AtencionAic;

/**
 * @author m3.MarioRoss - 13/11/2006
 */
public class AtencionAicImpl extends SynergiaBusinessObjectImpl 
	implements AtencionAic {

	private Long nroCuenta;
	private Long nroContacto;
	private Long idContactoDist;
	private String estado;
	private Long nroAtencion;
	private Long idAtencion;	
	private String tipoServicio;
	private Long nroServicio;
	private String tipo;
	private String descripcionArea;
	private String motivoCliente;
	private Date fechaCreacion;
	private Date fechaFinalizacion;
	private Date fechaVencimientoInterno;
	private Date fechaVencimientoLegal;
	private String usuarioResponsable;
	private String nombreResponsable;
	private String tipoRespuesta;
	
	private String tipoAtencionAnterior;
	private String areaInvolucradaAnterior;
	private String motivoClienteAnterior;
	
	
	
	public String getAreaInvolucradaAnterior() {
		return areaInvolucradaAnterior;
	}
	public void setAreaInvolucradaAnterior(String areaInvolucradaAnterior) {
		this.areaInvolucradaAnterior = areaInvolucradaAnterior;
	}
	public String getMotivoClienteAnterior() {
		return motivoClienteAnterior;
	}
	public void setMotivoClienteAnterior(String motivoClienteAnterior) {
		this.motivoClienteAnterior = motivoClienteAnterior;
	}
	public String getTipoAtencionAnterior() {
		return tipoAtencionAnterior;
	}
	public void setTipoAtencionAnterior(String tipoAtencionAnterior) {
		this.tipoAtencionAnterior = tipoAtencionAnterior;
	}
	public String getDescripcionArea() {
		return this.descripcionArea;
	}
	public void setDescripcionArea(String descripcionArea) {
		this.descripcionArea = descripcionArea;
	}
	public String getEstado() {
		return this.estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public Date getFechaFinalizacion() {
		return this.fechaFinalizacion;
	}
	public void setFechaFinalizacion(Date fechaFinalizacion) {
		this.fechaFinalizacion = fechaFinalizacion;
	}
	public Date getFechaVencimientoInterno() {
		return this.fechaVencimientoInterno;
	}
	public void setFechaVencimientoInterno(Date fechaVencimientoInterno) {
		this.fechaVencimientoInterno = fechaVencimientoInterno;
	}
	public Date getFechaVencimientoLegal() {
		return this.fechaVencimientoLegal;
	}
	public void setFechaVencimientoLegal(Date fechaVencimientoLegal) {
		this.fechaVencimientoLegal = fechaVencimientoLegal;
	}
	public Long getIdContactoDist() {
		return this.idContactoDist;
	}
	public void setIdContactoDist(Long idContactoDist) {
		this.idContactoDist = idContactoDist;
	}
	public String getMotivoCliente() {
		return this.motivoCliente;
	}
	public void setMotivoCliente(String motivoCliente) {
		this.motivoCliente = motivoCliente;
	}
	public String getNombreResponsable() {
		return this.nombreResponsable;
	}
	public void setNombreResponsable(String nombreResponsable) {
		this.nombreResponsable = nombreResponsable;
	}
	public Long getNroAtencion() {
		return this.nroAtencion;
	}
	public void setNroAtencion(Long nroAtencion) {
		this.nroAtencion = nroAtencion;
	}
	public Long getNroCuenta() {
		return this.nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public Long getNroServicio() {
		return this.nroServicio;
	}
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	public String getTipo() {
		return this.tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getTipoRespuesta() {
		return this.tipoRespuesta;
	}
	public void setTipoRespuesta(String tipoRespuesta) {
		this.tipoRespuesta = tipoRespuesta;
	}
	public String getTipoServicio() {
		return this.tipoServicio;
	}
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	public String getUsuarioResponsable() {
		return this.usuarioResponsable;
	}
	public void setUsuarioResponsable(String usuarioResponsable) {
		this.usuarioResponsable = usuarioResponsable;
	}
	public Long getNroContacto() {
		return this.nroContacto;
	}
	public void setNroContacto(Long nroContacto) {
		this.nroContacto = nroContacto;
	}
	/**
	 * @return the idAtencion
	 */
	public Long getIdAtencion() {
		return this.idAtencion;
	}
	/**
	 * @param idAtencion the idAtencion to set
	 */
	public void setIdAtencion(Long idAtencion) {
		this.idAtencion = idAtencion;
	}
}
