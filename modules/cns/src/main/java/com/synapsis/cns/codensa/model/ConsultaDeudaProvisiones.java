package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaDeudaProvisiones extends SynergiaBusinessObject {

	/**
	 * @return the cantidadProvisiones
	 */
	public Long getCantidadProvisiones();

	/**
	 * @param cantidadProvisiones the cantidadProvisiones to set
	 */
	public void setCantidadProvisiones(Long cantidadProvisiones);

	/**
	 * @return the fechaUltimaLecturaNormal
	 */
	public Date getFechaUltimaLecturaNormal();

	/**
	 * @param fechaUltimaLecturaNormal the fechaUltimaLecturaNormal to set
	 */
	public void setFechaUltimaLecturaNormal(Date fechaUltimaLecturaNormal);

	/**
	 * @return the idCuenta
	 */
	public Long getIdCuenta();

	/**
	 * @param idCuenta the idCuenta to set
	 */
	public void setIdCuenta(Long idCuenta);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

}