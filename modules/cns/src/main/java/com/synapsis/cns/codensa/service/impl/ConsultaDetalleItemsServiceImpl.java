/**
 * 
 */
package com.synapsis.cns.codensa.service.impl;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.suivant.arquitectura.core.dao.DAO;
import com.suivant.arquitectura.core.exception.IllegalDAOAccessException;
import com.suivant.arquitectura.core.integration.Module;
import com.suivant.arquitectura.core.queryFilter.CompositeQueryFilter;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.queryFilter.SimpleQueryFilter;
import com.synapsis.cns.codensa.service.ConsultaDetalleItemsService;
import com.synapsis.synergia.core.service.impl.SynergiaServiceImpl;

public class ConsultaDetalleItemsServiceImpl extends SynergiaServiceImpl
		implements ConsultaDetalleItemsService {

	private static final String SERVICIO_FINANCIERO = "SF";

	private static final String SERVICIO_CONVENIO = "SC";

	private static final String ENCARGO_COBRANZA = "EC";

	public ConsultaDetalleItemsServiceImpl() {
		super();
	}

	public ConsultaDetalleItemsServiceImpl(Module module) {
		super(module);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.service.impl.ConsultaDetalleDeuda#findByCriteria(java.lang.Long,
	 *      java.lang.String)
	 */
	public List findByCriteria(QueryFilter filter)
			throws IllegalDAOAccessException {

		Class klass = null;
		List results = null;
		String detailName = searchDetailName((CompositeQueryFilter)filter);
	
		if (StringUtils.equalsIgnoreCase(detailName,
				SERVICIO_FINANCIERO))
			klass = com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleServicioFinancieroItemsImpl.class;
		else if (StringUtils.equalsIgnoreCase(detailName,
				SERVICIO_CONVENIO))
			klass = com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleServicioConvenioItemsImpl.class;
		else if (StringUtils
				.equalsIgnoreCase(detailName, ENCARGO_COBRANZA))
			klass = com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleEncargoCobranzaItemsImpl.class;
		
		QueryFilter q = obtainFilter((CompositeQueryFilter)filter);
		((QueryFilter)q).setQueryFilter(this.getEmpresaQueryFilter());
		q.setPositionStart(filter.getPositionStart().intValue());
		q.setMaxResults(filter.getMaxResults().intValue());
		if (klass != null)
			results = getDao(klass).findByCriteria(q);

		return results;
	}

	public int findByCriteriaToPageable(QueryFilter filter)
			throws IllegalDAOAccessException {

		Class klass = null;
		int results=0;
		String detailName = searchDetailName((CompositeQueryFilter)filter);

		if (StringUtils.equalsIgnoreCase(detailName,
				SERVICIO_FINANCIERO))
			klass = com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleServicioFinancieroItemsImpl.class;
		else if (StringUtils.equalsIgnoreCase(detailName,
				SERVICIO_CONVENIO))
			klass = com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleServicioConvenioItemsImpl.class;
		else if (StringUtils
				.equalsIgnoreCase(detailName, ENCARGO_COBRANZA))
			klass = com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleEncargoCobranzaItemsImpl.class;
		QueryFilter q = obtainFilter((CompositeQueryFilter)filter);
		((QueryFilter)q).setQueryFilter(this.getEmpresaQueryFilter());
		if (klass != null)
			 results = getDao(klass).countAll(q);
		return results;
	}

	private String searchDetailName(CompositeQueryFilter cqf) {
		Iterator it = cqf.getFiltersList().iterator();
		while (it.hasNext()) {
			Object ofilter = it.next();
				SimpleQueryFilter sqf = (SimpleQueryFilter) ofilter;
					if( StringUtils.equalsIgnoreCase(sqf.getAttributeName(), "detailName"))
						return sqf.getAttributeValue().toString();
		}
		return "";
	}
	
	private QueryFilter obtainFilter(CompositeQueryFilter filter){
		Iterator it = filter.getFiltersList().iterator();
		while (it.hasNext()) {
			Object ofilter = it.next();
				SimpleQueryFilter sqf = (SimpleQueryFilter) ofilter;
					if( StringUtils.equalsIgnoreCase(sqf.getAttributeName(), "idServicio"))
						return sqf;
		}
		return null;
	}

	/**
	 * retorna el dao asociado
	 * 
	 * @return DAO
	 * @throws IllegalDAOAccessException
	 */
	public DAO getDao(Class klass) throws IllegalDAOAccessException {
		return getDAO(klass);
	}
}
