/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaEncargosCobranza;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaEncargosCobranzaImpl extends SynergiaBusinessObjectImpl implements ConsultaEncargosCobranza
{
	   private String tipoSrvFin;          
	   private Long nroSrvFin;             
	   private String nombreTit;           
	   private String tipoIdTitular;       
	   private String nroIdTitular;        
	   private Long nroEco;                
	   private String lineaNegocio;        
	   private String plan;                
	   private String producto;            
	   private Date fechaActivacion;       
	   private Date fechaDesactivacion;    
	   private String nroIdEco;            
	   private Long nroCuenta;             
	   private Double valorCargado;        
	   private Double saldoEco;
	   private Long idLineaNegocio;
	   private Long idPlan;
	   private Long idTipoServicio;
	   private Long idTipoProducto;
	   private String tipoDocTitular;
	   private String numDocTitular;
	   private String indicadorSaldoAnterior;
	   
	/**
	 * @return the fechaActivacion
	 */
	public Date getFechaActivacion() {
		return fechaActivacion;
	}
	/**
	 * @param fechaActivacion the fechaActivacion to set
	 */
	public void setFechaActivacion(Date fechaActivacion) {
		this.fechaActivacion = fechaActivacion;
	}
	/**
	 * @return the fechaDesactivacion
	 */
	public Date getFechaDesactivacion() {
		return fechaDesactivacion;
	}
	/**
	 * @param fechaDesactivacion the fechaDesactivacion to set
	 */
	public void setFechaDesactivacion(Date fechaDesactivacion) {
		this.fechaDesactivacion = fechaDesactivacion;
	}
	/**
	 * @return the lineaNegocio
	 */
	public String getLineaNegocio() {
		return lineaNegocio;
	}
	/**
	 * @param lineaNegocio the lineaNegocio to set
	 */
	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}
	/**
	 * @return the nombreTit
	 */
	public String getNombreTit() {
		return nombreTit;
	}
	/**
	 * @param nombreTit the nombreTit to set
	 */
	public void setNombreTit(String nombreTit) {
		this.nombreTit = nombreTit;
	}
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/**
	 * @return the nroEco
	 */
	public Long getNroEco() {
		return nroEco;
	}
	/**
	 * @param nroEco the nroEco to set
	 */
	public void setNroEco(Long nroEco) {
		this.nroEco = nroEco;
	}
	/**
	 * @return the nroIdEco
	 */
	public String getNroIdEco() {
		return nroIdEco;
	}
	/**
	 * @param nroIdEco the nroIdEco to set
	 */
	public void setNroIdEco(String nroIdEco) {
		this.nroIdEco = nroIdEco;
	}
	/**
	 * @return the nroIdTitular
	 */
	public String getNroIdTitular() {
		return nroIdTitular;
	}
	/**
	 * @param nroIdTitular the nroIdTitular to set
	 */
	public void setNroIdTitular(String nroIdTitular) {
		this.nroIdTitular = nroIdTitular;
	}
	/**
	 * @return the nroSrvFin
	 */
	public Long getNroSrvFin() {
		return nroSrvFin;
	}
	/**
	 * @param nroSrvFin the nroSrvFin to set
	 */
	public void setNroSrvFin(Long nroSrvFin) {
		this.nroSrvFin = nroSrvFin;
	}
	/**
	 * @return the plan
	 */
	public String getPlan() {
		return plan;
	}
	/**
	 * @param plan the plan to set
	 */
	public void setPlan(String plan) {
		this.plan = plan;
	}
	/**
	 * @return the producto
	 */
	public String getProducto() {
		return producto;
	}
	/**
	 * @param producto the producto to set
	 */
	public void setProducto(String producto) {
		this.producto = producto;
	}
	/**
	 * @return the saldoEco
	 */
	public Double getSaldoEco() {
		return saldoEco;
	}
	/**
	 * @param saldoEco the saldoEco to set
	 */
	public void setSaldoEco(Double saldoEco) {
		this.saldoEco = saldoEco;
	}
	/**
	 * @return the tipoIdTitular
	 */
	public String getTipoIdTitular() {
		return tipoIdTitular;
	}
	/**
	 * @param tipoIdTitular the tipoIdTitular to set
	 */
	public void setTipoIdTitular(String tipoIdTitular) {
		this.tipoIdTitular = tipoIdTitular;
	}
	/**
	 * @return the tipoSrvFin
	 */
	public String getTipoSrvFin() {
		return tipoSrvFin;
	}
	/**
	 * @param tipoSrvFin the tipoSrvFin to set
	 */
	public void setTipoSrvFin(String tipoSrvFin) {
		this.tipoSrvFin = tipoSrvFin;
	}
	/**
	 * @return the valorCargado
	 */
	public Double getValorCargado() {
		return valorCargado;
	}
	/**
	 * @param valorCargado the valorCargado to set
	 */
	public void setValorCargado(Double valorCargado) {
		this.valorCargado = valorCargado;
	}
	/**
	 * @return the idLineaNegocio
	 */
	public Long getIdLineaNegocio() {
		return idLineaNegocio;
	}
	/**
	 * @param idLineaNegocio the idLineaNegocio to set
	 */
	public void setIdLineaNegocio(Long idLineaNegocio) {
		this.idLineaNegocio = idLineaNegocio;
	}
	/**
	 * @return the idPlan
	 */
	public Long getIdPlan() {
		return idPlan;
	}
	/**
	 * @param idPlan the idPlan to set
	 */
	public void setIdPlan(Long idPlan) {
		this.idPlan = idPlan;
	}
	/**
	 * @return the idTipoServicio
	 */
	public Long getIdTipoServicio() {
		return idTipoServicio;
	}
	/**
	 * @param idTipoServicio the idTipoServicio to set
	 */
	public void setIdTipoServicio(Long idTipoServicio) {
		this.idTipoServicio = idTipoServicio;
	}
	public Long getIdTipoProducto() {
		return idTipoProducto;
	}
	public void setIdTipoProducto(Long idTipoProducto) {
		this.idTipoProducto = idTipoProducto;
	}
	public String getNumDocTitular() {
		return numDocTitular;
	}
	public void setNumDocTitular(String numDocTitular) {
		this.numDocTitular = numDocTitular;
	}
	public String getTipoDocTitular() {
		return tipoDocTitular;
	}
	public void setTipoDocTitular(String tipoDocTitular) {
		this.tipoDocTitular = tipoDocTitular;
	}
	public String getIndicadorSaldoAnterior() {
		return indicadorSaldoAnterior;
	}
	public void setIndicadorSaldoAnterior(String indicadorSaldoAnterior) {
		this.indicadorSaldoAnterior = indicadorSaldoAnterior;
	}
	
	
}
