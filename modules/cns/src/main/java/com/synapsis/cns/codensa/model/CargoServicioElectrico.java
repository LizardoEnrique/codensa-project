package com.synapsis.cns.codensa.model;

/**
 * Representa los Cargos por Servicio El�ctrico (Energ�a) que se muestran en el Duplicado Legal de Factura.
 * 
 * @author egrande
 */
public interface CargoServicioElectrico extends CargoFactura {

}
