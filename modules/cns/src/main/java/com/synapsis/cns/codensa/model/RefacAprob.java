package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface RefacAprob extends SynergiaBusinessObject {

	public String getTipoOperacion() ;

	public void setTipoOperacion(String tipoOperacion) ;
	
	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public String getNroAjuste();

	public void setNroAjuste(String nroAjuste);

	public String getNroOrdenRefa();

	public void setNroOrdenRefa(String nroOrdenRefa);

	public Long getServAjustados();

	public void setServAjustados(Long servAjustados);

	public Double getValorTotalAjustado();

	public void setValorTotalAjustado(Double valorTotalAjustado);

	public Double getCantKwhAjust();

	public void setCantKwhAjust(Double cantKwhAjust);

	public Double getValorTotalPagar();

	public void setValorTotalPagar(Double valorTotalPagar);

	public Date getFecVencPagoAjuste();

	public void setFecVencPagoAjuste(Date fecVencPagoAjuste);

	public Date getFecHorRealizAjuste();

	public void setFecHorRealizAjuste(Date fecHorRealizAjuste);

	public String getUsuarioRealizAjuste();

	public void setUsuarioRealizAjuste(String usuarioRealizAjuste);

	public String getNombreUsuarioRealizAjuste();

	public void setNombreUsuarioRealizAjuste(String nombreUsuarioRealizAjuste);

	public Date getFecHorAprobAjuste();

	public void setFecHorAprobAjuste(Date fecHorAprobAjuste);

	public String getUsuariosAprobAjustes();

	public void setUsuariosAprobAjustes(String usuariosAprobAjustes);

	public String getNombresUsuariosAprobAjustes();

	public void setNombresUsuariosAprobAjustes(String nombresUsuariosAprobAjustes);

}