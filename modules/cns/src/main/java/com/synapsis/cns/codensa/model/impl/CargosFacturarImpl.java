/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.CargosFacturar;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 *
 */
public class CargosFacturarImpl extends SynergiaBusinessObjectImpl implements CargosFacturar {
	
	private static final long serialVersionUID = 1L;

	
   	private String codigoCargo;
   	private String descripcionCargo;
   	private Long valorCargo;
   	private String tipoServicioCargo;
   	private String nroServicioCargo;
   	private Date fechaIngresoCargo;
   	private String usuarioIngresoCargo;
   	private String nombreIngresoCargo;  
   	private String observaciones;
	private Long nroCuenta;
	   	
   	
	
	public Long getNroCuenta() {
		return nroCuenta;
	}
	
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CargosFacturaCargosManuales#getCodigoCargo()
	 */
	public String getCodigoCargo() {
		return codigoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CargosFacturaCargosManuales#setCodigoCargo(java.lang.String)
	 */
	public void setCodigoCargo(String codigoCargo) {
		this.codigoCargo = codigoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CargosFacturaCargosManuales#getDescripcionCargo()
	 */
	public String getDescripcionCargo() {
		return descripcionCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CargosFacturaCargosManuales#setDescripcionCargo(java.lang.String)
	 */
	public void setDescripcionCargo(String descripcionCargo) {
		this.descripcionCargo = descripcionCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CargosFacturaCargosManuales#getFechaIngresoCargo()
	 */
	public Date getFechaIngresoCargo() {
		return fechaIngresoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CargosFacturaCargosManuales#setFechaIngresoCargo(java.util.Date)
	 */
	public void setFechaIngresoCargo(Date fechaIngresoCargo) {
		this.fechaIngresoCargo = fechaIngresoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CargosFacturaCargosManuales#getNombreIngresoCargo()
	 */
	public String getNombreIngresoCargo() {
		return nombreIngresoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CargosFacturaCargosManuales#setNombreIngresoCargo(java.lang.String)
	 */
	public void setNombreIngresoCargo(String nombreIngresoCargo) {
		this.nombreIngresoCargo = nombreIngresoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CargosFacturaCargosManuales#getNroServicioCargo()
	 */
	public String getNroServicioCargo() {
		return nroServicioCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CargosFacturaCargosManuales#setNroServicioCargo(java.lang.String)
	 */
	public void setNroServicioCargo(String nroServicioCargo) {
		this.nroServicioCargo = nroServicioCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CargosFacturaCargosManuales#getObservaciones()
	 */
	public String getObservaciones() {
		return observaciones;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CargosFacturaCargosManuales#setObservaciones(java.lang.String)
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CargosFacturaCargosManuales#getTipoServicioCargo()
	 */
	public String getTipoServicioCargo() {
		return tipoServicioCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CargosFacturaCargosManuales#setTipoServicioCargo(java.lang.String)
	 */
	public void setTipoServicioCargo(String tipoServicioCargo) {
		this.tipoServicioCargo = tipoServicioCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CargosFacturaCargosManuales#getUsuarioIngresoCargo()
	 */
	public String getUsuarioIngresoCargo() {
		return usuarioIngresoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CargosFacturaCargosManuales#setUsuarioIngresoCargo(java.lang.String)
	 */
	public void setUsuarioIngresoCargo(String usuarioIngresoCargo) {
		this.usuarioIngresoCargo = usuarioIngresoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CargosFacturaCargosManuales#getValorCargo()
	 */
	public Long getValorCargo() {
		return valorCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CargosFacturaCargosManuales#setValorCargo(java.lang.Long)
	 */
	public void setValorCargo(Long valorCargo) {
		this.valorCargo = valorCargo;
	}
	
}
