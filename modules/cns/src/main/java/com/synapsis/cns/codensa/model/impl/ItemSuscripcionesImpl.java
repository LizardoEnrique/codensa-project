package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ItemSuscripciones;

/**
 * Item de Producto Suscripciones para mostrar en el Reporte de Impresion de Anexo de Factura (CNS003)
 * 
 * @author jhack
 */
public class ItemSuscripcionesImpl extends ItemECOImpl implements ItemSuscripciones {
	private static final long serialVersionUID = 1L;

}