package com.synapsis.cns.codensa.model.buscaCuenta;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface BuscaCuentaTipoNroServicio extends SynergiaBusinessObject{

	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio();

	/**
	 * @return the tipoServicio
	 */
	public String getTipoServicio();

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio);

	/**
	 * @param tipoServicio the tipoServicio to set
	 */
	public void setTipoServicio(String tipoServicio);
	
	
	public Long getIdClienteTelem();
	
	public void setIdClienteTelem(Long idClienteTelem);	

}