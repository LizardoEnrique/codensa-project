package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


public interface ConsultaFacturacionDetalleServicioConvenioItems extends SynergiaBusinessObject {

	/**
	 * @return the id
	 */
	public Long getId();

	/**
	 * @param id the id to set
	 */
	public void setId(Long id);
	/**
	 * @return the periodoFact
	 */
	public Date getPeriodoFact();
	/**
	 * @param periodoFact the periodoFact to set
	 */
	public void setPeriodoFact(Date periodoFact);
	/**
	 * @return the codItem
	 */
	public String getCodItem() ;
	/**
	 * @param codItem the codItem to set
	 */
	public void setCodItem(String codItem);
	/**
	 * @return the descItem
	 */
	public String getDescItem();
	/**
	 * @param descItem the descItem to set
	 */
	public void setDescItem(String descItem);
	/**
	 * @return the valFacItem
	 */
	public Long getValFacItem();
	/**
	 * @param valFacItem the valFacItem to set
	 */
	public void setValFacItem(Long valFacItem);
	
	public Long getIdServicio();
	
	public void setIdServicio(Long idServicio);
	
}