package com.synapsis.cns.codensa.model.impl;
import java.util.Date;

import com.synapsis.cns.codensa.model.ReporteDatosGenerales;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author m3.MarioRoss - 18/12/2006
 *
 */
public abstract class ReporteDatosGeneralesImpl extends SynergiaBusinessObjectImpl implements ReporteDatosGenerales {

	private FechaParser fechaExpedicionParser;
	
	private Long nroCuenta;
	private String nombreSolicitante;
	private String tipoIdentificacion;
	private String numeroIdentificacion;
	private String nombreTitularCuenta;
	private String apellidoTitularCuenta;
	private String direccion;
	private String localizacion;
	private String municipio;
	private String barrio;
	private String telefono;
	private String direccionRepartoEspecial;
	private String usuarioSistema;

	public Integer getDiaExpedicion() {
		return fechaExpedicionParser.getDia();
	}

	public String getMesExpedicion() {
		return fechaExpedicionParser.getMes();
	}

	public Integer getAnioExpedicion() {
		return fechaExpedicionParser.getAnio();
	}

	
	public String getBarrio() {
		return barrio;
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getDireccionRepartoEspecial() {
		return direccionRepartoEspecial;
	}

	public void setDireccionRepartoEspecial(String direccionRepartoEspecial) {
		this.direccionRepartoEspecial = direccionRepartoEspecial;
	}

	public Date getFechaExpedicion() {
		return fechaExpedicionParser.getFecha(); 
	}

	public void setFechaExpedicion(Date fechaExpedicion) {
		fechaExpedicionParser = new FechaParser(fechaExpedicion);
	}

	public String getLocalizacion() {
		return localizacion;
	}

	public void setLocalizacion(String localizacion) {
		this.localizacion = localizacion;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getNombreSolicitante() {
		return nombreSolicitante;
	}

	public void setNombreSolicitante(String nombreSolicitante) {
		this.nombreSolicitante = nombreSolicitante;
	}

	public String getNombreTitularCuenta() {
		return nombreTitularCuenta;
	}

	public void setNombreTitularCuenta(String nombreTitularCuenta) {
		this.nombreTitularCuenta = nombreTitularCuenta;
	}

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getNumeroIdentificacion() {
		return numeroIdentificacion;
	}

	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	public String getUsuarioSistema() {
		return usuarioSistema;
	}

	public void setUsuarioSistema(String usuarioSistema) {
		this.usuarioSistema = usuarioSistema;
	}

	public String getApellidoTitularCuenta() {
		return apellidoTitularCuenta;
	}

	public void setApellidoTitularCuenta(String apellidoTitularCuenta) {
		this.apellidoTitularCuenta = apellidoTitularCuenta;
	}

	public FechaParser getFechaExpedicionParser() {
		return fechaExpedicionParser;
	}

	public void setFechaExpedicionParser(FechaParser fechaExpedicionParser) {
		this.fechaExpedicionParser = fechaExpedicionParser;
	}
}