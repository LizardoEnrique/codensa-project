package com.synapsis.cns.codensa.model.combo;

public interface ComboTiposServicioCCA {

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the tipo
	 */
	public String getTipo();

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo);

}