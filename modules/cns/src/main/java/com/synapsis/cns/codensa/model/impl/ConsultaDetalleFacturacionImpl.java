/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaDetalleFacturacion;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaDetalleFacturacionImpl extends SynergiaBusinessObjectImpl implements ConsultaDetalleFacturacion {
	   private Long nroCuenta;             
	   private Date periodoFacturacion;    
	   private Date fechaFacturacion;      
	   private Date fechaLectura;          
	   private String valorTotalFactura;   
	   private Long nroFacturaEmitida;     
	   private Date fechaVencimiento;
	/**
	 * @return the fechaFacturacion
	 */
	public Date getFechaFacturacion() {
		return fechaFacturacion;
	}
	/**
	 * @param fechaFacturacion the fechaFacturacion to set
	 */
	public void setFechaFacturacion(Date fechaFacturacion) {
		this.fechaFacturacion = fechaFacturacion;
	}
	/**
	 * @return the fechaLectura
	 */
	public Date getFechaLectura() {
		return fechaLectura;
	}
	/**
	 * @param fechaLectura the fechaLectura to set
	 */
	public void setFechaLectura(Date fechaLectura) {
		this.fechaLectura = fechaLectura;
	}
	/**
	 * @return the fechaVencimiento
	 */
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}
	/**
	 * @param fechaVencimiento the fechaVencimiento to set
	 */
	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/**
	 * @return the nroFacturaEmitida
	 */
	public Long getNroFacturaEmitida() {
		return nroFacturaEmitida;
	}
	/**
	 * @param nroFacturaEmitida the nroFacturaEmitida to set
	 */
	public void setNroFacturaEmitida(Long nroFacturaEmitida) {
		this.nroFacturaEmitida = nroFacturaEmitida;
	}
	/**
	 * @return the periodoFacturacion
	 */
	public Date getPeriodoFacturacion() {
		return periodoFacturacion;
	}
	/**
	 * @param periodoFacturacion the periodoFacturacion to set
	 */
	public void setPeriodoFacturacion(Date periodoFacturacion) {
		this.periodoFacturacion = periodoFacturacion;
	}
	/**
	 * @return the valorTotalFactura
	 */
	public String getValorTotalFactura() {
		return valorTotalFactura;
	}
	/**
	 * @param valorTotalFactura the valorTotalFactura to set
	 */
	public void setValorTotalFactura(String valorTotalFactura) {
		this.valorTotalFactura = valorTotalFactura;
	}      
}
