package com.synapsis.cns.codensa.model.impl;

import java.math.BigDecimal;
import java.util.Date;

import com.synapsis.cns.codensa.model.DetalleInventarioInicialInfraestructura;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ccamba
 * 
 */
public class DetalleInventarioInicialInfraestructuraImpl extends SynergiaBusinessObjectImpl implements
		DetalleInventarioInicialInfraestructura {

	private Long nroCuenta;
	private Long nroServicio;
	private Long idClienteTelem;
	private Long periodo;
	private Long nroProyecto;
	private String zona;
	private String producto;
	private String subProducto;
	private Date fechaViabilidad;
	private BigDecimal cantidad;
	private Date fechaFactura;
	private Date fechaPeriodoGracia;
	private Integer diasCobro;
	private BigDecimal costoPeriodo;
	private BigDecimal tarifaAplicada;
	private String estadoFacturacion;

	public Long getNroCuenta() {
		return this.nroCuenta;
	}

	public Long getNroServicio() {
		return this.nroServicio;
	}

	public Long getIdClienteTelem() {
		return this.idClienteTelem;
	}

	public Long getPeriodo() {
		return this.periodo;
	}

	public Long getNroProyecto() {
		return this.nroProyecto;
	}

	public String getZona() {
		return this.zona;
	}

	public String getProducto() {
		return this.producto;
	}

	public String getSubProducto() {
		return this.subProducto;
	}

	public Date getFechaViabilidad() {
		return this.fechaViabilidad;
	}

	public BigDecimal getCantidad() {
		return this.cantidad;
	}

	public Date getFechaFactura() {
		return this.fechaFactura;
	}

	public Date getFechaPeriodoGracia() {
		return this.fechaPeriodoGracia;
	}

	public Integer getDiasCobro() {
		return this.diasCobro;
	}

	public BigDecimal getCostoPeriodo() {
		return this.costoPeriodo;
	}

	public BigDecimal getTarifaAplicada() {
		return this.tarifaAplicada;
	}

	public String getEstadoFacturacion() {
		return this.estadoFacturacion;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}

	public void setIdClienteTelem(Long idClienteTelem) {
		this.idClienteTelem = idClienteTelem;
	}

	public void setPeriodo(Long periodo) {
		this.periodo = periodo;
	}

	public void setNroProyecto(Long nroProyecto) {
		this.nroProyecto = nroProyecto;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public void setSubProducto(String subProducto) {
		this.subProducto = subProducto;
	}

	public void setFechaViabilidad(Date fechaViabilidad) {
		this.fechaViabilidad = fechaViabilidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}

	public void setFechaPeriodoGracia(Date fechaPeriodoGracia) {
		this.fechaPeriodoGracia = fechaPeriodoGracia;
	}

	public void setDiasCobro(Integer diasCobro) {
		this.diasCobro = diasCobro;
	}

	public void setCostoPeriodo(BigDecimal costoPeriodo) {
		this.costoPeriodo = costoPeriodo;
	}

	public void setTarifaAplicada(BigDecimal tarifaAplicada) {
		this.tarifaAplicada = tarifaAplicada;
	}

	public void setEstadoFacturacion(String estadoFacturacion) {
		this.estadoFacturacion = estadoFacturacion;
	}

}
