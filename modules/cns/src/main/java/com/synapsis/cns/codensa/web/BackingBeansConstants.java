/**
 * 
 */
package com.synapsis.cns.codensa.web;

/**
 * @author ar30557486
 * Constantes para los bb
 */
public interface BackingBeansConstants {
	
	//NOMBRES DE SERVICIOS UTILIZADOS EN LOS BB
	public static final String SRV_COMBO_DEPARTAMENTOS 	= "comboDepartamentosServiceFinder";
	public static final String SRV_COMBO_MUNICIPIOS 	= "comboMunicipiosServiceFinder";
	public static final String SRV_COMBO_SUCURSALES 	= "comboSucursalesServiceFinder";
	public static final String SRV_COMBO_ZONAS 			= "comboZonasServiceFinder";
	public static final String SRV_COMBO_CICLOS 		= "comboCiclosServiceFinder";
	public static final String SRV_COMBO_MANZANAS 		= "comboManzanasServiceFinder";
	public static final String SRV_COMBO_GRUPOS 		= "comboGruposServiceFinder";
	public static final String SRV_COMBO_ORDENES 	= 	  "comboTipoOrdenServiceFinder";
	public static final String SRV_COMBO_ESTADOS_ORDENES 	= "comboEstadoOrdenServiceFinder";
	
	//NOMBRES DE ALGUNOS FILTROS UTILIZADOS EN LOS BB
	public static final String QUERY_FILTER_CONSULTA_ORDENES = "consultaOrdenesQueryFilter";
	public static final String QUERY_FILTER_COMBO = "comboQueryFilter";
	public static final String QUERY_FILTER_ORDENES_ESTADO = "consultaOrdenesQueryFilterToEstadoChild";
	
	//nombres de algunas outcome que emiten los backing bean
	public static final String CERTIFICA_CONVENIO_EMITIR_PDF="emitirPdf";
	
	//NOMBRES DE ALGUNAS PROPERTIES UTILIZADAS PARA GENERAR LOS COMBOS
	public static final String COMBO_DEFAULT_LABEL = "descripcion";
	public static final String COMBO_DEFAULT_VALUE = "codigo";
	public static final String COMBO_DEFAULT_ID = "id";
	
}
