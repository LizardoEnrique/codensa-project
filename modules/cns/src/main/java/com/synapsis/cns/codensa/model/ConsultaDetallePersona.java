package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface ConsultaDetallePersona {

	/**
	 * @return the descTipoPersona
	 */
	public String getDescTipoPersona();

	/**
	 * @param descTipoPersona the descTipoPersona to set
	 */
	public void setDescTipoPersona(String descTipoPersona);

	/**
	 * @return the direComercial
	 */
	public String getDireComercial();

	/**
	 * @param direComercial the direComercial to set
	 */
	public void setDireComercial(String direComercial);

	/**
	 * @return the fechaNacimiento
	 */
	public Date getFechaNacimiento();

	/**
	 * @param fechaNacimiento the fechaNacimiento to set
	 */
	public void setFechaNacimiento(Date fechaNacimiento);

	/**
	 * @return the giro
	 */
	public String getGiro();

	/**
	 * @param giro the giro to set
	 */
	public void setGiro(String giro);

	/**
	 * @return the nombre
	 */
	public String getNombre();

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the sexo
	 */
	public String getSexo();

	/**
	 * @param sexo the sexo to set
	 */
	public void setSexo(String sexo);

}