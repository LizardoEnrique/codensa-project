/**
 * 
 */
package com.synapsis.cns.codensa.web;

import com.suivant.arquitectura.core.exception.ObjectNotFoundException;

import com.suivant.arquitectura.core.model.BusinessObject;
import com.suivant.arquitectura.core.service.FinderService;
import com.suivant.arquitectura.presentation.model.ListableBB;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;

/**
 * @author dbraccio
 *
 */
public class CaracteristicasSuministroBB extends ListableBB {

	/**objeto que hace referencia  a datos de carga */
	BusinessObject businessObject3;
	/**objeto que hace referencia a datos del transformador */
	BusinessObject businessObject2;
	/**objeto que hace referencia a datos generales */
	BusinessObject businessObject1;
	String serviceNameBODatosCarga;
	String serviceNameBODatosTransformador;
	String serviceNameBODatosGenerales;
	
	public CaracteristicasSuministroBB() {
	}
	

	public void buscarSuministroAction() {
			searchBusiness();
	}
	
	private void searchBusiness() {
		try {
			setBusinessObject3(getService(getServiceNameBODatosCarga()
					).findByCriteriaUnique(getQueryFilter()));
		}catch (ObjectNotFoundException e) {
		//ErrorsManager.addInfoMessage(e.getMessage());
		}
		try {
			setBusinessObject1(getService(getServiceNameBODatosGenerales()
					).findByCriteriaUnique(getQueryFilter()));
		}catch (ObjectNotFoundException e) {
			//ErrorsManager.addInfoMessage(e.getMessage());
			}
		try {
			setBusinessObject2(getService(getServiceNameBODatosTransformador()
					).findByCriteriaUnique(getQueryFilter()));
		}catch (ObjectNotFoundException e) {
			//ErrorsManager.addInfoMessage(e.getMessage());
			}
			
	}
	
	private FinderService getService(String serviceFinder){
		return (FinderService)SynergiaApplicationContext.findService(
				serviceFinder);
	}

	public BusinessObject getBusinessObject2() {
		return businessObject2;
	}

	public void setBusinessObject2(BusinessObject businessObject2) {
		this.businessObject2 = businessObject2;
	}

	public BusinessObject getBusinessObject1() {
		return businessObject1;
	}

	public void setBusinessObject1(BusinessObject businessObject1) {
		this.businessObject1 = businessObject1;
	}

	public BusinessObject getBusinessObject3() {
		return businessObject3;
	}

	public void setBusinessObject3(BusinessObject businessObject3) {
		this.businessObject3 = businessObject3;
	}

	public String getServiceNameBODatosCarga() {
		return serviceNameBODatosCarga;
	}

	public void setServiceNameBODatosCarga(String serviceNameBODatosCarga) {
		this.serviceNameBODatosCarga = serviceNameBODatosCarga;
	}

	public String getServiceNameBODatosGenerales() {
		return serviceNameBODatosGenerales;
	}

	public void setServiceNameBODatosGenerales(String serviceNameBODatosGenerales) {
		this.serviceNameBODatosGenerales = serviceNameBODatosGenerales;
	}

	public String getServiceNameBODatosTransformador() {
		return serviceNameBODatosTransformador;
	}

	public void setServiceNameBODatosTransformador(
			String serviceNameBODatosTransformador) {
		this.serviceNameBODatosTransformador = serviceNameBODatosTransformador;
	}

}
