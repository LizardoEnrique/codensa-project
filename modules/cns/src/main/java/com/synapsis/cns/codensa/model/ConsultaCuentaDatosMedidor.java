package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaCuentaDatosMedidor extends SynergiaBusinessObject {
	
	/**
	 * @return the idConfigFact
	 */
	public Long getIdConfigFact();
	/**
	 * @param idConfigFact the idConfigFact to set
	 */
	public void setIdConfigFact(Long idConfigFact);
	
	/**
	 * @return the fechaProceso
	 */
	public Date getFechaProceso() ;
	/**
	 * @param fechaProceso the fechaProceso to set
	 */
	public void setFechaProceso(Date fechaProceso);
	
	
	/**
	 * @return the cargaContratada
	 */
	public Long getCargaContratada();

	/**
	 * @param cargaContratada the cargaContratada to set
	 */
	public void setCargaContratada(Long cargaContratada);

	/**
	 * @return the cargaInstalada
	 */
	public Long getCargaInstalada();

	/**
	 * @param cargaInstalada the cargaInstalada to set
	 */
	public void setCargaInstalada(Long cargaInstalada);

	/**
	 * @return the factorLiquidacion
	 */
	public Long getFactorLiquidacion();

	/**
	 * @param factorLiquidacion the factorLiquidacion to set
	 */
	public void setFactorLiquidacion(Long factorLiquidacion);
	
	/**
	 * @return the marcaMedidor
	 */
	public String getMarcaMedidor();

	/**
	 * @param marcaMedidor the marcaMedidor to set
	 */
	public void setMarcaMedidor(String marcaMedidor);

	/**
	 * @return the nroMedidor
	 */
	public String getNroMedidor();

	/**
	 * @param nroMedidor the nroMedidor to set
	 */
	public void setNroMedidor(String nroMedidor);

	/**
	 * @return the tipoMedidor
	 */
	public String getTipoMedidor();

	/**
	 * @param tipoMedidor the tipoMedidor to set
	 */
	public void setTipoMedidor(String tipoMedidor);
	
	public String getFactorPulso();
	
	public void setFactorPulso(String factorPulso);
	
	public Long getNroCuenta();
	
	public void setNroCuenta(Long nroCuenta);
	
	public void setIdDetalleCtaMed(Long idDetalleCtaMed);
	
	public Long getIdDetalleCtaMed();


}