/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaLecturaConsumoAuditoria;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author dbraccio
 *
 *CU  006
 */
public class ConsultaLecturaConsumoAuditoriaImpl extends SynergiaBusinessObjectImpl implements ConsultaLecturaConsumoAuditoria{
	
	private Long nroCuenta;
	private Long idCuenta;
	private Long idServicio;
	private Long nroServicio;
	private Date fechaHoraAnalisisLectura;
	private String usuarioRealizaLectura;
	private String tipoLecturaInicial;
	private String anomaliaLecturaInicial;
	private Double lecturaActivaOriginalFP;
	private Double lecturaActivaOriginalHP;
	private Double lecturaActivaOriginalXP;
	private Double lecturaReactivaOriginalFP;
	private Double lecturaReactivaOriginalHP;
	private Double lecturaReactivaOriginalXP;
	private String tipoLecturaFinal;
	private String anomaliaLecturaFinal;
	private Double lecturaActivaModificadaFP;
	private Double lecturaActivaModificadaHP;
	private Double lecturaActivaModificadaXP;
	private Double lecturaReactivaModificadaFP;
	private Double lecturaReactivaModificadaHP;
	private Double lecturaReactivaModificadaXP;
	private Date fechaLecturaAnterior;
	private Date fechaLecturaActual;
	private String numeroMedidor;
	private String marcaMedidor;
	private String direccionIP;
	private String observaciones;
	
	public String getAnomaliaLecturaFinal() {
		return anomaliaLecturaFinal;
	}
	public void setAnomaliaLecturaFinal(String anomaliaLecturaFinal) {
		this.anomaliaLecturaFinal = anomaliaLecturaFinal;
	}
	public String getAnomaliaLecturaInicial() {
		return anomaliaLecturaInicial;
	}
	public void setAnomaliaLecturaInicial(String anomaliaLecturaInicial) {
		this.anomaliaLecturaInicial = anomaliaLecturaInicial;
	}
	public String getDireccionIP() {
		return direccionIP;
	}
	public void setDireccionIP(String direccionIP) {
		this.direccionIP = direccionIP;
	}
	public Date getFechaLecturaActual() {
		return fechaLecturaActual;
	}
	public void setFechaLecturaActual(Date fechaLecturaActual) {
		this.fechaLecturaActual = fechaLecturaActual;
	}
	public Date getFechaLecturaAnterior() {
		return fechaLecturaAnterior;
	}
	public void setFechaLecturaAnterior(Date fechaLecturaAnterior) {
		this.fechaLecturaAnterior = fechaLecturaAnterior;
	}
	public String getMarcaMedidor() {
		return marcaMedidor;
	}
	public void setMarcaMedidor(String marcaMedidor) {
		this.marcaMedidor = marcaMedidor;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public String getTipoLecturaFinal() {
		return tipoLecturaFinal;
	}
	public void setTipoLecturaFinal(String tipoLecturaFinal) {
		this.tipoLecturaFinal = tipoLecturaFinal;
	}
	public String getTipoLecturaInicial() {
		return tipoLecturaInicial;
	}
	public void setTipoLecturaInicial(String tipoLecturaInicial) {
		this.tipoLecturaInicial = tipoLecturaInicial;
	}
	public String getUsuarioRealizaLectura() {
		return usuarioRealizaLectura;
	}
	public void setUsuarioRealizaLectura(String usuarioRealizaLectura) {
		this.usuarioRealizaLectura = usuarioRealizaLectura;
	}
	public Date getFechaHoraAnalisisLectura() {
		return fechaHoraAnalisisLectura;
	}
	public void setFechaHoraAnalisisLectura(Date fechaHoraAnalisisLectura) {
		this.fechaHoraAnalisisLectura = fechaHoraAnalisisLectura;
	}
	public Double getLecturaActivaModificadaFP() {
		return lecturaActivaModificadaFP;
	}
	public void setLecturaActivaModificadaFP(Double lecturaActivaModificadaFP) {
		this.lecturaActivaModificadaFP = lecturaActivaModificadaFP;
	}
	public Double getLecturaActivaModificadaHP() {
		return lecturaActivaModificadaHP;
	}
	public void setLecturaActivaModificadaHP(Double lecturaActivaModificadaHP) {
		this.lecturaActivaModificadaHP = lecturaActivaModificadaHP;
	}
	public Double getLecturaActivaModificadaXP() {
		return lecturaActivaModificadaXP;
	}
	public void setLecturaActivaModificadaXP(Double lecturaActivaModificadaXP) {
		this.lecturaActivaModificadaXP = lecturaActivaModificadaXP;
	}
	public Double getLecturaActivaOriginalFP() {
		return lecturaActivaOriginalFP;
	}
	public void setLecturaActivaOriginalFP(Double lecturaActivaOriginalFP) {
		this.lecturaActivaOriginalFP = lecturaActivaOriginalFP;
	}
	public Double getLecturaActivaOriginalHP() {
		return lecturaActivaOriginalHP;
	}
	public void setLecturaActivaOriginalHP(Double lecturaActivaOriginalHP) {
		this.lecturaActivaOriginalHP = lecturaActivaOriginalHP;
	}
	public Double getLecturaActivaOriginalXP() {
		return lecturaActivaOriginalXP;
	}
	public void setLecturaActivaOriginalXP(Double lecturaActivaOriginalXP) {
		this.lecturaActivaOriginalXP = lecturaActivaOriginalXP;
	}
	public Double getLecturaReactivaModificadaFP() {
		return lecturaReactivaModificadaFP;
	}
	public void setLecturaReactivaModificadaFP(Double lecturaReactivaModificadaFP) {
		this.lecturaReactivaModificadaFP = lecturaReactivaModificadaFP;
	}
	public Double getLecturaReactivaModificadaHP() {
		return lecturaReactivaModificadaHP;
	}
	public void setLecturaReactivaModificadaHP(Double lecturaReactivaModificadaHP) {
		this.lecturaReactivaModificadaHP = lecturaReactivaModificadaHP;
	}
	public Double getLecturaReactivaModificadaXP() {
		return lecturaReactivaModificadaXP;
	}
	public void setLecturaReactivaModificadaXP(Double lecturaReactivaModificadaXP) {
		this.lecturaReactivaModificadaXP = lecturaReactivaModificadaXP;
	}
	public Double getLecturaReactivaOriginalFP() {
		return lecturaReactivaOriginalFP;
	}
	public void setLecturaReactivaOriginalFP(Double lecturaReactivaOriginalFP) {
		this.lecturaReactivaOriginalFP = lecturaReactivaOriginalFP;
	}
	public Double getLecturaReactivaOriginalHP() {
		return lecturaReactivaOriginalHP;
	}
	public void setLecturaReactivaOriginalHP(Double lecturaReactivaOriginalHP) {
		this.lecturaReactivaOriginalHP = lecturaReactivaOriginalHP;
	}
	public Double getLecturaReactivaOriginalXP() {
		return lecturaReactivaOriginalXP;
	}
	public void setLecturaReactivaOriginalXP(Double lecturaReactivaOriginalXP) {
		this.lecturaReactivaOriginalXP = lecturaReactivaOriginalXP;
	}
	public Long getIdCuenta() {
		return idCuenta;
	}
	public Long getIdServicio() {
		return idServicio;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public Long getNroServicio() {
		return nroServicio;
	}
	public String getNumeroMedidor() {
		return numeroMedidor;
	}
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	public void setNumeroMedidor(String numeroMedidor) {
		this.numeroMedidor = numeroMedidor;
	}
	
}
