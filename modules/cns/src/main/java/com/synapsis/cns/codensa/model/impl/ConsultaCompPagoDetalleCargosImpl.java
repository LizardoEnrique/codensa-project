package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaCompPagoDetalleCargos;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaCompPagoDetalleCargosImpl extends SynergiaBusinessObjectImpl implements ConsultaCompPagoDetalleCargos {
	
	 private static final long serialVersionUID = 1L;
	
	private Long nroCuenta;
	private String codCargo;
	private String descripcionCargo;
	private double valorCargo;
	private String tipoCargo;
	private String periodoCargo;
	private long   idDocumento;
	private Long nroServicio;
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCompPagoDetalleCargos#getCodCargo()
	 */
	public String getCodCargo() {
		return codCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCompPagoDetalleCargos#setCodCargo(java.lang.String)
	 */
	public void setCodCargo(String codCargo) {
		this.codCargo = codCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCompPagoDetalleCargos#getDescripci�nCargo()
	 */
	public String getDescripcionCargo() {
		return descripcionCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCompPagoDetalleCargos#setDescripci�nCargo(java.lang.String)
	 */
	public void setDescripcionCargo(String descripcionCargo) {
		this.descripcionCargo = descripcionCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCompPagoDetalleCargos#getPeriodoCargo()
	 */
	public String getPeriodoCargo() {
		return periodoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCompPagoDetalleCargos#setPeriodoCargo(java.lang.String)
	 */
	public void setPeriodoCargo(String periodoCargo) {
		this.periodoCargo = periodoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCompPagoDetalleCargos#getTipoCargo()
	 */
	public String getTipoCargo() {
		return tipoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCompPagoDetalleCargos#setTipoCargo(java.lang.String)
	 */
	public void setTipoCargo(String tipoCargo) {
		this.tipoCargo = tipoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCompPagoDetalleCargos#getValorCargo()
	 */
	public double getValorCargo() {
		return valorCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCompPagoDetalleCargos#setValorCargo(double)
	 */
	public void setValorCargo(double valorCargo) {
		this.valorCargo = valorCargo;
	}
	/**
	 * @return the idDocumento
	 */
	public long getIdDocumento() {
		return idDocumento;
	}
	/**
	 * @param idDocumento the idDocumento to set
	 */
	public void setIdDocumento(long idDocumento) {
		this.idDocumento = idDocumento;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}

}