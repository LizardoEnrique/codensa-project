/**
 * 
 */
package com.synapsis.cns.codensa.web;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.custom.service.UIService;
/**
 * @author ar18799631
 *
 */
public class ConsultaConvenioEstadosHogarBB {

		//filtros 
		// fin filtros
		
		private UIService svcHistoriaEstados;
		private HtmlDataTable dataTableHistoria;
		private Long nroServicio;
		private Long nroServicioEstados;
		public void search() {
			this.getSvcHistoriaEstados().execute();
			this.getDataTableHistoria().setFirst(0);
		}
		/**
		 * @return the dataTableHistoria
		 */
		public HtmlDataTable getDataTableHistoria() {
			return dataTableHistoria;
		}
		/**
		 * @param dataTableHistoria the dataTableHistoria to set
		 */
		public void setDataTableHistoria(HtmlDataTable dataTableHistoria) {
			this.dataTableHistoria = dataTableHistoria;
		}
		/**
		 * @return the svcHistoriaEstados
		 */
		public UIService getSvcHistoriaEstados() {
			return svcHistoriaEstados;
		}
		/**
		 * @param svcHistoriaEstados the svcHistoriaEstados to set
		 */
		public void setSvcHistoriaEstados(UIService svcHistoriaEstados) {
			this.svcHistoriaEstados = svcHistoriaEstados;
		}
		/**
		 * @return the nroServicio
		 */
		public Long getNroServicio() {
			return nroServicio;
		}
		/**
		 * @param nroServicio the nroServicio to set
		 */
		public void setNroServicio(Long nroServicio) {
			this.nroServicio = nroServicio;
		}
		/**
		 * @return the nroServicioEstados
		 */
		public Long getNroServicioEstados() {
			return nroServicioEstados;
		}
		/**
		 * @param nroServicioEstados the nroServicioEstados to set
		 */
		public void setNroServicioEstados(Long nroServicioEstados) {
			this.nroServicioEstados = nroServicioEstados;
		}


}
