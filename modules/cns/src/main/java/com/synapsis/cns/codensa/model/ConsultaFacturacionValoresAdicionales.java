package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


public interface ConsultaFacturacionValoresAdicionales extends SynergiaBusinessObject{

	public abstract String getCodigoCargo();

	public abstract void setCodigoCargo(String codigoCargo);

	public abstract String getDescripcionCargo();

	public abstract void setDescripcionCargo(String descripcionCargo);

	public abstract Date getFechaFacturacion();

	public abstract void setFechaFacturacion(Date fechaFacturacion);

	public abstract Date getFechaLectura();

	public abstract void setFechaLectura(Date fechaLectura);

	public abstract String getPeriodo();

	public abstract void setPeriodo(String periodo);

	public abstract String getValor();

	public abstract void setValor(String valor);
	
	public void setNroCuenta(Long nroCuenta);
	
	public Long getNroCuenta();

}