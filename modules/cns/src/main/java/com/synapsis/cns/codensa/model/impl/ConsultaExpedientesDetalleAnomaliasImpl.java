/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaExpedientesDetalleAnomalias;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaExpedientesDetalleAnomaliasImpl extends
		SynergiaBusinessObjectImpl implements ConsultaExpedientesDetalleAnomalias {
	private String codigoAnomalia;
	private String descripcionAnomalia;
	private Long numeroOrdenInspeccion;
	private Long cantidad;
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedientesDetalleAnomalias#getCodigoAnomalia()
	 */
	public String getCodigoAnomalia() {
		return codigoAnomalia;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedientesDetalleAnomalias#setCodigoAnomalia(java.lang.String)
	 */
	public void setCodigoAnomalia(String codigoAnomalia) {
		this.codigoAnomalia = codigoAnomalia;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedientesDetalleAnomalias#getDescripcionAnomalia()
	 */
	public String getDescripcionAnomalia() {
		return descripcionAnomalia;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedientesDetalleAnomalias#setDescripcionAnomalia(java.lang.String)
	 */
	public void setDescripcionAnomalia(String descripcionAnomalia) {
		this.descripcionAnomalia = descripcionAnomalia;
	}
	public Long getNumeroOrdenInspeccion() {
		return numeroOrdenInspeccion;
	}
	public void setNumeroOrdenInspeccion(Long numeroOrdenInspeccion) {
		this.numeroOrdenInspeccion = numeroOrdenInspeccion;
	}
	public Long getCantidad() {
		return cantidad;
	}
	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}
	
	
	
	
}
