package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.InformacionCargos;
/**
 * 
 * @author Emiliano Arango (ar30557486)
 *
 */
public class InformacionCargosImpl extends SynergiaBusinessObjectImpl implements InformacionCargos {
	
	private Long idPago;
	private Date fechaPago;	
	private String codigoCargo; 	
	private String descripcionCargo;	
	private Long valorCargo; 	
	private String tipoServicio;	
	private Long nroServicio;	
	private Date fechaFactura;	
	private Long nroDocumento;	
	private String origenCargo;	
	private String nroConvenioEncargo;	
	private Long valorPago;
	private String tipoDocumento;
	private Long nroCuenta;
	private String nroDocTitularSF;
	
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public String getCodigoCargo() {
		return codigoCargo;
	}
	public void setCodigoCargo(String codigoCargo) {
		this.codigoCargo = codigoCargo;
	}
	public String getDescripcionCargo() {
		return descripcionCargo;
	}
	public void setDescripcionCargo(String descripcionCargo) {
		this.descripcionCargo = descripcionCargo;
	}
	public Date getFechaFactura() {
		return fechaFactura;
	}
	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}
	public Date getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}
	public String getNroConvenioEncargo() {
		return nroConvenioEncargo;
	}
	public void setNroConvenioEncargo(String nroConvenioEncargo) {
		this.nroConvenioEncargo = nroConvenioEncargo;
	}
	public Long getNroDocumento() {
		return nroDocumento;
	}
	public void setNroDocumento(Long nroDocumento) {
		this.nroDocumento = nroDocumento;
	}
	public Long getNroServicio() {
		return nroServicio;
	}
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	public String getOrigenCargo() {
		return origenCargo;
	}
	public void setOrigenCargo(String origenCargo) {
		this.origenCargo = origenCargo;
	}
	public String getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	public Long getValorCargo() {
		return valorCargo;
	}
	public void setValorCargo(Long valorCargo) {
		this.valorCargo = valorCargo;
	}
	public Long getValorPago() {
		return valorPago;
	}
	public void setValorPago(Long valorPago) {
		this.valorPago = valorPago;
	}
	public Long getIdPago() {
		return idPago;
	}
	public void setIdPago(Long idPago) {
		this.idPago = idPago;
	}
	/**
	 * @return the tipoDocumento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	/**
	 * @return the nroDocTitularSF
	 */
	public String getNroDocTitularSF() {
		return nroDocTitularSF;
	}
	/**
	 * @param nroDocTitularSF the nroDocTitularSF to set
	 */
	public void setNroDocTitularSF(String nroDocTitularSF) {
		this.nroDocTitularSF = nroDocTitularSF;
	}	
}