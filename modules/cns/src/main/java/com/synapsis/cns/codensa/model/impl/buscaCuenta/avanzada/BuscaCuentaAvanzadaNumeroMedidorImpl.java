/**
 * 
 */
package com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada;

import com.synapsis.cns.codensa.model.buscaCuenta.avanzada.BuscaCuentaAvanzadaNumeroMedidor;
import com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaImpl;

/**
 * @author Emiliano Arango (ar30557486)
 *
 */
public class BuscaCuentaAvanzadaNumeroMedidorImpl extends BuscaCuentaImpl implements BuscaCuentaAvanzadaNumeroMedidor {
	private String nroMedidor;

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada.BuscaCuentaAvanzadaNumeroMedidor#getNroMedidor()
	 */
	public String getNroMedidor() {
		return nroMedidor;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada.BuscaCuentaAvanzadaNumeroMedidor#setNroMedidor(java.lang.String)
	 */
	public void setNroMedidor(String nroMedidor) {
		this.nroMedidor = nroMedidor;
	}
	
}
