/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;


import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaDetalleEncargosCobranza;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaDetalleEncargosCobranzaImpl extends
		SynergiaBusinessObjectImpl implements ConsultaDetalleEncargosCobranza {
	   private String tipoSrvFin;          
	   private Long nroSrvFin;             
	   private String nombreTit;           
	   private String nroIdentSocio;       
	   private String tipoIdTitular;       
	   private String nroIdTitular;        
	   private String nombreTitular;       
	   private String lineaNegocio;        
	   private Long nroEco;                
	   private String nroIdEco;            
	   private String socio;               
	   private String producto;            
	   private String plan;                
	   private String nroCuotas;           
	   private Double valCuota;            
	   private String indicadSaldoAnt;     
	   private Date fechaActivacion;       
	   private String nroCuotasFact;       
	   private String nroCuotasFaltantes;  
	   private Double saldoEco;            
	   private Double saldoIntMora;        
	   private Double periodAtrasados;     
	   private Double diasMora;            
	   private Double valCuotasMora;
	   private Long nroCuenta;
	/**
	 * @return the diasMora
	 */
	public Double getDiasMora() {
		return diasMora;
	}
	/**
	 * @param diasMora the diasMora to set
	 */
	public void setDiasMora(Double diasMora) {
		this.diasMora = diasMora;
	}
	/**
	 * @return the fechaActivacion
	 */
	public Date getFechaActivacion() {
		return fechaActivacion;
	}
	/**
	 * @param fechaActivacion the fechaActivacion to set
	 */
	public void setFechaActivacion(Date fechaActivacion) {
		this.fechaActivacion = fechaActivacion;
	}
	/**
	 * @return the indicadSaldoAnt
	 */
	public String getIndicadSaldoAnt() {
		return indicadSaldoAnt;
	}
	/**
	 * @param indicadSaldoAnt the indicadSaldoAnt to set
	 */
	public void setIndicadSaldoAnt(String indicadSaldoAnt) {
		this.indicadSaldoAnt = indicadSaldoAnt;
	}
	/**
	 * @return the lineaNegocio
	 */
	public String getLineaNegocio() {
		return lineaNegocio;
	}
	/**
	 * @param lineaNegocio the lineaNegocio to set
	 */
	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}
	/**
	 * @return the nombreTit
	 */
	public String getNombreTit() {
		return nombreTit;
	}
	/**
	 * @param nombreTit the nombreTit to set
	 */
	public void setNombreTit(String nombreTit) {
		this.nombreTit = nombreTit;
	}
	/**
	 * @return the nombreTitular
	 */
	public String getNombreTitular() {
		return nombreTitular;
	}
	/**
	 * @param nombreTitular the nombreTitular to set
	 */
	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}
	/**
	 * @return the nroCuotas
	 */
	public String getNroCuotas() {
		return nroCuotas;
	}
	/**
	 * @param nroCuotas the nroCuotas to set
	 */
	public void setNroCuotas(String nroCuotas) {
		this.nroCuotas = nroCuotas;
	}
	/**
	 * @return the nroCuotasFact
	 */
	public String getNroCuotasFact() {
		return nroCuotasFact;
	}
	/**
	 * @param nroCuotasFact the nroCuotasFact to set
	 */
	public void setNroCuotasFact(String nroCuotasFact) {
		this.nroCuotasFact = nroCuotasFact;
	}
	/**
	 * @return the nroCuotasFaltantes
	 */
	public String getNroCuotasFaltantes() {
		return nroCuotasFaltantes;
	}
	/**
	 * @param nroCuotasFaltantes the nroCuotasFaltantes to set
	 */
	public void setNroCuotasFaltantes(String nroCuotasFaltantes) {
		this.nroCuotasFaltantes = nroCuotasFaltantes;
	}
	/**
	 * @return the nroEco
	 */
	public Long getNroEco() {
		return nroEco;
	}
	/**
	 * @param nroEco the nroEco to set
	 */
	public void setNroEco(Long nroEco) {
		this.nroEco = nroEco;
	}
	/**
	 * @return the nroIdEco
	 */
	public String getNroIdEco() {
		return nroIdEco;
	}
	/**
	 * @param nroIdEco the nroIdEco to set
	 */
	public void setNroIdEco(String nroIdEco) {
		this.nroIdEco = nroIdEco;
	}
	/**
	 * @return the nroIdentSocio
	 */
	public String getNroIdentSocio() {
		return nroIdentSocio;
	}
	/**
	 * @param nroIdentSocio the nroIdentSocio to set
	 */
	public void setNroIdentSocio(String nroIdentSocio) {
		this.nroIdentSocio = nroIdentSocio;
	}
	/**
	 * @return the nroIdTitular
	 */
	public String getNroIdTitular() {
		return nroIdTitular;
	}
	/**
	 * @param nroIdTitular the nroIdTitular to set
	 */
	public void setNroIdTitular(String nroIdTitular) {
		this.nroIdTitular = nroIdTitular;
	}
	/**
	 * @return the nroSrvFin
	 */
	public Long getNroSrvFin() {
		return nroSrvFin;
	}
	/**
	 * @param nroSrvFin the nroSrvFin to set
	 */
	public void setNroSrvFin(Long nroSrvFin) {
		this.nroSrvFin = nroSrvFin;
	}
	/**
	 * @return the periodAtrasados
	 */
	public Double getPeriodAtrasados() {
		return periodAtrasados;
	}
	/**
	 * @param periodAtrasados the periodAtrasados to set
	 */
	public void setPeriodAtrasados(Double periodAtrasados) {
		this.periodAtrasados = periodAtrasados;
	}
	/**
	 * @return the plan
	 */
	public String getPlan() {
		return plan;
	}
	/**
	 * @param plan the plan to set
	 */
	public void setPlan(String plan) {
		this.plan = plan;
	}
	/**
	 * @return the producto
	 */
	public String getProducto() {
		return producto;
	}
	/**
	 * @param producto the producto to set
	 */
	public void setProducto(String producto) {
		this.producto = producto;
	}
	/**
	 * @return the saldoEco
	 */
	public Double getSaldoEco() {
		return saldoEco;
	}
	/**
	 * @param saldoEco the saldoEco to set
	 */
	public void setSaldoEco(Double saldoEco) {
		this.saldoEco = saldoEco;
	}
	/**
	 * @return the saldoIntMora
	 */
	public Double getSaldoIntMora() {
		return saldoIntMora;
	}
	/**
	 * @param saldoIntMora the saldoIntMora to set
	 */
	public void setSaldoIntMora(Double saldoIntMora) {
		this.saldoIntMora = saldoIntMora;
	}
	/**
	 * @return the socio
	 */
	public String getSocio() {
		return socio;
	}
	/**
	 * @param socio the socio to set
	 */
	public void setSocio(String socio) {
		this.socio = socio;
	}
	/**
	 * @return the tipoIdTitular
	 */
	public String getTipoIdTitular() {
		return tipoIdTitular;
	}
	/**
	 * @param tipoIdTitular the tipoIdTitular to set
	 */
	public void setTipoIdTitular(String tipoIdTitular) {
		this.tipoIdTitular = tipoIdTitular;
	}
	/**
	 * @return the tipoSrvFin
	 */
	public String getTipoSrvFin() {
		return tipoSrvFin;
	}
	/**
	 * @param tipoSrvFin the tipoSrvFin to set
	 */
	public void setTipoSrvFin(String tipoSrvFin) {
		this.tipoSrvFin = tipoSrvFin;
	}
	/**
	 * @return the valCuota
	 */
	public Double getValCuota() {
		return valCuota;
	}
	/**
	 * @param valCuota the valCuota to set
	 */
	public void setValCuota(Double valCuota) {
		this.valCuota = valCuota;
	}
	/**
	 * @return the valCuotasMora
	 */
	public Double getValCuotasMora() {
		return valCuotasMora;
	}
	/**
	 * @param valCuotasMora the valCuotasMora to set
	 */
	public void setValCuotasMora(Double valCuotasMora) {
		this.valCuotasMora = valCuotasMora;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}       
}
