/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaDeudaProvisiones;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 *
 */
public class ConsultaDeudaProvisionesImpl extends SynergiaBusinessObjectImpl implements ConsultaDeudaProvisiones {

	private static final long serialVersionUID = 1L;
	private Long cantidadProvisiones; // esto es el Numero de meses sin lectura real
	private Date fechaUltimaLecturaNormal;
	private Long idCuenta;
	private Long nroCuenta;
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaProvisiones#getCantidadProvisiones()
	 */
	public Long getCantidadProvisiones() {
		return cantidadProvisiones;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaProvisiones#setCantidadProvisiones(java.lang.Long)
	 */
	public void setCantidadProvisiones(Long cantidadProvisiones) {
		this.cantidadProvisiones = cantidadProvisiones;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaProvisiones#getFechaUltimaLecturaNormal()
	 */
	public Date getFechaUltimaLecturaNormal() {
		return fechaUltimaLecturaNormal;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaProvisiones#setFechaUltimaLecturaNormal(java.util.Date)
	 */
	public void setFechaUltimaLecturaNormal(Date fechaUltimaLecturaNormal) {
		this.fechaUltimaLecturaNormal = fechaUltimaLecturaNormal;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaProvisiones#getIdCuenta()
	 */
	public Long getIdCuenta() {
		return idCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaProvisiones#setIdCuenta(java.lang.Long)
	 */
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaProvisiones#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaProvisiones#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	
	
	
	
}
