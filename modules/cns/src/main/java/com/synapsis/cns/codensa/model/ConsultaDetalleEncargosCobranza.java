package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface ConsultaDetalleEncargosCobranza {

	
	public Long getNroCuenta();
	public void setNroCuenta(Long nroCuenta);
	/**
	 * @return the diasMora
	 */
	public Double getDiasMora();

	/**
	 * @param diasMora the diasMora to set
	 */
	public void setDiasMora(Double diasMora);

	/**
	 * @return the fechaActivacion
	 */
	public Date getFechaActivacion();

	/**
	 * @param fechaActivacion the fechaActivacion to set
	 */
	public void setFechaActivacion(Date fechaActivacion);

	/**
	 * @return the indicadSaldoAnt
	 */
	public String getIndicadSaldoAnt();

	/**
	 * @param indicadSaldoAnt the indicadSaldoAnt to set
	 */
	public void setIndicadSaldoAnt(String indicadSaldoAnt);

	/**
	 * @return the lineaNegocio
	 */
	public String getLineaNegocio();

	/**
	 * @param lineaNegocio the lineaNegocio to set
	 */
	public void setLineaNegocio(String lineaNegocio);

	/**
	 * @return the nombreTit
	 */
	public String getNombreTit();

	/**
	 * @param nombreTit the nombreTit to set
	 */
	public void setNombreTit(String nombreTit);

	/**
	 * @return the nombreTitular
	 */
	public String getNombreTitular();

	/**
	 * @param nombreTitular the nombreTitular to set
	 */
	public void setNombreTitular(String nombreTitular);

	/**
	 * @return the nroCuotas
	 */
	public String getNroCuotas();

	/**
	 * @param nroCuotas the nroCuotas to set
	 */
	public void setNroCuotas(String nroCuotas);

	/**
	 * @return the nroCuotasFact
	 */
	public String getNroCuotasFact();

	/**
	 * @param nroCuotasFact the nroCuotasFact to set
	 */
	public void setNroCuotasFact(String nroCuotasFact);

	/**
	 * @return the nroCuotasFaltantes
	 */
	public String getNroCuotasFaltantes();

	/**
	 * @param nroCuotasFaltantes the nroCuotasFaltantes to set
	 */
	public void setNroCuotasFaltantes(String nroCuotasFaltantes);

	/**
	 * @return the nroEco
	 */
	public Long getNroEco();

	/**
	 * @param nroEco the nroEco to set
	 */
	public void setNroEco(Long nroEco);

	/**
	 * @return the nroIdEco
	 */
	public String getNroIdEco();

	/**
	 * @param nroIdEco the nroIdEco to set
	 */
	public void setNroIdEco(String nroIdEco);

	/**
	 * @return the nroIdentSocio
	 */
	public String getNroIdentSocio();

	/**
	 * @param nroIdentSocio the nroIdentSocio to set
	 */
	public void setNroIdentSocio(String nroIdentSocio);

	/**
	 * @return the nroIdTitular
	 */
	public String getNroIdTitular();

	/**
	 * @param nroIdTitular the nroIdTitular to set
	 */
	public void setNroIdTitular(String nroIdTitular);

	/**
	 * @return the nroSrvFin
	 */
	public Long getNroSrvFin();

	/**
	 * @param nroSrvFin the nroSrvFin to set
	 */
	public void setNroSrvFin(Long nroSrvFin);

	/**
	 * @return the periodAtrasados
	 */
	public Double getPeriodAtrasados();

	/**
	 * @param periodAtrasados the periodAtrasados to set
	 */
	public void setPeriodAtrasados(Double periodAtrasados);

	/**
	 * @return the plan
	 */
	public String getPlan();

	/**
	 * @param plan the plan to set
	 */
	public void setPlan(String plan);

	/**
	 * @return the producto
	 */
	public String getProducto();

	/**
	 * @param producto the producto to set
	 */
	public void setProducto(String producto);

	/**
	 * @return the saldoEco
	 */
	public Double getSaldoEco();

	/**
	 * @param saldoEco the saldoEco to set
	 */
	public void setSaldoEco(Double saldoEco);

	/**
	 * @return the saldoIntMora
	 */
	public Double getSaldoIntMora();

	/**
	 * @param saldoIntMora the saldoIntMora to set
	 */
	public void setSaldoIntMora(Double saldoIntMora);

	/**
	 * @return the socio
	 */
	public String getSocio();

	/**
	 * @param socio the socio to set
	 */
	public void setSocio(String socio);

	/**
	 * @return the tipoIdTitular
	 */
	public String getTipoIdTitular();

	/**
	 * @param tipoIdTitular the tipoIdTitular to set
	 */
	public void setTipoIdTitular(String tipoIdTitular);

	/**
	 * @return the tipoSrvFin
	 */
	public String getTipoSrvFin();

	/**
	 * @param tipoSrvFin the tipoSrvFin to set
	 */
	public void setTipoSrvFin(String tipoSrvFin);

	/**
	 * @return the valCuota
	 */
	public Double getValCuota();

	/**
	 * @param valCuota the valCuota to set
	 */
	public void setValCuota(Double valCuota);

	/**
	 * @return the valCuotasMora
	 */
	public Double getValCuotasMora();

	/**
	 * @param valCuotasMora the valCuotasMora to set
	 */
	public void setValCuotasMora(Double valCuotasMora);

}