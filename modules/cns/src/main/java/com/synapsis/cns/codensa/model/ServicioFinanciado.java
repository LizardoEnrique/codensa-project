package com.synapsis.cns.codensa.model;

public interface ServicioFinanciado {

	public Long getIdConvenio();
	
	public void setIdConvenio(Long idConvenio);
	
	public String getTipoServicio();
	
	public void setTipoServicio(String tipoServicio);
	
	public Long getNumeroServicio();
	
	public void setNumeroServicio(Long numeroServicio);
	
	public Long getNroConvenio();
	
	public void setNroConvenio(Long nroConvenio);
}
