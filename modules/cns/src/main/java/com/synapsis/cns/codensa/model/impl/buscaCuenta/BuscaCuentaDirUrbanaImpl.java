/**
 * 
 */
package com.synapsis.cns.codensa.model.impl.buscaCuenta;

import com.synapsis.cns.codensa.model.buscaCuenta.BuscaCuentaDirUrbana;

/**
 * @author Emiliano Arango (ar30557486)
 *
 */
public class BuscaCuentaDirUrbanaImpl extends BuscaCuentaImpl implements BuscaCuentaDirUrbana {
	
	private String claseVia;
	private String nombreVia;
	private String numeroVia;
	private String letraVia;
	private String puntoCardinalVia;	
	private String numeroCruce;
	private String letraCruce;
	private String numeroPlaca;
	private String puntoCardinalCruce;
	private String nombreUnidadPredial;
	private String numeroUnidadPredial;
	private String tipoUnidadPredial;
	private String modalidadUnidadPredial;	
	private String numeroModalidadUnidadPredial;
	private String numeroTipoUnidadPredial;
	private String indicadorLocalizacion;
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaDirUrbana#getIndicadorLocalizacion()
	 */
	public String getIndicadorLocalizacion() {
		return indicadorLocalizacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaDirUrbana#getLetraCruce()
	 */
	public String getLetraCruce() {
		return letraCruce;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaDirUrbana#getLetraVia()
	 */
	public String getLetraVia() {
		return letraVia;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaDirUrbana#getNombreUnidadPredial()
	 */
	public String getNombreUnidadPredial() {
		return nombreUnidadPredial;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaDirUrbana#getNombreVia()
	 */
	public String getNombreVia() {
		return nombreVia;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaDirUrbana#getNumeroCruce()
	 */
	public String getNumeroCruce() {
		return numeroCruce;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaDirUrbana#getNumeroPlaca()
	 */
	public String getNumeroPlaca() {
		return numeroPlaca;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaDirUrbana#getNumeroUnidadPredial()
	 */
	public String getNumeroUnidadPredial() {
		return numeroUnidadPredial;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaDirUrbana#getNumeroVia()
	 */
	public String getNumeroVia() {
		return numeroVia;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaDirUrbana#setIndicadorLocalizacion(java.lang.String)
	 */
	public void setIndicadorLocalizacion(String indicadorLocalizacion) {
		this.indicadorLocalizacion = indicadorLocalizacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaDirUrbana#setLetraCruce(java.lang.String)
	 */
	public void setLetraCruce(String letraCruce) {
		this.letraCruce = letraCruce;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaDirUrbana#setLetraVia(java.lang.String)
	 */
	public void setLetraVia(String letraVia) {
		this.letraVia = letraVia;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaDirUrbana#setNombreUnidadPredial(java.lang.String)
	 */
	public void setNombreUnidadPredial(String nombreUnidadPredial) {
		this.nombreUnidadPredial = nombreUnidadPredial;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaDirUrbana#setNombreVia(java.lang.String)
	 */
	public void setNombreVia(String nombreVia) {
		this.nombreVia = nombreVia;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaDirUrbana#setNumeroCruce(java.lang.String)
	 */
	public void setNumeroCruce(String numeroCruce) {
		this.numeroCruce = numeroCruce;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaDirUrbana#setNumeroPlaca(java.lang.String)
	 */
	public void setNumeroPlaca(String numeroPlaca) {
		this.numeroPlaca = numeroPlaca;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaDirUrbana#setNumeroUnidadPredial(java.lang.String)
	 */
	public void setNumeroUnidadPredial(String numeroUnidadPredial) {
		this.numeroUnidadPredial = numeroUnidadPredial;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaDirUrbana#setNumeroVia(java.lang.String)
	 */
	public void setNumeroVia(String numeroVia) {
		this.numeroVia = numeroVia;
	}
	/**
	 * @return the claseVia
	 */
	public String getClaseVia() {
		return claseVia;
	}
	/**
	 * @param claseVia the claseVia to set
	 */
	public void setClaseVia(String claseVia) {
		this.claseVia = claseVia;
	}
	/**
	 * @return the puntoCardinalVia
	 */
	public String getPuntoCardinalVia() {
		return puntoCardinalVia;
	}
	/**
	 * @param puntoCardinalVia the puntoCardinalVia to set
	 */
	public void setPuntoCardinalVia(String puntoCardinalVia) {
		this.puntoCardinalVia = puntoCardinalVia;
	}
	/**
	 * @return the puntoCardinalCruce
	 */
	public String getPuntoCardinalCruce() {
		return puntoCardinalCruce;
	}
	/**
	 * @param puntoCardinalCruce the puntoCardinalCruce to set
	 */
	public void setPuntoCardinalCruce(String puntoCardinalCruce) {
		this.puntoCardinalCruce = puntoCardinalCruce;
	}
	/**
	 * @return the tipoUnidadPredial
	 */
	public String getTipoUnidadPredial() {
		return tipoUnidadPredial;
	}
	/**
	 * @param tipoUnidadPredial the tipoUnidadPredial to set
	 */
	public void setTipoUnidadPredial(String tipoUnidadPredial) {
		this.tipoUnidadPredial = tipoUnidadPredial;
	}
	/**
	 * @return the numeroTipoUnidadPredial
	 */
	public String getNumeroTipoUnidadPredial() {
		return numeroTipoUnidadPredial;
	}
	/**
	 * @param numeroTipoUnidadPredial the numeroTipoUnidadPredial to set
	 */
	public void setNumeroTipoUnidadPredial(String numeroTipoUnidadPredial) {
		this.numeroTipoUnidadPredial = numeroTipoUnidadPredial;
	}
	/**
	 * @return the modalidadUnidadPredial
	 */
	public String getModalidadUnidadPredial() {
		return modalidadUnidadPredial;
	}
	/**
	 * @param modalidadUnidadPredial the modalidadUnidadPredial to set
	 */
	public void setModalidadUnidadPredial(String modalidadUnidadPredial) {
		this.modalidadUnidadPredial = modalidadUnidadPredial;
	}
	/**
	 * @return the numeroModalidadUnidadPredial
	 */
	public String getNumeroModalidadUnidadPredial() {
		return numeroModalidadUnidadPredial;
	}
	/**
	 * @param numeroModalidadUnidadPredial the numeroModalidadUnidadPredial to set
	 */
	public void setNumeroModalidadUnidadPredial(String numeroModalidadUnidadPredial) {
		this.numeroModalidadUnidadPredial = numeroModalidadUnidadPredial;
	}	
}