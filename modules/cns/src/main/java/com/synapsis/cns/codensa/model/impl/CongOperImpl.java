package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.CongOper;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class CongOperImpl extends SynergiaBusinessObjectImpl implements
		CongOper {

	private Long nroCuenta;
	
	private Long nroOperacion;
	
	private String state;
	
	private Double valorOperacion;
	
	private Date fechaRealizacionCongelacion;
	
	private Date fechaAprobacionCongelacion;
	
    private Date fechaRealizacionDescongelacion;
    
    private Date fechaAprobacionDescongelacion;

	/**
	 * @return the nroOperacion
	 */
	public Long getNroOperacion() {
		return nroOperacion;
	}

	/**
	 * @param nroOperacion the nroOperacion to set
	 */
	public void setNroOperacion(Long nroOperacion) {
		this.nroOperacion = nroOperacion;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the valorOperacion
	 */
	public Double getValorOperacion() {
		return valorOperacion;
	}

	/**
	 * @param valorOperacion the valorOperacion to set
	 */
	public void setValorOperacion(Double valorOperacion) {
		this.valorOperacion = valorOperacion;
	}

	/**
	 * @return the fechaRealizacionCongelacion
	 */
	public Date getFechaRealizacionCongelacion() {
		return fechaRealizacionCongelacion;
	}

	/**
	 * @param fechaRealizacionCongelacion the fechaRealizacionCongelacion to set
	 */
	public void setFechaRealizacionCongelacion(Date fechaRealizacionCongelacion) {
		this.fechaRealizacionCongelacion = fechaRealizacionCongelacion;
	}

	/**
	 * @return the fechaAprobacionCongelacion
	 */
	public Date getFechaAprobacionCongelacion() {
		return fechaAprobacionCongelacion;
	}

	/**
	 * @param fechaAprobacionCongelacion the fechaAprobacionCongelacion to set
	 */
	public void setFechaAprobacionCongelacion(Date fechaAprobacionCongelacion) {
		this.fechaAprobacionCongelacion = fechaAprobacionCongelacion;
	}

	/**
	 * @return the fechaRealizacionDescongelacion
	 */
	public Date getFechaRealizacionDescongelacion() {
		return fechaRealizacionDescongelacion;
	}

	/**
	 * @param fechaRealizacionDescongelacion the fechaRealizacionDescongelacion to set
	 */
	public void setFechaRealizacionDescongelacion(
			Date fechaRealizacionDescongelacion) {
		this.fechaRealizacionDescongelacion = fechaRealizacionDescongelacion;
	}

	/**
	 * @return the fechaAprobacionDescongelacion
	 */
	public Date getFechaAprobacionDescongelacion() {
		return fechaAprobacionDescongelacion;
	}

	/**
	 * @param fechaAprobacionDescongelacion the fechaAprobacionDescongelacion to set
	 */
	public void setFechaAprobacionDescongelacion(Date fechaAprobacionDescongelacion) {
		this.fechaAprobacionDescongelacion = fechaAprobacionDescongelacion;
	}

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

}