package com.synapsis.cns.codensa.service;

import com.suivant.arquitectura.core.dao.DAO;
import com.suivant.arquitectura.core.exception.IllegalDAOAccessException;
import java.util.List;

public interface ConsultaHistoricoDeudadService {

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.service.impl.ConsultaDetalleDeuda#findByCriteria(java.lang.Long, java.lang.String)
	 */
	public List findByCriteria(Long numeroCuenta)
			throws IllegalDAOAccessException;

	/**
	 * retorna el dao asociado
	 * 
	 * @return DAO
	 * @throws IllegalDAOAccessException
	 */
	public DAO getDao(Class klass) throws IllegalDAOAccessException;

}