/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaCuentaDetalleCargosTerceros;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 * 
 */
public class ConsultaCuentaDetalleCargosTercerosImpl extends SynergiaBusinessObjectImpl 
	implements ConsultaCuentaDetalleCargosTerceros {

	private static final long serialVersionUID = 1L;

	
	private Long nroCuenta;
	private Date fechaProceso;
	private String cargo;
	private String codigoCargo;
	private String producto;
	private String identificador;
	private Long valor;
	private String empresaDet;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargosTerceros#getCargo()
	 */
	public String getCargo() {
		return cargo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargosTerceros#setCargo(java.lang.String)
	 */
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargosTerceros#getCodigoCargo()
	 */
	public String getCodigoCargo() {
		return codigoCargo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargosTerceros#setCodigoCargo(java.lang.String)
	 */
	public void setCodigoCargo(String codigoCargo) {
		this.codigoCargo = codigoCargo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargosTerceros#getEmpresaDet()
	 */
	public String getEmpresaDet() {
		return empresaDet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargosTerceros#setEmpresaDet(java.lang.String)
	 */
	public void setEmpresaDet(String empresa) {
		this.empresaDet = empresa;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargosTerceros#getFechaProceso()
	 */
	public Date getFechaProceso() {
		return fechaProceso;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargosTerceros#setFechaProceso(java.util.Date)
	 */
	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargosTerceros#getIdentificador()
	 */
	public String getIdentificador() {
		return identificador;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargosTerceros#setIdentificador(java.lang.String)
	 */
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargosTerceros#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargosTerceros#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargosTerceros#getNumber()
	 */
	public Long getValor() {
		return valor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargosTerceros#setNumber(java.lang.Long)
	 */
	public void setValor(Long number) {
		this.valor = number;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargosTerceros#getProducto()
	 */
	public String getProducto() {
		return producto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargosTerceros#setProducto(java.lang.String)
	 */
	public void setProducto(String producto) {
		this.producto = producto;
	}
}