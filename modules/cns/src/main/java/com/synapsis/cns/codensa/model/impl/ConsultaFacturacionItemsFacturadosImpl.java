/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.ConsultaFacturacionItemsFacturados;

/**
 * @author ar30557486
 * 
 */
public class ConsultaFacturacionItemsFacturadosImpl extends SynergiaBusinessObjectImpl
		implements ConsultaFacturacionItemsFacturados {

	private String codigoItem;
	
	private Date periodoFacturacion;

	private String descripcionItem;

	private Double valorFacturadoItem;
	
	private Long idItemDoc;
	
	

	public Long getIdItemDoc() {
		return idItemDoc;
	}

	public void setIdItemDoc(Long idItemDoc) {
		this.idItemDoc = idItemDoc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionItemsFacturados#getCodigoItem()
	 */
	public String getCodigoItem() {
		return codigoItem;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionItemsFacturados#setCodigoItem(java.lang.Integer)
	 */
	public void setCodigoItem(String codigoItem) {
		this.codigoItem = codigoItem;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionItemsFacturados#getDescripcionItem()
	 */
	public String getDescripcionItem() {
		return descripcionItem;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionItemsFacturados#setDescripcionItem(java.lang.String)
	 */
	public void setDescripcionItem(String descripcionItem) {
		this.descripcionItem = descripcionItem;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionItemsFacturados#getValorFacturadoItem()
	 */
	public Double getValorFacturadoItem() {
		return valorFacturadoItem;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionItemsFacturados#setValorFacturadoItem(java.lang.String)
	 */
	public void setValorFacturadoItem(Double valorFacturadoItem) {
		this.valorFacturadoItem = valorFacturadoItem;
	}

	/**
	 * @return the periodoFacturacion
	 */
	public Date getPeriodoFacturacion() {
		return periodoFacturacion;
	}

	/**
	 * @param periodoFacturacion the periodoFacturacion to set
	 */
	public void setPeriodoFacturacion(Date periodoFacturacion) {
		this.periodoFacturacion = periodoFacturacion;
	}
}