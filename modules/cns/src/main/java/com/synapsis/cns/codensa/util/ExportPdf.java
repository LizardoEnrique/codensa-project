package com.synapsis.cns.codensa.util;

import com.suivant.arquitectura.core.exception.ExporterException;
/*     */ import com.synapsis.synergia.core.context.SynergiaApplicationContext;
/*     */ import java.io.IOException;
/*     */ import java.io.InputStream;
/*     */ import java.text.SimpleDateFormat;
/*     */ import java.util.Date;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ import javax.faces.context.ExternalContext;
/*     */ import javax.faces.context.FacesContext;
/*     */ import javax.servlet.ServletOutputStream;
/*     */ import javax.servlet.http.HttpServletResponse;
/*     */ import net.sf.jasperreports.engine.JRException;
/*     */ import net.sf.jasperreports.engine.JasperRunManager;
/*     */ import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class ExportPdf
/*     */ {
/*     */   private static final String LOGO_PATH = "com/synapsis/cns/codensa/jasper/logo-";
/*     */   public static final String DATE_FORMAT = "yyyyMMddHHmmss";
/*     */   
/*     */   public static void generatePdf(List list, Map map)
/*     */     throws ExporterException
/*     */   {
/*  38 */     putLogo(map);
/*  39 */     generateFile(list, map);
/*     */   }
/*     */   
/*     */   private static void putLogo(Map map) {
/*  43 */     map.put("logoCodensa", "com/synapsis/cns/codensa/jasper/logo-" + "codensa"/*SynergiaApplicationContext.getCurrentImplementationCompany().toLowerCase() */+ 
/*  44 */       ".jpg");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void generateFile(List list, Map map)
/*     */     throws ExporterException
/*     */   {
/*  55 */     String contentType = "application/pdf";
/*  56 */     FacesContext fc = FacesContext.getCurrentInstance();
/*  57 */     HttpServletResponse response = (HttpServletResponse)fc.getExternalContext().getResponse();
/*  58 */     String filename = getPdfFilePreffix(map) + "-" + getTimeStamp() + ".pdf";
/*  59 */     response.setHeader("Content-disposition", "attachment; filename=" + filename);
/*  60 */     response.setContentType(contentType);
/*     */     try {
/*  62 */       ServletOutputStream out = response.getOutputStream();
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*  67 */       byte[] bytes = JasperRunManager.runReportToPdf(getJasperFile(map), map, 
/*  68 */         new JRBeanCollectionDataSource(list));
/*  69 */       response.setContentLength(bytes.length);
/*  70 */       out.write(bytes, 0, bytes.length);
/*     */       
/*  72 */       out.close();
/*     */     }
/*     */     catch (IOException e) {
/*  75 */       e.printStackTrace();
/*  76 */       throw new ExporterException("exportar.epdf.error");
/*     */     }
/*     */     catch (JRException e) {
/*  79 */       e.printStackTrace();
/*     */     }
/*     */     catch (Exception e) {
/*  82 */       e.printStackTrace();
/*     */     }
/*  84 */     fc.responseComplete();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String getPdfFilePreffix(Map map)
/*     */   {
/*  94 */     return (String)map.get("pdfFilePreffix");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static String getJasperFileName(Map map)
/*     */   {
/* 104 */     return (String)map.get("jasperFileName");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static InputStream getJasperFile(Map map)
/*     */   {
/* 113 */     return ExportPdf.class.getResourceAsStream(getJasperFileName(map));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static String getTimeStamp()
/*     */   {
/* 122 */     return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
/*     */   }
/*     */ }