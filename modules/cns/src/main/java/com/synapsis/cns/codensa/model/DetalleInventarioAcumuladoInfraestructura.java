package com.synapsis.cns.codensa.model;

import java.math.BigDecimal;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author ccamba
 * 
 */
public interface DetalleInventarioAcumuladoInfraestructura extends SynergiaBusinessObject {

	public Long getNroCuenta();

	public Long getNroServicio();

	public Long getIdClienteTelem();

	public Long getPeriodo();

	public String getZona();

	public String getProducto();

	public String getSubProducto();

	public Integer getGrupo();

	public BigDecimal getCantidad();

	public BigDecimal getAcumuladoApoyos();

	public BigDecimal getAcumuladoValores();

	public BigDecimal getDescuentoEstructura();

	public BigDecimal getValorDescEstructura();

	public BigDecimal getAcumuladoValoresDescuento();

	public BigDecimal getTarifaAplicar();

	public String getEstadoFacturacion();

	public void setNroCuenta(Long nroCuenta);

	public void setNroServicio(Long nroServicio);

	public void setIdClienteTelem(Long idClienteTelem);

	public void setPeriodo(Long periodo);

	public void setZona(String zona);

	public void setProducto(String producto);

	public void setSubProducto(String subProducto);

	public void setGrupo(Integer grupo);

	public void setCantidad(BigDecimal cantidad);

	public void setAcumuladoApoyos(BigDecimal acumuladoApoyos);

	public void setAcumuladoValores(BigDecimal acumuladoValores);

	public void setDescuentoEstructura(BigDecimal descuentoEstructura);

	public void setValorDescEstructura(BigDecimal valorDescEstructura);

	public void setAcumuladoValoresDescuento(BigDecimal acumuladoValoresDescuento);

	public void setTarifaAplicar(BigDecimal tarifaAplicar);

	public void setEstadoFacturacion(String estadoFacturacion);

}
