package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.DatosComerciales;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class DatosComercialesImpl extends SynergiaBusinessObjectImpl implements DatosComerciales {

	private Long nroCuenta;
	private String mercado;
	private String sucursal;
	private String direccionReparto;
	private String rutaReparto;
	private String tarifa;
	private String estrato;
	private String claseServicio;
	private String subclaseServicio;
	private String nombreEjecutivo;
	private String nroTipoServ;
	private Double potenciaInstalada;
	private String potenciaFPHP;
	private String estadoServConexion;
	private String giro;
	private String tipoLiquidacion = "Mensual";
	private String tipoEsquemaFacturacion = "Tradicional";

	/**
	 * @return the nroTipoServ
	 */
	public String getNroTipoServ() {
		return nroTipoServ;
	}

	/**
	 * @param nroTipoServ the nroTipoServ to set
	 */
	public void setNroTipoServ(String nroTipoServ) {
		this.nroTipoServ = nroTipoServ;
	}

	/**
	 * @return the claseServicio
	 */
	public String getClaseServicio() {
		return claseServicio;
	}

	/**
	 * @param claseServicio the claseServicio to set
	 */
	public void setClaseServicio(String claseServicio) {
		this.claseServicio = claseServicio;
	}

	/**
	 * @return the direccionReparto
	 */
	public String getDireccionReparto() {
		return direccionReparto;
	}

	/**
	 * @param direccionReparto the direccionReparto to set
	 */
	public void setDireccionReparto(String direccionReparto) {
		this.direccionReparto = direccionReparto;
	}

	/**
	 * @return the estrato
	 */
	public String getEstrato() {
		return estrato;
	}

	/**
	 * @param estrato the estrato to set
	 */
	public void setEstrato(String estrato) {
		this.estrato = estrato;
	}

	/**
	 * @return the mercado
	 */
	public String getMercado() {
		return mercado;
	}

	/**
	 * @param mercado the mercado to set
	 */
	public void setMercado(String mercado) {
		this.mercado = mercado;
	}

	/**
	 * @return the nombreEjecutivo
	 */
	public String getNombreEjecutivo() {
		return nombreEjecutivo;
	}

	/**
	 * @param nombreEjecutivo the nombreEjecutivo to set
	 */
	public void setNombreEjecutivo(String nombreEjecutivo) {
		this.nombreEjecutivo = nombreEjecutivo;
	}

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	/**
	 * @return the rutaReparto
	 */
	public String getRutaReparto() {
		return rutaReparto;
	}

	/**
	 * @param rutaReparto the rutaReparto to set
	 */
	public void setRutaReparto(String rutaReparto) {
		this.rutaReparto = rutaReparto;
	}

	/**
	 * @return the subclaseServicio
	 */
	public String getSubclaseServicio() {
		return subclaseServicio;
	}

	/**
	 * @param subclaseServicio the subclaseServicio to set
	 */
	public void setSubclaseServicio(String subclaseServicio) {
		this.subclaseServicio = subclaseServicio;
	}

	/**
	 * @return the sucursal
	 */
	public String getSucursal() {
		return sucursal;
	}

	/**
	 * @param sucursal the sucursal to set
	 */
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	/**
	 * @return the tarifa
	 */
	public String getTarifa() {
		return tarifa;
	}

	/**
	 * @param tarifa the tarifa to set
	 */
	public void setTarifa(String tarifa) {
		this.tarifa = tarifa;
	}

	public String getPotenciaFPHP() {
		return potenciaFPHP;
	}

	public void setPotenciaFPHP(String potenciaFPHP) {
		this.potenciaFPHP = potenciaFPHP;
	}

	public Double getPotenciaInstalada() {
		return potenciaInstalada;
	}

	public void setPotenciaInstalada(Double potenciaInstalada) {
		this.potenciaInstalada = potenciaInstalada;
	}

	public String getEstadoServConexion() {
		return estadoServConexion;
	}

	public void setEstadoServConexion(String estadoServConexion) {
		this.estadoServConexion = estadoServConexion;
	}

	public String getGiro() {
		return giro;
	}

	public void setGiro(String giro) {
		this.giro = giro;
	}

	public String getTipoLiquidacion() {
		return tipoLiquidacion;
	}

	public void setTipoLiquidacion(String tipoLiquidacion) {
		this.tipoLiquidacion = tipoLiquidacion;
	}

	public String getTipoEsquemaFacturacion() {
		return tipoEsquemaFacturacion;
	}

	public void setTipoEsquemaFacturacion(String tipoEsquemaFacturacion) {
		this.tipoEsquemaFacturacion = tipoEsquemaFacturacion;
	}

}