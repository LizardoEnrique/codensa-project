/**
 * 
 */
package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;



/**
 * @author dBraccio
 *
 * 16/11/2006
 */
public interface AntecedenteTransformador extends SynergiaBusinessObject {
	
	public String getEstadoNegociacion();
	public void setEstadoNegociacion(String estadoNegociacion);
	public String getNombreActivo();
	public void setNombreActivo(String nombreActivo) ;
	public Long getNroCuenta() ;
	public void setNroCuenta(Long nroCuenta);
	public Long getNroServicio() ;
	public void setNroServicio(Long nroServicio);
	public String getNumeroTransformador();
	public void setNumeroTransformador(String numeroTransformador);	
	public String getPropiedadTransformador() ;
	public void setPropiedadTransformador(String propiedadTransformador);
	public String getObjetoCompra() ;
	public void setObjetoCompra(String objetoCompra);
	
}
