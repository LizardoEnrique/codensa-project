package com.synapsis.cns.codensa.model;

/**
 * Cargos del Duplicado Legal (el valor es String, se obtiene del registro de impresion)
 * 
 * @author egrande
 */
public interface CargoFacturaDuplicadoLegal extends CargoFacturaComun {
	public String getValor();

	public void setValor(String valor);
}
