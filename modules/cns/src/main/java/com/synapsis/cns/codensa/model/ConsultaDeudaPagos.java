package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.1 $ $Date: 2007/02/28 21:47:47 $
 * 
 */
public interface ConsultaDeudaPagos extends SynergiaBusinessObject {

	/**
	 * @return the entidadRecaudadora
	 */
	public String getEntidadRecaudadora();

	/**
	 * @param entidadRecaudadora
	 *            the entidadRecaudadora to set
	 */
	public void setEntidadRecaudadora(String entidadRecaudadora);

	/**
	 * @return the fechaUltimoPago
	 */
	public Date getFechaUltimoPago();

	/**
	 * @param fechaUltimoPago
	 *            the fechaUltimoPago to set
	 */
	public void setFechaUltimoPago(Date fechaUltimoPago);

	/**
	 * @return the medioPago
	 */
	public String getMedioPago();

	/**
	 * @param medioPago
	 *            the medioPago to set
	 */
	public void setMedioPago(String medioPago);

	/**
	 * @return the nombreSucursalRecaudo
	 */
	public String getNombreSucursalRecaudo();

	/**
	 * @param nombreSucursalRecaudo
	 *            the nombreSucursalRecaudo to set
	 */
	public void setNombreSucursalRecaudo(String nombreSucursalRecaudo);

	/**
	 * @return the valorTotalUltimoPago
	 */
	public Double getValorTotalUltimoPago();

	/**
	 * @param valorTotalUltimoPago
	 *            the valorTotalUltimoPago to set
	 */
	public void setValorTotalUltimoPago(Double valorTotalUltimoPago);

}