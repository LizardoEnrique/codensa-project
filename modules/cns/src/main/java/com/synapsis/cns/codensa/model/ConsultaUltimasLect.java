package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface ConsultaUltimasLect {

	public abstract Double getEnergiaFp();

	public abstract void setEnergiaFp(Double energiaFp);

	public abstract Date getFechaLectura();

	public abstract void setFechaLectura(Date fechaLectura);

	public abstract Double getLectEne_fp();

	public abstract void setLectEne_fp(Double lectEne_fp);

	public abstract String getLectura();

	public abstract void setLectura(String lectura);

	public abstract Long getNroCuenta();

	public abstract void setNroCuenta(Long nroCuenta);

}