/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaReposicion;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author AR18799631
 *
 */
public class ConsultaReposicionImpl extends SynergiaBusinessObjectImpl implements ConsultaReposicion 
{
	private Long nroCuenta;
	private Date fechaEmision;
	private String motivo;
	private String accRealizada;
	private Date fechaReal;
	/**
	 * @return the accRealizada
	 */
	public String getAccRealizada() {
		return accRealizada;
	}
	/**
	 * @param accRealizada the accRealizada to set
	 */
	public void setAccRealizada(String accRealizada) {
		this.accRealizada = accRealizada;
	}
	/**
	 * @return the fechaEmision
	 */
	public Date getFechaEmision() {
		return fechaEmision;
	}
	/**
	 * @param fechaEmision the fechaEmision to set
	 */
	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	/**
	 * @return the fechaReal
	 */
	public Date getFechaReal() {
		return fechaReal;
	}
	/**
	 * @param fechaReal the fechaReal to set
	 */
	public void setFechaReal(Date fechaReal) {
		this.fechaReal = fechaReal;
	}
	/**
	 * @return the motivo
	 */
	public String getMotivo() {
		return motivo;
	}
	/**
	 * @param motivo the motivo to set
	 */
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
}
