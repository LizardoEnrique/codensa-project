/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaCuentaDatosMedidor;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 *
 */
public class ConsultaCuentaDatosMedidorImpl extends SynergiaBusinessObjectImpl implements ConsultaCuentaDatosMedidor {
	
	private static final long serialVersionUID = 1L;
	
	private String nroMedidor;
	private String marcaMedidor;
	private String tipoMedidor;
	private Long factorLiquidacion;
	private Long cargaContratada;
	private Long cargaInstalada;
	private Date fechaProceso;
	private String factorPulso;
	private Long idConfigFact;
	private Long nroCuenta;
	private Long idDetalleCtaMed;
	
	
	
	
	/**
	 * @return the idConfigFact
	 */
	public Long getIdConfigFact() {
		return idConfigFact;
	}
	/**
	 * @param idConfigFact the idConfigFact to set
	 */
	public void setIdConfigFact(Long idConfigFact) {
		this.idConfigFact = idConfigFact;
	}
	/**
	 * @return the fechaProceso
	 */
	public Date getFechaProceso() {
		return fechaProceso;
	}
	/**
	 * @param fechaProceso the fechaProceso to set
	 */
	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosMedidor#getCargaContratada()
	 */
	public Long getCargaContratada() {
		return cargaContratada;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosMedidor#setCargaContratada(java.lang.Long)
	 */
	public void setCargaContratada(Long cargaContratada) {
		this.cargaContratada = cargaContratada;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosMedidor#getCargaInstalada()
	 */
	public Long getCargaInstalada() {
		return cargaInstalada;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosMedidor#setCargaInstalada(java.lang.Long)
	 */
	public void setCargaInstalada(Long cargaInstalada) {
		this.cargaInstalada = cargaInstalada;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosMedidor#getFactorLiquidacion()
	 */
	public Long getFactorLiquidacion() {
		return factorLiquidacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosMedidor#setFactorLiquidacion(java.lang.Long)
	 */
	public void setFactorLiquidacion(Long factorLiquidacion) {
		this.factorLiquidacion = factorLiquidacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosMedidor#getMarcaMedidor()
	 */
	public String getMarcaMedidor() {
		return marcaMedidor;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosMedidor#setMarcaMedidor(java.lang.String)
	 */
	public void setMarcaMedidor(String marcaMedidor) {
		this.marcaMedidor = marcaMedidor;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosMedidor#getNroMedidor()
	 */
	public String getNroMedidor() {
		return nroMedidor;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosMedidor#setNroMedidor(java.lang.String)
	 */
	public void setNroMedidor(String nroMedidor) {
		this.nroMedidor = nroMedidor;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosMedidor#getTipoMedidor()
	 */
	public String getTipoMedidor() {
		return tipoMedidor;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosMedidor#setTipoMedidor(java.lang.String)
	 */
	public void setTipoMedidor(String tipoMedidor) {
		this.tipoMedidor = tipoMedidor;
	}
	public String getFactorPulso() {
		return factorPulso;
	}
	public void setFactorPulso(String factorPulso) {
		this.factorPulso = factorPulso;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public Long getIdDetalleCtaMed() {
		return idDetalleCtaMed;
		
	}
	public void setIdDetalleCtaMed(Long idDetalleCtaMed) {
		this.idDetalleCtaMed = idDetalleCtaMed;
		
	}
}
