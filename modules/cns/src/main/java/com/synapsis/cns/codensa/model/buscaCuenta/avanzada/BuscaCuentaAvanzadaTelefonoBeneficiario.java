package com.synapsis.cns.codensa.model.buscaCuenta.avanzada;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface BuscaCuentaAvanzadaTelefonoBeneficiario extends SynergiaBusinessObject {

	/**
	 * @return the telefonoBeneficiario
	 */
	public String getTelefonoBeneficiario();

	/**
	 * @param telefonoBeneficiario the telefonoBeneficiario to set
	 */
	public void setTelefonoBeneficiario(String telefonoBeneficiario);

}