/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaHistoriaEstadoConvenio;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaHistoriaEstadoConvenioImpl extends
		SynergiaBusinessObjectImpl implements ConsultaHistoriaEstadoConvenio {
	   private Long nroServicio;           
	   private String nroSolicitante;      
	   private String lineaNegocio;        
	   private Date fechaActivacion;       
	   private Double valorOriginal;       
	   private Double saldoServicio;       
	   private Long nroCuenta;
	/**
	 * @return the fechaActivacion
	 */
	public Date getFechaActivacion() {
		return fechaActivacion;
	}
	/**
	 * @param fechaActivacion the fechaActivacion to set
	 */
	public void setFechaActivacion(Date fechaActivacion) {
		this.fechaActivacion = fechaActivacion;
	}
	/**
	 * @return the lineaNegocio
	 */
	public String getLineaNegocio() {
		return lineaNegocio;
	}
	/**
	 * @param lineaNegocio the lineaNegocio to set
	 */
	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	/**
	 * @return the nroSolicitante
	 */
	public String getNroSolicitante() {
		return nroSolicitante;
	}
	/**
	 * @param nroSolicitante the nroSolicitante to set
	 */
	public void setNroSolicitante(String nroSolicitante) {
		this.nroSolicitante = nroSolicitante;
	}
	/**
	 * @return the saldoServicio
	 */
	public Double getSaldoServicio() {
		return saldoServicio;
	}
	/**
	 * @param saldoServicio the saldoServicio to set
	 */
	public void setSaldoServicio(Double saldoServicio) {
		this.saldoServicio = saldoServicio;
	}
	/**
	 * @return the valorOriginal
	 */
	public Double getValorOriginal() {
		return valorOriginal;
	}
	/**
	 * @param valorOriginal the valorOriginal to set
	 */
	public void setValorOriginal(Double valorOriginal) {
		this.valorOriginal = valorOriginal;
	}             
}
