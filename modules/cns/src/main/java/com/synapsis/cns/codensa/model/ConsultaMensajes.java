package com.synapsis.cns.codensa.model;

public interface ConsultaMensajes {

	/**
	 * @return the mensaje1
	 */
	public String getMensaje1();

	/**
	 * @param mensaje1 the mensaje1 to set
	 */
	public void setMensaje1(String mensaje1);

	/**
	 * @return the mensaje2
	 */
	public String getMensaje2();

	/**
	 * @param mensaje2 the mensaje2 to set
	 */
	public void setMensaje2(String mensaje2);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

}