/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaDeudaMesesSinLecturas;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 *
 */
public class ConsultaDeudaMesesSinLecturasImpl extends SynergiaBusinessObjectImpl implements ConsultaDeudaMesesSinLecturas {

	private static final long serialVersionUID = 1L;
	
	private Long idCuenta;
	private Long nroCuenta;
	private Long idServicio;
	private Long nroServicio;
	private Long mesesSinLectura;
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaLecturasReales#getIdCuenta()
	 */
	public Long getIdCuenta() {
		return idCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaLecturasReales#setIdCuenta(java.lang.Long)
	 */
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaLecturasReales#getIdServicio()
	 */
	public Long getIdServicio() {
		return idServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaLecturasReales#setIdServicio(java.lang.Long)
	 */
	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaLecturasReales#getMesesSinLectura()
	 */
	public Long getMesesSinLectura() {
		return mesesSinLectura;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaLecturasReales#setMesesSinLectura(java.lang.Long)
	 */
	public void setMesesSinLectura(Long mesesSinLectura) {
		this.mesesSinLectura = mesesSinLectura;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaLecturasReales#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaLecturasReales#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaLecturasReales#getNroServicio()
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaLecturasReales#setNroServicio(java.lang.Long)
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	
	
	
}
