package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.cns.codensa.model.DeudaDetTotalValorCastigado;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class DeudaDetTotalValorCastigadoImpl extends SynergiaBusinessObjectImpl
		implements DeudaDetTotalValorCastigado {

	private Long nroCuenta;
	
	private String codCargo;

	private String descCargo;

	private Double valorCargo;

	private Long correlativoFacturacion;

	private String usuarioCreador;

	private String nombreUsuarioCreador;

	private Date fechaCastigo;

	private String motivoCastigo;

	private String tipoCastigo;

	private String cargoOriginal;

	private String nroDocumento;
	
	private String observaciones;
	
	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getCodCargo() {
		return codCargo;
	}

	public void setCodCargo(String codCargo) {
		this.codCargo = codCargo;
	}

	public String getDescCargo() {
		return descCargo;
	}

	public void setDescCargo(String descCargo) {
		this.descCargo = descCargo;
	}

	public Double getValorCargo() {
		return valorCargo;
	}

	public void setValorCargo(Double valorCargo) {
		this.valorCargo = valorCargo;
	}

	public Long getCorrelativoFacturacion() {
		return correlativoFacturacion;
	}

	public void setCorrelativoFacturacion(Long correlativoFacturacion) {
		this.correlativoFacturacion = correlativoFacturacion;
	}


	public String getUsuarioCreador() {
		return usuarioCreador;
	}

	public void setUsuarioCreador(String usuarioCreador) {
		this.usuarioCreador = usuarioCreador;
	}

	public String getNombreUsuarioCreador() {
		return nombreUsuarioCreador;
	}

	public void setNombreUsuarioCreador(String nombreUsuarioCreador) {
		this.nombreUsuarioCreador = nombreUsuarioCreador;
	}

	public Date getFechaCastigo() {
		return fechaCastigo;
	}

	public void setFechaCastigo(Date fechaCastigo) {
		this.fechaCastigo = fechaCastigo;
	}

	public String getMotivoCastigo() {
		return motivoCastigo;
	}

	public void setMotivoCastigo(String motivoCastigo) {
		this.motivoCastigo = motivoCastigo;
	}

	public String getTipoCastigo() {
		return tipoCastigo;
	}

	public void setTipoCastigo(String tipoCastigo) {
		this.tipoCastigo = tipoCastigo;
	}

	public String getCargoOriginal() {
		return cargoOriginal;
	}

	public void setCargoOriginal(String cargoOriginal) {
		this.cargoOriginal = cargoOriginal;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}
	
	
}