/**
 * 
 */
package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author dbraccio
 *
 */
public interface ConsultaRefacturacionCongelacionUsuario extends
		SynergiaBusinessObject {
	
	public String getAccionQueEjecuto();
	public String getAnalistaAprobador() ;
	public String getAnalistaQueDebeAprobar();
	public String getAnalistaQueElimino() ;
	public Date getFechaAprobacion() ;
	public Date getFechaEliminacion() ;
	public Date getFechaIntervencion() ;
	public Date getHoraAprobacion() ;
	public Date getHoraEliminacion() ;
	public Long getIdCuenta() ;
	public Long getIdOperacion();
	public Long getNroCuenta() ;
	public Long getNroOperacion();
	public Double getTiempoIntervencion();
	public String getUsuariosIntervinientes();


}
