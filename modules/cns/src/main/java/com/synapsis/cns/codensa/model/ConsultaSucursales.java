package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaSucursales extends SynergiaBusinessObject {

	/**
	 * @return the codigo
	 */
	public Long getCodigo();

	/**
	 * @return the descripcion
	 */
	public String getDescripcion();

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(Long codigo);

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion);

}