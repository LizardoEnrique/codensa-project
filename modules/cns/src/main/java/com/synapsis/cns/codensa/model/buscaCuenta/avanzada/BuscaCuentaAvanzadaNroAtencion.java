package com.synapsis.cns.codensa.model.buscaCuenta.avanzada;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface BuscaCuentaAvanzadaNroAtencion extends SynergiaBusinessObject{

	/**
	 * @return the nroAtencion
	 */
	public Long getNroAtencion();

	/**
	 * @param nroAtencion the nroAtencion to set
	 */
	public void setNroAtencion(Long nroAtencion);

}