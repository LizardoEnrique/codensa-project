/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaExpedientesKilowatios;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author AR18799631
 *
 */
public class ConsultaExpedientesKilowatiosImpl extends
		SynergiaBusinessObjectImpl implements ConsultaExpedientesKilowatios {
	   private Long idServicio;          
	   private Long nroExpediente;     
	   private Double consumoTotal;        
	   private Double consumoReintegrar;
	   private Double promedio;          

	/**
	 * @return the consumoReintegrar
	 */
	public Double getConsumoReintegrar() {
		return consumoReintegrar;
	}
	/**
	 * @param consumoReintegrar the consumoReintegrar to set
	 */
	public void setConsumoReintegrar(Double consumoReintegrar) {
		this.consumoReintegrar = consumoReintegrar;
	}
	/**
	 * @return the consumoTotal
	 */
	public Double getConsumoTotal() {
		return consumoTotal;
	}
	/**
	 * @param consumoTotal the consumoTotal to set
	 */
	public void setConsumoTotal(Double consumoTotal) {
		this.consumoTotal = consumoTotal;
	}
	/**
	 * @return the idCnrExpediente
	 */
	public Long getNroExpediente() {
		return nroExpediente;
	}
	/**
	 * @param idCnrExpediente the idCnrExpediente to set
	 */
	public void setNroExpediente(Long nroExpediente) {
		this.nroExpediente = nroExpediente;
	}
	/**
	 * @return the idServicio
	 */
	public Long getIdServicio() {
		return idServicio;
	}
	/**
	 * @param idServicio the idServicio to set
	 */
	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}
	public Double getPromedio() {
		return promedio;
	}
	public void setPromedio(Double promedio) {
		this.promedio = promedio;
	}   
}
