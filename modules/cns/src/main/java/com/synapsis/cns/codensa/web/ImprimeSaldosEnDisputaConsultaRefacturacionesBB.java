package com.synapsis.cns.codensa.web;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;

import com.synapsis.integration.codensa.integration.kendari.URLParameter;

/**
 * @author ar29302553
 */
public class ImprimeSaldosEnDisputaConsultaRefacturacionesBB extends
		ConsultaRefacturacionesKendariBB{

	public String execKndUseCase(ActionEvent event) {
		List parameters = new ArrayList();

		parameters.addAll(makeParameters());
		parameters.addAll(this.makeSpecificParameters());

		URLParameter[] urlURLParameter = new URLParameter[parameters.size()];
		for (int i = 0; i < urlURLParameter.length; i++) {
			urlURLParameter[i] = (URLParameter) parameters.get(i);
		}

		this.setUseCaseUrl(this.getUrlHelper().getUrlComplete(
				"ConsultaSEDAMEUseCaseFactory",
				urlURLParameter));

		return this.getUseCaseUrl();
	}
}