/**
 * 
 */
package com.synapsis.cns.codensa.service.impl;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.suivant.arquitectura.core.dao.DAO;
import com.suivant.arquitectura.core.exception.IllegalDAOAccessException;
import com.suivant.arquitectura.core.integration.Module;
import com.suivant.arquitectura.core.queryFilter.CompositeQueryFilter;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.queryFilter.SimpleQueryFilter;
import com.synapsis.cns.codensa.service.ConsultaDetalleDeudaService;
import com.synapsis.synergia.core.service.impl.SynergiaServiceImpl;

/**
 * @author Paola Attadio
 * @version $Revision: 1.4 $
 */
public class ConsultaDetalleDeudaServiceImpl extends SynergiaServiceImpl
		implements ConsultaDetalleDeudaService {

	private static final String DEUDA_TOTAL_ACTUAL = "DTA";

	private static final String SALDO_TOTAL_SUJETO_INTERESES = "STSI";

	private static final String SALDO_TOTAL_NO_SUJETO_INTERESES = "STNSI";

	private static final String VALOR_TOTAL_INTERESES = "VTI";

	private static final String VALOR_TOTAL_SANCIONES = "VTS";

	private static final String VALOR_TOTAL_OTROS_NEGOCIOS = "VTON";

	private static final String DEUDA_CONVENIO = "DC";

	private static final String DEUDA_CODENSA_HOGAR = "DCH";

	public ConsultaDetalleDeudaServiceImpl() {
		super();
	}

	public ConsultaDetalleDeudaServiceImpl(Module module) {
		super(module);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.service.impl.ConsultaDetalleDeuda#findByCriteria(java.lang.Long,
	 *      java.lang.String)
	 */
	public List findByCriteria(QueryFilter filter)
			throws IllegalDAOAccessException {

		Class klass = null;
		List results = null;
		String detailName = searchDetailName((CompositeQueryFilter)filter);
		/*
		 * Falta definir si tiene o no detalle !!!! if
		 * (StringUtils.equalsIgnoreCase(detailName, DEUDA_TOTAL_ACTUAL)) klass =
		 * com.synapsis.cns.codensa.model.impl.DeudaCargosNoSujetoInteresesImpl.class;
		 */
		if (StringUtils.equalsIgnoreCase(detailName,
				SALDO_TOTAL_SUJETO_INTERESES))
			klass = com.synapsis.cns.codensa.model.impl.DeudaSaldoSujetoAInteresesImpl.class;
		else if (StringUtils.equalsIgnoreCase(detailName,
				SALDO_TOTAL_NO_SUJETO_INTERESES))
			klass = com.synapsis.cns.codensa.model.impl.DeudaCargosNoSujetoInteresesImpl.class;
		else if (StringUtils
				.equalsIgnoreCase(detailName, VALOR_TOTAL_INTERESES))
			klass = com.synapsis.cns.codensa.model.impl.DeudaInteresesImpl.class;
		else if (StringUtils
				.equalsIgnoreCase(detailName, VALOR_TOTAL_SANCIONES))
			klass = com.synapsis.cns.codensa.model.impl.ValorTotalSancionesImpl.class;
		else if (StringUtils.equalsIgnoreCase(detailName,
				VALOR_TOTAL_OTROS_NEGOCIOS))
			klass = com.synapsis.cns.codensa.model.impl.ValorTotalOtrosNegociosImpl.class;
		else if (StringUtils.equalsIgnoreCase(detailName, DEUDA_CONVENIO))
			klass = com.synapsis.cns.codensa.model.impl.DeudaConvenioImpl.class;
		else if (StringUtils.equalsIgnoreCase(detailName, DEUDA_CODENSA_HOGAR))
			klass = com.synapsis.cns.codensa.model.impl.DeudaCodensaHogarImpl.class;
		else if (StringUtils.equalsIgnoreCase(detailName, DEUDA_TOTAL_ACTUAL))
			klass = com.synapsis.cns.codensa.model.impl.DeudaTotalActualImpl.class;
		QueryFilter q = obtainFilter((CompositeQueryFilter)filter);
		((QueryFilter)q).setQueryFilter(this.getEmpresaQueryFilter());
		q.setPositionStart(filter.getPositionStart().intValue());
		q.setMaxResults(filter.getMaxResults().intValue());
		if (klass != null)
			results = getDao(klass).findByCriteria(q);

		return results;
	}

	public int findByCriteriaToPageable(QueryFilter filter)
			throws IllegalDAOAccessException {

		Class klass = null;
		int results=0;
		String detailName = searchDetailName((CompositeQueryFilter)filter);

		/*
		 * Falta definir si tiene o no detalle !!!! if
		 * (StringUtils.equalsIgnoreCase(detailName, DEUDA_TOTAL_ACTUAL)) klass =
		 * com.synapsis.cns.codensa.model.impl.DeudaCargosNoSujetoInteresesImpl.class;
		 */
		if (StringUtils.equalsIgnoreCase(detailName,
				SALDO_TOTAL_SUJETO_INTERESES))
			klass = com.synapsis.cns.codensa.model.impl.DeudaSaldoSujetoAInteresesImpl.class;
		else if (StringUtils.equalsIgnoreCase(detailName,
				SALDO_TOTAL_NO_SUJETO_INTERESES))
			klass = com.synapsis.cns.codensa.model.impl.DeudaCargosNoSujetoInteresesImpl.class;
		else if (StringUtils
				.equalsIgnoreCase(detailName, VALOR_TOTAL_INTERESES))
			klass = com.synapsis.cns.codensa.model.impl.DeudaInteresesImpl.class;
		else if (StringUtils
				.equalsIgnoreCase(detailName, VALOR_TOTAL_SANCIONES))
			klass = com.synapsis.cns.codensa.model.impl.ValorTotalSancionesImpl.class;
		else if (StringUtils.equalsIgnoreCase(detailName,
				VALOR_TOTAL_OTROS_NEGOCIOS))
			klass = com.synapsis.cns.codensa.model.impl.ValorTotalOtrosNegociosImpl.class;
		else if (StringUtils.equalsIgnoreCase(detailName, DEUDA_CONVENIO))
			klass = com.synapsis.cns.codensa.model.impl.DeudaConvenioImpl.class;
		else if (StringUtils.equalsIgnoreCase(detailName, DEUDA_CODENSA_HOGAR))
			klass = com.synapsis.cns.codensa.model.impl.DeudaCodensaHogarImpl.class;
		else if (StringUtils.equalsIgnoreCase(detailName, DEUDA_TOTAL_ACTUAL))
			klass = com.synapsis.cns.codensa.model.impl.DeudaTotalActualImpl.class;

		QueryFilter q = obtainFilter((CompositeQueryFilter)filter);
		((QueryFilter)q).setQueryFilter(this.getEmpresaQueryFilter());
		if (klass != null)
			 results = getDao(klass).countAll(q);
		return results;
	}

	private String searchDetailName(CompositeQueryFilter cqf) {
		Iterator it = cqf.getFiltersList().iterator();
		while (it.hasNext()) {
			Object ofilter = it.next();
				SimpleQueryFilter sqf = (SimpleQueryFilter) ofilter;
					if( StringUtils.equalsIgnoreCase(sqf.getAttributeName(), "detailName"))
						return sqf.getAttributeValue().toString();
		}
		return "";
	}
	
	private QueryFilter obtainFilter(CompositeQueryFilter filter){
		Iterator it = filter.getFiltersList().iterator();
		while (it.hasNext()) {
			Object ofilter = it.next();
				SimpleQueryFilter sqf = (SimpleQueryFilter) ofilter;
					if( StringUtils.equalsIgnoreCase(sqf.getAttributeName(), "nroCuenta"))
						return sqf;
		}
		return null;
	}

	/**
	 * retorna el dao asociado
	 * 
	 * @return DAO
	 * @throws IllegalDAOAccessException
	 */
	public DAO getDao(Class klass) throws IllegalDAOAccessException {
		return getDAO(klass);
	}
}
