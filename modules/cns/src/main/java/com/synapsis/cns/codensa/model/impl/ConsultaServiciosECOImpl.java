package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaServiciosECO;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;


public class ConsultaServiciosECOImpl extends SynergiaBusinessObjectImpl implements ConsultaServiciosECO{


	private static final long serialVersionUID = 1L;
	
	public String tipoServicioFinanciero;
	  public Long nroServicioFinanciero;
	  public String nombreTitular;
	  public String nroIdentificacionSocio;
	  public String tipoIdentificacionTitular;
	  public String numeroIdentificacionTitular;
	  public String lineaNegocio;
	  public Long numeroECO;
	  public String numeroIdECO;
	  public String socio;
	  public String producto;
	  public String plan;
	  public Long numeroCuotas;
	  public Long valorCuotas;
	  public String indicadorSaldoAnterior;
	  public Date fechaActivacion;
	  public Long numeroDeCuotasFacturadas;
	  public String numeroDeCuotasFaltantes;
	  public Long saldoECO;
	  public Long saldoInteresMora;
	  public Long periodosAtrasados;
	  public Long diasMora;	 
	  public Long valorCuotasMora;
	  
	public Long getDiasMora() {
		return this.diasMora;
	}
	public void setDiasMora(Long diasMora) {
		this.diasMora = diasMora;
	}

	public String getIndicadorSaldoAnterior() {
		return indicadorSaldoAnterior;
	}
	public void setIndicadorSaldoAnterior(String indicadorSaldoAnterior) {
		this.indicadorSaldoAnterior = indicadorSaldoAnterior;
	} 
	public String getLineaNegocio() {
		return lineaNegocio;
	}
	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}
	public String getNombreTitular() {
		return nombreTitular;
	}
	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}
	public String getNroIdentificacionSocio() {
		return nroIdentificacionSocio;
	}
	public void setNroIdentificacionSocio(String nroIdentificacionSocio) {
		this.nroIdentificacionSocio = nroIdentificacionSocio;
	}

	public String getNumeroDeCuotasFaltantes() {
		return numeroDeCuotasFaltantes;
	}
	public void setNumeroDeCuotasFaltantes(String numeroDeCuotasFaltantes) {
		this.numeroDeCuotasFaltantes = numeroDeCuotasFaltantes;
	}
	public Long getNumeroECO() {
		return numeroECO;
	}
	public void setNumeroECO(Long numeroECO) {
		this.numeroECO = numeroECO;
	}
	public String getNumeroIdECO() {
		return numeroIdECO;
	}
	public void setNumeroIdECO(String numeroIdECO) {
		this.numeroIdECO = numeroIdECO;
	}
	public String getNumeroIdentificacionTitular() {
		return numeroIdentificacionTitular;
	}
	public void setNumeroIdentificacionTitular(String numeroIdentificacionTitular) {
		this.numeroIdentificacionTitular = numeroIdentificacionTitular;
	}
	public Long getNroServicioFinanciero() {
		return nroServicioFinanciero;
	}
	public void setNroServicioFinanciero(Long nroServicioFinanciero) {
		this.nroServicioFinanciero = nroServicioFinanciero;
	}
	public Long getPeriodosAtrasados() {
		return periodosAtrasados;
	}
	public void setPeriodosAtrasados(Long periodosAtrasados) {
		this.periodosAtrasados = periodosAtrasados;
	}
	public String getPlan() {
		return plan;
	}
	public void setPlan(String plan) {
		this.plan = plan;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public Long getSaldoECO() {
		return saldoECO;
	}
	public void setSaldoECO(Long saldoECO) {
		this.saldoECO = saldoECO;
	}
	public Long getSaldoInteresMora() {
		return saldoInteresMora;
	}
	public void setSaldoInteresMora(Long saldoInteresMora) {
		this.saldoInteresMora = saldoInteresMora;
	}
	public String getSocio() {
		return socio;
	}
	public void setSocio(String socio) {
		this.socio = socio;
	}
	public String getTipoIdentificacionTitular() {
		return tipoIdentificacionTitular;
	}
	public void setTipoIdentificacionTitular(String tipoIdentificacionTitular) {
		this.tipoIdentificacionTitular = tipoIdentificacionTitular;
	}
	public String getTipoServicioFinanciero() {
		return tipoServicioFinanciero;
	}
	public void setTipoServicioFinanciero(String tipoServicioFinanciero) {
		this.tipoServicioFinanciero = tipoServicioFinanciero;
	}
	public Long getValorCuotas() {
		return valorCuotas;
	}
	public void setValorCuotas(Long valorCuotas) {
		this.valorCuotas = valorCuotas;
	}
	public Long getValorCuotasMora() {
		return valorCuotasMora;
	}
	public void setValorCuotasMora(Long valorCuotasMora) {
		this.valorCuotasMora = valorCuotasMora;
	}
	public Long getNumeroCuotas() {
		return numeroCuotas;
	}
	public void setNumeroCuotas(Long numeroCuotas) {
		this.numeroCuotas = numeroCuotas;
	}
	public Long getNumeroDeCuotasFacturadas() {
		return numeroDeCuotasFacturadas;
	}
	public void setNumeroDeCuotasFacturadas(Long numeroDeCuotasFacturadas) {
		this.numeroDeCuotasFacturadas = numeroDeCuotasFacturadas;
	}
	public Date getFechaActivacion() {
		return fechaActivacion;
	}
	public void setFechaActivacion(Date fechaActivacion) {
		this.fechaActivacion = fechaActivacion;
	}

	  
}
