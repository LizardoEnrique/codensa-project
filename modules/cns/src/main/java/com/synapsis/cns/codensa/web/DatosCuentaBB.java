package com.synapsis.cns.codensa.web;

import com.suivant.arquitectura.core.model.PersistentObject;
import com.synapsis.cns.codensa.model.DatosBeneficiario;
import com.synapsis.cns.codensa.model.DatosTitular;
import com.synapsis.components.behaviour.Notifier;
import com.synapsis.synergia.presentation.model.NasServiceHelper;

public class DatosCuentaBB {

	private DatosBeneficiario bobBeneficiario;
	private DatosTitular bobTitular;


	/**
	 * @return the bobTitular
	 */
	public DatosTitular getBobTitular() {
		return bobTitular;
	}

	/**
	 * @param bobTitular the bobTitular to set
	 */
	public void setBobTitular(DatosTitular bobTitular) {
		this.bobTitular = bobTitular;
	}
	/**
	 * @return the bobBeneficiario
	 */
	public DatosBeneficiario getBobBeneficiario() {
		return bobBeneficiario;
	}

	/**
	 * @param bobBeneficiario the bobBeneficiario to set
	 */
	public void setBobBeneficiario(DatosBeneficiario bobBeneficiario) {
		this.bobBeneficiario = bobBeneficiario;
	}

	// **************************************************************
	// *edicion del telefono de la persona de numero de cuenta
	// ************************************************************

	protected static String OUTCOME_SUCCESS = "success";

	/**
	 * Objeto Persona
	 */
	private PersistentObject persona;
	private PersistentObject beneficiario;


	// ******************************************
	// acciones del BB
	// ***********************************


	/**
	 * Action del BB ejecuta el servicio de save or update
	 * 
	 * @return
	 */
	public void saveOrUpdate() {
		NasServiceHelper.invoke("personaServiceCRUD", "saveOrUpdate",
				new Object[] { getPersona() });
		Notifier.addMessage("com.synapsis.nuc.persona.telefono.guardado.con.exito");
	}

	public void saveOrUpdateBeneficiario() {
		NasServiceHelper.invoke("beneficiarioServiceCRUD", "saveOrUpdate",
				new Object[] { getBeneficiario() });
		Notifier.addMessage("com.synapsis.nuc.persona.telefono.guardado.con.exito");
	}
	

	// ************************************************
	// getters and setters para la edicion de cuadrilla
	// ************************************************


	/**
	 * @return the persona
	 */
	public PersistentObject getPersona() {
		return persona;
	}

	/**
	 * @param persona
	 *            the persona to set
	 */
	public void setPersona(PersistentObject persona) {
		this.persona = persona;
	}

	/**
	 * @return the beneficiario
	 */
	public PersistentObject getBeneficiario() {
		return beneficiario;
	}

	/**
	 * @param beneficiario the beneficiario to set
	 */
	public void setBeneficiario(PersistentObject beneficiario) {
		this.beneficiario = beneficiario;
	}

	

}
