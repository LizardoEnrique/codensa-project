package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.Empresa;
import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaModificacionesClienteRed extends SynergiaBusinessObject {

	public Long getId();
	public void setId(Long id);

	public Empresa getEmpresa();
	public void setEmpresa(Empresa empresa);

	public String getAccion();
	public void setAccion(String accion);
	
	public String getCircuito();
	public void setCircuito(String circuito);
	
	public String getEstadoTransformador();
	public void setEstadoTransformador(String estadoTransformador);
	
	public Boolean getIndicadorAlumbradoPublico();	
	public void setIndicadorAlumbradoPublico(Boolean indicadorAlumbradoPublico);
	
	public String getNivelTension();
	public void setNivelTension(String nivelTension);
	
	public Long getNroSrvElectrico();
	public void setNroSrvElectrico(Long nroSrvElectrico);

	public String getNroTotalSrvTransformador();
	public void setNroTotalSrvTransformador(String nroTotalSrvTransformador);
	
	public Long getNroTransformador();
	public void setNroTransformador(Long nroTransformador);
	
	public String getPropiedad();
	public void setPropiedad(String propiedad);
	
	public String getTipoConexion();
	public void setTipoConexion(String tipoConexion);
	
	public String getTipoModificacion();
	public void setTipoModificacion(String tipoModificacion);
	
	public String getTipoRed();
	public void setTipoRed(String tipoRed);
	
	public String getUsuarioRealizoMod();
	public void setUsuarioRealizoMod(String usuarioRealizoMod);

	public String getPeriodoDesde();
	public void setPeriodoDesde(String periodoDesde);
	
	public String getPeriodoHasta();
	public void setPeriodoHasta(String periodoHasta);

	public Long getCantSrvPeriodo();
	public void setCantSrvPeriodo(Long cantSrvPeriodo);
	
	public Date getPeriodoFact();
	public void setPeriodoFact(Date periodoFact);

}
