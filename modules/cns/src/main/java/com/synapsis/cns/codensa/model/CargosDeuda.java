package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


public interface CargosDeuda extends SynergiaBusinessObject{

	public Long getCodigoCargo();
	public void setCodigoCargo(Long codigoCargo);
	public Date getFechaCreacion();
	public void setFechaCreacion(Date fechaCreacion);
	public String getNombreCargo();
	public void setNombreCargo(String nombreCargo);
	public String getNombreUsuarioGenerador();
	public void setNombreUsuarioGenerador(String nombreUsuarioGenerador);
	public Long getNumeroDocumentoAsociado();
	public void setNumeroDocumentoAsociado(Long numeroDocumentoAsociado);
	public Long getNumeroUnidades();
	public void setNumeroUnidades(Long numeroUnidades);
	public Long getUnidad();
	public void setUnidad(Long unidad);
	public String getUsuarioGenerador();
	public void setUsuarioGenerador(String usuarioGenerador);
	public Long getValor();
	public void setValor(Long valor);	

}