package com.synapsis.cns.codensa.web;

/**
 * @author ccamba
 *
 */
public class CUOrigenImpresionDuplicadoFacturaBB extends
		GeneracionAutomaticaContactoKendariBB {

	protected String getCasoUsoOrigen() {
		return this.getParamEncryptedAndEncoded("CNS003"); 
	}

}
