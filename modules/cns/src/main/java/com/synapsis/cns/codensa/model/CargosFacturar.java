package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface CargosFacturar extends SynergiaBusinessObject {
	
	
   	
   	
   	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);
	
	/**
	 * @return the codigoCargo
	 */
	public String getCodigoCargo();

	/**
	 * @param codigoCargo the codigoCargo to set
	 */
	public void setCodigoCargo(String codigoCargo);

	/**
	 * @return the descripcionCargo
	 */
	public String getDescripcionCargo();

	/**
	 * @param descripcionCargo the descripcionCargo to set
	 */
	public void setDescripcionCargo(String descripcionCargo);

	/**
	 * @return the fechaIngresoCargo
	 */
	public Date getFechaIngresoCargo();

	/**
	 * @param fechaIngresoCargo the fechaIngresoCargo to set
	 */
	public void setFechaIngresoCargo(Date fechaIngresoCargo);

	/**
	 * @return the nombreIngresoCargo
	 */
	public String getNombreIngresoCargo();

	/**
	 * @param nombreIngresoCargo the nombreIngresoCargo to set
	 */
	public void setNombreIngresoCargo(String nombreIngresoCargo);

	/**
	 * @return the nroServicioCargo
	 */
	public String getNroServicioCargo();

	/**
	 * @param nroServicioCargo the nroServicioCargo to set
	 */
	public void setNroServicioCargo(String nroServicioCargo);

	/**
	 * @return the observaciones
	 */
	public String getObservaciones();

	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones);

	/**
	 * @return the tipoServicioCargo
	 */
	public String getTipoServicioCargo();

	/**
	 * @param tipoServicioCargo the tipoServicioCargo to set
	 */
	public void setTipoServicioCargo(String tipoServicioCargo);

	/**
	 * @return the usuarioIngresoCargo
	 */
	public String getUsuarioIngresoCargo();

	/**
	 * @param usuarioIngresoCargo the usuarioIngresoCargo to set
	 */
	public void setUsuarioIngresoCargo(String usuarioIngresoCargo);

	/**
	 * @return the valorCargo
	 */
	public Long getValorCargo();

	/**
	 * @param valorCargo the valorCargo to set
	 */
	public void setValorCargo(Long valorCargo);

}