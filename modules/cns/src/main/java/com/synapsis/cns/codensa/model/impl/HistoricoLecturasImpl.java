package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.HistoricoLecturas;

public class HistoricoLecturasImpl extends SynergiaBusinessObjectImpl 
	implements HistoricoLecturas {
    // Fields    
	
    private Date fechaLectura;
	private Long lecturaActivaFP;
    private Long consumoActivaFP;
    private Long lecturaActivaHP;
    private Long consumoActivoHP;
    private Long lecturaActivaXP;
    private Long consumoActivoXP;
    
    private Long lecturaReactivaFP;
    private Long consumoReactivoFacturadoFP;
    private Long consumoReactivoCalculadoFP;    
    private Long lecturaReactivaHP;
    private Long consumoReactivoFacturadoHP;
    private Long consumoReactivoCalculadoHP;    
    private Long lecturaReactivaXP;
    private Long consumoReactivoFacturadoXP;
    private Long consumoReactivoCalculadoXP;    

    private Long idMedidor;

	public Long getConsumoActivaFP() {
		return consumoActivaFP;
	}

	public void setConsumoActivaFP(Long consumoActivaFP) {
		this.consumoActivaFP = consumoActivaFP;
	}

	public Long getConsumoActivoHP() {
		return consumoActivoHP;
	}

	public void setConsumoActivoHP(Long consumoActivoHP) {
		this.consumoActivoHP = consumoActivoHP;
	}

	public Long getConsumoActivoXP() {
		return consumoActivoXP;
	}

	public void setConsumoActivoXP(Long consumoActivoXP) {
		this.consumoActivoXP = consumoActivoXP;
	}

	public Long getConsumoReactivoCalculadoFP() {
		return consumoReactivoCalculadoFP;
	}

	public void setConsumoReactivoCalculadoFP(Long consumoReactivoCalculadoFP) {
		this.consumoReactivoCalculadoFP = consumoReactivoCalculadoFP;
	}

	public Long getConsumoReactivoCalculadoHP() {
		return consumoReactivoCalculadoHP;
	}

	public void setConsumoReactivoCalculadoHP(Long consumoReactivoCalculadoHP) {
		this.consumoReactivoCalculadoHP = consumoReactivoCalculadoHP;
	}

	public Long getConsumoReactivoCalculadoXP() {
		return consumoReactivoCalculadoXP;
	}

	public void setConsumoReactivoCalculadoXP(Long consumoReactivoCalculadoXP) {
		this.consumoReactivoCalculadoXP = consumoReactivoCalculadoXP;
	}

	public Long getConsumoReactivoFacturadoFP() {
		return consumoReactivoFacturadoFP;
	}

	public void setConsumoReactivoFacturadoFP(Long consumoReactivoFacturadoFP) {
		this.consumoReactivoFacturadoFP = consumoReactivoFacturadoFP;
	}

	public Long getConsumoReactivoFacturadoHP() {
		return consumoReactivoFacturadoHP;
	}

	public void setConsumoReactivoFacturadoHP(Long consumoReactivoFacturadoHP) {
		this.consumoReactivoFacturadoHP = consumoReactivoFacturadoHP;
	}

	public Long getConsumoReactivoFacturadoXP() {
		return consumoReactivoFacturadoXP;
	}

	public void setConsumoReactivoFacturadoXP(Long consumoReactivoFacturadoXP) {
		this.consumoReactivoFacturadoXP = consumoReactivoFacturadoXP;
	}

	public Date getFechaLectura() {
		return fechaLectura;
	}

	public void setFechaLectura(Date fechaLectura) {
		this.fechaLectura = fechaLectura;
	}

	public Long getIdMedidor() {
		return idMedidor;
	}

	public void setIdMedidor(Long idMedidor) {
		this.idMedidor = idMedidor;
	}

	public Long getLecturaActivaFP() {
		return lecturaActivaFP;
	}

	public void setLecturaActivaFP(Long lecturaActivaFP) {
		this.lecturaActivaFP = lecturaActivaFP;
	}

	public Long getLecturaActivaXP() {
		return lecturaActivaXP;
	}

	public void setLecturaActivaXP(Long lecturaActivaXP) {
		this.lecturaActivaXP = lecturaActivaXP;
	}

	public Long getLecturaReactivaFP() {
		return lecturaReactivaFP;
	}

	public void setLecturaReactivaFP(Long lecturaReactivaFP) {
		this.lecturaReactivaFP = lecturaReactivaFP;
	}

	public Long getLecturaReactivaHP() {
		return lecturaReactivaHP;
	}

	public void setLecturaReactivaHP(Long lecturaReactivaHP) {
		this.lecturaReactivaHP = lecturaReactivaHP;
	}

	public Long getLecturaReactivaXP() {
		return lecturaReactivaXP;
	}

	public void setLecturaReactivaXP(Long lecturaReactivaXP) {
		this.lecturaReactivaXP = lecturaReactivaXP;
	}

	public Long getLecturaActivaHP() {
		return lecturaActivaHP;
	}

	public void setLecturaActivaHP(Long lecturaActivaHP) {
		this.lecturaActivaHP = lecturaActivaHP;
	}

}
