package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface ConsultaDetalleServiciosFinancieros {

	/**
	 * @return the causalNegacion
	 */
	public String getCausalNegacion();

	/**
	 * @param causalNegacion the causalNegacion to set
	 */
	public void setCausalNegacion(String causalNegacion);

	/**
	 * @return the ciclo
	 */
	public String getCiclo();

	/**
	 * @param ciclo the ciclo to set
	 */
	public void setCiclo(String ciclo);

	/**
	 * @return the diasMora
	 */
	public Double getDiasMora();

	/**
	 * @param diasMora the diasMora to set
	 */
	public void setDiasMora(Double diasMora);

	/**
	 * @return the estrato
	 */
	public String getEstrato();

	/**
	 * @param estrato the estrato to set
	 */
	public void setEstrato(String estrato);

	/**
	 * @return the fechaCompraProd
	 */
	public Date getFechaCompraProd();

	/**
	 * @param fechaCompraProd the fechaCompraProd to set
	 */
	public void setFechaCompraProd(Date fechaCompraProd);

	/**
	 * @return the fechaSolicCred
	 */
	public Date getFechaSolicCred();

	/**
	 * @param fechaSolicCred the fechaSolicCred to set
	 */
	public void setFechaSolicCred(Date fechaSolicCred);

	/**
	 * @return the lineaNegocio
	 */
	public String getLineaNegocio();

	/**
	 * @param lineaNegocio the lineaNegocio to set
	 */
	public void setLineaNegocio(String lineaNegocio);

	/**
	 * @return the marcaProd
	 */
	public String getMarcaProd();

	/**
	 * @param marcaProd the marcaProd to set
	 */
	public void setMarcaProd(String marcaProd);

	/**
	 * @return the montoAprobado
	 */
	public Long getMontoAprobado();

	/**
	 * @param montoAprobado the montoAprobado to set
	 */
	public void setMontoAprobado(Long montoAprobado);

	/**
	 * @return the montoDisponible
	 */
	public Long getMontoDisponible();

	/**
	 * @param montoDisponible the montoDisponible to set
	 */
	public void setMontoDisponible(Long montoDisponible);

	/**
	 * @return the nombreSocio
	 */
	public String getNombreSocio();

	/**
	 * @param nombreSocio the nombreSocio to set
	 */
	public void setNombreSocio(String nombreSocio);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroCuotas
	 */
	public String getNroCuotas();

	/**
	 * @param nroCuotas the nroCuotas to set
	 */
	public void setNroCuotas(String nroCuotas);

	/**
	 * @return the nroCuotasFac
	 */
	public String getNroCuotasFac();

	/**
	 * @param nroCuotasFac the nroCuotasFac to set
	 */
	public void setNroCuotasFac(String nroCuotasFac);

	/**
	 * @return the nroCuotasRestantes
	 */
	public String getNroCuotasRestantes();

	/**
	 * @param nroCuotasRestantes the nroCuotasRestantes to set
	 */
	public void setNroCuotasRestantes(String nroCuotasRestantes);

	/**
	 * @return the nroIdSolicitante
	 */
	public String getNroIdSolicitante();

	/**
	 * @param nroIdSolicitante the nroIdSolicitante to set
	 */
	public void setNroIdSolicitante(String nroIdSolicitante);

	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio();

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio);

	/**
	 * @return the periodAtrasado
	 */
	public Double getPeriodAtrasado();

	/**
	 * @param periodAtrasado the periodAtrasado to set
	 */
	public void setPeriodAtrasado(Double periodAtrasado);

	/**
	 * @return the resultEstCred
	 */
	public String getResultEstCred();

	/**
	 * @param resultEstCred the resultEstCred to set
	 */
	public void setResultEstCred(String resultEstCred);

	/**
	 * @return the saldoCapital
	 */
	public Double getSaldoCapital();

	/**
	 * @param saldoCapital the saldoCapital to set
	 */
	public void setSaldoCapital(Double saldoCapital);

	/**
	 * @return the saldoIntereses
	 */
	public Double getSaldoIntereses();

	/**
	 * @param saldoIntereses the saldoIntereses to set
	 */
	public void setSaldoIntereses(Double saldoIntereses);

	/**
	 * @return the saldoIntMora
	 */
	public Double getSaldoIntMora();

	/**
	 * @param saldoIntMora the saldoIntMora to set
	 */
	public void setSaldoIntMora(Double saldoIntMora);

	/**
	 * @return the solicitanteCred
	 */
	public String getSolicitanteCred();

	/**
	 * @param solicitanteCred the solicitanteCred to set
	 */
	public void setSolicitanteCred(String solicitanteCred);

	/**
	 * @return the sucursalSocio
	 */
	public String getSucursalSocio();

	/**
	 * @param sucursalSocio the sucursalSocio to set
	 */
	public void setSucursalSocio(String sucursalSocio);

	/**
	 * @return the tasaInt
	 */
	public Double getTasaInt();

	/**
	 * @param tasaInt the tasaInt to set
	 */
	public void setTasaInt(Double tasaInt);

	/**
	 * @return the tasaIntActual
	 */
	public Double getTasaIntActual();

	/**
	 * @param tasaIntActual the tasaIntActual to set
	 */
	public void setTasaIntActual(Double tasaIntActual);

	/**
	 * @return the tipoProd
	 */
	public String getTipoProd();

	/**
	 * @param tipoProd the tipoProd to set
	 */
	public void setTipoProd(String tipoProd);

	/**
	 * @return the valorCap
	 */
	public Double getValorCap();

	/**
	 * @param valorCap the valorCap to set
	 */
	public void setValorCap(Double valorCap);

	/**
	 * @return the valorInt
	 */
	public Double getValorInt();

	/**
	 * @param valorInt the valorInt to set
	 */
	public void setValorInt(Double valorInt);

	/**
	 * @return the valorOrigSrv
	 */
	public Double getValorOrigSrv();

	/**
	 * @param valorOrigSrv the valorOrigSrv to set
	 */
	public void setValorOrigSrv(Double valorOrigSrv);

}