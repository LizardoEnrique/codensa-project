package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface ConsultaECO {

	/**
	 * @return the diasMora
	 */
	public Long getDiasMora();

	/**
	 * @param diasMora the diasMora to set
	 */
	public void setDiasMora(Long diasMora);

	/**
	 * @return the fechaActivacion
	 */
	public Date getFechaActivacion();

	/**
	 * @param fechaActivacion the fechaActivacion to set
	 */
	public void setFechaActivacion(Date fechaActivacion);

	/**
	 * @return the indicadSaldoAnt
	 */
	public String getIndicadSaldoAnt();

	/**
	 * @param indicadSaldoAnt the indicadSaldoAnt to set
	 */
	public void setIndicadSaldoAnt(String indicadSaldoAnt);

	/**
	 * @return the lineaNegocio
	 */
	public String getLineaNegocio();

	/**
	 * @param lineaNegocio the lineaNegocio to set
	 */
	public void setLineaNegocio(String lineaNegocio);

	/**
	 * @return the nombreTit
	 */
	public String getNombreTit();

	/**
	 * @param nombreTit the nombreTit to set
	 */
	public void setNombreTit(String nombreTit);

	/**
	 * @return the nombreTitular
	 */
	public String getNombreTitular();

	/**
	 * @param nombreTitular the nombreTitular to set
	 */
	public void setNombreTitular(String nombreTitular);

	/**
	 * @return the nroCuotas
	 */
	public String getNroCuotas();

	/**
	 * @param nroCuotas the nroCuotas to set
	 */
	public void setNroCuotas(String nroCuotas);

	/**
	 * @return the nroCuotasFact
	 */
	public String getNroCuotasFact();

	/**
	 * @param nroCuotasFact the nroCuotasFact to set
	 */
	public void setNroCuotasFact(String nroCuotasFact);

	/**
	 * @return the nroCuotasFaltantes
	 */
	public String getNroCuotasFaltantes();

	/**
	 * @param nroCuotasFaltantes the nroCuotasFaltantes to set
	 */
	public void setNroCuotasFaltantes(String nroCuotasFaltantes);

	/**
	 * @return the nroEco
	 */
	public Long getNroEco();

	/**
	 * @param nroEco the nroEco to set
	 */
	public void setNroEco(Long nroEco);

	/**
	 * @return the nroIdEco
	 */
	public Long getNroIdEco();

	/**
	 * @param nroIdEco the nroIdEco to set
	 */
	public void setNroIdEco(Long nroIdEco);

	/**
	 * @return the nroIdentSocio
	 */
	public Long getNroIdentSocio();

	/**
	 * @param nroIdentSocio the nroIdentSocio to set
	 */
	public void setNroIdentSocio(Long nroIdentSocio);

	/**
	 * @return the nroIdTitular
	 */
	public String getNroIdTitular();

	/**
	 * @param nroIdTitular the nroIdTitular to set
	 */
	public void setNroIdTitular(String nroIdTitular);

	/**
	 * @return the nroSrvFin
	 */
	public Long getNroSrvFin();

	/**
	 * @param nroSrvFin the nroSrvFin to set
	 */
	public void setNroSrvFin(Long nroSrvFin);

	/**
	 * @return the periodAtrasados
	 */
	public Long getPeriodAtrasados();

	/**
	 * @param periodAtrasados the periodAtrasados to set
	 */
	public void setPeriodAtrasados(Long periodAtrasados);

	/**
	 * @return the plan
	 */
	public String getPlan();

	/**
	 * @param plan the plan to set
	 */
	public void setPlan(String plan);

	/**
	 * @return the producto
	 */
	public String getProducto();

	/**
	 * @param producto the producto to set
	 */
	public void setProducto(String producto);

	/**
	 * @return the saldoEco
	 */
	public String getSaldoEco();

	/**
	 * @param saldoEco the saldoEco to set
	 */
	public void setSaldoEco(String saldoEco);

	/**
	 * @return the saldoIntMora
	 */
	public String getSaldoIntMora();

	/**
	 * @param saldoIntMora the saldoIntMora to set
	 */
	public void setSaldoIntMora(String saldoIntMora);

	/**
	 * @return the socio
	 */
	public String getSocio();

	/**
	 * @param socio the socio to set
	 */
	public void setSocio(String socio);

	/**
	 * @return the tipoIdTitular
	 */
	public String getTipoIdTitular();

	/**
	 * @param tipoIdTitular the tipoIdTitular to set
	 */
	public void setTipoIdTitular(String tipoIdTitular);

	/**
	 * @return the tipoSrvFin
	 */
	public String getTipoSrvFin();

	/**
	 * @param tipoSrvFin the tipoSrvFin to set
	 */
	public void setTipoSrvFin(String tipoSrvFin);

	/**
	 * @return the valCuota
	 */
	public String getValCuota();

	/**
	 * @param valCuota the valCuota to set
	 */
	public void setValCuota(String valCuota);

	/**
	 * @return the valCuotasMora
	 */
	public String getValCuotasMora();

	/**
	 * @param valCuotasMora the valCuotasMora to set
	 */
	public void setValCuotasMora(String valCuotasMora);
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);


}