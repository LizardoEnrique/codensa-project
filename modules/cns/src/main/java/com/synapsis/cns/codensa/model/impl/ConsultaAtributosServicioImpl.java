/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaAtributosServicio;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author AR18799631
 *
 */
public class ConsultaAtributosServicioImpl extends SynergiaBusinessObjectImpl implements ConsultaAtributosServicio {

	private Long nroCuenta;
	private Long nroServicio;
	private String clienteFiador;
	private String clienteAfianzado;
	private String tip;
	private String cobranzaJudicial;
	private String demandaJudicial;
	private String debitoAutBancos;
	private String tieneKit;
	public String getClienteAfianzado() {
		return clienteAfianzado;
	}
	public void setClienteAfianzado(String clienteAfianzado) {
		this.clienteAfianzado = clienteAfianzado;
	}
	public String getClienteFiador() {
		return clienteFiador;
	}
	public void setClienteFiador(String clienteFiador) {
		this.clienteFiador = clienteFiador;
	}
	public String getCobranzaJudicial() {
		return cobranzaJudicial;
	}
	public void setCobranzaJudicial(String cobranzaJudicial) {
		this.cobranzaJudicial = cobranzaJudicial;
	}
	public String getDebitoAutBancos() {
		return debitoAutBancos;
	}
	public void setDebitoAutBancos(String debitoAutBancos) {
		this.debitoAutBancos = debitoAutBancos;
	}
	public String getDemandaJudicial() {
		return demandaJudicial;
	}
	public void setDemandaJudicial(String demandaJudicial) {
		this.demandaJudicial = demandaJudicial;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public Long getNroServicio() {
		return nroServicio;
	}
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	public String getTieneKit() {
		return tieneKit;
	}
	public void setTieneKit(String tieneKit) {
		this.tieneKit = tieneKit;
	}
	public String getTip() {
		return tip;
	}
	public void setTip(String tip) {
		this.tip = tip;
	}
}
