package com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada;

import com.synapsis.cns.codensa.model.buscaCuenta.avanzada.BuscaCuentaAvanzadaTelefonoBeneficiario;
import com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaImpl;

public class BuscaCuentaAvanzadaTelefonoBeneficiarioImpl extends
		BuscaCuentaImpl implements BuscaCuentaAvanzadaTelefonoBeneficiario {
	
	private String telefonoBeneficiario;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada.BuscaCuentaAvanzadaTelefonoBeneficiario#getTelefonoBeneficiario()
	 */
	public String getTelefonoBeneficiario() {
		return telefonoBeneficiario;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada.BuscaCuentaAvanzadaTelefonoBeneficiario#setTelefonoBeneficiario(java.lang.String)
	 */
	public void setTelefonoBeneficiario(String telefonoBeneficiario) {
		this.telefonoBeneficiario = telefonoBeneficiario;
	}

}
