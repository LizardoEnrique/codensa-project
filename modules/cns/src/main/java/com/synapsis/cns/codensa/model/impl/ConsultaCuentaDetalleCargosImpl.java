/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaCuentaDetalleCargos;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 *
 */
public class ConsultaCuentaDetalleCargosImpl extends SynergiaBusinessObjectImpl implements ConsultaCuentaDetalleCargos {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idDetalle;
	private String codigoCargo;
	private String cargo;
	private Long valorCargo;
	private Long consumo;
	private String tipoCargo;
	private String area;
	private String unidadCobro;
	private Long idConfigFact;
	private Date fechaProceso;
	
	private Long nroCuenta;
	private Date vigenciaTarifa;
	
		
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargos#getArea()
	 */
	public String getArea() {
		return area;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargos#setArea(java.lang.String)
	 */
	public void setArea(String area) {
		this.area = area;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargos#getCargo()
	 */
	public String getCargo() {
		return cargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargos#setCargo(java.lang.String)
	 */
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargos#getCodigoCargo()
	 */
	public String getCodigoCargo() {
		return codigoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargos#setCodigoCargo(java.lang.String)
	 */
	public void setCodigoCargo(String codigoCargo) {
		this.codigoCargo = codigoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargos#getConsumo()
	 */
	public Long getConsumo() {
		return consumo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargos#setConsumo(java.lang.Long)
	 */
	public void setConsumo(Long consumo) {
		this.consumo = consumo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargos#getFechaProceso()
	 */
	public Date getFechaProceso() {
		return fechaProceso;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargos#setFechaProceso(java.util.Date)
	 */
	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargos#getIdConfigFactura()
	 */
	public Long getIdConfigFact() {
		return idConfigFact;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargos#setIdConfigFactura(java.lang.Long)
	 */
	public void setIdConfigFact(Long idConfigFact) {
		this.idConfigFact = idConfigFact;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargos#getIdDetalle()
	 */
	public Long getIdDetalle() {
		return idDetalle;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargos#setIdDetalle(java.lang.Long)
	 */
	public void setIdDetalle(Long idDetalle) {
		this.idDetalle = idDetalle;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargos#getTipoCargo()
	 */
	public String getTipoCargo() {
		return tipoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargos#setTipoCargo(java.lang.String)
	 */
	public void setTipoCargo(String tipoCargo) {
		this.tipoCargo = tipoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargos#getUnidadCobro()
	 */
	public String getUnidadCobro() {
		return unidadCobro;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargos#setUnidadCobro(java.lang.String)
	 */
	public void setUnidadCobro(String unidadCobro) {
		this.unidadCobro = unidadCobro;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargos#getValorCargo()
	 */
	public Long getValorCargo() {
		return valorCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDetalleCargos#setValorCargo(java.lang.Long)
	 */
	public void setValorCargo(Long valorCargo) {
		this.valorCargo = valorCargo;
	}
	/**
	 * @return the vigenciaTarifa
	 */
	public Date getVigenciaTarifa() {
		return vigenciaTarifa;
	}
	/**
	 * @param vigenciaTarifa the vigenciaTarifa to set
	 */
	public void setVigenciaTarifa(Date vigenciaTarifa) {
		this.vigenciaTarifa = vigenciaTarifa;
	}
		
/*	
	
	ID_DETALLE_CTA_CAR	NUMBER	
	COD_CARGO			VARCHAR2(6 Bytes)	
	CARGO				VARCHAR2(50 Bytes)	
	VALOR_CARGO			NUMBER(19,4)	
	CONSUMO				NUMBER(16,4)	
	TIPO_CARGO			VARCHAR2(4 Bytes)	
	AREA				VARCHAR2(100 Bytes)	
	UNIDAD_COBRO		VARCHAR2(100 Bytes)	
	ID_CONFIG_FACT		NUMBER(18,0)	
	ID_EMPRESA			NUMBER(18,0)	
	FECHA_PROCESO		DATE	
*/
}
