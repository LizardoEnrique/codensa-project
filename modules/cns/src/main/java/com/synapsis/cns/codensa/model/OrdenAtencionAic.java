package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


/**
 * @author m3.MarioRoss - 14/11/2006
 */
public interface OrdenAtencionAic extends SynergiaBusinessObject {
	public Long getNroOrden();
	public void setNroOrden(Long nroOrden);

	public Long getIdAtencion();
	public void setIdAtencion(Long idAtencion);

	public String getTipo();
	public void setTipo(String tipo);

	public String getEstado();
	public void setEstado(String estado);

	public Date getFechaIngreso();
	public void setFechaIngreso(Date fechaIngreso);
}