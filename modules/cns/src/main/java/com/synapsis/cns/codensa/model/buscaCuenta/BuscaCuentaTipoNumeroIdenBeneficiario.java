package com.synapsis.cns.codensa.model.buscaCuenta;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface BuscaCuentaTipoNumeroIdenBeneficiario extends SynergiaBusinessObject{

	/**
	 * @return the numero
	 */
	public String getNumero();

	/**
	 * @return the tipo
	 */
	public String getTipo();

	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero);

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo);
	
	public Long getIdTipo();
	
	public void setIdTipo(Long idTipo);

}