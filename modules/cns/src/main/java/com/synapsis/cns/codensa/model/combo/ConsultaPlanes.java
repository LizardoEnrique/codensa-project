package com.synapsis.cns.codensa.model.combo;

public interface ConsultaPlanes {

	/**
	 * @return the descripcion
	 */
	public String getDescripcion();

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion);
	
	public Long getIdPlan();

	public void setIdPlan(Long idPlan);


}