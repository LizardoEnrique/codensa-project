package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.ContactoAic;

public class ContactoAicImpl extends SynergiaBusinessObjectImpl
		implements ContactoAic {
		
	private Long nroCuenta;
	
	private Long idContactoDist;

	private String canalComunicacionContacto;
	
	private Long idCuenta;

	private Date fechaHoraCreacion;

	private String grupoUsuarioGenerador;

	private String nombreGeneradorContacto;

	private Long numeroContacto;

	private Long numeroRadicacion;

	private String observacionesContacto;

	private String origenContacto;

	private String tipoRadicacion;

	private String usuarioGeneradorContacto;

	private String numeroAtencion;
	
	private String estadoAtencion;
	
	private Long idContacto;
	
	public String getEstadoAtencion() {
		return estadoAtencion;
	}

	public void setEstadoAtencion(String estadoAtencion) {
		this.estadoAtencion = estadoAtencion;
	}

	public String getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setNumeroAtencion(String numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	
	public String getCanalComunicacionContacto() {
		return canalComunicacionContacto;
	}

	public Date getFechaHoraCreacion() {
		return fechaHoraCreacion;
	}

	public String getGrupoUsuarioGenerador() {
		return grupoUsuarioGenerador;
	}
	
	public Long getIdContactoDist() {
		return idContactoDist;
	}
	
	public Long getIdCuenta() {
		return idCuenta;
	}

	public String getNombreGeneradorContacto() {
		return nombreGeneradorContacto;
	}

	public Long getNumeroContacto() {
		return numeroContacto;
	}

	public Long getNumeroRadicacion() {
		return numeroRadicacion;
	}

	public String getObservacionesContacto() {
		return observacionesContacto;
	}

	public String getOrigenContacto() {
		return origenContacto;
	}

	public String getTipoRadicacion() {
		return tipoRadicacion;
	}

	public String getUsuarioGeneradorContacto() {
		return usuarioGeneradorContacto;
	}
	
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	
	public void setIdContactoDist(Long idContactoDist) {
		this.idContactoDist = idContactoDist;
	}

	public void setCanalComunicacionContacto(String canalComunicacion) {
		this.canalComunicacionContacto = canalComunicacion;
	}

	public void setFechaHoraCreacion(Date fechaHoraCreacion) {
		this.fechaHoraCreacion = fechaHoraCreacion;
	}

	public void setGrupoUsuarioGenerador(String grupoUsuarioGenerador) {
		this.grupoUsuarioGenerador = grupoUsuarioGenerador;
	}

	public void setNombreGeneradorContacto(String nombreGeneradorContacto) {
		this.nombreGeneradorContacto = nombreGeneradorContacto;
	}

	public void setNumeroContacto(Long numeroContacto) {
		this.numeroContacto = numeroContacto;
	}

	public void setNumeroRadicacion(Long numeroRadicacion) {
		this.numeroRadicacion = numeroRadicacion;
	}

	public void setObservacionesContacto(String observacionesContacto) {
		this.observacionesContacto = observacionesContacto;
	}

	public void setOrigenContacto(String origenContacto) {
		this.origenContacto = origenContacto;
	}

	public void setTipoRadicacion(String tipoRadicacion) {
		this.tipoRadicacion = tipoRadicacion;
	}

	public void setUsuarioGeneradorContacto(String usuarioGeneradorContacto) {
		this.usuarioGeneradorContacto = usuarioGeneradorContacto;
	}

	/**
	 * @return the idContacto
	 */
	public Long getIdContacto() {
		return idContacto;
	}

	/**
	 * @param idContacto the idContacto to set
	 */
	public void setIdContacto(Long idContacto) {
		this.idContacto = idContacto;
	}
}