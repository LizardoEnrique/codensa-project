package com.synapsis.cns.codensa.model.buscaCuenta;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface BuscaCuenta extends SynergiaBusinessObject {
	
	/**
	 * @return the cantidadSrvAsoc
	 */
	public Long getCantidadSrvAsoc();

	/**
	 * @return the direccionTitular
	 */
	public String getDireccionTitular();

	/**
	 * @return the estado
	 */
	public String getEstado();

	/**
	 * @return the localizacion
	 */
	public String getLocalizacion();

	/**
	 * @return the manzana
	 */
	public String getManzana();

	/**
	 * @return the municipio
	 */
	public String getMunicipio();

	/**
	 * @return the nombreBeneficiarioSrvElectrico
	 */
	public String getNombreBeneficiarioSrvElectrico();

	/**
	 * @return the nombreTitular
	 */
	public String getNombreTitular();

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @return the rutaLectura
	 */
	public String getRutaLectura();

	/**
	 * @param cantidadSrvAsoc the cantidadSrvAsoc to set
	 */
	public void setCantidadSrvAsoc(Long cantidadSrvAsoc);

	/**
	 * @param direccionTitular the direccionTitular to set
	 */
	public void setDireccionTitular(String direccionTitular);

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado);

	/**
	 * @param localizacion the localizacion to set
	 */
	public void setLocalizacion(String localizacion);

	/**
	 * @param manzana the manzana to set
	 */
	public void setManzana(String manzana);

	/**
	 * @param municipio the municipio to set
	 */
	public void setMunicipio(String municipio);

	/**
	 * @param nombreBeneficiarioSrvElectrico the nombreBeneficiarioSrvElectrico to set
	 */
	public void setNombreBeneficiarioSrvElectrico(
			String nombreBeneficiarioSrvElectrico);

	/**
	 * @param nombreTitular the nombreTitular to set
	 */
	public void setNombreTitular(String nombreTitular);

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @param rutaLectura the rutaLectura to set
	 */
	public void setRutaLectura(String rutaLectura);
	
	public String getApellidoBeneficiarioSrvElectrico();
	
	public void setApellidoBeneficiarioSrvElectrico(String apellidoBeneficiarioSrvElectrico) ;
	
	/**
	 * @return the correlativoConjunto
	 */
	public String getCorrelativoConjunto();
	/**
	 * @param correlativoConjunto the correlativoConjunto to set
	 */
	public void setCorrelativoConjunto(String correlativoConjunto);


}