package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaExpedienteValorCobro extends SynergiaBusinessObject {

	public Long getNumeroExpediente();
	public void setNumeroExpediente(Long numeroExpediente);
	
	/**
	 * @return the codigo
	 */
	public abstract String getCodigo();

	/**
	 * @param codigo the codigo to set
	 */
	public abstract void setCodigo(String codigo);

	/**
	 * @return the descripcion
	 */
	public abstract String getDescripcion();

	/**
	 * @param descripcion the descripcion to set
	 */
	public abstract void setDescripcion(String descripcion);

	/**
	 * @return the valor
	 */
	public abstract Double getValor();

	/**
	 * @param valor the valor to set
	 */
	public abstract void setValor(Double valor);

}