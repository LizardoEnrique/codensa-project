package com.synapsis.cns.codensa.web;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.el.ValueBinding;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.myfaces.custom.service.UIService;

import com.suivant.arquitectura.core.model.impl.BusinessObjectImpl;
import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;
import com.suivant.arquitectura.core.security.model.User;
import com.suivant.arquitectura.presentation.model.ViewBB;
import com.synapsis.cns.codensa.model.ConsultaClientePreferencial;
import com.synapsis.cns.codensa.model.ConsultaMensajesUsuario;
import com.synapsis.cns.codensa.model.impl.ConsultaMensajesImpl;
import com.synapsis.cns.codensa.model.impl.TitularCuentaImpl;
import com.synapsis.cns.codensa.utils.AMETimeStampUtils;
import com.synapsis.nuc.codensa.model.AMETimeStamp;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;
import com.synapsis.synergia.core.security.integration.kendary.webapp.AMEAuthenticationDetailsSourceImpl;
import com.synapsis.synergia.core.security.integration.kendary.webapp.AMEWebAuthenticationDetails;
import com.synapsis.synergia.presentation.model.NasServiceHelper;

/**
 * Ejecuta una busqueda en la pagina principal (CNS 005)
 * 
 * @author ar30557486
 * 
 */
public class BuscaCuentaPaginaPrincipalBB extends ViewBB {
	private String jsExceptionAccountNotFound = "_JS_EXCEPTION_404";
	private String jsExceptionOldAccountNotFound = "_JS_EXCEPTION_OLD_404";
	private String aj4Error = "";
	private String mensaje1;
	private String mensaje2;
	private UIService svcMensajes;
	private ConsultaMensajesImpl mensajes;
	private String loginUsuario;
	private String msgUsuario;
	private String digitoValidador;
	private Long nroCuentaAnterior;
	private boolean clientePreferencialEnabled;
	private String tipoClientePreferencial;

	/**
	 * Sobrecargo el metodo para operar con el BO, si el mismo es nulo escribo en el aj4Error para luego
	 * disparar la excepcion del lado del cliente
	 */
	public String buscarAction() {
		this.setAj4Error("");
		this.setDigitoValidador("");
		super.buscarAction();
		BusinessObjectImpl bo = (BusinessObjectImpl) super.getBusinessObject();
		try {
			TitularCuentaImpl impl = (TitularCuentaImpl) bo;

			this.setDigitoValidador(impl.getDigitoVerificador());
			this.setNroCuentaAnterior(impl.getNroCuentaAnterior());

			this.findClientePreferencial(impl.getNroCuenta());

			/**
			 * Los Mensajes PARECE ser que no se usan, no existe la Vista en la DB. Lo comento...tiraba
			 * excepci�n.
			 */
			// this.getSvcMensajes().execute();
			// mensajes = (ConsultaMensajesImpl) this.getSvcMensajes()
			// .getLocalValue();
			// HtmlOutputLabel msg1 = (HtmlOutputLabel)
			// JSFUtils.getComponentFromTree("search:val1");
			// HtmlOutputLabel msg2 = (HtmlOutputLabel)
			// JSFUtils.getComponentFromTree("search:val2");
			// msg1.setValue(mensajes.getMensaje1());
			// msg2.setValue(mensajes.getMensaje2());
			/**
			 * Los Mensajes PARECE ser que no se usan, no existe la Vista en la DB. Lo comento...tiraba
			 * excepci�n.
			 */
			// mensaje1 = mensajes.getMensaje1();
			// mensaje2 = mensajes.getMensaje2();
			// this.getMensajesUsuario();
		}
		catch (NullPointerException e) {
			this.setAj4Error(this.getJsExceptionAccountNotFound());

			return getDynamicOutcome();
		}
		this.audit((TitularCuentaImpl) bo);

		return getDynamicOutcome();
	}

	public boolean isCuentaAnteriorEnabled() {
		return SynergiaApplicationContext.getCurrentImplementationCompany().equals(SynergiaApplicationContext.EEC);
	}

	public String buscarCuentaAnteriorAction() {
		Boolean exists = (Boolean) NasServiceHelper.getResponse("cuentaAnteriorService", "exists",
			new Object[] { this.getNroCuentaAnterior() });

		if (!exists.booleanValue()) {
			this.setAj4Error(this.getJsExceptionOldAccountNotFound());
			this.setDigitoValidador("");
		}
		else {
			this.setAj4Error("");
			TitularCuentaImpl titularCuenta = (TitularCuentaImpl) NasServiceHelper.getResponse("cuentaAnteriorService",
				"findByCriteriaUnique", new Object[] { this.getNroCuentaAnterior() });

			this.findClientePreferencial(titularCuenta.getNroCuenta());
			
			FacesContext context = FacesContext.getCurrentInstance();
			ValueBinding value = FacesContext
				.getCurrentInstance()
				.getApplication()
				.createValueBinding("#{numeroCuentaQueryFilter}");

			EqualQueryFilter filter = (EqualQueryFilter) value.getValue(context);
			filter.setAttributeValue(titularCuenta.getNroCuenta());
			this.setDigitoValidador(titularCuenta.getDigitoVerificador());
			this.audit(titularCuenta);
		}

		return getDynamicOutcome();
	}

	private void audit(TitularCuentaImpl titular) {
		// /////////////////////////////////////////////////////////////////////
		if (AMETimeStampUtils.getInstance().isUserAuditable()) {
			AMETimeStamp ame = AMETimeStampUtils.getInstance().makeNewInstance(this.getRemoteAdress(),
				titular.getNroCuenta(), titular.getEmpresa());
			AMETimeStampUtils.getInstance().putInSessionContext(ame);
		}
		else {
			AMETimeStampUtils.getInstance().clearSessionContext();
		}
		// /////////////////////////////////////////////////////////////////////
	}

	/**
	 * Devuelve la ip del usuario logueado
	 */
	private String getRemoteAdress() {
		AMEWebAuthenticationDetails request = (AMEWebAuthenticationDetails) new AMEAuthenticationDetailsSourceImpl()
			.buildDetails((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest());

		return request.getRemoteAddress();
	}

	/**
	 * 
	 * @author dBraccio
	 * @return
	 */
	public String getServerAdress() {
		String serverAddress = "error";
        try {
            InetAddress inetAddress;
            inetAddress = InetAddress.getLocalHost();
            serverAddress = inetAddress.toString();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        
        return serverAddress;
	}
	
	
	
	public void buscaMensajes() {
		this.getSvcMensajes().execute();
	}

	/**
	 * @param event
	 */
	public void cancelAction(ActionEvent event) {
		FacesContext context = FacesContext.getCurrentInstance();
		ValueBinding value = FacesContext
			.getCurrentInstance()
			.getApplication()
			.createValueBinding("#{numeroCuentaQueryFilter}");

		EqualQueryFilter filter = (EqualQueryFilter) value.getValue(context);
		filter.setAttributeValue(null);
		this.mensaje1 = "";
		this.mensaje2 = "";
		this.setDigitoValidador("");
		this.setNroCuentaAnterior(null);
		this.setAj4Error("");

		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		externalContext.getSessionMap().remove("searchBB");
	}

	// ---- Getters & Setters
	/**
	 * @return the aj4Error
	 */
	public String getAj4Error() {
		return aj4Error;
	}

	/**
	 * @param aj4Error the aj4Error to set
	 */
	public void setAj4Error(String aj4Error) {
		this.aj4Error = aj4Error;
	}

	/**
	 * @return the mensaje1
	 */
	public String getMensaje1() {
		return mensaje1;
	}

	/**
	 * @param mensaje1 the mensaje1 to set
	 */
	public void setMensaje1(String mensaje1) {
		this.mensaje1 = mensaje1;
	}

	/**
	 * @return the mensaje2
	 */
	public String getMensaje2() {
		return mensaje2;
	}

	/**
	 * @param mensaje2 the mensaje2 to set
	 */
	public void setMensaje2(String mensaje2) {
		this.mensaje2 = mensaje2;
	}

	/**
	 * @return the svcMensajes
	 */
	public UIService getSvcMensajes() {
		return svcMensajes;
	}

	/**
	 * @param svcMensajes the svcMensajes to set
	 */
	public void setSvcMensajes(UIService svcMensajes) {
		this.svcMensajes = svcMensajes;
	}

	public void getMensajesUsuario() {
		try {
			User usuario = SynergiaApplicationContext.getCurrentUser();
			loginUsuario = usuario.getUsername();
		}
		catch (Exception e) {
			loginUsuario = "xxx";
		}
		int cantMesagges = 1;
		Collection l = NasServiceHelper.getCollectionResponse("mensajesUsuarioService", "findByCriteria",
			new Object[] { loginUsuario });
		Iterator it = l.iterator();
		while (it.hasNext() && cantMesagges == 1) {
			ConsultaMensajesUsuario c = (ConsultaMensajesUsuario) it.next();
			this.msgUsuario = c.getMensaje();
			cantMesagges = 2;
		}
	}

	/**
	 * @return the msgUsuario
	 */
	public String getMsgUsuario() {
		return msgUsuario;
	}

	/**
	 * @param msgUsuario the msgUsuario to set
	 */
	public void setMsgUsuario(String msgUsuario) {
		this.msgUsuario = msgUsuario;
	}

	public boolean getCnsModule() {
		return SynergiaApplicationContext.getCurrentModuleName().equalsIgnoreCase("cns");
	}

	public String getDigitoValidador() {
		return digitoValidador;
	}

	public void setDigitoValidador(String digitoValidador) {
		this.digitoValidador = digitoValidador;
	}

	public Long getNroCuentaAnterior() {
		return nroCuentaAnterior;
	}

	public void setNroCuentaAnterior(Long nroCuentaAnterior) {
		this.nroCuentaAnterior = nroCuentaAnterior;
	}

	public String getJsExceptionAccountNotFound() {
		return jsExceptionAccountNotFound;
	}

	public void setJsExceptionAccountNotFound(String jsExceptionAccountNotFound) {
		this.jsExceptionAccountNotFound = jsExceptionAccountNotFound;
	}

	public String getJsExceptionOldAccountNotFound() {
		return jsExceptionOldAccountNotFound;
	}

	public void setJsExceptionOldAccountNotFound(String jsExceptionOldAccountNotFound) {
		this.jsExceptionOldAccountNotFound = jsExceptionOldAccountNotFound;
	}

	public boolean isClientePreferencialEnabled() {
		return this.clientePreferencialEnabled;
	}

	public void setClientePreferencialEnabled(boolean clientePreferencialEnabled) {
		this.clientePreferencialEnabled = clientePreferencialEnabled;
	}

	public String getTipoClientePreferencial() {
		return this.tipoClientePreferencial;
	}

	public void setTipoClientePreferencial(String tipoClientePreferencial) {
		this.tipoClientePreferencial = tipoClientePreferencial;
	}

	private void findClientePreferencial(Long nroCuenta) {
		Boolean exists = (Boolean) NasServiceHelper.getResponse("consultaClientePreferencialService", "exists",
			new Object[] { nroCuenta });
		if (!exists.booleanValue()) {
			this.setClientePreferencialEnabled(false);
			this.setTipoClientePreferencial(null);
		}
		else {
			ConsultaClientePreferencial clientePreferencial = (ConsultaClientePreferencial) NasServiceHelper
				.getResponse("consultaClientePreferencialService", "findByCriteriaUnique",
					new Object[] { nroCuenta });
			this.setClientePreferencialEnabled(true);
			this.setTipoClientePreferencial(clientePreferencial.getTipoCliente());
		}
		
	}
}