package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface DeudaOtrosConvenios extends SynergiaBusinessObject {

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public String getIndOtrosConvenios();

	public void setIndOtrosConvenios(String indOtrosConvenios);

	public Double getSaldoOtrosConvenios();

	public void setSaldoOtrosConvenios(Double saldoOtrosConvenios);
	
	
	public Long getAntiguedad();

	public void setAntiguedad(Long antiguedad);

	public String getDeudaECO();

	public void setDeudaECO(String deudaECO);

	public String getSaldoMora();

	public void setSaldoMora(String saldoMora);
	

}