/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaDatosGenerales;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author AR18799631
 *
 */
public class ConsultaDatosGeneralesImpl extends SynergiaBusinessObjectImpl implements ConsultaDatosGenerales {
	private Long nroCuenta;
	private Long nroLote;
	private Double numPcr;
	private String llave;
	private Long clienteAnt;
	private Long recargoTension;
	/**
	 * @return the llave
	 */
	public String getLlave() {
		return llave;
	}
	/**
	 * @param llave the llave to set
	 */
	public void setLlave(String llave) {
		this.llave = llave;
	}
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/**
	 * @return the nroLote
	 */
	public Long getNroLote() {
		return nroLote;
	}
	/**
	 * @param nroLote the nroLote to set
	 */
	public void setNroLote(Long nroLote) {
		this.nroLote = nroLote;
	}
	/**
	 * @return the numPcr
	 */
	public Double getNumPcr() {
		return numPcr;
	}
	/**
	 * @param numPcr the numPcr to set
	 */
	public void setNumPcr(Double numPcr) {
		this.numPcr = numPcr;
	}
	/**
	 * @return the clienteAnt
	 */
	public Long getClienteAnt() {
		return clienteAnt;
	}
	/**
	 * @param clienteAnt the clienteAnt to set
	 */
	public void setClienteAnt(Long clienteAnt) {
		this.clienteAnt = clienteAnt;
	}
	/**
	 * @return the recargoTension
	 */
	public Long getRecargoTension() {
		return recargoTension;
	}
	/**
	 * @param recargoTension the recargoTension to set
	 */
	public void setRecargoTension(Long recargoTension) {
		this.recargoTension = recargoTension;
	}
}
