package com.synapsis.cns.codensa.model.impl;
import java.util.Date;


/**
 * @author m3.MarioRoss - 12/12/2006
 *
 */
public class DetalleCertificadoPago {
	private String concepto;
	private Long numeroDocumento;
	private Date fecha;
	private String estado;
	private Double subtotal;
	
	public DetalleCertificadoPago() {
	}

	public DetalleCertificadoPago(String concepto, Long numeroDocumento, Date fecha, String estado, Double subtotal) {
		this.concepto = concepto;
		this.numeroDocumento = numeroDocumento;
		this.fecha = fecha;
		this.estado = estado;
		this.subtotal = subtotal;
	}
	
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Long getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(Long numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public Double getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}
	
}
