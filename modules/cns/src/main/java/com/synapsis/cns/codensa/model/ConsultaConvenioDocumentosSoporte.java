package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;
public interface ConsultaConvenioDocumentosSoporte extends SynergiaBusinessObject {

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioSoporte#getDocumentosSoporte()
	 */
	public String getDocumentosSoporte();

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioSoporte#setDocumentosSoporte(java.lang.String)
	 */
	public void setDocumentosSoporte(String documentosSoporte);
	
	
	public Long getNroConvenio();
	public void setNroConvenio(Long nroConvenio);
	
	public Long getNroCuenta();
	public void setNroCuenta(Long nroCuenta);
}