package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.DetalleCastigoCartera;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.1 $ $Date: 2007/02/28 21:47:47 $
 * 
 */
public class DetalleCastigoCarteraImpl extends SynergiaBusinessObjectImpl
		implements DetalleCastigoCartera {

	/**
	 * Comment for <code>codigoCargo</code>
	 */
	private String codigoCargo;

	/**
	 * Comment for <code>descripcionCargo</code>
	 */
	private String descripcionCargo;

	/**
	 * Comment for <code>valorCargo</code>
	 */
	private String valorCargo;

	/**
	 * Comment for <code>correlativoFacturacion</code>
	 */
	private String correlativoFacturacion;

	/**
	 * Comment for <code>fechaFacturacion</code>
	 */
	private Date fechaFacturacion;

	/**
	 * Comment for <code>usuarioCastigo</code>
	 */
	private String usuarioCastigo;

	/**
	 * Comment for <code>nombreUsuarioCastigo</code>
	 */
	private String nombreUsuarioCastigo;

	/**
	 * Comment for <code>fechaActualizacionCastigo</code>
	 */
	private Date fechaActualizacionCastigo;

	/**
	 * Comment for <code>motivoCastigo</code>
	 */
	private String motivoCastigo;

	/**
	 * Comment for <code>tipoCastigo</code>
	 */
	private String tipoCastigo;

	/**
	 * Comment for <code>cargoOriginal</code>
	 */
	private String cargoOriginal;

	/**
	 * Comment for <code>observaciones</code>
	 */
	private String observaciones;

	/**
	 * @return the cargoOriginal
	 */
	public String getCargoOriginal() {
		return this.cargoOriginal;
	}

	/**
	 * @param cargoOriginal the cargoOriginal to set
	 */
	public void setCargoOriginal(String cargoOriginal) {
		this.cargoOriginal = cargoOriginal;
	}

	/**
	 * @return the codigoCargo
	 */
	public String getCodigoCargo() {
		return this.codigoCargo;
	}

	/**
	 * @param codigoCargo the codigoCargo to set
	 */
	public void setCodigoCargo(String codigoCargo) {
		this.codigoCargo = codigoCargo;
	}

	/**
	 * @return the correlativoFacturacion
	 */
	public String getCorrelativoFacturacion() {
		return this.correlativoFacturacion;
	}

	/**
	 * @param correlativoFacturacion the correlativoFacturacion to set
	 */
	public void setCorrelativoFacturacion(String correlativoFacturacion) {
		this.correlativoFacturacion = correlativoFacturacion;
	}

	/**
	 * @return the descripcionCargo
	 */
	public String getDescripcionCargo() {
		return this.descripcionCargo;
	}

	/**
	 * @param descripcionCargo the descripcionCargo to set
	 */
	public void setDescripcionCargo(String descripcionCargo) {
		this.descripcionCargo = descripcionCargo;
	}

	/**
	 * @return the fechaActualizacionCastigo
	 */
	public Date getFechaActualizacionCastigo() {
		return this.fechaActualizacionCastigo;
	}

	/**
	 * @param fechaActualizacionCastigo the fechaActualizacionCastigo to set
	 */
	public void setFechaActualizacionCastigo(Date fechaActualizacionCastigo) {
		this.fechaActualizacionCastigo = fechaActualizacionCastigo;
	}

	/**
	 * @return the fechaFacturacion
	 */
	public Date getFechaFacturacion() {
		return this.fechaFacturacion;
	}

	/**
	 * @param fechaFacturacion the fechaFacturacion to set
	 */
	public void setFechaFacturacion(Date fechaFacturacion) {
		this.fechaFacturacion = fechaFacturacion;
	}

	/**
	 * @return the motivoCastigo
	 */
	public String getMotivoCastigo() {
		return this.motivoCastigo;
	}

	/**
	 * @param motivoCastigo the motivoCastigo to set
	 */
	public void setMotivoCastigo(String motivoCastigo) {
		this.motivoCastigo = motivoCastigo;
	}

	/**
	 * @return the nombreUsuarioCastigo
	 */
	public String getNombreUsuarioCastigo() {
		return this.nombreUsuarioCastigo;
	}

	/**
	 * @param nombreUsuarioCastigo the nombreUsuarioCastigo to set
	 */
	public void setNombreUsuarioCastigo(String nombreUsuarioCastigo) {
		this.nombreUsuarioCastigo = nombreUsuarioCastigo;
	}

	/**
	 * @return the observaciones
	 */
	public String getObservaciones() {
		return this.observaciones;
	}

	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	/**
	 * @return the tipoCastigo
	 */
	public String getTipoCastigo() {
		return this.tipoCastigo;
	}

	/**
	 * @param tipoCastigo the tipoCastigo to set
	 */
	public void setTipoCastigo(String tipoCastigo) {
		this.tipoCastigo = tipoCastigo;
	}

	/**
	 * @return the usuarioCastigo
	 */
	public String getUsuarioCastigo() {
		return this.usuarioCastigo;
	}

	/**
	 * @param usuarioCastigo the usuarioCastigo to set
	 */
	public void setUsuarioCastigo(String usuarioCastigo) {
		this.usuarioCastigo = usuarioCastigo;
	}

	/**
	 * @return the valorCargo
	 */
	public String getValorCargo() {
		return this.valorCargo;
	}

	/**
	 * @param valorCargo the valorCargo to set
	 */
	public void setValorCargo(String valorCargo) {
		this.valorCargo = valorCargo;
	}

}