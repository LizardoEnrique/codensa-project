/**
 * $Id: ConsultaDetalleItemsService.java,v 1.1 2008/06/13 16:53:14 ar29261698 Exp $
 */
package com.synapsis.cns.codensa.service;

import java.util.List;

import com.suivant.arquitectura.core.dao.DAO;
import com.suivant.arquitectura.core.exception.IllegalDAOAccessException;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;

/**
 * 
 * @author Paola Attadio
 * @version $Revision: 1.1 $
 */
public interface ConsultaDetalleItemsService {

	public List findByCriteria(QueryFilter filter)
			throws IllegalDAOAccessException;


	public int findByCriteriaToPageable(QueryFilter filter)
			throws IllegalDAOAccessException;	
	/**
	 * retorna el dao asociado
	 * 
	 * @return DAO
	 * @throws IllegalDAOAccessException
	 */
	public DAO getDao(Class klass) throws IllegalDAOAccessException;

}