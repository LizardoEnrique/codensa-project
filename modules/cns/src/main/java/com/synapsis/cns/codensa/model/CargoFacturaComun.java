package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * Impresión duplicado factura - Cargos de la factura
 * 
 * @author adambrosio
 */
public interface CargoFacturaComun extends SynergiaBusinessObject {
	public Long getNroFactura();
	public void setNroFactura(Long nroFactura);
	public String getCodigo();
	public void setCodigo(String codigo);
	public String getConcepto();
	public void setConcepto(String concepto);
	public Long getIndicadorCargo();
	public void setIndicadorCargo(Long indicadorCargo); 
}
