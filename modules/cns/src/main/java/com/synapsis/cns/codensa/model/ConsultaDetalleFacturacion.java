package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface ConsultaDetalleFacturacion {

	/**
	 * @return the fechaFacturacion
	 */
	public Date getFechaFacturacion();

	/**
	 * @param fechaFacturacion the fechaFacturacion to set
	 */
	public void setFechaFacturacion(Date fechaFacturacion);

	/**
	 * @return the fechaLectura
	 */
	public Date getFechaLectura();

	/**
	 * @param fechaLectura the fechaLectura to set
	 */
	public void setFechaLectura(Date fechaLectura);

	/**
	 * @return the fechaVencimiento
	 */
	public Date getFechaVencimiento();

	/**
	 * @param fechaVencimiento the fechaVencimiento to set
	 */
	public void setFechaVencimiento(Date fechaVencimiento);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroFacturaEmitida
	 */
	public Long getNroFacturaEmitida();

	/**
	 * @param nroFacturaEmitida the nroFacturaEmitida to set
	 */
	public void setNroFacturaEmitida(Long nroFacturaEmitida);

	/**
	 * @return the periodoFacturacion
	 */
	public Date getPeriodoFacturacion();

	/**
	 * @param periodoFacturacion the periodoFacturacion to set
	 */
	public void setPeriodoFacturacion(Date periodoFacturacion);

	/**
	 * @return the valorTotalFactura
	 */
	public String getValorTotalFactura();

	/**
	 * @param valorTotalFactura the valorTotalFactura to set
	 */
	public void setValorTotalFactura(String valorTotalFactura);

}