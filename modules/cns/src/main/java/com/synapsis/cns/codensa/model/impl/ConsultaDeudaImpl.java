package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaDeuda;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.1 $ $Date: 2007/02/28 21:47:47 $
 * 
 */
public class ConsultaDeudaImpl extends SynergiaBusinessObjectImpl implements
		ConsultaDeuda {

	/**
	 * Comment for <code>deudaTotalActual</code>
	 */
	private Double deudaTotalActual;

	/**
	 * Comment for <code>saldoTotalSujetoIntereses</code>
	 */
	private Double saldoTotalSujetoIntereses;

	/**
	 * Comment for <code>saldoNoSujetoIntereses</code>
	 */
	private Double saldoNoSujetoIntereses;

	/**
	 * Comment for <code>valorTotalIntereses</code>
	 */
	private Double valorTotalIntereses;

	/**
	 * Comment for <code>valorTotalSanciones</code>
	 */
	private Double valorTotalSanciones;

	/**
	 * Comment for <code>valorTotalOtrosNegocios</code>
	 */
	private Double valorTotalOtrosNegocios;

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.ConsultaDeuda#getDeudaTotalActual()
	 */
	public Double getDeudaTotalActual() {
		return this.deudaTotalActual;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.ConsultaDeuda#setDeudaTotalActual(java.lang.Double)
	 */
	public void setDeudaTotalActual(Double deudaTotalActual) {
		this.deudaTotalActual = deudaTotalActual;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.ConsultaDeuda#getSaldoNoSujetoIntereses()
	 */
	public Double getSaldoNoSujetoIntereses() {
		return this.saldoNoSujetoIntereses;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.ConsultaDeuda#setSaldoNoSujetoIntereses(java.lang.Double)
	 */
	public void setSaldoNoSujetoIntereses(Double saldoNoSujetoIntereses) {
		this.saldoNoSujetoIntereses = saldoNoSujetoIntereses;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.ConsultaDeuda#getSaldoTotalSujetoIntereses()
	 */
	public Double getSaldoTotalSujetoIntereses() {
		return this.saldoTotalSujetoIntereses;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.ConsultaDeuda#setSaldoTotalSujetoIntereses(java.lang.Double)
	 */
	public void setSaldoTotalSujetoIntereses(Double saldoTotalSujetoIntereses) {
		this.saldoTotalSujetoIntereses = saldoTotalSujetoIntereses;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.ConsultaDeuda#getValorTotalIntereses()
	 */
	public Double getValorTotalIntereses() {
		return this.valorTotalIntereses;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.ConsultaDeuda#setValorTotalIntereses(java.lang.Double)
	 */
	public void setValorTotalIntereses(Double valorTotalIntereses) {
		this.valorTotalIntereses = valorTotalIntereses;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.ConsultaDeuda#getValorTotalOtrosNegocios()
	 */
	public Double getValorTotalOtrosNegocios() {
		return this.valorTotalOtrosNegocios;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.ConsultaDeuda#setValorTotalOtrosNegocios(java.lang.Double)
	 */
	public void setValorTotalOtrosNegocios(Double valorTotalOtrosNegocios) {
		this.valorTotalOtrosNegocios = valorTotalOtrosNegocios;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.ConsultaDeuda#getValorTotalSanciones()
	 */
	public Double getValorTotalSanciones() {
		return this.valorTotalSanciones;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.ConsultaDeuda#setValorTotalSanciones(java.lang.Double)
	 */
	public void setValorTotalSanciones(Double valorTotalSanciones) {
		this.valorTotalSanciones = valorTotalSanciones;
	}

}