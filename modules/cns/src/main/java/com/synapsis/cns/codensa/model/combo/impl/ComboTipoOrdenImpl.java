/**
 * $Id: ComboTipoOrdenImpl.java,v 1.4 2007/02/26 13:18:43 ar30557486 Exp $
 */

package com.synapsis.cns.codensa.model.combo.impl;

import com.synapsis.cns.codensa.model.combo.ComboTipoOrden;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ComboTipoOrdenImpl extends SynergiaBusinessObjectImpl
		implements ComboTipoOrden {
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String codigo;
	private String descripcion;
	
	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return this.codigo;
	}
	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
		
	
}