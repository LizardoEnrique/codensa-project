package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.OrdenAtencionAic;

/**
 * @author m3.MarioRoss - 14/11/2006
 */
public class OrdenAtencionAicImpl extends SynergiaBusinessObjectImpl 
	implements OrdenAtencionAic {
	
	private Long nroOrden;
	private Long idAtencion;
	private String tipo;
	private String estado;
	private Date fechaIngreso;
	
	
	public String getEstado() {
		return this.estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Date getFechaIngreso() {
		return this.fechaIngreso;
	}
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public Long getIdAtencion() {
		return this.idAtencion;
	}
	public void setIdAtencion(Long idAtencion) {
		this.idAtencion = idAtencion;
	}
	public Long getNroOrden() {
		return this.nroOrden;
	}
	public void setNroOrden(Long nroOrden) {
		this.nroOrden = nroOrden;
	}
	public String getTipo() {
		return this.tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
}
