package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface AtributosServicio {

	public String getCastigado();

	public void setCastigado(String castigado);

	public String getCentralRiesgo();

	public void setCentralRiesgo(String centralRiesgo);

	public String getChequeDevuelto();

	public void setChequeDevuelto(String chequeDevuelto);

	public String getClienteVIP();

	public void setClienteVIP(String clienteVIP);

	public String getDeudaCorregida();

	public void setDeudaCorregida(String deudaCorregida);

	public Date getFechaRetiro();

	public void setFechaRetiro(Date fechaRetiro);

	public String getMantPreventivo();

	public void setMantPreventivo(String mantPreventivo);

	public String getMedicionProceso();

	public void setMedicionProceso(String medicionProceso);

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public String getReconexionAnulada();

	public void setReconexionAnulada(String reconexionAnulada);

	public String getReincorporado();

	public void setReincorporado(String reincorporado);

	public String getTieneCNR();

	public void setTieneCNR(String tieneCNR);

	public String getTieneConvenio();

	public void setTieneConvenio(String tieneConvenio);

	public String getTieneEncargoCobranza();

	public void setTieneEncargoCobranza(String tieneEncargoCobranza);

	public String getTieneLeyArrendamiento();

	public void setTieneLeyArrendamiento(String tieneLeyArrendamiento);

	public String getTieneSaldosDisputa();

	public void setTieneSaldosDisputa(String tieneSaldosDisputa);

	public String getTieneServicioFinanciero();

	public void setTieneServicioFinanciero(String tieneServicioFinanciero);
	public String getClienteRegular();
	public void setClienteRegular(String clienteRegular);
	/**
	 * @return the blockPrueba
	 */
	public String getBlockPrueba();
	/**
	 * @param blockPrueba the blockPrueba to set
	 */
	public void setBlockPrueba(String blockPrueba);
	/**
	 * @return the celdaFoto
	 */
	public String getCeldaFoto();
	/**
	 * @param celdaFoto the celdaFoto to set
	 */
	public void setCeldaFoto(String celdaFoto);
	/**
	 * @return the condensador
	 */
	public String getCondensador();
	/**
	 * @param condensador the condensador to set
	 */
	public void setCondensador(String condensador);
	/**
	 * @return the interHorario
	 */
	public String getInterHorario();
	/**
	 * @param interHorario the interHorario to set
	 */
	public void setInterHorario(String interHorario);
	

}