package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


public interface CertificaBuenCliente extends SynergiaBusinessObject {

	public Boolean getCertifica();

	public void setCertifica(Boolean certifica);

	public Boolean getDeuda();

	public void setDeuda(Boolean deuda);

	public Long getIdCuenta();

	public void setIdCuenta(Long idCuenta);

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public Boolean getExpedientes();
	
	public void setExpedientes(Boolean expedientes);
	
	public Boolean getHabitos();
	
	public void setHabitos(Boolean habitos);
}