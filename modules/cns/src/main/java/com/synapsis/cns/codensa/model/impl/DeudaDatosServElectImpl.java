package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.cns.codensa.model.DeudaDatosServElect;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class DeudaDatosServElectImpl extends SynergiaBusinessObjectImpl
		implements DeudaDatosServElect {

	private Long nroCuenta;
	private Double antiguedadDeuda;
	private Double ultimoValorConsumoKw;
	private String ultimoValorConsumoPesos;
	private Date fechaFacturaUltimoConsumo;
	private Double activoFp;
	private Double activoHp;
	private Double reactivoFp;
	private Double reactivoHp;
	private Double activoXP;
	private Double reactivoXP;
	private String estadoCobranza;

	
	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public Double getAntiguedadDeuda() {
		return antiguedadDeuda;
	}

	public void setAntiguedadDeuda(Double antiguedadDeuda) {
		this.antiguedadDeuda = antiguedadDeuda;
	}

	public Double getUltimoValorConsumoKw() {
		return ultimoValorConsumoKw;
	}

	public void setUltimoValorConsumoKw(Double ultimoValorConsumoKw) {
		this.ultimoValorConsumoKw = ultimoValorConsumoKw;
	}

	public String getUltimoValorConsumoPesos() {
		return this.ultimoValorConsumoPesos;
	}

	public void setUltimoValorConsumoPesos(String ultimoValorConsumoPesos) {
		this.ultimoValorConsumoPesos = ultimoValorConsumoPesos;
	}

	public Date getFechaFacturaUltimoConsumo() {
		return fechaFacturaUltimoConsumo;
	}

	public void setFechaFacturaUltimoConsumo(Date fechaFacturaUltimoConsumo) {
		this.fechaFacturaUltimoConsumo = fechaFacturaUltimoConsumo;
	}

	public Double getActivoFp() {
		return activoFp;
	}

	public void setActivoFp(Double activoFp) {
		this.activoFp = activoFp;
	}

	public Double getActivoHp() {
		return activoHp;
	}

	public void setActivoHp(Double activoHp) {
		this.activoHp = activoHp;
	}

	public Double getReactivoFp() {
		return reactivoFp;
	}

	public void setReactivoFp(Double reactivoFp) {
		this.reactivoFp = reactivoFp;
	}

	public Double getReactivoHp() {
		return reactivoHp;
	}

	public void setReactivoHp(Double reactivoHp) {
		this.reactivoHp = reactivoHp;
	}

	public String getEstadoCobranza() {
		return estadoCobranza;
	}

	public void setEstadoCobranza(String estadoCobranza) {
		this.estadoCobranza = estadoCobranza;
	}

	public Double getActivoXP() {
		return activoXP;
	}

	public void setActivoXP(Double activoXP) {
		this.activoXP = activoXP;
	}

	public Double getReactivoXP() {
		return reactivoXP;
	}

	public void setReactivoXP(Double reactivoXP) {
		this.reactivoXP = reactivoXP;
	}
	
	
}