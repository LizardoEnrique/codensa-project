package com.synapsis.cns.codensa.model.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.synapsis.cns.codensa.model.ReporteCertificaConvenio;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author jhv - 20/03/2007
 *
 */
public class ReporteCertificaConvenioImpl extends SynergiaBusinessObjectImpl implements ReporteCertificaConvenio {
	
	// atributos necesarios para obtener cada campo (dia, mes, a�o) de la fechaExpedicion
	private Calendar cal = Calendar.getInstance();
	private static DateFormat df = new SimpleDateFormat("MMMMM", Locale.getDefault());
	
	private Long nroCuenta;
	private Date fechaExpedicion;

	private String nombreSolicitante;
	private String tipoIdentificacion;
	private String numeroIdentificacion;
	
	private String nombreTitularCuenta;
	private String apellidoTitularCuenta;
	private String direccion;
	private String localizacion;
	private String municipio;
	private String barrio;
	private String telefono;
	private String direccionRepartoEspecial;
	
	private String usuarioSistema;
	
	public Integer getDiaExpedicion(){
		return new Integer(cal.get(Calendar.DAY_OF_MONTH));
	}
	
	public String getMesExpedicion(){
		return df.format(getFechaExpedicion());
	}
	
	public Integer getAnioExpedicion(){
		return new Integer(cal.get(Calendar.YEAR));
	}
	/**
	 * Devuelve un String tal como est� si no es null. Si es null devuelve un String vac�o
	 */
	private String nullToEmpty(String string) {
		return string != null ? string : "";
	}
	
	public String getBarrio() {
		return nullToEmpty(barrio);
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}
	
	public String getDireccion() {
		return nullToEmpty(direccion);
	}
	
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	public String getDireccionRepartoEspecial() {
		return nullToEmpty(direccionRepartoEspecial);
	}
	
	public void setDireccionRepartoEspecial(String direccionRepartoEspecial) {
		this.direccionRepartoEspecial = direccionRepartoEspecial;
	}

	public Date getFechaExpedicion() {
		return fechaExpedicion;
	}
	
	public void setFechaExpedicion(Date fechaExpedicion) {
		this.fechaExpedicion = fechaExpedicion;
		cal.setTime(fechaExpedicion);
	}
	
	public String getLocalizacion() {
		return nullToEmpty(localizacion);
	}
	
	public void setLocalizacion(String localizacion) {
		this.localizacion = localizacion;
	}
	
	public String getMunicipio() {
		return nullToEmpty(municipio);
	}
	
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	
	public String getNombreSolicitante() {
		return nullToEmpty(nombreSolicitante);
	}
	
	public void setNombreSolicitante(String nombreSolicitante) {
		this.nombreSolicitante = nombreSolicitante;
	}
	
	public String getNombreTitularCuenta() {
		return nombreTitularCuenta;
	}
	
	public void setNombreTitularCuenta(String nombreTitularCuenta) {
		this.nombreTitularCuenta = nombreTitularCuenta;
	}

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getNumeroIdentificacion() {
		return numeroIdentificacion;
	}
	
	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}
	
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	public String getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}
	
	public String getUsuarioSistema() {
		return usuarioSistema;
	
	}	
	public void setUsuarioSistema(String usuarioSistema) {
		this.usuarioSistema = usuarioSistema;
	}
	
	public String getApellidoTitularCuenta() {
		return nullToEmpty(apellidoTitularCuenta);
	}
	public void setApellidoTitularCuenta(String apellidoTitularCuenta) {
		this.apellidoTitularCuenta = apellidoTitularCuenta;
	}
}
