package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface EstadoServicio extends SynergiaBusinessObject{

	/**
	 * @return the accionLegal
	 */
	public abstract String getAccionLegal();

	/**
	 * @param accionLegal the accionLegal to set
	 */
	public abstract void setAccionLegal(String accionLegal);

	/**
	 * @return the cobranzaExterna
	 */
	public abstract String getCobranzaExterna();

	/**
	 * @param cobranzaExterna the cobranzaExterna to set
	 */
	public abstract void setCobranzaExterna(String cobranzaExterna);

	/**
	 * @return the convenioRestringido
	 */
	public abstract String getConvenioRestringido();

	/**
	 * @param convenioRestringido the convenioRestringido to set
	 */
	public abstract void setConvenioRestringido(String convenioRestringido);

	/**
	 * @return the corteRestringido
	 */
	public abstract String getCorteRestringido();

	/**
	 * @param corteRestringido the corteRestringido to set
	 */
	public abstract void setCorteRestringido(String corteRestringido);

	/**
	 * @return the nroCuenta
	 */
	public abstract Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public abstract void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroServicio
	 */
	public abstract Long getNroServicio();

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public abstract void setNroServicio(Long nroServicio);

	/**
	 * @return the suministro
	 */
	public abstract String getSuministro();

	/**
	 * @param suministro the suministro to set
	 */
	public abstract void setSuministro(String suministro);

}