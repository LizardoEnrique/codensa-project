package com.synapsis.cns.codensa.model.combo.impl;

import com.synapsis.cns.codensa.model.combo.ComboTiposServicioCCA;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ComboTiposServicioCCAImpl extends SynergiaBusinessObjectImpl implements ComboTiposServicioCCA {
	   private String tipo;                
	   private Long nroCuenta;
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}
	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}             
}
