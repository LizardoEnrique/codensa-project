package com.synapsis.cns.codensa.model.impl.buscaCuenta;

import com.synapsis.cns.codensa.model.buscaCuenta.BuscaCuentaTipoNroDocumento;

public class BuscaCuentaTipoNroDocumentoImpl extends BuscaCuentaImpl implements BuscaCuentaTipoNroDocumento {
	
	private String tipoDocumento;
	private Long nroDocumento;

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaTipoNroDocumento#getNroDocumento()
	 */
	public Long getNroDocumento() {
		return nroDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaTipoNroDocumento#getTipoDocumento()
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaTipoNroDocumento#setNroDocumento(java.lang.Long)
	 */
	public void setNroDocumento(Long nroDocumento) {
		this.nroDocumento = nroDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaTipoNroDocumento#setTipoDocumento(java.lang.Long)
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
}
