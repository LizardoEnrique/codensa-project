package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaExpedienteAgendaEjecutivo extends SynergiaBusinessObject{
	public Long getNumeroExpediente();
	public void setNumeroExpediente(Long numeroExpediente);
	/**
	 * @return the fechaAgendaOtorgada
	 */
	public Date getFechaAgendaOtorgada();

	/**
	 * @param fechaAgendaOtorgada the fechaAgendaOtorgada to set
	 */
	public void setFechaAgendaOtorgada(Date fechaAgendaOtorgada);

	/**
	 * @return the fechaHoraCita
	 */
	public Date getFechaHoraCita();

	/**
	 * @param fechaHoraCita the fechaHoraCita to set
	 */
	public void setFechaHoraCita(Date fechaHoraCita);

	/**
	 * @return the lugarCitacion
	 */
	public String getLugarCitacion();

	/**
	 * @param lugarCitacion the lugarCitacion to set
	 */
	public void setLugarCitacion(String lugarCitacion);

	/**
	 * @return the nombreEjecutivo
	 */
	public String getNombreEjecutivo();

	/**
	 * @param nombreEjecutivo the nombreEjecutivo to set
	 */
	public void setNombreEjecutivo(String nombreEjecutivo);

	/**
	 * @return the observacionesEjecutivo
	 */
	public String getObservacionesEjecutivo();

	/**
	 * @param observacionesEjecutivo the observacionesEjecutivo to set
	 */
	public void setObservacionesEjecutivo(String observacionesEjecutivo);

	/**
	 * @return the resultadoCita
	 */
	public String getResultadoCita();

	/**
	 * @param resultadoCita the resultadoCita to set
	 */
	public void setResultadoCita(String resultadoCita);

}