package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaDeudaCodensaHogar;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.4 $ $Date: 2010/11/05 16:55:00 $
 * 
 */
public class ConsultaDeudaCodensaHogarImpl extends SynergiaBusinessObjectImpl
		implements ConsultaDeudaCodensaHogar {

	/**
	 * Comment for <code>deuda</code>
	 */
	private String deuda;
	private String valorDeuda;
	private Long nroCuenta;
	
	/**
	 * Comment for <code>antiguedadDeuda</code>
	 */
	private Long antiguedadDeuda;

	/**
	 * Comment for <code>estadoCobranza</code>
	 */
	private String estadoCobranza;

	/**
	 * @return the deuda
	 */
	public String getDeuda() {
		return this.deuda;
	}
	
	/**
	 * @param deuda the deuda to set
	 */
	public void setDeuda(String deuda) {
		this.deuda = deuda;
	}
	
	/**
	 * @return the antiguedadDeuda
	 */
	public Long getAntiguedadDeuda() {
		return this.antiguedadDeuda;
	}

	/**
	 * @param antiguedadDeuda
	 *            the antiguedadDeuda to set
	 */
	public void setAntiguedadDeuda(Long antiguedadDeuda) {
		this.antiguedadDeuda = antiguedadDeuda;
	}

	/**
	 * @return the estadoCobranza
	 */
	public String getEstadoCobranza() {
		return this.estadoCobranza;
	}

	/**
	 * @param estadoCobranza
	 *            the estadoCobranza to set
	 */
	public void setEstadoCobranza(String estadoCobranza) {
		this.estadoCobranza = estadoCobranza;
	}

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getValorDeuda() {
		return this.valorDeuda;
	}

	public void setValorDeuda(String valorDeuda) {
		this.valorDeuda = valorDeuda;
		
	}

	
	
	
}