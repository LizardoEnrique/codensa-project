package com.synapsis.cns.codensa.model;

import java.math.BigDecimal;
import java.util.Date;

public interface ConsultaOrdenSED {

	public Date getFechaAprobacion();
	public void setFechaAprobacion(Date fechaAprobacion);
	public Long getNumeroSaldo() ;
	public void setNumeroSaldo(Long numeroOrden);
	public String getEstado() ;
	public void setEstado(String estado);
	public Double getValor() ;
	public void setValor(Double valor);
	public Double getValorAntesCongelacion();
	public void setValorAntesCongelacion(Double valorAntesCongelacion);
	public Double getValorDespuesCongelacion() ;
	public void setValorDespuesCongelacion(Double valorDespuesCongelacion);
	public Double getValorAntesDescongelacion();
	public void setValorAntesDescongelacion(Double valorAntesDescongelacion);
	public Double getValorDespuesDescongelacion();
	public void setValorDespuesDescongelacion(Double valorDespuesDescongelacion);
	public Long getNroCuenta();
	public void setNroCuenta(Long nroCuenta);
	
}
