package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.cns.codensa.model.DeudaCargosManuales;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class DeudaCargosManualesImpl extends SynergiaBusinessObjectImpl
		implements DeudaCargosManuales {

	private Long nroCuenta;

	private Long idItemDoc;

	private String codigoCargo;

	private String descripcionCargo;

	private Double valorCargo;

	private String tipoServicioCargo;

	private Long nroServicioCargo;

	private Date fechaIngresoCargo;

	private Date fechaDesarrolloActividadTerreno;

	private String usuarioIngresoCargo;

	private String nombreIngresoCargo;

	private Long nroDocumentoIngresoCargo;

	private String estadoFactura;

	private String observaciones;
	
	private String origenCargo;

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public Long getIdItemDoc() {
		return idItemDoc;
	}

	public void setIdItemDoc(Long idItemDoc) {
		this.idItemDoc = idItemDoc;
	}

	public String getCodigoCargo() {
		return codigoCargo;
	}

	public void setCodigoCargo(String codigoCargo) {
		this.codigoCargo = codigoCargo;
	}

	public String getDescripcionCargo() {
		return descripcionCargo;
	}

	public void setDescripcionCargo(String descripcionCargo) {
		this.descripcionCargo = descripcionCargo;
	}

	public Double getValorCargo() {
		return valorCargo;
	}

	public void setValorCargo(Double valorCargo) {
		this.valorCargo = valorCargo;
	}

	public String getTipoServicioCargo() {
		return tipoServicioCargo;
	}

	public void setTipoServicioCargo(String tipoServicioCargo) {
		this.tipoServicioCargo = tipoServicioCargo;
	}

	public Long getNroServicioCargo() {
		return nroServicioCargo;
	}

	public void setNroServicioCargo(Long nroServicioCargo) {
		this.nroServicioCargo = nroServicioCargo;
	}

	public Date getFechaIngresoCargo() {
		return fechaIngresoCargo;
	}

	public void setFechaIngresoCargo(Date fechaIngresoCargo) {
		this.fechaIngresoCargo = fechaIngresoCargo;
	}

	public Date getFechaDesarrolloActividadTerreno() {
		return fechaDesarrolloActividadTerreno;
	}

	public void setFechaDesarrolloActividadTerreno(Date fechaDesarrolloActividadTerreno) {
		this.fechaDesarrolloActividadTerreno = fechaDesarrolloActividadTerreno;
	}

	public String getUsuarioIngresoCargo() {
		return usuarioIngresoCargo;
	}

	public void setUsuarioIngresoCargo(String usuarioIngresoCargo) {
		this.usuarioIngresoCargo = usuarioIngresoCargo;
	}

	public String getNombreIngresoCargo() {
		return nombreIngresoCargo;
	}

	public void setNombreIngresoCargo(String nombreIngresoCargo) {
		this.nombreIngresoCargo = nombreIngresoCargo;
	}

	public Long getNroDocumentoIngresoCargo() {
		return nroDocumentoIngresoCargo;
	}

	public void setNroDocumentoIngresoCargo(Long nroDocumentoIngresoCargo) {
		this.nroDocumentoIngresoCargo = nroDocumentoIngresoCargo;
	}

	public String getEstadoFactura() {
		return estadoFactura;
	}

	public void setEstadoFactura(String estadoFactura) {
		this.estadoFactura = estadoFactura;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	public String getOrigenCargo() {
		return origenCargo;
	}

	public void setOrigenCargo(String origenCargo) {
		this.origenCargo = origenCargo;
	}
}