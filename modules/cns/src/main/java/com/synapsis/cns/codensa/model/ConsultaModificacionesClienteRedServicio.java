package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.Empresa;
import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaModificacionesClienteRedServicio extends SynergiaBusinessObject {

    
	public String getDirSuministro();
	public void setDirSuministro(String dirSuministro);
	
	public Empresa getEmpresa();
	public void setEmpresa(Empresa empresa);
	
	public String getEstadoSrvElectrico();
	public void setEstadoSrvElectrico(String estadoSrvElectrico);
	
	public Long getId();
	public void setId(Long id);
	
	public String getMercado();
	public void setMercado(String mercado);
	
	public Long getNroSrvElectrico();
	public void setNroSrvElectrico(Long nroSrvElectrico);
	
	public String getRutaFact();
	public void setRutaFact(String rutaFact);
	
	public Double getTarifa();
	public void setTarifa(Double tarifa);
	
	public String getTipoSrvElectrico();
	public void setTipoSrvElectrico(String tipoSrvElectrico);
	
	public String getPeriodoDesde();
	public void setPeriodoDesde(String periodoDesde);
	
	public String getPeriodoHasta();
	public void setPeriodoHasta(String periodoHasta);


}
