package com.synapsis.cns.codensa.model.constants;



/**
 */
public class FacturaConstants {
	
	
	
	/**
	 * Keys de todos los campos mapeados en la LINEA de la factura.
	 * DEBEN COINCIDIR CON EL CAMPO "NOMBRE" de la tabla CNS_CAMPO_FACTURA, 
	 * en cuya tabla, se encuentra la ubicación actual del campo dentro del la 
	 * línea (posición inicial, posición final ... etc) 
	 */
	
	public static String 	TIPO_DOC_FACTURA = "FACTURA";
	
	// CAMPOS DE PRO-C
	public static String FIELD_NOMBRE_CLIENTE = "nombre_del_cliente";
	public static String FIELD_NUMERO_CUENTA = "numero_cuenta";
	public static String FIELD_DV_NUMERO_CUENTA = "dv_numero_cuenta";
	public static String FIELD_CODIGO_FACTURACION = "codigo_facturacion";
	public static String FIELD_FECHA_PROCESO = "aniomes_factura";
	public static String FIELD_NIT = "tipo_de_identificacion_cliente";
	public static String FIELD_NUMERO_FACTURA = "numero_de_factura";
	public static String FIELD_DIGITO_FACTURA = "digito_factura";
	public static String FIELD_NUMERO_SERVICIO = "numero_servicio_electrico"; 
	public static String FIELD_PERIODO_FACTURADO_INICIO = "fecha_desde"; 
	public static String FIELD_PERIODO_FACTURADO_FIN = "fecha_hasta"; 
	public static String FIELD_DIRECC_CLIENTE = "localizacion_de_lectura";
	public static String FIELD_DIRECC_SUMINISTRO = "direccion_de_suministro";
	public static String FIELD_BARRIO_CLIENTE = "descripcion_de_barrio_o_vereda_lectura";  
	public static String FIELD_MUNICIPIO_CLIENTE = "descripcion_de_municipio_lectura"; 
	public static String FIELD_INDIC_REPARTO_ESPEC = "codigo_reparto";
	public static String FIELD_DIRECCION_REPARTO = "descripcion_de_municipio_reparto";
	public static String FIELD_MUNICIPIO_REPARTO = "direccion_de_reparto";
	public static String FIELD_BARRIO_REPARTO = "descripcion_de_barrio_o_vereda_reparto";

	public static String FIELD_PROXIMA_LECTURA = "fecha_proxima_lectura";
	
	public static String FIELD_TIPO_SERV_INFO_TEC = "clase_servicio"; 
	public static String FIELD_ZONA_LECTURA = "zona_lectura"; 
	public static String FIELD_CICLO_LECTURA = "ciclo_lectura"; 
	public static String FIELD_GRUPO_LECTURA = "grupo_lectura"; 
	public static String FIELD_SUCURSAL_LECTURA = "codigo_sucursal_lectura";
	public static String FIELD_MANZANA_LECTURA = "manzana_lectura";
	public static String FIELD_CORRELATIVO_LECTURA = "correlativo_ruta_lectura";
	
	public static String FIELD_ZONA_REPARTO = "zona_reparto"; 
	public static String FIELD_CICLO_REPARTO = "ciclo_reparto"; 
	public static String FIELD_GRUPO_REPARTO = "grupo_reparto"; 
	public static String FIELD_SUCURSAL_REPARTO = "codigo_sucursal_reparto"; 
	public static String FIELD_MANZANA_REPARTO = "manzana_reparto";
	public static String FIELD_CORRELATIVO_REPARTO = "correlativo_ruta_reparto";
	
	public static String FIELD_NRO_MEDIDOR_1 = "medidor_activa";
	public static String FIELD_NRO_MEDIDOR_2 = "medidor_reactiva";
	public static String FIELD_NRO_CIRCUITO = "circuito";
	
	//No existe en la tabla
	public static String FIELD_GRUPO_C = "grupo_c";
	public static String FIELD_GRUPO_C2 = "grupo_c2";
	public static String FIELD_GRUPO_C3 = "grupo_c3";
	
	public static String FIELD_ESTRATO_SOCIO = "estrato_socio_economico";
	public static String FIELD_CARGA_CONTRATADA = "carga_contratada";
	public static String FIELD_NIVEL_TENSION = "nivel_de_tension";
	public static String FIELD_PERIODO_FACT_ACTUAL = "aniomes_factura";
	public static String FIELD_CONSUMO_PERIODO_ACTUAL = "total_consumo_kw";
	public static String FIELD_FEC_VENC_PAGO_OPORT = "fecha_oportuno";
	public static String FIELD_FEC_VENC_AVISO_SUSP = "fecha_recargo";
	public static String FIELD_TOTAL_A_PAGAR = "valor_unico_pagar";

	public static String FIELD_DIR_OFIC_PPAL = "FIELD_DIR_OFIC_PPAL";	
	public static String FIELD_FECHA_EXPEDICION = "FIELD_FECHA_EXPEDICION";
	
	// CODIGO DE BARRAS
	public static String FIELD_COD_BARRAS_BEGIN = "cadena_ocr_codigo_de_barras_begin";
	public static String FIELD_COD_BARRAS_NIEOCR = "cadena_ocr_codigo_de_barras_nie_ocr";
	public static String FIELD_COD_BARRAS_DVNIE = "cadena_ocr_codigo_de_barras_dv_nie";
	public static String FIELD_COD_BARRAS_VALOROCR = "cadena_ocr_codigo_de_barras_valor_ocr";
	public static String FIELD_COD_BARRAS_FACTOCR = "cadena_ocr_codigo_de_barras_factura_ocr";
	public static String FIELD_COD_BARRAS_STOP = "cadena_ocr_codigo_de_barras_stopp";
	// NUMERO DE CODIGO DE BARRAS
	public static String FIELD_VAR_IDENTIF_P415P = "identif_p415p";
	public static String FIELD_VAR_IDENTIF_EAN_SV = "identif_ean_sv";
	public static String FIELD_VAR_IDENTIF_P8020P = "identif_p8020p";
	public static String FIELD_VAR_TIPO_DOCUMENTO2 = "tipo_documento2";
	public static String FIELD_VAR_NIE_CODBAR_SV = "nie_codbar_sv";
	public static String FIELD_VAR_NROFAC_CODBAR_SV = "nrofac_codbar_sv";
	public static String FIELD_VAR_DIGFAC_CODBAR_SV = "digfac_codbar_sv";
	public static String FIELD_VAR_IDENTIF_P3900P = "identif_p3900p";
	public static String FIELD_VAR_VALOR_CODBAR_SV = "valor_codbar_sv";

	
	// INFORMACION DE INTERES
	public static String FIELD_VAR_MENSAJE_FIJO = "mensaje_fijo";
	public static String FIELD_VAR_MENSAJE_CUADRO1 = "mensaje_cuadro1";
	public static String FIELD_VAR_MENSAJE_CUADRO2 = "mensaje_cuadro2";
	public static String FIELD_VAR_MENSAJE_CUADRO3 = "mensaje_cuadro3";

	
	// INFORMACIÓN DEL CONSUMO
	public static String FIELD_TIPO_LECT_PER_ACTUAL = "lectu_tipo_lectura";
	public static String FIELD_ANOMALIA = "anomalia";
	public static String FIELD_MES_TARIFA_LIQ = "fecha_tarifas";
	public static String FIELD_CONS_PROM_ULT_MESES = "suma_promedio_activa";
	public static String FIELD_COSTO_UNIT = "costo_unitario_referencia";
	public static String FIELD_TARIF_FACT_ACTUAL = "tarifa";
	public static String FIELD_TOTAL_CONS_KWH = "total_consumo_kw";
	public static String FIELD_VALOR_PROM_KWH = "valor_promedio_del_kwh";
		
	public static String FIELD_MAX_PERM_INTERRUP = "max_fes";
	public static String FIELD_MAX_PERM_INTERRUP2 = "max_fes2";
	public static String FIELD_MAX_PERM_INTERRUP3 = "max_fes3";
	public static String FIELD_MAX_PERM_HORAS = "max_des";
	public static String FIELD_MAX_PERM_HORAS2 = "max_des2";
	public static String FIELD_MAX_PERM_HORAS3 = "max_des3";
	public static String FIELD_NRO_INTERRUP_ACUM = "fes";
	public static String FIELD_NRO_INTERRUP_ACUM2 = "fes2";
	public static String FIELD_NRO_INTERRUP_ACUM3 = "fes3";
	public static String FIELD_NRO_HORAS_INTERRUP_ACUM = "des"; 			
	public static String FIELD_NRO_HORAS_INTERRUP_ACUM2 = "des2"; 
	public static String FIELD_NRO_HORAS_INTERRUP_ACUM3 = "des3";
	
	public static String FIELD_PERIODO_INTERRUP = "trimestre";
	public static String FIELD_TRIMESTRE_INTERRUP = "trimestre"; 
	
	//Factura Totales
	public static String FIELD_SUBT_TOTAL_CARGOS = "subtotal_conceptos_energia";
	public static String FIELD_SUBT_CARGOS_DESC = "subtotal_descuentos";
	public static String FIELD_SUBT_CARGOS_SERV_PORT = "subtotal_terceros";
	public static String FIELD_SUBT_OTROS_CARGOS = "subtotal_cnc";
	public static String FIELD_SUBT_TOTAL_FACTURA = "total_consumo";
	public static String FIELD_SUBT_CARGOS_CONS = "total_consumo_val";

	
	//Consumos ultimos 6 meses	
	public static String FIELD_CONS_ULT_6MES_MES = "feclec_06_anterior";
	public static String FIELD_CONS_ULT_6MES_CONS_KWH = "kwhval_06_anterior";
	public static String FIELD_CONS_ULT_5MES_MES = "feclec_05_anterior";
	public static String FIELD_CONS_ULT_5MES_CONS_KWH = "kwhval_05_anterior";
	public static String FIELD_CONS_ULT_4MES_MES = "feclec_04_anterior";
	public static String FIELD_CONS_ULT_4MES_CONS_KWH = "kwhval_04_anterior";
	public static String FIELD_CONS_ULT_3MES_MES = "feclec_03_anterior";
	public static String FIELD_CONS_ULT_3MES_CONS_KWH = "kwhval_03_anterior";
	public static String FIELD_CONS_ULT_2MES_MES = "feclec_02_anterior";
	public static String FIELD_CONS_ULT_2MES_CONS_KWH = "kwhval_02_anterior";
	public static String FIELD_CONS_ULT_1MES_MES = "feclec_01_anterior";
	public static String FIELD_CONS_ULT_1MES_CONS_KWH = "kwhval_01_anterior";

	// Siglas debajo de Informacion de Interes
	public static String FIELD_COMPRA_ENE_REF = "compra_ene_ref"; // G
	public static String FIELD_TRANSMISIO_REF = "transmisio_ref"; // T
	public static String FIELD_DISTRIBUCI_REF = "distribuci_ref"; // D
	public static String FIELD_COMERCIALI_REF = "comerciali_ref"; // CV
	public static String FIELD_FRACCION_P_REF = "fraccion_p_ref"; // PR
	public static String FIELD_COSTOS_ASO_REF = "costos_aso_ref"; // R
	public static String FIELD_COSTO_UNITARIO_REFERENCIA = "costo_unitario_referencia"; // CU
	public static String FIELD_COSTO_UNITARIO_FIJO_REFERENCIA = "costo_unitario_fijo_referencia"; // CF
	public static String FIELD_GTDCVPRRCUCF = "GTDCVPRRCUCF"; // all you need is love
	

	// Ultimo campo FIJO
	// Se usa para ubicar el comienzo de la parte variable ...
	public static String FIELD_CANT_CARGOS = "cantidad_cargos";
	
	
	// CAMPOS VARIABLES (CARGOS)
	public static String FIELD_VAR_CARGO_CODIGO = "codigo_de_cargo";
	public static String FIELD_VAR_CARGO_CONCEPTO = "descripcion_de_cargo";
	public static String FIELD_VAR_CARGO_VALOR = "valor_facturado";
	public static String FIELD_VAR_TIPO_SERVICIO = "tipo_servicio";
	public static String FIELD_VAR_NUMERO_SERVICIO = "numero_servicio";
	public static String FIELD_VAR_DV_NUMERO_SERVICIO = "dv_numero_servicio";
	public static String FIELD_VAR_INDICADOR_CARGO = "indicador_cargo";
	public static String FIELD_VAR_CANTIDAD = "cantidad";
	public static String FIELD_VAR_ANTIGUEDAD = "antiguedad";
	public static String FIELD_VAR_INDICADOR_LECTURA = "indicador_lectura";
	public static String FIELD_VAR_LECT_ACTUAL = "lect_actual";
	public static String FIELD_VAR_LECT_ANTERIOR = "lect_anterior";
	public static String FIELD_VAR_DIFERENCIA_LECTURAS = "diferencia_lecturas";
	public static String FIELD_VAR_FACTOR = "factor";
	public static String FIELD_VAR_ACR = "acr";
	public static String FIELD_VAR_ENERGIA_FACTURADA = "energia_facturada";
	public static String FIELD_VAR_ENERGIA_CONSUMIDA = "energia_consumida";
	public static String FIELD_VAR_PRECIO_UNITARIO = "precio_unitario";
	
	public static String[] FIELDS_VARIABLES = { FIELD_VAR_CARGO_CODIGO, FIELD_VAR_CARGO_CONCEPTO,
		FIELD_VAR_CARGO_VALOR, FIELD_VAR_TIPO_SERVICIO, FIELD_VAR_NUMERO_SERVICIO, FIELD_VAR_DV_NUMERO_SERVICIO,
		FIELD_VAR_INDICADOR_CARGO, FIELD_VAR_CANTIDAD, FIELD_VAR_ANTIGUEDAD, FIELD_VAR_INDICADOR_LECTURA,
		FIELD_VAR_LECT_ACTUAL, FIELD_VAR_LECT_ANTERIOR, FIELD_VAR_DIFERENCIA_LECTURAS, FIELD_VAR_FACTOR, FIELD_VAR_ACR,
		FIELD_VAR_ENERGIA_FACTURADA, FIELD_VAR_ENERGIA_CONSUMIDA, FIELD_VAR_PRECIO_UNITARIO };
}
