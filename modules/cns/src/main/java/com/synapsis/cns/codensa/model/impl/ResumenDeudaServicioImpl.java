package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ResumenDeudaServicio;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ResumenDeudaServicioImpl extends SynergiaBusinessObjectImpl implements ResumenDeudaServicio {

	private Long nroCuenta; 
	private String deudaPagar; 
	private String deudaConvenida; 
	private String deudaTotal; 
	private String deudaEnergiaActual;   
	private String deudaServicioFinanciero;   
	private String deudaEncargoCobranza;   
	private String deudaDisputa; 
//	private String deudaCorte; 
	private Double antiguedadDeuda;
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ResumenDeudaServicio#getAntiguedadDeuda()
	 */
	public Double getAntiguedadDeuda() {
		return antiguedadDeuda;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ResumenDeudaServicio#setAntiguedadDeuda(java.lang.Double)
	 */
	public void setAntiguedadDeuda(Double antiguedadDeuda) {
		this.antiguedadDeuda = antiguedadDeuda;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ResumenDeudaServicio#getDeudaConvenida()
	 */
	public String getDeudaConvenida() {
		return deudaConvenida;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ResumenDeudaServicio#setDeudaConvenida(java.lang.Double)
	 */
	public void setDeudaConvenida(String deudaConvenida) {
		this.deudaConvenida = deudaConvenida;
	}
//	/* (non-Javadoc)
//	 * @see com.synapsis.cns.edelnor.model.impl.ResumenDeudaServicio#getDeudaCorte()
//	 */
//	public String getDeudaCorte() {
//		return deudaCorte;
//	}
//	/* (non-Javadoc)
//	 * @see com.synapsis.cns.edelnor.model.impl.ResumenDeudaServicio#setDeudaCorte(java.lang.Double)
//	 */
//	public void setDeudaCorte(String deudaCorte) {
//		this.deudaCorte = deudaCorte;
//	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ResumenDeudaServicio#getDeudaDisputa()
	 */
	public String getDeudaDisputa() {
		return deudaDisputa;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ResumenDeudaServicio#setDeudaDisputa(java.lang.Double)
	 */
	public void setDeudaDisputa(String deudaDisputa) {
		this.deudaDisputa = deudaDisputa;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ResumenDeudaServicio#getDeudaEncargoCobranza()
	 */
	public String getDeudaEncargoCobranza() {
		return deudaEncargoCobranza;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ResumenDeudaServicio#setDeudaEncargoCobranza(java.lang.Double)
	 */
	public void setDeudaEncargoCobranza(String deudaEncargoCobranza) {
		this.deudaEncargoCobranza = deudaEncargoCobranza;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ResumenDeudaServicio#getDeudaEnergiaActual()
	 */
	public String getDeudaEnergiaActual() {
		return deudaEnergiaActual;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ResumenDeudaServicio#setDeudaEnergiaActual(java.lang.Double)
	 */
	public void setDeudaEnergiaActual(String deudaEnergiaActual) {
		this.deudaEnergiaActual = deudaEnergiaActual;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ResumenDeudaServicio#getDeudaPagar()
	 */
	public String getDeudaPagar() {
		return deudaPagar;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ResumenDeudaServicio#setDeudaPagar(java.lang.Double)
	 */
	public void setDeudaPagar(String deudaPagar) {
		this.deudaPagar = deudaPagar;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ResumenDeudaServicio#getDeudaServicioFinanciero()
	 */
	public String getDeudaServicioFinanciero() {
		return deudaServicioFinanciero;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ResumenDeudaServicio#setDeudaServicioFinanciero(java.lang.Double)
	 */
	public void setDeudaServicioFinanciero(String deudaServicioFinanciero) {
		this.deudaServicioFinanciero = deudaServicioFinanciero;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ResumenDeudaServicio#getDeudaTotal()
	 */
	public String getDeudaTotal() {
		return deudaTotal;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ResumenDeudaServicio#setDeudaTotal(java.lang.Double)
	 */
	public void setDeudaTotal(String deudaTotal) {
		this.deudaTotal = deudaTotal;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ResumenDeudaServicio#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ResumenDeudaServicio#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	} 
}
