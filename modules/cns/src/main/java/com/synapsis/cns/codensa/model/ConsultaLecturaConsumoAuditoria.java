/**
 * 
 */
package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author dbraccio
 *
 *CU 006
 */
public interface ConsultaLecturaConsumoAuditoria extends SynergiaBusinessObject {

	public String getAnomaliaLecturaFinal() ;
	public String getAnomaliaLecturaInicial() ;
	public String getDireccionIP() ;
	public Date getFechaLecturaActual() ;
	public Date getFechaLecturaAnterior() ;
	public String getMarcaMedidor() ;
	public String getObservaciones() ;
	public String getTipoLecturaFinal();
	public String getTipoLecturaInicial();
	public String getUsuarioRealizaLectura();
	public Date getFechaHoraAnalisisLectura() ;
	public Double getLecturaActivaModificadaFP();
	public Double getLecturaActivaModificadaHP() ;
	public Double getLecturaActivaModificadaXP() ;
	public Double getLecturaActivaOriginalFP() ;
	public Double getLecturaActivaOriginalHP() ;
	public Double getLecturaActivaOriginalXP() ;
	public Double getLecturaReactivaModificadaFP();
	public Double getLecturaReactivaModificadaHP() ;
	public Double getLecturaReactivaModificadaXP() ;
	public Double getLecturaReactivaOriginalFP() ;
	public Double getLecturaReactivaOriginalHP() ;
	public Double getLecturaReactivaOriginalXP() ;
	public Long getIdCuenta() ;
	public Long getIdServicio() ;
	public Long getNroCuenta() ;
	public Long getNroServicio() ;
	public String getNumeroMedidor();
}
