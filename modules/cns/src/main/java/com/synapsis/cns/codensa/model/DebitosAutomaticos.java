package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * 
 * @author adambrosio
 */
public interface DebitosAutomaticos extends SynergiaBusinessObject{

	public Date getFechaActivacion();
	public void setFechaActivacion(Date fechaActivacion);
	public Date getFechaDesactivacion();
	public void setFechaDesactivacion(Date fechaDesactivacion);
	public Date getFechaInscripcionCuentaBancaria();
	public void setFechaInscripcionCuentaBancaria(Date fechaInscripcionCuentaBancaria);
	public String getNombreEntidadRecaudadora();
	public void setNombreEntidadRecaudadora(String nombreEntidadRecaudadora);
	public String getNombreEntidadRecaudadoraOrigen();
	public void setNombreEntidadRecaudadoraOrigen(String nombreEntidadRecaudadoraOrigen);
	public Long getNroCuentaBancaria();
	public void setNroCuentaBancaria(Long nroCuentaBancaria);
	public String getTipoCuentaBancaria();
	public void setTipoCuentaBancaria(String tipoCuentaBancaria);
	public Double getValorMaxDebito();
	public void setValorMaxDebito(Double valorMaxDebito);
	public Long getNroCuenta();
	public void setNroCuenta(Long nroCuenta);
}
