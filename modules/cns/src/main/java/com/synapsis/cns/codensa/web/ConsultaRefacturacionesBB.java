/**
 * $Id: ConsultaRefacturacionesBB.java,v 1.2 2009/05/19 15:46:12 ar29302553 Exp $
 */
package com.synapsis.cns.codensa.web;

import javax.faces.event.ActionEvent;

import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;
import com.suivant.arquitectura.presentation.model.ListableBB;
import com.suivant.arquitectura.presentation.utils.JSFUtils;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;

/**
 * @author Paola Attadio (se agrego seleccion en caso de invocación directa desde kendari)
 *
 */
public class ConsultaRefacturacionesBB extends ListableBB {
	
	private String uriRefa;
	
	private String uriSaldos;

	public ConsultaRefacturacionesBB (){
		EqualQueryFilter queryFilter = (EqualQueryFilter) VariableResolverUtils
		.getObject("numeroCuentaQueryFilter");
		Object parameter = JSFUtils.getParameter("nroCuenta");
		if (parameter != null) {
			Long nroCuentaaa = new Long(parameter.toString());
			queryFilter.setAttributeValue(nroCuentaaa);
		}
	}
	
	public String execKndImprimeRefacturacionUseCase(ActionEvent event) {
		ImprimeRefacturacionConsultaRefacturacionesBB bb = (ImprimeRefacturacionConsultaRefacturacionesBB) VariableResolverUtils
			.getObject("imprimeRefacturacionConsultaRefacturacionesBB");
		
		this.uriRefa = bb.execKndUseCase(event);
		this.uriSaldos = "";
				
		return this.uriRefa;
	}
	
	public String execKndImprimeSaldosEnDisputaUseCase(ActionEvent event) {
		ImprimeSaldosEnDisputaConsultaRefacturacionesBB bb = (ImprimeSaldosEnDisputaConsultaRefacturacionesBB) VariableResolverUtils
			.getObject("imprimeSaldosEnDisputaConsultaRefacturacionesBB");
	
		this.uriSaldos = bb.execKndUseCase(event);
		this.uriRefa = "";

		return this.uriSaldos;
	}

	/**
	 * @return the uriSaldos
	 */
	public String getUriSaldos() {
		return uriSaldos;
	}

	/**
	 * @param uriSaldos the uriSaldos to set
	 */
	public void setUriSaldos(String uriSaldos) {
		this.uriSaldos = uriSaldos;
	}

	/**
	 * @return the uriRefa
	 */
	public String getUriRefa() {
		return uriRefa;
	}

	/**
	 * @param uriRefa the uriRefa to set
	 */
	public void setUriRefa(String uriRefa) {
		this.uriRefa = uriRefa;
	}
}