package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface InformacionPago extends SynergiaBusinessObject{

	public Long getNroCuenta();
	
	public String getEntidadRecaudadora();

	public String getEstadoPago();

	public Date getFechaIngreso();

	public Date getFechaPagoEntidad();

	public String getMedioPago();

	public Long getNroDocumento();

	public String getProcedenciaPago();

	public String getSucursalRecaudo();

	public String getTipoDocumento();

	public String getValorPago();
	
	public String getValorPagoFormat();

	public void setNroCuenta(Long nroCuenta);
	
	public void setEntidadRecaudadora(String entidadRecaudadora);

	public void setEstadoPago(String estadoPago);

	public void setFechaIngreso(Date fechaIngreso);

	public void setFechaPagoEntidad(Date fechaPagoEntidad);

	public void setMedioPago(String medioPago);

	public void setNroDocumento(Long nroDocumento);

	public void setProcedenciaPago(String procedenciaPago);

	public void setSucursalRecaudo(String sucursalRecaudo);

	public void setTipoDocumento(String tipoDocumento);

	public void setValorPago(String valorPago);
	
	public void setValorPagoFormat(String valorPagoFormat);
	
	public Boolean getChequeDevuelto();

	public void setChequeDevuelto(Boolean chequeDevuelto);
	

}