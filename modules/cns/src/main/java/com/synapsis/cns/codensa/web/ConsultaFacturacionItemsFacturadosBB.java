/**
 * $Id: ConsultaFacturacionItemsFacturadosBB.java,v 1.1 2008/06/13 16:53:14 ar29261698 Exp $
 */
package com.synapsis.cns.codensa.web;

public class ConsultaFacturacionItemsFacturadosBB {
	
	private String detailName;
    private Long id;
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public String getDetailName() {
		return detailName;
	}

	/**
	 * 
	 * @param detailName
	 */
	public void setDetailName(String detailName) {
		this.detailName = detailName;
	}

}	
