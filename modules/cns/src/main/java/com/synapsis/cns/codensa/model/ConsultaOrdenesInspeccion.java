package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaOrdenesInspeccion extends SynergiaBusinessObject {
	
	public Long getIdOrden();
	
	public void setIdOrden(Long idOrden);
	
	public Long getNroCuenta();
	
	public void setNroCuenta(Long nroCuenta);
	
	public Long getNroServicio();
	
	public void setNroServicio(Long nroServicio);
	
	public String getClase();
	
	public void setClase(String clase);
	
	public String getSubClase();
	
	public void setSubClase(String subClase);
	
	public String getEstrato();
	
	public void setEstrato(String estrato);
	
	public String getNroOrden();
	
	public void setNroOrden(String nroOrden);
	
	public String getTipoInspeccion();
	
	public void setTipoInspeccion(String tipoInspeccion);
	
	public String getEstadoOrden();
	
	public void setEstadoOrden(String estadoOrden);
	
	public String getTipoResultado();
	
	public void setTipoResultado(String tipoResultado);
	
	public Date getFechaCreacion();
	
	public void setFechaCreacion(Date fechaCreacion);
	
	public Date getFechaFinEjecucion();
	
	public void setFechaFinEjecucion(Date fechaFinEjecucion);
	
	public Date getFechaInicioEjecucion();
	
	public void setFechaInicioEjecucion(Date fechaInicioEjecucion);
	
	public String getNroComponente();
	
	public void setNroComponente(String nroComponente);
	
	public Double getLecturaActivaFP();
	
	public void setLecturaActivaFP(Double lecturaActivaFP);
	
	public Double getLecturaActivaHP();
	
	public void setLecturaActivaHP(Double lecturaActivaHP);
	
	public Double getLecturaReactivaFP();
	
	public void setLecturaReactivaFP(Double lecturaReactivaFP);
	
	public Double getLecturaReactivaHP();
	
	public void setLecturaReactivaHP(Double lecturaReactivaHP);
	
	public String getObservacionesInspeccion();
	
	public void setObservacionesInspeccion(String observacionesInspeccion);

}