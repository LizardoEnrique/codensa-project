package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.DetalleECO;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class DetalleECOImpl extends SynergiaBusinessObjectImpl implements DetalleECO  {

    private static final long serialVersionUID = 1L;
    
	private String nombreTitular;
	private String ciclo;
	private Long valorCuota;
	private String socio;
	private Long saldoSocio;
	private Long numeroCuotasFacturadas;
	private Long numeroCuotasFacturar;
	private Long antiguedadDeuda;
	private Long diaMora;
	private Long saldoInteresesMora;
	private Long valorCuotaMora;

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleECO#getAntiguedadDeuda()
	 */
	public Long getAntiguedadDeuda() {
		return antiguedadDeuda;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleECO#setAntiguedadDeuda(java.lang.Long)
	 */
	public void setAntiguedadDeuda(Long antiguedadDeuda) {
		this.antiguedadDeuda = antiguedadDeuda;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleECO#getCiclo()
	 */
	public String getCiclo() {
		return ciclo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleECO#setCiclo(java.lang.String)
	 */
	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleECO#getDiaMora()
	 */
	public Long getDiaMora() {
		return diaMora;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleECO#setDiaMora(java.lang.Long)
	 */
	public void setDiaMora(Long diaMora) {
		this.diaMora = diaMora;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleECO#getNombreTitular()
	 */
	public String getNombreTitular() {
		return nombreTitular;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleECO#setNombreTitular(java.lang.String)
	 */
	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleECO#getNumeroCuotasFacturadas()
	 */
	public Long getNumeroCuotasFacturadas() {
		return numeroCuotasFacturadas;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleECO#setNumeroCuotasFacturadas(java.lang.Long)
	 */
	public void setNumeroCuotasFacturadas(Long numeroCuotasFacturadas) {
		this.numeroCuotasFacturadas = numeroCuotasFacturadas;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleECO#getNumeroCuotasFacturar()
	 */
	public Long getNumeroCuotasFacturar() {
		return numeroCuotasFacturar;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleECO#setNumeroCuotasFacturar(java.lang.Long)
	 */
	public void setNumeroCuotasFacturar(Long numeroCuotasFacturar) {
		this.numeroCuotasFacturar = numeroCuotasFacturar;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleECO#getSaldoInteresesMora()
	 */
	public Long getSaldoInteresesMora() {
		return saldoInteresesMora;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleECO#setSaldoInteresesMora(java.lang.Long)
	 */
	public void setSaldoInteresesMora(Long saldoInteresesMora) {
		this.saldoInteresesMora = saldoInteresesMora;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleECO#getSaldoSocio()
	 */
	public Long getSaldoSocio() {
		return saldoSocio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleECO#setSaldoSocio(java.lang.Long)
	 */
	public void setSaldoSocio(Long saldoSocio) {
		this.saldoSocio = saldoSocio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleECO#getSocio()
	 */
	public String getSocio() {
		return socio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleECO#setSocio(java.lang.String)
	 */
	public void setSocio(String socio) {
		this.socio = socio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleECO#getValorCuota()
	 */
	public Long getValorCuota() {
		return valorCuota;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleECO#setValorCuota(java.lang.Long)
	 */
	public void setValorCuota(Long valorCuota) {
		this.valorCuota = valorCuota;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleECO#getValorCuotaMora()
	 */
	public Long getValorCuotaMora() {
		return valorCuotaMora;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleECO#setValorCuotaMora(java.lang.Long)
	 */
	public void setValorCuotaMora(Long valorCuotaMora) {
		this.valorCuotaMora = valorCuotaMora;
	}
}
