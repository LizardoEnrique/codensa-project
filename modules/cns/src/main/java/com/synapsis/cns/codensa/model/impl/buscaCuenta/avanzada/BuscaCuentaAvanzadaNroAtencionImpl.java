/**
 * 
 */
package com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada;


import com.synapsis.cns.codensa.model.buscaCuenta.avanzada.BuscaCuentaAvanzadaNroAtencion;
import com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaImpl;

/**
 * @author Emiliano Arango (ar30557486)
 *
 */
public class BuscaCuentaAvanzadaNroAtencionImpl extends BuscaCuentaImpl implements BuscaCuentaAvanzadaNroAtencion {
	private Long nroAtencion;

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada.BuscaCuentaAvanzadaNroAtencion#getNroAtencion()
	 */
	public Long getNroAtencion() {
		return nroAtencion;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada.BuscaCuentaAvanzadaNroAtencion#setNroAtencion(java.lang.Long)
	 */
	public void setNroAtencion(Long nroAtencion) {
		this.nroAtencion = nroAtencion;
	}
	
}
