package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaCuentaDatosBasicos extends SynergiaBusinessObject {

	public Date getFechaLectura();

	public void setFechaLectura(Date fechaLectura);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the codigoAnomalia
	 */
	public String getCodigoAnomalia();

	/**
	 * @param codigoAnomalia the codigoAnomalia to set
	 */
	public void setCodigoAnomalia(String codigoAnomalia);

	/**
	 * @return the consumoMinimoSubsistencia
	 */
	public Long getConsumoMinimoSubsistencia();

	/**
	 * @param consumoMinimoSubsistencia the consumoMinimoSubsistencia to set
	 */
	public void setConsumoMinimoSubsistencia(Long consumoMinimoSubsistencia);

	/**
	 * @return the direccionRepartoEspecial
	 */
	public String getDireccionRepartoEspecial();

	/**
	 * @param direccionRepartoEspecial the direccionRepartoEspecial to set
	 */
	public void setDireccionRepartoEspecial(String direccionRepartoEspecial);

	/**
	 * @return the estratoSocioeconomico
	 */
	public String getEstratoSocioeconomico();

	/**
	 * @param estratoSocioeconomico the estratoSocioeconomico to set
	 */
	public void setEstratoSocioeconomico(String estratoSocioeconomico);

	/**
	 * @return the indicadorConsumoKW
	 */
	public String getIndicadorConsumoKW();

	/**
	 * @param indicadorConsumoKW the indicadorConsumoKW to set
	 */
	public void setIndicadorConsumoKW(String indicadorConsumoKW);

	/**
	 * @return the nivelTension
	 */
	public String getNivelTension();

	/**
	 * @param nivelTension the nivelTension to set
	 */
	public void setNivelTension(String nivelTension);

	/**
	 * @return the nroDocumento
	 */
	public Long getNroDocumento();

	/**
	 * @param nroDocumento the nroDocumento to set
	 */
	public void setNroDocumento(Long nroDocumento);

	/**
	 * @return the periodoFacturacion
	 */
	public Date getPeriodoFacturacion();

	/**
	 * @param periodoFacturacion the periodoFacturacion to set
	 */
	public void setPeriodoFacturacion(Date periodoFacturacion);

	/**
	 * @return the periodoLectura
	 */
	public String getPeriodoLectura();

	/**
	 * @param periodoLectura the periodoLectura to set
	 */
	public void setPeriodoLectura(String periodoLectura);

	/**
	 * @return the tarifa
	 */
	public String getTarifa();

	/**
	 * @param tarifa the tarifa to set
	 */
	public void setTarifa(String tarifa);

	/**
	 * @return the tipoCliente
	 */
	public String getTipoCliente();

	/**
	 * @param tipoCliente the tipoCliente to set
	 */
	public void setTipoCliente(String tipoCliente);

	/**
	 * @return the tipoLectura
	 */
	public String getTipoLectura();

	/**
	 * @param tipoLectura the tipoLectura to set
	 */
	public void setTipoLectura(String tipoLectura);

	public String getTipoLiquidacion();

	public void setTipoLiquidacion(String tipoLiquidacion);
	
	public String getTipoEsquemaFacturacion();

	public void setTipoEsquemaFacturacion(String tipoEsquemaFacturacion);
	
}