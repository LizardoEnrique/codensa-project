package com.synapsis.cns.codensa.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.myfaces.shared_impl.util.MessageUtils;

import com.suivant.arquitectura.core.exception.ExporterException;
import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;
import com.suivant.arquitectura.core.security.model.User;
import com.suivant.arquitectura.core.service.FinderService;
import com.suivant.arquitectura.presentation.common.ExportPdf;
import com.suivant.arquitectura.presentation.model.ViewBB;
import com.suivant.arquitectura.presentation.utils.ComboFactory;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.synapsis.cns.codensa.model.ReporteCertificaConvenio;
import com.synapsis.cns.codensa.model.impl.ConsultaConvenioImpl;
import com.synapsis.commons.faces.messages.ErrorsManager;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;

/**
 * 
 * @author ar30557486
 * 
 * Backing bean que me permite realizar la impresion del archivo pdf de certifica convenios.
 * 
 */
public class CertificaConvenioBB extends ViewBB {

	private Map reportMap;

	private Map reportConvenioActivoMap;

	private Map reportConvenioTerminadoMap;

	// datos de la pantalla necesarios para generar el archivo PDF
	private String nombreSolicitante;

	private String tipoDocumento;

	private String nroDocumento;

	private HtmlSelectOneMenu htmlTiposDocumento;

	// private static final String Service_Name = "aICContactoService";
	// private static final String Service_Method =
	// "generarContactoCertificaBuenCliente";

	// lista de cosas que me llegan de la pantallan anterior, se persiste x un
	// save state
	private Object valoresConvenio; // <ConsultaConvenio>

	private EqualQueryFilter queryFilterNroCuenta;

	private ConsultaConvenioImpl convenio;

	private String errorMessages;

	private String uri;

	public String execAction() {
		setValoresConvenio(convenio);
		return BackingBeansConstants.CERTIFICA_CONVENIO_EMITIR_PDF;
	}

	public void generatePdfAction() {
		if (this.isConvenioActivo() || this.isConvenioTerminado()) {
			queryFilterNroCuenta = (EqualQueryFilter) VariableResolverUtils.getObject("numeroCuentaQueryFilter");
			this.generateClientData();
			this.doGeneratePDF();
			// String response =
			// (String)NasServiceHelper.getResponse(Service_Name,
			// Service_Method,
			// generateWebServicesParameters());
		}
		else
			this.errorMessages = MessageUtils.getMessage("certificado.convenio.error", null).getDetail();
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(this.errorMessages));
	}

	// private Map generateWebServicesParameters() {
	// Map parameters = new HashMap();
	// parameters.put("cuenta", queryFilterNroCuenta.getAttributeValue());
	// parameters.put("nombreSolicitante", getNombreSolicitante());
	// parameters.put("tipoDocumento", getTipoDocumento());
	// parameters.put("nroDocumento", Long.valueOf(getNroDocumento()));
	// parameters.put("userName", obtainUserName());
	// return parameters;
	// }

	private String obtainUserName() {
		try {
			User user = SynergiaApplicationContext.getCurrentUser();
			return (user.getFirstname() + " " + user.getLastName());
		}
		catch (Exception e) {
			return "user_codensa";
		}
	}

	private void doGeneratePDF() {
		try {
			ExportPdf.generatePdf(this.generateDatosConvenios(), this.getReportMap());
		}
		catch (ExporterException e) {
			e.printStackTrace();
			ErrorsManager.addError(e.getMessage());
		}
	}

	private void generateClientData() {
		List out = ((FinderService) SynergiaApplicationContext.findService("certificaConvenioServiceFinder"))
			.findByCriteria(queryFilterNroCuenta);
		if (out.size() != 0) {
			// datos cabecera
			ReporteCertificaConvenio reporte = (ReporteCertificaConvenio) out.get(0);
			this.getReportMap().put("nombreSolicitante", this.getNombreSolicitante());
			this.getReportMap().put("tipoIdentificacion", this.getTipoDocumento());
			this.getReportMap().put("numeroIdentificacion", this.getNroDocumento());
			reporte.setFechaExpedicion(new Date());
			// reporte.setUsuarioSistema("usuario");
			this.getReportMap().put("nroCuenta", reporte.getNroCuenta());
			this.getReportMap().put("diaExpedicion", reporte.getDiaExpedicion().toString());
			this.getReportMap().put("mesExpedicion", reporte.getMesExpedicion().toString());
			this.getReportMap().put("anioExpedicion", reporte.getAnioExpedicion().toString());
			this.getReportMap().put("nombreTitularCuenta", reporte.getNombreTitularCuenta());
			this.getReportMap().put("apellidoTitularCuenta", reporte.getApellidoTitularCuenta());
			this.getReportMap().put("direccion", reporte.getDireccion());
			this.getReportMap().put("localizacion", reporte.getLocalizacion());
			this.getReportMap().put("municipio", reporte.getMunicipio());
			this.getReportMap().put("barrio", reporte.getBarrio());
			this.getReportMap().put("telefono", reporte.getTelefono());
			this.getReportMap().put("direccionRepartoEspecial", reporte.getDireccionRepartoEspecial());
			this.getReportMap().put("usuarioSistema", obtainUserName());

			StringBuffer buff = new StringBuffer("SERVICIO AL CLIENTE");
			if (SynergiaApplicationContext.getCurrentImplementationCompany().equals(SynergiaApplicationContext.CODENSA))
				buff.append(" DE CODENSA");

			this.getReportMap().put("texto", buff.toString());
		}
	}

	private List generateDatosConvenios() {
		List l = new ArrayList();
		l.add(this.getConvenio());
		return l;
	}

	public SelectItem[] getTipoDocumentoList() {
		return ComboFactory.makeCombo("comboTipoDocumentoServiceFinder", "comboQueryFilter", "descripcion", "codigo");
	}

	public String getNombreSolicitante() {
		return nombreSolicitante;
	}

	public void setNombreSolicitante(String nombreSolicitante) {
		this.nombreSolicitante = nombreSolicitante;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public HtmlSelectOneMenu getHtmlTiposDocumento() {
		return htmlTiposDocumento;
	}

	public void setHtmlTiposDocumento(HtmlSelectOneMenu htmlTiposDocumento) {
		this.htmlTiposDocumento = htmlTiposDocumento;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public Object getValoresConvenio() {
		return valoresConvenio;
	}

	public void setValoresConvenio(Object valoresConvenio) {
		this.valoresConvenio = valoresConvenio;
	}

	public Map getReportMap() {
		if (this.isConvenioActivo()) {
			this.reportMap = this.getReportConvenioActivoMap();
		}
		if (this.isConvenioTerminado()) {
			this.reportMap = this.getReportConvenioTerminadoMap();
		}
		return reportMap;
	}

	private boolean isConvenioTerminado() {
		return this.getConvenio().getEstado().equals("Terminado");
	}

	private boolean isConvenioActivo() {
		return this.getConvenio().getEstado().equals("Activo");
	}

	public void setReportMap(Map reportMap) {
		this.reportMap = reportMap;
	}

	public ConsultaConvenioImpl getConvenio() {
		return convenio;
	}

	public void setConvenio(ConsultaConvenioImpl convenio) {
		this.convenio = convenio;
	}

	public Map getReportConvenioActivoMap() {
		return reportConvenioActivoMap;
	}

	public void setReportConvenioActivoMap(Map reportConvenioActivoMap) {
		this.reportConvenioActivoMap = reportConvenioActivoMap;
	}

	public Map getReportConvenioTerminadoMap() {
		return reportConvenioTerminadoMap;
	}

	public void setReportConvenioTerminadoMap(Map reportConvenioTerminadoMap) {
		this.reportConvenioTerminadoMap = reportConvenioTerminadoMap;
	}

	public String getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(String errorMessages) {
		this.errorMessages = errorMessages;
	}

	public String execKndUseCase(ActionEvent event) {
		CUOrigenCertificaConveniosBB bb = (CUOrigenCertificaConveniosBB) VariableResolverUtils
			.getObject("cuOrigenCertificaConveniosBB");

		this.uri = bb.execKndUseCase(event);
		return this.uri;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}
}