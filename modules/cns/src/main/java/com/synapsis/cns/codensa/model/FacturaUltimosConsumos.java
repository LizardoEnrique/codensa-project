package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface FacturaUltimosConsumos extends SynergiaBusinessObject {

	public String getNroFactura();

	public void setNroFactura(String nroFactura);

	public String getMes();

	public void setMes(String mes);

	public Double getCons();

	public void setCons(Double cons);

}