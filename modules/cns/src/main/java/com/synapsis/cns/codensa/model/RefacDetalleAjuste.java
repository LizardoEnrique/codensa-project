package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface RefacDetalleAjuste extends SynergiaBusinessObject {

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public String getNroAjuste();

	public void setNroAjuste(String nroAjuste);

	public String getClasificacionAjuste();

	public void setClasificacionAjuste(String clasificacionAjuste);

	public String getAreaSolicitanteAjuste();

	public void setAreaSolicitanteAjuste(String areaSolicitanteAjuste);

	public String getAreaResponsableAjuste();

	public void setAreaResponsableAjuste(String areaResponsableAjuste);

	public String getSubmotivoAjuste();

	public void setSubmotivoAjuste(String submotivoAjuste);

	public Double getTiempoDesdeCreacionAjuste();

	public void setTiempoDesdeCreacionAjuste(Double tiempoDesdeCreacionAjuste);
	
	public Date getFechaCreacionAjuste() ;

	public void setFechaCreacionAjuste(Date fechaCreacionAjuste);
	

}