package com.synapsis.cns.codensa.model.combo;

public interface ConsultaLineasNegocioCCA {

	/**
	 * @return the descripcion
	 */
	public String getDescripcion();

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion);
	
	public Long getIdLineaNegocio();

	public void setIdLineaNegocio(Long idLineaNegocio);

}