package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface DeudaUltimoPago extends SynergiaBusinessObject {

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public Long getIdPago();

	public void setIdPago(Long idPago);

	public String getValorTotalUltimoPago();

	public void setValorTotalUltimoPago(String valorTotalUltimoPago);

	public String getMedioPago();

	public void setMedioPago(String medioPago);

	public String getNombreSucursalRecaudo();

	public void setNombreSucursalRecaudo(String nombreSucursalRecaudo);

	public String getEntidadRecaudadora();

	public void setEntidadRecaudadora(String entidadRecaudadora);

	public Date getFechaUltimoPago();

	public void setFechaUltimoPago(Date fechaUltimoPago);

}