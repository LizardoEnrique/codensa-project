package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.Empresa;
import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaMovimientosDisposicionLegalDetail extends SynergiaBusinessObject {

	public Long getCodigoItem();
	public void setCodigoItem(Long codigoItem);
	
	public Double getConsumoKW();	
	public void setConsumoKW(Double consumoKW);
	
	public String getDescItem();
	public void setDescItem(String descItem);
	
	public Empresa getEmpresa();
	public void setEmpresa(Empresa empresa);
	
	public Date getFecPeriodo();
	public void setFecPeriodo(Date fecPeriodo);
	
	public Long getId();
	public void setId(Long id);
	
	public Long getIdMovRefa();

	public void setIdMovRefa(Long idMovRefa);

	public String getMovimiento();

	public void setMovimiento(String movimiento);
	
	public Double getValorAfterAjuste();
	public void setValorAfterAjuste(Double valorAfterAjuste);
	
	public Double getValorAjuste();
	public void setValorAjuste(Double valorAjuste);
	
	public Double getValorBeforeAjuste();
	public void setValorBeforeAjuste(Double valorBeforeAjuste);

	
}
