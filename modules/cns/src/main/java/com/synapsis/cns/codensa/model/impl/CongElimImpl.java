package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.cns.codensa.model.CongElim;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class CongElimImpl extends SynergiaBusinessObjectImpl implements
		CongElim {

	private Long nroCuenta;
	
	private Long nroOperacion;

	private String state;

	private String motivo;
	
	private String usrEliminadorCong;

	private Date fechaEliminacion;

	/**
	 * @return the nroOperacion
	 */
	public Long getNroOperacion() {
		return nroOperacion;
	}

	/**
	 * @param nroOperacion the nroOperacion to set
	 */
	public void setNroOperacion(Long nroOperacion) {
		this.nroOperacion = nroOperacion;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the motivo
	 */
	public String getMotivo() {
		return motivo;
	}

	/**
	 * @param motivo the motivo to set
	 */
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	/**
	 * @return the fechaEliminacion
	 */
	public Date getFechaEliminacion() {
		return fechaEliminacion;
	}

	/**
	 * @param fechaEliminacion the fechaEliminacion to set
	 */
	public void setFechaEliminacion(Date fechaEliminacion) {
		this.fechaEliminacion = fechaEliminacion;
	}

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	
	/**
	 * 
	 * @return el usuario que elimino la congelación 
	 */
	public String getUsrEliminadorCong() {
		return usrEliminadorCong;
	}

	public void setUsrEliminadorCong(String usrEliminadorCong) {
		this.usrEliminadorCong = usrEliminadorCong;
	}


}