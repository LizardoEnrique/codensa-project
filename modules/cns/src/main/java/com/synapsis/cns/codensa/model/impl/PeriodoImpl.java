/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.Periodo;
import com.synapsis.commons.types.datetime.CalendarDate;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 *
 */
public class PeriodoImpl extends SynergiaBusinessObjectImpl implements Periodo {
	
	private static final long serialVersionUID = 1L;
	
	private Long idPeriodo;
	private Long idConfigFact;
	private Long idAgrupCiclo;
	private CalendarDate fechaProceso;
	private String mes;
	private String anio;
	private String periodoFacturacion;
	private Long cli;
	
	private Long nroCuenta;
	
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.Periodo#getAnio()
	 */
	public String getAnio() {
		return anio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.Periodo#setAnio(java.lang.String)
	 */
	public void setAnio(String anio) {
		this.anio = anio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.Periodo#getIdAgrupCiclo()
	 */
	public Long getIdAgrupCiclo() {
		return idAgrupCiclo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.Periodo#setIdAgrupCiclo(java.lang.Long)
	 */
	public void setIdAgrupCiclo(Long idAgrupCiclo) {
		this.idAgrupCiclo = idAgrupCiclo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.Periodo#getIdConfigFact()
	 */
	public Long getIdConfigFact() {
		return idConfigFact;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.Periodo#setIdConfigFact(java.lang.Long)
	 */
	public void setIdConfigFact(Long idConfigFact) {
		this.idConfigFact = idConfigFact;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.Periodo#getIdPeriodo()
	 */
	public Long getIdPeriodo() {
		return idPeriodo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.Periodo#setIdPeriodo(java.lang.Long)
	 */
	public void setIdPeriodo(Long idPeriodo) {
		this.idPeriodo = idPeriodo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.Periodo#getMes()
	 */
	public String getMes() {
		return mes;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.Periodo#setMes(java.lang.String)
	 */
	public void setMes(String mes) {
		this.mes = mes;
	}
	/**
	 * @return the fechaProceso
	 */
	public CalendarDate getFechaProceso() {
		return fechaProceso;
	}
	/**
	 * @param fechaProceso the fechaProceso to set
	 */
	public void setFechaProceso(CalendarDate fechaProceso) {
		this.fechaProceso = fechaProceso;
	}
	
	public String getPeriodoFacturacion() {
		return periodoFacturacion;
	}

	public void setPeriodoFacturacion(String periodoFacturacion) {
		this.periodoFacturacion = periodoFacturacion;
	}
	/**
	 * @return the cli
	 */
	public Long getCli() {
		return cli;
	}
	public void setCli(Long cli) {
		this.cli = cli;
	}
}