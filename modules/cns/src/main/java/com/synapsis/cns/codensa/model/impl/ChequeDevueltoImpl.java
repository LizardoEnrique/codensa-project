package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.ChequeDevuelto;
/**
 * 
 * @author Emiliano Arango (ar30557486)
 *
 */
public class ChequeDevueltoImpl extends SynergiaBusinessObjectImpl 
	implements ChequeDevuelto {
	
	private Long idPago;
	private String nroCheque;
	private String girador;
	private Double valor;
	private Date fechaDevolucion;
	private String causa;
	private String devolucion;
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsutalHistoricoPagosChequeDevuelto#getCausa()
	 */
	public String getCausa() {
		return causa;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsutalHistoricoPagosChequeDevuelto#getDevolucion()
	 */
	public String getDevolucion() {
		return devolucion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsutalHistoricoPagosChequeDevuelto#getFechaDevolucion()
	 */
	public Date getFechaDevolucion() {
		return fechaDevolucion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsutalHistoricoPagosChequeDevuelto#getGirador()
	 */
	public String getGirador() {
		return girador;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsutalHistoricoPagosChequeDevuelto#setCausa(java.lang.String)
	 */
	public void setCausa(String causa) {
		this.causa = causa;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsutalHistoricoPagosChequeDevuelto#setDevolucion(java.lang.String)
	 */
	public void setDevolucion(String devolucion) {
		this.devolucion = devolucion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsutalHistoricoPagosChequeDevuelto#setFechaDevolucion(java.lang.String)
	 */
	public void setFechaDevolucion(Date fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsutalHistoricoPagosChequeDevuelto#setGirador(java.lang.String)
	 */
	public void setGirador(String girador) {
		this.girador = girador;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsutalHistoricoPagosChequeDevuelto#setNroCheque(java.lang.Long)
	 */
	public Long getIdPago() {
		return idPago;
	}
	public void setIdPago(Long idPago) {
		this.idPago = idPago;
	}
	/**
	 * @return the nroCheque
	 */
	public String getNroCheque() {
		return this.nroCheque;
	}
	/**
	 * @param nroCheque the nroCheque to set
	 */
	public void setNroCheque(String nroCheque) {
		this.nroCheque = nroCheque;
	}
	/**
	 * @return the valor
	 */
	public Double getValor() {
		return this.valor;
	}
	/**
	 * @param valor the valor to set
	 */
	public void setValor(Double valor) {
		this.valor = valor;
	}
}