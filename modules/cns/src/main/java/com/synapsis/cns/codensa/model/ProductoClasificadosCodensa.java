package com.synapsis.cns.codensa.model;

import java.util.Set;

/**
 * Interfaz del producto financiero Clasificados Codensa, para el reporte de duplicado de anexo de factura (Servicios Financieros)
 *    
 * @author jhack
 */
public interface ProductoClasificadosCodensa extends ServicioFinancieroECO{
	public Set getItemsClasificadosCodensa();
	public void setItemsClasificadosCodensa(Set itemsClasificadosCodensa);
}
