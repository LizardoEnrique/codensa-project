package com.synapsis.cns.codensa.model.impl;
/**
 * Clase que wrapea el detalle de los cargos que se muestran en el reporte 
 * creaDuplicadoFactura, COD-CU-CNS003
 * 
 * @author m3.MarioRoss - 22/11/2006
 */
public class DetalleCuenta
{
	private String codigo;
	private String concepto;
	private Double valor;
	
	public DetalleCuenta(String codigo, String concepto, Double valor)
	{
		this.codigo = codigo;
		this.concepto = concepto;
		this.valor = valor;
	}
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
}