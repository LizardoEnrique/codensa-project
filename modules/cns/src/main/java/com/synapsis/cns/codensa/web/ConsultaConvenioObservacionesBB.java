package com.synapsis.cns.codensa.web;

import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.model.BusinessObject;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.service.FinderService;
import com.suivant.arquitectura.presentation.model.ViewBB;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;



public class ConsultaConvenioObservacionesBB extends ViewBB {
	/*Observaciones Creacion*/
	BusinessObject businessObject1;
	/*Observaciones Aprobacion*/
	BusinessObject businessObject2;
	/*Observaciones Estado Convenio*/
	BusinessObject businessObject3;

	String serviceNameObservaciones;
	
	private QueryFilter filterObsCreacion;
	private QueryFilter filterObsAprobacion;
	private QueryFilter filterObsEstadoConvenio;
	
	public void searchBusiness()  {
		try {
			setBusinessObject1(getService(getServiceNameObservaciones()
					).findByCriteriaUnique(getFilterObsCreacion()));
		}catch (ObjectNotFoundException e) {
			//ErrorsManager.addInfoMessage(e.getMessage());
		}
		try {
			setBusinessObject2(getService(getServiceNameObservaciones()
					).findByCriteriaUnique(getFilterObsAprobacion()));
		}catch (ObjectNotFoundException e) {
			//ErrorsManager.addInfoMessage(e.getMessage());
		}
		try {
			setBusinessObject3(getService(getServiceNameObservaciones()
					).findByCriteriaUnique(getFilterObsEstadoConvenio()));
		}catch (ObjectNotFoundException e) {
			//ErrorsManager.addInfoMessage(e.getMessage());
		}
	}
	
	private FinderService getService(String service){
		return (FinderService)SynergiaApplicationContext.findService(
				service);
	}

	public BusinessObject getBusinessObject2() {
		return businessObject2;
	}

	public void setBusinessObject2(BusinessObject businessObject2) {
		this.businessObject2 = businessObject2;
	}

	public BusinessObject getBusinessObject1() {
		return businessObject1;
	}

	public void setBusinessObject1(BusinessObject businessObject1) {
		this.businessObject1 = businessObject1;
	}

	public BusinessObject getBusinessObject3() {
		return businessObject3;
	}

	public void setBusinessObject3(BusinessObject businessObject3) {
		this.businessObject3 = businessObject3;
	}


	/**
	 * @return the filterObsAprobacion
	 */
	public QueryFilter getFilterObsAprobacion() {
		return filterObsAprobacion;
	}

	/**
	 * @param filterObsAprobacion the filterObsAprobacion to set
	 */
	public void setFilterObsAprobacion(QueryFilter filterObsAprobacion) {
		this.filterObsAprobacion = filterObsAprobacion;
	}

	/**
	 * @return the filterObsCreacion
	 */
	public QueryFilter getFilterObsCreacion() {
		return filterObsCreacion;
	}

	/**
	 * @param filterObsCreacion the filterObsCreacion to set
	 */
	public void setFilterObsCreacion(QueryFilter filterObsCreacion) {
		this.filterObsCreacion = filterObsCreacion;
	}

	/**
	 * @return the filterObsEstadoConvenio
	 */
	public QueryFilter getFilterObsEstadoConvenio() {
		return filterObsEstadoConvenio;
	}

	/**
	 * @param filterObsEstadoConvenio the filterObsEstadoConvenio to set
	 */
	public void setFilterObsEstadoConvenio(QueryFilter filterObsEstadoConvenio) {
		this.filterObsEstadoConvenio = filterObsEstadoConvenio;
	}

	/**
	 * @return the serviceNameObservaciones
	 */
	public String getServiceNameObservaciones() {
		return serviceNameObservaciones;
	}

	/**
	 * @param serviceNameObservaciones the serviceNameObservaciones to set
	 */
	public void setServiceNameObservaciones(String serviceNameObservaciones) {
		this.serviceNameObservaciones = serviceNameObservaciones;
	}

	
}
