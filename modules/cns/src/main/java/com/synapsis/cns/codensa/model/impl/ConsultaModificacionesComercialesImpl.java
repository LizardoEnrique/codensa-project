package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.suivant.arquitectura.core.model.impl.BusinessObjectImpl;
import com.synapsis.cns.codensa.model.ConsultaModificacionesComerciales;
import com.synapsis.synergia.core.model.Empresa;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaModificacionesComercialesImpl extends SynergiaBusinessObjectImpl implements ConsultaModificacionesComerciales {

	private String datoAnterior;
	private String datoNuevo;
	private String direccionIP;
	private String usuarioModificador;
	private String tipoModificacion;
	private Date fechaModificacion;
	private Long numeroServicioElectrico;
	private Long numeroModificacion; //Averiguar: Este numero de modificacion bien podr�a ser el ID del objeto no?
	private Long numeroCuenta;
	private String observaciones;
	
	public String getDatoAnterior() {
		return datoAnterior;
	}
	public void setDatoAnterior(String datoAnterior) {
		this.datoAnterior = datoAnterior;
	}
	public String getDatoNuevo() {
		return this.datoNuevo;
	}
	public void setDatoNuevo(String datoNuevo) {
		this.datoNuevo = datoNuevo;
	}
	public String getDireccionIP() {
		return this.direccionIP;
	}
	public void setDireccionIP(String direccionIP) {
		this.direccionIP = direccionIP;
	}
	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	public Long getNumeroServicioElectrico() {
		return this.numeroServicioElectrico;
	}
	public void setNumeroServicioElectrico(Long numeroServicioElectrico) {
		this.numeroServicioElectrico = numeroServicioElectrico;
	}
	public String getUsuarioModificador() {
		return this.usuarioModificador;
	}
	public void setUsuarioModificador(String usuarioModificador) {
		this.usuarioModificador = usuarioModificador;
	}
	public Long getNumeroModificacion() {
		return this.numeroModificacion;
	}
	public void setNumeroModificacion(Long numeroModificacion) {
		this.numeroModificacion = numeroModificacion;
	}

	public String getTipoModificacion() {
		return this.tipoModificacion;
	}
	public void setTipoModificacion(String tipoModificacion) {
		this.tipoModificacion = tipoModificacion;
	}
	public Long getNumeroCuenta() {
		return this.numeroCuenta;
	}
	public void setNumeroCuenta(Long numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}


}
