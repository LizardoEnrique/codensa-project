package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


public interface ConsultaFacturacionDetalleFinalizacion extends SynergiaBusinessObject {

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getFechaAlta()
	 */
	public Date getFechaAlta();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setFechaAlta(java.util.Date)
	 */
	public void setFechaAlta(Date fechaAlta);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getFechaFacturacion()
	 */
	public Date getFechaFacturacion();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setFechaFacturacion(java.util.Date)
	 */
	public void setFechaFacturacion(Date fechaFacturacion);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getIdentificacion()
	 */
	public String getIdentificacion();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setIdentificacion(java.lang.String)
	 */
	public void setIdentificacion(String identificacion);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getInteresCuota()
	 */
	public Double getInteresCuota();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setInteresCuota(java.lang.Integer)
	 */
	public void setInteresCuota(Double interesCuota);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getInteresMora()
	 */
	public Double getInteresMora();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setInteresMora(java.lang.Integer)
	 */
	public void setInteresMora(Double interesMora);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getItemsFacturados()
	 */
	public String getItemsFacturados();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setItemsFacturados(java.lang.String)
	 */
	public void setItemsFacturados(String itemsFacturados);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getNombreCliente()
	 */
	public String getNombreCliente();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setNombreCliente(java.lang.String)
	 */
	public void setNombreCliente(String nombreCliente);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getNumeroCuotas()
	 */
	public Integer getNumeroCuotas();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setNumeroCuotas(java.lang.Integer)
	 */
	public void setNumeroCuotas(Integer numeroCuotas);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getNumeroDocumento()
	 */
	public String getNumeroDocumento();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setNumeroDocumento(java.lang.Integer)
	 */
	public void setNumeroDocumento(String numeroDocumento);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getNumeroEncargoConvenio()
	 */
	public Long getNumeroEncargoConvenio();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setNumeroEncargoConvenio(java.lang.Integer)
	 */
	public void setNumeroEncargoConvenio(Long numeroEncargoConvenio);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getPeriodo()
	 */
	public String getPeriodo();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setPeriodo(java.util.Date)
	 */
	public void setPeriodo(String periodo);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getTasaInteresConvenio()
	 */
	public Double getTasaInteresConvenio();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setTasaInteresConvenio(java.lang.Integer)
	 */
	public void setTasaInteresConvenio(Double tasaInteresConvenio);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getValorCuota()
	 */
	public Double getValorCuota();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setValorCuota(java.lang.Integer)
	 */
	public void setValorCuota(Double valorCuota);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#getVecesFacturado()
	 */
	public Integer getVecesFacturado();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionDetalleFinalizacio#setVecesFacturado(java.lang.Integer)
	 */
	public void setVecesFacturado(Integer vecesFacturado);
	
	public Integer getCuotasPendientes();

	/**
	 * @param cuotasPendientes the cuotasPendientes to set
	 */
	public void setCuotasPendientes(Integer cuotasPendientes);	
	
	/**
	 * @return the tipoServicio
	 */
	public String getTipoServicio();

	/**
	 * @param tipoServicio the tipoServicio to set
	 */
	public void setTipoServicio(String tipoServicio);
	
	/**
	 * @return the fechaCreacion
	 */
	public Date getFechaCreacion();

	/**
	 * @param fechaCreacion the fechaCreacion to set
	 */
	public void setFechaCreacion(Date fechaCreacion);	
}