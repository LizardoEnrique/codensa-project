package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


/**
 * @author m3.MarioRoss - 14/11/2006
 * refactor dBraccio	- 29/11/2006
 */
public interface EventosComponentes extends SynergiaBusinessObject {

	public String getClaseComponente();

	public void setClaseComponente(String claseComponente);

	public Date getFechaInstalacion();

	public void setFechaInstalacion(Date fechaInstalacion);

	public Date getFechaRetiro();

	public void setFechaRetiro(Date fechaRetiro);

	public Long getIdComponente();

	public void setIdComponente(Long idComponente);

	public String getMarcaComponente();

	public void setMarcaComponente(String marcaComponente);

	public String getModeloComponente();

	public void setModeloComponente(String modeloComponente);

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public String getNumeroComponente();

	public void setNumeroComponente(String numeroComponente);

	public String getRelacionTransformacion();

	public void setRelacionTransformacion(String relacionTransformacion);

	public String getTipoComponente();

	public void setTipoComponente(String tipoComponente);
	
	public Long getIdMedidor();

	public void setIdMedidor(Long idMedidor);	

}