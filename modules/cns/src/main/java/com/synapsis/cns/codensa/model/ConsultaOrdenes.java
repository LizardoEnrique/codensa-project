package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author m3.MarioRoss - 14/11/2006
 */
public interface ConsultaOrdenes extends SynergiaBusinessObject {

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

    public Long getNumeroServicio();

    public void setNumeroServicio(Long numeroServicio);

    public String getTipoServicio();

    public void setTipoServicio(String tipoServicio);
    
	public String getAreaResponsable();

	public void setAreaResponsable(String areaResponsable);

	public String getEstadoOrden();

	public void setEstadoOrden(String estadoOrden);

	public Date getFechaFinalizacion();

	public void setFechaFinalizacion(Date fechaFinalizacion);

	public Date getFechaGeneracion();

	public void setFechaGeneracion(Date fechaGeneracion);

	public Date getFechaVencimientoInterna();

	public void setFechaVencimientoInterna(Date fechaVencimientoInterna);

	public Date getFechaVencimientoLegal();

	public void setFechaVencimientoLegal(Date fechaVencimientoLegal);

	public String getNumeroOrden();

	public void setNumeroOrden(String numeroOrden);

	public String getObservaciones();

	public void setObservaciones(String observaciones);

	public String getTipoOrden();

	public void setTipoOrden(String tipoOrden);

	public String getUsuarioFinalizador();

	public void setUsuarioFinalizador(String usuarioFinalizador);

	public String getNombreUsuarioFinalizador();

	public void setNombreUsuarioFinalizador(String nombreUsuarioFinalizador);

	public String getUsuarioGenerador();

	public void setUsuarioGenerador(String usuarioGenerador);	

	public String getNombreUsuarioGenerador();

	public void setNombreUsuarioGenerador(String nombreUsuarioGenerador);	
	
	public String getCodArea() ;

	public void setCodArea(String codArea) ;

	public Long getDiasHabilesDif() ;

	public void setDiasHabilesDif(Long diasHabilesDif) ;
	
	public String getCodigoOrden();
	
	public void setCodigoOrden(String codigoOrden);	
	
}