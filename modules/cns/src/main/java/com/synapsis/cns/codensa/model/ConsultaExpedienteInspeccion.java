package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaExpedienteInspeccion extends SynergiaBusinessObject {

	/**
	 * @return the anomaliasEncontradas
	 */
	public abstract String getAnomaliasEncontradas();

	/**
	 * @param anomaliasEncontradas the anomaliasEncontradas to set
	 */
	public abstract void setAnomaliasEncontradas(String anomaliasEncontradas);

	/**
	 * @return the calidadVisita
	 */
	public abstract String getCalidadVisita();

	/**
	 * @param calidadVisita the calidadVisita to set
	 */
	public abstract void setCalidadVisita(String calidadVisita);

	/**
	 * @return the fechaDeteccion
	 */
	public abstract Date getFechaDeteccion();

	/**
	 * @param fechaDeteccion the fechaDeteccion to set
	 */
	public abstract void setFechaDeteccion(Date fechaDeteccion);

	/**
	 * @return the nombreApellidoVisita
	 */
	public abstract String getNombreApellidoVisita();

	/**
	 * @param nombreApellidoVisita the nombreApellidoVisita to set
	 */
	public abstract void setNombreApellidoVisita(String nombreApellidoVisita);

	public abstract Long getNumeroOrdenInspeccion();

	/**
	 * @param numeroOrdenInspeccion the numeroOrdenInspeccion to set
	 */
	public abstract void setNumeroOrdenInspeccion(Long numeroOrdenInspeccion);

	/**
	 * @return the observaciones
	 */
	public abstract String getObservaciones();

	/**
	 * @param observaciones the observaciones to set
	 */
	public abstract void setObservaciones(String observaciones);

	/**
	 * @return the tipoDocumentoVisita
	 */
	public abstract String getTipoDocumentoVisita();

	/**
	 * @param tipoDocumentoVisita the tipoDocumentoVisita to set
	 */
	public abstract void setTipoDocumentoVisita(String tipoDocumentoVisita);

	public String getNumeroDocumentoVisita();
	public void setNumeroDocumentoVisita(String numeroDocumentoVisita);
	
	public Long getNumeroExpediente();
	
	public void setNumeroExpediente(Long numeroExpediente);
	
}