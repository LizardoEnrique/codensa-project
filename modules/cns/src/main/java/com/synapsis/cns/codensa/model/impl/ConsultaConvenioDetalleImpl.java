/**
 * $Id: ConsultaConvenioDetalleImpl.java,v 1.3 2008/01/31 22:22:24 ar26557682 Exp $
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaConvenioDetalle;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaConvenioDetalleImpl  extends SynergiaBusinessObjectImpl implements ConsultaConvenioDetalle {
	
	private static final long serialVersionUID = 1L;
	
	private String codCargo;
	private String descripcionCargo;	
	private Date periodoFacturacionCargo;	
	private String antiguedadCargo;	
	private Double valorCargo;	
	private String tipoDocumento;	
	private Date fechaDocumento;	
	private Long numDocumento;	
	private Date fechaFactura;
	private Long nroCuenta;
	private Long nroConvenio;
	
	public Long getNroConvenio() {
		return nroConvenio;
	}

	public void setNroConvenio(Long nroConvenio) {
		this.nroConvenio = nroConvenio;
	}
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioDetalleDeudaInicial#getAntiguedadCargo()
	 */
	public String getAntiguedadCargo() {
		return antiguedadCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioDetalleDeudaInicial#setAntiguedadCargo(java.lang.String)
	 */
	public void setAntiguedadCargo(String antiguedadCargo) {
		this.antiguedadCargo = antiguedadCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioDetalleDeudaInicial#getCodCargo()
	 */
	public String getCodCargo() {
		return codCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioDetalleDeudaInicial#setCodCargo(java.lang.String)
	 */
	public void setCodCargo(String codCargo) {
		this.codCargo = codCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioDetalleDeudaInicial#getDescripCargo()
	 */
	public String getDescripcionCargo() {
		return descripcionCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioDetalleDeudaInicial#setDescripCargo(java.lang.String)
	 */
	public void setDescripcionCargo(String descripCargo) {
		this.descripcionCargo = descripCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioDetalleDeudaInicial#getFechaDocumento()
	 */
	public Date getFechaDocumento() {
		return fechaDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioDetalleDeudaInicial#setFechaDocumento(java.util.Date)
	 */
	public void setFechaDocumento(Date fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioDetalleDeudaInicial#getFechaFactura()
	 */
	public Date getFechaFactura() {
		return fechaFactura;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioDetalleDeudaInicial#setFechaFactura(java.util.Date)
	 */
	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioDetalleDeudaInicial#getPeriodoFacturacionCargo()
	 */
	public Date getPeriodoFacturacionCargo() {
		return periodoFacturacionCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioDetalleDeudaInicial#setPeriodoFacturacionCargo(java.lang.String)
	 */
	public void setPeriodoFacturacionCargo(Date periodoFacturacionCargo) {
		this.periodoFacturacionCargo = periodoFacturacionCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioDetalleDeudaInicial#getTipoDocumento()
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioDetalleDeudaInicial#setTipoDocumento(java.lang.String)
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public Long getNumDocumento() {
		return numDocumento;
	}
	public void setNumDocumento(Long numDocumento) {
		this.numDocumento = numDocumento;
	}
	public Double getValorCargo() {
		return valorCargo;
	}
	public void setValorCargo(Double valorCargo) {
		this.valorCargo = valorCargo;
	}

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
}
