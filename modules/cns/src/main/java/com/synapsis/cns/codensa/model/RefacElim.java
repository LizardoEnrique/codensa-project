package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface RefacElim extends SynergiaBusinessObject {

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public String getNroAjuste();

	public void setNroAjuste(String nroAjuste);

	public Long getNroOrdenRefa();

	public void setNroOrdenRefa(Long nroOrdenRefa);

	public Long getServAjustados();

	public void setServAjustados(Long servAjustados);

	public Double getValorTotalAjustado();

	public void setValorTotalAjustado(Double valorTotalAjustado);

	public Double getCantKwhAjust();

	public void setCantKwhAjust(Double cantKwhAjust);

	public Double getValorTotalPagar();

	public void setValorTotalPagar(Double valorTotalPagar);

	public Date getFecHorRealizAjuste();

	public void setFecHorRealizAjuste(Date fecHorRealizAjuste);

	public String getUsuarioRealizAjuste();

	public void setUsuarioRealizAjuste(String usuarioRealizAjuste);

	public String getNombreUsuarioRealizAjuste();

	public void setNombreUsuarioRealizAjuste(String nombreUsuarioRealizAjuste);

	public Date getFecHorElimAjuste();

	public void setFecHorElimAjuste(Date fecHorElimAjuste);

	public String getUsuarioElimAjuste();

	public void setUsuarioElimAjuste(String usuarioElimAjuste);

	public String getNombreUsuarioElimAjuste();

	public void setNombreUsuarioElimAjuste(String nombreUsuarioElimAjuste);

	public String getObservacionesElim();
	
	public String getTipoOperacion() ;

	public void setTipoOperacion(String tipoOperacion) ;


	public void setObservacionesElim(String observacionesElim);

}