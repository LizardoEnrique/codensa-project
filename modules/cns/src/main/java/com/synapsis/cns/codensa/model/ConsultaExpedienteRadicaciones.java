package com.synapsis.cns.codensa.model;

public interface ConsultaExpedienteRadicaciones {

	/**
	 * @return the notificaciones
	 */
	public  String getNotificaciones();

	/**
	 * @param notificaciones the notificaciones to set
	 */
	public  void setNotificaciones(String notificaciones);

	/**
	 * @return the radicaciones
	 */
	public  String getRadicaciones();

	/**
	 * @param radicaciones the radicaciones to set
	 */
	public  void setRadicaciones(String radicaciones);

	/**
	 * @return the numeroExpediente
	 */
	public  Long getNumeroExpediente();

	/**
	 * @param numeroExpediente the numeroExpediente to set
	 */
	public  void setNumeroExpediente(Long numeroExpediente);

}