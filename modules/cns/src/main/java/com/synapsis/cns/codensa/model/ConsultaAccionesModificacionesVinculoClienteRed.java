package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaAccionesModificacionesVinculoClienteRed extends SynergiaBusinessObject {

	public String getFecCambio();
	public void setFecCambio(String fecCambio);
	public String getFormaActual();
	public void setFormaActual(String formaActual);
	public String getIpCambio();
	public void setIpCambio(String ipCambio);
	public String getTipoAccion();
	public void setTipoAccion(String tipoAccion);
	public String getTransformador();
	public void setTransformador(String transformador);
	public String getUsuarioSolic();
	public void setUsuarioSolic(String usuarioSolic);
	
}
