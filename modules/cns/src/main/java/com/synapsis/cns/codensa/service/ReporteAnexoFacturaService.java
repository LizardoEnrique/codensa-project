package com.synapsis.cns.codensa.service;

import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.queryFilter.CompositeQueryFilter;
import com.synapsis.cns.codensa.model.ReporteAnexoFactura;

/**
 * @author jhack
 */
public interface ReporteAnexoFacturaService {

	public ReporteAnexoFactura getReporteAnexoFactura(CompositeQueryFilter compositeQueryFilter)throws ObjectNotFoundException;
}
