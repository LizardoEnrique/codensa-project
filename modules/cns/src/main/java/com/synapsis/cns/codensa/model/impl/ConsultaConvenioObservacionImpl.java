/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaConvenioObservacion;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 *
 */
public class ConsultaConvenioObservacionImpl extends SynergiaBusinessObjectImpl implements ConsultaConvenioObservacion {
	
	private Long nroConvenio;
	private Long tipoObservacion;
	private String descripcionObservacion; 
	private Long nroCuenta;
	
	
	
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public String getDescripcionObservacion() {
		return descripcionObservacion;
	}
	public void setDescripcionObservacion(String descripcionObservacion) {
		this.descripcionObservacion = descripcionObservacion;
	}
	public Long getNroConvenio() {
		return nroConvenio;
	}
	public void setNroConvenio(Long nroConvenio) {
		this.nroConvenio = nroConvenio;
	}
	public Long getTipoObservacion() {
		return tipoObservacion;
	}
	public void setTipoObservacion(Long tipoObservacion) {
		this.tipoObservacion = tipoObservacion;
	}

}