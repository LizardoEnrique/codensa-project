package com.synapsis.cns.codensa.model.impl;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaHistorialEstadoConvenioEstadosServicio extends SynergiaBusinessObject {

	/**
	 * @return the estados
	 */
	public String getEstados();

	/**
	 * @param estados the estados to set
	 */
	public void setEstados(String estados);

	/**
	 * @return the fechasQuedaronEstado
	 */
	public String getFechasQuedaronEstado();

	/**
	 * @param fechasQuedaronEstado the fechasQuedaronEstado to set
	 */
	public void setFechasQuedaronEstado(String fechasQuedaronEstado);

	/**
	 * @return the usuariosIngresaronEstado
	 */
	public String getUsuariosIngresaronEstado();

	/**
	 * @param usuariosIngresaronEstado the usuariosIngresaronEstado to set
	 */
	public void setUsuariosIngresaronEstado(String usuariosIngresaronEstado);

}