package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.cns.codensa.model.DeudaUltimoPago;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class DeudaUltimoPagoImpl extends SynergiaBusinessObjectImpl implements
		DeudaUltimoPago {

	private Long nroCuenta;

	private Long idPago;

	private String valorTotalUltimoPago;

	private String medioPago;

	private String nombreSucursalRecaudo;

	private String entidadRecaudadora;

	private Date fechaUltimoPago;

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public Long getIdPago() {
		return idPago;
	}

	public void setIdPago(Long idPago) {
		this.idPago = idPago;
	}

	public String getValorTotalUltimoPago() {
		return valorTotalUltimoPago;
	}

	public void setValorTotalUltimoPago(String valorTotalUltimoPago) {
		this.valorTotalUltimoPago = valorTotalUltimoPago;
	}

	public String getMedioPago() {
		return medioPago;
	}

	public void setMedioPago(String medioPago) {
		this.medioPago = medioPago;
	}

	public String getNombreSucursalRecaudo() {
		return nombreSucursalRecaudo;
	}

	public void setNombreSucursalRecaudo(String nombreSucursalRecaudo) {
		this.nombreSucursalRecaudo = nombreSucursalRecaudo;
	}

	public String getEntidadRecaudadora() {
		return entidadRecaudadora;
	}

	public void setEntidadRecaudadora(String entidadRecaudadora) {
		this.entidadRecaudadora = entidadRecaudadora;
	}

	public Date getFechaUltimoPago() {
		return fechaUltimoPago;
	}

	public void setFechaUltimoPago(Date fechaUltimoPago) {
		this.fechaUltimoPago = fechaUltimoPago;
	}
}