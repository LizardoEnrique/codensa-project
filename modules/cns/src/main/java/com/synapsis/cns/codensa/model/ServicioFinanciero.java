package com.synapsis.cns.codensa.model;

import java.util.Set;

/**
 * Interfaz del servicio financiero, para el reporte de duplicado de anexo de factura (Servicios Financieros)
 *    
 * @author jhack
 */
public interface ServicioFinanciero {

	public String getNombre();
	public void setNombre(String nombre);
}
