package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface CongDetalleFecUsr extends SynergiaBusinessObject {

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroSaldo the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the usuario
	 */
	public String getUsuario();

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario);

	/**
	 * @return the usuarioNombre
	 */
	public String getUsuarioNombre();

	/**
	 * @param usuarioNombre the usuarioNombre to set
	 */
	public void setUsuarioNombre(String usuarioNombre);

	/**
	 * @return the fechaIntervencion
	 */
	public Date getFechaIntervencion();

	/**
	 * @param fechaIntervencion the fechaIntervencion to set
	 */
	public void setFechaIntervencion(Date fechaIntervencion);

	/**
	 * @return the horaIntervencion
	 */
	public String getHoraIntervencion() ;

	/**
	 * @param horaIntervencion the horaIntervencion to set
	 */
	public void setHoraIntervencion(String horaIntervencion);

	/**
	 * @return the estado
	 */
	public String getEstado() ;

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado);

	/**
	 * @return the tiempoIntervencionUsuario
	 */
	public String getTiempoIntervencionUsuario();

	/**
	 * @param tiempoIntervencionUsuario the tiempoIntervencionUsuario to set
	 */
	public void setTiempoIntervencionUsuario(String tiempoIntervencionUsuario);

	/**
	 * @return the observacion
	 */
	public String getObservacion();

	/**
	 * @param observacion the observacion to set
	 */
	public void setObservacion(String observacion);
	
	/**
	 * @return the nroSaldo
	 */
	public Long getNroSaldo();

	/**
	 * @param nroSaldo the nroSaldo to set
	 */
	public void setNroSaldo(Long nroSaldo);
	
	
	
	public Long getIdOrden();

	public void setIdOrden(Long idOrden);

}