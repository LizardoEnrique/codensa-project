package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface CargosManuales {

	/**
	 * @return the codigoCargo
	 */
	public String getCodigoCargo();

	/**
	 * @param codigoCargo the codigoCargo to set
	 */
	public void setCodigoCargo(String codigoCargo);

	/**
	 * @return the descripcionCargo
	 */
	public String getDescripcionCargo();

	/**
	 * @param descripcionCargo the descripcionCargo to set
	 */
	public void setDescripcionCargo(String descripcionCargo);

	/**
	 * @return the estadoFactura
	 */
	public String getEstadoFactura();

	/**
	 * @param estadoFactura the estadoFactura to set
	 */
	public void setEstadoFactura(String estadoFactura);

	/**
	 * @return the fechaDesarrolloActividadTerreno
	 */
	public Date getFechaDesarrolloActividadTerreno();

	/**
	 * @param fechaDesarrolloActividadTerreno the fechaDesarrolloActividadTerreno to set
	 */
	public void setFechaDesarrolloActividadTerreno(
			Date fechaDesarrolloActividadTerreno);

	/**
	 * @return the fechaIngresoCargo
	 */
	public Date getFechaIngresoCargo();

	/**
	 * @param fechaIngresoCargo the fechaIngresoCargo to set
	 */
	public void setFechaIngresoCargo(Date fechaIngresoCargo);

	/**
	 * @return the nombreIngresoCargo
	 */
	public String getNombreIngresoCargo();

	/**
	 * @param nombreIngresoCargo the nombreIngresoCargo to set
	 */
	public void setNombreIngresoCargo(String nombreIngresoCargo);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroDocumentoIngresoCargo
	 */
	public String getNroDocumentoIngresoCargo();

	/**
	 * @param nroDocumentoIngresoCargo the nroDocumentoIngresoCargo to set
	 */
	public void setNroDocumentoIngresoCargo(String nroDocumentoIngresoCargo);

	/**
	 * @return the nroServicioCargo
	 */
	public String getNroServicioCargo();

	/**
	 * @param nroServicioCargo the nroServicioCargo to set
	 */
	public void setNroServicioCargo(String nroServicioCargo);

	/**
	 * @return the observaciones
	 */
	public String getObservaciones();

	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones);

	/**
	 * @return the origenCargo
	 */
	public String getOrigenCargo();

	/**
	 * @param origenCargo the origenCargo to set
	 */
	public void setOrigenCargo(String origenCargo);

	/**
	 * @return the tipoServicioCargo
	 */
	public String getTipoServicioCargo();

	/**
	 * @param tipoServicioCargo the tipoServicioCargo to set
	 */
	public void setTipoServicioCargo(String tipoServicioCargo);

	/**
	 * @return the usuarioIngresoCargo
	 */
	public String getUsuarioIngresoCargo();

	/**
	 * @param usuarioIngresoCargo the usuarioIngresoCargo to set
	 */
	public void setUsuarioIngresoCargo(String usuarioIngresoCargo);

	/**
	 * @return the valorCargo
	 */
	public String getValorCargo();

	/**
	 * @param valorCargo the valorCargo to set
	 */
	public void setValorCargo(String valorCargo);

}