package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import com.synapsis.cns.codensa.model.CongDetalleSaldos;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class CongDetalleSaldosImpl extends SynergiaBusinessObjectImpl implements
		CongDetalleSaldos {

	private Long nroCuenta;
	
	private Long nroSaldo;

	private Double saldoTotal;

	private Double valorEnergiaKvar;
	
	private Double energiaSd;
	
	private Double saldoOtrosNeg;

	private Double saldoEnergDespuesCong;
	
	private Double valorEnergiaKwh;

    private Long valorDisputado;
    
    private Double saldoInicialCuentaDesc;
    
    private Double saldoFinalCuentaDesc;
    
    private Double valorDescongelado;
    
    private Double saldoInicialServDesc;
    
    private Double saldoFinalServDesc;
    
    

	/**
	 * @return the saldoInicialCuentaDesc
	 */
	public Double getSaldoInicialCuentaDesc() {
		return saldoInicialCuentaDesc;
	}

	/**
	 * @param saldoInicialCuentaDesc the saldoInicialCuentaDesc to set
	 */
	public void setSaldoInicialCuentaDesc(Double saldoInicialCuentaDesc) {
		this.saldoInicialCuentaDesc = saldoInicialCuentaDesc;
	}

	/**
	 * @return the saldoFinalCuentaDesc
	 */
	public Double getSaldoFinalCuentaDesc() {
		return saldoFinalCuentaDesc;
	}

	/**
	 * @param saldoFinalCuentaDesc the saldoFinalCuentaDesc to set
	 */
	public void setSaldoFinalCuentaDesc(Double saldoFinalCuentaDesc) {
		this.saldoFinalCuentaDesc = saldoFinalCuentaDesc;
	}

	/**
	 * @return the valorDescongelado
	 */
	public Double getValorDescongelado() {
		return valorDescongelado;
	}

	/**
	 * @param valorDescongelado the valorDescongelado to set
	 */
	public void setValorDescongelado(Double valorDescongelado) {
		this.valorDescongelado = valorDescongelado;
	}

	/**
	 * @return the saldoInicialServDesc
	 */
	public Double getSaldoInicialServDesc() {
		return saldoInicialServDesc;
	}

	/**
	 * @param saldoInicialServDesc the saldoInicialServDesc to set
	 */
	public void setSaldoInicialServDesc(Double saldoInicialServDesc) {
		this.saldoInicialServDesc = saldoInicialServDesc;
	}

	/**
	 * @return the saldoFinalServDesc
	 */
	public Double getSaldoFinalServDesc() {
		return saldoFinalServDesc;
	}

	/**
	 * @param saldoFinalServDesc the saldoFinalServDesc to set
	 */
	public void setSaldoFinalServDesc(Double saldoFinalServDesc) {
		this.saldoFinalServDesc = saldoFinalServDesc;
	}

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	/**
	 * @return the saldoTotal
	 */
	public Double getSaldoTotal() {
		return saldoTotal;
	}

	/**
	 * @param saldoTotal the saldoTotal to set
	 */
	public void setSaldoTotal(Double saldoTotal) {
		this.saldoTotal = saldoTotal;
	}

	/**
	 * @return the valorEnergiaKvar
	 */
	public Double getValorEnergiaKvar() {
		return valorEnergiaKvar;
	}

	/**
	 * @param valorEnergiaKvar the valorEnergiaKvar to set
	 */
	public void setValorEnergiaKvar(Double valorEnergiaKvar) {
		this.valorEnergiaKvar = valorEnergiaKvar;
	}

	/**
	 * @return the energiaSd
	 */
	public Double getEnergiaSd() {
		return energiaSd;
	}

	/**
	 * @param energiaSd the energiaSd to set
	 */
	public void setEnergiaSd(Double energiaSd) {
		this.energiaSd = energiaSd;
	}

	/**
	 * @return the saldoOtrosNeg
	 */
	public Double getSaldoOtrosNeg() {
		return saldoOtrosNeg;
	}

	/**
	 * @param saldoOtrosNeg the saldoOtrosNeg to set
	 */
	public void setSaldoOtrosNeg(Double saldoOtrosNeg) {
		this.saldoOtrosNeg = saldoOtrosNeg;
	}

	/**
	 * @return the saldoEnergDespuesCong
	 */
	public Double getSaldoEnergDespuesCong() {
		return saldoEnergDespuesCong;
	}

	/**
	 * @param saldoEnergDespuesCong the saldoEnergDespuesCong to set
	 */
	public void setSaldoEnergDespuesCong(Double saldoEnergDespuesCong) {
		this.saldoEnergDespuesCong = saldoEnergDespuesCong;
	}

	/**
	 * @return the valorEnergiaKwh
	 */
	public Double getValorEnergiaKwh() {
		return valorEnergiaKwh;
	}

	/**
	 * @param valorEnergiaKwh the valorEnergiaKwh to set
	 */
	public void setValorEnergiaKwh(Double valorEnergiaKwh) {
		this.valorEnergiaKwh = valorEnergiaKwh;
	}

	/**
	 * @return the valorDisputado
	 */
	public Long getValorDisputado() {
		return valorDisputado;
	}

	/**
	 * @param valorDisputado the valorDisputado to set
	 */
	public void setValorDisputado(Long valorDisputado) {
		this.valorDisputado = valorDisputado;
	}

	/**
	 * @return the nroSaldo
	 */
	public Long getNroSaldo() {
		return nroSaldo;
	}

	/**
	 * @param nroSaldo the nroSaldo to set
	 */
	public void setNroSaldo(Long nroSaldo) {
		this.nroSaldo = nroSaldo;
	}

	
}