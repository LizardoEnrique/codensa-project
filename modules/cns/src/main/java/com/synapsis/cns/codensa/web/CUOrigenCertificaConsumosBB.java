package com.synapsis.cns.codensa.web;

/**
 * @author ccamba
 * 
 */
public class CUOrigenCertificaConsumosBB extends
		GeneracionAutomaticaContactoKendariBB {

	protected String getCasoUsoOrigen() {
		return this.getParamEncryptedAndEncoded("CNS033");
	}

}
