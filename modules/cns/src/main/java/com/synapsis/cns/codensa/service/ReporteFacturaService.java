package com.synapsis.cns.codensa.service;

import java.util.Date;

import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.queryFilter.CompositeQueryFilter;
import com.synapsis.cns.codensa.model.ReporteFactura;
import com.synapsis.cns.codensa.model.ReporteFacturaDuplicadoLegal;

/**
 * @author jhack
 */
public interface ReporteFacturaService {

	public ReporteFacturaDuplicadoLegal getReporteFactura(CompositeQueryFilter compositeQueryFilter) throws ObjectNotFoundException;
	
	/**
	 * Devuelve un array de 4 Doubles, con los siguientes valores (en este orden):
	 * <li>NUMERO DE INTERRUPCIONES (FES)
	 * <li>HORAS INTERRUMPIDAS (DES)
	 * <li>NUMERO DE INTERRUPCIONES(VMFES)
	 * <li>HORAS INTERRUNPIDAS(VMDES)
	 * 
	 * @param fechaFactura
	 * @param nroCuenta
	 * @param reporteFactura
	 * @return
	 */
	public Double[] getInterrupciones(Date fechaFactura, Long nroCuenta, ReporteFactura reporteFactura);
	
	public ReporteFacturaDuplicadoLegal getReporteFacturaForTest();
}
