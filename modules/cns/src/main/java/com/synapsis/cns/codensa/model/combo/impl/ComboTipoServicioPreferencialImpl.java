package com.synapsis.cns.codensa.model.combo.impl;

import com.synapsis.cns.codensa.model.combo.Combo;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ComboTipoServicioPreferencialImpl extends SynergiaBusinessObjectImpl implements Combo {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String codigo;
	private String descripcion;
	
	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return this.codigo;
	}
	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
