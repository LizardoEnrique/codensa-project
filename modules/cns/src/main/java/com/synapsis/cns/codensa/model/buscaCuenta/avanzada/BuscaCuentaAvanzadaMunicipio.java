package com.synapsis.cns.codensa.model.buscaCuenta.avanzada;

public interface BuscaCuentaAvanzadaMunicipio {

	/**
	 * @return the ciclo
	 */
	public Long getCiclo();
	/**
	 * @param ciclo the ciclo to set
	 */
	public void setCiclo(Long ciclo);
	/**
	 * @return the departamento
	 */
	public Long getDepartamento();
	/**
	 * @param departamento the departamento to set
	 */
	public void setDepartamento(Long departamento);
	/**
	 * @return the grupo
	 */
	public Long getGrupo();
	/**
	 * @param grupo the grupo to set
	 */
	public void setGrupo(Long grupo);
	/**
	 * @return the idManzana
	 */
	public Long getIdManzana();
	/**
	 * @param idManzana the idManzana to set
	 */
	public void setIdManzana(Long idManzana);
	/**
	 * @return the idMunicipio
	 */
	public Long getIdMunicipio();
	/**
	 * @param idMunicipio the idMunicipio to set
	 */
	public void setIdMunicipio(Long idMunicipio);
	/**
	 * @return the manzana
	 */
	public String getManzana();
	/**
	 * @param manzana the manzana to set
	 */
	public void setManzana(String manzana);
	/**
	 * @return the municipio
	 */
	public String getMunicipio();
	/**
	 * @param municipio the municipio to set
	 */
	public void setMunicipio(String municipio);
	/**
	 * @return the sucursal
	 */
	public Long getSucursal();
	/**
	 * @param sucursal the sucursal to set
	 */
	public void setSucursal(Long sucursal);
	/**
	 * @return the zona
	 */
	public Long getZona();
	/**
	 * @param zona the zona to set
	 */
	public void setZona(Long zona);	
	
	public String getCorrelativo() ;
	
	public void setCorrelativo(String correlativo) ;


}