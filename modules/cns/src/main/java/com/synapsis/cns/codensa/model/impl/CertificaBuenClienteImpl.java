/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.CertificaBuenCliente;

/**
 * @author ar30557486
 * 
 */
public class CertificaBuenClienteImpl extends SynergiaBusinessObjectImpl implements	CertificaBuenCliente {

	private Long nroCuenta;

	private Long idCuenta;

	private Boolean certifica; // CERTIFICA VARCHAR2(4000 Bytes)

	private Boolean deuda;// DEUDA VARCHAR2(4000 Bytes)

	private Boolean expedientes; //	presenta expedientes

	private Boolean habitos;// habitos de pago

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.CertificaBuenCliente#getCertifica()
	 */
	public Boolean getCertifica() {
		return certifica;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.CertificaBuenCliente#setCertifica(java.lang.String)
	 */
	public void setCertifica(Boolean certifica) {
		this.certifica = certifica;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.CertificaBuenCliente#getDeuda()
	 */
	public Boolean getDeuda() {
		return deuda;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.CertificaBuenCliente#setDeuda(java.lang.String)
	 */
	public void setDeuda(Boolean deuda) {
		this.deuda = deuda;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.CertificaBuenCliente#getIdCuenta()
	 */
	public Long getIdCuenta() {
		return idCuenta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.CertificaBuenCliente#setIdCuenta(java.lang.Long)
	 */
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.CertificaBuenCliente#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.CertificaBuenCliente#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	/**
	 * @return the expedientes
	 */
	public Boolean getExpedientes() {
		return expedientes;
	}

	/**
	 * @param expedientes the expedientes to set
	 */
	public void setExpedientes(Boolean expedientes) {
		this.expedientes = expedientes;
	}

	/**
	 * @return the habitos
	 */
	public Boolean getHabitos() {
		return habitos;
	}

	/**
	 * @param habitos the habitos to set
	 */
	public void setHabitos(Boolean habitos) {
		this.habitos = habitos;
	}
}