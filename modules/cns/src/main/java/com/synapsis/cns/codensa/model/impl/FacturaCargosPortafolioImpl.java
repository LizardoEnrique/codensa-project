package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import com.synapsis.cns.codensa.model.FacturaCargosPortafolio;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class FacturaCargosPortafolioImpl extends SynergiaBusinessObjectImpl
		implements FacturaCargosPortafolio {

	private String nroFactura;

	private String codigo;

	private String concepto;

	public String getNroFactura() {
		return nroFactura;
	}

	public void setNroFactura(String nroFactura) {
		this.nroFactura = nroFactura;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
}