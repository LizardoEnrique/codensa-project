package com.synapsis.cns.codensa.model.buscaCuenta;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface TipoNumeroServicioCuenta extends SynergiaBusinessObject {

	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio();

	/**
	 * @return the tipoServicio
	 */
	public String getTipoServicio();

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();
	
	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio);

	/**
	 * @param tipoServicio the tipoServicio to set
	 */
	public void setTipoServicio(String tipoServicio);
	
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);
	

}