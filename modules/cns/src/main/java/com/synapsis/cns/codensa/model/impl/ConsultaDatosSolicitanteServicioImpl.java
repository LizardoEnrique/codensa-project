/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaDatosSolicitanteServicio;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaDatosSolicitanteServicioImpl extends
		SynergiaBusinessObjectImpl implements ConsultaDatosSolicitanteServicio {
	   private Long nroServicio;           
	   private String nroSolicitante;      
	   private String nombreSolicitante;   
	   private String resultadoEstudio;    
	   private Long montoAprobado;         
	   private Long montoDisponible;       
	   private String fechaSolicitudCred;  
	   private String causalNegacion;      
	   private String estrato;             
	   private Double cicloFacturacion;    
	   private Long nroCuenta;
	/**
	 * @return the causalNegacion
	 */
	public String getCausalNegacion() {
		return causalNegacion;
	}
	/**
	 * @param causalNegacion the causalNegacion to set
	 */
	public void setCausalNegacion(String causalNegacion) {
		this.causalNegacion = causalNegacion;
	}
	/**
	 * @return the cicloFacturacion
	 */
	public Double getCicloFacturacion() {
		return cicloFacturacion;
	}
	/**
	 * @param cicloFacturacion the cicloFacturacion to set
	 */
	public void setCicloFacturacion(Double cicloFacturacion) {
		this.cicloFacturacion = cicloFacturacion;
	}
	/**
	 * @return the estrato
	 */
	public String getEstrato() {
		return estrato;
	}
	/**
	 * @param estrato the estrato to set
	 */
	public void setEstrato(String estrato) {
		this.estrato = estrato;
	}
	/**
	 * @return the fechaSolicitudCred
	 */
	public String getFechaSolicitudCred() {
		return fechaSolicitudCred;
	}
	/**
	 * @param fechaSolicitudCred the fechaSolicitudCred to set
	 */
	public void setFechaSolicitudCred(String fechaSolicitudCred) {
		this.fechaSolicitudCred = fechaSolicitudCred;
	}
	/**
	 * @return the montoAprobado
	 */
	public Long getMontoAprobado() {
		return montoAprobado;
	}
	/**
	 * @param montoAprobado the montoAprobado to set
	 */
	public void setMontoAprobado(Long montoAprobado) {
		this.montoAprobado = montoAprobado;
	}
	/**
	 * @return the montoDisponible
	 */
	public Long getMontoDisponible() {
		return montoDisponible;
	}
	/**
	 * @param montoDisponible the montoDisponible to set
	 */
	public void setMontoDisponible(Long montoDisponible) {
		this.montoDisponible = montoDisponible;
	}
	/**
	 * @return the nombreSolicitante
	 */
	public String getNombreSolicitante() {
		return nombreSolicitante;
	}
	/**
	 * @param nombreSolicitante the nombreSolicitante to set
	 */
	public void setNombreSolicitante(String nombreSolicitante) {
		this.nombreSolicitante = nombreSolicitante;
	}
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	/**
	 * @return the nroSolicitante
	 */
	public String getNroSolicitante() {
		return nroSolicitante;
	}
	/**
	 * @param nroSolicitante the nroSolicitante to set
	 */
	public void setNroSolicitante(String nroSolicitante) {
		this.nroSolicitante = nroSolicitante;
	}
	/**
	 * @return the resultadoEstudio
	 */
	public String getResultadoEstudio() {
		return resultadoEstudio;
	}
	/**
	 * @param resultadoEstudio the resultadoEstudio to set
	 */
	public void setResultadoEstudio(String resultadoEstudio) {
		this.resultadoEstudio = resultadoEstudio;
	}             
}
