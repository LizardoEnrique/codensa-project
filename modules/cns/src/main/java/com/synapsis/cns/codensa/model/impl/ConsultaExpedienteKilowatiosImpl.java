package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaExpedienteKilowatios;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaExpedienteKilowatiosImpl extends
		SynergiaBusinessObjectImpl implements ConsultaExpedienteKilowatios {

	private Double total;
	private Double promedio;
	private String reintegrar;
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteKilowatios#getPromedio()
	 */
	public Double getPromedio() {
		return promedio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteKilowatios#setPromedio(java.lang.Double)
	 */
	public void setPromedio(Double promedio) {
		this.promedio = promedio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteKilowatios#getReintegrar()
	 */
	public String getReintegrar() {
		return reintegrar;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteKilowatios#setReintegrar(java.lang.String)
	 */
	public void setReintegrar(String reintegrar) {
		this.reintegrar = reintegrar;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteKilowatios#getTotal()
	 */
	public Double getTotal() {
		return total;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteKilowatios#setTotal(java.lang.Double)
	 */
	public void setTotal(Double total) {
		this.total = total;
	}
	


}
