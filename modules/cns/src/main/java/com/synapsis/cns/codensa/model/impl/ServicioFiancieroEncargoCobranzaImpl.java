/**
 * $Id: ServicioFiancieroEncargoCobranzaImpl.java,v 1.1 2008/02/26 12:14:41 ar26557682 Exp $
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ServicioFiancieroEncargoCobranza;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ServicioFiancieroEncargoCobranzaImpl extends SynergiaBusinessObjectImpl 
	implements ServicioFiancieroEncargoCobranza {

	
	private static final long serialVersionUID = 1L;
	
	private Long nroCuenta;
	private Long nroServicio;
	private String descTipoServicio;
	private String estadoCobranzaServicio;
	private String titularServicio;
	private String tipoNumeroDocSolicitante;
	private Date fechaServicio;
	private Date fechaVencimientoServicio;
	
	public String getDescTipoServicio() {
		return descTipoServicio;
	}
	public void setDescTipoServicio(String descTipoServicio) {
		this.descTipoServicio = descTipoServicio;
	}
	public String getEstadoCobranzaServicio() {
		return estadoCobranzaServicio;
	}
	public void setEstadoCobranzaServicio(String estadoCobranzaServicio) {
		this.estadoCobranzaServicio = estadoCobranzaServicio;
	}
	public Date getFechaServicio() {
		return fechaServicio;
	}
	public void setFechaServicio(Date fechaServicio) {
		this.fechaServicio = fechaServicio;
	}
	public Date getFechaVencimientoServicio() {
		return fechaVencimientoServicio;
	}
	public void setFechaVencimientoServicio(Date fechaVencimientoServicio) {
		this.fechaVencimientoServicio = fechaVencimientoServicio;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public Long getNroServicio() {
		return nroServicio;
	}
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	public String getTipoNumeroDocSolicitante() {
		return tipoNumeroDocSolicitante;
	}
	public void setTipoNumeroDocSolicitante(String tipoNumeroDocSolicitante) {
		this.tipoNumeroDocSolicitante = tipoNumeroDocSolicitante;
	}
	public String getTitularServicio() {
		return titularServicio;
	}
	public void setTitularServicio(String titularServicio) {
		this.titularServicio = titularServicio;
	}
}
