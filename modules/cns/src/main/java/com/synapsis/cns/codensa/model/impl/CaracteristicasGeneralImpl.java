package com.synapsis.cns.codensa.model.impl;
import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.CaracteristicasGeneral;

// default package
// Generated by MyEclipse - Hibernate Tools



/**
 * CaracteristicasGeneral generated by MyEclipse - Hibernate Tools
 */
public class CaracteristicasGeneralImpl extends SynergiaBusinessObjectImpl 
	implements CaracteristicasGeneral{

//	 Fields    


    private Long nroCuenta;
    private Long nroServicio;
    private String nivelTension;
    private String descripcionVoltaje;
    private String circuito;
    private String puntoTransformacion;
    private Date fechaConexion;
    private String numeroOrden;
    private String tarifa;
    
    
	public String getCircuito() {
		return circuito;
	}
	public void setCircuito(String circuito) {
		this.circuito = circuito;
	}
	public String getDescripcionVoltaje() {
		return descripcionVoltaje;
	}
	public void setDescripcionVoltaje(String descripcionVoltaje) {
		this.descripcionVoltaje = descripcionVoltaje;
	}
	public Date getFechaConexion() {
		return fechaConexion;
	}
	public void setFechaConexion(Date fechaConexion) {
		this.fechaConexion = fechaConexion;
	}
	public String getNivelTension() {
		return nivelTension;
	}
	public void setNivelTension(String nivelTension) {
		this.nivelTension = nivelTension;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public Long getNroServicio() {
		return nroServicio;
	}
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	public String getNumeroOrden() {
		return numeroOrden;
	}
	public void setNumeroOrden(String numeroOrden) {
		this.numeroOrden = numeroOrden;
	}
	public String getPuntoTransformacion() {
		return puntoTransformacion;
	}
	public void setPuntoTransformacion(String puntoTransformacion) {
		this.puntoTransformacion = puntoTransformacion;
	}
	public String getTarifa() {
		return tarifa;
	}
	public void setTarifa(String tarifa) {
		this.tarifa = tarifa;
	}
    
    
}
