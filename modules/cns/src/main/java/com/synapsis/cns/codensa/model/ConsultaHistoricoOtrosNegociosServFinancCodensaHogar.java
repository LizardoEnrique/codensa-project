package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * Representa la información de Servicios Financieros Codensa Hogar que se muestra de la Consulta Histórico
 * Otros Negocios. Tiene los campos que se quitaron de ConsultaServFinancCodensaHogar por el error de filas
 * repetidas en las grillas
 * 
 * @author egrande
 * 
 */
public interface ConsultaHistoricoOtrosNegociosServFinancCodensaHogar extends SynergiaBusinessObject {
	/**
	 * @return the fechaActivProd
	 */
	public Date getFechaActivProd();

	/**
	 * @param fechaActivProd the fechaActivProd to set
	 */
	public void setFechaActivProd(Date fechaActivProd);

	/**
	 * @return the fechaDesactProd
	 */
	public Date getFechaDesactProd();

	/**
	 * @param fechaDesactProd the fechaDesactProd to set
	 */
	public void setFechaDesactProd(Date fechaDesactProd);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio();

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio);

	/**
	 * @return the valorOrigSrv
	 */
	public Double getValorOrigSrv();

	/**
	 * @param valorOrigSrv the valorOrigSrv to set
	 */
	public void setValorOrigSrv(Double valorOrigSrv);

	/**
	 * @return the nroCuotasFac
	 */
	public Long getNroCuotasFac();

	/**
	 * @param nroCuotasFac the nroCuotasFac to set
	 */
	public void setNroCuotasFac(Long nroCuotasFac);

	/**
	 * @return the nroCuotasFaltantes
	 */
	public Long getNroCuotasFaltantes();

	/**
	 * @param nroCuotasFaltantes the nroCuotasFaltantes to set
	 */
	public void setNroCuotasFaltantes(Long nroCuotasFaltantes);

	/**
	 * @return the nroCuotasOri
	 */
	public Long getNroCuotasOri();

	/**
	 * @param nroCuotasOri the nroCuotasOri to set
	 */
	public void setNroCuotasOri(Long nroCuotasOri);

	/**
	 * @return the periodAtrasado
	 */
	public Long getPeriodAtrasado();

	/**
	 * @param periodAtrasado the periodAtrasado to set
	 */
	public void setPeriodAtrasado(Long periodAtrasado);

	/**
	 * @return the saldoCapital
	 */
	public Double getSaldoCapital();

	/**
	 * @param saldoCapital the saldoCapital to set
	 */
	public void setSaldoCapital(Double saldoCapital);

	/**
	 * @return the saldoIntereses
	 */
	public Double getSaldoIntereses();

	/**
	 * @param saldoIntereses the saldoIntereses to set
	 */
	public void setSaldoIntereses(Double saldoIntereses);

	/**
	 * @return the saldoIntMora
	 */
	public Double getSaldoIntMora();

	/**
	 * @param saldoIntMora the saldoIntMora to set
	 */
	public void setSaldoIntMora(Double saldoIntMora);

	/**
	 * @return the tasaInteres
	 */
	public Double getTasaInteres();

	/**
	 * @param tasaInteres the tasaInteres to set
	 */
	public void setTasaInteres(Double tasaInteres);

	/**
	 * @return the valorCuota
	 */
	public Double getValorCuota();

	/**
	 * @param valorCuota the valorCuota to set
	 */
	public void setValorCuota(Double valorCuota);

	/**
	 * @return the deudaActual
	 */
	public Double getDeudaActual();

	/**
	 * @param deudaActual the deudaActual to set
	 */
	public void setDeudaActual(Double deudaActual);

	/**
	 * @return the estado
	 */
	public String getEstado();

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado);

	public Date getFechaActivSrv();

	public void setFechaActivSrv(Date fechaActivSrv);

	public Long getIdLineaNegocio();

	public void setIdLineaNegocio(Long idLineaNegocio);

	public Long getIdPlan();

	public void setIdPlan(Long idPlan);

	public Long getIdTipoProducto();

	public void setIdTipoProducto(Long idTipoProducto);

	public Long getIdTipoServicio();

	public void setIdTipoServicio(Long idTipoServicio);

	public String getLineaNegocio();

	public void setLineaNegocio(String lineaNegocio);

	public String getMotivoFinalizacion();

	public void setMotivoFinalizacion(String motivoFinalizacion);

	public String getNombreTit();

	public void setNombreTit(String nombreTit);

	public Long getNroIdentServicio();

	public void setNroIdentServicio(Long nroIdentServicio);

	public String getNroIdentSocio();

	public void setNroIdentSocio(String nroIdentSocio);

	public String getNroIdentTitular();

	public void setNroIdentTitular(String nroIdentTitular);

	public Long getNroSrvFin();

	public void setNroSrvFin(Long nroSrvFin);

	public String getPlan();

	public void setPlan(String plan);

	public String getProducto();

	public void setProducto(String producto);

	public Double getSaldoSrv();

	public void setSaldoSrv(Double saldoSrv);

	public String getSolicitanteCred();

	public void setSolicitanteCred(String solicitanteCred);

	public String getTipoIdent();

	public void setTipoIdent(String tipoIdent);

	public String getTipoIdentCode();

	public void setTipoIdentCode(String tipoIdentCode);

	public String getTipoSrvFin();

	public void setTipoSrvFin(String tipoSrvFin);
}
