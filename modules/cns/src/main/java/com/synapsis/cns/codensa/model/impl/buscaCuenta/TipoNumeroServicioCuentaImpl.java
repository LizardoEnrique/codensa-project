package com.synapsis.cns.codensa.model.impl.buscaCuenta;

import com.synapsis.cns.codensa.model.buscaCuenta.TipoNumeroServicioCuenta;

public class TipoNumeroServicioCuentaImpl extends BuscaCuentaImpl implements TipoNumeroServicioCuenta {
	
	private String tipoServicio;
	private Long nroServicio;
	private Long nroCuenta;

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return this.nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaTipoNroServicio#getNroServicio()
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaTipoNroServicio#getTipoServicio()
	 */
	public String getTipoServicio() {
		return tipoServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaTipoNroServicio#setNroServicio(java.lang.Long)
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaTipoNroServicio#setTipoServicio(java.lang.Long)
	 */
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
}
