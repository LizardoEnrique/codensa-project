/**
 * $Id: DetallePagoBB.java,v 1.1 2007/11/14 18:23:36 ar26557682 Exp $
 */
package com.synapsis.cns.codensa.web;


/**
 * 
 * @author Paola Attadio
 * @version $Revision: 1.1 $
 */
public class DetallePagoBB {
	
	
	private Long nroDocumento;
	private String tipoDocumento;
	
	
	public Long getNroDocumento() {
		return nroDocumento;
	}
	public void setNroDocumento(Long nroDocumento) {
		this.nroDocumento = nroDocumento;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
}	
