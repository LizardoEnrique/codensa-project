package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaDeudaInformacionRefacturacion extends SynergiaBusinessObject{

	/**
	 * @return the fechaEmicionUltimaFactura
	 */
	public Date getFechaEmicionUltimaFactura();

	/**
	 * @param fechaEmicionUltimaFactura the fechaEmicionUltimaFactura to set
	 */
	public void setFechaEmicionUltimaFactura(Date fechaEmicionUltimaFactura);

	/**
	 * @return the idCuenta
	 */
	public Long getIdCuenta();

	/**
	 * @param idCuenta the idCuenta to set
	 */
	public void setIdCuenta(Long idCuenta);

	/**
	 * @return the idServicio
	 */
	public Long getIdServicio();

	/**
	 * @param idServicio the idServicio to set
	 */
	public void setIdServicio(Long idServicio);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio();

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio);

	/**
	 * @return the nroUltimoRefacturacion
	 */
	public String getNroUltimoRefacturacion();

	/**
	 * @param nroUltimoRefacturacion the nroUltimoRefacturacion to set
	 */
	public void setNroUltimoRefacturacion(String nroUltimoRefacturacion);

	/**
	 * @return the valorRefacturacion
	 */
	public String getValorRefacturacion();
	
	public String getIdDocumentoOriginal();
	
	public void setIdDocumentoOriginal(String idDocumentoOriginal);

	/**
	 * @param valorRefacturacion the valorRefacturacion to set
	 */
	public void setValorRefacturacion(String valorRefacturacion);

}