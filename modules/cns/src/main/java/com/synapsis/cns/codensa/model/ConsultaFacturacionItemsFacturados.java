package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


public interface ConsultaFacturacionItemsFacturados extends SynergiaBusinessObject {

	/**
	 * @return the codigoItem
	 */
	public abstract String getCodigoItem();

	/**
	 * @param codigoItem the codigoItem to set
	 */
	public abstract void setCodigoItem(String codigoItem);

	/**
	 * @return the descripcionItem
	 */
	public abstract String getDescripcionItem();

	/**
	 * @param descripcionItem the descripcionItem to set
	 */
	public abstract void setDescripcionItem(String descripcionItem);

	/**
	 * @return the valorFacturadoItem
	 */
	public abstract Double getValorFacturadoItem();

	/**
	 * @param valorFacturadoItem the valorFacturadoItem to set
	 */
	public abstract void setValorFacturadoItem(Double valorFacturadoItem);
	
	public Long getIdItemDoc();

	public void setIdItemDoc(Long idItemDoc);
	
	public Date getPeriodoFacturacion();
	
	public void setPeriodoFacturacion(Date periodoFacturacion);

}