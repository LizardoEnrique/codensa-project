package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.1 $ $Date: 2007/02/28 21:47:47 $
 * 
 */
public interface DetalleCastigoCartera extends SynergiaBusinessObject {

	/**
	 * @return the cargoOriginal
	 */
	public String getCargoOriginal();

	/**
	 * @param cargoOriginal the cargoOriginal to set
	 */
	public void setCargoOriginal(String cargoOriginal);

	/**
	 * @return the codigoCargo
	 */
	public String getCodigoCargo();

	/**
	 * @param codigoCargo the codigoCargo to set
	 */
	public void setCodigoCargo(String codigoCargo);

	/**
	 * @return the correlativoFacturacion
	 */
	public String getCorrelativoFacturacion();

	/**
	 * @param correlativoFacturacion the correlativoFacturacion to set
	 */
	public void setCorrelativoFacturacion(String correlativoFacturacion);

	/**
	 * @return the descripcionCargo
	 */
	public String getDescripcionCargo();

	/**
	 * @param descripcionCargo the descripcionCargo to set
	 */
	public void setDescripcionCargo(String descripcionCargo);

	/**
	 * @return the fechaActualizacionCastigo
	 */
	public Date getFechaActualizacionCastigo();

	/**
	 * @param fechaActualizacionCastigo the fechaActualizacionCastigo to set
	 */
	public void setFechaActualizacionCastigo(Date fechaActualizacionCastigo);

	/**
	 * @return the fechaFacturacion
	 */
	public Date getFechaFacturacion();

	/**
	 * @param fechaFacturacion the fechaFacturacion to set
	 */
	public void setFechaFacturacion(Date fechaFacturacion);

	/**
	 * @return the motivoCastigo
	 */
	public String getMotivoCastigo();

	/**
	 * @param motivoCastigo the motivoCastigo to set
	 */
	public void setMotivoCastigo(String motivoCastigo);

	/**
	 * @return the nombreUsuarioCastigo
	 */
	public String getNombreUsuarioCastigo();

	/**
	 * @param nombreUsuarioCastigo the nombreUsuarioCastigo to set
	 */
	public void setNombreUsuarioCastigo(String nombreUsuarioCastigo);

	/**
	 * @return the observaciones
	 */
	public String getObservaciones();

	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones);

	/**
	 * @return the tipoCastigo
	 */
	public String getTipoCastigo();

	/**
	 * @param tipoCastigo the tipoCastigo to set
	 */
	public void setTipoCastigo(String tipoCastigo);

	/**
	 * @return the usuarioCastigo
	 */
	public String getUsuarioCastigo();

	/**
	 * @param usuarioCastigo the usuarioCastigo to set
	 */
	public void setUsuarioCastigo(String usuarioCastigo);

	/**
	 * @return the valorCargo
	 */
	public String getValorCargo();

	/**
	 * @param valorCargo the valorCargo to set
	 */
	public void setValorCargo(String valorCargo);

}