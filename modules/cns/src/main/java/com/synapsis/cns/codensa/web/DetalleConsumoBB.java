/**
 * 
 */
package com.synapsis.cns.codensa.web;

/**
 * @author ar18817018
 *
 */
public class DetalleConsumoBB {
	
	private Long idMedidor;
	/**
	 * Este numeroMedidor lo pongo para resolver el SJCOD-18484 con m�nimos
	 * cambios, ya que todas las vistas relacionadas a esta consulta tienen un
	 * campo con alias id_medidor (Long) cuando en realidad trae el nro_medidor
	 * (String).
	 * 
	 */
	private String numeroMedidor;

	public Long getIdMedidor() {
		return idMedidor;
	}

	public void setIdMedidor(Long idMedidor) {
		this.idMedidor = idMedidor;
	}

	public String getnumeroMedidor() {
		return numeroMedidor;
	}

	public void setnumeroMedidor(String numeroMedidor) {
		this.numeroMedidor = numeroMedidor;
		this.setIdMedidor(new Long(numeroMedidor));
	}
	
	
	

}
