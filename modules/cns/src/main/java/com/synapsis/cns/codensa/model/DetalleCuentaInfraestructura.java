package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author ccamba
 * 
 */
public interface DetalleCuentaInfraestructura extends SynergiaBusinessObject {

	public Long getNroCuenta();

	public Long getNroServicio();

	public String getTitularCuenta();

	public String getRutaReparto();

	public String getManzanaReparto();

	public String getNroContrato();

	public Long getIdClienteTelem();

	public String getAplicaDescuento();

	public String getAplicaPeriodoGracia();

	public String getContratoVigente();

	public Date getFechaInstalacion();

	public void setNroCuenta(Long nroCuenta);

	public void setNroServicio(Long nroServicio);

	public void setTitularCuenta(String titularCuenta);

	public void setRutaReparto(String rutaReparto);

	public void setManzanaReparto(String manzanaReparto);

	public void setNroContrato(String nroContrato);

	public void setIdClienteTelem(Long idClienteTelem);

	public void setAplicaDescuento(String aplicaDescuento);

	public void setAplicaPeriodoGracia(String aplicaPeriodoGracia);

	public void setContratoVigente(String contratoVigente);

	public void setFechaInstalacion(Date fechaInstalacion);

}
