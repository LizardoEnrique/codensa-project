/**
 * 
 */
package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author dbraccio
 *
 *CU 016
 */
public interface ConsultaRefacturacionAjusteSaldo extends
		SynergiaBusinessObject {

	public Long getIdAjuste() ;
	public Long getIdCuenta() ;
	public Long getNroAjuste() ;
	public Long getNroCuenta() ;
	public Double getSaldoEnergiaAntesAjuste();
	public Double getSaldoEnergiaDespuesAjuste();
	public Double getSaldoOtrosNegociosAntesAjuste();
	public Double getSaldoOtrosNEgociosDespuesAjuste();
	public Double getSaldoTotalAntesAjuste() ;
	public Double getSaldoTotalDespuesAjuste();
	public Double getValorEnergiaAjuste() ;
	public Double getValorOtrosNegociosAjuste();
}
