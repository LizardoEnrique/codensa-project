package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.DetalleCCA;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class DetalleCCAImpl extends SynergiaBusinessObjectImpl implements DetalleCCA {

    private static final long serialVersionUID = 1L;
    
	private Date fechaCompra;
	private String socioNegocio;
	private String puntoVenta;
	private String tipoProducto;
	private String marca;
	private Long valorCompra;
	private Long cuotasPactadas;
	private Double tasaInteresPactada;
	private Double tasaInteresActual;
	private Long cuotasFacturadas;
	private Long cuotasFaltantes;
	private Long antiguedadAervicio;
	private Long diasMora;
	private Long cuotaCapitalMora;
	private Long valorCuotasInteresMora;
	private Long saldoCapital;
	private Long saldoIntereses;
	private Long valorInteresesMoraCapital;
	private Long valorCuotaCapital;
	private Long valorCuotaIntereses;
	private Long valorCuotaMensual;
	private Long cuotasImpagas;
	private Long fechaEstudioCredito;
	private String periodoPrimeraCuota;
	private Long deudaActual;
	private Long valorCuotaCapitalNoAfecto;
	private String nombreTitular;
	private Long cicloFacturacion;
	

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getAntiguedadAervicio()
	 */
	public Long getAntiguedadAervicio() {
		return antiguedadAervicio;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setAntiguedadAervicio(java.lang.Long)
	 */
	public void setAntiguedadAervicio(Long antiguedadAervicio) {
		this.antiguedadAervicio = antiguedadAervicio;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getCuotaCapitalMora()
	 */
	public Long getCuotaCapitalMora() {
		return cuotaCapitalMora;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setCuotaCapitalMora(java.lang.Long)
	 */
	public void setCuotaCapitalMora(Long cuotaCapitalMora) {
		this.cuotaCapitalMora = cuotaCapitalMora;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getCuotasFacturadas()
	 */
	public Long getCuotasFacturadas() {
		return cuotasFacturadas;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setCuotasFacturadas(java.lang.Long)
	 */
	public void setCuotasFacturadas(Long cuotasFacturadas) {
		this.cuotasFacturadas = cuotasFacturadas;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getCuotasFaltantes()
	 */
	public Long getCuotasFaltantes() {
		return cuotasFaltantes;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setCuotasFaltantes(java.lang.Long)
	 */
	public void setCuotasFaltantes(Long cuotasFaltantes) {
		this.cuotasFaltantes = cuotasFaltantes;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getCuotasImpagas()
	 */
	public Long getCuotasImpagas() {
		return cuotasImpagas;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setCuotasImpagas(java.lang.Long)
	 */
	public void setCuotasImpagas(Long cuotasImpagas) {
		this.cuotasImpagas = cuotasImpagas;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getCuotasPactadas()
	 */
	public Long getCuotasPactadas() {
		return cuotasPactadas;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setCuotasPactadas(java.lang.Long)
	 */
	public void setCuotasPactadas(Long cuotasPactadas) {
		this.cuotasPactadas = cuotasPactadas;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getDeudaActual()
	 */
	public Long getDeudaActual() {
		return deudaActual;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setDeudaActual(java.lang.Long)
	 */
	public void setDeudaActual(Long deudaActual) {
		this.deudaActual = deudaActual;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getDiasMora()
	 */
	public Long getDiasMora() {
		return diasMora;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setDiasMora(java.lang.Long)
	 */
	public void setDiasMora(Long diasMora) {
		this.diasMora = diasMora;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getFechaCompra()
	 */
	public Date getFechaCompra() {
		return fechaCompra;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setFechaCompra(java.util.Date)
	 */
	public void setFechaCompra(Date fechaCompra) {
		this.fechaCompra = fechaCompra;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getFechaEstudioCredito()
	 */
	public Long getFechaEstudioCredito() {
		return fechaEstudioCredito;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setFechaEstudioCredito(java.util.Date)
	 */
	public void setFechaEstudioCredito(Long fechaEstudioCredito) {
		this.fechaEstudioCredito = fechaEstudioCredito;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getMarca()
	 */
	public String getMarca() {
		return marca;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setMarca(java.lang.String)
	 */
	public void setMarca(String marca) {
		this.marca = marca;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getPeriodoPrimeraCuota()
	 */
	public String getPeriodoPrimeraCuota() {
		return periodoPrimeraCuota;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setPeriodoPrimeraCuota(java.lang.Long)
	 */
	public void setPeriodoPrimeraCuota(String periodoPrimeraCuota) {
		this.periodoPrimeraCuota = periodoPrimeraCuota;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getPuntoVenta()
	 */
	public String getPuntoVenta() {
		return puntoVenta;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setPuntoVenta(java.lang.String)
	 */
	public void setPuntoVenta(String puntoVenta) {
		this.puntoVenta = puntoVenta;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getSaldoCapital()
	 */
	public Long getSaldoCapital() {
		return saldoCapital;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setSaldoCapital(java.lang.Long)
	 */
	public void setSaldoCapital(Long saldoCapital) {
		this.saldoCapital = saldoCapital;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getSaldoIntereses()
	 */
	public Long getSaldoIntereses() {
		return saldoIntereses;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setSaldoIntereses(java.lang.Long)
	 */
	public void setSaldoIntereses(Long saldoIntereses) {
		this.saldoIntereses = saldoIntereses;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getSocioNegocio()
	 */
	public String getSocioNegocio() {
		return socioNegocio;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setSocioNegocio(java.lang.String)
	 */
	public void setSocioNegocio(String socioNegocio) {
		this.socioNegocio = socioNegocio;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getTasaInteresActual()
	 */
	public Double getTasaInteresActual() {
		return tasaInteresActual;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setTasaInteresActual(java.lang.Double)
	 */
	public void setTasaInteresActual(Double tasaInteresActual) {
		this.tasaInteresActual = tasaInteresActual;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getTasaInteresPactada()
	 */
	public Double getTasaInteresPactada() {
		return tasaInteresPactada;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setTasaInteresPactada(java.lang.Double)
	 */
	public void setTasaInteresPactada(Double tasaInteresPactada) {
		this.tasaInteresPactada = tasaInteresPactada;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getTipoProducto()
	 */
	public String getTipoProducto() {
		return tipoProducto;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setTipoProducto(java.lang.String)
	 */
	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getValorCompra()
	 */
	public Long getValorCompra() {
		return valorCompra;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setValorCompra(java.lang.Long)
	 */
	public void setValorCompra(Long valorCompra) {
		this.valorCompra = valorCompra;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getValorCuotaCapital()
	 */
	public Long getValorCuotaCapital() {
		return valorCuotaCapital;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setValorCuotaCapital(java.lang.Long)
	 */
	public void setValorCuotaCapital(Long valorCuotaCapital) {
		this.valorCuotaCapital = valorCuotaCapital;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getValorCuotaCapitalNoAfecto()
	 */
	public Long getValorCuotaCapitalNoAfecto() {
		return valorCuotaCapitalNoAfecto;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setValorCuotaCapitalNoAfecto(java.lang.Long)
	 */
	public void setValorCuotaCapitalNoAfecto(Long valorCuotaCapitalNoAfecto) {
		this.valorCuotaCapitalNoAfecto = valorCuotaCapitalNoAfecto;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getValorCuotaIntereses()
	 */
	public Long getValorCuotaIntereses() {
		return valorCuotaIntereses;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setValorCuotaIntereses(java.lang.Long)
	 */
	public void setValorCuotaIntereses(Long valorCuotaIntereses) {
		this.valorCuotaIntereses = valorCuotaIntereses;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getValorCuotaMensual()
	 */
	public Long getValorCuotaMensual() {
		return valorCuotaMensual;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setValorCuotaMensual(java.lang.Long)
	 */
	public void setValorCuotaMensual(Long valorCuotaMensual) {
		this.valorCuotaMensual = valorCuotaMensual;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getValorCuotasInteresMora()
	 */
	public Long getValorCuotasInteresMora() {
		return valorCuotasInteresMora;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setValorCuotasInteresMora(java.lang.Long)
	 */
	public void setValorCuotasInteresMora(Long valorCuotasInteresMora) {
		this.valorCuotasInteresMora = valorCuotasInteresMora;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#getValorInteresesMoraCapital()
	 */
	public Long getValorInteresesMoraCapital() {
		return valorInteresesMoraCapital;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCC#setValorInteresesMoraCapital(java.lang.Long)
	 */
	public void setValorInteresesMoraCapital(Long valorInteresesMoraCapital) {
		this.valorInteresesMoraCapital = valorInteresesMoraCapital;
	}

	public Long getCicloFacturacion() {
		return cicloFacturacion;
	}

	public void setCicloFacturacion(Long cicloFacturacion) {
		this.cicloFacturacion = cicloFacturacion;
	}

	public String getNombreTitular() {
		return nombreTitular;
	}

	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}

	
}
