/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaHistoriaEstadosConvenio;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 *
 */
public class ConsultaHistoriaEstadosConvenioImpl extends SynergiaBusinessObjectImpl implements ConsultaHistoriaEstadosConvenio {
	
	private static final long serialVersionUID = 1L;
	
	private Long nroServicio;
	private Long nroIdentificacionSolicitantecredito;
	private String lineaNegocioPerteneciente;
	private Date fechaActivacionServicio;	
	private Long valorOriginalServicio;
	private Long saldoServicio;
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenio#getFechaactivacionServicio()
	 */
	public Date getFechaActivacionServicio() {
		return fechaActivacionServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenio#setFechaactivacionServicio(java.util.Date)
	 */
	public void setFechaActivacionServicio(Date fechaactivacionServicio) {
		this.fechaActivacionServicio = fechaactivacionServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenio#getLineaNegocioPerteneciente()
	 */
	public String getLineaNegocioPerteneciente() {
		return lineaNegocioPerteneciente;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenio#setLineaNegocioPerteneciente(java.lang.String)
	 */
	public void setLineaNegocioPerteneciente(String lineaNegocioPerteneciente) {
		this.lineaNegocioPerteneciente = lineaNegocioPerteneciente;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenio#getNroIdentificacionSolicitantecredito()
	 */
	public Long getNroIdentificacionSolicitantecredito() {
		return nroIdentificacionSolicitantecredito;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenio#setNroIdentificacionSolicitantecredito(java.lang.Long)
	 */
	public void setNroIdentificacionSolicitantecredito(
			Long nroIdentificacionSolicitantecredito) {
		this.nroIdentificacionSolicitantecredito = nroIdentificacionSolicitantecredito;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenio#getNroServicio()
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenio#setNroServicio(java.lang.Long)
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenio#getSaldoServicio()
	 */
	public Long getSaldoServicio() {
		return saldoServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenio#setSaldoServicio(java.lang.Long)
	 */
	public void setSaldoServicio(Long saldoServicio) {
		this.saldoServicio = saldoServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenio#getValorOriginalServicio()
	 */
	public Long getValorOriginalServicio() {
		return valorOriginalServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenio#setValorOriginalServicio(java.lang.Long)
	 */
	public void setValorOriginalServicio(Long valorOriginalServicio) {
		this.valorOriginalServicio = valorOriginalServicio;
	}
	
	
	
	
}
