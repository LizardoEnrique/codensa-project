package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface DetalleCargosConvenio extends SynergiaBusinessObject {

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public String getCodigoCargo();

	public void setCodigoCargo(String codCargo);

	public String getDescripcionCargo();
	
	public void setDescripcionCargo(String descCargo);

	public Double getValorCargo();
	
	public void setValorCargo(Double valorCargo);

	public Date getFechaFacturacion();
	
	public void setFechaFacturacion(Date fecFactura);

	public String getPeriodoFacturacion();
	
	public void setPeriodoFacturacion(String periodoFacturacion);

	public String getTipoDocumento();
	
	public void setTipoDocumento(String TipoDocumento);

	public Long getNroDocumentoAssoc();
	
	public void setNroDocumentoAssoc(Long nroDocumento);

}
