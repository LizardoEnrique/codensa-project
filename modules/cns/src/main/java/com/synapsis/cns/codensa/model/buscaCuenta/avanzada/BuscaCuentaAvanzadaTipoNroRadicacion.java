package com.synapsis.cns.codensa.model.buscaCuenta.avanzada;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface BuscaCuentaAvanzadaTipoNroRadicacion extends SynergiaBusinessObject {

	/**
	 * @return the nroRadicacion
	 */
	public Integer getNroRadicacion();

	/**
	 * @return the tipoRadicacion
	 */
	public String getTipoRadicacion();

	/**
	 * @param nroRadicacion the nroRadicacion to set
	 */
	public void setNroRadicacion(Integer nroRadicacion);

	/**
	 * @param tipoRadicacion the tipoRadicacion to set
	 */
	public void setTipoRadicacion(String tipoRadicacion);

}