package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.CargoFactura;

/**
 * 
 * @author adambrosio
 */
public class CargoFacturaImpl extends AbstractCargoFactura implements CargoFactura {

	private Long valor;

	public Long getValor() {
		return valor;
	}

	public void setValor(Long valor) {
		this.valor = valor;
	}
}
