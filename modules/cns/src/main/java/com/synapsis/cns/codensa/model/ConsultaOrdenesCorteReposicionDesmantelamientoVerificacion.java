package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * Consulta de Órdenes de Corte, Reposición, Desmantelamiento y Verificación<br>
 * (Vista XCNS_EDE_COR_ORDEN)
 * 
 * @author egrande
 * 
 */
public interface ConsultaOrdenesCorteReposicionDesmantelamientoVerificacion
		extends SynergiaBusinessObject {
	public Long getNroCuenta();

	public Long getNroServicio();

	public String getNroOrden();

	public String getTipo();

	public String getSubtipo();

	public Date getFechaVigencia();

	public Date getFechaEmision();

	public String getUsuarioCreador();

	public String getSitEncontrada1();

	public String getSitEncontrada2();

	public String getAccRealizada();

	public Date getFechaEjecucion();

	public Date getFechaReal();

	public String getContratista();

	public String getEjecutor();

	public String getMotivo();

	public String getCargo();

	public String getEstadoCargo();

	public Double getValorCargo();

	public String getEstado();

	public String getEsMasiva();

	public String getObservacion();

	public Long getLecturas();

}
