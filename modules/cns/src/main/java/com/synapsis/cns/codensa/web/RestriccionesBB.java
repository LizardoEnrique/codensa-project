/**
 * 
 */
package com.synapsis.cns.codensa.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.model.SelectItem;

import com.suivant.arquitectura.core.queryFilter.CompositeQueryFilter;
import com.suivant.arquitectura.presentation.model.ListableBB;
import com.suivant.arquitectura.presentation.utils.ComboUtils;
import com.suivant.arquitectura.presentation.utils.JSFUtils;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.synapsis.synergia.core.components.dataTable.ScrollableList;

/**
 * 
 *
 * @author Paola Attadio
 * @version $Id: RestriccionesBB.java,v 1.5 2007/03/16 21:36:14 ar30557486 Exp $
 */
public class RestriccionesBB extends ListableBB {
	
	
	

	/**
	 * @return the items
	 */
	public SelectItem[] getItems() {
		
		List result = new ArrayList();

		Map element0 = new HashMap();
		element0.put("id", "Electrico");
		element0.put("descipcion", "Electrico");

		Map element1 = new HashMap();
		element1.put("id", "Venta");
		element1.put("descipcion", "Venta");
		
		Map element2 = new HashMap();
		element2.put("id", "Financiacion");
		element2.put("descipcion", "Financiacion");
		
		Map element3 = new HashMap();
		element3.put("id", "Convenio");
		element3.put("descipcion", "Convenio");
		
		Map element4 = new HashMap();
		element4.put("id", "EncargoCobranza");
		element4.put("descipcion", "Encargo de Cobranza");

		result.add(element0);
		result.add(element1);
		result.add(element2);
		result.add(element3);
		result.add(element4);
		
		return new ComboUtils("id","descipcion").createSelecItems(result);
		
	}
	
	/**
	 * @return
	 */
	public String restriccionesQueryFilter() {
		// bring dataTable to life ...
		HtmlDataTable dataTable = (HtmlDataTable) JSFUtils.getComponentFromTree("restriccionesForm:restriccionesTable");
		// ??? found the table ?!?!?! ...
		if (dataTable != null) {
			// updates the query filter ...
			CompositeQueryFilter queryFilter = (CompositeQueryFilter) VariableResolverUtils.
				getObject("restriccionesQueryFilter");
			// refresh de data value ...
			dataTable.setValue(new ScrollableList(queryFilter,
					super.getMultiList().getUniqueList().getServiceName(), 
					super.getMultiList().getUniqueList().getMetadata()));
			// go' first (reset the internal cursor):P
			dataTable.setFirst(0);
		}
		return null;
	}	
	

}
