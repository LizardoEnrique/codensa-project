package com.synapsis.cns.codensa.model.combo.impl;

import com.synapsis.cns.codensa.model.combo.Combo;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ComboSucursalesImpl extends SynergiaBusinessObjectImpl implements Combo {

	private static final long serialVersionUID = 1L;

	private String codigo;

	private String descripcion;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.combo.impl.ComboDetalle#getCodigo()
	 */
	public String getCodigo() {
		return codigo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.combo.impl.ComboDetalle#setCodigo(java.lang.String)
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.combo.impl.ComboDetalle#getDescripcion()
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.combo.impl.ComboDetalle#setDescripcion(java.lang.String)
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}