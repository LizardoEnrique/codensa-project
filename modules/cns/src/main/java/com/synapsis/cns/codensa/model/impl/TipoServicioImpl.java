package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.TipoServicio;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @see com.synapsis.cns.codensa.model.TipoServicio
 *  
 * @author adambrosio
 */
public class TipoServicioImpl extends SynergiaBusinessObjectImpl implements TipoServicio {
	private Long nroCuenta;
	private Long nroServicio;
	private String tipoServicio;

	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroFactura) {
		this.nroCuenta = nroFactura;
	}
	public Long getNroServicio() {
		return nroServicio;
	}
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	public String getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
}