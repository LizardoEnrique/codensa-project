/**
 * 
 */
package com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada;

import com.synapsis.cns.codensa.model.buscaCuenta.avanzada.BuscaCuentaAvanzadaTipoNroOrden;
import com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaImpl;

/**
 * @author Emiliano Arango (ar30557486)
 * 
 */
public class BuscaCuentaAvanzadaTipoNroOrdenImpl extends BuscaCuentaImpl
		implements BuscaCuentaAvanzadaTipoNroOrden {

	private String tipoOrden;

	private String nroOrden;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada.BuscaCuentaAvanzadaTipoNroOrden#getNroOrden()
	 */
	public String getNroOrden() {
		return nroOrden;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada.BuscaCuentaAvanzadaTipoNroOrden#getTipoOrden()
	 */
	public String getTipoOrden() {
		return tipoOrden;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada.BuscaCuentaAvanzadaTipoNroOrden#setNroOrden(java.lang.String)
	 */
	public void setNroOrden(String nroOrden) {
		this.nroOrden = nroOrden;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada.BuscaCuentaAvanzadaTipoNroOrden#setTipoOrden(java.lang.String)
	 */
	public void setTipoOrden(String tipoOrden) {
		this.tipoOrden = tipoOrden;
	}

}
