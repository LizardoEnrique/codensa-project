package com.synapsis.cns.codensa.web;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.event.ActionEvent;

import org.apache.myfaces.custom.service.UIService;

import com.suivant.arquitectura.presentation.utils.JSFUtils;
import com.synapsis.commons.faces.messages.ErrorsManager;
import com.synapsis.synergia.presentation.model.NasServiceHelper;

/**
 * @author ccamba
 * 
 */
public class ConsultaContratoInfraestructuraBB implements Serializable {
	public static final String STYLE_SHOW = "display: block;";
	public static final String STYLE_HIDE = "display: none;";
	private transient UIService cuentasInfraestructuraService;
	private transient HtmlDataTable cuentasInfraestructuraDataTable;
	private transient HtmlPanelGroup gridResultsPanel;
	private Long nroCuenta;
	private Long nroServicio;
	private Long idClienteTelem;
	private String nroContrato;

	public UIService getCuentasInfraestructuraService() {
		return this.cuentasInfraestructuraService;
	}

	public void setCuentasInfraestructuraService(UIService cuentasInfraestructuraService) {
		this.cuentasInfraestructuraService = cuentasInfraestructuraService;
	}

	public HtmlDataTable getCuentasInfraestructuraDataTable() {
		return this.cuentasInfraestructuraDataTable;
	}

	public void setCuentasInfraestructuraDataTable(HtmlDataTable cuentasInfraestructuraDataTable) {
		this.cuentasInfraestructuraDataTable = cuentasInfraestructuraDataTable;
	}

	public Long getNroCuenta() {
		return this.nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public Long getIdClienteTelem() {
		return this.idClienteTelem;
	}

	public String getNroContrato() {
		return this.nroContrato;
	}

	public void setIdClienteTelem(Long idClienteTelem) {
		this.idClienteTelem = idClienteTelem;
	}

	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}

	public Long getNroServicio() {
		return this.nroServicio;
	}

	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}

	public void search() {
		Map parameters = this.getParametersMap();
//		if (!parameters.isEmpty()) {
			try {
				Boolean exists = (Boolean) NasServiceHelper.getResponse("cuentasInfraestructuraService", "exists",
					parameters);

				if (!exists.booleanValue()) {
					ErrorsManager.addWarmError("La busqueda no trajo resultados.");
					this.setGridResultsPanelStyleCode(STYLE_HIDE);
					return;
				}
				this.getCuentasInfraestructuraService().execute();
				this.setGridResultsPanelStyleCode(STYLE_SHOW);
			}
			catch (Exception e) {
				ErrorsManager.addError(e.getMessage() != null ? e.getMessage() : e.getCause() != null ? e
					.getCause()
					.toString() : e.toString());
				e.printStackTrace();
			}
//		}
//		else {
//			ErrorsManager.addWarmError("Debe completar por lo menos un filtro de busqueda.");
//			this.setGridResultsPanelStyleCode(STYLE_HIDE);
//		}
	}

	public void clean(ActionEvent event) {
		this.setNroCuenta(null);
		this.setNroServicio(null);
		this.setIdClienteTelem(null);
		this.setNroContrato(null);
		if (this.getCuentasInfraestructuraDataTable() != null)
			this.getCuentasInfraestructuraDataTable().setRowIndex(-1);
		this.setGridResultsPanelStyleCode(STYLE_HIDE);
	}

	public HtmlPanelGroup getGridResultsPanel() {
		return this.gridResultsPanel;
	}

	public void setGridResultsPanel(HtmlPanelGroup gridResultsPanel) {
		this.gridResultsPanel = gridResultsPanel;
	}

	public void setGridResultsPanelStyleCode(String styleCode) {
		if (this.gridResultsPanel == null) {
			this.gridResultsPanel = (HtmlPanelGroup) JSFUtils.getComponentFromTree("form1:gridResultsPanel");
		}
		try {
			this.getGridResultsPanel().setStyle(styleCode);
		}
		catch (NullPointerException e) {
		}
	}

	private Map getParametersMap() {
		Map parameters = new HashMap();

		if (this.getNroCuenta() != null)
			parameters.put("nroCuenta", this.getNroCuenta());

		if (this.getNroServicio() != null)
			parameters.put("nroServicio", this.getNroServicio());

		if (this.getIdClienteTelem() != null)
			parameters.put("idClienteTelem", this.getIdClienteTelem());

		if (this.getNroContrato().length() > 0)
			parameters.put("nroContrato", this.getNroContrato());

		return parameters;
	}

}
