package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface DatosComerciales extends SynergiaBusinessObject {
		
	/**
	 * @return the claseServicio
	 */
	public String getClaseServicio();
	/**
	 * @param claseServicio the claseServicio to set
	 */
	public void setClaseServicio(String claseServicio);
	/**
	 * @return the direccionReparto
	 */
	public String getDireccionReparto();
	/**
	 * @param direccionReparto the direccionReparto to set
	 */
	public void setDireccionReparto(String direccionReparto);
	/**
	 * @return the estrato
	 */
	public String getEstrato();
	/**
	 * @param estrato the estrato to set
	 */
	public void setEstrato(String estrato);
	/**
	 * @return the mercado
	 */
	public String getMercado();
	/**
	 * @param mercado the mercado to set
	 */
	public void setMercado(String mercado);
	/**
	 * @return the nombreEjecutivo
	 */
	public String getNombreEjecutivo();
	/**
	 * @param nombreEjecutivo the nombreEjecutivo to set
	 */
	public void setNombreEjecutivo(String nombreEjecutivo);
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);
	public String getRutaReparto();
	/**
	 * @param rutaReparto the rutaReparto to set
	 */
	public void setRutaReparto(String rutaReparto);
	/**
	 * @return the subclaseServicio
	 */
	public String getSubclaseServicio();
	/**
	 * @param subclaseServicio the subclaseServicio to set
	 */
	public void setSubclaseServicio(String subclaseServicio);
	/**
	 * @return the sucursal
	 */
	public String getSucursal();
	/**
	 * @param sucursal the sucursal to set
	 */
	public void setSucursal(String sucursal);
	/**
	 * @return the tarifa
	 */
	public String getTarifa();
	/**
	 * @param tarifa the tarifa to set
	 */
	public void setTarifa(String tarifa);
	/**
	 * @return the nroTipoServ
	 */
	public String getNroTipoServ();
	/**
	 * @param nroTipoServ the nroTipoServ to set
	 */
	public void setNroTipoServ(String nroTipoServ);
	public String getPotenciaFPHP();
	public void setPotenciaFPHP(String potenciaFPHP);
	public Double getPotenciaInstalada();
	public void setPotenciaInstalada(Double potenciaInstalada);
	
	
	public String getEstadoServConexion();
	
	public void setEstadoServConexion(String estadoServConexion);
	
	public String getGiro();
	
	public void setGiro(String giro);

	public String getTipoLiquidacion();
	
	public void setTipoLiquidacion(String tipoLiquidacion);
	
	public String getTipoEsquemaFacturacion();
	
	public void setTipoEsquemaFacturacion(String tipoEsquemaFacturacion);
	
}