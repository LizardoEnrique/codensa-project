package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;



public interface ContactoAic extends SynergiaBusinessObject {

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);	
	
	public Long getIdContactoDist();

	public void setIdContactoDist(Long idContactoDist);
	
	public Long getIdCuenta();

	public void setIdCuenta(Long idCuenta);
	
	public String getOrigenContacto();

	public void setOrigenContacto(String origenContacto);

	public Long getNumeroContacto();

	public void setNumeroContacto(Long numeroContacto);

	public void setFechaHoraCreacion(Date fechaHoraCreacion);

	public Date getFechaHoraCreacion();

	public String getCanalComunicacionContacto();

	public void setCanalComunicacionContacto(String canalComunicacionContacto);

	public String getUsuarioGeneradorContacto();

	public void setUsuarioGeneradorContacto(String usuarioGeneradorContacto);

	public String getNombreGeneradorContacto();

	public void setNombreGeneradorContacto(String nombreGeneradorContacto);

	public String getGrupoUsuarioGenerador();

	public void setGrupoUsuarioGenerador(String grupoUsuarioGenerador);

	public String getObservacionesContacto();

	public void setObservacionesContacto(String observacionesContacto);

	public String getTipoRadicacion();

	public void setTipoRadicacion(String tipoRadicacion);

	public Long getNumeroRadicacion();

	public void setNumeroRadicacion(Long numeroRadicacion);

	public String getEstadoAtencion();

	public void setEstadoAtencion(String estadoAtencion);
	
	public String getNumeroAtencion();

	public void setNumeroAtencion(String numeroAtencion);
	
	public Long getIdContacto();

	public void setIdContacto(Long idContacto);
}

