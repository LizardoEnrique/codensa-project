package com.synapsis.cns.codensa.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * Interfaz del item del serv financiero CCA, para el reporte de duplicado de anexo de factura (Servicios Financieros)
 *    
 * @author jhack
 */
public interface ItemServicioFinancieroCCA {

	public String getTitular();
	public void setTitular(String titular);
	public Long getNroServicio();
	public void setNroServicio(Long nroServicio);
	public String getTipoProducto();
	public void setTipoProducto(String tipoProducto);
	
	
	public BigDecimal getPagoMinimo();
	public void setPagoMinimo(BigDecimal pagoMinimo);
	public BigDecimal getSaldoEnMora();
	public void setSaldoEnMora(BigDecimal saldoEnMora);
	public BigDecimal getInteresPorMora();
	public void setInteresPorMora(BigDecimal interesPorMora);
	public BigDecimal getPagoTotal();
	public void setPagoTotal(BigDecimal pagoTotal);
	public Double getTasaInteres();
	public void setTasaInteres(Double tasaInteres);
	public Long getCupoTotal();
	public void setCupoTotal(Long cupoTotal);
	public Long getCupoDisponible();
	public void setCupoDisponible(Long cupoDisponible);

	public Date getFecha();
	public void setFecha(Date fecha);
	public String getDescripcion();
	public void setDescripcion(String descripcion);
	public Long getCuotaActual();
	public void setCuotaActual(Long cuotaActual);
	public Long getTotalDeCuotas();
	public void setTotalDeCuotas(Long totalDeCuotas);
	public BigDecimal getValorCompra();
	public void setValorCompra(BigDecimal valorCompra);
	public BigDecimal getCuotaACapital();
	public void setCuotaACapital(BigDecimal cuotaACapital);
	public BigDecimal getCuotaAInteres();
	public void setCuotaAInteres(BigDecimal cuotaAInteres);
	public BigDecimal getSaldoAPagar();
	public void setSaldoAPagar(BigDecimal saldoAPagar);
	public BigDecimal getValorCuota();
	public void setValorCuota(BigDecimal valorCuota);
	

}
