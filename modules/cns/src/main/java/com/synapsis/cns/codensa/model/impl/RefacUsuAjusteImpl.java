package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.cns.codensa.model.RefacUsuAjuste;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class RefacUsuAjusteImpl extends SynergiaBusinessObjectImpl implements RefacUsuAjuste {

	private Long nroCuenta;

	private String nroAjuste;
	
	private String usuarios;

	private String nombreUsuarios;

	private Date fechaIntervencion;

	private String accionEjecutada;

	private Double tiemposIntervUsuarios;

	private String obs;
	
	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getNroAjuste() {
		return nroAjuste;
	}

	public void setNroAjuste(String nroAjuste) {
		this.nroAjuste = nroAjuste;
	}
	
	public String getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(String usuarios) {
		this.usuarios = usuarios;
	}


	public String getNombreUsuarios() {
		return nombreUsuarios;
	}

	public void setNombreUsuarios(String nombreUsuarios) {
		this.nombreUsuarios = nombreUsuarios;
	}

	public Date getFechaIntervencion() {
		return fechaIntervencion;
	}

	public void setFechaIntervencion(Date fechaIntervencion) {
		this.fechaIntervencion = fechaIntervencion;
	}

	public String getAccionEjecutada() {
		return accionEjecutada;
	}

	public void setAccionEjecutada(String accionEjecutada) {
		this.accionEjecutada = accionEjecutada;
	}

	public Double getTiemposIntervUsuarios() {
		return tiemposIntervUsuarios;
	}

	public void setTiemposIntervUsuarios(Double tiemposIntervUsuarios) {
		this.tiemposIntervUsuarios = tiemposIntervUsuarios;
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}
}