/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 *
 */
public class ConsultaHistorialEstadoConvenioEstadosServicioImpl extends	SynergiaBusinessObjectImpl implements ConsultaHistorialEstadoConvenioEstadosServicio {

	private static final long serialVersionUID = 1L;
	
	private String estados;
	private String usuariosIngresaronEstado;
	private String fechasQuedaronEstado;
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistorialEstadoConvenioEstadosServicio#getEstados()
	 */
	public String getEstados() {
		return estados;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistorialEstadoConvenioEstadosServicio#setEstados(java.lang.String)
	 */
	public void setEstados(String estados) {
		this.estados = estados;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistorialEstadoConvenioEstadosServicio#getFechasQuedaronEstado()
	 */
	public String getFechasQuedaronEstado() {
		return fechasQuedaronEstado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistorialEstadoConvenioEstadosServicio#setFechasQuedaronEstado(java.lang.String)
	 */
	public void setFechasQuedaronEstado(String fechasQuedaronEstado) {
		this.fechasQuedaronEstado = fechasQuedaronEstado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistorialEstadoConvenioEstadosServicio#getUsuariosIngresaronEstado()
	 */
	public String getUsuariosIngresaronEstado() {
		return usuariosIngresaronEstado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistorialEstadoConvenioEstadosServicio#setUsuariosIngresaronEstado(java.lang.String)
	 */
	public void setUsuariosIngresaronEstado(String usuariosIngresaronEstado) {
		this.usuariosIngresaronEstado = usuariosIngresaronEstado;
	}
	
	
	


}
