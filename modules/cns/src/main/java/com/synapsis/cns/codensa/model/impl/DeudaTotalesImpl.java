package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import com.synapsis.cns.codensa.model.DeudaTotales;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class DeudaTotalesImpl extends SynergiaBusinessObjectImpl implements
		DeudaTotales {		
	
	private Long nroCuenta;

	private String deudaTotalActual;

	private String saldoTotalSujetoIntereses;

	private String saldoNoSujetoIntereses;

	private String valorTotalIntereses;

	private String valorTotalOtrosNegocios;
	
	private String valorTotalSanciones;

	private String saldoConvenios;

	private String saldoCodensaHogar;
	
	private String saldoEncargoCobranza; 

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getDeudaTotalActual() {
		return deudaTotalActual;
	}

	public void setDeudaTotalActual(String deudaTotalActual) {
		this.deudaTotalActual = deudaTotalActual;
	}

	public String getSaldoTotalSujetoIntereses() {
		return saldoTotalSujetoIntereses;
	}

	public void setSaldoTotalSujetoIntereses(String saldoTotalSujetoIntereses) {
		this.saldoTotalSujetoIntereses = saldoTotalSujetoIntereses;
	}

	public String getSaldoNoSujetoIntereses() {
		return saldoNoSujetoIntereses;
	}

	public void setSaldoNoSujetoIntereses(String saldoNoSujetoIntereses) {
		this.saldoNoSujetoIntereses = saldoNoSujetoIntereses;
	}

	public String getValorTotalIntereses() {
		return valorTotalIntereses;
	}

	public void setValorTotalIntereses(String valorTotalIntereses) {
		this.valorTotalIntereses = valorTotalIntereses;
	}

	public String getValorTotalOtrosNegocios() {
		return valorTotalOtrosNegocios;
	}

	public void setValorTotalOtrosNegocios(String valorTotalOtrosNegocios) {
		this.valorTotalOtrosNegocios = valorTotalOtrosNegocios;
	}

	public String getSaldoConvenios() {
		return saldoConvenios;
	}

	public void setSaldoConvenios(String saldoConvenios) {
		this.saldoConvenios = saldoConvenios;
	}

	public String getSaldoCodensaHogar() {
		return saldoCodensaHogar;
	}

	public void setSaldoCodensaHogar(String saldoCodensaHogar) {
		this.saldoCodensaHogar = saldoCodensaHogar;
	}
	
	public String getValorTotalSanciones() {
		return valorTotalSanciones;
	}
	
	public void setValorTotalSanciones(String valorTotalSanciones) {
		this.valorTotalSanciones = valorTotalSanciones;
	}

	public String getSaldoEncargoCobranza() {
		return saldoEncargoCobranza;
	}

	public void setSaldoEncargoCobranza(String saldoEncargoCobranza) {
		this.saldoEncargoCobranza = saldoEncargoCobranza;
		
	}	
}