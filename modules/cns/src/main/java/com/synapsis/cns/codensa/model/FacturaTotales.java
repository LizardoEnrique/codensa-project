package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface FacturaTotales extends SynergiaBusinessObject {

	public Long getNroFactura();

	public void setNroFactura(Long nroFactura);

	public Long getSubtCargosCons();

	public void setSubtCargosCons(Long subtCargosCons);

	public Double getSubtOtrosCargos();

	public void setSubtOtrosCargos(Double subtOtrosCargos);

	public Double getSubtCargosDesc();

	public void setSubtCargosDesc(Double subtCargosDesc);

	public Double getSubtTotCargosEne();

	public void setSubtTotCargosEne(Double subtTotCargosEne);

	public Double getSubtCargosSrvPort();

	public void setSubtCargosSrvPort(Double subtCargosSrvPort);

	public Double

	getTotalFactura();

	public void setTotalFactura(Double

	totalFactura);

}