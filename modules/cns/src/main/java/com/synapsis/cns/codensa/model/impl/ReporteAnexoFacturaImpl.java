package com.synapsis.cns.codensa.model.impl;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import com.synapsis.cns.codensa.model.ReporteAnexoFactura;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * Reporte de Impresion de Anexo de Factura (CNS003)
 * 
 * @author jhack
 */
public class ReporteAnexoFacturaImpl extends SynergiaBusinessObjectImpl implements ReporteAnexoFactura {
	private static final long serialVersionUID = 1L;

	// Cabecera
	private Long 	nroCuenta; 
	private String 	direccion;
	private String 	telefono;
	private Date 	periodoInicio;
	private Date 	periodoFin;
	private String 	rutaReparto;	

	// Servicios CCA
	private Collection 	serviciosCCA;
	
	// Encargos de Cobranza
	private Collection 	serviciosECO;
			
	public ReporteAnexoFacturaImpl() {
		this.setServiciosCCA(new HashSet());
		this.setServiciosECO(new HashSet());
	}

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Date getPeriodoInicio() {
		return periodoInicio;
	}

	public void setPeriodoInicio(Date periodoInicio) {
		this.periodoInicio = periodoInicio;
	}

	public Date getPeriodoFin() {
		return periodoFin;
	}

	public void setPeriodoFin(Date periodoFin) {
		this.periodoFin = periodoFin;
	}

	public String getRutaReparto() {
		return rutaReparto;
	}

	public void setRutaReparto(String rutaReparto) {
		this.rutaReparto = rutaReparto;
	}

	public Collection getServiciosCCA() {
		return serviciosCCA;
	}

	public void setServiciosCCA(Collection setServiciosCCA) {
		this.serviciosCCA = setServiciosCCA;
	}

	public Collection getServiciosECO() {
		return serviciosECO;
	}

	public void setServiciosECO(Collection serviciosECO) {
		this.serviciosECO = serviciosECO;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
	
}
