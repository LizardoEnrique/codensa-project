package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


public interface DetalleDeuda extends SynergiaBusinessObject {

	public Long getDeudaConvenioSrvElectrico();
	public void setDeudaConvenioSrvElectrico(Long deudaConvenioSrvElectrico);
	public Long getDeudaConvenioSrvOtros();
	public void setDeudaConvenioSrvOtros(Long deudaConvenioSrvOtros);
	public Long getDeudaExigible();
	public void setDeudaExigible(Long deudaExigible);
	public Long getSaldoInteres();
	public void setSaldoInteres(Long saldoInteres);
	public Long getSaldoSancion();
	public void setSaldoSancion(Long saldoSancion);
	public Long getSaldoSrvElectrico();
	public void setSaldoSrvElectrico(Long saldoSrvElectrico);
	public Long getSaldoSrvOtros();
	public void setSaldoSrvOtros(Long saldoSrvOtros);
	public Long getTotalDeuda();
	public void setTotalDeuda(Long totalDeuda);
	public Long getValorCastigo();
	public void setValorCastigo(Long valorCastigo);
	public Long getValorCondonadoConvenio();
	public void setValorCondonadoConvenio(Long valorCondonadoConvenio);
	public Long getValorDisputa();
	public void setValorDisputa(Long valorDisputa);
	public Long getIdMovimiento();
	public void setIdMovimiento(Long idMovimiento);	

}