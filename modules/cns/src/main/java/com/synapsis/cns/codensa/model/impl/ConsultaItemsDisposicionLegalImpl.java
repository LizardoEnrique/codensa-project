package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaItemsDisposicionLegal;
import com.synapsis.synergia.core.model.Empresa;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaItemsDisposicionLegalImpl extends SynergiaBusinessObjectImpl implements ConsultaItemsDisposicionLegal {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Empresa empresa;
	private Long idCargo;
	private Long nroAjuste;
	private String descripcion;
	

	public ConsultaItemsDisposicionLegalImpl() {
		
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public Long getIdCargo() {
		return idCargo;
	}


	public void setIdCargo(Long idCargo) {
		this.idCargo = idCargo;
	}


	public Long getNroAjuste() {
		return nroAjuste;
	}


	public void setNroAjuste(Long nroAjuste) {
		this.nroAjuste = nroAjuste;
	}

	

}
