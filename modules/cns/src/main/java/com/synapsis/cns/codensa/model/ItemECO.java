package com.synapsis.cns.codensa.model;

import java.math.BigDecimal;
import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * Interfaz del item de Encargo de Cobranza, para el reporte de duplicado de anexo de factura (Servicios Financieros)
 *    
 * @author jhack
 */
public interface ItemECO extends SynergiaBusinessObject{
	
	public String getNroEco();
	public void setNroEco(String nroEco);
	public String getIdEco();
	public void setIdEco(String idEco);
	public String getTitular();
	public void setTitular(String titular);
	public String getLineaNegocio();
	public void setLineaNegocio(String lineaNegocio);
	public String getSocioNegocio();
	public void setSocioNegocio(String socioNegocio);
	public String getTipoProducto();
	public void setTipoProducto(String tipoProducto);
	public String getNombrePlan();
	public void setNombrePlan(String nombrePlan);
	public BigDecimal getValorCuota();
	public void setValorCuota(BigDecimal valorCuota);
	public String getPerioricidad();
	public void setPerioricidad(String perioricidad);
	public Date getFechaCargue();
	public void setFechaCargue(Date fechaCargue);
	public Long getNroTotalCuotas();
	public void setNroTotalCuotas(Long nroTotalCuotas);
	public Long getCuotasFacturadas();
	public void setCuotasFacturadas(Long cuotasFacturadas);
	public Long getNroPeriodosImpagos();
	public void setNroPeriodosImpagos(Long nroPeriodosImpagos);
	public Date getFechaCompra();
	public void setFechaCompra(Date fechaCompra);
	public Long getCuotasFaltantes();
	public void setCuotasFaltantes(Long cuotasFaltantes);
	public BigDecimal getTotalServicios();
	public void setTotalServicios(BigDecimal totalServicios);
	public BigDecimal getTotalCedulas();
	public void setTotalCedulas(BigDecimal totalCedulas);
	
	
//	public BigDecimal getValorAPagar();
//	public void setValorAPagar(BigDecimal valorAPagar);
//	public String getDescripcion();
//	public void setDescripcion(String descripcion);
//
//	
//	// Suscripciones
//	public Date getFechaSuscripcion();
//	public void setFechaSuscripcion(Date fechaSuscripcion);
//	public BigDecimal getValorSuscripcion();
//	public void setValorSuscripcion(BigDecimal valorSuscripcion);
//	public Long getCuotaAPagar();
//	public void setCuotaAPagar(Long cuotaAPagar);
//	public Long getCuotaTotal();
//	public void setCuotaTotal(Long cuotaTotal);
//	
//	// Clasificados
//	public Date getFechaPublicacion();
//	public void setFechaPublicacion(Date fechaPublicacion);
//	public BigDecimal getValorAviso();
//	public void setValorAviso(BigDecimal valorAviso);
//	
//	//Seguros
//	public Date getFechaAfiliacion();
//	public void setFechaAfiliacion(Date fechaAfiliacion);
//	public Long getAntiguedad();
//	public void setAntiguedad(Long antiguedad);
}
