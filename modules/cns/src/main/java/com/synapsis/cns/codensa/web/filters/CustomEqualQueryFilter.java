package com.synapsis.cns.codensa.web.filters;

import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;

/**
 * Sobrecarga del equalQueryFilter para poder trabajar con diff servicios para
 * un mismo BB
 * 
 * @author Emiliano Arango (ar30557486)
 * 
 */
public class CustomEqualQueryFilter extends EqualQueryFilter implements
		CustomQueryFilter {

	private String serviceName;

	public String getServiceName() {
		return this.serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

}
