package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.TipoMedidaMedidor;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class TipoMedidaMedidorImpl extends SynergiaBusinessObjectImpl 
	implements TipoMedidaMedidor {
    // Fields    
	

    private String tipoMedida;
    
    private Long idComponente;

	public String getTipoMedida() {
		return tipoMedida;
	}

	public void setTipoMedida(String tipoMedida) {
		this.tipoMedida = tipoMedida;
	}

	public Long getIdComponente() {
		return idComponente;
	}

	public void setIdComponente(Long idComponente) {
		this.idComponente = idComponente;
	}


}
