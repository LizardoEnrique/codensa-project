package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface CongOper extends SynergiaBusinessObject {

	/**
	 * @return the nroOperacion
	 */
	public Long getNroOperacion();
	/**
	 * @param nroOperacion the nroOperacion to set
	 */
	public void setNroOperacion(Long nroOperacion);

	/**
	 * @return the state
	 */
	public String getState();

	/**
	 * @param state the state to set
	 */
	public void setState(String state);

	/**
	 * @return the valorOperacion
	 */
	public Double getValorOperacion();

	/**
	 * @param valorOperacion the valorOperacion to set
	 */
	public void setValorOperacion(Double valorOperacion);

	/**
	 * @return the fechaRealizacionCongelacion
	 */
	public Date getFechaRealizacionCongelacion();

	/**
	 * @param fechaRealizacionCongelacion the fechaRealizacionCongelacion to set
	 */
	public void setFechaRealizacionCongelacion(Date fechaRealizacionCongelacion);
	/**
	 * @return the fechaAprobacionCongelacion
	 */
	public Date getFechaAprobacionCongelacion();

	/**
	 * @param fechaAprobacionCongelacion the fechaAprobacionCongelacion to set
	 */
	public void setFechaAprobacionCongelacion(Date fechaAprobacionCongelacion);

	/**
	 * @return the fechaRealizacionDescongelacion
	 */
	public Date getFechaRealizacionDescongelacion();

	/**
	 * @param fechaRealizacionDescongelacion the fechaRealizacionDescongelacion to set
	 */
	public void setFechaRealizacionDescongelacion(
			Date fechaRealizacionDescongelacion);

	/**
	 * @return the fechaAprobacionDescongelacion
	 */
	public Date getFechaAprobacionDescongelacion();

	/**
	 * @param fechaAprobacionDescongelacion the fechaAprobacionDescongelacion to set
	 */
	public void setFechaAprobacionDescongelacion(Date fechaAprobacionDescongelacion);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroSaldo the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

}