package com.synapsis.cns.codensa.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.integration.Module;
import com.suivant.arquitectura.core.queryFilter.CompositeQueryFilter;
import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;
import com.synapsis.cns.codensa.model.ItemECO;
import com.synapsis.cns.codensa.model.ItemServicioFinancieroCCA;
import com.synapsis.cns.codensa.model.LineaFactura;
import com.synapsis.cns.codensa.model.ReporteAnexoFactura;
import com.synapsis.cns.codensa.model.ServicioFinancieroCCA;
import com.synapsis.cns.codensa.model.constants.AnexoFacturaConstants;
import com.synapsis.cns.codensa.model.impl.ItemECOImpl;
import com.synapsis.cns.codensa.model.impl.ItemServicioFinancieroCCAImpl;
import com.synapsis.cns.codensa.model.impl.ReporteAnexoFacturaImpl;
import com.synapsis.cns.codensa.model.impl.ServicioFinancieroCCAImpl;
import com.synapsis.cns.codensa.service.ReporteAnexoFacturaService;
import com.synapsis.cns.codensa.service.ReporteHelperService;
import com.synapsis.synergia.core.service.impl.SynergiaServiceImpl;

/**
 * Logica para imprimir el anexo de la factura.
 * @author jhack
 */
public class ReporteAnexoFacturaServiceImpl extends SynergiaServiceImpl
		implements ReporteAnexoFacturaService {

	private ReporteHelperService reporteHelperService;
	
	public ReporteAnexoFacturaServiceImpl() {
		super();
	}
	public ReporteAnexoFacturaServiceImpl(Module module) {
		super(module);
	
	}
	
	/**
	 * Surge la necesidad de hacer este m�todo para poder aplicar el validator
	 * NumeroCuentaQueryFilterValidation. El Constructor de este validator recibe como par�metro un Filtro, el
	 * cual obtiene del par�metro del m�todo de b�squeda del Service. Como hasta ahora nuestro Service recib�a
	 * por par�metro (Date, Long) fallaba al intentar instanciar el Validator con un Date como par�metro.<br>
	 * La l�gica de la b�squeda sigue siendo la que estaba en los respectivos m�todos de los Servicios, s�lo
	 * modifico la forma de acceder al Service.
	 * 
	 * @author egrande
	 * 
	 */
	public ReporteAnexoFactura getReporteAnexoFactura(CompositeQueryFilter compositeQueryFilter) throws ObjectNotFoundException {
		Long nroCuenta = null;
		Date fechaFactura = new Date();
		for (Iterator it = compositeQueryFilter.getFiltersList().iterator(); it.hasNext();) {
			EqualQueryFilter queryFilter = (EqualQueryFilter) it.next();
			if ("nroCuenta".equals(queryFilter.getAttributeName())) {
				nroCuenta = (Long) queryFilter.getAttributeValue();
			}
			if ("fechaFactura".equals(queryFilter.getAttributeName())) {
				fechaFactura = (Date) queryFilter.getAttributeValue();
			}
		}

		return this.getReporteAnexoFactura(fechaFactura, nroCuenta);
	}

	/*
	 * Devuelve un reporte anexo factura populado con los datos
	 */
	private ReporteAnexoFactura getReporteAnexoFactura(Date fechaFactura, Long nroCuenta) throws ObjectNotFoundException{

		ReporteAnexoFacturaImpl reporteAnexo = new ReporteAnexoFacturaImpl();

		LineaFactura 		lineaFactura 	= getHelper().getDataLine(fechaFactura, nroCuenta, reporteAnexo);

		// Cabecera del ANEXO
		String  nroCuentaFromDB = getHelper().getDato(AnexoFacturaConstants.FIELD_numero_cuenta, lineaFactura);
		String  direccion = getHelper().getDato(AnexoFacturaConstants.FIELD_direccion_reparto, lineaFactura);
//		String  telefono = getHelper().getDato(AnexoFacturaConstants.FIELD_TELEF_CLIENTE, lineaFactura);
		String  periodoInicio = getHelper().getDato(AnexoFacturaConstants.FIELD_fecha_compra, lineaFactura);
		String  periodoFin = getHelper().getDato(AnexoFacturaConstants.FIELD_fecha_expedicion, lineaFactura);
		String  rutaReparto = this.getRuta(lineaFactura);
		
		reporteAnexo.setNroCuenta(nroCuenta);
		reporteAnexo.setDireccion(direccion);
//		reporteAnexo.setTelefono(telefono);
		reporteAnexo.setPeriodoInicio(getHelper().getDate(periodoInicio));
		reporteAnexo.setPeriodoFin(getHelper().getDate(periodoFin));
		reporteAnexo.setRutaReparto(rutaReparto);
		

		// Servicios CCA
		Collection serviciosCCA = this.getServiciosFinancierosCCA(lineaFactura);
		reporteAnexo.setServiciosCCA(serviciosCCA);
		
		// Servicios de Encargos de Cobranza (ECO)
		Collection serviciosECO = this.getServiciosEncargosCobranza(lineaFactura);
		reporteAnexo.setServiciosECO(serviciosECO);
		
		return  reporteAnexo;
	}

	private String getRuta(LineaFactura linea) {
		//Ruta 
		String  zonaLectura = getHelper().getDato(AnexoFacturaConstants.FIELD_zona_reparto, linea);
		String  cicloLectura = getHelper().getDato(AnexoFacturaConstants.FIELD_ciclo_reparto, linea);
		String  grupoLectura = getHelper().getDato(AnexoFacturaConstants.FIELD_grupo_reparto, linea);
		String  sucursalLectura = getHelper().getDato(AnexoFacturaConstants.FIELD_sucursal_reparto, linea);
		String  rutaLectura = zonaLectura.concat(cicloLectura).concat(grupoLectura).concat(sucursalLectura);
		return 	rutaLectura;
	}
	
	/**
	 * Devuelve los servicios CCA (Servicios Financieros Crediticios) del String de datos del anexo
	 * @param linea
	 * @return
	 */
	
	public Collection getServiciosFinancierosCCA(LineaFactura linea) {
		HashMap agrupamiento = new HashMap();
		Collection serviciosFinancCCA = new ArrayList();
		int cantServFinanc = getReporteHelperService().getCantServFinancAnexo(linea);

		if(linea.getDatosAnexo() != null){
			int servFinancCCASize 	= 	this.getHelper().getCCASize(linea);
			int inicioCamposCCA 	= 	this.getHelper().getInicioCamposCCAAnexo(linea);
			int longCamposCCA 		=  	cantServFinanc * servFinancCCASize;
			
			// Bloque de datos con los serv financieros CCA
			String allCCASubString = linea.getDatosAnexo().substring(inicioCamposCCA, inicioCamposCCA + longCamposCCA);
		
			int posInic = 0;
			// vamos parseando los subbloques con la info de cada uno de los ITEMS de los serv financieros y los agrupamos por CCA ...
			Set itemsServiciosFinancCCA = new HashSet();
			for(int i=0; i<cantServFinanc;i++){
				ItemServicioFinancieroCCA itemServFinacCCA = new ItemServicioFinancieroCCAImpl();
					
				String itemSubString = allCCASubString.substring(posInic, posInic + servFinancCCASize);
									
				String titular 	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_nombre_titular, linea, itemSubString);
				String nroServicio 	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_nro_servicio_financiero, linea, itemSubString);
				String tipoProducto = getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_tipo_producto, linea, itemSubString);
				
				String cupoTotal 	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_cupo_aprobado, linea, itemSubString);
				String cupoDisponible 	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_cupo_disponible, linea, itemSubString);
//				String pagoMinimo 	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_total_cedulas, linea, itemSubString);
				String saldoEnMora 	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_saldo_serv_financiero, linea, itemSubString);
				String interesPorMora 	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_tasa_interes_mora, linea, itemSubString);
				String pagoTotal 	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_capital_facturado, linea, itemSubString);
				String tasaInteres 	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_tasa_efectiva_mensual, linea, itemSubString);
					
				String fecha = getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_fecha_compra, linea, itemSubString);
				String descripcion = getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_nombre_plan, linea, itemSubString);
				String cuotaActual = getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_nro_cuotas_facturadas, linea, itemSubString);
				String totalDeCuotas = getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_nro_total_cuotas, linea, itemSubString);
				String valorCompra = getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_precio_producto, linea, itemSubString);
				String cuotaACapital = getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_capital_facturado, linea, itemSubString);
				String cuotaAInteres = getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_intereses_facturados, linea, itemSubString);
				String saldoAPagar = getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_saldo_serv_financiero, linea, itemSubString);
				String valorCuota = getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_valor_cuota_mensual, linea, itemSubString);
				
				// datos del item comunes
				itemServFinacCCA.setTitular(titular);
				itemServFinacCCA.setNroServicio(getHelper().getLong(nroServicio));
				itemServFinacCCA.setTipoProducto(tipoProducto);
				
				// datos del item para agrupar/sumar y mostrar en la cabecera del servicio CCA 
				itemServFinacCCA.setCupoTotal(getHelper().getLong(cupoTotal));
				itemServFinacCCA.setCupoTotal(getHelper().getLong(cupoDisponible));
//				itemServFinacCCA.setPagoMinimo(getHelper().getBigDecimal(pagoMinimo));
				itemServFinacCCA.setSaldoEnMora(getHelper().getBigDecimal(saldoEnMora));
				itemServFinacCCA.setInteresPorMora(getHelper().getBigDecimal(interesPorMora));
				itemServFinacCCA.setPagoTotal(getHelper().getBigDecimal(pagoTotal));
				itemServFinacCCA.setTasaInteres(getHelper().getDouble(tasaInteres));
				
				// Datos del item para mostrar en el detalle
				itemServFinacCCA.setFecha(getHelper().getDate(fecha));
				itemServFinacCCA.setDescripcion(descripcion);
				itemServFinacCCA.setCuotaActual(getHelper().getLong(cuotaActual));
				itemServFinacCCA.setTotalDeCuotas(getHelper().getLong(totalDeCuotas));
				itemServFinacCCA.setValorCompra(getHelper().getBigDecimal(valorCompra));
				itemServFinacCCA.setCuotaACapital(getHelper().getBigDecimal(cuotaACapital));
				itemServFinacCCA.setCuotaAInteres(getHelper().getBigDecimal(cuotaAInteres));
				itemServFinacCCA.setSaldoAPagar(getHelper().getBigDecimal(saldoAPagar));
				itemServFinacCCA.setValorCuota(getHelper().getBigDecimal(valorCuota));

				posInic = posInic + servFinancCCASize;
				
				// Agrupamos por nro de servicio
				
				addToGrupo(agrupamiento, itemServFinacCCA);
			}
	
			serviciosFinancCCA.addAll(agrupamiento.values());
		}

		return serviciosFinancCCA;
	}
	
	
	protected void addToGrupo(HashMap agrupamiento, ItemServicioFinancieroCCA item){
		// Agrupamos por nro del servicio ...
		Long nroServicio = item.getNroServicio();
		
		if (! agrupamiento.containsKey(nroServicio)){
			// Si no existe lo agregamos
			ServicioFinancieroCCA servFinCCA = null;
			servFinCCA = new ServicioFinancieroCCAImpl();
			
			Set itemsCCA = new HashSet();
			String titular = item.getTitular();
			String tipoProducto = item.getTipoProducto();

			servFinCCA.setNroServicio(nroServicio);
			servFinCCA.setTitular(titular);
			servFinCCA.setItemsCCA(itemsCCA);
			servFinCCA.setTipoProducto(tipoProducto);
			
			agrupamiento.put(item.getNroServicio(), servFinCCA);
		}
		
		((ServicioFinancieroCCA) agrupamiento.get(item.getNroServicio())).getItemsCCA().add(item);
	}
	
	
	
	
	
	
	

	public Collection getServiciosEncargosCobranza(LineaFactura linea) {
		Collection itemsEco = new ArrayList();
		int cantECOs = getReporteHelperService().getCantECOAnexo(linea);

		if(linea.getDatosAnexo() != null){
			int ecoSize 			= 	this.getHelper().getECOSize(linea);
			int inicioCamposECO 	= 	this.getHelper().getInicioCamposECOAnexo(linea);
			int longCamposECO 		=  	cantECOs * ecoSize;
			
			// Bloque de datos con los ECO
			String allECOSubString = linea.getDatosAnexo().substring(inicioCamposECO, inicioCamposECO + longCamposECO);
		
			int posInic = 0;
			for(int i=0; i<cantECOs;i++){

				ItemECO itemEco = new ItemECOImpl();
					
				String ecoSubString = allECOSubString.substring(posInic, posInic + ecoSize);
				
				String nroEco	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_nro_eco, linea, ecoSubString);
				String idEco	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_id_eco, linea, ecoSubString);
				String titular	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_titular_eco, linea, ecoSubString);
				String lineaNegocio	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_linea_negocio_eco, linea, ecoSubString);
				String socioNegocio	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_socio_negocio_eco, linea, ecoSubString);
				String tipoProducto	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_tipo_producto_eco, linea, ecoSubString);
				String nombrePlan	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_plan_eco, linea, ecoSubString);
				String  valorCuota	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_valor_cuota_eco, linea, ecoSubString);
				String  perioricidad	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_perioricidad_cuota_eco, linea, ecoSubString);
				String  fechaCargue	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_fecha_cargue_eco, linea, ecoSubString);
				String  nroTotalCuotas	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_nro_total_cuotas, linea, ecoSubString);
				String  cuotasFacturadas	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_cuotas_facturadas_eco, linea, ecoSubString);
				String  nroPeriodosImpagos	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_nro_periodos_impagos_eco, linea, ecoSubString);
				String  fechaCompra	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_fecha_compra_eco, linea, ecoSubString);
				String  cuotasFaltantes	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_cuotas_faltantes_eco, linea, ecoSubString);
//				String  totalServicios	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_total_servicios_eco, linea, ecoSubString);
//				String  totalCedulas	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_total_cedulas_eco, linea, ecoSubString);

				itemEco.setNroEco(nroEco);
				itemEco.setIdEco(idEco);
				itemEco.setTitular(titular);
				itemEco.setLineaNegocio(lineaNegocio);
				itemEco.setSocioNegocio(socioNegocio);
				itemEco.setTipoProducto(tipoProducto);
				itemEco.setNombrePlan(nombrePlan);
				itemEco.setValorCuota(getHelper().getBigDecimal(valorCuota));
				itemEco.setPerioricidad(perioricidad);
				itemEco.setFechaCargue(getHelper().getDate(fechaCargue));
				itemEco.setNroTotalCuotas(getHelper().getLong(nroTotalCuotas));
				itemEco.setCuotasFacturadas(getHelper().getLong(cuotasFacturadas));
				itemEco.setNroPeriodosImpagos(getHelper().getLong(nroPeriodosImpagos));
				itemEco.setFechaCompra(getHelper().getDate(fechaCompra));
				itemEco.setCuotasFaltantes(getHelper().getLong(cuotasFaltantes));
//				itemEco.setTotalServicios(getHelper().getBigDecimal(totalServicios));
//				itemEco.setTotalCedulas(getHelper().getBigDecimal(totalCedulas));
				
	
				
				
//				String descripcion 	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_plan_eco, linea, ecoSubString);
//				String valorAPagar 	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_total_todos_servicios_eco, linea, ecoSubString);
//				String fechaPublicacion 	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_fecha_compra, linea, ecoSubString);
//				String valorAviso 	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_total_todos_servicios_eco, linea, ecoSubString);
//				String fechaAfiliacion 	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_fecha_compra_eco, linea, ecoSubString);
//				String antiguedad 	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_titular_eco, linea, ecoSubString);
//				String fechaSuscripcion 	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_titular_eco, linea, ecoSubString);
//				String cuotaTotal 	= getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_titular_eco, linea, ecoSubString);
//				String valorSuscripcion = getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_titular_eco, linea, ecoSubString);;
//				String cuotaAPagar = getReporteHelperService().getDatoVariable(AnexoFacturaConstants.FIELD_titular_eco, linea, ecoSubString);
//
//				
//				itemEco.setDescripcion(descripcion);
//				itemEco.setValorAPagar(getHelper().getBigDecimal(valorAPagar));	
//
//				// Clasificados
//				itemEco.setFechaPublicacion(getHelper().getDate(fechaPublicacion));
//				itemEco.setValorAviso(getHelper().getBigDecimal(valorAviso));
//				
//				// Seguros 
//				itemEco.setFechaAfiliacion(getHelper().getDate(fechaAfiliacion));
//				itemEco.setAntiguedad(getHelper().getLong(antiguedad));
//				
//				// Suscripciones
//				itemEco.setFechaSuscripcion(getHelper().getDate(fechaSuscripcion));
//				itemEco.setValorSuscripcion(getHelper().getBigDecimal(valorSuscripcion));
//				itemEco.setCuotaAPagar(getHelper().getLong(cuotaAPagar));
//				itemEco.setCuotaTotal(getHelper().getLong(cuotaTotal));

				posInic = posInic + ecoSize;
				itemsEco.add(itemEco);
			}
		}
			
		return itemsEco;
	}	
	
	
	private ReporteHelperService getHelper(){
//		return new ReporteHelperServiceImpl();
		return this.getReporteHelperService();
	}
	
	public ReporteHelperService getReporteHelperService() {
		return reporteHelperService;
	}
	
	public void setReporteHelperService(ReporteHelperService reporteHelperService) {
		this.reporteHelperService = reporteHelperService;
	}

}