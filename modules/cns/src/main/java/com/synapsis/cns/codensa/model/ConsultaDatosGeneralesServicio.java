package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface ConsultaDatosGeneralesServicio {

	/**
	 * @return the cantCuotFact
	 */
	public Double getCantCuotFact();

	/**
	 * @param cantCuotFact the cantCuotFact to set
	 */
	public void setCantCuotFact(Double cantCuotFact);

	/**
	 * @return the cantCuotRestantes
	 */
	public Double getCantCuotRestantes();

	/**
	 * @param cantCuotRestantes the cantCuotRestantes to set
	 */
	public void setCantCuotRestantes(Double cantCuotRestantes);

	/**
	 * @return the diasEnMora
	 */
	public Double getDiasEnMora();

	/**
	 * @param diasEnMora the diasEnMora to set
	 */
	public void setDiasEnMora(Double diasEnMora);

	/**
	 * @return the fechaCompra
	 */
	public Date getFechaCompra();

	/**
	 * @param fechaCompra the fechaCompra to set
	 */
	public void setFechaCompra(Date fechaCompra);

	/**
	 * @return the lineaNegocio
	 */
	public String getLineaNegocio();

	/**
	 * @param lineaNegocio the lineaNegocio to set
	 */
	public void setLineaNegocio(String lineaNegocio);

	/**
	 * @return the marcaProducto
	 */
	public String getMarcaProducto();

	/**
	 * @param marcaProducto the marcaProducto to set
	 */
	public void setMarcaProducto(String marcaProducto);

	/**
	 * @return the nroCuotas
	 */
	public Long getNroCuotas();

	/**
	 * @param nroCuotas the nroCuotas to set
	 */
	public void setNroCuotas(Long nroCuotas);

	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio();

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio);

	/**
	 * @return the periodosAtrasados
	 */
	public Double getPeriodosAtrasados();

	/**
	 * @param periodosAtrasados the periodosAtrasados to set
	 */
	public void setPeriodosAtrasados(Double periodosAtrasados);

	/**
	 * @return the saldoCapital
	 */
	public Double getSaldoCapital();

	/**
	 * @param saldoCapital the saldoCapital to set
	 */
	public void setSaldoCapital(Double saldoCapital);

	/**
	 * @return the saldoIntCorriente
	 */
	public Double getSaldoIntCorriente();

	/**
	 * @param saldoIntCorriente the saldoIntCorriente to set
	 */
	public void setSaldoIntCorriente(Double saldoIntCorriente);

	/**
	 * @return the saldoIntMora
	 */
	public Double getSaldoIntMora();

	/**
	 * @param saldoIntMora the saldoIntMora to set
	 */
	public void setSaldoIntMora(Double saldoIntMora);

	/**
	 * @return the socioNegocio
	 */
	public String getSocioNegocio();

	/**
	 * @param socioNegocio the socioNegocio to set
	 */
	public void setSocioNegocio(String socioNegocio);

	/**
	 * @return the sucursalSocio
	 */
	public String getSucursalSocio();

	/**
	 * @param sucursalSocio the sucursalSocio to set
	 */
	public void setSucursalSocio(String sucursalSocio);

	/**
	 * @return the tasaIntActual
	 */
	public Double getTasaIntActual();

	/**
	 * @param tasaIntActual the tasaIntActual to set
	 */
	public void setTasaIntActual(Double tasaIntActual);

	/**
	 * @return the tasaIntOrigen
	 */
	public Double getTasaIntOrigen();

	/**
	 * @param tasaIntOrigen the tasaIntOrigen to set
	 */
	public void setTasaIntOrigen(Double tasaIntOrigen);

	/**
	 * @return the tipoProducto
	 */
	public String getTipoProducto();

	/**
	 * @param tipoProducto the tipoProducto to set
	 */
	public void setTipoProducto(String tipoProducto);

	/**
	 * @return the valCapitalMora
	 */
	public Double getValCapitalMora();

	/**
	 * @param valCapitalMora the valCapitalMora to set
	 */
	public void setValCapitalMora(Double valCapitalMora);

	/**
	 * @return the valInteresMora
	 */
	public Double getValInteresMora();

	/**
	 * @param valInteresMora the valInteresMora to set
	 */
	public void setValInteresMora(Double valInteresMora);

	/**
	 * @return the valorServicio
	 */
	public Double getValorServicio();

	/**
	 * @param valorServicio the valorServicio to set
	 */
	public void setValorServicio(Double valorServicio);

}