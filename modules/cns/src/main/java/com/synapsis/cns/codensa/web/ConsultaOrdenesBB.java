package com.synapsis.cns.codensa.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.myfaces.shared_impl.util.MessageUtils;

import com.suivant.arquitectura.core.queryFilter.CompositeQueryFilter;
import com.suivant.arquitectura.core.queryFilter.SimpleQueryFilter;
import com.suivant.arquitectura.presentation.model.ListableBB;
import com.suivant.arquitectura.presentation.utils.ComboFactory;
import com.suivant.arquitectura.presentation.utils.ComboUtils;
import com.suivant.arquitectura.presentation.utils.JSFUtils;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.synapsis.integration.codensa.backingBeans.helpers.DataTableFactoryRefresh;
import com.synapsis.synergia.core.components.dataTable.ScrollableList;

public class ConsultaOrdenesBB extends ListableBB implements BackingBeansConstants{

	//combo tipo servicio
	private SelectItem[] items1;
	//combo tipo de orden
	private SelectItem[] items2;
	//combo estado de ordenes
	private SelectItem[] items3;
	
	public void fillEstadosOrdenes(){
		SimpleQueryFilter sq = (SimpleQueryFilter)VariableResolverUtils.getObject(QUERY_FILTER_ORDENES_ESTADO);
		
		if (!sq.getAttributeValue().equals("")){
			this.items3 = ComboFactory.makeCombo(SRV_COMBO_ESTADOS_ORDENES, QUERY_FILTER_ORDENES_ESTADO,
					COMBO_DEFAULT_LABEL, COMBO_DEFAULT_VALUE);
		}else{
			this.items3 = makeEmptySelectItem();
		}
		
	}
	
//combos 	
	public SelectItem[] getItems1() {
		if (items1==null){
			List result = new ArrayList();
			Map element0 = new HashMap();
			element0.put(COMBO_DEFAULT_ID, "Electrico");
			element0.put(COMBO_DEFAULT_LABEL, "Electrico");
			Map element1 = new HashMap();
			element1.put(COMBO_DEFAULT_ID, "Venta");
			element1.put(COMBO_DEFAULT_LABEL, "Venta");
			Map element2 = new HashMap();
			element2.put(COMBO_DEFAULT_ID, "Financiacion");
			element2.put(COMBO_DEFAULT_LABEL, "Financiacion");
			Map element3 = new HashMap();
			element3.put(COMBO_DEFAULT_ID, "Convenio");
			element3.put(COMBO_DEFAULT_LABEL, "Convenio");
			Map element4 = new HashMap();
			element4.put(COMBO_DEFAULT_ID, "EncargoCobranza");
			element4.put(COMBO_DEFAULT_LABEL, "Encargo de Cobranza");
			result.add(element0);
			result.add(element1);
			result.add(element2);
			result.add(element3);
			result.add(element4);
			items1=new ComboUtils(COMBO_DEFAULT_ID, COMBO_DEFAULT_LABEL).createSelecItems(result);
		}
		return items1; 

	}
	public SelectItem[] getItems2() {
		if (items2==null){
			items2 = ComboFactory.makeCombo(SRV_COMBO_ORDENES, QUERY_FILTER_COMBO, 
					COMBO_DEFAULT_LABEL, COMBO_DEFAULT_VALUE);
		}
		return items2;
	}	
	public SelectItem[] getItems3() {
		if (items3==null){
			items3= makeEmptySelectItem();
		}
		return items3;
	}

	
//	devuelve una array de SelectItem con un solo elemento vacio
	private SelectItem[] makeEmptySelectItem() {
		SelectItem[] select = new SelectItem[1];
		select[0] = new SelectItem("","");
		return select;
	}

	public String ordenesQueryFilter() {
		//obtengo el filtro del contexto
		CompositeQueryFilter queryFilter = (CompositeQueryFilter)VariableResolverUtils.
			getObject(QUERY_FILTER_CONSULTA_ORDENES);
		//realizo el refresco de la tabla
		DataTableFactoryRefresh.makeDataTable("ordenesForm:ordenesTable", queryFilter, 
				getMultiList().getUniqueList().getServiceName(), getMultiList().getUniqueList().getMetadata());
		
		HtmlDataTable dataTable = (HtmlDataTable) JSFUtils.getComponentFromTree("ordenesForm:ordenesTable");
		
		if (dataTable != null) {
			if (((ScrollableList)dataTable.getValue()).size() == 0) {
				FacesContext
				.getCurrentInstance()
				.addMessage(
						null,
						MessageUtils
								.getMessage(
										"consulta.ordenes.no.hay.observaciones",
										null));
			}
		}
		return null;
	}	
	
//setter de la properties	

	public void setItems1(SelectItem[] items1) {
		this.items1 = items1;
	}

	public void setItems2(SelectItem[] items2) {
		this.items2 = items2;
	}

	public void setItems3(SelectItem[] items3) {
		this.items3 = items3;
	}
	

}
