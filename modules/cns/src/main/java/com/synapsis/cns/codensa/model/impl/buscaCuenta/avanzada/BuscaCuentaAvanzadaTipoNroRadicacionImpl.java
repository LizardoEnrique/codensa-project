/**
 * 
 */
package com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada;

import com.synapsis.cns.codensa.model.buscaCuenta.avanzada.BuscaCuentaAvanzadaTipoNroRadicacion;
import com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaImpl;

/**
 * @author Emiliano Arango (ar30557486)
 * 
 */
public class BuscaCuentaAvanzadaTipoNroRadicacionImpl extends BuscaCuentaImpl
		implements BuscaCuentaAvanzadaTipoNroRadicacion {

	private String tipoRadicacion;
	private Integer nroRadicacion;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada.BuscaCuentaAvanzadaTipoNroRadicacion#getNroRadicacion()
	 */
	public Integer getNroRadicacion() {
		return nroRadicacion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada.BuscaCuentaAvanzadaTipoNroRadicacion#getTipoRadicacion()
	 */
	public String getTipoRadicacion() {
		return tipoRadicacion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada.BuscaCuentaAvanzadaTipoNroRadicacion#setNroRadicacion(java.lang.Integer)
	 */
	public void setNroRadicacion(Integer nroRadicacion) {
		this.nroRadicacion = nroRadicacion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada.BuscaCuentaAvanzadaTipoNroRadicacion#setTipoRadicacion(java.lang.String)
	 */
	public void setTipoRadicacion(String tipoRadicacion) {
		this.tipoRadicacion = tipoRadicacion;
	}

}
