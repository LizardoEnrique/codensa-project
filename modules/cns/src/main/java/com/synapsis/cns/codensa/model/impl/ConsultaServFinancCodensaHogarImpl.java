/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaServFinancCodensaHogar;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 * 
 */
public class ConsultaServFinancCodensaHogarImpl extends SynergiaBusinessObjectImpl implements
		ConsultaServFinancCodensaHogar {

	private Date fechaActivProd;

	private Date fechaDesactProd;

	private Long nroCuenta;

	private Long nroServicio;

	private String valorOrigSrv;

	private Long nroCuotasOri;

	private Double tasaInteres;

	private Long nroCuotasFac;

	private Long nroCuotasFaltantes;

	private String valorCuota;

	private String saldoCapital;

	private String saldoIntereses;

	private String saldoIntMora;

	private Long periodAtrasado;

	private String deudaActual;

	private String estado;

	private String nomTitular;

	private String docTitular;

	private String deudaCapital;

	private String deudaInteres;

	private String lineaNegocio;

	/**
	 * @return the fechaActivProd
	 */
	public Date getFechaActivProd() {
		return fechaActivProd;
	}

	/**
	 * @param fechaActivProd the fechaActivProd to set
	 */
	public void setFechaActivProd(Date fechaActivProd) {
		this.fechaActivProd = fechaActivProd;
	}

	/**
	 * @return the fechaDesactProd
	 */
	public Date getFechaDesactProd() {
		return fechaDesactProd;
	}

	/**
	 * @param fechaDesactProd the fechaDesactProd to set
	 */
	public void setFechaDesactProd(Date fechaDesactProd) {
		this.fechaDesactProd = fechaDesactProd;
	}

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio() {
		return nroServicio;
	}

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}

	/**
	 * @return the valorOrigSrv
	 */
	public String getValorOrigSrv() {
		return valorOrigSrv;
	}

	/**
	 * @param valorOrigSrv the valorOrigSrv to set
	 */
	public void setValorOrigSrv(String valorOrigSrv) {
		this.valorOrigSrv = valorOrigSrv;
	}

	/**
	 * @return the nroCuotasFac
	 */
	public Long getNroCuotasFac() {
		return nroCuotasFac;
	}

	/**
	 * @param nroCuotasFac the nroCuotasFac to set
	 */
	public void setNroCuotasFac(Long nroCuotasFac) {
		this.nroCuotasFac = nroCuotasFac;
	}

	/**
	 * @return the nroCuotasFaltantes
	 */
	public Long getNroCuotasFaltantes() {
		return nroCuotasFaltantes;
	}

	/**
	 * @param nroCuotasFaltantes the nroCuotasFaltantes to set
	 */
	public void setNroCuotasFaltantes(Long nroCuotasFaltantes) {
		this.nroCuotasFaltantes = nroCuotasFaltantes;
	}

	/**
	 * @return the nroCuotasOri
	 */
	public Long getNroCuotasOri() {
		return nroCuotasOri;
	}

	/**
	 * @param nroCuotasOri the nroCuotasOri to set
	 */
	public void setNroCuotasOri(Long nroCuotasOri) {
		this.nroCuotasOri = nroCuotasOri;
	}

	/**
	 * @return the periodAtrasado
	 */
	public Long getPeriodAtrasado() {
		return periodAtrasado;
	}

	/**
	 * @param periodAtrasado the periodAtrasado to set
	 */
	public void setPeriodAtrasado(Long periodAtrasado) {
		this.periodAtrasado = periodAtrasado;
	}

	/**
	 * @return the saldoCapital
	 */
	public String getSaldoCapital() {
		return saldoCapital;
	}

	/**
	 * @param saldoCapital the saldoCapital to set
	 */
	public void setSaldoCapital(String saldoCapital) {
		this.saldoCapital = saldoCapital;
	}

	/**
	 * @return the saldoIntereses
	 */
	public String getSaldoIntereses() {
		return saldoIntereses;
	}

	/**
	 * @param saldoIntereses the saldoIntereses to set
	 */
	public void setSaldoIntereses(String saldoIntereses) {
		this.saldoIntereses = saldoIntereses;
	}

	/**
	 * @return the saldoIntMora
	 */
	public String getSaldoIntMora() {
		return saldoIntMora;
	}

	/**
	 * @param saldoIntMora the saldoIntMora to set
	 */
	public void setSaldoIntMora(String saldoIntMora) {
		this.saldoIntMora = saldoIntMora;
	}

	/**
	 * @return the tasaInteres
	 */
	public Double getTasaInteres() {
		return tasaInteres;
	}

	/**
	 * @param tasaInteres the tasaInteres to set
	 */
	public void setTasaInteres(Double tasaInteres) {
		this.tasaInteres = tasaInteres;
	}

	/**
	 * @return the valorCuota
	 */
	public String getValorCuota() {
		return valorCuota;
	}

	/**
	 * @param valorCuota the valorCuota to set
	 */
	public void setValorCuota(String valorCuota) {
		this.valorCuota = valorCuota;
	}

	/**
	 * @return the deudaActual
	 */
	public String getDeudaActual() {
		return deudaActual;
	}

	/**
	 * @param deudaActual the deudaActual to set
	 */
	public void setDeudaActual(String deudaActual) {
		this.deudaActual = deudaActual;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the deudaCapital
	 */
	public String getDeudaCapital() {
		return deudaCapital;
	}

	/**
	 * @param deudaCapital the deudaCapital to set
	 */
	public void setDeudaCapital(String deudaCapital) {
		this.deudaCapital = deudaCapital;
	}

	/**
	 * @return the deudaInteres
	 */
	public String getDeudaInteres() {
		return deudaInteres;
	}

	/**
	 * @param deudaInteres the deudaInteres to set
	 */
	public void setDeudaInteres(String deudaInteres) {
		this.deudaInteres = deudaInteres;
	}

	/**
	 * @return the nombreTitular
	 */
	public String getNomTitular() {
		return nomTitular;
	}

	/**
	 * @param nombreTitular the nombreTitular to set
	 */
	public void setNomTitular(String nomTitular) {
		this.nomTitular = nomTitular;
	}

	/**
	 * @return the nroDocumento
	 */
	public String getDocTitular() {
		return docTitular;
	}

	/**
	 * @param nroDocumento the nroDocumento to set
	 */
	public void setDocTitular(String docTitular) {
		this.docTitular = docTitular;
	}

	public String getLineaNegocio() {
		return this.lineaNegocio;
	}

	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}

}
