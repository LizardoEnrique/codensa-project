package com.synapsis.cns.codensa.web;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.apache.commons.lang.StringUtils;
import org.apache.myfaces.custom.datatable.ext.HtmlDataTable;
import org.apache.myfaces.custom.service.UIService;
import org.apache.myfaces.shared_impl.util.MessageUtils;

import com.suivant.arquitectura.core.logging.Logging;
import com.synapsis.cns.codensa.model.impl.ConsultaDatosTransformadorImpl;
import com.synapsis.nuc.codensa.utils.DateConverterUtils;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;
import com.synapsis.synergia.presentation.model.NasServiceHelper;

public class ConsultaDatosTransformadorBB {
	/**
	 * 
	 * @author Alex Valencia
	 * 
	 */

	private static Logging logger = SynergiaApplicationContext.getLogger();

	private static final long serialVersionUID = 1L;

	private String transformador;
	private UIService findDatosTransformadorByCriteriaService;
	private UIService findServiciosElectricosByNroTransformadorService;
	//private UIServiceUnmarshaller findServiciosElectricosByNroTransformadorServiceUnmarshaller;
	private HtmlDataTable serviciosElectricosDataTable;
	
	//datosTransformador BO
	private ConsultaDatosTransformadorImpl datosTransformador;
	private Date fechaProceso;

	public void search() {
		if(validateFields()){
			try {
				Map parametro = new HashMap();
				parametro.put("transformador", this.transformador);
				parametro.put("fechaProceso", DateConverterUtils.getDateFormatddmmyyyy(this.fechaProceso));
				List datos = (List)NasServiceHelper.getResponse(
				"datosTransformadorByNroFinderService", "findByCriteria",parametro);
				Iterator datosIter = datos.iterator();
				if(datosIter.hasNext()){
					datosTransformador = (ConsultaDatosTransformadorImpl)datosIter.next();
				}
				//this.getFindDatosTransformadorByCriteriaService().execute();
				this.getFindServiciosElectricosByNroTransformadorService()
						.execute();
			//	datosTransformador = getFindServiciosElectricosByNroTransformadorServiceUnmarshaller().getValue();
			} catch (Throwable e) {
				FacesContext
						.getCurrentInstance()
						.addMessage(
								null,
								MessageUtils
										.getMessage(
												"consultaDatosVinculoClienteRed.busqueda.error",
												null));
			}
			
		}
		this.getServiciosElectricosDataTable().setFirst(0);
	}
	
	private boolean validateFields(){
		boolean validate = true;
		if (StringUtils.isEmpty(this.getTransformador())) {
			FacesContext.getCurrentInstance().addMessage(null,MessageUtils.getMessage("servicio.nroTransformador.cns.requerido.error", null));
		    validate = false;
		}
		if (this.getFechaProceso() == null) {
			FacesContext.getCurrentInstance().addMessage(null,MessageUtils.getMessage("servicio.periodo.cns.requerido.error", null));
			validate = false;
		}
		return validate;
	}

	/**
	 * @return the transformador
	 */
	public String getTransformador() {
		return transformador;
	}

	/**
	 * @param transformador
	 *            the transformador to set
	 */
	public void setTransformador(String transformador) {
		this.transformador = transformador;
	}

	/**
	 * @return the findDatosTransformadorByCriteriaService
	 */
	public UIService getFindDatosTransformadorByCriteriaService() {
		return findDatosTransformadorByCriteriaService;
	}

	/**
	 * @param findDatosTransformadorByCriteriaService
	 *            the findDatosTransformadorByCriteriaService to set
	 */
	public void setFindDatosTransformadorByCriteriaService(
			UIService findDatosTransformadorByCriteriaService) {
		this.findDatosTransformadorByCriteriaService = findDatosTransformadorByCriteriaService;
	}

	/**
	 * @return the findServiciosElectricosByNroTransformadorService
	 */
	public UIService getFindServiciosElectricosByNroTransformadorService() {
		return findServiciosElectricosByNroTransformadorService;
	}

	/**
	 * @param findServiciosElectricosByNroTransformadorService
	 *            the findServiciosElectricosByNroTransformadorService to set
	 */
	public void setFindServiciosElectricosByNroTransformadorService(
			UIService findServiciosElectricosByNroTransformadorService) {
		this.findServiciosElectricosByNroTransformadorService = findServiciosElectricosByNroTransformadorService;
	}

	/**
	 * @return the serviciosElectricosDataTable
	 */
	public HtmlDataTable getServiciosElectricosDataTable() {
		return serviciosElectricosDataTable;
	}

	/**
	 * @param serviciosElectricosDataTable
	 *            the serviciosElectricosDataTable to set
	 */
	public void setServiciosElectricosDataTable(
			HtmlDataTable serviciosElectricosDataTable) {
		this.serviciosElectricosDataTable = serviciosElectricosDataTable;
	}

	/**
	 * @return the datosTransformador
	 */
	public ConsultaDatosTransformadorImpl getDatosTransformador() {
		return datosTransformador;
	}

	/**
	 * @param datosTransformador the datosTransformador to set
	 */
	public void setDatosTransformador(
			ConsultaDatosTransformadorImpl datosTransformador) {
		this.datosTransformador = datosTransformador;
	}

	public Date getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}
}
