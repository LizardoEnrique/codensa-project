/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaDetalleServiciosFinancieros;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaDetalleServiciosFinancierosImpl extends
		SynergiaBusinessObjectImpl implements ConsultaDetalleServiciosFinancieros {
	   private Double diasMora;            
	   private Double valorCap;            
	   private Double valorInt;            
	   private String solicitanteCred;     
	   private String nroIdSolicitante;    
	   private String resultEstCred;       
	   private Long montoAprobado;         
	   private Long montoDisponible;       
	   private Date fechaSolicCred;        
	   private String causalNegacion;      
	   private String estrato;             
	   private String ciclo;               
	   private Long nroCuenta;             
	   private String lineaNegocio;        
	   private Long nroServicio;           
	   private Date fechaCompraProd;       
	   private String nombreSocio;         
	   private String sucursalSocio;       
	   private String tipoProd;            
	   private String marcaProd;           
	   private Double valorOrigSrv;        
	   private String nroCuotas;           
	   private Double tasaInt;             
	   private String nroCuotasFac;        
	   private String nroCuotasRestantes;  
	   private Double tasaIntActual;       
	   private Double saldoCapital;        
	   private Double saldoIntereses;      
	   private Double saldoIntMora;        
	   private Double periodAtrasado;
	/**
	 * @return the causalNegacion
	 */
	public String getCausalNegacion() {
		return causalNegacion;
	}
	/**
	 * @param causalNegacion the causalNegacion to set
	 */
	public void setCausalNegacion(String causalNegacion) {
		this.causalNegacion = causalNegacion;
	}
	/**
	 * @return the ciclo
	 */
	public String getCiclo() {
		return ciclo;
	}
	/**
	 * @param ciclo the ciclo to set
	 */
	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
	/**
	 * @return the diasMora
	 */
	public Double getDiasMora() {
		return diasMora;
	}
	/**
	 * @param diasMora the diasMora to set
	 */
	public void setDiasMora(Double diasMora) {
		this.diasMora = diasMora;
	}
	/**
	 * @return the estrato
	 */
	public String getEstrato() {
		return estrato;
	}
	/**
	 * @param estrato the estrato to set
	 */
	public void setEstrato(String estrato) {
		this.estrato = estrato;
	}
	/**
	 * @return the fechaCompraProd
	 */
	public Date getFechaCompraProd() {
		return fechaCompraProd;
	}
	/**
	 * @param fechaCompraProd the fechaCompraProd to set
	 */
	public void setFechaCompraProd(Date fechaCompraProd) {
		this.fechaCompraProd = fechaCompraProd;
	}
	/**
	 * @return the fechaSolicCred
	 */
	public Date getFechaSolicCred() {
		return fechaSolicCred;
	}
	/**
	 * @param fechaSolicCred the fechaSolicCred to set
	 */
	public void setFechaSolicCred(Date fechaSolicCred) {
		this.fechaSolicCred = fechaSolicCred;
	}
	/**
	 * @return the lineaNegocio
	 */
	public String getLineaNegocio() {
		return lineaNegocio;
	}
	/**
	 * @param lineaNegocio the lineaNegocio to set
	 */
	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}
	/**
	 * @return the marcaProd
	 */
	public String getMarcaProd() {
		return marcaProd;
	}
	/**
	 * @param marcaProd the marcaProd to set
	 */
	public void setMarcaProd(String marcaProd) {
		this.marcaProd = marcaProd;
	}
	/**
	 * @return the montoAprobado
	 */
	public Long getMontoAprobado() {
		return montoAprobado;
	}
	/**
	 * @param montoAprobado the montoAprobado to set
	 */
	public void setMontoAprobado(Long montoAprobado) {
		this.montoAprobado = montoAprobado;
	}
	/**
	 * @return the montoDisponible
	 */
	public Long getMontoDisponible() {
		return montoDisponible;
	}
	/**
	 * @param montoDisponible the montoDisponible to set
	 */
	public void setMontoDisponible(Long montoDisponible) {
		this.montoDisponible = montoDisponible;
	}
	/**
	 * @return the nombreSocio
	 */
	public String getNombreSocio() {
		return nombreSocio;
	}
	/**
	 * @param nombreSocio the nombreSocio to set
	 */
	public void setNombreSocio(String nombreSocio) {
		this.nombreSocio = nombreSocio;
	}
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/**
	 * @return the nroCuotas
	 */
	public String getNroCuotas() {
		return nroCuotas;
	}
	/**
	 * @param nroCuotas the nroCuotas to set
	 */
	public void setNroCuotas(String nroCuotas) {
		this.nroCuotas = nroCuotas;
	}
	/**
	 * @return the nroCuotasFac
	 */
	public String getNroCuotasFac() {
		return nroCuotasFac;
	}
	/**
	 * @param nroCuotasFac the nroCuotasFac to set
	 */
	public void setNroCuotasFac(String nroCuotasFac) {
		this.nroCuotasFac = nroCuotasFac;
	}
	/**
	 * @return the nroCuotasRestantes
	 */
	public String getNroCuotasRestantes() {
		return nroCuotasRestantes;
	}
	/**
	 * @param nroCuotasRestantes the nroCuotasRestantes to set
	 */
	public void setNroCuotasRestantes(String nroCuotasRestantes) {
		this.nroCuotasRestantes = nroCuotasRestantes;
	}
	/**
	 * @return the nroIdSolicitante
	 */
	public String getNroIdSolicitante() {
		return nroIdSolicitante;
	}
	/**
	 * @param nroIdSolicitante the nroIdSolicitante to set
	 */
	public void setNroIdSolicitante(String nroIdSolicitante) {
		this.nroIdSolicitante = nroIdSolicitante;
	}
	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	/**
	 * @return the periodAtrasado
	 */
	public Double getPeriodAtrasado() {
		return periodAtrasado;
	}
	/**
	 * @param periodAtrasado the periodAtrasado to set
	 */
	public void setPeriodAtrasado(Double periodAtrasado) {
		this.periodAtrasado = periodAtrasado;
	}
	/**
	 * @return the resultEstCred
	 */
	public String getResultEstCred() {
		return resultEstCred;
	}
	/**
	 * @param resultEstCred the resultEstCred to set
	 */
	public void setResultEstCred(String resultEstCred) {
		this.resultEstCred = resultEstCred;
	}
	/**
	 * @return the saldoCapital
	 */
	public Double getSaldoCapital() {
		return saldoCapital;
	}
	/**
	 * @param saldoCapital the saldoCapital to set
	 */
	public void setSaldoCapital(Double saldoCapital) {
		this.saldoCapital = saldoCapital;
	}
	/**
	 * @return the saldoIntereses
	 */
	public Double getSaldoIntereses() {
		return saldoIntereses;
	}
	/**
	 * @param saldoIntereses the saldoIntereses to set
	 */
	public void setSaldoIntereses(Double saldoIntereses) {
		this.saldoIntereses = saldoIntereses;
	}
	/**
	 * @return the saldoIntMora
	 */
	public Double getSaldoIntMora() {
		return saldoIntMora;
	}
	/**
	 * @param saldoIntMora the saldoIntMora to set
	 */
	public void setSaldoIntMora(Double saldoIntMora) {
		this.saldoIntMora = saldoIntMora;
	}
	/**
	 * @return the solicitanteCred
	 */
	public String getSolicitanteCred() {
		return solicitanteCred;
	}
	/**
	 * @param solicitanteCred the solicitanteCred to set
	 */
	public void setSolicitanteCred(String solicitanteCred) {
		this.solicitanteCred = solicitanteCred;
	}
	/**
	 * @return the sucursalSocio
	 */
	public String getSucursalSocio() {
		return sucursalSocio;
	}
	/**
	 * @param sucursalSocio the sucursalSocio to set
	 */
	public void setSucursalSocio(String sucursalSocio) {
		this.sucursalSocio = sucursalSocio;
	}
	/**
	 * @return the tasaInt
	 */
	public Double getTasaInt() {
		return tasaInt;
	}
	/**
	 * @param tasaInt the tasaInt to set
	 */
	public void setTasaInt(Double tasaInt) {
		this.tasaInt = tasaInt;
	}
	/**
	 * @return the tasaIntActual
	 */
	public Double getTasaIntActual() {
		return tasaIntActual;
	}
	/**
	 * @param tasaIntActual the tasaIntActual to set
	 */
	public void setTasaIntActual(Double tasaIntActual) {
		this.tasaIntActual = tasaIntActual;
	}
	/**
	 * @return the tipoProd
	 */
	public String getTipoProd() {
		return tipoProd;
	}
	/**
	 * @param tipoProd the tipoProd to set
	 */
	public void setTipoProd(String tipoProd) {
		this.tipoProd = tipoProd;
	}
	/**
	 * @return the valorCap
	 */
	public Double getValorCap() {
		return valorCap;
	}
	/**
	 * @param valorCap the valorCap to set
	 */
	public void setValorCap(Double valorCap) {
		this.valorCap = valorCap;
	}
	/**
	 * @return the valorInt
	 */
	public Double getValorInt() {
		return valorInt;
	}
	/**
	 * @param valorInt the valorInt to set
	 */
	public void setValorInt(Double valorInt) {
		this.valorInt = valorInt;
	}
	/**
	 * @return the valorOrigSrv
	 */
	public Double getValorOrigSrv() {
		return valorOrigSrv;
	}
	/**
	 * @param valorOrigSrv the valorOrigSrv to set
	 */
	public void setValorOrigSrv(Double valorOrigSrv) {
		this.valorOrigSrv = valorOrigSrv;
	}      

}
