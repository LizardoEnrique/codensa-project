package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaDetalleOtrosNegociosDatosServicio;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaDetalleOtrosNegociosDatosServicioImpl extends
SynergiaBusinessObjectImpl implements ConsultaDetalleOtrosNegociosDatosServicio {


	private Long nroCuenta;

	private Long nroServicio;

	private String nombreTitular;

	private String lineaNegocio;

	private String producto;

	private String plan;

	private String tipoIdentTitular;

	private String nroIdentTitular;

	private Date fechaActivProd;

	private Date fechaDesactProd;

	private String motivoFinalizacion;

	private Double valorOrigSrv;

	/**
	 * @return the fechaActivProd
	 */
	public Date getFechaActivProd() {
		return fechaActivProd;
	}

	/**
	 * @param fechaActivProd the fechaActivProd to set
	 */
	public void setFechaActivProd(Date fechaActivProd) {
		this.fechaActivProd = fechaActivProd;
	}

	/**
	 * @return the fechaDesactProd
	 */
	public Date getFechaDesactProd() {
		return fechaDesactProd;
	}

	/**
	 * @param fechaDesactProd the fechaDesactProd to set
	 */
	public void setFechaDesactProd(Date fechaDesactProd) {
		this.fechaDesactProd = fechaDesactProd;
	}

	/**
	 * @return the lineaNegocio
	 */
	public String getLineaNegocio() {
		return lineaNegocio;
	}

	/**
	 * @param lineaNegocio the lineaNegocio to set
	 */
	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}

	/**
	 * @return the motivoFinalizacion
	 */
	public String getMotivoFinalizacion() {
		return motivoFinalizacion;
	}

	/**
	 * @param motivoFinalizacion the motivoFinalizacion to set
	 */
	public void setMotivoFinalizacion(String motivoFinalizacion) {
		this.motivoFinalizacion = motivoFinalizacion;
	}

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	/**
	 * @return the nroIdentTitular
	 */
	public String getNroIdentTitular() {
		return nroIdentTitular;
	}

	/**
	 * @param nroIdentTitular the nroIdentTitular to set
	 */
	public void setNroIdentTitular(String nroIdentTitular) {
		this.nroIdentTitular = nroIdentTitular;
	}

	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio() {
		return nroServicio;
	}

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}

	/**
	 * @return the plan
	 */
	public String getPlan() {
		return plan;
	}

	/**
	 * @param plan the plan to set
	 */
	public void setPlan(String plan) {
		this.plan = plan;
	}

	/**
	 * @return the producto
	 */
	public String getProducto() {
		return producto;
	}

	/**
	 * @param producto the producto to set
	 */
	public void setProducto(String producto) {
		this.producto = producto;
	}

	/**
	 * @return the tipoIdentTitular
	 */
	public String getTipoIdentTitular() {
		return tipoIdentTitular;
	}

	/**
	 * @param tipoIdentTitular the tipoIdentTitular to set
	 */
	public void setTipoIdentTitular(String tipoIdentTitular) {
		this.tipoIdentTitular = tipoIdentTitular;
	}

	/**
	 * @return the valorOrigSrv
	 */
	public Double getValorOrigSrv() {
		return valorOrigSrv;
	}

	/**
	 * @param valorOrigSrv the valorOrigSrv to set
	 */
	public void setValorOrigSrv(Double valorOrigSrv) {
		this.valorOrigSrv = valorOrigSrv;
	}

	/**
	 * @return the nombreTitular
	 */
	public String getNombreTitular() {
		return nombreTitular;
	}

	/**
	 * @param nombreTitular the nombreTitular to set
	 */
	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}
}