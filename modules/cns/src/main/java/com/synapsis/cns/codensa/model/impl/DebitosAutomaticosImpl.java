package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.DebitosAutomaticos;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * 
 * @author adambrosio
 */
public class DebitosAutomaticosImpl extends SynergiaBusinessObjectImpl
	implements DebitosAutomaticos {

	private Long nroCuenta;
	private String nombreEntidadRecaudadora;
	private Long nroCuentaBancaria;
	private String tipoCuentaBancaria;
	private Double valorMaxDebito;
	private Date fechaActivacion;
	private Date fechaDesactivacion;
	private Date fechaInscripcionCuentaBancaria;
	private String nombreEntidadRecaudadoraOrigen;
	
	public Date getFechaActivacion() {
		return fechaActivacion;
	}
	public void setFechaActivacion(Date fechaActivacion) {
		this.fechaActivacion = fechaActivacion;
	}
	public Date getFechaDesactivacion() {
		return fechaDesactivacion;
	}
	public void setFechaDesactivacion(Date fechaDesactivacion) {
		this.fechaDesactivacion = fechaDesactivacion;
	}
	public Date getFechaInscripcionCuentaBancaria() {
		return fechaInscripcionCuentaBancaria;
	}
	public void setFechaInscripcionCuentaBancaria(
			Date fechaInscripcionCuentaBancaria) {
		this.fechaInscripcionCuentaBancaria = fechaInscripcionCuentaBancaria;
	}
	public String getNombreEntidadRecaudadora() {
		return nombreEntidadRecaudadora;
	}
	public void setNombreEntidadRecaudadora(String nombreEntidadRecaudadora) {
		this.nombreEntidadRecaudadora = nombreEntidadRecaudadora;
	}
	public String getNombreEntidadRecaudadoraOrigen() {
		return nombreEntidadRecaudadoraOrigen;
	}
	public void setNombreEntidadRecaudadoraOrigen(
			String nombreEntidadRecaudadoraOrigen) {
		this.nombreEntidadRecaudadoraOrigen = nombreEntidadRecaudadoraOrigen;
	}
	public Long getNroCuentaBancaria() {
		return nroCuentaBancaria;
	}
	public void setNroCuentaBancaria(Long nroCuentaBancaria) {
		this.nroCuentaBancaria = nroCuentaBancaria;
	}
	public String getTipoCuentaBancaria() {
		return tipoCuentaBancaria;
	}
	public void setTipoCuentaBancaria(String tipoCuentaBancaria) {
		this.tipoCuentaBancaria = tipoCuentaBancaria;
	}
	public Double getValorMaxDebito() {
		return valorMaxDebito;
	}
	public void setValorMaxDebito(Double valorMaxDebito) {
		this.valorMaxDebito = valorMaxDebito;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
}
