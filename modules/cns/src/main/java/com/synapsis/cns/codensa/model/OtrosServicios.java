package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


/**
 * TitularCuentaId generated by MyEclipse - Hibernate Tools
 */

public interface OtrosServicios extends SynergiaBusinessObject {

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public Long getNumeroServicio();

	public void setNumeroServicio(Long numeroServicio);

	public String getEstadoServicio();

	public void setEstadoServicio(String estadoServicio);

	public String getTipoServicio();

	public void setTipoServicio(String tipoServicio);

	public String getTitular();

	public void setTitular(String titular);

	public String getDocumento();

	public void setDocumento(String documento);

	public Date getFechaIngreso();

	public void setFechaIngreso(Date fechaIngreso);

	public Date getFechaVencimiento();

	public void setFechaVencimiento(Date fechaVencimiento);

}