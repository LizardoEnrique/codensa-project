package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.OtrosServicios;

public class OtrosServiciosImpl extends SynergiaBusinessObjectImpl implements
		OtrosServicios {
	// Fields    

	private Long nroCuenta;

	private Long numeroServicio;

	private String tipoServicio;

	private String estadoServicio;

	private String titular;

	private String documento;

	private Date fechaIngreso;

	private Date fechaVencimiento;

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public Long getNumeroServicio() {
		return numeroServicio;
	}

	public void setNumeroServicio(Long numeroServicio) {
		this.numeroServicio = numeroServicio;
	}

	public String getEstadoServicio() {
		return estadoServicio;
	}

	public void setEstadoServicio(String estadoServicio) {
		this.estadoServicio = estadoServicio;
	}

	public String getTipoServicio() {
		return tipoServicio;
	}

	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	public String getTitular() {
		return this.titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public String getDocumento() {
		return this.documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

}
