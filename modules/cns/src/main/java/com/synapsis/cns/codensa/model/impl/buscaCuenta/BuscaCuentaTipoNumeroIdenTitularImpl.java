package com.synapsis.cns.codensa.model.impl.buscaCuenta;

import com.synapsis.cns.codensa.model.buscaCuenta.BuscaCuentaTipoNumeroIdenTitular;

public class BuscaCuentaTipoNumeroIdenTitularImpl extends BuscaCuentaImpl implements BuscaCuentaTipoNumeroIdenTitular {
	
	private String numero;
	private String tipo;
	private Long idTipo;
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaTipoNumeroIdenTitular#getNumero()
	 */
	public String getNumero() {
		return numero;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaTipoNumeroIdenTitular#getTipo()
	 */
	public String getTipo() {
		return tipo;
	}
	
	public Long getIdTipo() {
		return this.idTipo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaTipoNumeroIdenTitular#setNumero(java.lang.Long)
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaTipoNumeroIdenTitular#setTipo(java.lang.Long)
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public void setIdTipo(Long idTipo) {
		this.idTipo = idTipo;
	}		
}
