package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


public interface RestriccionesSuspension extends SynergiaBusinessObject {

	public String getDiasSuspensionSolicitados() ;
	public void setDiasSuspensionSolicitados(String diasSuspensionSolicitados) ;
	public Date getFechaCreacion() ;
	public void setFechaCreacion(Date fechaCreacion);
	public Date getFechaVencimiento();
	public void setFechaVencimiento(Date fechaVencimiento) ;
	public String getIdServicioElectrico() ;
	public void setIdServicioElectrico(String idServicioElectrico) ;
	public String getMotivo();
	public void setMotivo(String motivo);
	public String getNombreUsuario() ;
	public void setNombreUsuario(String nombreUsuario) ;
	public String getNombreUsuarioGenerador() ;
	public void setNombreUsuarioGenerador(String nombreUsuarioGenerador);
	public Long getNroCuenta();
	public void setNroCuenta(Long nroCuenta);
	public Long getNroServicio() ;
	public void setNroServicio(Long nroServicio) ;
	public String getObservaciones();
	public void setObservaciones(String observaciones);
	public String getUsuario() ;
	public void setUsuario(String usuario) ;
	public String getUsuarioGenerador();
	public void setUsuarioGenerador(String usuarioGenerador) ;
}
