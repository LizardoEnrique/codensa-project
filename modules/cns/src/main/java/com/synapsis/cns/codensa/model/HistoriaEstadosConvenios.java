package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface HistoriaEstadosConvenios {

	public String getEstado();

	public void setEstado(String estado);

	public Date getFechaCambioEstado();

	public void setFechaCambioEstado(Date fechaCambioEstado);

	public String getUsuarioRealizoOperacion();

	public void setUsuarioRealizoOperacion(String usuarioRealizoOperacion);
	
	public Long getIdServicio();
	
	public void setIdServicio(Long idServicio);


}