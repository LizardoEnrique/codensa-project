package com.synapsis.cns.codensa.model.impl;
import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaModificacionesClienteRed;
import com.synapsis.synergia.core.model.Empresa;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;


/**
 * 
 * @author lcanas
 * 
 */
public class ConsultaModificacionesClienteRedImpl extends SynergiaBusinessObjectImpl implements ConsultaModificacionesClienteRed {

	private Long id;
	private Empresa empresa;
	private String circuito;
	private String propiedad;
	private String tipoConexion;
	private String nivelTension;
    private Long nroTransformador;
    private String estadoTransformador;
    private String tipoRed;
    private Boolean indicadorAlumbradoPublico;
    private Long nroSrvElectrico;
    private String accion;
    private String tipoModificacion;
    private String usuarioRealizoMod;
    private String nroTotalSrvTransformador;

    private Long cantSrvPeriodo;
    private Date periodoFact;

	private String periodoDesde;
	private String periodoHasta;

    
	public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		this.accion = accion;
	}
	public String getCircuito() {
		return circuito;
	}
	public void setCircuito(String circuito) {
		this.circuito = circuito;
	}
	public String getEstadoTransformador() {
		return estadoTransformador;
	}
	public void setEstadoTransformador(String estadoTransformador) {
		this.estadoTransformador = estadoTransformador;
	}
	public Boolean getIndicadorAlumbradoPublico() {
		return indicadorAlumbradoPublico;
	}
	public void setIndicadorAlumbradoPublico(Boolean indicadorAlumbradoPublico) {
		this.indicadorAlumbradoPublico = indicadorAlumbradoPublico;
	}
	public String getNivelTension() {
		return nivelTension;
	}
	public void setNivelTension(String nivelTension) {
		this.nivelTension = nivelTension;
	}
	public String getNroTotalSrvTransformador() {
		return nroTotalSrvTransformador;
	}
	public void setNroTotalSrvTransformador(String nroTotalSrvTransformador) {
		this.nroTotalSrvTransformador = nroTotalSrvTransformador;
	}
	public Long getNroTransformador() {
		return nroTransformador;
	}
	public void setNroTransformador(Long nroTransformador) {
		this.nroTransformador = nroTransformador;
	}
	public String getPropiedad() {
		return propiedad;
	}
	public void setPropiedad(String propiedad) {
		this.propiedad = propiedad;
	}
	public String getTipoConexion() {
		return tipoConexion;
	}
	public void setTipoConexion(String tipoConexion) {
		this.tipoConexion = tipoConexion;
	}
	public String getTipoModificacion() {
		return tipoModificacion;
	}
	public void setTipoModificacion(String tipoModificacion) {
		this.tipoModificacion = tipoModificacion;
	}
	public String getTipoRed() {
		return tipoRed;
	}
	public void setTipoRed(String tipoRed) {
		this.tipoRed = tipoRed;
	}
	public String getUsuarioRealizoMod() {
		return usuarioRealizoMod;
	}
	public void setUsuarioRealizoMod(String usuarioRealizoMod) {
		this.usuarioRealizoMod = usuarioRealizoMod;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getNroSrvElectrico() {
		return nroSrvElectrico;
	}
	public void setNroSrvElectrico(Long nroSrvElectrico) {
		this.nroSrvElectrico = nroSrvElectrico;
	}
	public String getPeriodoDesde() {
		return periodoDesde;
	}
	public void setPeriodoDesde(String periodoDesde) {
		this.periodoDesde = periodoDesde;
	}
	public String getPeriodoHasta() {
		return periodoHasta;
	}
	public void setPeriodoHasta(String periodoHasta) {
		this.periodoHasta = periodoHasta;
	}
	public Long getCantSrvPeriodo() {
		return cantSrvPeriodo;
	}
	public void setCantSrvPeriodo(Long cantSrvPeriodo) {
		this.cantSrvPeriodo = cantSrvPeriodo;
	}
	public Date getPeriodoFact() {
		return periodoFact;
	}
	public void setPeriodoFact(Date periodoFact) {
		this.periodoFact = periodoFact;
	}

}
