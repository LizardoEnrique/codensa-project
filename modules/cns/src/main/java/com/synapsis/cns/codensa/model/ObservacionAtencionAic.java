package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


/**
 * @author m3.MarioRoss - 14/11/2006
 */
public interface ObservacionAtencionAic extends SynergiaBusinessObject {
	public Long getIdAtencion();
	public void setIdAtencion(Long idAtencion);
	
	public String getObservacion();
	public void setObservacion(String observacion);
	/**
	 * @return the fechaObservacion
	 */
	public Date getFechaObservacion();
	/**
	 * @param fechaObservacion the fechaObservacion to set
	 */
	public void setFechaObservacion(Date fechaObservacion);
}
