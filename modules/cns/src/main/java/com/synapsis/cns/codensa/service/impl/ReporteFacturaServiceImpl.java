package com.synapsis.cns.codensa.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.integration.Module;
import com.suivant.arquitectura.core.queryFilter.CompositeQueryFilter;
import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;
import com.synapsis.cns.codensa.model.CampoFactura;
import com.synapsis.cns.codensa.model.CargoFacturaDuplicadoLegal;
import com.synapsis.cns.codensa.model.ConsumoSUltimos6Meses;
import com.synapsis.cns.codensa.model.LineaFactura;
import com.synapsis.cns.codensa.model.ReporteFactura;
import com.synapsis.cns.codensa.model.ReporteFacturaDuplicadoLegal;
import com.synapsis.cns.codensa.model.constants.FacturaConstants;
import com.synapsis.cns.codensa.model.impl.CargoFacturaDuplicadoLegalImpl;
import com.synapsis.cns.codensa.model.impl.ConsumosUltimos6MesesImpl;
import com.synapsis.cns.codensa.model.impl.LineaFacturaImpl;
import com.synapsis.cns.codensa.model.impl.ReporteFacturaDuplicadoLegalImpl;
import com.synapsis.cns.codensa.service.ReporteFacturaService;
import com.synapsis.cns.codensa.service.ReporteHelperService;
import com.synapsis.synergia.core.service.impl.SynergiaServiceImpl;

/**
 * @author jhack
 */
public class ReporteFacturaServiceImpl extends SynergiaServiceImpl implements ReporteFacturaService {
	private static final String NL = "\n";
	private ReporteHelperService reporteHelperService;
	private static final int NIVEL_CONSUMO = 0;
	private static final int NIVEL_SRV_ELE = 1;
	private static final int NIVEL_DESC = 4;
	private static final int NIVEL_SUBSIDIOS_CONT = 8;

	public ReporteFacturaServiceImpl() {
		super();
	}

	public ReporteFacturaServiceImpl(Module module) {
		super(module);

	}

	/**
	 * Surge la necesidad de hacer este m�todo para poder aplicar el validator
	 * NumeroCuentaQueryFilterValidation. El Constructor de este validator recibe como par�metro un Filtro, el
	 * cual obtiene del par�metro del m�todo de b�squeda del Service. Como hasta ahora nuestro Service recib�a
	 * por par�metro (Date, Long) fallaba al intentar instanciar el Validator con un Date como par�metro.<br>
	 * La l�gica de la b�squeda sigue siendo la que estaba en los respectivos m�todos de los Servicios, s�lo
	 * modifico la forma de acceder al Service.
	 * 
	 * @author egrande
	 * 
	 */
	public ReporteFacturaDuplicadoLegal getReporteFactura(CompositeQueryFilter compositeQueryFilter)
			throws ObjectNotFoundException {
		Long nroCuenta = null;
		Date fechaFactura = new Date();
		for (Iterator it = compositeQueryFilter.getFiltersList().iterator(); it.hasNext();) {
			EqualQueryFilter queryFilter = (EqualQueryFilter) it.next();
			if ("nroCuenta".equals(queryFilter.getAttributeName())) {
				nroCuenta = (Long) queryFilter.getAttributeValue();
			}
			if ("fechaFactura".equals(queryFilter.getAttributeName())) {
				fechaFactura = (Date) queryFilter.getAttributeValue();
			}
		}

		return this.getReporteFactura(fechaFactura, nroCuenta);
	}

	/*
	 * Devuelve un reporte populado con los datos de la factura.
	 */
	private ReporteFacturaDuplicadoLegal getReporteFactura(Date fechaFactura, Long nroCuenta)
			throws ObjectNotFoundException {

		ReporteFacturaDuplicadoLegal reporte = new ReporteFacturaDuplicadoLegalImpl();

		LineaFactura linea = reporteHelperService.getDataLine(fechaFactura, nroCuenta, reporte);

		this.fillReporteFacturaDuplicadoLegal(reporte, linea);

		return reporte;
	}

	private void fillReporteFacturaDuplicadoLegal(ReporteFacturaDuplicadoLegal reporte, LineaFactura linea) {
		String fechaProceso = getReporteHelperService().getDato(FacturaConstants.FIELD_FECHA_PROCESO, linea);
		reporte.setFechaProceso(fechaProceso);
		reporte.setFechaExpedicion(this.formatearFecha(fechaProceso));

		String nit = getReporteHelperService().getDato(FacturaConstants.FIELD_NIT, linea);
		reporte.setNit(nit);

		String direccionOficinaPrincipal = (String) getReporteHelperService().getDato(
			FacturaConstants.FIELD_DIR_OFIC_PPAL, linea);
		reporte.setDireccionOficinaPrincipal(direccionOficinaPrincipal);

		String nroCuentaFromBD = reporteHelperService.getDato(FacturaConstants.FIELD_NUMERO_CUENTA, linea);
		reporte.setNroCuenta(reporteHelperService.getLong(nroCuentaFromBD));
		reporte.setDigitoVerificadorCta(reporteHelperService.getLong(reporteHelperService.getDato(
			FacturaConstants.FIELD_DV_NUMERO_CUENTA, linea)));

		String numeroServicio = getReporteHelperService().getDato(FacturaConstants.FIELD_NUMERO_SERVICIO, linea);
		reporte.setNumeroServicio(reporteHelperService.getLong(numeroServicio));

		String periodoFacturadoInicio = getReporteHelperService().getDato(
			FacturaConstants.FIELD_PERIODO_FACTURADO_INICIO, linea);
		reporte.setPeriodoFacturadoInicio(periodoFacturadoInicio);

		String periodoFacturadoFin = getReporteHelperService().getDato(FacturaConstants.FIELD_PERIODO_FACTURADO_FIN,
			linea);
		reporte.setPeriodoFacturadoFin(periodoFacturadoFin);

		// CUENTA O CLIENTE
		reporte.setNombreCliente(getReporteHelperService().getDato(FacturaConstants.FIELD_NOMBRE_CLIENTE, linea));

		reporte.setDireccionCliente(getReporteHelperService().getDato(FacturaConstants.FIELD_DIRECC_CLIENTE, linea));
		reporte.setDireccionSuministro(getReporteHelperService().getDato(FacturaConstants.FIELD_DIRECC_SUMINISTRO,
			linea));
		reporte.setBarrioCliente(getReporteHelperService().getDato(FacturaConstants.FIELD_BARRIO_CLIENTE, linea));
		reporte.setMunicipioCliente(getReporteHelperService().getDato(FacturaConstants.FIELD_MUNICIPIO_CLIENTE, linea));

		// REPARTO ESPECIAL
		String indicadorRepartoEspecial = getReporteHelperService().getDato(FacturaConstants.FIELD_INDIC_REPARTO_ESPEC,
			linea);
		reporte.setIndicadorRepartoEspecial(indicadorRepartoEspecial);
		if (!"".equals(reporte.getIndicadorRepartoEspecial())) {
			reporte.setDireccionReparto(getReporteHelperService().getDato(FacturaConstants.FIELD_DIRECCION_REPARTO,
				linea));
			reporte.setMunicipioReparto(getReporteHelperService().getDato(FacturaConstants.FIELD_MUNICIPIO_REPARTO,
				linea));
			reporte.setBarrioReparto(getReporteHelperService().getDato(FacturaConstants.FIELD_BARRIO_REPARTO, linea));
		}

		// INFORMACI�N T�CNICA
		String tipoServicioInfoTecnica = getReporteHelperService().getDato(FacturaConstants.FIELD_TIPO_SERV_INFO_TEC,
			linea);
		reporte.setTipoServicioInfoTecnica(tipoServicioInfoTecnica);

		// Ruta Lectura
		String sucursalLectura = getReporteHelperService().getDato(FacturaConstants.FIELD_SUCURSAL_LECTURA, linea);
		String zonaLectura = getReporteHelperService().getDato(FacturaConstants.FIELD_ZONA_LECTURA, linea);
		String cicloLectura = getReporteHelperService().getDato(FacturaConstants.FIELD_CICLO_LECTURA, linea);
		String grupoLectura = getReporteHelperService().getDato(FacturaConstants.FIELD_GRUPO_LECTURA, linea);
		String correlativoLectura = getReporteHelperService()
			.getDato(FacturaConstants.FIELD_CORRELATIVO_LECTURA, linea);
		String rutaLectura = sucursalLectura.concat(zonaLectura).concat(cicloLectura).concat(grupoLectura).concat(
			correlativoLectura);
		reporte.setRutaLectura(rutaLectura);

		// Ruta Reparto
		String sucursalReparto = getReporteHelperService().getDato(FacturaConstants.FIELD_SUCURSAL_REPARTO, linea);
		String zonaReparto = getReporteHelperService().getDato(FacturaConstants.FIELD_ZONA_REPARTO, linea);
		String cicloReparto = getReporteHelperService().getDato(FacturaConstants.FIELD_CICLO_REPARTO, linea);
		String grupoReparto = getReporteHelperService().getDato(FacturaConstants.FIELD_GRUPO_REPARTO, linea);
		String correlativoReparto = getReporteHelperService()
			.getDato(FacturaConstants.FIELD_CORRELATIVO_REPARTO, linea);

		String rutaReparto = sucursalReparto.concat(zonaReparto).concat(cicloReparto).concat(grupoReparto).concat(
			correlativoReparto);
		reporte.setRutaReparto(rutaReparto);

		reporte.setManzanaLectura(getReporteHelperService().getDato(FacturaConstants.FIELD_MANZANA_LECTURA, linea));
		reporte.setManzanaReparto(getReporteHelperService().getDato(FacturaConstants.FIELD_MANZANA_REPARTO, linea));
		reporte.setProximaLectura(getReporteHelperService().getDato(FacturaConstants.FIELD_PROXIMA_LECTURA, linea));

		String numeroMedidor1 = getReporteHelperService().getDato(FacturaConstants.FIELD_NRO_MEDIDOR_1, linea);
		reporte.setNumeroMedidor1(numeroMedidor1);

		String numeroMedidor2 = getReporteHelperService().getDato(FacturaConstants.FIELD_NRO_MEDIDOR_2, linea);
		reporte.setNumeroMedidor2(numeroMedidor2);

		String grupoC = null;
		StringBuffer buff = new StringBuffer();	
		
		// QUE LINDA MIERDA. GRACIAS POR NO MODIFICAR EL IMPRESOR
		try {
			buff.append(getReporteHelperService().getDato(
				FacturaConstants.FIELD_GRUPO_C, linea).trim());
			new Double(buff.toString().trim());
			grupoC = buff.toString();
			buff.setLength(0);
		}
		catch (NumberFormatException e) {
			try {
				buff.setLength(0);
				buff.append(getReporteHelperService().getDato(
					FacturaConstants.FIELD_GRUPO_C2, linea).trim());
				new Double(buff.toString().trim());
				grupoC = buff.toString();
				buff.setLength(0);				 
			}
			catch (NumberFormatException e1) {
				try {
					buff.setLength(0);
					buff.append(getReporteHelperService().getDato(
						FacturaConstants.FIELD_GRUPO_C3, linea).trim());
					new Double(buff.toString().trim());
					grupoC = buff.toString();
					buff.setLength(0);				 					
				}
				catch (NumberFormatException e2) {
					grupoC = "-";
					buff.setLength(0);
				}				
			}			 
		}
		reporte.setGrupoC(grupoC);

		String numeroCircuito = getReporteHelperService().getDato(FacturaConstants.FIELD_NRO_CIRCUITO, linea);
		reporte.setNumeroCircuito(numeroCircuito);

		String estratoSocioeconomico = getReporteHelperService().getDato(FacturaConstants.FIELD_ESTRATO_SOCIO, linea);
		reporte.setEstratoSocioeconomico(estratoSocioeconomico);

		String cargaContratada = getReporteHelperService().getDato(FacturaConstants.FIELD_CARGA_CONTRATADA, linea);
		reporte.setCargaContratada(reporteHelperService.getDouble(cargaContratada));

		String nivelTension = getReporteHelperService().getDato(FacturaConstants.FIELD_NIVEL_TENSION, linea);
		reporte.setNivelTension(reporteHelperService.getLong(nivelTension));

		// EVOLUCI�N DEL CONSUMO (�ltimos 6 meses)
		String periodoFacturadoActual = getReporteHelperService().getDato(FacturaConstants.FIELD_FECHA_PROCESO, linea); // idem
		// fechaProceso
		// ??
		reporte.setPeriodoFacturadoActual(periodoFacturadoActual);

		String consumoPeriodoActual = getReporteHelperService().getDato(FacturaConstants.FIELD_CONSUMO_PERIODO_ACTUAL,
			linea);
		reporte.setConsumoPeriodoActual(reporteHelperService.getDouble(consumoPeriodoActual));

		// OTROS DATOS
		String fechaVencimientoPagoOportuno = getReporteHelperService().getDato(
			FacturaConstants.FIELD_FEC_VENC_PAGO_OPORT, linea);
		reporte.setFechaVencimientoPagoOportuno(fechaVencimientoPagoOportuno);

		String fechaVencimientoAvisoSuspension = getReporteHelperService().getDato(
			FacturaConstants.FIELD_FEC_VENC_AVISO_SUSP, linea);
		reporte.setFechaVencimientoAvisoSuspension(fechaVencimientoAvisoSuspension);

		String totalAPagar = getReporteHelperService().getDato(FacturaConstants.FIELD_TOTAL_A_PAGAR, linea);
		reporte.setTotalAPagar(totalAPagar);

		this.getInfoCons(reporte, linea);
		this.fillTotalesFactura(reporte, linea);

		reporte.setInformacionInteres(this.getInformacionInteres(linea));
		// EVOLUCI�N DEL CONSUMO (�ltimos 6 meses)
		reporte.setConsumosUltimos6Meses(this.getConsUlt6Meses(linea));

		// TIPOS SERVICIO CLIENTE
		// reporte.setTiposServicioCliente(this.getTiposServicioCliente(linea));

		// CARGOS
		this.fillCargos(reporte, linea);

		// CODIGO BARRA
		// dummy, no es valido para pago asi que no se muestra nada
		reporte.setCodigoDeBarras("}�JZgiaaaaaaaaaaaaaaaaaaaaaaaaaaa~");
		reporte.setCodigoDeBarrasNro(this.getNroCodigoBarras(linea));
		String nroFactura = this.getReporteHelperService().getDato(FacturaConstants.FIELD_NUMERO_FACTURA, linea);
		reporte.setNroFactura(reporteHelperService.getLong(nroFactura));

		String digitoVerificadorDoc = this.getReporteHelperService().getDato(FacturaConstants.FIELD_DIGITO_FACTURA,
			linea);
		reporte.setDigitoVerificadorDoc(reporteHelperService.getLong(digitoVerificadorDoc));

		// System.out.println(this.getHelper().getInsertsStrB());
	}

	private String getNroCodigoBarras(LineaFactura linea) {
		StringBuffer nro = new StringBuffer();
		nro.append(getReporteHelperService().getDato(FacturaConstants.FIELD_VAR_IDENTIF_P415P, linea));
		nro.append(getReporteHelperService().getDato(FacturaConstants.FIELD_VAR_IDENTIF_EAN_SV, linea));
		nro.append(getReporteHelperService().getDato(FacturaConstants.FIELD_VAR_IDENTIF_P8020P, linea));
		nro.append(getReporteHelperService().getDato(FacturaConstants.FIELD_VAR_TIPO_DOCUMENTO2, linea));
		nro.append(getReporteHelperService().getDato(FacturaConstants.FIELD_VAR_NIE_CODBAR_SV, linea));
		nro.append(getReporteHelperService().getDato(FacturaConstants.FIELD_VAR_NROFAC_CODBAR_SV, linea));
		nro.append(getReporteHelperService().getDato(FacturaConstants.FIELD_VAR_DIGFAC_CODBAR_SV, linea));
		nro.append(getReporteHelperService().getDato(FacturaConstants.FIELD_VAR_IDENTIF_P3900P, linea));
		nro.append(getReporteHelperService().getDato(FacturaConstants.FIELD_VAR_VALOR_CODBAR_SV, linea));

		return nro.toString();
	}

	private void getInfoCons(ReporteFacturaDuplicadoLegal reporte, LineaFactura linea) {
		// INFORMACI�N DEL CONSUMO
		String tipoLecturaPeriodoActual = getReporteHelperService().getDato(
			FacturaConstants.FIELD_TIPO_LECT_PER_ACTUAL, linea);
		String tarifaParaFacturacionActual = getReporteHelperService().getDato(
			FacturaConstants.FIELD_TARIF_FACT_ACTUAL, linea);

		String totalConsumoKwh = getReporteHelperService().getDato(FacturaConstants.FIELD_TOTAL_CONS_KWH, linea);
		String valorPromedioKwh = getReporteHelperService().getDato(FacturaConstants.FIELD_VALOR_PROM_KWH, linea);
		String anomaliaLecturaPeriodoActual = getReporteHelperService().getDato(FacturaConstants.FIELD_ANOMALIA, linea);
		String mesTarifaParaLiquidacion = getReporteHelperService().getDato(FacturaConstants.FIELD_MES_TARIFA_LIQ,
			linea);
		String consumoPromedioUltimosMeses = getReporteHelperService().getDato(
			FacturaConstants.FIELD_CONS_PROM_ULT_MESES, linea);

		reporte.setTipoLecturaPeriodoActual(tipoLecturaPeriodoActual);
		reporte.setTarifaParaFacturacionActual(tarifaParaFacturacionActual);
		reporte.setTotalConsumoKwh(totalConsumoKwh);
		reporte.setValorPromedioKwh(valorPromedioKwh);
		reporte.setAnomaliaLecturaPeriodoActual(anomaliaLecturaPeriodoActual);
		reporte.setMesTarifaParaLiquidacion(mesTarifaParaLiquidacion);
		reporte.setConsumoPromedioUltimosMeses(consumoPromedioUltimosMeses);

		// CALIDAD DEL SERVICIO
		String maximoPermitidoInterrupciones = null; 
		String maximoPermitidoHoras	 = null;
		String numeroInterrupcionesAcumuladas = null;
		String numeroHorasInterrupcionesAcumuladas = null;
		StringBuffer buff = new StringBuffer();	
			
		// QUE LINDA MIERDA. GRACIAS POR NO MODIFICAR EL IMPRESOR
		try {
			buff.append(getReporteHelperService().getDato(
				FacturaConstants.FIELD_MAX_PERM_INTERRUP, linea).trim());
			new Double(buff.toString().trim());
			maximoPermitidoInterrupciones = buff.toString();
			buff.setLength(0);
		}
		catch (NumberFormatException e) {
			try {
				buff.setLength(0);
				buff.append(getReporteHelperService().getDato(
					FacturaConstants.FIELD_MAX_PERM_INTERRUP2, linea).trim());
				new Double(buff.toString().trim());
				maximoPermitidoInterrupciones = buff.toString();
				buff.setLength(0);				 
			}
			catch (NumberFormatException e1) {
				try {
					buff.setLength(0);
					buff.append(getReporteHelperService().getDato(
						FacturaConstants.FIELD_MAX_PERM_INTERRUP3, linea).trim());
					new Double(buff.toString().trim());
					maximoPermitidoInterrupciones = buff.toString();
					buff.setLength(0);				 					
				}
				catch (NumberFormatException e2) {
					maximoPermitidoInterrupciones = "0.0";
					buff.setLength(0);
				}
			}			 
		}
		
		try {
			buff.append(getReporteHelperService().getDato(
				FacturaConstants.FIELD_MAX_PERM_HORAS, linea).trim());
			new Double(buff.toString().trim());
			maximoPermitidoHoras = buff.toString();
			buff.setLength(0);
		}
		catch (NumberFormatException e) {
			try {
				buff.setLength(0);
				buff.append(getReporteHelperService().getDato(
					FacturaConstants.FIELD_MAX_PERM_HORAS2, linea).trim());
				new Double(buff.toString().trim());
				maximoPermitidoHoras = buff.toString();
				buff.setLength(0);				 
			}
			catch (NumberFormatException e1) {
				try {
					buff.setLength(0);
					buff.append(getReporteHelperService().getDato(
						FacturaConstants.FIELD_MAX_PERM_HORAS3, linea).trim());
					new Double(buff.toString().trim());
					maximoPermitidoHoras = buff.toString();
					buff.setLength(0);				 					
				}
				catch (NumberFormatException e2) {
					maximoPermitidoHoras = "0.0";
					buff.setLength(0);
				}				
			}			 
		}
		
		try {
			buff.append(getReporteHelperService().getDato(
				FacturaConstants.FIELD_NRO_INTERRUP_ACUM, linea).trim());
			new Double(buff.toString().trim());
			numeroInterrupcionesAcumuladas = buff.toString();
			buff.setLength(0);
		}
		catch (NumberFormatException e) {
			try {
				buff.setLength(0);
				buff.append(getReporteHelperService().getDato(
					FacturaConstants.FIELD_NRO_INTERRUP_ACUM2, linea).trim());
				new Double(buff.toString().trim());
				numeroInterrupcionesAcumuladas = buff.toString();
				buff.setLength(0);				 
			}
			catch (NumberFormatException e1) {
				try {
					buff.setLength(0);
					buff.append(getReporteHelperService().getDato(
						FacturaConstants.FIELD_NRO_INTERRUP_ACUM3, linea).trim());
					new Double(buff.toString().trim());
					numeroInterrupcionesAcumuladas = buff.toString();
					buff.setLength(0);				 					
				}
				catch (NumberFormatException e2) {
					numeroInterrupcionesAcumuladas = "0.0";
					buff.setLength(0);
				}
			}			 
		}
		
		try {
			buff.append(getReporteHelperService().getDato(
				FacturaConstants.FIELD_NRO_HORAS_INTERRUP_ACUM, linea).trim());
			new Double(buff.toString().trim());
			numeroHorasInterrupcionesAcumuladas = buff.toString();
			buff.setLength(0);
		}
		catch (NumberFormatException e) {
			try {
				buff.setLength(0);
				buff.append(getReporteHelperService().getDato(
					FacturaConstants.FIELD_NRO_HORAS_INTERRUP_ACUM2, linea).trim());
				new Double(buff.toString().trim());
				numeroHorasInterrupcionesAcumuladas = buff.toString();
				buff.setLength(0);				 
			}
			catch (NumberFormatException e1) {
				try {
					buff.setLength(0);
					buff.append(getReporteHelperService().getDato(
						FacturaConstants.FIELD_NRO_HORAS_INTERRUP_ACUM3, linea).trim());
					new Double(buff.toString().trim());
					numeroHorasInterrupcionesAcumuladas = buff.toString();
					buff.setLength(0);				 					
				}
				catch (NumberFormatException e2) {
					numeroHorasInterrupcionesAcumuladas = "0.0";
					buff.setLength(0);
				}
			}			 
		}
				
		String periodoInterrupciones = getReporteHelperService()
		.getDato(FacturaConstants.FIELD_PERIODO_INTERRUP, linea);
		String trimestreInterrupciones = getReporteHelperService().getDato(FacturaConstants.FIELD_TRIMESTRE_INTERRUP,
		linea);

		reporte.setMaximoPermitidoInterrupciones(maximoPermitidoInterrupciones);
		reporte.setMaximoPermitidoHoras(maximoPermitidoHoras);
		reporte.setNumeroInterrupcionesAcumuladas(numeroInterrupcionesAcumuladas);
		reporte.setNumeroHorasInterrupcionesAcumuladas(numeroHorasInterrupcionesAcumuladas);
		reporte.setPeriodoInterrupciones(periodoInterrupciones.trim());
		reporte.setTrimestreInterrupciones(trimestreInterrupciones.trim());
				
		try {			
			buff = new StringBuffer(getReporteHelperService().getDato(FacturaConstants.FIELD_GTDCVPRRCUCF,
				linea).trim());			
			String[] result = new String[8];
			String[] tmp = buff.toString().split(" ");
	
			int j = 0;
			for (int i = 0; i < tmp.length; i++) {
				if (!tmp[i].equals("") && tmp[i].indexOf('.') > 0) {
					result[j] = tmp[i];
					BigDecimal bd = new BigDecimal(result[j]);
					bd = bd.setScale(2, BigDecimal.ROUND_DOWN);
					result[j] = bd.toString();
					j++;
				}
			}
			reporte.setGeneracion(result[1]);
			reporte.setTransmision(result[2]);
			reporte.setDistribucion(result[3]);
			reporte.setComercializacion(result[4]);
			reporte.setPerdidas(result[6]);
			reporte.setRestricciones(result[5]);
			reporte.setCostoUnitario(result[0]);
			reporte.setCostoUnitarioFijo(result[7]);		
		}
		catch (Exception e) {
			reporte.setGeneracion("-");
			reporte.setTransmision("-");
			reporte.setDistribucion("-");
			reporte.setComercializacion("-");
			reporte.setPerdidas("-");
			reporte.setRestricciones("-");
			reporte.setCostoUnitario("-");
			reporte.setCostoUnitarioFijo("-");		
		}
	}

	private void fillTotalesFactura(ReporteFacturaDuplicadoLegal reporte, LineaFactura linea) {
		String subtTotCargos = getReporteHelperService().getDato(FacturaConstants.FIELD_SUBT_TOTAL_CARGOS, linea);
		String subtCargosCons = getReporteHelperService().getDato(FacturaConstants.FIELD_SUBT_CARGOS_CONS, linea);
		String subtCargosDesc = getReporteHelperService().getDato(FacturaConstants.FIELD_SUBT_CARGOS_DESC, linea);
		String subtCargosServPort = getReporteHelperService().getDato(FacturaConstants.FIELD_SUBT_CARGOS_SERV_PORT,
			linea);
		String subtOtrosCargos = getReporteHelperService().getDato(FacturaConstants.FIELD_SUBT_OTROS_CARGOS, linea);
		String subtTotalFactura = getReporteHelperService().getDato(FacturaConstants.FIELD_SUBT_TOTAL_FACTURA, linea);

		reporte.setSubtTotCargosEne(subtTotCargos);
		reporte.setSubtCargosCons(subtCargosCons);
		reporte.setSubtCargosDesc(subtCargosDesc);
		reporte.setSubtCargosSrvPort(subtCargosServPort);
		reporte.setSubtOtrosCargos(subtOtrosCargos);
		reporte.setTotalFactura(subtTotalFactura);
	}

	/**
	 * EVOLUCI�N DEL CONSUMO (�ltimos 6 meses)
	 * 
	 * @return
	 */
	private Collection getConsUlt6Meses(LineaFactura linea) {
		// EVOLUCI�N DEL CONSUMO (�ltimos 6 meses)
		Collection consumosUltimos6Meses = new ArrayList();

		// Consumos ultimos 6 meses
		ConsumoSUltimos6Meses consumo;
		String mes;
		String cons_KWH;

		// 6
		consumo = new ConsumosUltimos6MesesImpl();
		mes = getReporteHelperService().getDato(FacturaConstants.FIELD_CONS_ULT_6MES_MES, linea);
		cons_KWH = getReporteHelperService().getDato(FacturaConstants.FIELD_CONS_ULT_6MES_CONS_KWH, linea);
		consumo.setMes(this.getMesShort(mes));
		consumo.setCons_KWH(reporteHelperService.getDouble(cons_KWH));
		consumosUltimos6Meses.add(consumo);

		// 5
		consumo = new ConsumosUltimos6MesesImpl();
		mes = getReporteHelperService().getDato(FacturaConstants.FIELD_CONS_ULT_5MES_MES, linea);
		cons_KWH = getReporteHelperService().getDato(FacturaConstants.FIELD_CONS_ULT_5MES_CONS_KWH, linea);
		consumo.setMes(this.getMesShort(mes));
		consumo.setCons_KWH(reporteHelperService.getDouble(cons_KWH));
		consumosUltimos6Meses.add(consumo);

		// 4
		consumo = new ConsumosUltimos6MesesImpl();
		mes = getReporteHelperService().getDato(FacturaConstants.FIELD_CONS_ULT_4MES_MES, linea);
		cons_KWH = getReporteHelperService().getDato(FacturaConstants.FIELD_CONS_ULT_4MES_CONS_KWH, linea);
		consumo.setMes(this.getMesShort(mes));
		consumo.setCons_KWH(reporteHelperService.getDouble(cons_KWH));
		consumosUltimos6Meses.add(consumo);

		// 3
		consumo = new ConsumosUltimos6MesesImpl();
		mes = getReporteHelperService().getDato(FacturaConstants.FIELD_CONS_ULT_3MES_MES, linea);
		cons_KWH = getReporteHelperService().getDato(FacturaConstants.FIELD_CONS_ULT_3MES_CONS_KWH, linea);
		consumo.setMes(this.getMesShort(mes));
		consumo.setCons_KWH(reporteHelperService.getDouble(cons_KWH));
		consumosUltimos6Meses.add(consumo);

		// 2
		consumo = new ConsumosUltimos6MesesImpl();
		mes = getReporteHelperService().getDato(FacturaConstants.FIELD_CONS_ULT_2MES_MES, linea);
		cons_KWH = getReporteHelperService().getDato(FacturaConstants.FIELD_CONS_ULT_2MES_CONS_KWH, linea);
		consumo.setMes(this.getMesShort(mes));
		consumo.setCons_KWH(reporteHelperService.getDouble(cons_KWH));
		consumosUltimos6Meses.add(consumo);

		// 1
		consumo = new ConsumosUltimos6MesesImpl();
		mes = getReporteHelperService().getDato(FacturaConstants.FIELD_CONS_ULT_1MES_MES, linea);
		cons_KWH = getReporteHelperService().getDato(FacturaConstants.FIELD_CONS_ULT_1MES_CONS_KWH, linea);
		consumo.setMes(this.getMesShort(mes));
		consumo.setCons_KWH(reporteHelperService.getDouble(cons_KWH));
		consumosUltimos6Meses.add(consumo);

		return consumosUltimos6Meses;
	}

	private int getCargosVariablesSize(LineaFactura linea) {
		int size = 0;
		for (int i = 0; i < FacturaConstants.FIELDS_VARIABLES.length; i++) {
			CampoFactura campo = (CampoFactura) linea.getFormato().get(FacturaConstants.FIELDS_VARIABLES[i]);
			size += campo.getLongitud().intValue();
		}
		return size;
	}

	/**
	 * Recibe un String del tipo 'MMM/YYYY' y devuelve 'MMM/YY'. Ej: 'ENE/2009' -> 'ENE/09'
	 * 
	 * @param mes
	 * @return
	 */
	private String getMesShort(String mes) {
		int posEsp = mes.indexOf(" ");
		int posBar = mes.indexOf("/");
		if (posEsp < 0 || posBar < 0) {
			return "";
		}
		return mes.substring(posEsp + 1, posBar + 1).concat(mes.substring(posBar + 3));
	}

	private void fillCargos(ReporteFacturaDuplicadoLegal reporteFactura, LineaFactura linea) {
		CampoFactura campoCantCargos = (CampoFactura) linea.getFormato().get(FacturaConstants.FIELD_CANT_CARGOS);
		String cantidadCargos = getReporteHelperService().getDato(FacturaConstants.FIELD_CANT_CARGOS, linea);

		int inicioCamposVariables = campoCantCargos.getPosFin().intValue();

		if (linea.getDatos() != null && linea.getDatos().length() > inicioCamposVariables) {
			int offset = 0;
			final int TAM_REGISTROS_VARIABLES = getCargosVariablesSize(linea);

			for (int i = 0; i < Integer.parseInt(cantidadCargos); i++) {
				CargoFacturaDuplicadoLegal cargoFactura = new CargoFacturaDuplicadoLegalImpl();

				cargoFactura.setCodigo(getReporteHelperService().getDatoVariable(
					FacturaConstants.FIELD_VAR_CARGO_CODIGO, linea, offset));
				cargoFactura.setConcepto(getReporteHelperService().getDatoVariable(
					FacturaConstants.FIELD_VAR_CARGO_CONCEPTO, linea, offset));
				cargoFactura.setValor(getReporteHelperService().getDatoVariable(FacturaConstants.FIELD_VAR_CARGO_VALOR,
					linea, offset));

				String indicadorCargo = getReporteHelperService().getDatoVariable(
					FacturaConstants.FIELD_VAR_INDICADOR_CARGO, linea, offset);
				int nivelImpresion = Integer.parseInt(indicadorCargo);

				if (nivelImpresion == NIVEL_CONSUMO || nivelImpresion == NIVEL_SUBSIDIOS_CONT) {
					reporteFactura.getCargosServicioElectrico().add(cargoFactura);
				}
				else if (nivelImpresion == NIVEL_DESC) {
					reporteFactura.getCargosDescuento().add(cargoFactura);
				}
				else if (nivelImpresion == NIVEL_SRV_ELE) {
					reporteFactura.getOtrosCargos().add(cargoFactura);
				}
				else {
					reporteFactura.getCargosOtrosServPort().add(cargoFactura);
				}

				fillInfoConsumo(reporteFactura, linea, offset);

				offset += TAM_REGISTROS_VARIABLES;
			}

			this.fillCargosDummies(reporteFactura);
		}
	}

	/**
	 * 
	 * HARDCODEADA HORRIBLE PERO ME ESPERAN PARA CERRAR EL PROYECTO :D<BR>
	 * EL JASPER NO ME MUESTRA LOS SUBREPORTES SI LAS COLECCIONES SON VACIAS
	 * 
	 * @param reporteFactura
	 */
	private void fillCargosDummies(ReporteFacturaDuplicadoLegal reporteFactura) {
		String emptyStr = "";
		CargoFacturaDuplicadoLegal cargoDummy = new CargoFacturaDuplicadoLegalImpl();
		cargoDummy.setCodigo(emptyStr);
		cargoDummy.setConcepto(emptyStr);
		cargoDummy.setValor(emptyStr);

		if (reporteFactura.getCargosServicioElectrico().isEmpty()) {
			reporteFactura.getCargosServicioElectrico().add(cargoDummy);
		}
		if (reporteFactura.getCargosDescuento().isEmpty()) {
			reporteFactura.getCargosDescuento().add(cargoDummy);
		}
		if (reporteFactura.getOtrosCargos().isEmpty()) {
			reporteFactura.getOtrosCargos().add(cargoDummy);
		}
		if (reporteFactura.getCargosOtrosServPort().isEmpty()) {
			reporteFactura.getCargosOtrosServPort().add(cargoDummy);
		}
	}

	private void fillInfoConsumo(ReporteFacturaDuplicadoLegal reporteFactura, LineaFactura linea, int offset) {
		String indicadorCargo = getReporteHelperService().getDatoVariable(FacturaConstants.FIELD_VAR_INDICADOR_CARGO,
			linea, offset);
		if (Integer.parseInt(indicadorCargo) == 0) {
			String lecturaActual = getReporteHelperService().getDatoVariable(FacturaConstants.FIELD_VAR_LECT_ACTUAL,
				linea, offset);
			String lecturaAnterior = getReporteHelperService().getDatoVariable(
				FacturaConstants.FIELD_VAR_LECT_ANTERIOR, linea, offset);
			String diferenciaEntreLecturaActualYAnterior = getReporteHelperService().getDatoVariable(
				FacturaConstants.FIELD_VAR_DIFERENCIA_LECTURAS, linea, offset);
			String energiaConsumida = getReporteHelperService().getDatoVariable(
				FacturaConstants.FIELD_VAR_ENERGIA_CONSUMIDA, linea, offset);
			String energiaFacturada = getReporteHelperService().getDatoVariable(
				FacturaConstants.FIELD_VAR_ENERGIA_FACTURADA, linea, offset);
			String factor = getReporteHelperService().getDatoVariable(FacturaConstants.FIELD_VAR_FACTOR, linea, offset);

			reporteFactura.setLecturaActual(lecturaActual);
			reporteFactura.setLecturaAnterior(lecturaAnterior);
			reporteFactura.setDiferenciaEntreLecturaActualYAnterior(diferenciaEntreLecturaActualYAnterior);
			reporteFactura.setEnergiaConsumida(energiaConsumida);
			reporteFactura.setEnergiaFacturada(energiaFacturada);
			reporteFactura.setFactorLiquidacion(factor);

		}
	}

	public ReporteHelperService getReporteHelperService() {
		return reporteHelperService;
	}

	public void setReporteHelperService(ReporteHelperService reporteHelperService) {
		this.reporteHelperService = reporteHelperService;
	}

	public Double[] getInterrupciones(Date fechaFactura, Long nroCuenta, ReporteFactura reporteFactura) {
		Double[] interr = { new Double("0"), new Double("0"), new Double("0"), new Double("0") };
		try {
			LineaFactura lineaFactura = reporteHelperService.getDataLine(fechaFactura, nroCuenta, reporteFactura);
			if (lineaFactura == null) {
				return interr;
			}
			String linea = lineaFactura.getDatos();
			String tituloFES = "NUMERO DE INTERRUPCIONES (FES)";
			String tituloDES = "HORAS INTERRUMPIDAS (DES)";
			String tituloVMFES = "NUMERO DE INTERRUPCIONES(VMFES)";
			String tituloVMDES = "HORAS INTERRUNPIDAS(VMDES)";

			int[] posFES = getPosicionTitulo(linea, tituloFES);
			int[] posDES = getPosicionTitulo(linea, tituloDES);
			int[] posMax = getPosicionTitulo(linea, "INDICADORES MAXIMOS PERMITIDOS");
			int[] posVMFES = getPosicionTitulo(linea, tituloVMFES);
			int[] posVMDES = getPosicionTitulo(linea, tituloVMDES);

			interr[0] = getDoubleValue(linea, posFES[1] + 1, posDES[0] - 1);
			interr[1] = getDoubleValue(linea, posDES[1] + 1, posMax[0] - 1);
			interr[2] = getDoubleValue(linea, posVMFES[1] + 1, posVMDES[0] - 1);
			interr[3] = getDoubleValue(linea, posVMDES[1], posVMDES[0] + 44);
		}
		catch (Exception e) {

		}
		return interr;
	}

	private static Double getDoubleValue(String linea, int ini, int fin) {
		try {
			return new Double(linea.substring(ini, fin));
		}
		catch (Exception e) {
			return new Double("0");
		}
	}

	private static int[] getPosicionTitulo(String linea, String titulo) {
		int posiciones[] = { 0, linea.length() };
		posiciones[0] = linea.indexOf(titulo);
		posiciones[1] = linea.indexOf(":", posiciones[0] + titulo.length()) + 1;
		return posiciones;
	}

	private String getInformacionInteres(LineaFactura linea) {
		StringBuffer infoInteres = new StringBuffer();
		appendMensaje(getReporteHelperService().getDato(FacturaConstants.FIELD_VAR_MENSAJE_FIJO, linea), infoInteres);
		appendMensaje(getReporteHelperService().getDato(FacturaConstants.FIELD_VAR_MENSAJE_CUADRO1, linea), infoInteres);
		appendMensaje(getReporteHelperService().getDato(FacturaConstants.FIELD_VAR_MENSAJE_CUADRO2, linea), infoInteres);
		appendMensaje(getReporteHelperService().getDato(FacturaConstants.FIELD_VAR_MENSAJE_CUADRO3, linea), infoInteres);

		return infoInteres.toString();
	}

	private void appendMensaje(String mensaje, StringBuffer infoInteres) {
		if (!ReporteHelperService.NA.equals(mensaje)) {
			infoInteres.append(mensaje);
			infoInteres.append(NL);
		}
	}

	public ReporteFacturaDuplicadoLegal getReporteFacturaForTest() {
		ReporteFacturaDuplicadoLegal reporte = new ReporteFacturaDuplicadoLegalImpl();
		LineaFactura linea = new LineaFacturaImpl();
		linea
			.setDatos("  00000121MARIO N TORRES B                        DMD                    121000 011001MS001321010140  1000 011001MS001321010276                                                                                                                                                                                                                                                                                                                                                       0000000121000 011001MS001321010140 11BOGOT�, D.C.                            11001000BOGOT�, D.C.                            00SAN CRIST�BAL                           000LA GLORIA                               0013SAN MART�N SUR                          RURAL   CL 40 SUR NO 2 ESTE - 04 AP 100                                            PISO 1                                  MRSEN1  Comercial  B�sica                                  1120       0 N     0.00% 20.00%    3    3978239            11 FEB/200911 MAR/2009200903165462782001 SEP/2008                   174         0         0         0         0         0         0         0         0         0         0         0             56825000                      FACTURACION NORMAL01 OCT/2008                   189         0         0         0         0         0         0         0         0         0         0         0             61723000                      FACTURACION NORMAL01 NOV/2008                   180         0         0         0         0         0         0         0         0         0         0         0             60068000                      FACTURACION NORMAL01 DIC/2008                   178         0         0         0         0         0         0         0         0         0         0         0             59740000                      FACTURACION NORMAL01 ENE/2009                   199         0         0         0         0         0         0         0         0         0         0         0             67582000                      FACTURACION NORMAL01 FEB/2009                   183         0         0         0         0         0         0         0         0         0         0         0             62370000                      FACTURACION NORMAL       182         0         0         0         0         0         0         0         0       182         0E020CONSUMO ACTIVA SENCILLA                      1    204             $59,098   100%I198CONTRIBUCION COMERCIAL SENCILLA ACTIVA       1    204             $11,820    20%                                                                                                                                                                                                                                                                                                                                                FEB/2009CONSUMO DE ENERGIA   289.6952(Valor kWh)x                 204(Consumo en kWh)           $59,098CONTRIBUCION RES.079/97-CREG       20.00%           $11,820               %                             $70,918CARGO FIJO                 $0$70,918           25 MAR/2009           $70,920 SETENTA MIL NOVECIENTOS VEINTE  PESOS COLOMBIANOS                                                                                                                                                                                                        31 MAR/2009% 000<000001210000000007092001165462782>$0                                $2     ESTIMADO CLIENTE:           La tarifa final es de $289.6952 KWh                                                                                               ESTE ES UN SERVICIO NORMAL                                                                                                                                                                                                                                             308   314.2694   118.3845    22.7357   112.1254    29.8449     7.3145    23.8644     0.0000   314.2694   118.3845    22.7357   112.1254    29.8449     7.3145    23.8644     0.0000                                                                                                                                                                                                                                                   �4157707209914253802001000001211654627820�390000000000070920  4(415)7707209914253(8020)01000001211654627820(3900)00000000070920  4Circuito: VI21 Grupo: 1                  NUMERO DE INTERRUPCIONES (FES) :         0.0 HORAS INTERRUMPIDAS (DES)      :         0.0       INDICADORES MAXIMOS PERMITIDOS           NUMERO DE INTERRUPCIONES(VMFES) :        7.0HORAS INTERRUNPIDAS(VMDES) :          2.5                                                                                       SERVICIO DE FINANCIACION CODENSA HOGAR A:                                                                                                                                                                                                                         $0           $70,920                $0                $0                $0                $0VI21                53963TR1   752Empresa        Uso            Aerea          Nivel II                                               Real      Lectura Normal        13 ABR/200913 ABR/2009OCTUBRE - DICIEMBRE N$2,533                     0                                                                                                                    $0VALOR EN ACLARACION                                                                                                         03Electrico                               0000000120E0201  0   CONSUMO ACTIVA SENCILLA                                 7212  7008   204    1000       204       204  289.6952    $59,098           Electrico                               0000000121E7971  0   AJUSTE DECENA RES. CREG 108-97(DEB)                                                                               $2                Electrico                               0000000128I1981  0   CONTRIBUCION COMERCIAL SENCILLA ACTIVA                  7212  7008   204    1000       204       204  289.6952    $11,820           ");
		linea.setFechaFactura(new Date());
		linea.setNroCuenta(new Long(123));
		reporteHelperService.setupLineaFactura(reporte, linea);

		this.fillReporteFacturaDuplicadoLegal(reporte, linea);

		return reporte;
	}

	/**
	 * @param fecha: formato = 200903
	 */
	private String formatearFecha(String fecha) {
		String mes = fecha.substring(4);
		String anio = fecha.substring(0, 4);

		return "01 " + this.getMesAbreviado(Integer.valueOf(mes).intValue()) + "/" + anio;
	}

	private String getMesAbreviado(int mes) {
		String abrev = null;

		switch (mes) {
			case (1):
				abrev = "ENE";
				break;
			case (2):
				abrev = "FEB";
				break;
			case (3):
				abrev = "MAR";
				break;
			case (4):
				abrev = "ABR";
				break;
			case (5):
				abrev = "MAY";
				break;
			case (6):
				abrev = "JUN";
				break;
			case (7):
				abrev = "JUL";
				break;
			case (8):
				abrev = "AGO";
				break;
			case (9):
				abrev = "SET";
				break;
			case (10):
				abrev = "OCT";
				break;
			case (11):
				abrev = "NOV";
				break;
			case (12):
				abrev = "DIC";
				break;
		}

		return abrev;
	}
}