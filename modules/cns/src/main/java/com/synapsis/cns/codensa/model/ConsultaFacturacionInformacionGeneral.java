package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


public interface ConsultaFacturacionInformacionGeneral extends SynergiaBusinessObject{
	
	
	/**
	 * @return the saldoAnterior
	 */
	public abstract String getSaldoAnterior();

	/**
	 * @param saldoAnterior the saldoAnterior to set
	 */
	public abstract void setSaldoAnterior(String saldoAnterior);
	
	public abstract void setFechaProceso(Date fechaProceso);
	
	public abstract Date getFechaProceso();

	/**
	 * @return the totalCodensaHogar
	 */
	public abstract String getTotalCodensaHogar();

	/**
	 * @param totalCodensaHogar the totalCodensaHogar to set
	 */
	public abstract void setTotalCodensaHogar(String totalCodensaHogar);

	/**
	 * @return the totalConReconnecion
	 */
	public abstract String getTotalConReconnecion();

	/**
	 * @param totalConReconnecion the totalConReconnecion to set
	 */
	public abstract void setTotalConReconnecion(String totalConReconnecion);

	/**
	 * @return the totalConsumo
	 */
	public abstract String getTotalConsumo();

	/**
	 * @param totalConsumo the totalConsumo to set
	 */
	public abstract void setTotalConsumo(String totalConsumo);

	/**
	 * @return the totalEncargoCobranzas
	 */
	public abstract String getTotalEncargoCobranzas();

	/**
	 * @param totalEncargoCobranzas the totalEncargoCobranzas to set
	 */
	public abstract void setTotalEncargoCobranzas(String totalEncargoCobranzas);

	/**
	 * @return the totalFinanciado
	 */
	public abstract String getTotalFinanciado();

	/**
	 * @param totalFinanciado the totalFinanciado to set
	 */
	public abstract void setTotalFinanciado(String totalFinanciado);

	/**
	 * @return the totalInteres
	 */
	public abstract String getTotalInteres();

	/**
	 * @param totalInteres the totalInteres to set
	 */
	public abstract void setTotalInteres(String totalInteres);

	/**
	 * @return the totalOpAdic
	 */
	public abstract String getTotalOpAdic();

	/**
	 * @param totalOpAdic the totalOpAdic to set
	 */
	public abstract void setTotalOpAdic(String totalOpAdic);

	/**
	 * @return the totalSrvVentas
	 */
	public abstract String getTotalSrvVentas();

	/**
	 * @param totalSrvVentas the totalSrvVentas to set
	 */
	public abstract void setTotalSrvVentas(String totalSrvVentas);

	/**
	 * @return the nroCuenta
	 */
	public abstract Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public abstract void setNroCuenta(Long nroCuenta);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#getFechaFacturacion()
	 */
	public abstract Date getFechaFacturacion();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#getFechaLectura()
	 */
	public abstract Date getFechaLectura();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#getFechaVencimiento()
	 */
	public abstract Date getFechaVencimiento();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#getNumeroFactura()
	 */
	public abstract Integer getNumeroFactura();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#getPeriodo()
	 */
	public abstract String getPeriodo();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#getTotalFactura()
	 */
	public abstract String getTotalFactura();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#setFechaFacturacion(java.util.Date)
	 */
	public abstract void setFechaFacturacion(Date fechaFacturacion);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#setFechaLectura(java.util.Date)
	 */
	public abstract void setFechaLectura(Date fechaLectura);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#setFechaVencimiento(java.util.Date)
	 */
	public abstract void setFechaVencimiento(Date fechaVencimiento);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#setNumeroFactura(java.lang.Integer)
	 */
	public abstract void setNumeroFactura(Integer numeroFactura);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#setPeriodo(java.util.Date)
	 */
	public abstract void setPeriodo(String periodo);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#setTotalFactura(java.lang.String)
	 */
	public abstract void setTotalFactura(String totalFactura);
	
	/**
	 * @return the nroDocCli
	 */
	public String getNroDocCli();
	/**
	 * @param nroDocCli the nroDocCli to set
	 */
	public void setNroDocCli(String nroDocCli);
	
	public String getTipoLiquidacion();

	public void setTipoLiquidacion(String tipoLiquidacion);
	
	public String getTipoEsquemaFacturacion();

	public void setTipoEsquemaFacturacion(String tipoEsquemaFacturacion);

}