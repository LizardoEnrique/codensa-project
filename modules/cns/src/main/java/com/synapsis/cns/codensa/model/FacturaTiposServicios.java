package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface FacturaTiposServicios extends SynergiaBusinessObject {

	public Long getNroFactura();

	public void setNroFactura(Long nroFactura);

	public Long getNroServicio();

	public void setNroServicio(Long nroServicio);

	public String getTipoServicio();

	public void setTipoServicio(String tipoServicio);

}