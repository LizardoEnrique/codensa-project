/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaRefacturacionAjusteSaldo;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author dbraccio
 *
 *CU 016
 */
public class ConsultaRefacturacionAjusteSaldoImpl extends
		SynergiaBusinessObjectImpl implements ConsultaRefacturacionAjusteSaldo{

	private Long idCuenta;
	private Long nroCuenta;
	private Long idAjuste;
	private Long nroAjuste;
	private Double valorEnergiaAjuste;
	private Double valorOtrosNegociosAjuste;
	private Double saldoTotalAntesAjuste;
	private Double saldoTotalDespuesAjuste;
	private Double saldoEnergiaAntesAjuste;
	private Double saldoEnergiaDespuesAjuste;
	private Double saldoOtrosNegociosAntesAjuste;
	private Double saldoOtrosNEgociosDespuesAjuste;
	
	public Long getIdAjuste() {
		return idAjuste;
	}
	public Long getIdCuenta() {
		return idCuenta;
	}
	public Long getNroAjuste() {
		return nroAjuste;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public Double getSaldoEnergiaAntesAjuste() {
		return saldoEnergiaAntesAjuste;
	}
	public Double getSaldoEnergiaDespuesAjuste() {
		return saldoEnergiaDespuesAjuste;
	}
	public Double getSaldoOtrosNegociosAntesAjuste() {
		return saldoOtrosNegociosAntesAjuste;
	}
	public Double getSaldoOtrosNEgociosDespuesAjuste() {
		return saldoOtrosNEgociosDespuesAjuste;
	}
	public Double getSaldoTotalAntesAjuste() {
		return saldoTotalAntesAjuste;
	}
	public Double getSaldoTotalDespuesAjuste() {
		return saldoTotalDespuesAjuste;
	}
	public Double getValorEnergiaAjuste() {
		return valorEnergiaAjuste;
	}
	public Double getValorOtrosNegociosAjuste() {
		return valorOtrosNegociosAjuste;
	}
	public void setIdAjuste(Long idAjuste) {
		this.idAjuste = idAjuste;
	}
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	public void setNroAjuste(Long nroAjuste) {
		this.nroAjuste = nroAjuste;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public void setSaldoEnergiaAntesAjuste(Double saldoEnergiaAntesAjuste) {
		this.saldoEnergiaAntesAjuste = saldoEnergiaAntesAjuste;
	}
	public void setSaldoEnergiaDespuesAjuste(Double saldoEnergiaDespuesAjuste) {
		this.saldoEnergiaDespuesAjuste = saldoEnergiaDespuesAjuste;
	}
	public void setSaldoOtrosNegociosAntesAjuste(
			Double saldoOtrosNegociosAntesAjuste) {
		this.saldoOtrosNegociosAntesAjuste = saldoOtrosNegociosAntesAjuste;
	}
	public void setSaldoOtrosNEgociosDespuesAjuste(
			Double saldoOtrosNEgociosDespuesAjuste) {
		this.saldoOtrosNEgociosDespuesAjuste = saldoOtrosNEgociosDespuesAjuste;
	}
	public void setSaldoTotalAntesAjuste(Double saldoTotalAntesAjuste) {
		this.saldoTotalAntesAjuste = saldoTotalAntesAjuste;
	}
	public void setSaldoTotalDespuesAjuste(Double saldoTotalDespuesAjuste) {
		this.saldoTotalDespuesAjuste = saldoTotalDespuesAjuste;
	}
	public void setValorEnergiaAjuste(Double valorEnergiaAjuste) {
		this.valorEnergiaAjuste = valorEnergiaAjuste;
	}
	public void setValorOtrosNegociosAjuste(Double valorOtrosNegociosAjuste) {
		this.valorOtrosNegociosAjuste = valorOtrosNegociosAjuste;
	}
	
	
	
	

}
