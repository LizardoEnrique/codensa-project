package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaInformacionTransformadorByNroTransformador extends SynergiaBusinessObject {

	
	public String getCircuito();
	public void setCircuito(String circuito);
	public String getConexion();
	public void setConexion(String conexion);
	public String getPropiedadTra();
	public void setPropiedadTra(String propiedadTra);
	public String getTension();
	public void setTension(String tension);
	public String getTransformador();
	public void setTransformador(String transformador);
}
