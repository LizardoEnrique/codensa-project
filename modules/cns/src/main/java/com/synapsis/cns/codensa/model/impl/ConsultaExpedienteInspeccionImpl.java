package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaExpedienteInspeccion;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaExpedienteInspeccionImpl extends SynergiaBusinessObjectImpl implements ConsultaExpedienteInspeccion {
	private Date fechaDeteccion;	
	private String anomaliasEncontradas;	
	private String nombreApellidoVisita;
	private String tipoDocumentoVisita;	
	private String numeroDocumentoVisita;	
	private String calidadVisita;	
	private String observaciones;
	private Long numeroExpediente;
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteInspeccion#getAnomaliasEncontradas()
	 */
	public String getAnomaliasEncontradas() {
		return anomaliasEncontradas;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteInspeccion#setAnomaliasEncontradas(java.lang.String)
	 */
	public void setAnomaliasEncontradas(String anomaliasEncontradas) {
		this.anomaliasEncontradas = anomaliasEncontradas;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteInspeccion#getCalidadVisita()
	 */
	public String getCalidadVisita() {
		return calidadVisita;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteInspeccion#setCalidadVisita(java.lang.String)
	 */
	public void setCalidadVisita(String calidadVisita) {
		this.calidadVisita = calidadVisita;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteInspeccion#getFechaDeteccion()
	 */
	public Date getFechaDeteccion() {
		return fechaDeteccion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteInspeccion#setFechaDeteccion(java.util.Date)
	 */
	public void setFechaDeteccion(Date fechaDeteccion) {
		this.fechaDeteccion = fechaDeteccion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteInspeccion#getNombreApellidoVisita()
	 */
	public String getNombreApellidoVisita() {
		return nombreApellidoVisita;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteInspeccion#setNombreApellidoVisita(java.lang.String)
	 */
	public void setNombreApellidoVisita(String nombreApellidoVisita) {
		this.nombreApellidoVisita = nombreApellidoVisita;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteInspeccion#getNúmeroDocuementoVisita()
	 */
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteInspeccion#getNumeroOrdenInspeccion()
	 */
	public Long getNumeroOrdenInspeccion() {
		return this.getId();
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteInspeccion#setNumeroOrdenInspeccion(java.lang.Long)
	 */
	public void setNumeroOrdenInspeccion(Long numeroOrdenInspeccion) {
		this.setId(numeroOrdenInspeccion);
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteInspeccion#getObservaciones()
	 */
	public String getObservaciones() {
		return observaciones;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteInspeccion#setObservaciones(java.lang.String)
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteInspeccion#getTipoDocumentoVisita()
	 */
	public String getTipoDocumentoVisita() {
		return tipoDocumentoVisita;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteInspeccion#setTipoDocumentoVisita(java.lang.String)
	 */
	public void setTipoDocumentoVisita(String tipoDocumentoVisita) {
		this.tipoDocumentoVisita = tipoDocumentoVisita;
	}
	/**
	 * @return the numeroDocumentoVisita
	 */
	public String getNumeroDocumentoVisita() {
		return numeroDocumentoVisita;
	}
	/**
	 * @param numeroDocumentoVisita the numeroDocumentoVisita to set
	 */
	public void setNumeroDocumentoVisita(String numeroDocumentoVisita) {
		this.numeroDocumentoVisita = numeroDocumentoVisita;
	}
	public Long getNumeroExpediente() {
		return numeroExpediente;
	}
	public void setNumeroExpediente(Long numeroExpediente) {
		this.numeroExpediente = numeroExpediente;
	}		
	
	

}
