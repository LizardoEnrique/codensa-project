package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface ConsultaReposicion {

	/**
	 * @return the accRealizada
	 */
	public String getAccRealizada();

	/**
	 * @param accRealizada the accRealizada to set
	 */
	public void setAccRealizada(String accRealizada);

	/**
	 * @return the fechaEmision
	 */
	public Date getFechaEmision();

	/**
	 * @param fechaEmision the fechaEmision to set
	 */
	public void setFechaEmision(Date fechaEmision);

	/**
	 * @return the fechaReal
	 */
	public Date getFechaReal();

	/**
	 * @param fechaReal the fechaReal to set
	 */
	public void setFechaReal(Date fechaReal);

	/**
	 * @return the motivo
	 */
	public String getMotivo();

	/**
	 * @param motivo the motivo to set
	 */
	public void setMotivo(String motivo);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

}