package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface ConsultaServFinancCodensaHogar {

	/**
	 * @return the fechaActivProd
	 */
	public Date getFechaActivProd();

	/**
	 * @param fechaActivProd
	 *            the fechaActivProd to set
	 */
	public void setFechaActivProd(Date fechaActivProd);

	/**
	 * @return the fechaDesactProd
	 */
	public Date getFechaDesactProd();

	/**
	 * @param fechaDesactProd
	 *            the fechaDesactProd to set
	 */
	public void setFechaDesactProd(Date fechaDesactProd);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta
	 *            the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio();

	/**
	 * @param nroServicio
	 *            the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio);

	/**
	 * @return the valorOrigSrv
	 */
	public String getValorOrigSrv();

	/**
	 * @param valorOrigSrv
	 *            the valorOrigSrv to set
	 */
	public void setValorOrigSrv(String valorOrigSrv);

	/**
	 * @return the nroCuotasFac
	 */
	public Long getNroCuotasFac();

	/**
	 * @param nroCuotasFac
	 *            the nroCuotasFac to set
	 */
	public void setNroCuotasFac(Long nroCuotasFac);

	/**
	 * @return the nroCuotasFaltantes
	 */
	public Long getNroCuotasFaltantes();

	/**
	 * @param nroCuotasFaltantes
	 *            the nroCuotasFaltantes to set
	 */
	public void setNroCuotasFaltantes(Long nroCuotasFaltantes);

	/**
	 * @return the nroCuotasOri
	 */
	public Long getNroCuotasOri();

	/**
	 * @param nroCuotasOri
	 *            the nroCuotasOri to set
	 */
	public void setNroCuotasOri(Long nroCuotasOri);

	/**
	 * @return the periodAtrasado
	 */
	public Long getPeriodAtrasado();

	/**
	 * @param periodAtrasado
	 *            the periodAtrasado to set
	 */
	public void setPeriodAtrasado(Long periodAtrasado);

	/**
	 * @return the saldoCapital
	 */
	public String getSaldoCapital();

	/**
	 * @param saldoCapital
	 *            the saldoCapital to set
	 */
	public void setSaldoCapital(String saldoCapital);

	/**
	 * @return the saldoIntereses
	 */
	public String getSaldoIntereses();

	/**
	 * @param saldoIntereses
	 *            the saldoIntereses to set
	 */
	public void setSaldoIntereses(String saldoIntereses);

	/**
	 * @return the saldoIntMora
	 */
	public String getSaldoIntMora();

	/**
	 * @param saldoIntMora
	 *            the saldoIntMora to set
	 */
	public void setSaldoIntMora(String saldoIntMora);

	/**
	 * @return the tasaInteres
	 */
	public Double getTasaInteres();

	/**
	 * @param tasaInteres
	 *            the tasaInteres to set
	 */
	public void setTasaInteres(Double tasaInteres);

	/**
	 * @return the valorCuota
	 */
	public String getValorCuota();

	/**
	 * @param valorCuota
	 *            the valorCuota to set
	 */
	public void setValorCuota(String valorCuota);

	/**
	 * @return the deudaActual
	 */
	public String getDeudaActual();

	/**
	 * @param deudaActual
	 *            the deudaActual to set
	 */
	public void setDeudaActual(String deudaActual);

	/**
	 * @return the estado
	 */
	public String getEstado();

	/**
	 * @param estado
	 *            the estado to set
	 */
	public void setEstado(String estado);
	
	/**
	 * @return the deudaCapital
	 */
	public String getDeudaCapital();

	/**
	 * @param deudaCapital the deudaCapital to set
	 */
	public void setDeudaCapital(String deudaCapital);

	/**
	 * @return the deudaInteres
	 */
	public String getDeudaInteres();

	/**
	 * @param deudaInteres the deudaInteres to set
	 */
	public void setDeudaInteres(String deudaInteres);

	/**
	 * @return the nombreTitular
	 */
	public String getNomTitular();

	/**
	 * @param nombreTitular the nombreTitular to set
	 */
	public void setNomTitular(String nomTitular);

	/**
	 * @return the nroDocumento
	 */
	public String getDocTitular();
	/**
	 * @param nroDocumento the nroDocumento to set
	 */
	public void setDocTitular(String docTitular);

	public String getLineaNegocio();

	public void setLineaNegocio(String lineaNegocio);
	
}