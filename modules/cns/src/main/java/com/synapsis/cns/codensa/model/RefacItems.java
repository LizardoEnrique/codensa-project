package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface RefacItems {

	public Long getNroOrden();
	
	public void setNroOrden(Long nroOrden);
	
	/**
	 * @return the codigoItemAjuste
	 */
	public String getCodigoItemAjuste();

	/**
	 * @param codigoItemAjuste the codigoItemAjuste to set
	 */
	public void setCodigoItemAjuste(String codigoItemAjuste);

	/**
	 * @return the consumoKwhAjuste
	 */
	public Long getConsumoKwhAjuste();

	/**
	 * @param consumoKwhAjuste the consumoKwhAjuste to set
	 */
	public void setConsumoKwhAjuste(Long consumoKwhAjuste);

	/**
	 * @return the consumoKwhAntesAjuste
	 */
	public Long getConsumoKwhAntesAjuste();

	/**
	 * @param consumoKwhAntesAjuste the consumoKwhAntesAjuste to set
	 */
	public void setConsumoKwhAntesAjuste(Long consumoKwhAntesAjuste);

	/**
	 * @return the consumoKwhDespuesAjuste
	 */
	public Long getConsumoKwhDespuesAjuste();

	/**
	 * @param consumoKwhDespuesAjuste the consumoKwhDespuesAjuste to set
	 */
	public void setConsumoKwhDespuesAjuste(Long consumoKwhDespuesAjuste);

	/**
	 * @return the descItemAjuste
	 */
	public String getDescItemAjuste();

	/**
	 * @param descItemAjuste the descItemAjuste to set
	 */
	public void setDescItemAjuste(String descItemAjuste);

	/**
	 * @return the fechaAjusteItem
	 */
	public Date getFechaAjusteItem();

	/**
	 * @param fechaAjusteItem the fechaAjusteItem to set
	 */
	public void setFechaAjusteItem(Date fechaAjusteItem);

	/**
	 * @return the idAjuste
	 */
	public Long getIdAjuste();

	/**
	 * @param idAjuste the idAjuste to set
	 */
	public void setIdAjuste(Long idAjuste);

	/**
	 * @return the idServicio
	 */
	public Long getIdServicio();

	/**
	 * @param idServicio the idServicio to set
	 */
	public void setIdServicio(Long idServicio);

	/**
	 * @return the nroAjuste
	 */
	public String getNroAjuste();

	/**
	 * @param nroAjuste the nroAjuste to set
	 */
	public void setNroAjuste(String nroAjuste);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroDocumento
	 */
	public Long getNroDocumento();

	/**
	 * @param nroDocumento the nroDocumento to set
	 */
	public void setNroDocumento(Long nroDocumento);

	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio();

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio);

	/**
	 * @return the periodoDeFact
	 */
	public String getPeriodoDeFact();

	/**
	 * @param periodoDeFact the periodoDeFact to set
	 */
	public void setPeriodoDeFact(String periodoDeFact);

	/**
	 * @return the valorAjusteItem
	 */
	public Long getValorAjusteItem();

	/**
	 * @param valorAjusteItem the valorAjusteItem to set
	 */
	public void setValorAjusteItem(Long valorAjusteItem);

	/**
	 * @return the valorItemAntesAjuste
	 */
	public Long getValorItemAntesAjuste();

	/**
	 * @param valorItemAntesAjuste the valorItemAntesAjuste to set
	 */
	public void setValorItemAntesAjuste(Long valorItemAntesAjuste);

	/**
	 * @return the valorItemDespuesAjuste
	 */
	public Long getValorItemDespuesAjuste();

	/**
	 * @param valorItemDespuesAjuste the valorItemDespuesAjuste to set
	 */
	public void setValorItemDespuesAjuste(Long valorItemDespuesAjuste);

}