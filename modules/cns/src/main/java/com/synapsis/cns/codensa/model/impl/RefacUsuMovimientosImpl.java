package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import com.synapsis.cns.codensa.model.RefacUsuMovimientos;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class RefacUsuMovimientosImpl extends SynergiaBusinessObjectImpl
		implements RefacUsuMovimientos {

	private Long nroCuenta;

	private String nroAjuste;

	private String observaciones;

	private String movimientos;
	private Long valorAjustado;

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getNroAjuste() {
		return nroAjuste;
	}

	public void setNroAjuste(String nroAjuste) {
		this.nroAjuste = nroAjuste;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getMovimientos() {
		return movimientos;
	}

	public void setMovimientos(String movimientos) {
		this.movimientos = movimientos;
	}

	public Long getValorAjustado() {
		return valorAjustado;
	}

	public void setValorAjustado(Long valorAjustado) {
		this.valorAjustado = valorAjustado;
	}
	
	
	
}