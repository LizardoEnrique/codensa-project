package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.DetalleCargosConvenio;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class DetalleCargosConvenioImpl extends SynergiaBusinessObjectImpl
		implements DetalleCargosConvenio {
	private Long nroCuenta;
	public String codigoCargo;
	public String descripcionCargo;
	private Double valorCargo;
	private Date fechaFacturacion;
	private String periodoFacturacion;
	private String tipoDocumento;
	private Long nroDocumentoAssoc;

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getCodigoCargo() {
		return codigoCargo;
	}

	public void setCodigoCargo(String codCargo) {
		this.codigoCargo = codCargo;
	}

	public String getDescripcionCargo() {
		return descripcionCargo;
	}

	public void setDescripcionCargo(String descCargo) {
		this.descripcionCargo = descCargo;
	}

	public Double getValorCargo() {
		return valorCargo;
	}

	public void setValorCargo(Double valorCargo) {
		this.valorCargo = valorCargo;
	}

	public Date getFechaFacturacion() {
		return fechaFacturacion;
	}

	public void setFechaFacturacion(Date fecFactura) {
		this.fechaFacturacion = fecFactura;
	}

	public String getPeriodoFacturacion() {
		return periodoFacturacion;
	}

	public void setPeriodoFacturacion(String periodoFacturacion) {
		this.periodoFacturacion = periodoFacturacion;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public Long getNroDocumentoAssoc() {
		return nroDocumentoAssoc;
	}

	public void setNroDocumentoAssoc(Long nroDocumento) {
		this.nroDocumentoAssoc = nroDocumento;
	}

}
