package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaDeudaPagos;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.1 $ $Date: 2007/02/28 21:47:47 $
 * 
 */
public class ConsultaDeudaPagosImpl extends SynergiaBusinessObjectImpl
		implements ConsultaDeudaPagos {
	/**
	 * Comment for <code>valorTotalUltimoPago</code>
	 */
	private Double valorTotalUltimoPago;

	/**
	 * Comment for <code>medioPago</code>
	 */
	private String medioPago;

	/**
	 * Comment for <code>nombreSucursalRecaudo</code>
	 */
	private String nombreSucursalRecaudo;

	/**
	 * Comment for <code>entidadRecaudadora</code>
	 */
	private String entidadRecaudadora;

	/**
	 * Comment for <code>fechaUltimoPago</code>
	 */
	private Date fechaUltimoPago;

	/**
	 * @return the entidadRecaudadora
	 */
	public String getEntidadRecaudadora() {
		return this.entidadRecaudadora;
	}

	/**
	 * @param entidadRecaudadora
	 *            the entidadRecaudadora to set
	 */
	public void setEntidadRecaudadora(String entidadRecaudadora) {
		this.entidadRecaudadora = entidadRecaudadora;
	}

	/**
	 * @return the fechaUltimoPago
	 */
	public Date getFechaUltimoPago() {
		return this.fechaUltimoPago;
	}

	/**
	 * @param fechaUltimoPago
	 *            the fechaUltimoPago to set
	 */
	public void setFechaUltimoPago(Date fechaUltimoPago) {
		this.fechaUltimoPago = fechaUltimoPago;
	}

	/**
	 * @return the medioPago
	 */
	public String getMedioPago() {
		return this.medioPago;
	}

	/**
	 * @param medioPago
	 *            the medioPago to set
	 */
	public void setMedioPago(String medioPago) {
		this.medioPago = medioPago;
	}

	/**
	 * @return the nombreSucursalRecaudo
	 */
	public String getNombreSucursalRecaudo() {
		return this.nombreSucursalRecaudo;
	}

	/**
	 * @param nombreSucursalRecaudo
	 *            the nombreSucursalRecaudo to set
	 */
	public void setNombreSucursalRecaudo(String nombreSucursalRecaudo) {
		this.nombreSucursalRecaudo = nombreSucursalRecaudo;
	}

	/**
	 * @return the valorTotalUltimoPago
	 */
	public Double getValorTotalUltimoPago() {
		return this.valorTotalUltimoPago;
	}

	/**
	 * @param valorTotalUltimoPago
	 *            the valorTotalUltimoPago to set
	 */
	public void setValorTotalUltimoPago(Double valorTotalUltimoPago) {
		this.valorTotalUltimoPago = valorTotalUltimoPago;
	}

}