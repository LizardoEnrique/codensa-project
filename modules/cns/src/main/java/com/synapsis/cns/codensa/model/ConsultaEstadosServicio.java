package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface ConsultaEstadosServicio {

	/**
	 * @return the estado
	 */
	public String getEstado();

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado);

	/**
	 * @return the fechaCambioEstado
	 */
	public Date getFechaCambioEstado();

	/**
	 * @param fechaCambioEstado the fechaCambioEstado to set
	 */
	public void setFechaCambioEstado(Date fechaCambioEstado);

	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio();

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio);

	/**
	 * @return the usuarioEjecutor
	 */
	public Long getUsuarioEjecutor();

	/**
	 * @param usuarioEjecutor the usuarioEjecutor to set
	 */
	public void setUsuarioEjecutor(Long usuarioEjecutor);
	
	/**
	 * @return the motivo
	 */
	public String getMotivo();
	/**
	 * @param motivo the motivo to set
	 */
	public void setMotivo(String motivo);	

}