package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


/**
 * @author m3.MarioRoss - 13/11/2006
 */
public interface AtencionAic extends SynergiaBusinessObject {
	
	public Long getNroCuenta();
	public void setNroCuenta(Long nroCuenta);
	
	public Long getNroContacto();
	public void setNroContacto(Long nroContacto);

	public Long getIdContactoDist();
	public void setIdContactoDist(Long idContactoDist);
	
	public String getEstado();
	public void setEstado(String estado);
	
	public Long getNroAtencion();
	public void setNroAtencion(Long nroAtencion);
	
	public String getTipoServicio();
	public void setTipoServicio(String tipoServicio);
	
	public Long getNroServicio();
	public void setNroServicio(Long nroServicio);
	
	public String getTipo();
	public void setTipo(String tipo);
	
	public String getDescripcionArea();
	public void setDescripcionArea(String descripcionArea);
	
	public String getMotivoCliente();
	public void setMotivoCliente(String motivoCliente);
	
	public Date getFechaCreacion();
	public void setFechaCreacion(Date fechaCreacion);
	
	public Date getFechaFinalizacion();
	public void setFechaFinalizacion(Date fechaFinalizacion);
	
	public Date getFechaVencimientoInterno();
	public void setFechaVencimientoInterno(Date fechaVencimientoInterno);
	
	public Date getFechaVencimientoLegal();
	public void setFechaVencimientoLegal(Date fechaVencimientoLegal);
	
	public String getUsuarioResponsable();
	public void setUsuarioResponsable(String usuarioResponsable);
	
	public String getNombreResponsable();
	public void setNombreResponsable(String nombreResponsable);
	
	public String getTipoRespuesta();
	public void setTipoRespuesta(String tipoRespuesta);
	
	public Long getIdAtencion();
	public void setIdAtencion(Long idAtencion);
	
	public String getAreaInvolucradaAnterior();
	public void setAreaInvolucradaAnterior(String areaInvolucradaAnterior);
	
	public String getMotivoClienteAnterior();
	public void setMotivoClienteAnterior(String motivoClienteAnterior);
	
	public String getTipoAtencionAnterior();
	public void setTipoAtencionAnterior(String tipoAtencionAnterior);

}
