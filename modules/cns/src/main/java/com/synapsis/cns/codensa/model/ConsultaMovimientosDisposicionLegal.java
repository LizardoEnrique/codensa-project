package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaMovimientosDisposicionLegal extends SynergiaBusinessObject {

	//public Empresa getEmpresa();
	//public void setEmpresa(Empresa empresa);

	public Long getId();
	public void setId(Long id);

	public String getDescripcion();
	public void setDescripcion(String descripcion);

	public Long getIdMovRefa();
	public void setIdMovRefa(Long idMovRefa);


	public Long getNroAjuste();
	public void setNroAjuste(Long nroAjuste);
	

}
