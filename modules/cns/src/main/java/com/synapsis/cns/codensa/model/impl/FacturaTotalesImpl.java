package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import com.synapsis.cns.codensa.model.FacturaTotales;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class FacturaTotalesImpl extends SynergiaBusinessObjectImpl implements
		FacturaTotales {

	private Long nroFactura;

	private Long subtCargosCons;

	private Double subtOtrosCargos;

	private Double subtCargosDesc;

	private Double subtTotCargosEne;

	private Double subtCargosSrvPort;

	private Double totalFactura;

	
	public Long getNroFactura() {
		return nroFactura;
	}

	public void setNroFactura(Long nroFactura) {
		this.nroFactura = nroFactura;
	}

	public Long getSubtCargosCons() {
		return subtCargosCons;
	}

	public void setSubtCargosCons(Long subtCargosCons) {
		this.subtCargosCons = subtCargosCons;
	}

	public Double getSubtOtrosCargos() {
		return subtOtrosCargos;
	}

	public void setSubtOtrosCargos(Double subtOtrosCargos) {
		this.subtOtrosCargos = subtOtrosCargos;
	}

	public Double getSubtCargosDesc() {
		return subtCargosDesc;
	}

	public void setSubtCargosDesc(Double subtCargosDesc) {
		this.subtCargosDesc = subtCargosDesc;
	}

	public Double getSubtTotCargosEne() {
		return subtTotCargosEne;
	}

	public void setSubtTotCargosEne(Double subtTotCargosEne) {
		this.subtTotCargosEne = subtTotCargosEne;
	}

	public Double getSubtCargosSrvPort() {
		return subtCargosSrvPort;
	}

	public void setSubtCargosSrvPort(Double subtCargosSrvPort) {
		this.subtCargosSrvPort = subtCargosSrvPort;
	}

	public Double getTotalFactura() {
		return totalFactura;
	}

	public void setTotalFactura(Double	totalFactura) {
		this.totalFactura = totalFactura;
	}
}