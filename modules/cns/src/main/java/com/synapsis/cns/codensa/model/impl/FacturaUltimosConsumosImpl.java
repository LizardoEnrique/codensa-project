package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.FacturaUltimosConsumos;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author jhv
 */

public class FacturaUltimosConsumosImpl extends SynergiaBusinessObjectImpl
		implements FacturaUltimosConsumos {

	private String nroFactura;

	private String mes;

	private Double cons;

	public String getNroFactura() {
		return nroFactura;
	}

	public void setNroFactura(String nroFactura) {
		this.nroFactura = nroFactura;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public Double getCons() {
		return cons;
	}

	public void setCons(Double cons) {
		this.cons = cons;
	}
}