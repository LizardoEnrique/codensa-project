package com.synapsis.cns.codensa.web;

/**
 * @author dbraccio - assert solutions 14/05/2008
 * @egrande REFACTOR: creo ConsultaPrincipalUniqueBB (13/08/2009)
 */
public class ConsultaOrdenReconexionBB extends ConsultaPrincipalUniqueBB {
	
	public void buscarConsultaOrdenReconexion() {
		findObject("reposicionService");
	}
}
