/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaCuentaLecturasConsumo;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 *
 */
public class ConsultaCuentaLecturasConsumoImpl extends SynergiaBusinessObjectImpl implements ConsultaCuentaLecturasConsumo {

	private static final long serialVersionUID = 1L;
	
	private Long idCuenta;
	private Long idDetalleCuenta;
	private Long nroCuenta;
	private Date fechaProceso;
	private Long lecturaAnteriorEAFP;
	private Long lecturaActualEAFP;
	private Double consumoEAFP;
	private Long lecturaAnteriorEAXP;
	private Long lecturaActualEAXP;
	private Double consumoEAXP;
	private Long lecturaAnteriorERHP;
	private Long lecturaActualERHP;
	private Double consumoERHP;
	private Long lecturaAnteriorERXP;
	private Long lecturaActualERXP;
	private Double consumoERXP;
	private Double demandaFP;
	private Double demandaHP;
	private Double demandaXP;
	private Double consumoERFP;
	private Double consumoEAHP;
	
	private Long lecAnteriorERHP;
	private Long lecAcERHP;
	private Long lecAntERXP;
	private Long lecAcERXP;
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#getConsumoEAFP()
	 */
	public Double getConsumoEAFP() {
		return consumoEAFP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#setConsumoEAFP(java.lang.Long)
	 */
	public void setConsumoEAFP(Double consumoEAFP) {
		this.consumoEAFP = consumoEAFP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#getConsumoEAXP()
	 */
	public Double getConsumoEAXP() {
		return consumoEAXP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#setConsumoEAXP(java.lang.Long)
	 */
	public void setConsumoEAXP(Double consumoEAXP) {
		this.consumoEAXP = consumoEAXP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#getConsumoERHP()
	 */
	public Double getConsumoERHP() {
		return consumoERHP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#setConsumoERHP(java.lang.Long)
	 */
	public void setConsumoERHP(Double consumoERHP) {
		this.consumoERHP = consumoERHP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#getConsumoERXP()
	 */
	public Double getConsumoERXP() {
		return consumoERXP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#setConsumoERXP(java.lang.Long)
	 */
	public void setConsumoERXP(Double consumoERXP) {
		this.consumoERXP = consumoERXP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#getDemandaFP()
	 */
	public Double getDemandaFP() {
		return demandaFP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#setDemandaFP(java.lang.Long)
	 */
	public void setDemandaFP(Double demandaFP) {
		this.demandaFP = demandaFP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#getDemandaHP()
	 */
	public Double getDemandaHP() {
		return demandaHP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#setDemandaHP(java.lang.Long)
	 */
	public void setDemandaHP(Double demandaHP) {
		this.demandaHP = demandaHP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#getDemandaXP()
	 */
	public Double getDemandaXP() {
		return demandaXP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#setDemandaXP(java.lang.Long)
	 */
	public void setDemandaXP(Double demandaXP) {
		this.demandaXP = demandaXP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#getFechaProceso()
	 */
	public Date getFechaProceso() {
		return fechaProceso;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#setFechaProceso(java.util.Date)
	 */
	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#getIdCuenta()
	 */
	public Long getIdCuenta() {
		return idCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#setIdCuenta(java.lang.Long)
	 */
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#getIdDetalleCuenta()
	 */
	public Long getIdDetalleCuenta() {
		return idDetalleCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#setIdDetalleCuenta(java.lang.Long)
	 */
	public void setIdDetalleCuenta(Long idDetalleCuenta) {
		this.idDetalleCuenta = idDetalleCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#getLecturaActualEAFP()
	 */
	public Long getLecturaActualEAFP() {
		return lecturaActualEAFP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#setLecturaActualEAFP(java.lang.Long)
	 */
	public void setLecturaActualEAFP(Long lecturaActualEAFP) {
		this.lecturaActualEAFP = lecturaActualEAFP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#getLecturaActualEAXP()
	 */
	public Long getLecturaActualEAXP() {
		return lecturaActualEAXP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#setLecturaActualEAXP(java.lang.Long)
	 */
	public void setLecturaActualEAXP(Long lecturaActualEAXP) {
		this.lecturaActualEAXP = lecturaActualEAXP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#getLecturaActualERHP()
	 */
	public Long getLecturaActualERHP() {
		return lecturaActualERHP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#setLecturaActualERHP(java.lang.Long)
	 */
	public void setLecturaActualERHP(Long lecturaActualERHP) {
		this.lecturaActualERHP = lecturaActualERHP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#getLecturaActualERXP()
	 */
	public Long getLecturaActualERXP() {
		return lecturaActualERXP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#setLecturaActualERXP(java.lang.Long)
	 */
	public void setLecturaActualERXP(Long lecturaActualERXP) {
		this.lecturaActualERXP = lecturaActualERXP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#getLecturaAnteriorEAFP()
	 */
	public Long getLecturaAnteriorEAFP() {
		return lecturaAnteriorEAFP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#setLecturaAnteriorEAFP(java.lang.Long)
	 */
	public void setLecturaAnteriorEAFP(Long lecturaAnteriorEAFP) {
		this.lecturaAnteriorEAFP = lecturaAnteriorEAFP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#getLecturaAnteriorEAXP()
	 */
	public Long getLecturaAnteriorEAXP() {
		return lecturaAnteriorEAXP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#setLecturaAnteriorEAXP(java.lang.Long)
	 */
	public void setLecturaAnteriorEAXP(Long lecturaAnteriorEAXP) {
		this.lecturaAnteriorEAXP = lecturaAnteriorEAXP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#getLecturaAnteriorERHP()
	 */
	public Long getLecturaAnteriorERHP() {
		return lecturaAnteriorERHP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#setLecturaAnteriorERHP(java.lang.Long)
	 */
	public void setLecturaAnteriorERHP(Long lecturaAnteriorERHP) {
		this.lecturaAnteriorERHP = lecturaAnteriorERHP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#getLecturaAnteriorERXP()
	 */
	public Long getLecturaAnteriorERXP() {
		return lecturaAnteriorERXP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#setLecturaAnteriorERXP(java.lang.Long)
	 */
	public void setLecturaAnteriorERXP(Long lecturaAnteriorERXP) {
		this.lecturaAnteriorERXP = lecturaAnteriorERXP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumo#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public Double getConsumoEAHP() {
		return consumoEAHP;
	}
	public void setConsumoEAHP(Double consumoEAHP) {
		this.consumoEAHP = consumoEAHP;
	}
	public Double getConsumoERFP() {
		return consumoERFP;
	}
	public void setConsumoERFP(Double consumoERFP) {
		this.consumoERFP = consumoERFP;
	}
	public Long getLecAnteriorERHP() {
		return lecAnteriorERHP;
	}
	public void setLecAnteriorERHP(Long lecAnteriorERHP) {
		this.lecAnteriorERHP = lecAnteriorERHP;
	}
	public Long getLecAcERHP() {
		return lecAcERHP;
	}
	public void setLecAcERHP(Long lecAcERHP) {
		this.lecAcERHP = lecAcERHP;
	}
	public Long getLecAntERXP() {
		return lecAntERXP;
	}
	public void setLecAntERXP(Long lecAntERXP) {
		this.lecAntERXP = lecAntERXP;
	}
	public Long getLecAcERXP() {
		return lecAcERXP;
	}
	public void setLecAcERXP(Long lecAcERXP) {
		this.lecAcERXP = lecAcERXP;
	}
}
