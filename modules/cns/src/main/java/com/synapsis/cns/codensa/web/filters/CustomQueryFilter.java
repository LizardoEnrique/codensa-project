package com.synapsis.cns.codensa.web.filters;

/**
 * Modelo de comportamiento de un queryFilter el cual deja setear un servicio en
 * particular. El mismo es para backingBeans que no tienen un service assoc.
 * 
 * @author Emiliano Arango (ar30557486)
 * 
 */
public interface CustomQueryFilter {
	/**
	 * @return the serviceName
	 */
	public String getServiceName();

	/**
	 * @param serviceName
	 *            the serviceName to set
	 */
	public void setServiceName(String serviceName);
}
