package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface CongAprob extends SynergiaBusinessObject {

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public Long getNroSaldo();

	public void setNroSaldo(Long nroSaldo);

	public Date getFecCreacion();

	public void setFecCreacion(Date fecCreacion);

	public String getUsuarioCreador();

	public void setUsuarioCreador(String usuarioCreador);

	public Date getFechaAprobacion();

	public void setFechaAprobacion(Date fechaAprobacion);

	public String getUsuarioAprobador();

	public void setUsuarioAprobador(String usuarioAprobador);

	public Double getMontoTotal();

	public void setMontoTotal(Double montoTotal);

	public Double getEnergiaSd();

	public void setEnergiaSd(Double energiaSd);

	public String getTipoOperacion() ;

	public void setTipoOperacion(String tipoOperacion) ;

}