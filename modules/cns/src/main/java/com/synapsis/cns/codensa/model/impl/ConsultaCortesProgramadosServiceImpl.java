package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaCortesProgramadosService;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaCortesProgramadosServiceImpl extends SynergiaBusinessObjectImpl implements ConsultaCortesProgramadosService {
	private static final long serialVersionUID = 1L;
	
	private String codCircuito;
	private String codTransformador;
	private String nroMedidor;
	private String marcaMedidor;
	private String regional;
	private String comercializador;
	private String grupoRuta;
	private String gestorNegocio;
	private String cicloFacturacion;
	private String manzana;	
	private String corrFacturacion;
	private Long nroCuenta;
	private Long nroServicio;
	private Long nroCuentaPadre;
	private String titularCuenta;
	private String direccion;
	private String municipio;
	private String localizacion;
	private String dirRepartoEspecial;
	private String contacto;
	private String telContacto;
	private String isVip;
	private Date fechaInicioPrevista;
	private Date fechaFinPrevista;
	private Date fechaProceso;
	private String tipoClienteEmpresarial;
	private Date fechaProcesoTrunc;
	private Date fechaInicioPrevistaTrunc;

	public String getCodCircuito() {
		return codCircuito;
	}
	public void setCodCircuito(String codCircuito) {
		this.codCircuito = codCircuito;
	}
	public String getCodTransformador() {
		return codTransformador;
	}
	public void setCodTransformador(String codTransformador) {
		this.codTransformador = codTransformador;
	}
	public String getComercializador() {
		return comercializador;
	}
	public void setComercializador(String comercializador) {
		this.comercializador = comercializador;
	}
	public String getContacto() {
		return contacto;
	}
	public void setContacto(String contacto) {
		this.contacto = contacto;
	}
	public String getCorrFacturacion() {
		return corrFacturacion;
	}
	public void setCorrFacturacion(String corrFacturacion) {
		this.corrFacturacion = corrFacturacion;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getDirRepartoEspecial() {
		return dirRepartoEspecial;
	}
	public void setDirRepartoEspecial(String dirRepartoEspecial) {
		this.dirRepartoEspecial = dirRepartoEspecial;
	}
	public Date getFechaFinPrevista() {
		return fechaFinPrevista;
	}
	public void setFechaFinPrevista(Date fechaFinPrevista) {
		this.fechaFinPrevista = fechaFinPrevista;
	}
	public Date getFechaInicioPrevista() {
		return fechaInicioPrevista;
	}
	public void setFechaInicioPrevista(Date fechaInicioPrevista) {
		this.fechaInicioPrevista = fechaInicioPrevista;
	}
	public Date getFechaProceso() {
		return fechaProceso;
	}
	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}
	public String getGestorNegocio() {
		return gestorNegocio;
	}
	public void setGestorNegocio(String gestorNegocio) {
		this.gestorNegocio = gestorNegocio;
	}
	public String getGrupoRuta() {
		return grupoRuta;
	}
	public void setGrupoRuta(String grupoRuta) {
		this.grupoRuta = grupoRuta;
	}
	public String getIsVip() {
		return isVip;
	}
	public void setIsVip(String isVip) {
		this.isVip = isVip;
	}
	public String getLocalizacion() {
		return localizacion;
	}
	public void setLocalizacion(String localizacion) {
		this.localizacion = localizacion;
	}
	public String getMarcaMedidor() {
		return marcaMedidor;
	}
	public void setMarcaMedidor(String marcaMedidor) {
		this.marcaMedidor = marcaMedidor;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public Long getNroCuentaPadre() {
		return nroCuentaPadre;
	}
	public void setNroCuentaPadre(Long nroCuentaPadre) {
		this.nroCuentaPadre = nroCuentaPadre;
	}
	public String getNroMedidor() {
		return nroMedidor;
	}
	public void setNroMedidor(String nroMedidor) {
		this.nroMedidor = nroMedidor;
	}
	public Long getNroServicio() {
		return nroServicio;
	}
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	public String getRegional() {
		return regional;
	}
	public void setRegional(String regional) {
		this.regional = regional;
	}
	public String getTelContacto() {
		return telContacto;
	}
	public void setTelContacto(String telContacto) {
		this.telContacto = telContacto;
	}
	public String getTipoClienteEmpresarial() {
		return tipoClienteEmpresarial;
	}
	public void setTipoClienteEmpresarial(String tipoClienteEmpresarial) {
		this.tipoClienteEmpresarial = tipoClienteEmpresarial;
	}
	public String getTitularCuenta() {
		return titularCuenta;
	}
	public void setTitularCuenta(String titularCuenta) {
		this.titularCuenta = titularCuenta;
	}
	public String getManzana() {
		return manzana;
	}
	public void setManzana(String manzana) {
		this.manzana = manzana;
	}
	public String getCicloFacturacion() {
		return cicloFacturacion;
	}
	public void setCicloFacturacion(String cicloFacturacion) {
		this.cicloFacturacion = cicloFacturacion;
	}
	public Date getFechaProcesoTrunc() {
		return fechaProcesoTrunc;
	}
	public void setFechaProcesoTrunc(Date fechaProcesoTrunc) {
		this.fechaProcesoTrunc = fechaProcesoTrunc;
	}
	public Date getFechaInicioPrevistaTrunc() {
		return fechaInicioPrevistaTrunc;
	}
	public void setFechaInicioPrevistaTrunc(Date fechaInicioPrevistaTrunc) {
		this.fechaInicioPrevistaTrunc = fechaInicioPrevistaTrunc;
	}
}
