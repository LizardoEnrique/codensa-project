package com.synapsis.cns.codensa.model;

public interface ConsultaDatosGenerales {

	/**
	 * @return the llave
	 */
	public String getLlave();

	/**
	 * @param llave the llave to set
	 */
	public void setLlave(String llave);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroLote
	 */
	public Long getNroLote();

	/**
	 * @param nroLote the nroLote to set
	 */
	public void setNroLote(Long nroLote);

	/**
	 * @return the numPcr
	 */
	public Double getNumPcr();

	/**
	 * @param numPcr the numPcr to set
	 */
	public void setNumPcr(Double numPcr);

	public Long getClienteAnt() ;
	/**
	 * @param clienteAnt the clienteAnt to set
	 */
	public void setClienteAnt(Long clienteAnt);
	/**
	 * @return the recargoTension
	 */
	public Long getRecargoTension();
	/**
	 * @param recargoTension the recargoTension to set
	 */
	public void setRecargoTension(Long recargoTension);
}