package com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada;

import com.synapsis.cns.codensa.model.buscaCuenta.avanzada.BuscaCuentaAvanzadaNroContacto;
import com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaImpl;

public class BuscaCuentaAvanzadaNroContactoImpl extends BuscaCuentaImpl 
implements BuscaCuentaAvanzadaNroContacto {
	
	private Integer nroContacto;

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada.BuscaCuentaAvanzadaNroContacto#getNroContacto()
	 */
	public Integer getNroContacto() {
		return nroContacto;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada.BuscaCuentaAvanzadaNroContacto#setNroContacto(java.lang.Long)
	 */
	public void setNroContacto(Integer nroContacto) {
		this.nroContacto = nroContacto;
	}	
}
