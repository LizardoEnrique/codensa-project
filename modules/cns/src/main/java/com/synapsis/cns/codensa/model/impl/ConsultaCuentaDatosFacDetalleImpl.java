/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaCuentaDatosFacDetalle;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 *
 */
public class ConsultaCuentaDatosFacDetalleImpl extends SynergiaBusinessObjectImpl implements ConsultaCuentaDatosFacDetalle {
	
	private Long idDetalleCuenta;
	private Date fechaEmicion;
	private Date periodoFacturacion;
	private Date fechaPrimerVto;
	private Date fechaSegundoVto;
	private Long totalPagar;
	private Long idConfigFact;
	private Long idCuenta;
	private Long indicadorConsumo;
	private String codFactura;
	private String messFactura;
	private Long nroCuenta;
	
	
	/**
	 * @return the indicadorConsumo
	 */
	public Long getIndicadorConsumo() {
		return indicadorConsumo;
	}
	/**
	 * @param indicadorConsumo the indicadorConsumo to set
	 */
	public void setIndicadorConsumo(Long indicadorConsumo) {
		this.indicadorConsumo = indicadorConsumo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosFacDetalle#getFechaEmicion()
	 */
	public Date getFechaEmicion() {
		return fechaEmicion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosFacDetalle#setFechaEmicion(java.util.Date)
	 */
	public void setFechaEmicion(Date fechaEmicion) {
		this.fechaEmicion = fechaEmicion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosFacDetalle#getFechaPrimerVto()
	 */
	public Date getFechaPrimerVto() {
		return fechaPrimerVto;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosFacDetalle#setFechaPrimerVto(java.util.Date)
	 */
	public void setFechaPrimerVto(Date fechaPrimerVto) {
		this.fechaPrimerVto = fechaPrimerVto;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosFacDetalle#getFechaSegundoVto()
	 */
	public Date getFechaSegundoVto() {
		return fechaSegundoVto;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosFacDetalle#setFechaSegundoVto(java.util.Date)
	 */
	public void setFechaSegundoVto(Date fechaSegundoVto) {
		this.fechaSegundoVto = fechaSegundoVto;
	}
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosFacDetalle#getIdCuenta()
	 */
	public Long getIdCuenta() {
		return idCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosFacDetalle#setIdCuenta(java.lang.Long)
	 */
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosFacDetalle#getIdDetalleCuenta()
	 */
	public Long getIdDetalleCuenta() {
		return idDetalleCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosFacDetalle#setIdDetalleCuenta(java.lang.Long)
	 */
	public void setIdDetalleCuenta(Long idDetalleCuenta) {
		this.idDetalleCuenta = idDetalleCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosFacDetalle#getPeriodoFacturacion()
	 */
	public Date getPeriodoFacturacion() {
		return periodoFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosFacDetalle#setPeriodoFacturacion(java.util.Date)
	 */
	public void setPeriodoFacturacion(Date periodoFacturacion) {
		this.periodoFacturacion = periodoFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosFacDetalle#getTotalPagar()
	 */
	public Long getTotalPagar() {
		return totalPagar;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosFacDetalle#setTotalPagar(java.lang.Long)
	 */
	public void setTotalPagar(Long totalPagar) {
		this.totalPagar = totalPagar;
	}
	/**
	 * @return the idConfigFact
	 */
	public Long getIdConfigFact() {
		return idConfigFact;
	}
	/**
	 * @param idConfigFact the idConfigFact to set
	 */
	public void setIdConfigFact(Long idConfigFact) {
		this.idConfigFact = idConfigFact;
	}
	public String getCodFactura() {
		return codFactura;
	}
	public void setCodFactura(String codFactura) {
		this.codFactura = codFactura;
	}
	public String getMessFactura() {
		return messFactura;
	}
	public void setMessFactura(String messFactura) {
		this.messFactura = messFactura;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	
	
	
}