package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import java.util.Date;


import com.synapsis.cns.codensa.model.RefacItems;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class RefacItemsImpl extends SynergiaBusinessObjectImpl implements RefacItems {
	   private Long nroCuenta;              
	   private Long idServicio;             
	   private Long nroServicio;            
	   private Long idAjuste;               
	   private String nroAjuste;            
	   private String codigoItemAjuste;     
	   private String descItemAjuste;       
	   private Long valorAjusteItem;        
	   private Long valorItemAntesAjuste;   
	   private Long valorItemDespuesAjuste; 
	   private Date fechaAjusteItem;        
	   private Long consumoKwhAjuste;       
	   private Long consumoKwhAntesAjuste;  
	   private Long consumoKwhDespuesAjuste;
	   private String periodoDeFact;        
	   private Long nroDocumento;
	   private Long nroOrden;
	   
	   
	public Long getNroOrden() {
		return nroOrden;
	}
	public void setNroOrden(Long nroOrden) {
		this.nroOrden = nroOrden;
	}
	/**
	 * @return the codigoItemAjuste
	 */
	public String getCodigoItemAjuste() {
		return codigoItemAjuste;
	}
	/**
	 * @param codigoItemAjuste the codigoItemAjuste to set
	 */
	public void setCodigoItemAjuste(String codigoItemAjuste) {
		this.codigoItemAjuste = codigoItemAjuste;
	}
	/**
	 * @return the consumoKwhAjuste
	 */
	public Long getConsumoKwhAjuste() {
		return consumoKwhAjuste;
	}
	/**
	 * @param consumoKwhAjuste the consumoKwhAjuste to set
	 */
	public void setConsumoKwhAjuste(Long consumoKwhAjuste) {
		this.consumoKwhAjuste = consumoKwhAjuste;
	}
	/**
	 * @return the consumoKwhAntesAjuste
	 */
	public Long getConsumoKwhAntesAjuste() {
		return consumoKwhAntesAjuste;
	}
	/**
	 * @param consumoKwhAntesAjuste the consumoKwhAntesAjuste to set
	 */
	public void setConsumoKwhAntesAjuste(Long consumoKwhAntesAjuste) {
		this.consumoKwhAntesAjuste = consumoKwhAntesAjuste;
	}
	/**
	 * @return the consumoKwhDespuesAjuste
	 */
	public Long getConsumoKwhDespuesAjuste() {
		return consumoKwhDespuesAjuste;
	}
	/**
	 * @param consumoKwhDespuesAjuste the consumoKwhDespuesAjuste to set
	 */
	public void setConsumoKwhDespuesAjuste(Long consumoKwhDespuesAjuste) {
		this.consumoKwhDespuesAjuste = consumoKwhDespuesAjuste;
	}
	/**
	 * @return the descItemAjuste
	 */
	public String getDescItemAjuste() {
		return descItemAjuste;
	}
	/**
	 * @param descItemAjuste the descItemAjuste to set
	 */
	public void setDescItemAjuste(String descItemAjuste) {
		this.descItemAjuste = descItemAjuste;
	}
	/**
	 * @return the fechaAjusteItem
	 */
	public Date getFechaAjusteItem() {
		return fechaAjusteItem;
	}
	/**
	 * @param fechaAjusteItem the fechaAjusteItem to set
	 */
	public void setFechaAjusteItem(Date fechaAjusteItem) {
		this.fechaAjusteItem = fechaAjusteItem;
	}
	/**
	 * @return the idAjuste
	 */
	public Long getIdAjuste() {
		return idAjuste;
	}
	/**
	 * @param idAjuste the idAjuste to set
	 */
	public void setIdAjuste(Long idAjuste) {
		this.idAjuste = idAjuste;
	}
	/**
	 * @return the idServicio
	 */
	public Long getIdServicio() {
		return idServicio;
	}
	/**
	 * @param idServicio the idServicio to set
	 */
	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}
	/**
	 * @return the nroAjuste
	 */
	public String getNroAjuste() {
		return nroAjuste;
	}
	/**
	 * @param nroAjuste the nroAjuste to set
	 */
	public void setNroAjuste(String nroAjuste) {
		this.nroAjuste = nroAjuste;
	}
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/**
	 * @return the nroDocumento
	 */
	public Long getNroDocumento() {
		return nroDocumento;
	}
	/**
	 * @param nroDocumento the nroDocumento to set
	 */
	public void setNroDocumento(Long nroDocumento) {
		this.nroDocumento = nroDocumento;
	}
	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	/**
	 * @return the periodoDeFact
	 */
	public String getPeriodoDeFact() {
		return periodoDeFact;
	}
	/**
	 * @param periodoDeFact the periodoDeFact to set
	 */
	public void setPeriodoDeFact(String periodoDeFact) {
		this.periodoDeFact = periodoDeFact;
	}
	/**
	 * @return the valorAjusteItem
	 */
	public Long getValorAjusteItem() {
		return valorAjusteItem;
	}
	/**
	 * @param valorAjusteItem the valorAjusteItem to set
	 */
	public void setValorAjusteItem(Long valorAjusteItem) {
		this.valorAjusteItem = valorAjusteItem;
	}
	/**
	 * @return the valorItemAntesAjuste
	 */
	public Long getValorItemAntesAjuste() {
		return valorItemAntesAjuste;
	}
	/**
	 * @param valorItemAntesAjuste the valorItemAntesAjuste to set
	 */
	public void setValorItemAntesAjuste(Long valorItemAntesAjuste) {
		this.valorItemAntesAjuste = valorItemAntesAjuste;
	}
	/**
	 * @return the valorItemDespuesAjuste
	 */
	public Long getValorItemDespuesAjuste() {
		return valorItemDespuesAjuste;
	}
	/**
	 * @param valorItemDespuesAjuste the valorItemDespuesAjuste to set
	 */
	public void setValorItemDespuesAjuste(Long valorItemDespuesAjuste) {
		this.valorItemDespuesAjuste = valorItemDespuesAjuste;
	}           
}