/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaEstadosServicio;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaEstadosServicioImpl extends SynergiaBusinessObjectImpl implements ConsultaEstadosServicio {
	   private String estado;     
	   private String motivo;
	   private Long usuarioEjecutor;       
	   private Date fechaCambioEstado;     
	   private Long nroServicio;
	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * @return the fechaCambioEstado
	 */
	public Date getFechaCambioEstado() {
		return fechaCambioEstado;
	}
	/**
	 * @param fechaCambioEstado the fechaCambioEstado to set
	 */
	public void setFechaCambioEstado(Date fechaCambioEstado) {
		this.fechaCambioEstado = fechaCambioEstado;
	}
	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	/**
	 * @return the usuarioEjecutor
	 */
	public Long getUsuarioEjecutor() {
		return usuarioEjecutor;
	}
	/**
	 * @param usuarioEjecutor the usuarioEjecutor to set
	 */
	public void setUsuarioEjecutor(Long usuarioEjecutor) {
		this.usuarioEjecutor = usuarioEjecutor;
	}
	/**
	 * @return the motivo
	 */
	public String getMotivo() {
		return motivo;
	}
	/**
	 * @param motivo the motivo to set
	 */
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}           
}
