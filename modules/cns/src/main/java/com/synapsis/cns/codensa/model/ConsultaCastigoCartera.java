package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.1 $ $Date: 2007/02/28 21:47:47 $
 * 
 */
public interface ConsultaCastigoCartera extends SynergiaBusinessObject {

	/**
	 * @return the fechaGeneracionDeuda
	 */
	public Date getFechaGeneracionDeuda();

	/**
	 * @param fechaGeneracionDeuda
	 *            the fechaGeneracionDeuda to set
	 */
	public void setFechaGeneracionDeuda(Date fechaGeneracionDeuda);

	/**
	 * @return the usuarioNombreIngresoDeuda
	 */
	public String getUsuarioNombreIngresoDeuda();

	/**
	 * @param usuarioNombreIngresoDeuda
	 *            the usuarioNombreIngresoDeuda to set
	 */
	public void setUsuarioNombreIngresoDeuda(String usuarioNombreIngresoDeuda);

	/**
	 * @return the valorTotalCastigado
	 */
	public Double getValorTotalCastigado();

	/**
	 * @param valorTotalCastigado
	 *            the valorTotalCastigado to set
	 */
	public void setValorTotalCastigado(Double valorTotalCastigado);


}