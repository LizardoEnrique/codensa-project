/**
 * $Id: HistoricosLecturasBB.java,v 1.6 2007/02/28 14:14:09 ar26557682 Exp $
 */
package com.synapsis.cns.codensa.web;

import com.suivant.arquitectura.presentation.model.ListableBB;
import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 *
 * @author dBraccio
 */
public class HistoricosLecturasBB extends ListableBB {

	/**
	 * objeto que hace referencia a datos de las lecturas a medidores 
	 */
	SynergiaBusinessObject businessObject1;


	public SynergiaBusinessObject getBusinessObject1() {

		return businessObject1;
	}
	
	public void setBusinessObject1(SynergiaBusinessObject businessObject1) {
		this.businessObject1 = businessObject1;
	}
}
