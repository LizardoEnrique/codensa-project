package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;
/**
 * 
 * @author Emiliano Arango (ar30557486)
 *
 */
public interface ChequeDevuelto extends SynergiaBusinessObject {

	/**
	 * @return the causa
	 */
	public String getCausa();

	/**
	 * @return the devolucion
	 */
	public String getDevolucion();

	/**
	 * @return the fechaDevolucion
	 */
	public Date getFechaDevolucion();

	/**
	 * @return the girador
	 */
	public String getGirador();

	/**
	 * @param causa the causa to set
	 */
	public void setCausa(String causa);

	/**
	 * @param devolucion the devolucion to set
	 */
	public void setDevolucion(String devolucion);

	/**
	 * @param fechaDevolucion the fechaDevolucion to set
	 */
	public void setFechaDevolucion(Date fechaDevolucion);

	/**
	 * @param girador the girador to set
	 */
	public void setGirador(String girador);

	public Long getIdPago();
	
	public void setIdPago(Long idPago);	
	
	/**
	 * @return the nroCheque
	 */
	public String getNroCheque();
	public void setNroCheque(String nroCheque);
	public Double getValor();
	public void setValor(Double valor);	

}