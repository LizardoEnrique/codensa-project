package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.ObservacionAtencionAic;

/**
 * @author m3.MarioRoss - 14/11/2006
 */
public class ObservacionAtencionAicImpl extends SynergiaBusinessObjectImpl 
		implements ObservacionAtencionAic {

	private String observacion;
	private Long idAtencion;
	private Date fechaObservacion;
	
	public Long getIdAtencion() {
		return this.idAtencion;
	}
	public void setIdAtencion(Long idAtencion) {
		this.idAtencion = idAtencion;
	}
	public String getObservacion() {
		return this.observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	/**
	 * @return the fechaObservacion
	 */
	public Date getFechaObservacion() {
		return fechaObservacion;
	}
	/**
	 * @param fechaObservacion the fechaObservacion to set
	 */
	public void setFechaObservacion(Date fechaObservacion) {
		this.fechaObservacion = fechaObservacion;
	}
}
