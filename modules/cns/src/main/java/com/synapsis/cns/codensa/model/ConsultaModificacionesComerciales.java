package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.suivant.arquitectura.core.model.BusinessObject;
import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaModificacionesComerciales extends SynergiaBusinessObject{

	public Long getNumeroModificacion();
	public void setNumeroModificacion(Long numeroModificacion);
	
	public Long getNumeroServicioElectrico();
	public void setNumeroServicioElectrico(Long numeroServicioElectrico);
	
	public Long getNumeroCuenta();
	public void setNumeroCuenta(Long numeroCuenta);
	
	public Date getFechaModificacion();
	public void setFechaModificacion(Date fechaModificacion);

	public String getDatoAnterior();
	public void setDatoAnterior(String datoAnterior);

	public String getDatoNuevo();
	public void setDatoNuevo(String datoNuevo);
	
	public String getUsuarioModificador();
	public void setUsuarioModificador(String usuarioModificador);

	public String getDireccionIP();
	public void setDireccionIP(String direccionIP);
	
	public String getTipoModificacion();
	public void setTipoModificacion(String tipoModificacion);
	
	public String getObservaciones();
	public void setObservaciones(String observaciones);
	
	
}
