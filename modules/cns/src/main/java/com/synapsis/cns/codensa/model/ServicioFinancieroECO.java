package com.synapsis.cns.codensa.model;

import java.util.Set;


/**
 * Interfaz del Serv Financ Encargo de Cobranza, para el reporte de duplicado de anexo de factura (Servicios Financieros)
 *    
 * @author jhack
 */
public interface ServicioFinancieroECO extends ServicioFinanciero{
	public Set getItemsECO();
	public void setItemsECO(Set itemsECO);
}
