package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaHistoricoOtrosNegociosServFinancCodensaHogar;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * Representa la informaci�n de Servicios Financieros Codensa Hogar que se muestra de la Consulta Hist�rico
 * Otros Negocios. Tiene los campos de ConsultaServFinancCodensaHogarImpl m�s que se quitaron de
 * ConsultaServFinancCodensaHogarImpl por el error de filas repetidas en las grillas<br />
 * <i> 16/07: Saco la herencia de ConsultaServFinancCodensaHogarImpl porque trajo problemas con hibernate y
 * levantaron un Absoluto (ANS-136) que ya ya yaaaaa tengo que cerrar :P</i>
 * 
 * @author egrande
 * 
 */
public class ConsultaHistoricoOtrosNegociosServFinancCodensaHogarImpl extends SynergiaBusinessObjectImpl implements
		ConsultaHistoricoOtrosNegociosServFinancCodensaHogar {

	private Date fechaActivProd;

	private Date fechaDesactProd;

	private Long nroCuenta;

	private Long nroServicio;

	private Double valorOrigSrv;

	private Long nroCuotasOri;

	private Double tasaInteres;

	private Long nroCuotasFac;

	private Long nroCuotasFaltantes;

	private Double valorCuota;

	private Double saldoCapital;

	private Double saldoIntereses;

	private Double saldoIntMora;

	private Long periodAtrasado;

	private Double deudaActual;

	private String estado;

	private Long idLineaNegocio;
	private Long idTipoProducto;
	private String tipoSrvFin;
	private Long nroSrvFin;
	private String nombreTit;
	private String tipoIdent;
	private String tipoIdentCode;
	private String nroIdentTitular;
	private String nroIdentSocio;
	private Long nroIdentServicio;
	private String lineaNegocio;
	private String plan;
	private String producto;

	private String solicitanteCred;
	private Date fechaActivSrv;
	private Double saldoSrv;
	private Long idTipoServicio;
	private Long idPlan;

	private String motivoFinalizacion;

	/**
	 * @return the fechaActivProd
	 */
	public Date getFechaActivProd() {
		return fechaActivProd;
	}

	/**
	 * @param fechaActivProd the fechaActivProd to set
	 */
	public void setFechaActivProd(Date fechaActivProd) {
		this.fechaActivProd = fechaActivProd;
	}

	/**
	 * @return the fechaDesactProd
	 */
	public Date getFechaDesactProd() {
		return fechaDesactProd;
	}

	/**
	 * @param fechaDesactProd the fechaDesactProd to set
	 */
	public void setFechaDesactProd(Date fechaDesactProd) {
		this.fechaDesactProd = fechaDesactProd;
	}

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio() {
		return nroServicio;
	}

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}

	/**
	 * @return the valorOrigSrv
	 */
	public Double getValorOrigSrv() {
		return valorOrigSrv;
	}

	/**
	 * @param valorOrigSrv the valorOrigSrv to set
	 */
	public void setValorOrigSrv(Double valorOrigSrv) {
		this.valorOrigSrv = valorOrigSrv;
	}

	/**
	 * @return the nroCuotasFac
	 */
	public Long getNroCuotasFac() {
		return nroCuotasFac;
	}

	/**
	 * @param nroCuotasFac the nroCuotasFac to set
	 */
	public void setNroCuotasFac(Long nroCuotasFac) {
		this.nroCuotasFac = nroCuotasFac;
	}

	/**
	 * @return the nroCuotasFaltantes
	 */
	public Long getNroCuotasFaltantes() {
		return nroCuotasFaltantes;
	}

	/**
	 * @param nroCuotasFaltantes the nroCuotasFaltantes to set
	 */
	public void setNroCuotasFaltantes(Long nroCuotasFaltantes) {
		this.nroCuotasFaltantes = nroCuotasFaltantes;
	}

	/**
	 * @return the nroCuotasOri
	 */
	public Long getNroCuotasOri() {
		return nroCuotasOri;
	}

	/**
	 * @param nroCuotasOri the nroCuotasOri to set
	 */
	public void setNroCuotasOri(Long nroCuotasOri) {
		this.nroCuotasOri = nroCuotasOri;
	}

	/**
	 * @return the periodAtrasado
	 */
	public Long getPeriodAtrasado() {
		return periodAtrasado;
	}

	/**
	 * @param periodAtrasado the periodAtrasado to set
	 */
	public void setPeriodAtrasado(Long periodAtrasado) {
		this.periodAtrasado = periodAtrasado;
	}

	/**
	 * @return the saldoCapital
	 */
	public Double getSaldoCapital() {
		return saldoCapital;
	}

	/**
	 * @param saldoCapital the saldoCapital to set
	 */
	public void setSaldoCapital(Double saldoCapital) {
		this.saldoCapital = saldoCapital;
	}

	/**
	 * @return the saldoIntereses
	 */
	public Double getSaldoIntereses() {
		return saldoIntereses;
	}

	/**
	 * @param saldoIntereses the saldoIntereses to set
	 */
	public void setSaldoIntereses(Double saldoIntereses) {
		this.saldoIntereses = saldoIntereses;
	}

	/**
	 * @return the saldoIntMora
	 */
	public Double getSaldoIntMora() {
		return saldoIntMora;
	}

	/**
	 * @param saldoIntMora the saldoIntMora to set
	 */
	public void setSaldoIntMora(Double saldoIntMora) {
		this.saldoIntMora = saldoIntMora;
	}

	/**
	 * @return the tasaInteres
	 */
	public Double getTasaInteres() {
		return tasaInteres;
	}

	/**
	 * @param tasaInteres the tasaInteres to set
	 */
	public void setTasaInteres(Double tasaInteres) {
		this.tasaInteres = tasaInteres;
	}

	/**
	 * @return the valorCuota
	 */
	public Double getValorCuota() {
		return valorCuota;
	}

	/**
	 * @param valorCuota the valorCuota to set
	 */
	public void setValorCuota(Double valorCuota) {
		this.valorCuota = valorCuota;
	}

	/**
	 * @return the deudaActual
	 */
	public Double getDeudaActual() {
		return deudaActual;
	}

	/**
	 * @param deudaActual the deudaActual to set
	 */
	public void setDeudaActual(Double deudaActual) {
		this.deudaActual = deudaActual;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaActivSrv() {
		return fechaActivSrv;
	}

	public void setFechaActivSrv(Date fechaActivSrv) {
		this.fechaActivSrv = fechaActivSrv;
	}

	public Long getIdLineaNegocio() {
		return idLineaNegocio;
	}

	public void setIdLineaNegocio(Long idLineaNegocio) {
		this.idLineaNegocio = idLineaNegocio;
	}

	public Long getIdPlan() {
		return idPlan;
	}

	public void setIdPlan(Long idPlan) {
		this.idPlan = idPlan;
	}

	public Long getIdTipoProducto() {
		return idTipoProducto;
	}

	public void setIdTipoProducto(Long idTipoProducto) {
		this.idTipoProducto = idTipoProducto;
	}

	public Long getIdTipoServicio() {
		return idTipoServicio;
	}

	public void setIdTipoServicio(Long idTipoServicio) {
		this.idTipoServicio = idTipoServicio;
	}

	public String getLineaNegocio() {
		return lineaNegocio;
	}

	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}

	public String getMotivoFinalizacion() {
		return motivoFinalizacion;
	}

	public void setMotivoFinalizacion(String motivoFinalizacion) {
		this.motivoFinalizacion = motivoFinalizacion;
	}

	public String getNombreTit() {
		return nombreTit;
	}

	public void setNombreTit(String nombreTit) {
		this.nombreTit = nombreTit;
	}

	public Long getNroIdentServicio() {
		return nroIdentServicio;
	}

	public void setNroIdentServicio(Long nroIdentServicio) {
		this.nroIdentServicio = nroIdentServicio;
	}

	public String getNroIdentSocio() {
		return nroIdentSocio;
	}

	public void setNroIdentSocio(String nroIdentSocio) {
		this.nroIdentSocio = nroIdentSocio;
	}

	public String getNroIdentTitular() {
		return nroIdentTitular;
	}

	public void setNroIdentTitular(String nroIdentTitular) {
		this.nroIdentTitular = nroIdentTitular;
	}

	public Long getNroSrvFin() {
		return nroSrvFin;
	}

	public void setNroSrvFin(Long nroSrvFin) {
		this.nroSrvFin = nroSrvFin;
	}

	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public Double getSaldoSrv() {
		return saldoSrv;
	}

	public void setSaldoSrv(Double saldoSrv) {
		this.saldoSrv = saldoSrv;
	}

	public String getSolicitanteCred() {
		return solicitanteCred;
	}

	public void setSolicitanteCred(String solicitanteCred) {
		this.solicitanteCred = solicitanteCred;
	}

	public String getTipoIdent() {
		return tipoIdent;
	}

	public void setTipoIdent(String tipoIdent) {
		this.tipoIdent = tipoIdent;
	}

	public String getTipoIdentCode() {
		return tipoIdentCode;
	}

	public void setTipoIdentCode(String tipoIdentCode) {
		this.tipoIdentCode = tipoIdentCode;
	}

	public String getTipoSrvFin() {
		return tipoSrvFin;
	}

	public void setTipoSrvFin(String tipoSrvFin) {
		this.tipoSrvFin = tipoSrvFin;
	}
}
