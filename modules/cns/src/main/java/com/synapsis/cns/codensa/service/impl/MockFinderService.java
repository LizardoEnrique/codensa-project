/**
 * 
 */
package com.synapsis.cns.codensa.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.suivant.arquitectura.core.dao.DAO;
import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.integration.Module;
import com.suivant.arquitectura.core.model.BusinessObject;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.service.FinderService;
import com.suivant.arquitectura.core.service.impl.ServiceImpl;

/**
 * @author dBraccio
 * TODO:[dbraccio] esta una clase solamente para test hasta que las tablas de BD
 * esten realizadas
 * 
 * 
 * 17/11/2006
 * 
 * Esta clase fue hecha solo para fines especificos de testing. No debe ser
 * utilizada para nada.
 * TODO:
 * Esta clase ser� sacada para produccion.
 */
public class MockFinderService extends ServiceImpl implements FinderService {

	//lista de objetos si queres probar un tabla de datos
	List listObjectsMock;

	//este es un businessObject si queremos probar una grilla
	BusinessObject businessObject;

	//este dao es inyectado para obtener el nombre de la clase
	private DAO dao;

	public List findByCriteria(QueryFilter filter) {
		((QueryFilter)filter).setAmountResults(listObjectsMock.size());
		List results = new ArrayList();
		// afalduto: fixes 4 new queryFilter interface.
		int maxresults = 10;
		for (int i = filter.getPositionStart().intValue(); i < listObjectsMock
				.size()
				&& i < (filter.getPositionStart().intValue() + maxresults); ++i) {
			results.add(listObjectsMock.get(i));
		}
		// fix the for Scrollable data
		if (results.size() > 0) {
			filter.setLastResult(filter.getPositionStart().intValue()
					+ results.size() - 1);
		}
		return results;
	}

	public BusinessObject findByCriteriaUnique(QueryFilter filter)
			throws ObjectNotFoundException {
		// TODO Auto-generated method stub
		return businessObject;
	}

	public DAO getDao() {
		return dao;
	}

	public void setDao(DAO dao) {
		this.dao = dao;
	}

	public List getListObjectsMock() {
		return listObjectsMock;
	}

	public void setListObjectsMock(List listObjectsMock) {
		this.listObjectsMock = listObjectsMock;
	}

	public BusinessObject getBusinessObject() {
		return businessObject;
	}

	public void setBusinessObject(BusinessObject businessObject) {
		this.businessObject = businessObject;
	}


	public int countAll(QueryFilter filter) {
		return listObjectsMock.size();
	}

	public String getBusinessClassName() {
		return getDao().getBusinessClassName();
	}

	public String getName() {
		return null;
	}
//	only for backcompatibility
	public void setModule(Module module) {
	}


}
