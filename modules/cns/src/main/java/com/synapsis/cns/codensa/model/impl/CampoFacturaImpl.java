package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.CampoFactura;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author jhack
 */
public class CampoFacturaImpl extends SynergiaBusinessObjectImpl 
	implements CampoFactura {

	private String 	tipoDocumento;
	private String 	tipoInfo;
	private String 	tipoCampo;
	private String 	tipoDato;
	private String 	nombre;
	private String 	descripcion;
	private Long 	posInicio;
	private Long 	longitud;
	private Long 	posFin;
	private Long 	version;
	
	
//	CampoFacturaImpl(String tipo,String nombre, String descripcion,Long posInicio, Long longitud, Long 	posFin){
//		this.setTipo(tipo);
//		this.setNombre(nombre);
//		this.setDescripcion(descripcion);
//		this.setPosInicio(posInicio);
//		this.setPosFin(posFin);
//		this.setLongitud(longitud);
//	}
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Long getPosInicio() {
		return posInicio;
	}
	public void setPosInicio(Long posInicio) {
		this.posInicio = posInicio;
	}
	public Long getLongitud() {
		return longitud;
	}
	public void setLongitud(Long longitud) {
		this.longitud = longitud;
	}
	public Long getPosFin() {
		return posFin;
	}
	public void setPosFin(Long posFin) {
		this.posFin = posFin;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	/**
	 * @return the tipoDocumento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getTipoCampo() {
		return tipoCampo;
	}
	public void setTipoCampo(String tipoCampo) {
		this.tipoCampo = tipoCampo;
	}
	public String getTipoDato() {
		return tipoDato;
	}
	public void setTipoDato(String tipoDato) {
		this.tipoDato = tipoDato;
	}
	public String getTipoInfo() {
		return tipoInfo;
	}
	public void setTipoInfo(String tipoInfo) {
		this.tipoInfo = tipoInfo;
	}

}
