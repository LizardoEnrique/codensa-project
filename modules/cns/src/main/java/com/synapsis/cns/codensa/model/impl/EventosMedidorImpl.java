package com.synapsis.cns.codensa.model.impl;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.EventosMedidor;

public class EventosMedidorImpl extends SynergiaBusinessObjectImpl 
	implements EventosMedidor {
    // Fields    
	
    private Long nroCuenta;
	private String marcaMedidor;
	private String modeloMedidor;
	private String numeroMedidor;
    private String anioFabricacion;
    private String tipoMedida;
    private String tipoMedidor;
    private String nivelTension;
    private String condicionInstalacion;
    private String factorLiquidacion;
    private String estadoMedidor;

    private Long idMedidor;

	public String getAnioFabricacion() {
		return anioFabricacion;
	}

	public void setAnioFabricacion(String anioFabricacion) {
		this.anioFabricacion = anioFabricacion;
	}

	public String getCondicionInstalacion() {
		return condicionInstalacion;
	}

	public void setCondicionInstalacion(String condicionInstalacion) {
		this.condicionInstalacion = condicionInstalacion;
	}

	public String getEstadoMedidor() {
		return estadoMedidor;
	}

	public void setEstadoMedidor(String estadoMedidor) {
		this.estadoMedidor = estadoMedidor;
	}

	public String getFactorLiquidacion() {
		return factorLiquidacion;
	}

	public void setFactorLiquidacion(String factorLiquidacion) {
		this.factorLiquidacion = factorLiquidacion;
	}

	public Long getIdMedidor() {
		return idMedidor;
	}

	public void setIdMedidor(Long idMedidor) {
		this.idMedidor = idMedidor;
	}

	public String getMarcaMedidor() {
		return marcaMedidor;
	}

	public void setMarcaMedidor(String marcaMedidor) {
		this.marcaMedidor = marcaMedidor;
	}

	public String getModeloMedidor() {
		return modeloMedidor;
	}

	public void setModeloMedidor(String modeloMedidor) {
		this.modeloMedidor = modeloMedidor;
	}

	public String getNivelTension() {
		return nivelTension;
	}

	public void setNivelTension(String nivelTension) {
		this.nivelTension = nivelTension;
	}

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getNumeroMedidor() {
		return numeroMedidor;
	}

	public void setNumeroMedidor(String numeroMedidor) {
		this.numeroMedidor = numeroMedidor;
	}

	public String getTipoMedida() {
		return tipoMedida;
	}

	public void setTipoMedida(String tipoMedida) {
		this.tipoMedida = tipoMedida;
	}

	public String getTipoMedidor() {
		return tipoMedidor;
	}

	public void setTipoMedidor(String tipoMedidor) {
		this.tipoMedidor = tipoMedidor;
	}
}
