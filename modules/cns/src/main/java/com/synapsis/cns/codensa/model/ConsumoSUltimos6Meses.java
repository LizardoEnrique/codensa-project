package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * Impresión duplicado factura - Consumo Ultimos 6 Meses
 * 
 * @author adambrosio
 */
public interface ConsumoSUltimos6Meses extends Comparable, SynergiaBusinessObject {
	
	public Double getCons_KWH();
	public void setCons_KWH(Double cons_KWH);
	public String getMes();
	public void setMes(String mes);
	public Long getNroFactura();
	public void setNroFactura(Long nroFactura);
}
