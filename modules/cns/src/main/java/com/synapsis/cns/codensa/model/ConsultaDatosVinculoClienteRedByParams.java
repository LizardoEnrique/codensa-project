package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaDatosVinculoClienteRedByParams extends SynergiaBusinessObject {

	public String getDireccion();
	public void setDireccion(String direccion);
	public String getEstado();
	public void setEstado(String estado);
	public String getFecAsociacion();
	public void setFecAsociacion(String fecAsociacion);
	public String getMercado();
	public void setMercado(String mercado);
	public String getMunicipio();
	public void setMunicipio(String municipio);
	public String getRutaFacturacion();
	public void setRutaFacturacion(String rutaFacturacion);
	public Long getServicio();
	public void setServicio(Long servicio); 
	public String getTarifa();
	public void setTarifa(String tarifa);
	public String getTipoServicio();
	public void setTipoServicio(String tipoServicio);
	public Date getFechaProceso();
	public void setFechaProceso(Date fechaProceso);
}
