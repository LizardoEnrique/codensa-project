package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface TipoMedidaMedidor extends SynergiaBusinessObject {

	

	public String getTipoMedida();

	public void setTipoMedida(String tipoMedida);

	public Long getIdComponente();

	public void setIdComponente(Long idComponente);

}