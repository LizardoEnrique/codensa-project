package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.DatosCreacionConvenio;

public class DatosCreacionConvenioImpl extends SynergiaBusinessObjectImpl 
	implements DatosCreacionConvenio {
	
	private Long idConvenio;
	private Long nroConvenio;
	private String nombreSolicitante;
	private String usuarioCreador;
	private String usuarioAprobador;
	private Date fechaAprobacion;
	private String usuarioCaduco;
	private Date fechaCaducacion;
	private Double valorCondonado;
	private String calidadSolicitante;
	private String tipoIdentificacionSolicitante;
	private String numeroIdentificacionSolicitante;	
	private String nombreCreador;
	private String nombreAprobador;
	private String numeroAprobacion;
	private String nombreCaduco;
	private String motivoEstadoConvenio;
	private Long nroCuenta;
	
	public String getCalidadSolicitante() {
		return calidadSolicitante;
	}
	public void setCalidadSolicitante(String calidadSolicitante) {
		this.calidadSolicitante = calidadSolicitante;
	}
	public Date getFechaAprobacion() {
		return fechaAprobacion;
	}
	public void setFechaAprobacion(Date fechaAprobacion) {
		this.fechaAprobacion = fechaAprobacion;
	}
	public Date getFechaCaducacion() {
		return fechaCaducacion;
	}
	public void setFechaCaducacion(Date fechaCaducacion) {
		this.fechaCaducacion = fechaCaducacion;
	}
	public Long getIdConvenio() {
		return idConvenio;
	}
	public void setIdConvenio(Long idConvenio) {
		this.idConvenio = idConvenio;
	}
	public String getMotivoEstadoConvenio() {
		return motivoEstadoConvenio;
	}
	public void setMotivoEstadoConvenio(String motivoEstadoConvenio) {
		this.motivoEstadoConvenio = motivoEstadoConvenio;
	}
	public String getNombreAprobador() {
		return nombreAprobador;
	}
	public void setNombreAprobador(String nombreAprobador) {
		this.nombreAprobador = nombreAprobador;
	}
	public String getNombreCaduco() {
		return nombreCaduco;
	}
	public void setNombreCaduco(String nombreCaduco) {
		this.nombreCaduco = nombreCaduco;
	}
	public String getNombreCreador() {
		return nombreCreador;
	}
	public void setNombreCreador(String nombreCreador) {
		this.nombreCreador = nombreCreador;
	}
	public String getNombreSolicitante() {
		return nombreSolicitante;
	}
	public void setNombreSolicitante(String nombreSolicitante) {
		this.nombreSolicitante = nombreSolicitante;
	}
	public Long getNroConvenio() {
		return nroConvenio;
	}
	public void setNroConvenio(Long nroConvenio) {
		this.nroConvenio = nroConvenio;
	}
	public String getNumeroAprobacion() {
		return numeroAprobacion;
	}
	public void setNumeroAprobacion(String numeroAprobacion) {
		this.numeroAprobacion = numeroAprobacion;
	}
	public String getUsuarioAprobador() {
		return usuarioAprobador;
	}
	public void setUsuarioAprobador(String usuarioAprobador) {
		this.usuarioAprobador = usuarioAprobador;
	}
	public String getUsuarioCaduco() {
		return usuarioCaduco;
	}
	public void setUsuarioCaduco(String usuarioCaduco) {
		this.usuarioCaduco = usuarioCaduco;
	}
	public String getUsuarioCreador() {
		return usuarioCreador;
	}
	public void setUsuarioCreador(String usuarioCreador) {
		this.usuarioCreador = usuarioCreador;
	}
	public Double getValorCondonado() {
		return valorCondonado;
	}
	public void setValorCondonado(Double valorCondonado) {
		this.valorCondonado = valorCondonado;
	}
	public String getNumeroIdentificacionSolicitante() {
		return numeroIdentificacionSolicitante;
	}
	public void setNumeroIdentificacionSolicitante(
			String numeroIdentificacionSolicitante) {
		this.numeroIdentificacionSolicitante = numeroIdentificacionSolicitante;
	}
	public String getTipoIdentificacionSolicitante() {
		return tipoIdentificacionSolicitante;
	}
	public void setTipoIdentificacionSolicitante(
			String tipoIdentificacionSolicitante) {
		this.tipoIdentificacionSolicitante = tipoIdentificacionSolicitante;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	
	
}