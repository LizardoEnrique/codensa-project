package com.synapsis.cns.codensa.model.buscaCuenta;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface BuscaCuentaTipoNroDocumento extends SynergiaBusinessObject{

	/**
	 * @return the nroDocumento
	 */
	public Long getNroDocumento();

	/**
	 * @return the tipoDocumento
	 */
	public String getTipoDocumento();

	/**
	 * @param nroDocumento the nroDocumento to set
	 */
	public void setNroDocumento(Long nroDocumento);

	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public void setTipoDocumento(String tipoDocumento);

}