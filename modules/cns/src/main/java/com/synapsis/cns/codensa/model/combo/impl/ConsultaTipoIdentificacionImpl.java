/**
 * 
 */
package com.synapsis.cns.codensa.model.combo.impl;

import com.synapsis.cns.codensa.model.combo.ConsultaTipoIdentificacion;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaTipoIdentificacionImpl extends SynergiaBusinessObjectImpl implements ConsultaTipoIdentificacion {
	   private String codTipDocPersona;    
	   private String desTipDocPersona;
	/**
	 * @return the codTipDocPersona
	 */
	public String getCodTipDocPersona() {
		return codTipDocPersona;
	}
	/**
	 * @param codTipDocPersona the codTipDocPersona to set
	 */
	public void setCodTipDocPersona(String codTipDocPersona) {
		this.codTipDocPersona = codTipDocPersona;
	}
	/**
	 * @return the desTipDocPersona
	 */
	public String getDesTipDocPersona() {
		return desTipDocPersona;
	}
	/**
	 * @param desTipDocPersona the desTipDocPersona to set
	 */
	public void setDesTipDocPersona(String desTipDocPersona) {
		this.desTipDocPersona = desTipDocPersona;
	}    
}
