package com.synapsis.cns.codensa.model.impl;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.ConsultaAnomalia;

public class ConsultaAnomaliaImpl extends SynergiaBusinessObjectImpl implements ConsultaAnomalia {
	
	private String codigo;
	private String descripcion;
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaAnomalia#getCodigo()
	 */
	public String getCodigo() {
		return codigo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaAnomalia#getDescripcion()
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaAnomalia#setCodigo(java.lang.Long)
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaAnomalia#setDescripcion(java.lang.String)
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
	

}
