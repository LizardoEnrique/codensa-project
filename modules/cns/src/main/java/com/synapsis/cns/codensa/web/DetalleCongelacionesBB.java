/**
 * $Id: DetalleCongelacionesBB.java,v 1.10 2008/07/24 21:03:50 ar26557682 Exp $
 */
package com.synapsis.cns.codensa.web;

import java.util.List;

import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.model.BusinessObject;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.service.FinderService;
import com.suivant.arquitectura.presentation.model.ListableBB;
import com.synapsis.commons.faces.messages.ErrorsManager;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;
import com.synapsis.synergia.core.model.SynergiaBusinessObject;


public class DetalleCongelacionesBB extends ListableBB {

	/** objeto que hace referencia a datos generales */
	SynergiaBusinessObject businessObject2;

	String serviceNameBO;

	QueryFilter queryFilterBO;

	private SynergiaBusinessObject businessObject1;
	private SynergiaBusinessObject businessObject3;

	public SynergiaBusinessObject getBusinessObject1()
			throws ObjectNotFoundException {
		return businessObject1;
	}

	public void setBusinessObject1(SynergiaBusinessObject businessObject1) {
		this.businessObject1 = businessObject1;
	}

	public void buscarCongelacionAction() {
		try {
			searchBusiness();
		} catch (ObjectNotFoundException e) {
			ErrorsManager.addInfoError(e.getMessage());
		}
	}

	private void searchBusiness() throws ObjectNotFoundException {
		/* por el momento */
		try {
			List l = getService(
							getServiceNameBO()).findByCriteria(
							this.getQueryFilterBO());
			this.setBusinessObject2((SynergiaBusinessObject)l.get(0) );
		} catch (Exception e) {
		}
		FinderService finderService = (FinderService) SynergiaApplicationContext
				.findService("congDetalleSaldosServiceFinder");
		this.setBusinessObject3((SynergiaBusinessObject) finderService
				.findByCriteriaUnique(this.getQueryFilterBO()));
		this.getBusinessObject3();
	}

	// me devuleve un servicio pidiendoselo al SynergiaApplicacontext
	private FinderService getService(String service) {
		return (FinderService) SynergiaApplicationContext.findService(service);
	}

	public void setBusinessObject2(SynergiaBusinessObject businessObject2) {
		this.businessObject2 = businessObject2;
	}

	public BusinessObject getBusinessObject2() {
		return businessObject2;
	}

	public QueryFilter getQueryFilterBO() {
		return queryFilterBO;
	}

	public void setQueryFilterBO(QueryFilter queryFilterBO) {
		this.queryFilterBO = queryFilterBO;
	}

	public String getServiceNameBO() {
		return serviceNameBO;
	}

	public void setServiceNameBO(String serviceNameBO) {
		this.serviceNameBO = serviceNameBO;
	}

	public void setBusinessObject3(SynergiaBusinessObject businessObject3) {
		this.businessObject3 = businessObject3;
	}

	public BusinessObject getBusinessObject3() {
		return businessObject3;
	}
}
