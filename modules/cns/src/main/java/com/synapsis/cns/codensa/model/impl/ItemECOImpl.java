package com.synapsis.cns.codensa.model.impl;

import java.math.BigDecimal;
import java.util.Date;

import com.synapsis.cns.codensa.model.ItemECO;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * Item de Producto de Encargo de Cobranza para mostrar en el Reporte de Impresion de Anexo de Factura (CNS003)
 * Por el momento agrupa todas las propiedades de todos los tipos de encargo conocidos (Seguros, Suscripciones y Clasificados)
 * 
 * @author jhack
 */
public class ItemECOImpl extends SynergiaBusinessObjectImpl implements ItemECO {
	private static final long serialVersionUID = 1L;
	
	private String 		nroEco;
	private String 		idEco;
	private String 		titular;
	private String 		lineaNegocio;
	private String 		socioNegocio;
	private String 		tipoProducto;
	
	private String 		nombrePlan;
	private BigDecimal	valorCuota;
	private String		perioricidad;
	private Date		fechaCargue;
	private Long 		nroTotalCuotas;
	private Long 		cuotasFacturadas;
	private Long 		nroPeriodosImpagos;
	private Date 		fechaCompra;
	private Long 		cuotasFaltantes;

	// Totales facturados
	private BigDecimal totalServicios;
	private BigDecimal totalCedulas;
	
	
	public String getNombrePlan() {
		return nombrePlan;
	}
	public void setNombrePlan(String nombrePlan) {
		this.nombrePlan = nombrePlan;
	}
	public BigDecimal getValorCuota() {
		return valorCuota;
	}
	public void setValorCuota(BigDecimal valorCuota) {
		this.valorCuota = valorCuota;
	}
	public String getPerioricidad() {
		return perioricidad;
	}
	public void setPerioricidad(String perioricidad) {
		this.perioricidad = perioricidad;
	}
	public Date getFechaCargue() {
		return fechaCargue;
	}
	public void setFechaCargue(Date fechaCargue) {
		this.fechaCargue = fechaCargue;
	}
	public Long getNroTotalCuotas() {
		return nroTotalCuotas;
	}
	public void setNroTotalCuotas(Long nroTotalCuotas) {
		this.nroTotalCuotas = nroTotalCuotas;
	}
	public Long getCuotasFacturadas() {
		return cuotasFacturadas;
	}
	public void setCuotasFacturadas(Long cuotasFacturadas) {
		this.cuotasFacturadas = cuotasFacturadas;
	}
	public Long getNroPeriodosImpagos() {
		return nroPeriodosImpagos;
	}
	public void setNroPeriodosImpagos(Long nroPeriodosImpagos) {
		this.nroPeriodosImpagos = nroPeriodosImpagos;
	}
	public Date getFechaCompra() {
		return fechaCompra;
	}
	public void setFechaCompra(Date fechaCompra) {
		this.fechaCompra = fechaCompra;
	}
	public Long getCuotasFaltantes() {
		return cuotasFaltantes;
	}
	public void setCuotasFaltantes(Long cuotasFaltantes) {
		this.cuotasFaltantes = cuotasFaltantes;
	}
	public BigDecimal getTotalServicios() {
		return totalServicios;
	}
	public void setTotalServicios(BigDecimal totalServicios) {
		this.totalServicios = totalServicios;
	}
	public BigDecimal getTotalCedulas() {
		return totalCedulas;
	}
	public void setTotalCedulas(BigDecimal totalCedulas) {
		this.totalCedulas = totalCedulas;
	}
	public String getNroEco() {
		return nroEco;
	}
	public void setNroEco(String nroEco) {
		this.nroEco = nroEco;
	}
	public String getIdEco() {
		return idEco;
	}
	public void setIdEco(String idEco) {
		this.idEco = idEco;
	}
	public String getTitular() {
		return titular;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	public String getLineaNegocio() {
		return lineaNegocio;
	}
	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}
	public String getSocioNegocio() {
		return socioNegocio;
	}
	public void setSocioNegocio(String socioNegocio) {
		this.socioNegocio = socioNegocio;
	}
	public String getTipoProducto() {
		return tipoProducto;
	}
	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}
	
/*	private String descripcion;
	private BigDecimal valorAPagar;	

	// Clasificados
	private Date fechaPublicacion;
	private BigDecimal valorAviso;
	
	// Seguros 
	private Date fechaAfiliacion;
	private Long antiguedad;
	
	// Suscripciones
	private Date fechaSuscripcion;
	private BigDecimal valorSuscripcion;
	private Long cuotaAPagar;
	private Long cuotaTotal;
	
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public BigDecimal getValorAPagar() {
		return valorAPagar;
	}
	public void setValorAPagar(BigDecimal valorAPagar) {
		this.valorAPagar = valorAPagar;
	}
	public Date getFechaPublicacion() {
		return fechaPublicacion;
	}
	public void setFechaPublicacion(Date fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}
	public BigDecimal getValorAviso() {
		return valorAviso;
	}
	public void setValorAviso(BigDecimal valorAviso) {
		this.valorAviso = valorAviso;
	}
	public Date getFechaAfiliacion() {
		return fechaAfiliacion;
	}
	public void setFechaAfiliacion(Date fechaAfiliacion) {
		this.fechaAfiliacion = fechaAfiliacion;
	}
	public Long getAntiguedad() {
		return antiguedad;
	}
	public void setAntiguedad(Long antiguedad) {
		this.antiguedad = antiguedad;
	}
	public Date getFechaSuscripcion() {
		return fechaSuscripcion;
	}
	public void setFechaSuscripcion(Date fechaSuscripcion) {
		this.fechaSuscripcion = fechaSuscripcion;
	}
	public BigDecimal getValorSuscripcion() {
		return valorSuscripcion;
	}
	public void setValorSuscripcion(BigDecimal valorSuscripcion) {
		this.valorSuscripcion = valorSuscripcion;
	}
	public Long getCuotaAPagar() {
		return cuotaAPagar;
	}
	public void setCuotaAPagar(Long cuotaAPagar) {
		this.cuotaAPagar = cuotaAPagar;
	}
	public Long getCuotaTotal() {
		return cuotaTotal;
	}
	public void setCuotaTotal(Long cuotaTotal) {
		this.cuotaTotal = cuotaTotal;
	}
	
*/		
}