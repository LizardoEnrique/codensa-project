/**
 * 
 */
package com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada;

import com.synapsis.cns.codensa.model.buscaCuenta.avanzada.BuscaCuentaNumeroTransformador;
import com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaImpl;

/**
 * @author Emiliano Arango (ar30557486)
 * 
 */
public class BuscaCuentaNumeroTransformadorImpl extends BuscaCuentaImpl
		implements BuscaCuentaNumeroTransformador {
	private String nroTransformador;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada.BuscaCuentaNumeroTransformador#getNroTransformador()
	 */
	public String getNroTransformador() {
		return nroTransformador;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada.BuscaCuentaNumeroTransformador#setNroTransformador(java.lang.String)
	 */
	public void setNroTransformador(String nroTransformador) {
		this.nroTransformador = nroTransformador;
	}

}
