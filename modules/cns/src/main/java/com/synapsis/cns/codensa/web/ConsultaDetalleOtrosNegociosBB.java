package com.synapsis.cns.codensa.web;

import org.apache.myfaces.custom.datatable.ext.HtmlDataTable;
import org.apache.myfaces.custom.service.UIService;

/**
 *
 * @author adambrosio
 *
 */
public class ConsultaDetalleOtrosNegociosBB  {
	private static final long serialVersionUID = 1L;
	
	private Integer comboMaxResults = new Integer(100);	
	private Long nroServicio;
	private String nombreTitular;
	private String nroIdSolicitante;
	private String nroIdSocio;
	private String tipoIdentificacionSolicitante;
//	private TipoServicioByNroCuenta tipoServicioByNroCuenta;
    private String tipoServicioFinanciero;	
	private UIService findServiciosFinancierosOCHByCriteriaService;
	private UIService findServiciosECOByCriteriaService;
	private UIService findServiciosCuotaManejoByCriteriaService;
	private HtmlDataTable dataTableOCH;	
	private HtmlDataTable dataTableECO;
	private HtmlDataTable dataTableCuotaManejo;
	
	public void search() {
		  this.getFindServiciosECOByCriteriaService().execute();
	      this.getDataTableECO().setFirst(0);
  		  this.getFindServiciosFinancierosOCHByCriteriaService().execute();
		  this.getDataTableOCH().setFirst(0);
		  this.getFindServiciosCuotaManejoByCriteriaService().execute();
		  this.getDataTableECO().setFirst(0);		  
	}

	public HtmlDataTable getDataTableOCH() {
		return dataTableOCH;
	}

	public void setDataTableOCH(HtmlDataTable dataTableOCH) {
		this.dataTableOCH = dataTableOCH;
	}


	
/*	public TipoServicioByNroCuenta getTipoServicioByNroCuenta() {
		return tipoServicioByNroCuenta;
	}

	public void setTipoServicioByNroCuenta(
			TipoServicioByNroCuenta tipoServicioByNroCuenta) {
		this.tipoServicioByNroCuenta = tipoServicioByNroCuenta;
	}*/

	public UIService getFindServiciosECOByCriteriaService() {
		return findServiciosECOByCriteriaService;
	}

	public void setFindServiciosECOByCriteriaService(
			UIService findServiciosECOByCriteriaService) {
		this.findServiciosECOByCriteriaService = findServiciosECOByCriteriaService;
	}

	public UIService getFindServiciosFinancierosOCHByCriteriaService() {
		return findServiciosFinancierosOCHByCriteriaService;
	}

	public void setFindServiciosFinancierosOCHByCriteriaService(
			UIService findServiciosFinancierosOCHByCriteriaService) {
		this.findServiciosFinancierosOCHByCriteriaService = findServiciosFinancierosOCHByCriteriaService;
	}

	public String getNombreTitular() {
	   return nombreTitular;
	}

	public void setNombreTitular(String nombreTitular) {
	   this.nombreTitular = nombreTitular;
	}

	public String getNroIdSocio() {
	   return nroIdSocio;
	}

	public void setNroIdSocio(String nroIdSocio) {
	   this.nroIdSocio = nroIdSocio;
	}

	public String getNroIdSolicitante() {
	   return nroIdSolicitante;
	}

	public void setNroIdSolicitante(String nroIdSolicitante) {
	   this.nroIdSolicitante = nroIdSolicitante;
	}

	public Long getNroServicio() {
	   return nroServicio;
	}

	public void setNroServicio(Long nroServicio) {
	   this.nroServicio = nroServicio;
	}

	public Integer getComboMaxResults() {
		return comboMaxResults;
	}

	public void setComboMaxResults(Integer comboMaxResults) {
		this.comboMaxResults = comboMaxResults;
	}

	public String getTipoIdentificacionSolicitante() {
		return tipoIdentificacionSolicitante;
	}

	public void setTipoIdentificacionSolicitante(
			String tipoIdentificacionSolicitante) {
		this.tipoIdentificacionSolicitante = tipoIdentificacionSolicitante;
	}

	public String getTipoServicioFinanciero() {
		return tipoServicioFinanciero;
	}


	public void setTipoServicioFinanciero(String tipoServicioFinanciero) {
		this.tipoServicioFinanciero = tipoServicioFinanciero;
	}

	public HtmlDataTable getDataTableECO() {
		return dataTableECO;
	}

	public void setDataTableECO(HtmlDataTable dataTableECO) {
		this.dataTableECO = dataTableECO;
	}

	public UIService getFindServiciosCuotaManejoByCriteriaService() {
		return findServiciosCuotaManejoByCriteriaService;
	}

	public void setFindServiciosCuotaManejoByCriteriaService(
			UIService findServiciosCuotaManejoByCriteriaService) {
		this.findServiciosCuotaManejoByCriteriaService = findServiciosCuotaManejoByCriteriaService;
	}

	public HtmlDataTable getDataTableCuotaManejo() {
		return dataTableCuotaManejo;
	}

	public void setDataTableCuotaManejo(HtmlDataTable dataTableCuotaManejo) {
		this.dataTableCuotaManejo = dataTableCuotaManejo;
	}

//	public HtmlDataTable getDataTableCuotaManejo() {
//		return dataTableCuotaManejo;
//	}
//
//	public void setDataTableCuotaManejo(HtmlDataTable dataTableCuotaManejo) {
//		this.dataTableCuotaManejo = dataTableCuotaManejo;
//	}
}


