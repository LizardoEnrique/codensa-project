package com.synapsis.cns.codensa.model.buscaCuenta.avanzada;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface BuscaCuentaAvanzadaNumeroMedidor extends SynergiaBusinessObject {

	/**
	 * @return the nroMedidor
	 */
	public String getNroMedidor();

	/**
	 * @param nroMedidor the nroMedidor to set
	 */
	public void setNroMedidor(String nroMedidor);

}