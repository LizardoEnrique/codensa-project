/*$Id: AICContactoMockServiceImpl.java,v 1.7 2008/03/20 00:39:09 ar29261698 Exp $*/
package com.synapsis.aic.service.integration;

import com.suivant.arquitectura.core.exception.BusinessException;
import com.suivant.arquitectura.core.integration.Module;
import com.suivant.arquitectura.core.service.impl.ServiceImpl;
import com.synapsis.cns.codensa.integration.kendari.service.AICContactoService;

/**
 * @autor: chino
 * @date: 31/01/2007
 */
public class AICContactoMockServiceImpl extends ServiceImpl implements AICContactoService {

	
	

	public AICContactoMockServiceImpl(Module module) {
		super(module);
	}

	public String generarContactoCertificaBuenCliente(Long cuenta, String nombreSolicitante, String tipoDocumento, Long nroDocumento, String userName) throws BusinessException {
        return "9999";   
    }
    
    public String generarContactoCertificaConvenio(Long cuenta, String nombreSolicitante, String tipoDocumento, Long nroDocumento, String userName) throws BusinessException {
        return "9999";   
    }


	public String getName() {
		return null;
	}
}
