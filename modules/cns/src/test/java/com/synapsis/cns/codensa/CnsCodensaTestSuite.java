/**
 * 
 */
package com.synapsis.cns.codensa;

import junit.framework.Test;
import junit.framework.TestResult;
import junit.framework.TestSuite;

import com.synapsis.cns.codensa.service.AntecedenteTransformadorServiceTest;
import com.synapsis.cns.codensa.service.ConsultaExpedienteEnvioRadicacionServiceTest;
import com.synapsis.cns.codensa.service.ConsultaExpedienteEnvioSSPDServiceTest;
import com.synapsis.cns.codensa.service.ConsultaExpedienteEvaluacionLaboratorioServiceTest;
import com.synapsis.cns.codensa.service.ConsultaExpedienteInspeccionServiceTest;
import com.synapsis.cns.codensa.service.ConsultaExpedienteKilowatiosServiceTest;
import com.synapsis.cns.codensa.service.ConsultaExpedienteRadicacionesServiceTest;
import com.synapsis.cns.codensa.service.ConsultaExpedientesDetalleAnomaliasServiceTest;
import com.synapsis.cns.codensa.service.ConsultaOrdenesServiceTest;
import com.synapsis.cns.codensa.service.ConsultaPagosServiceTest;
import com.synapsis.cns.codensa.service.ContactoAicServiceTest;
import com.synapsis.cns.codensa.service.DatosCuentaServiceTest;
import com.synapsis.cns.codensa.service.DatosMedidorServiceTest;
import com.synapsis.cns.codensa.service.DetalleAnomaliaServiceTest;
import com.synapsis.cns.codensa.service.DeudaServicioServiceTest;
import com.synapsis.cns.codensa.service.EstadoMedidorServiceTest;
import com.synapsis.cns.codensa.service.EventosComponentesServiceTest;
import com.synapsis.cns.codensa.service.EventosMedidorServiceTest;
import com.synapsis.cns.codensa.service.EventosSellosServiceTest;
import com.synapsis.cns.codensa.service.EventosTransformadorServiceTest;
import com.synapsis.cns.codensa.service.InformacionMedidorServiceTest;
import com.synapsis.cns.codensa.service.InformacionOperacionServiceTest;
import com.synapsis.cns.codensa.service.LeyArrendamientoServiceTest;
import com.synapsis.cns.codensa.service.ObservacionAtencionAicServiceTest;
import com.synapsis.cns.codensa.service.OrdenAtencionAicServiceTest;
import com.synapsis.cns.codensa.service.OtrosServiciosServiceTest;
import com.synapsis.cns.codensa.service.ReporteCertificaBuenClienteServiceTest;
import com.synapsis.cns.codensa.service.RestriccionServiceTest;
import com.synapsis.cns.codensa.service.RestriccionesSuspensionServiceTest;
import com.synapsis.cns.codensa.service.ResumenDeudaServiceTest;
import com.synapsis.cns.codensa.service.SellosMedidorServiceTest;
import com.synapsis.cns.codensa.service.ServicioElectricoServiceTest;
import com.synapsis.cns.codensa.service.TitularCuentaServiceTest;

/**
 * @author dbraccio
 * 
 * La suite de los test de cns-codensa
 * 
 */
public class CnsCodensaTestSuite {

	// public static String SPRING_TEST_CONFIG=""META-INF/aic/spring/*.xml"";
	public static String SPRING_TEST_CONFIG = "META-INF/cns/spring/*.xml";

	public static Test suite() {
		TestSuite suite = new TestSuite();

		suite.addTestSuite(AntecedenteTransformadorServiceTest.class);
		// suite.addTestSuite(CaracteristicasCargaServiceTest.class);
		// suite.addTestSuite(CaracteristicasGeneralServiceTest.class);
		// suite.addTestSuite(CaracteristicasTransformadorServiceTest.class);
		suite.addTestSuite(ConsultaOrdenesServiceTest.class);
		suite.addTestSuite(ConsultaPagosServiceTest.class);
		suite.addTestSuite(ContactoAicServiceTest.class);
		suite.addTestSuite(DatosCuentaServiceTest.class);
		suite.addTestSuite(DatosMedidorServiceTest.class);
		suite.addTestSuite(DetalleAnomaliaServiceTest.class);
		suite.addTestSuite(DeudaServicioServiceTest.class);
		suite.addTestSuite(EstadoMedidorServiceTest.class);
		suite.addTestSuite(EventosComponentesServiceTest.class);
		suite.addTestSuite(EventosMedidorServiceTest.class);
		suite.addTestSuite(EventosSellosServiceTest.class);
		suite.addTestSuite(EventosTransformadorServiceTest.class);
		// suite.addTestSuite(HistoricoLecturasServiceTest.class);
		suite.addTestSuite(InformacionMedidorServiceTest.class);
		suite.addTestSuite(InformacionOperacionServiceTest.class);
		suite.addTestSuite(LeyArrendamientoServiceTest.class);
		suite.addTestSuite(ObservacionAtencionAicServiceTest.class);
		suite.addTestSuite(OrdenAtencionAicServiceTest.class);
		suite.addTestSuite(OtrosServiciosServiceTest.class);
		suite.addTestSuite(ReporteCertificaBuenClienteServiceTest.class);
		suite.addTestSuite(RestriccionesSuspensionServiceTest.class);
		suite.addTestSuite(RestriccionServiceTest.class);
		suite.addTestSuite(ResumenDeudaServiceTest.class);
		suite.addTestSuite(SellosMedidorServiceTest.class);
		suite.addTestSuite(ServicioElectricoServiceTest.class);
		suite.addTestSuite(TitularCuentaServiceTest.class);

		suite.addTestSuite(ConsultaExpedienteEnvioRadicacionServiceTest.class);
		suite.addTestSuite(ConsultaExpedienteEnvioSSPDServiceTest.class);
		suite.addTestSuite(ConsultaExpedienteEvaluacionLaboratorioServiceTest.class);
		suite.addTestSuite(ConsultaExpedienteInspeccionServiceTest.class);
		suite.addTestSuite(ConsultaExpedienteKilowatiosServiceTest.class);
		suite.addTestSuite(ConsultaExpedienteRadicacionesServiceTest.class);
		suite.addTestSuite(ConsultaExpedientesDetalleAnomaliasServiceTest.class);
		return suite;
	}

	public void testAll() {
		suite().run(new TestResult());
	}
}
