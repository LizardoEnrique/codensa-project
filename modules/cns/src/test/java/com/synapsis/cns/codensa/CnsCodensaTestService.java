/**
 * 
 */
package com.synapsis.cns.codensa;

import com.suivant.arquitectura.support.tests.service.SpringFinderServiceTestCase;


/**
 * 
 * @author dbraccio
 *
 *	 clase de la cual deben extender los test case de services 
 *	 de cns-codensa
 */

public abstract class CnsCodensaTestService extends SpringFinderServiceTestCase {
	public CnsCodensaTestService() {
		super(CnsCodensaTestSuite.SPRING_TEST_CONFIG);
	}
	
	

}
