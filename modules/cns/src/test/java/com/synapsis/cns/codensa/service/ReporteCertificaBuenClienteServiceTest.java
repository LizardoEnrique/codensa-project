package com.synapsis.cns.codensa.service;

import com.suivant.arquitectura.core.model.PersistentObject;
import com.synapsis.cns.codensa.CnsCodensaTestService;
import com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenClienteImpl;

/**
 * @author m3.MarioRoss - 12/12/2006
 *
 */
public class ReporteCertificaBuenClienteServiceTest extends CnsCodensaTestService {
	
	protected String getServiceName() {
		return "certificaBuenClienteServiceFinder";
	}
	protected void assertsUpdate(PersistentObject oldBean, PersistentObject newBean) {
		// TODO Auto-generated method stub
		
	}

	protected Class getBeanClass() {
		// TODO Auto-generated method stub
		return ReporteCertificaBuenClienteImpl.class;
	}

	protected PersistentObject getMockBean() {
		// TODO Auto-generated method stub
		return null;
	}

	protected PersistentObject getMockModifiedBean(PersistentObject bean) {
		// TODO Auto-generated method stub
		return null;
	}

	
//	public ReporteCertificaBuenClienteServiceTest() {
//		super();
//	}
//
//	protected void setUp() throws Exception {
//		super.setUp();
//		
//		cal = Calendar.getInstance();
//		cal.set(2006, 5, 21); 
//		
//		reporte = new ReporteCertificaBuenClienteImpl();
//		reporte.setFechaExpedicion(cal.getTime());
//	}
//
//	/**
//	 * Test method for {@link com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenClienteImpl#getDiaExpedicion()}.
//	 */
//	public void testGetDiaExpedicion() {
//		assertEquals(reporte.getDiaExpedicion().intValue(), 21);
//	}
//
//	/**
//	 * Test method for {@link com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenClienteImpl#getMesExpedicion()}.
//	 */
//	public void testGetMesExpedicion() {
//		assertEquals(reporte.getMesExpedicion(), "junio");
//	}
//
//	/**
//	 * Test method for {@link com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenClienteImpl#getAnioExpedicion()}.
//	 */
//	public void testGetAnioExpedicion() {
//		assertEquals(reporte.getAnioExpedicion().intValue(), 2006);
//	}
//
//	public void testReporteCertificaBuenCliente() {		
//		EqualQueryFilter eqf = new EqualQueryFilter();
//		eqf.setAttributeName("nroCuenta");
//		eqf.setAttributeValue(new Long("3836"));
//
//		eqf.setMaxResults(new Integer(10));
//		eqf.setPositionStart(new Integer(0));
//
//		List list = this.getFinderService().findByCriteria(eqf);
//		System.out.println(list);
//		ReporteCertificaBuenCliente reporte = (ReporteCertificaBuenCliente) list.get(0);
//		System.out.println(reporte.getNombreTitularCuenta());		
//	}
}
