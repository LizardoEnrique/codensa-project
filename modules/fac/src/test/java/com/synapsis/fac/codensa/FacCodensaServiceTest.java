/**
 * 
 */
package com.synapsis.fac.codensa;

import com.suivant.arquitectura.support.tests.service.SpringFinderServiceTestCase;

/**
 * @author ar18799631
 *
 */
public abstract class FacCodensaServiceTest extends SpringFinderServiceTestCase {

	public FacCodensaServiceTest(){
		super(FacCodensaTestSuite.SPRING_TEST_CONFIG);
	}
	
}
