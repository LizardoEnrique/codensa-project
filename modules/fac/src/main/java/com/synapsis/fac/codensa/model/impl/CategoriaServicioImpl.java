/**
 * 
 */
package com.synapsis.fac.codensa.model.impl;

import com.synapsis.fac.codensa.model.CategoriaServicio;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class CategoriaServicioImpl extends SynergiaBusinessObjectImpl implements CategoriaServicio {
	private static final long serialVersionUID = 1L;
	private String codCategoria;
	private String descripCategoria;
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CategoriaServicio#getCodCategoria()
	 */
	public String getCodCategoria() {
		return codCategoria;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CategoriaServicio#setCodCategoria(java.lang.String)
	 */
	public void setCodCategoria(String codCategoria) {
		this.codCategoria = codCategoria;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CategoriaServicio#getDescripCategoria()
	 */
	public String getDescripCategoria() {
		return descripCategoria;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CategoriaServicio#setDescripCategoria(java.lang.String)
	 */
	public void setDescripCategoria(String descripCategoria) {
		this.descripCategoria = descripCategoria;
	}
	
}
