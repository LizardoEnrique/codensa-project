package com.synapsis.fac.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface CobrosAdicionalesModificaciones extends SynergiaBusinessObject {

	/**
	 * @return the areaGeneroCargo
	 */
	public String getAreaGeneroCargo();

	/**
	 * @param areaGeneroCargo the areaGeneroCargo to set
	 */
	public void setAreaGeneroCargo(String areaGeneroCargo);

	/**
	 * @return the codigoCargo
	 */
	public String getCodigoCargo();

	/**
	 * @param codigoCargo the codigoCargo to set
	 */
	public void setCodigoCargo(String codigoCargo);

	/**
	 * @return the datoAnterior
	 */
	public String getDatoAnterior();

	/**
	 * @param datoAnterior the datoAnterior to set
	 */
	public void setDatoAnterior(String datoAnterior);

	/**
	 * @return the datoNuevo
	 */
	public String getDatoNuevo();

	/**
	 * @param datoNuevo the datoNuevo to set
	 */
	public void setDatoNuevo(String datoNuevo);

	/**
	 * @return the descripcionCargo
	 */
	public String getDescripcionCargo();

	/**
	 * @param descripcionCargo the descripcionCargo to set
	 */
	public void setDescripcionCargo(String descripcionCargo);

	/**
	 * @return the estadoCargo
	 */
	public String getEstadoCargo();

	/**
	 * @param estadoCargo the estadoCargo to set
	 */
	public void setEstadoCargo(String estadoCargo);

	/**
	 * @return the fechaFacturacion
	 */
	public Date getFechaFacturacion();

	/**
	 * @param fechaFacturacion the fechaFacturacion to set
	 */
	public void setFechaFacturacion(Date fechaFacturacion);

	/**
	 * @return the fechaModificacion
	 */
	public Date getFechaModificacion();

	/**
	 * @param fechaModificacion the fechaModificacion to set
	 */
	public void setFechaModificacion(Date fechaModificacion);

	/**
	 * @return the numeroDocumento
	 */
	public String getNumeroDocumento();

	/**
	 * @param numeroDocumento the numeroDocumento to set
	 */
	public void setNumeroDocumento(String numeroDocumento);

	/**
	 * @return the observacionesIngreso
	 */
	public String getObservacionesIngreso();

	/**
	 * @param observacionesIngreso the observacionesIngreso to set
	 */
	public void setObservacionesIngreso(String observacionesIngreso);

	/**
	 * @return the observacionesModificacion
	 */
	public String getObservacionesModificacion();

	/**
	 * @param observacionesModificacion the observacionesModificacion to set
	 */
	public void setObservacionesModificacion(String observacionesModificacion);

	/**
	 * @return the rolIngresoCargo
	 */
	public String getRolIngresoCargo();

	/**
	 * @param rolIngresoCargo the rolIngresoCargo to set
	 */
	public void setRolIngresoCargo(String rolIngresoCargo);

	/**
	 * @return the rolModifico
	 */
	public String getRolModifico();

	/**
	 * @param rolModifico the rolModifico to set
	 */
	public void setRolModifico(String rolModifico);

	/**
	 * @return the soporte
	 */
	public String getSoporte();

	/**
	 * @param soporte the soporte to set
	 */
	public void setSoporte(String soporte);

	/**
	 * @return the tipoDocumento
	 */
	public String getTipoDocumento();

	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public void setTipoDocumento(String tipoDocumento);

	/**
	 * @return the tipoModificacion
	 */
	public String getTipoModificacion();

	/**
	 * @param tipoModificacion the tipoModificacion to set
	 */
	public void setTipoModificacion(String tipoModificacion);

	/**
	 * @return the valorCargo
	 */
	public String getValorCargo();

	/**
	 * @param valorCargo the valorCargo to set
	 */
	public void setValorCargo(String valorCargo);

}