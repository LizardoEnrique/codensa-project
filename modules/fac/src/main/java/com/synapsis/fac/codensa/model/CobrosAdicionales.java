package com.synapsis.fac.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface CobrosAdicionales extends SynergiaBusinessObject {

	/**
	 * @return the areaGenera
	 */
	public abstract String getAreaGenera();

	/**
	 * @param areaGenera the areaGenera to set
	 */
	public abstract void setAreaGenera(String areaGenera);

	/**
	 * @return the cantidadCobros
	 */
	public abstract Long getCantidadCobros();

	/**
	 * @param cantidadCobros the cantidadCobros to set
	 */
	public abstract void setCantidadCobros(Long cantidadCobros);

	/**
	 * @return the cantidadUnidades
	 */
	public abstract Long getCantidadUnidades();

	/**
	 * @param cantidadUnidades the cantidadUnidades to set
	 */
	public abstract void setCantidadUnidades(Long cantidadUnidades);

	/**
	 * @return the cicloRutaFacturacion
	 */
	public abstract String getCicloRutaFacturacion();

	/**
	 * @param cicloRutaFacturacion the cicloRutaFacturacion to set
	 */
	public abstract void setCicloRutaFacturacion(String cicloRutaFacturacion);

	/**
	 * @return the cicloRutaLectura
	 */
	public abstract String getCicloRutaLectura();

	/**
	 * @param cicloRutaLectura the cicloRutaLectura to set
	 */
	public abstract void setCicloRutaLectura(String cicloRutaLectura);

	/**
	 * @return the claseServicio
	 */
	public abstract String getClaseServicio();

	/**
	 * @param claseServicio the claseServicio to set
	 */
	public abstract void setClaseServicio(String claseServicio);

	/**
	 * @return the codigoCargo
	 */
	public abstract String getCodigoCargo();

	/**
	 * @param codigoCargo the codigoCargo to set
	 */
	public abstract void setCodigoCargo(String codigoCargo);

	/**
	 * @return the codigoPrecio
	 */
	public abstract String getCodigoPrecio();

	/**
	 * @param codigoPrecio the codigoPrecio to set
	 */
	public abstract void setCodigoPrecio(String codigoPrecio);

	/**
	 * @return the codMunicipioRutaFacturacion
	 */
	public abstract String getCodMunicipioRutaFacturacion();

	/**
	 * @param codMunicipioRutaFacturacion the codMunicipioRutaFacturacion to set
	 */
	public abstract void setCodMunicipioRutaFacturacion(
			String codMunicipioRutaFacturacion);

	/**
	 * @return the codMunicipioRutaLectura
	 */
	public abstract String getCodMunicipioRutaLectura();

	/**
	 * @param codMunicipioRutaLectura the codMunicipioRutaLectura to set
	 */
	public abstract void setCodMunicipioRutaLectura(
			String codMunicipioRutaLectura);

	/**
	 * @return the codSucursalRutaFacturacion
	 */
	public abstract String getCodSucursalRutaFacturacion();

	/**
	 * @param codSucursalRutaFacturacion the codSucursalRutaFacturacion to set
	 */
	public abstract void setCodSucursalRutaFacturacion(
			String codSucursalRutaFacturacion);

	/**
	 * @return the codSucursalRutaLectura
	 */
	public abstract String getCodSucursalRutaLectura();

	/**
	 * @param codSucursalRutaLectura the codSucursalRutaLectura to set
	 */
	public abstract void setCodSucursalRutaLectura(String codSucursalRutaLectura);

	/**
	 * @return the descripcionCargo
	 */
	public abstract String getDescripcionCargo();

	/**
	 * @param descripcionCargo the descripcionCargo to set
	 */
	public abstract void setDescripcionCargo(String descripcionCargo);

	/**
	 * @return the estadoCargo
	 */
	public abstract String getEstadoCargo();

	/**
	 * @param estadoCargo the estadoCargo to set
	 */
	public abstract void setEstadoCargo(String estadoCargo);

	/**
	 * @return the estratoSocioEconomico
	 */
	public abstract String getEstratoSocioEconomico();

	/**
	 * @param estratoSocioEconomico the estratoSocioEconomico to set
	 */
	public abstract void setEstratoSocioEconomico(String estratoSocioEconomico);

	/**
	 * @return the fechaEmision
	 */
	public abstract Date getFechaEmision();

	/**
	 * @param fechaEmision the fechaEmision to set
	 */
	public abstract void setFechaEmision(Date fechaEmision);

	/**
	 * @return the fechaFacturacion
	 */
	public abstract Date getFechaFacturacion();

	/**
	 * @param fechaFacturacion the fechaFacturacion to set
	 */
	public abstract void setFechaFacturacion(Date fechaFacturacion);

	/**
	 * @return the fechaIngresoSistema
	 */
	public abstract Date getFechaIngresoSistema();

	/**
	 * @param fechaIngresoSistema the fechaIngresoSistema to set
	 */
	public abstract void setFechaIngresoSistema(Date fechaIngresoSistema);

	/**
	 * @return the monto
	 */
	public abstract Double getMonto();

	/**
	 * @param monto the monto to set
	 */
	public abstract void setMonto(Double monto);

	/**
	 * @return the municipioRutaFacturacion
	 */
	public abstract String getMunicipioRutaFacturacion();

	/**
	 * @param municipioRutaFacturacion the municipioRutaFacturacion to set
	 */
	public abstract void setMunicipioRutaFacturacion(
			String municipioRutaFacturacion);

	/**
	 * @return the municipioRutaLectura
	 */
	public abstract String getMunicipioRutaLectura();

	/**
	 * @param municipioRutaLectura the municipioRutaLectura to set
	 */
	public abstract void setMunicipioRutaLectura(String municipioRutaLectura);

	/**
	 * @return the nroCuenta
	 */
	public abstract Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public abstract void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroServicio
	 */
	public abstract Long getNroServicio();

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public abstract void setNroServicio(Long nroServicio);

	/**
	 * @return the numeroDocumento
	 */
	public abstract String getNumeroDocumento();

	/**
	 * @param numeroDocumento the numeroDocumento to set
	 */
	public abstract void setNumeroDocumento(String numeroDocumento);

	/**
	 * @return the observacionesIngreso
	 */
	public abstract String getObservacionesIngreso();

	/**
	 * @param observacionesIngreso the observacionesIngreso to set
	 */
	public abstract void setObservacionesIngreso(String observacionesIngreso);

	/**
	 * @return the presentaModificaciones
	 */
	public abstract String getPresentaModificaciones();

	/**
	 * @param presentaModificaciones the presentaModificaciones to set
	 */
	public abstract void setPresentaModificaciones(String presentaModificaciones);

	/**
	 * @return the rolIngreso
	 */
	public abstract String getRolIngreso();

	/**
	 * @param rolIngreso the rolIngreso to set
	 */
	public abstract void setRolIngreso(String rolIngreso);

	/**
	 * @return the rutaFacturacion
	 */
	public abstract String getRutaFacturacion();

	/**
	 * @param rutaFacturacion the rutaFacturacion to set
	 */
	public abstract void setRutaFacturacion(String rutaFacturacion);

	/**
	 * @return the rutaLectura
	 */
	public abstract String getRutaLectura();

	/**
	 * @param rutaLectura the rutaLectura to set
	 */
	public abstract void setRutaLectura(String rutaLectura);

	/**
	 * @return the soporte
	 */
	public abstract String getSoporte();

	/**
	 * @param soporte the soporte to set
	 */
	public abstract void setSoporte(String soporte);

	/**
	 * @return the subclaseServicio
	 */
	public abstract String getSubclaseServicio();

	/**
	 * @param subclaseServicio the subclaseServicio to set
	 */
	public abstract void setSubclaseServicio(String subclaseServicio);

	/**
	 * @return the sucursalRutaFacturacion
	 */
	public abstract String getSucursalRutaFacturacion();

	/**
	 * @param sucursalRutaFacturacion the sucursalRutaFacturacion to set
	 */
	public abstract void setSucursalRutaFacturacion(
			String sucursalRutaFacturacion);

	/**
	 * @return the sucursalRutaLectura
	 */
	public abstract String getSucursalRutaLectura();

	/**
	 * @param sucursalRutaLectura the sucursalRutaLectura to set
	 */
	public abstract void setSucursalRutaLectura(String sucursalRutaLectura);

	/**
	 * @return the tipoCargo
	 */
	public abstract String getTipoCargo();

	/**
	 * @param tipoCargo the tipoCargo to set
	 */
	public abstract void setTipoCargo(String tipoCargo);

	/**
	 * @return the tipoDocumento
	 */
	public abstract String getTipoDocumento();

	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public abstract void setTipoDocumento(String tipoDocumento);

	/**
	 * @return the tipoServicio
	 */
	public abstract String getTipoServicio();

	/**
	 * @param tipoServicio the tipoServicio to set
	 */
	public abstract void setTipoServicio(String tipoServicio);

	/**
	 * @return the valorCargo
	 */
	public abstract String getValorCargo();

	/**
	 * @param valorCargo the valorCargo to set
	 */
	public abstract void setValorCargo(String valorCargo);

}