package com.synapsis.fac.codensa.model;

public interface CategoriaServicio {

	/**
	 * @return the codCategoria
	 */
	public String getCodCategoria();

	/**
	 * @param codCategoria the codCategoria to set
	 */
	public void setCodCategoria(String codCategoria);

	/**
	 * @return the descripCategoria
	 */
	public String getDescripCategoria();

	/**
	 * @param descripCategoria the descripCategoria to set
	 */
	public void setDescripCategoria(String descripCategoria);

}