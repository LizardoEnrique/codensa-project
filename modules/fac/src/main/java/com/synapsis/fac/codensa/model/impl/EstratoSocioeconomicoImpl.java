/**
 * 
 */
package com.synapsis.fac.codensa.model.impl;

import com.synapsis.fac.codensa.model.EstratoSocioeconomico;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class EstratoSocioeconomicoImpl extends SynergiaBusinessObjectImpl implements EstratoSocioeconomico {
	private static final long serialVersionUID = 1L;
	
	private String codigo;
	private String descripcion;
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.EstratoSocioeconomico#getCodigo()
	 */
	public String getCodigo() {
		return codigo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.EstratoSocioeconomico#setCodigo(java.lang.String)
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.EstratoSocioeconomico#getDescripcion()
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.EstratoSocioeconomico#setDescripcion(java.lang.String)
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
