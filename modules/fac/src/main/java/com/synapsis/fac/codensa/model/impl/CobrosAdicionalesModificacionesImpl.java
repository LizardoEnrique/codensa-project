/**
 * 
 */
package com.synapsis.fac.codensa.model.impl;

import java.util.Date;

import com.synapsis.fac.codensa.model.CobrosAdicionalesModificaciones;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class CobrosAdicionalesModificacionesImpl extends
		SynergiaBusinessObjectImpl implements CobrosAdicionalesModificaciones  {

	private String codigoCargo;
	private String descripcionCargo;
	private String valorCargo;
	private String estadoCargo;
	private String rolIngresoCargo;
	private String areaGeneroCargo;
	private String soporte;
	private String observacionesIngreso;
	private String tipoDocumento;
	private String numeroDocumento;
	private Date fechaFacturacion;
	private String tipoModificacion;
	private Date fechaModificacion;
	private String datoAnterior;
	private String datoNuevo;
	private String rolModifico;
	private String observacionesModificacion;
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#getAreaGeneroCargo()
	 */
	public String getAreaGeneroCargo() {
		return areaGeneroCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#setAreaGeneroCargo(java.lang.String)
	 */
	public void setAreaGeneroCargo(String areaGeneroCargo) {
		this.areaGeneroCargo = areaGeneroCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#getCodigoCargo()
	 */
	public String getCodigoCargo() {
		return codigoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#setCodigoCargo(java.lang.String)
	 */
	public void setCodigoCargo(String codigoCargo) {
		this.codigoCargo = codigoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#getDatoAnterior()
	 */
	public String getDatoAnterior() {
		return datoAnterior;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#setDatoAnterior(java.lang.String)
	 */
	public void setDatoAnterior(String datoAnterior) {
		this.datoAnterior = datoAnterior;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#getDatoNuevo()
	 */
	public String getDatoNuevo() {
		return datoNuevo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#setDatoNuevo(java.lang.String)
	 */
	public void setDatoNuevo(String datoNuevo) {
		this.datoNuevo = datoNuevo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#getDeescripcionCargo()
	 */
	public String getDescripcionCargo() {
		return descripcionCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#setDeescripcionCargo(java.lang.String)
	 */
	public void setDescripcionCargo(String descripcionCargo) {
		this.descripcionCargo = descripcionCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#getEstadoCargo()
	 */
	public String getEstadoCargo() {
		return estadoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#setEstadoCargo(java.lang.String)
	 */
	public void setEstadoCargo(String estadoCargo) {
		this.estadoCargo = estadoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#getFechaFacturacion()
	 */
	public Date getFechaFacturacion() {
		return fechaFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#setFechaFacturacion(java.util.Date)
	 */
	public void setFechaFacturacion(Date fechaFacturacion) {
		this.fechaFacturacion = fechaFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#getFechaModificacion()
	 */
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#setFechaModificacion(java.util.Date)
	 */
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#getNumeroDocumento()
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#setNumeroDocumento(java.lang.String)
	 */
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#getObservacionesIngreso()
	 */
	public String getObservacionesIngreso() {
		return observacionesIngreso;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#setObservacionesIngreso(java.lang.String)
	 */
	public void setObservacionesIngreso(String observacionesIngreso) {
		this.observacionesIngreso = observacionesIngreso;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#getObservacionesModificacion()
	 */
	public String getObservacionesModificacion() {
		return observacionesModificacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#setObservacionesModificacion(java.lang.String)
	 */
	public void setObservacionesModificacion(String observacionesModificacion) {
		this.observacionesModificacion = observacionesModificacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#getRolIngresoCargo()
	 */
	public String getRolIngresoCargo() {
		return rolIngresoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#setRolIngresoCargo(java.lang.String)
	 */
	public void setRolIngresoCargo(String rolIngresoCargo) {
		this.rolIngresoCargo = rolIngresoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#getRolModifico()
	 */
	public String getRolModifico() {
		return rolModifico;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#setRolModifico(java.lang.String)
	 */
	public void setRolModifico(String rolModifico) {
		this.rolModifico = rolModifico;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#getSoporte()
	 */
	public String getSoporte() {
		return soporte;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#setSoporte(java.lang.String)
	 */
	public void setSoporte(String soporte) {
		this.soporte = soporte;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#getTipoDocumento()
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#setTipoDocumento(java.lang.String)
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#getTipoModificacion()
	 */
	public String getTipoModificacion() {
		return tipoModificacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#setTipoModificacion(java.lang.String)
	 */
	public void setTipoModificacion(String tipoModificacion) {
		this.tipoModificacion = tipoModificacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#getValorCargo()
	 */
	public String getValorCargo() {
		return valorCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionalesModificaciones#setValorCargo(java.lang.String)
	 */
	public void setValorCargo(String valorCargo) {
		this.valorCargo = valorCargo;
	} 

}
