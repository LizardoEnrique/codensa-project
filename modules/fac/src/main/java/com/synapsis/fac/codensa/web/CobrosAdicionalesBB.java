package com.synapsis.fac.codensa.web;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.faces.component.html.HtmlCommandButton;

import org.apache.myfaces.component.html.ext.HtmlDataTable;

import com.suivant.arquitectura.presentation.utils.JSFUtils;
import org.apache.myfaces.custom.service.UIService;

public class CobrosAdicionalesBB {

	
	private Long nroCuenta;
	private String codigoCargo;
	private String tipoServicio;
	private String areaGenera;
	private String rolIngreso;
	private Date fechaFacturacionDesde;
	private Date fechaFacturacionHasta;
	private Date fechaIngresoSistemaDesde;
	private Date fechaIngresoSistemaHasta;
	private Double montoDesde;
	private Double montoHasta;
	
	private String codMunicipioRutaFacturacion;
	private String codSucursalRutaFacturacion;
	private String cicloRutaFacturacion;
	private String codMunicipioRutaLectura;
	private String codSucursalRutaLectura;
	private String cicloRutaLectura;
	
	private Long claseServicio;
	private Long subclaseServicio;
	private Long estratoSocioEconomico;
	
	//private CobrosAdicionalesImpl cobrosAdi;
	// La idea del datatable es que generalmente el target pertenezca a un BO, 
	// entonces la premisa que se asume es que si yo BO tengo una lista de hijos, 
	// el que resulve el modelado de 1 a n no me deja un bean de hijos nulos , sino 
	// que me deja una arreglo vacio ... entonces ... 
	private Collection cobrosAdicionales = new ArrayList();
	/**
	 * Comment for <code>dataTable</code> Tabla en la cual se dejan los
	 * resultados de la busqueda ralizada. FIXME: [dbraccio]necesitamos la tabla
	 * para poder refrescarla
	 */
	private HtmlDataTable dataTableCobros;
	private HtmlDataTable dataTableCobrosModificaciones;
	private HtmlCommandButton buttonConsultaModificaciones;
	
	/**
	 * Comment for <code>findByCriteriaService</code> Servicio para realizar
	 * la busqueda
	 */
	private UIService findByCriteriaCobrosAdicionalesService;
	private UIService findByCriteriaCobrosAdicionalesModificacionesService;
	
	
	/**
	 * Action del BB Action para realizar la busqueda de PersistentObject segun
	 * el filtro seleccionado Al presionar sobre el boton Buscar, se ejecuta la
	 * busqueda FIXME: [dbraccio] ver con falduto para luego de realizar la
	 * busqueda no tener que setear la posicion de inicio de la tabla
	 */
	public void search() {
		//HtmlDataTable dataTable = (HtmlDataTable) JSFUtils.getComponentFromTree(componentName);
		//this.getFindByCriteriaCobrosAdicionalesService().setValue(null);
		//this.getDataTableCobros().setValue(null);
		this.getFindByCriteriaCobrosAdicionalesService().execute();
		this.getDataTableCobros().setFirst(0);
		//limpio la grid de modificaciones, si tviera datos
		this.getFindByCriteriaCobrosAdicionalesModificacionesService().setValue(null);
		this.getDataTableCobrosModificaciones().setFirst(0);
	}


	public void searchModificaciones(){
		//deberia validas que se hayan seleccionado al menos algun item del dataTable principal
		this.getFindByCriteriaCobrosAdicionalesModificacionesService().execute();
		this.getDataTableCobrosModificaciones().setFirst(0);
	}

	/**
	 * @return the codigoCargo
	 */
	public String getCodigoCargo() {
		return codigoCargo;
	}


	/**
	 * @param codigoCargo the codigoCargo to set
	 */
	public void setCodigoCargo(String codigoCargo) {
		this.codigoCargo = codigoCargo;
	}


	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}


	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}


	/**
	 * @return the areaGenera
	 */
	public String getAreaGenera() {
		return areaGenera;
	}


	/**
	 * @param areaGenera the areaGenera to set
	 */
	public void setAreaGenera(String areaGenera) {
		this.areaGenera = areaGenera;
	}




	/**
	 * @return the tipoServicio
	 */
	public String getTipoServicio() {
		return tipoServicio;
	}


	/**
	 * @param tipoServicio the tipoServicio to set
	 */
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}


	public String getRolIngreso() {
		return rolIngreso;
	}


	public void setRolIngreso(String rolIngreso) {
		this.rolIngreso = rolIngreso;
	}


	/**
	 * @return the montoDesde
	 */
	public Double getMontoDesde() {
		return montoDesde;
	}


	/**
	 * @param montoDesde the montoDesde to set
	 */
	public void setMontoDesde(Double montoDesde) {
		this.montoDesde = montoDesde;
	}


	/**
	 * @return the montoHasta
	 */
	public Double getMontoHasta() {
		return montoHasta;
	}


	/**
	 * @param montoHasta the montoHasta to set
	 */
	public void setMontoHasta(Double montoHasta) {
		this.montoHasta = montoHasta;
	}


	/**
	 * @return the fechaFacturacionDesde
	 */
	public Date getFechaFacturacionDesde() {
		return fechaFacturacionDesde;
	}


	/**
	 * @param fechaFacturacionDesde the fechaFacturacionDesde to set
	 */
	public void setFechaFacturacionDesde(Date fechaFacturacionDesde) {
		this.fechaFacturacionDesde = fechaFacturacionDesde;
	}


	/**
	 * @return the fechaFacturacionHasta
	 */
	public Date getFechaFacturacionHasta() {
		return fechaFacturacionHasta;
	}


	/**
	 * @param fechaFacturacionHasta the fechaFacturacionHasta to set
	 */
	public void setFechaFacturacionHasta(Date fechaFacturacionHasta) {
		this.fechaFacturacionHasta = fechaFacturacionHasta;
	}


	/**
	 * @return the fechaIngresoSistemaDesde
	 */
	public Date getFechaIngresoSistemaDesde() {
		return fechaIngresoSistemaDesde;
	}


	/**
	 * @param fechaIngresoSistemaDesde the fechaIngresoSistemaDesde to set
	 */
	public void setFechaIngresoSistemaDesde(Date fechaIngresoSistemaDesde) {
		this.fechaIngresoSistemaDesde = fechaIngresoSistemaDesde;
	}


	/**
	 * @return the fechaIngresoSistemaHasta
	 */
	public Date getFechaIngresoSistemaHasta() {
		return fechaIngresoSistemaHasta;
	}


	/**
	 * @param fechaIngresoSistemaHasta the fechaIngresoSistemaHasta to set
	 */
	public void setFechaIngresoSistemaHasta(Date fechaIngresoSistemaHasta) {
		this.fechaIngresoSistemaHasta = fechaIngresoSistemaHasta;
	}


	/**
	 * @return the cicloRutaFacturacion
	 */
	public String getCicloRutaFacturacion() {
		return cicloRutaFacturacion;
	}


	/**
	 * @param cicloRutaFacturacion the cicloRutaFacturacion to set
	 */
	public void setCicloRutaFacturacion(String cicloRutaFacturacion) {
		this.cicloRutaFacturacion = cicloRutaFacturacion;
	}


	/**
	 * @return the cicloRutaLectura
	 */
	public String getCicloRutaLectura() {
		return cicloRutaLectura;
	}


	/**
	 * @param cicloRutaLectura the cicloRutaLectura to set
	 */
	public void setCicloRutaLectura(String cicloRutaLectura) {
		this.cicloRutaLectura = cicloRutaLectura;
	}


	/**
	 * @return the claseServicio
	 */
	public Long getClaseServicio() {
		return claseServicio;
	}


	/**
	 * @param claseServicio the claseServicio to set
	 */
	public void setClaseServicio(Long claseServicio) {
		this.claseServicio = claseServicio;
	}


	/**
	 * @return the codMunicipioRutaFacturacion
	 */
	public String getCodMunicipioRutaFacturacion() {
		return codMunicipioRutaFacturacion;
	}


	/**
	 * @param codMunicipioRutaFacturacion the codMunicipioRutaFacturacion to set
	 */
	public void setCodMunicipioRutaFacturacion(String codMunicipioRutaFacturacion) {
		this.codMunicipioRutaFacturacion = codMunicipioRutaFacturacion;
	}


	/**
	 * @return the codMunicipioRutaLectura
	 */
	public String getCodMunicipioRutaLectura() {
		return codMunicipioRutaLectura;
	}


	/**
	 * @param codMunicipioRutaLectura the codMunicipioRutaLectura to set
	 */
	public void setCodMunicipioRutaLectura(String codMunicipioRutaLectura) {
		this.codMunicipioRutaLectura = codMunicipioRutaLectura;
	}


	/**
	 * @return the codSucursalRutaFacturacion
	 */
	public String getCodSucursalRutaFacturacion() {
		return codSucursalRutaFacturacion;
	}


	/**
	 * @param codSucursalRutaFacturacion the codSucursalRutaFacturacion to set
	 */
	public void setCodSucursalRutaFacturacion(String codSucursalRutaFacturacion) {
		this.codSucursalRutaFacturacion = codSucursalRutaFacturacion;
	}


	/**
	 * @return the codSucursalRutaLectura
	 */
	public String getCodSucursalRutaLectura() {
		return codSucursalRutaLectura;
	}


	/**
	 * @param codSucursalRutaLectura the codSucursalRutaLectura to set
	 */
	public void setCodSucursalRutaLectura(String codSucursalRutaLectura) {
		this.codSucursalRutaLectura = codSucursalRutaLectura;
	}


	/**
	 * @return the estratoSocioEconomico
	 */
	public Long getEstratoSocioEconomico() {
		return estratoSocioEconomico;
	}


	/**
	 * @param estratoSocioEconomico the estratoSocioEconomico to set
	 */
	public void setEstratoSocioEconomico(Long estratoSocioEconomico) {
		this.estratoSocioEconomico = estratoSocioEconomico;
	}


	/**
	 * @return the subclaseServicio
	 */
	public Long getSubclaseServicio() {
		return subclaseServicio;
	}


	/**
	 * @param subclaseServicio the subclaseServicio to set
	 */
	public void setSubclaseServicio(Long subclaseServicio) {
		this.subclaseServicio = subclaseServicio;
	}

	/**
	 * @return the cobrosAdicionales
	 */
	public Collection getCobrosAdicionales() {
		return cobrosAdicionales;
	}

	/**
	 * @param cobrosAdicionales the cobrosAdicionales to set
	 */
	public void setCobrosAdicionales(Collection cobrosAdicionales) {
		this.cobrosAdicionales = cobrosAdicionales;
	}

	/**
	 * @return the buttonConsultaModificaciones
	 */
	public HtmlCommandButton getButtonConsultaModificaciones() {
		return buttonConsultaModificaciones;
	}

	/**
	 * @param buttonConsultaModificaciones the buttonConsultaModificaciones to set
	 */
	public void setButtonConsultaModificaciones(
			HtmlCommandButton buttonConsultaModificaciones) {
		this.buttonConsultaModificaciones = buttonConsultaModificaciones;
		
		
	}

	/**
	 * @return the findByCriteriaCobrosAdicionalesModificacionesService
	 */
	public UIService getFindByCriteriaCobrosAdicionalesModificacionesService() {
		return findByCriteriaCobrosAdicionalesModificacionesService;
	}

	/**
	 * @param findByCriteriaCobrosAdicionalesModificacionesService the findByCriteriaCobrosAdicionalesModificacionesService to set
	 */
	public void setFindByCriteriaCobrosAdicionalesModificacionesService(
			UIService findByCriteriaCobrosAdicionalesModificacionesService) {
		this.findByCriteriaCobrosAdicionalesModificacionesService = findByCriteriaCobrosAdicionalesModificacionesService;
	}

	/**
	 * @return the findByCriteriaCobrosAdicionalesService
	 */
	public UIService getFindByCriteriaCobrosAdicionalesService() {
		return findByCriteriaCobrosAdicionalesService;
	}

	/**
	 * @param findByCriteriaCobrosAdicionalesService the findByCriteriaCobrosAdicionalesService to set
	 */
	public void setFindByCriteriaCobrosAdicionalesService(
			UIService findByCriteriaCobrosAdicionalesService) {
		this.findByCriteriaCobrosAdicionalesService = findByCriteriaCobrosAdicionalesService;
	}

	/**
	 * @return the dataTableCobros
	 */
	public HtmlDataTable getDataTableCobros() {
		return dataTableCobros;
	}

	/**
	 * @param dataTableCobros the dataTableCobros to set
	 */
	public void setDataTableCobros(HtmlDataTable dataTableCobros) {
		this.dataTableCobros = dataTableCobros;
	}

	/**
	 * @return the dataTableCobrosModificaciones
	 */
	public HtmlDataTable getDataTableCobrosModificaciones() {
		return dataTableCobrosModificaciones;
	}

	/**
	 * @param dataTableCobrosModificaciones the dataTableCobrosModificaciones to set
	 */
	public void setDataTableCobrosModificaciones(
			HtmlDataTable dataTableCobrosModificaciones) {
		this.dataTableCobrosModificaciones = dataTableCobrosModificaciones;
	}

		
	
	

}
