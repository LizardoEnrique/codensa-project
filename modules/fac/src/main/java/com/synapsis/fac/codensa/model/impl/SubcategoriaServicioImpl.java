/**
 * 
 */
package com.synapsis.fac.codensa.model.impl;

import com.synapsis.fac.codensa.model.SubcategoriaServicio;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class SubcategoriaServicioImpl extends SynergiaBusinessObjectImpl 
implements SubcategoriaServicio {
	private static final long serialVersionUID = 1L;
	private String codSubcategoria;
	private String descripSubcategoria;
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.SubcategoriaServicio#getCodSubcategoria()
	 */
	public String getCodSubcategoria() {
		return codSubcategoria;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.SubcategoriaServicio#setCodSubcategoria(java.lang.String)
	 */
	public void setCodSubcategoria(String codSubcategoria) {
		this.codSubcategoria = codSubcategoria;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.SubcategoriaServicio#getDescripSubcategoria()
	 */
	public String getDescripSubcategoria() {
		return descripSubcategoria;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.SubcategoriaServicio#setDescripSubcategoria(java.lang.String)
	 */
	public void setDescripSubcategoria(String descripSubcategoria) {
		this.descripSubcategoria = descripSubcategoria;
	}
	
}
