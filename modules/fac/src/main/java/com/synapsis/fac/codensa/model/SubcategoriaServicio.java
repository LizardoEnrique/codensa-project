package com.synapsis.fac.codensa.model;

public interface SubcategoriaServicio {

	/**
	 * @return the codSubcategoria
	 */
	public String getCodSubcategoria();

	/**
	 * @param codSubcategoria the codSubcategoria to set
	 */
	public void setCodSubcategoria(String codSubcategoria);

	/**
	 * @return the descripSubcategoria
	 */
	public String getDescripSubcategoria();

	/**
	 * @param descripSubcategoria the descripSubcategoria to set
	 */
	public void setDescripSubcategoria(String descripSubcategoria);

}