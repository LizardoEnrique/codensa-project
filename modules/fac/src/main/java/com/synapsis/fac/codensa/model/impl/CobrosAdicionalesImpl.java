/**
 * 
 */
package com.synapsis.fac.codensa.model.impl;

import java.util.Date;

import com.synapsis.fac.codensa.model.CobrosAdicionales;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class CobrosAdicionalesImpl extends SynergiaBusinessObjectImpl implements CobrosAdicionales  {
	private Long nroServicio;
	private String tipoServicio;
	private String rutaLectura;
	private Long nroCuenta;
	private String rutaFacturacion;
	private String codigoCargo;
	private String descripcionCargo;
	private String valorCargo;
	private String estadoCargo;// (Pendiente por facturar / Facturado)
	private Date fechaIngresoSistema; //filtro x rango
	private String presentaModificaciones;
	private String tipoCargo;
	private String codigoPrecio;
	private Long cantidadUnidades;
	private Long cantidadCobros;
	private String rolIngreso;
	private String areaGenera;
	private String soporte;
	private String observacionesIngreso;
	private String numeroDocumento;
	private String tipoDocumento;
	private Date fechaEmision;
	private Double monto; // para filtro por rango de montos
	private Date fechaFacturacion; // para filtro
	private String codMunicipioRutaFacturacion;
	private String municipioRutaFacturacion;
	private String codSucursalRutaFacturacion;
	private String sucursalRutaFacturacion;
	private String cicloRutaFacturacion;
	
	private String codMunicipioRutaLectura;
	private String municipioRutaLectura;
	private String codSucursalRutaLectura;
	private String sucursalRutaLectura;
	private String cicloRutaLectura;
	private String claseServicio;
	private String subclaseServicio;
	private String estratoSocioEconomico;
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getAreaGenera()
	 */
	public String getAreaGenera() {
		return areaGenera;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setAreaGenera(java.lang.String)
	 */
	public void setAreaGenera(String areaGenera) {
		this.areaGenera = areaGenera;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getCantidadCobros()
	 */
	public Long getCantidadCobros() {
		return cantidadCobros;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setCantidadCobros(java.lang.Long)
	 */
	public void setCantidadCobros(Long cantidadCobros) {
		this.cantidadCobros = cantidadCobros;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getCantidadUnidades()
	 */
	public Long getCantidadUnidades() {
		return cantidadUnidades;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setCantidadUnidades(java.lang.Long)
	 */
	public void setCantidadUnidades(Long cantidadUnidades) {
		this.cantidadUnidades = cantidadUnidades;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getCicloRutaFacturacion()
	 */
	public String getCicloRutaFacturacion() {
		return cicloRutaFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setCicloRutaFacturacion(java.lang.String)
	 */
	public void setCicloRutaFacturacion(String cicloRutaFacturacion) {
		this.cicloRutaFacturacion = cicloRutaFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getCicloRutaLectura()
	 */
	public String getCicloRutaLectura() {
		return cicloRutaLectura;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setCicloRutaLectura(java.lang.String)
	 */
	public void setCicloRutaLectura(String cicloRutaLectura) {
		this.cicloRutaLectura = cicloRutaLectura;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getClaseServicio()
	 */
	public String getClaseServicio() {
		return claseServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setClaseServicio(java.lang.String)
	 */
	public void setClaseServicio(String claseServicio) {
		this.claseServicio = claseServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getCodigoCargo()
	 */
	public String getCodigoCargo() {
		return codigoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setCodigoCargo(java.lang.String)
	 */
	public void setCodigoCargo(String codigoCargo) {
		this.codigoCargo = codigoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getCodigoPrecio()
	 */
	public String getCodigoPrecio() {
		return codigoPrecio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setCodigoPrecio(java.lang.String)
	 */
	public void setCodigoPrecio(String codigoPrecio) {
		this.codigoPrecio = codigoPrecio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getCodMunicipioRutaFacturacion()
	 */
	public String getCodMunicipioRutaFacturacion() {
		return codMunicipioRutaFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setCodMunicipioRutaFacturacion(java.lang.String)
	 */
	public void setCodMunicipioRutaFacturacion(String codMunicipioRutaFacturacion) {
		this.codMunicipioRutaFacturacion = codMunicipioRutaFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getCodMunicipioRutaLectura()
	 */
	public String getCodMunicipioRutaLectura() {
		return codMunicipioRutaLectura;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setCodMunicipioRutaLectura(java.lang.String)
	 */
	public void setCodMunicipioRutaLectura(String codMunicipioRutaLectura) {
		this.codMunicipioRutaLectura = codMunicipioRutaLectura;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getCodSucursalRutaFacturacion()
	 */
	public String getCodSucursalRutaFacturacion() {
		return codSucursalRutaFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setCodSucursalRutaFacturacion(java.lang.String)
	 */
	public void setCodSucursalRutaFacturacion(String codSucursalRutaFacturacion) {
		this.codSucursalRutaFacturacion = codSucursalRutaFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getCodSucursalRutaLectura()
	 */
	public String getCodSucursalRutaLectura() {
		return codSucursalRutaLectura;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setCodSucursalRutaLectura(java.lang.String)
	 */
	public void setCodSucursalRutaLectura(String codSucursalRutaLectura) {
		this.codSucursalRutaLectura = codSucursalRutaLectura;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getDescripcionCargo()
	 */
	public String getDescripcionCargo() {
		return descripcionCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setDescripcionCargo(java.lang.String)
	 */
	public void setDescripcionCargo(String descripcionCargo) {
		this.descripcionCargo = descripcionCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getEstadoCargo()
	 */
	public String getEstadoCargo() {
		return estadoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setEstadoCargo(java.lang.String)
	 */
	public void setEstadoCargo(String estadoCargo) {
		this.estadoCargo = estadoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getEstratoSocioEconomico()
	 */
	public String getEstratoSocioEconomico() {
		return estratoSocioEconomico;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setEstratoSocioEconomico(java.lang.String)
	 */
	public void setEstratoSocioEconomico(String estratoSocioEconomico) {
		this.estratoSocioEconomico = estratoSocioEconomico;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getFechaEmision()
	 */
	public Date getFechaEmision() {
		return fechaEmision;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setFechaEmision(java.util.Date)
	 */
	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getFechaFacturacion()
	 */
	public Date getFechaFacturacion() {
		return fechaFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setFechaFacturacion(java.util.Date)
	 */
	public void setFechaFacturacion(Date fechaFacturacion) {
		this.fechaFacturacion = fechaFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getFechaIngresoSistema()
	 */
	public Date getFechaIngresoSistema() {
		return fechaIngresoSistema;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setFechaIngresoSistema(java.util.Date)
	 */
	public void setFechaIngresoSistema(Date fechaIngresoSistema) {
		this.fechaIngresoSistema = fechaIngresoSistema;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getMonto()
	 */
	public Double getMonto() {
		return monto;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setMonto(java.lang.Double)
	 */
	public void setMonto(Double monto) {
		this.monto = monto;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getMunicipioRutaFacturacion()
	 */
	public String getMunicipioRutaFacturacion() {
		return municipioRutaFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setMunicipioRutaFacturacion(java.lang.String)
	 */
	public void setMunicipioRutaFacturacion(String municipioRutaFacturacion) {
		this.municipioRutaFacturacion = municipioRutaFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getMunicipioRutaLectura()
	 */
	public String getMunicipioRutaLectura() {
		return municipioRutaLectura;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setMunicipioRutaLectura(java.lang.String)
	 */
	public void setMunicipioRutaLectura(String municipioRutaLectura) {
		this.municipioRutaLectura = municipioRutaLectura;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getNroServicio()
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setNroServicio(java.lang.Long)
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getNumeroDocumento()
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setNumeroDocumento(java.lang.String)
	 */
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getObservacionesIngreso()
	 */
	public String getObservacionesIngreso() {
		return observacionesIngreso;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setObservacionesIngreso(java.lang.String)
	 */
	public void setObservacionesIngreso(String observacionesIngreso) {
		this.observacionesIngreso = observacionesIngreso;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getPresentaModificaciones()
	 */
	public String getPresentaModificaciones() {
		return presentaModificaciones;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setPresentaModificaciones(java.lang.String)
	 */
	public void setPresentaModificaciones(String presentaModificaciones) {
		this.presentaModificaciones = presentaModificaciones;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getRolIngreso()
	 */
	public String getRolIngreso() {
		return rolIngreso;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setRolIngreso(java.lang.String)
	 */
	public void setRolIngreso(String rolIngreso) {
		this.rolIngreso = rolIngreso;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getRutaFacturacion()
	 */
	public String getRutaFacturacion() {
		return rutaFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setRutaFacturacion(java.lang.String)
	 */
	public void setRutaFacturacion(String rutaFacturacion) {
		this.rutaFacturacion = rutaFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getRutaLectura()
	 */
	public String getRutaLectura() {
		return rutaLectura;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setRutaLectura(java.lang.String)
	 */
	public void setRutaLectura(String rutaLectura) {
		this.rutaLectura = rutaLectura;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getSoporte()
	 */
	public String getSoporte() {
		return soporte;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setSoporte(java.lang.String)
	 */
	public void setSoporte(String soporte) {
		this.soporte = soporte;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getSubclaseServicio()
	 */
	public String getSubclaseServicio() {
		return subclaseServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setSubclaseServicio(java.lang.String)
	 */
	public void setSubclaseServicio(String subclaseServicio) {
		this.subclaseServicio = subclaseServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getSucursalRutaFacturacion()
	 */
	public String getSucursalRutaFacturacion() {
		return sucursalRutaFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setSucursalRutaFacturacion(java.lang.String)
	 */
	public void setSucursalRutaFacturacion(String sucursalRutaFacturacion) {
		this.sucursalRutaFacturacion = sucursalRutaFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getSucursalRutaLectura()
	 */
	public String getSucursalRutaLectura() {
		return sucursalRutaLectura;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setSucursalRutaLectura(java.lang.String)
	 */
	public void setSucursalRutaLectura(String sucursalRutaLectura) {
		this.sucursalRutaLectura = sucursalRutaLectura;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getTipoCargo()
	 */
	public String getTipoCargo() {
		return tipoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setTipoCargo(java.lang.String)
	 */
	public void setTipoCargo(String tipoCargo) {
		this.tipoCargo = tipoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getTipoDocumento()
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setTipoDocumento(java.lang.String)
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getTipoServicio()
	 */
	public String getTipoServicio() {
		return tipoServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setTipoServicio(java.lang.String)
	 */
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#getValorCargo()
	 */
	public String getValorCargo() {
		return valorCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.fac.codensa.model.impl.CobrosAdicionales#setValorCargo(java.lang.String)
	 */
	public void setValorCargo(String valorCargo) {
		this.valorCargo = valorCargo;
	}
	
	
	
	}
