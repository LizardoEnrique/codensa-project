/**
 * $id: Clase[ComboLineaNegocio.java], Autor[ar31059727], Time[28/02/2007-11:13:36]
 */
package com.synapsis.fac.codensa.model.listbox;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * Interface de todos los combo.
 * 
 * @author ar31059727
 */
public interface ListBox extends SynergiaBusinessObject {

	/**
	 * @return the codigo
	 */
	public String getCodigo();

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo(String codigo);

	/**
	 * @return the descripcion
	 */
	public String getDescripcion();

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion);

}