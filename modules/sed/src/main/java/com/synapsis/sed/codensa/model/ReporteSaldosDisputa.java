/**
 * $Id: ReporteSaldosDisputa.java,v 1.7 2008/07/16 21:45:34 ar26557682 Exp $
 */
package com.synapsis.sed.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ReporteSaldosDisputa extends SynergiaBusinessObject{

	public String getAreaRealizadora();

	public void setAreaRealizadora(String areaRealizadora);

	public String getCicloFacturacion();

	public void setCicloFacturacion(String cicloFacturacion);

	public String getDiasAmpliados();

	public void setDiasAmpliados(String diasAmpliados);

	public String getEnergiaItemsAmortizadosAjusteReliq();

	public void setEnergiaItemsAmortizadosAjusteReliq(String energiaItemsAmortizadosAjusteReliq);

	public String getEnergiaItemsAmortizadosPorPago();

	public void setEnergiaItemsAmortizadosPorPago(String energiaItemsAmortizadosPorPago);

	public Double getEnergiaItemsDisputadosNoAmortizados();

	public void setEnergiaItemsDisputadosNoAmortizados(Double energiaItemsDisputadosNoAmortizados);

	public   String getEnergiaSED();

	public   void setEnergiaSED(String energiaSED);

	public   String getEstadoSED();

	public   void setEstadoSED(String estadoSED);

	public   Date getFechaAmpliacionTerminos();

	public   void setFechaAmpliacionTerminos(Date fechaAmpliacionTerminos);

	public   Date getFechaAprobacionCongelacion();

	public   void setFechaAprobacionCongelacion(Date fechaAprobacionCongelacion);

	public   Date getFechaEntraFacturacion();

	public   void setFechaEntraFacturacion(Date fechaEntraFacturacion);

	public   Date getFechaRealizacion();

	public   void setFechaRealizacion(Date fechaRealizacion);

	public   Date getFechaRealizacionDescongelacion();

	public   void setFechaRealizacionDescongelacion(Date fechaRealizacionDescongelacion);

	public   Date getFechaVencimientoCongelacionTransitoria();

	public   void setFechaVencimientoCongelacionTransitoria(Date fechaVencimientoCongelacionTransitoria);

	public   String getMotivoCongelacion();

	public   void setMotivoCongelacion(String motivoCongelacion);

	public   String getMotivoDescongelacion();

	public   void setMotivoDescongelacion(String motivoDescongelacion);

	public   String getNroAtencion();

	public   void setNroAtencion(String nroAtencion);

	public   Long getNroCuenta();

	public   void setNroCuenta(Long nroCuenta);

	public   String getNroOrdenSED();

	public   void setNroOrdenSED(String nroOrdenSED);

	public   Long getNroSED();

	public   void setNroSED(Long nroSED);

	public   String getTiempoTranscurridoCreacionAprobacionFinal();

	public   void setTiempoTranscurridoCreacionAprobacionFinal(String tiempoTranscurridoCreacionAprobacionFinal);

	public   String getTipoCongelacion();

	public   void setTipoCongelacion(String tipoCongelacion);

	public   String getUsuarioAprobador();

	public   void setUsuarioAprobador(String usuarioAprobador);

	public   String getUsuarioRealizador();

	public   void setUsuarioRealizador(String usuarioRealizador);

	public   String getValorSED();

	public   void setValorSED(String valorSED);

	public   String getValorTotalItemsDisputadosAmortizadosAjusteReliq();

	public   void setValorTotalItemsDisputadosAmortizadosAjusteReliq(String valorTotalItemsDisputadosAmortizadosAjusteReliq);

	public   String getValorTotalItemsDisputadosAmortizadosPorPago();

	public   void setValorTotalItemsDisputadosAmortizadosPorPago(String valorTotalItemsDisputadosAmortizadosPorPago);

	public   Double getValorTotalItemsNoAmortizados();

	public   void setValorTotalItemsNoAmortizados( Double valorTotalItemsNoAmortizados);

	public Long getIdAreaSolicitante();
	
	public void setIdAreaSolicitante(Long idAreaSolicitante);
	
	public String getIdEstado();
	
	public void setIdEstado(String idEstado);
	
	public   String getCantidadSED();
	
	public   void setCantidadSED(String cantidadSED);
	
	public   String getCargoItems();
	
	public   void setCargoItems(String cargoItems);
	
	public   String getCargoItemsAmortAjusteReliq();
	
	public   void setCargoItemsAmortAjusteReliq(String cargoItemsAmortAjusteReliq);
	
	public   String getClaseServicio();
	
	public   void setClaseServicio(String claseServicio);
	
	public   String getValoresDisputaAmortizados();
	
	public   void setValoresDisputaAmortizados(String valoresDisputaAmortizados);
	
	public   Long getIdUsuarioGestor();
	
	public   void setIdUsuarioGestor(Long idUsuarioGestor);
	
	public   Long getIdMotivoCongelacion();
	
	public   void setIdMotivoCongelacion(Long idMotivoCongelacion);
	
	public   String getCargoItemsAclaracion();
	
	public   void setCargoItemsAclaracion(String cargoItemsAclaracion);
	
	public String getIdTipoCongelacion();
	
	public void setIdTipoCongelacion(String idTipoCongelacion);
	
	public Date getFechaFinalizacion();
	
	public void setFechaFinalizacion(Date fechaFinalizacion);

	public Long getIdMotivoDescongelacion();
	
	public void setIdMotivoDescongelacion(Long idMotivoDescongelacion);
	
	public String getUsuarioCreadorDescongelacion();
	
	public void setUsuarioCreadorDescongelacion(String usuarioCreadorDescongelacion);

}