package com.synapsis.sed.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ReporteOrdenesSaldosDisputa extends SynergiaBusinessObject{

	/**
	 * @return the areaSolicitante
	 */
	public abstract String getAreaSolicitante();

	/**
	 * @param areaSolicitante the areaSolicitante to set
	 */
	public abstract void setAreaSolicitante(String areaSolicitante);

	/**
	 * @return the cicloFacturacion
	 */
	public abstract String getCicloFacturacion();

	/**
	 * @param cicloFacturacion the cicloFacturacion to set
	 */
	public abstract void setCicloFacturacion(String cicloFacturacion);

	/**
	 * @return the energia
	 */
	public abstract String getEnergia();

	/**
	 * @param energia the energia to set
	 */
	public abstract void setEnergia(String energia);

	/**
	 * @return the estado
	 */
	public abstract String getEstado();

	/**
	 * @param estado the estado to set
	 */
	public abstract void setEstado(String estado);

	/**
	 * @return the estadoSed
	 */
	public abstract String getEstadoSed();

	/**
	 * @param estadoSed the estadoSed to set
	 */
	public abstract void setEstadoSed(String estadoSed);

	/**
	 * @return the fechaCiclo
	 */
	public abstract Date getFechaCiclo();

	/**
	 * @param fechaCiclo the fechaCiclo to set
	 */
	public abstract void setFechaCiclo(Date fechaCiclo);

	/**
	 * @return the fechaCreacion
	 */
	public abstract Date getFechaCreacion();

	/**
	 * @param fechaCreacion the fechaCreacion to set
	 */
	public abstract void setFechaCreacion(Date fechaCreacion);

	/**
	 * @return the motivo
	 */
	public abstract String getMotivo();

	/**
	 * @param motivo the motivo to set
	 */
	public abstract void setMotivo(String motivo);

	/**
	 * @return the nroCuenta
	 */
	public abstract Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public abstract void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroOrden
	 */
	public abstract Long getNroOrden();

	/**
	 * @param nroOrden the nroOrden to set
	 */
	public abstract void setNroOrden(Long nroOrden);

	/**
	 * @return the nroSaldoDisputa
	 */
	public abstract Long getNroSaldoDisputa();

	/**
	 * @param nroSaldoDisputa the nroSaldoDisputa to set
	 */
	public abstract void setNroSaldoDisputa(Long nroSaldoDisputa);

	/**
	 * @return the tiempoTranscurridoCreacionHastaAprobacion
	 */
	public abstract Long getTiempoTranscurridoCreacionHastaAprobacion();

	/**
	 * @param tiempoTranscurridoCreacionHastaAprobacion the tiempoTranscurridoCreacionHastaAprobacion to set
	 */
	public abstract void setTiempoTranscurridoCreacionHastaAprobacion(
			Long tiempoTranscurridoCreacionHastaAprobacion);

	/**
	 * @return the tipoOrden
	 */
	public abstract String getTipoOrden();

	/**
	 * @param tipoOrden the tipoOrden to set
	 */
	public abstract void setTipoOrden(String tipoOrden);

	/**
	 * @return the usuarioCreador
	 */
	public abstract String getUsuarioCreador();

	/**
	 * @param usuarioCreador the usuarioCreador to set
	 */
	public abstract void setUsuarioCreador(String usuarioCreador);

	/**
	 * @return the valorSed
	 */
	public abstract Long getValorSed();

	/**
	 * @param valorSed the valorSed to set
	 */
	public abstract void setValorSed(Long valorSed);
	
	
	public abstract Long getIdAreaSolicitante(); 
	public abstract void setIdAreaSolicitante(Long idAreaSolicitante);
	
	public abstract String getIdEstado();
	public abstract void setIdEstado(String idEstado); 

	public Long getIdAreaAsignada();
	
	public void setIdAreaAsignada(Long idAreaAsignada);

}