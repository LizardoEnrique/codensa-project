/**
 * 
 */
package com.synapsis.sed.codensa.model.impl;

import java.util.Date;

import com.synapsis.sed.codensa.model.DatosUsuariosIntervinientes;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 *
 */
public class DatosUsuariosIntervinientesImpl extends SynergiaBusinessObjectImpl implements DatosUsuariosIntervinientes {
	
	
	private static final long serialVersionUID = 1L;
	
	private String usuariosIntervinientes;
	private Date fechaIntervencion;
	private String accionIntervencion; 	
	private String tiemposIntervencion;
	
	
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.DatosUsuariosIntervinientes#getAccionIntervencion()
	 */
	public String getAccionIntervencion() {
		return accionIntervencion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.DatosUsuariosIntervinientes#setAccionIntervencion(java.lang.String)
	 */
	public void setAccionIntervencion(String accionIntervencion) {
		this.accionIntervencion = accionIntervencion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.DatosUsuariosIntervinientes#getFechaIntervencion()
	 */
	public Date getFechaIntervencion() {
		return fechaIntervencion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.DatosUsuariosIntervinientes#setFechaIntervencion(java.util.Date)
	 */
	public void setFechaIntervencion(Date fechaIntervencion) {
		this.fechaIntervencion = fechaIntervencion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.DatosUsuariosIntervinientes#getTiemposIntervencion()
	 */
	public String getTiemposIntervencion() {
		return tiemposIntervencion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.DatosUsuariosIntervinientes#setTiemposIntervencion(java.lang.String)
	 */
	public void setTiemposIntervencion(String tiemposIntervencion) {
		this.tiemposIntervencion = tiemposIntervencion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.DatosUsuariosIntervinientes#getUsuariosIntervinientes()
	 */
	public String getUsuariosIntervinientes() {
		return usuariosIntervinientes;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.DatosUsuariosIntervinientes#setUsuariosIntervinientes(java.lang.String)
	 */
	public void setUsuariosIntervinientes(String usuariosIntervinientes) {
		this.usuariosIntervinientes = usuariosIntervinientes;
	}	
}