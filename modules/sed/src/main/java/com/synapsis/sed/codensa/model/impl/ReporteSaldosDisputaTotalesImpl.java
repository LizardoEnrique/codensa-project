package com.synapsis.sed.codensa.model.impl;

import java.util.Date;

import com.synapsis.sed.codensa.model.ReporteSaldosDisputaTotales;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ReporteSaldosDisputaTotalesImpl extends SynergiaBusinessObjectImpl 
 implements ReporteSaldosDisputaTotales {
	private Double valorTotalSed;
	private Double energiaTotalSed;
	private Double valorTotalItemsNoAmortizados;

	private Double energiaTotalItemsNoAmortizados;
	private Double valorTotalItemsAmortizadosAjuste;
	private Double energiaTotalItemsAmortizadosAjuste;

	private Double valorTotalItemsAmortizadosPago; 
	private Double energiaTotalItemsAmortizadosPago;
	private Double totalesCantidadOperacionesSed;
	
	private String tipoCongelacion;
	
   //properties de filtros
	private Date fechaRealizacion;
	private Date fechaFinalizacion;
	private String idEstado;
	private Long idAreaSolicitante;
	private Long idMotivoCongelacion;
	private Long idMotivoDescongelacion;
	private String idTipoCongelacion;
	private String usuarioRealizador;
	private String usuarioCreadorDescongelacion;
	private String cargoItems;
	private String claseServicio;
	private String cicloFacturacion;
	private String energiaSED;
	
	
	/**
	 * @return the cicloFacturacion
	 */
	public String getCicloFacturacion() {
		return cicloFacturacion;
	}
	/**
	 * @param cicloFacturacion the cicloFacturacion to set
	 */
	public void setCicloFacturacion(String cicloFacturacion) {
		this.cicloFacturacion = cicloFacturacion;
	}
	/**
	 * @return the fechaRealizacion
	 */
	public Date getFechaRealizacion() {
		return fechaRealizacion;
	}
	/**
	 * @param fechaRealizacion the fechaRealizacion to set
	 */
	public void setFechaRealizacion(Date fechaRealizacion) {
		this.fechaRealizacion = fechaRealizacion;
	}
	/**
	 * @return the idAreaSolicitante
	 */
	public Long getIdAreaSolicitante() {
		return idAreaSolicitante;
	}
	/**
	 * @param idAreaSolicitante the idAreaSolicitante to set
	 */
	public void setIdAreaSolicitante(Long idAreaSolicitante) {
		this.idAreaSolicitante = idAreaSolicitante;
	}
	/**
	 * @return the idEstado
	 */
	public String getIdEstado() {
		return idEstado;
	}
	/**
	 * @param idEstado the idEstado to set
	 */
	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}
	/**
	 * @return the tipoCongelacion
	 */
	public String getTipoCongelacion() {
		return tipoCongelacion;
	}
	/**
	 * @param tipoCongelacion the tipoCongelacion to set
	 */
	public void setTipoCongelacion(String tipoCongelacion) {
		this.tipoCongelacion = tipoCongelacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteSaldosDisputaTotales#getEnergiaTotalItemsAmortizadosAjuste()
	 */
	public Double getEnergiaTotalItemsAmortizadosAjuste() {
		return energiaTotalItemsAmortizadosAjuste;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteSaldosDisputaTotales#setEnergiaTotalItemsAmortizadosAjuste(java.lang.Double)
	 */
	public void setEnergiaTotalItemsAmortizadosAjuste(
			Double energiaTotalItemsAmortizadosAjuste) {
		this.energiaTotalItemsAmortizadosAjuste = energiaTotalItemsAmortizadosAjuste;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteSaldosDisputaTotales#getEnergiaTotalItemsAmortizadosPago()
	 */
	public Double getEnergiaTotalItemsAmortizadosPago() {
		return energiaTotalItemsAmortizadosPago;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteSaldosDisputaTotales#setEnergiaTotalItemsAmortizadosPago(java.lang.Double)
	 */
	public void setEnergiaTotalItemsAmortizadosPago(
			Double energiaTotalItemsAmortizadosPago) {
		this.energiaTotalItemsAmortizadosPago = energiaTotalItemsAmortizadosPago;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteSaldosDisputaTotales#getEnergiaTotalItemsNoAmortizados()
	 */
	public Double getEnergiaTotalItemsNoAmortizados() {
		return energiaTotalItemsNoAmortizados;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteSaldosDisputaTotales#setEnergiaTotalItemsNoAmortizados(java.lang.Double)
	 */
	public void setEnergiaTotalItemsNoAmortizados(
			Double energiaTotalItemsNoAmortizados) {
		this.energiaTotalItemsNoAmortizados = energiaTotalItemsNoAmortizados;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteSaldosDisputaTotales#getEnergiaTotalSed()
	 */
	public Double getEnergiaTotalSed() {
		return energiaTotalSed;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteSaldosDisputaTotales#setEnergiaTotalSed(java.lang.Double)
	 */
	public void setEnergiaTotalSed(Double energiaTotalSed) {
		this.energiaTotalSed = energiaTotalSed;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteSaldosDisputaTotales#getTotalesCantidadOperacionesSed()
	 */
	public Double getTotalesCantidadOperacionesSed() {
		return totalesCantidadOperacionesSed;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteSaldosDisputaTotales#setTotalesCantidadOperacionesSed(java.lang.Double)
	 */
	public void setTotalesCantidadOperacionesSed(
			Double totalesCantidadOperacionesSed) {
		this.totalesCantidadOperacionesSed = totalesCantidadOperacionesSed;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteSaldosDisputaTotales#getValorTotalItemsAmortizadosAjuste()
	 */
	public Double getValorTotalItemsAmortizadosAjuste() {
		return valorTotalItemsAmortizadosAjuste;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteSaldosDisputaTotales#setValorTotalItemsAmortizadosAjuste(java.lang.Double)
	 */
	public void setValorTotalItemsAmortizadosAjuste(
			Double valorTotalItemsAmortizadosAjuste) {
		this.valorTotalItemsAmortizadosAjuste = valorTotalItemsAmortizadosAjuste;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteSaldosDisputaTotales#getValorTotalItemsAmortizadosPago()
	 */
	public Double getValorTotalItemsAmortizadosPago() {
		return valorTotalItemsAmortizadosPago;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteSaldosDisputaTotales#setValorTotalItemsAmortizadosPago(java.lang.Double)
	 */
	public void setValorTotalItemsAmortizadosPago(
			Double valorTotalItemsAmortizadosPago) {
		this.valorTotalItemsAmortizadosPago = valorTotalItemsAmortizadosPago;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteSaldosDisputaTotales#getValorTotalItemsNoAmortizados()
	 */
	public Double getValorTotalItemsNoAmortizados() {
		return valorTotalItemsNoAmortizados;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteSaldosDisputaTotales#setValorTotalItemsNoAmortizados(java.lang.Double)
	 */
	public void setValorTotalItemsNoAmortizados(Double valorTotalItemsNoAmortizados) {
		this.valorTotalItemsNoAmortizados = valorTotalItemsNoAmortizados;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteSaldosDisputaTotales#getValorTotalSed()
	 */
	public Double getValorTotalSed() {
		return valorTotalSed;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteSaldosDisputaTotales#setValorTotalSed(java.lang.Double)
	 */
	public void setValorTotalSed(Double valorTotalSed) {
		this.valorTotalSed = valorTotalSed;
	}
	
	public String getCargoItems() {
		return cargoItems;
	}
	public void setCargoItems(String cargoItems) {
		this.cargoItems = cargoItems;
	}
	public String getClaseServicio() {
		return claseServicio;
	}
	public void setClaseServicio(String claseServicio) {
		this.claseServicio = claseServicio;
	}
	public String getEnergiaSED() {
		return energiaSED;
	}
	public void setEnergiaSED(String energiaSED) {
		this.energiaSED = energiaSED;
	}
	public Date getFechaFinalizacion() {
		return fechaFinalizacion;
	}
	public void setFechaFinalizacion(Date fechaFinalizacion) {
		this.fechaFinalizacion = fechaFinalizacion;
	}
	public Long getIdMotivoCongelacion() {
		return idMotivoCongelacion;
	}
	public void setIdMotivoCongelacion(Long idMotivoCongelacion) {
		this.idMotivoCongelacion = idMotivoCongelacion;
	}
	public Long getIdMotivoDescongelacion() {
		return idMotivoDescongelacion;
	}
	public void setIdMotivoDescongelacion(Long idMotivoDescongelacion) {
		this.idMotivoDescongelacion = idMotivoDescongelacion;
	}
	public String getIdTipoCongelacion() {
		return idTipoCongelacion;
	}
	public void setIdTipoCongelacion(String idTipoCongelacion) {
		this.idTipoCongelacion = idTipoCongelacion;
	}
	public String getUsuarioCreadorDescongelacion() {
		return usuarioCreadorDescongelacion;
	}
	public void setUsuarioCreadorDescongelacion(String usuarioCreadorDescongelacion) {
		this.usuarioCreadorDescongelacion = usuarioCreadorDescongelacion;
	}
	public String getUsuarioRealizador() {
		return usuarioRealizador;
	}
	public void setUsuarioRealizador(String usuarioRealizador) {
		this.usuarioRealizador = usuarioRealizador;
	}

	
	
}
