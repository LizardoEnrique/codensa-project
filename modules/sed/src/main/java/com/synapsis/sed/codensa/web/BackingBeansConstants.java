/**
 * $id: Clase[BackingBeansConstants.java], Autor[ar31059727], Time[28/02/2007-10:15:27]
 */
package com.synapsis.sed.codensa.web;

/**
 * Clase de soporte con las constantes utilizadas desde los Backing Beans.
 * 
 * @author ar31059727
 */
public class BackingBeansConstants {

	public static final String CODIGO = "codigo";

	public static final String DESCRIPCION = "descripcion";

	public static final String EMPTY_QUERY_FILTER = "sedEmptyQueryFilter";

	public static int MAX_ELEMENTS_LISTBOX = 5;
}