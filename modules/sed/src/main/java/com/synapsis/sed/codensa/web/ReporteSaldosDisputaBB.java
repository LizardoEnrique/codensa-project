package com.synapsis.sed.codensa.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.event.ActionEvent;

import org.apache.myfaces.custom.service.UIService;

import com.synapsis.commons.services.ServiceHelper;
import com.synapsis.sed.codensa.model.impl.ReporteSaldosDisputaTotalesImpl;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;

/**
 * Se paso la funcionalidad del modulo a la nueva forma de trabajo
 * 
 * @author (refactor)dbraccio - Suivant
 * 
 */
public class ReporteSaldosDisputaBB {

	private static final long serialVersionUID = 1L;

	// properties de los filtros de busqueda
	private Date fechaDesde;
	private Date fechaHasta;

	// las dos listas de multiple seleccion - si o si hay que inicializar estos tipos !!
	private List idEstados = new ArrayList();;
	private List idAreaSolicitantes = new ArrayList();;

	// los dos combos
	private Long idMotivoCongelacion;
	private Long idMotivoDescongelacion;
	private String idTipoCongelacion;
	private String usuarioRealizador;
	private String usuarioCreadorDescongelacion;
	
	private String cargoItems;
	private String claseServicio;
	private String cicloFacturacion;
	

	//TODO: ver estos filtros me parecen que no estan en la pantalla
	//energiaItemsDisputadosNoAmortizados
	//cargoItemsAclaracion
	//tiempoTranscurridoCreacionAprobacionFinal

	// property para armar el filtro de la Segunda tabla de la pantalla
	private Long idSaldoSelected;

	/**
	 * Tabla en la cual se dejan los resultados de la busqueda realizada de los
	 * saldos.
	 */
	private HtmlDataTable dataTableSaldos;

	/**
	 * Tabla en la cual se dejan los resultados de la busqueda ralizada de
	 * usuarios.
	 */
	private HtmlDataTable dataTableUsuariosSaldos;

	/**
	 * Servicio para realizar la busqueda
	 */
	private UIService findByCriteriaServiceSaldos;

	/**
	 * Servicio para realizar la busqueda
	 */
	private UIService findByCriteriaServiceUsuariosSaldos;

	// TODO: [dbraccio] ver esta property para que se usa
	private ReporteSaldosDisputaTotalesImpl totales;
	

	// ***********************************************************************
	// ********* actions del BB ************************************
	// **********************************************************************

	/**
	 * action que ejecuta la busqueda
	 */
	public void reporteSaldos() {
		this.getFindByCriteriaServiceSaldos().execute();
		this.getDataTableSaldos().setFirst(0);
		// esta linea borra los datos de la segunda tabla
		this.getFindByCriteriaServiceUsuariosSaldos().setValue(null);
		
		
		
		Map parameters = new HashMap();
		parameters.put("fechaRealizacion", this.fechaDesde);
		parameters.put("fechaFinalizacion", this.fechaHasta);
		parameters.put("idEstado", this.idEstados);
		parameters.put("idAreaSolicitante", this.idAreaSolicitantes);
		parameters.put("idMotivoCongelacion", this.idMotivoCongelacion);
		parameters.put("idMotivoDescongelacion", this.idMotivoDescongelacion);
		parameters.put("idTipoCongelacion", this.idTipoCongelacion);
		parameters.put("usuarioRealizador", this.usuarioRealizador);
		parameters.put("usuarioCreadorDescongelacion", this.usuarioCreadorDescongelacion);
		parameters.put("cargoItems", this.cargoItems);
		parameters.put("claseServicio", this.claseServicio);
		parameters.put("cicloFacturacion", this.cicloFacturacion);

		List result = (List) ServiceHelper.getResponse(
				SynergiaApplicationContext.getServiceManager(), "saldosDisputaTotalesServiceFinder", 
				"findByCriteria", parameters);
		
		if (result.size() != 0)
			this.totales = (ReporteSaldosDisputaTotalesImpl) result.get(0);
	}

	/**
	 * action que refresca la segunda tabla
	 * 
	 * @param event
	 */
	public void usuariosIntervinientes(ActionEvent event) {
		this.getFindByCriteriaServiceUsuariosSaldos().execute();
		this.getDataTableUsuariosSaldos().setFirst(0);
		
		List result = (List) ServiceHelper. getResponse(
				SynergiaApplicationContext.getServiceManager(), "saldosDisputaTotalesServiceFinder", 
				"findByCriteria", new Object [] {});
		
		if (result.size() != 0)
			this.totales = (ReporteSaldosDisputaTotalesImpl) result.get(0);
	}

	public ReporteSaldosDisputaTotalesImpl getTotales() {
		return totales;
	}

	public void setTotales(ReporteSaldosDisputaTotalesImpl totales) {
		this.totales = totales;
	}

	/**
	 * @return the cargoItems
	 */
	public String getCargoItems() {
		return cargoItems;
	}

	/**
	 * @return the cicloFacturacion
	 */
	public String getCicloFacturacion() {
		return cicloFacturacion;
	}

	/**
	 * @return the claseServicio
	 */
	public String getClaseServicio() {
		return claseServicio;
	}

	/**
	 * @return the dataTableSaldos
	 */
	public HtmlDataTable getDataTableSaldos() {
		return dataTableSaldos;
	}

	/**
	 * @return the dataTableUsuariosSaldos
	 */
	public HtmlDataTable getDataTableUsuariosSaldos() {
		return dataTableUsuariosSaldos;
	}

	/**
	 * @return the fechaDesde
	 */
	public Date getFechaDesde() {
		return fechaDesde;
	}

	/**
	 * @return the fechaHasta
	 */
	public Date getFechaHasta() {
		return fechaHasta;
	}

	/**
	 * @return the findByCriteriaServiceSaldos
	 */
	public UIService getFindByCriteriaServiceSaldos() {
		return findByCriteriaServiceSaldos;
	}

	/**
	 * @return the findByCriteriaServiceUsuariosSaldos
	 */
	public UIService getFindByCriteriaServiceUsuariosSaldos() {
		return findByCriteriaServiceUsuariosSaldos;
	}

	/**
	 * @return the idAreaSolicitantes
	 */
	public List getIdAreaSolicitantes() {
		return idAreaSolicitantes;
	}

	/**
	 * @return the idEstados
	 */
	public List getIdEstados() {
		return idEstados;
	}

	/**
	 * @return the idMotivoCongelacion
	 */
	public Long getIdMotivoCongelacion() {
		return idMotivoCongelacion;
	}

	/**
	 * @return the idSaldoSelected
	 */
	public Long getIdSaldoSelected() {
		return idSaldoSelected;
	}

	/**
	 * @return the idTipoCongelacion
	 */
	public String getIdTipoCongelacion() {
		return idTipoCongelacion;
	}

	/**
	 * @return the usuarioRealizador
	 */
	public String getUsuarioRealizador() {
		return usuarioRealizador;
	}

	/**
	 * @param cargoItems
	 *            the cargoItems to set
	 */
	public void setCargoItems(String cargoItems) {
		this.cargoItems = cargoItems;
	}

	/**
	 * @param cicloFacturacion
	 *            the cicloFacturacion to set
	 */
	public void setCicloFacturacion(String cicloFacturacion) {
		this.cicloFacturacion = cicloFacturacion;
	}

	/**
	 * @param claseServicio
	 *            the claseServicio to set
	 */
	public void setClaseServicio(String claseServicio) {
		this.claseServicio = claseServicio;
	}

	/**
	 * @param dataTableSaldos
	 *            the dataTableSaldos to set
	 */
	public void setDataTableSaldos(HtmlDataTable dataTableSaldos) {
		this.dataTableSaldos = dataTableSaldos;
	}

	/**
	 * @param dataTableUsuariosSaldos
	 *            the dataTableUsuariosSaldos to set
	 */
	public void setDataTableUsuariosSaldos(HtmlDataTable dataTableUsuariosSaldos) {
		this.dataTableUsuariosSaldos = dataTableUsuariosSaldos;
	}

	/**
	 * @param fechaDesde
	 *            the fechaDesde to set
	 */
	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	/**
	 * @param fechaHasta
	 *            the fechaHasta to set
	 */
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	/**
	 * @param findByCriteriaServiceSaldos
	 *            the findByCriteriaServiceSaldos to set
	 */
	public void setFindByCriteriaServiceSaldos(
			UIService findByCriteriaServiceSaldos) {
		this.findByCriteriaServiceSaldos = findByCriteriaServiceSaldos;
	}

	/**
	 * @param findByCriteriaServiceUsuariosSaldos
	 *            the findByCriteriaServiceUsuariosSaldos to set
	 */
	public void setFindByCriteriaServiceUsuariosSaldos(
			UIService findByCriteriaServiceUsuariosSaldos) {
		this.findByCriteriaServiceUsuariosSaldos = findByCriteriaServiceUsuariosSaldos;
	}

	/**
	 * @param idAreaSolicitantes
	 *            the idAreaSolicitantes to set
	 */
	public void setIdAreaSolicitantes(List idAreaSolicitantes) {
		this.idAreaSolicitantes = idAreaSolicitantes;
	}

	/**
	 * @param idEstados
	 *            the idEstados to set
	 */
	public void setIdEstados(List idEstados) {
		this.idEstados = idEstados;
	}

	/**
	 * @param idMotivoCongelacion
	 *            the idMotivoCongelacion to set
	 */
	public void setIdMotivoCongelacion(Long idMotivoCongelacion) {
		this.idMotivoCongelacion = idMotivoCongelacion;
	}

	/**
	 * @param idSaldoSelected
	 *            the idSaldoSelected to set
	 */
	public void setIdSaldoSelected(Long idSaldoSelected) {
		this.idSaldoSelected = idSaldoSelected;
	}

	/**
	 * @param idTipoCongelacion
	 *            the idTipoCongelacion to set
	 */
	public void setIdTipoCongelacion(String idTipoCongelacion) {
		this.idTipoCongelacion = idTipoCongelacion;
	}

	/**
	 * @param usuarioRealizador
	 *            the usuarioRealizador to set
	 */
	public void setUsuarioRealizador(String usuarioRealizador) {
		this.usuarioRealizador = usuarioRealizador;
	}

	public Long getIdMotivoDescongelacion() {
		return idMotivoDescongelacion;
	}

	public void setIdMotivoDescongelacion(Long idMotivoDescongelacion) {
		this.idMotivoDescongelacion = idMotivoDescongelacion;
	}

	public String getUsuarioCreadorDescongelacion() {
		return usuarioCreadorDescongelacion;
	}

	public void setUsuarioCreadorDescongelacion(String usuarioCreadorDescongelacion) {
		this.usuarioCreadorDescongelacion = usuarioCreadorDescongelacion;
	}
}
