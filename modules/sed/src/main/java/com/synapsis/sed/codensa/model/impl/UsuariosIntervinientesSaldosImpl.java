package com.synapsis.sed.codensa.model.impl;

import java.util.Date;

import com.synapsis.sed.codensa.model.UsuariosIntervinientesSaldos;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class UsuariosIntervinientesSaldosImpl extends SynergiaBusinessObjectImpl implements  UsuariosIntervinientesSaldos {
	private String usuarioInterviniente;
	private Date fechaIntervencion;
	private String accion;
	private String tiemposIntervencion;
	private Long idOrden;

	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientes#getAccion()
	 */
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientesSaldos#getAccion()
	 */
	public String getAccion() {
		return accion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientes#setAccion(java.lang.String)
	 */
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientesSaldos#setAccion(java.lang.String)
	 */
	public void setAccion(String accion) {
		this.accion = accion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientes#getFechaIntervencion()
	 */
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientesSaldos#getFechaIntervencion()
	 */
	public Date getFechaIntervencion() {
		return fechaIntervencion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientes#setFechaIntervencion(java.util.Date)
	 */
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientesSaldos#setFechaIntervencion(java.util.Date)
	 */
	public void setFechaIntervencion(Date fechaIntervencion) {
		this.fechaIntervencion = fechaIntervencion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientes#getUsuarioInterviniente()
	 */
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientesSaldos#getUsuarioInterviniente()
	 */
	public String getUsuarioInterviniente() {
		return usuarioInterviniente;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientes#setUsuarioInterviniente(java.lang.String)
	 */
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientesSaldos#setUsuarioInterviniente(java.lang.String)
	 */
	public void setUsuarioInterviniente(String usuarioInterviniente) {
		this.usuarioInterviniente = usuarioInterviniente;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.DatosUsuariosIntervinientes#getTiemposIntervencion()
	 */
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientesSaldos#getTiemposIntervencion()
	 */
	public String getTiemposIntervencion() {
		return tiemposIntervencion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.DatosUsuariosIntervinientes#setTiemposIntervencion(java.lang.String)
	 */
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientesSaldos#setTiemposIntervencion(java.lang.String)
	 */
	public void setTiemposIntervencion(String tiemposIntervencion) {
		this.tiemposIntervencion = tiemposIntervencion;
	}
	/**
	 * @return the idOrden
	 */
	public Long getIdOrden() {
		return idOrden;
	}
	/**
	 * @param idOrden the idOrden to set
	 */
	public void setIdOrden(Long idOrden) {
		this.idOrden = idOrden;
	}

	
}
