package com.synapsis.sed.codensa.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.event.ActionEvent;

import org.apache.myfaces.custom.service.UIService;

/**
 * Refactorizacion del modulo de SED a la nueva forma de trabajo con
 * modularizacion.
 * 
 * @author (refactor) dbraccio - Suivant
 * 
 */
public class ReporteOrdenesBB {

	private static final long serialVersionUID = 1L;

	// *****************************************************
	// ***************** properties del BB
	// *****************************************************

	// properties de los filtros de busqueda que ingresa el usuario
	private Date fechaCreacionDesde;
	private Date fechaCreacionHasta;
	private String usuarioCreador;
	private String motivo;
	private Long nroOrden;
	private String tipoOrden;
	private List idEstados = new ArrayList();
	private List idAreaSolicitantes = new ArrayList() ;
	private List idAreaAsignadas = new ArrayList() ;

	// property para armar el filtro de la Segunda tabla de la pantalla
	private Long idOrdenSelected;

	/**
	 * Tabla en la cual se dejan los resultados de la busqueda ralizada de las
	 * ordenes.
	 */
	private HtmlDataTable dataTableOrdenes;

	/**
	 * Tabla en la cual se dejan los resultados de la busqueda ralizada de
	 * usuarios.
	 */
	private HtmlDataTable dataTableUsuariosIntervinientes;

	/**
	 * Comment for <code>findByCriteriaService</code> Servicio para realizar
	 * la busqueda
	 */
	private UIService findByCriteriaServiceOrdenes;

	/**
	 * Comment for <code>findByCriteriaService</code> Servicio para realizar
	 * la busqueda
	 */
	private UIService findByCriteriaServiceUsuarios;

	// *******************************************************************
	// *************** actions de los BB **************************
	// *******************************************************************
	/**
	 * Action del BB que realiza la busqueda de las ordenes
	 */
	public void reporteOrdenes() {
		this.getFindByCriteriaServiceOrdenes().execute();
		this.getDataTableOrdenes().setFirst(0);
		// esta linea limpia la tabla de abajo de usuarios
		this.getFindByCriteriaServiceUsuarios().setValue(null);
	}

	/**
	 * Action del BB que refresca la tabla de abajo de usuarios
	 * 
	 * @param event
	 */
	public void usuariosIntervinientes(ActionEvent event) {
		this.getFindByCriteriaServiceUsuarios().execute();
		this.getDataTableUsuariosIntervinientes().setFirst(0);
	}

	// ************************************************************
	// *************** getter and setter **************************
	// ************************************************************

	/**
	 * @return the fechaCreacionDesde
	 */
	public Date getFechaCreacionDesde() {
		return fechaCreacionDesde;
	}

	/**
	 * @return the fechaCreacionHasta
	 */
	public Date getFechaCreacionHasta() {
		return fechaCreacionHasta;
	}

	/**
	 * @return the idAreaSolicitantes
	 */
	public List getIdAreaSolicitantes() {
		return idAreaSolicitantes;
	}

	/**
	 * @return the idEstados
	 */
	public List getIdEstados() {
		return idEstados;
	}

	/**
	 * @return the motivo
	 */
	public String getMotivo() {
		return motivo;
	}

	/**
	 * @return the nroOrden
	 */
	public Long getNroOrden() {
		return nroOrden;
	}

	/**
	 * @return the usuarioCreador
	 */
	public String getUsuarioCreador() {
		return usuarioCreador;
	}

	/**
	 * @return the idOrdenSelected
	 */
	public Long getIdOrdenSelected() {
		return idOrdenSelected;
	}

	/**
	 * @param idOrdenSelected
	 *            the idOrdenSelected to set
	 */
	public void setIdOrdenSelected(Long idOrdenSelected) {
		this.idOrdenSelected = idOrdenSelected;
	}

	/**
	 * @param fechaCreacionDesde
	 *            the fechaCreacionDesde to set
	 */
	public void setFechaCreacionDesde(Date fechaCreacionDesde) {
		this.fechaCreacionDesde = fechaCreacionDesde;
	}

	/**
	 * @param fechaCreacionHasta
	 *            the fechaCreacionHasta to set
	 */
	public void setFechaCreacionHasta(Date fechaCreacionHasta) {
		this.fechaCreacionHasta = fechaCreacionHasta;
	}

	/**
	 * @param idAreaSolicitantes
	 *            the idAreaSolicitantes to set
	 */
	public void setIdAreaSolicitantes(List idAreaSolicitantes) {
		this.idAreaSolicitantes = idAreaSolicitantes;
	}

	/**
	 * @param idEstados
	 *            the idEstados to set
	 */
	public void setIdEstados(List idEstados) {
		this.idEstados = idEstados;
	}

	/**
	 * @param motivo
	 *            the motivo to set
	 */
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	/**
	 * @param nroOrden
	 *            the nroOrden to set
	 */
	public void setNroOrden(Long nroOrden) {
		this.nroOrden = nroOrden;
	}

	/**
	 * @param usuarioCreador
	 *            the usuarioCreador to set
	 */
	public void setUsuarioCreador(String usuarioCreador) {
		this.usuarioCreador = usuarioCreador;
	}

	/**
	 * @return the dataTableOrdenes
	 */
	public HtmlDataTable getDataTableOrdenes() {
		return dataTableOrdenes;
	}

	/**
	 * @return the dataTableUsuariosIntervinientes
	 */
	public HtmlDataTable getDataTableUsuariosIntervinientes() {
		return dataTableUsuariosIntervinientes;
	}

	/**
	 * @return the findByCriteriaServiceOrdenes
	 */
	public UIService getFindByCriteriaServiceOrdenes() {
		return findByCriteriaServiceOrdenes;
	}

	/**
	 * @return the findByCriteriaServiceUsuarios
	 */
	public UIService getFindByCriteriaServiceUsuarios() {
		return findByCriteriaServiceUsuarios;
	}

	/**
	 * @param dataTableOrdenes
	 *            the dataTableOrdenes to set
	 */
	public void setDataTableOrdenes(HtmlDataTable dataTableOrdenes) {
		this.dataTableOrdenes = dataTableOrdenes;
	}

	/**
	 * @param dataTableUsuariosIntervinientes
	 *            the dataTableUsuariosIntervinientes to set
	 */
	public void setDataTableUsuariosIntervinientes(
			HtmlDataTable dataTableUsuariosIntervinientes) {
		this.dataTableUsuariosIntervinientes = dataTableUsuariosIntervinientes;
	}

	/**
	 * @param findByCriteriaServiceOrdenes
	 *            the findByCriteriaServiceOrdenes to set
	 */
	public void setFindByCriteriaServiceOrdenes(
			UIService findByCriteriaServiceOrdenes) {
		this.findByCriteriaServiceOrdenes = findByCriteriaServiceOrdenes;
	}

	/**
	 * @param findByCriteriaServiceUsuarios
	 *            the findByCriteriaServiceUsuarios to set
	 */
	public void setFindByCriteriaServiceUsuarios(
			UIService findByCriteriaServiceUsuarios) {
		this.findByCriteriaServiceUsuarios = findByCriteriaServiceUsuarios;
	}

	public String getTipoOrden() {
		return tipoOrden;
	}

	public void setTipoOrden(String tipoOrden) {
		this.tipoOrden = tipoOrden;
	}


	public List getIdAreaAsignadas() {
		return idAreaAsignadas;
	}

	public void setIdAreaAsignadas(List idAreaAsignadas) {
		this.idAreaAsignadas = idAreaAsignadas;
	}
	
	

}
