package com.synapsis.sed.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ReporteSaldosDisputaTotales extends SynergiaBusinessObject {

	public abstract Double getEnergiaTotalItemsAmortizadosAjuste();

	public abstract void setEnergiaTotalItemsAmortizadosAjuste(
			Double energiaTotalItemsAmortizadosAjuste);

	public abstract Double getEnergiaTotalItemsAmortizadosPago();

	public abstract void setEnergiaTotalItemsAmortizadosPago(
			Double energiaTotalItemsAmortizadosPago);

	public abstract Double getEnergiaTotalItemsNoAmortizados();

	public abstract void setEnergiaTotalItemsNoAmortizados(
			Double energiaTotalItemsNoAmortizados);

	public abstract Double getEnergiaTotalSed();

	public abstract void setEnergiaTotalSed(Double energiaTotalSed);

	public abstract Double getTotalesCantidadOperacionesSed();

	public abstract void setTotalesCantidadOperacionesSed(
			Double totalesCantidadOperacionesSed);

	public abstract Double getValorTotalItemsAmortizadosAjuste();

	public abstract void setValorTotalItemsAmortizadosAjuste(
			Double valorTotalItemsAmortizadosAjuste);

	public abstract Double getValorTotalItemsAmortizadosPago();

	public abstract void setValorTotalItemsAmortizadosPago(
			Double valorTotalItemsAmortizadosPago);

	public abstract Double getValorTotalItemsNoAmortizados();

	public abstract void setValorTotalItemsNoAmortizados(
			Double valorTotalItemsNoAmortizados);

	public abstract Double getValorTotalSed();

	public abstract void setValorTotalSed(Double valorTotalSed);

	
	public String getCargoItems();
	
	public void setCargoItems(String cargoItems);
	
	public String getClaseServicio();
	
	public void setClaseServicio(String claseServicio);
	
	public String getEnergiaSED();
	
	public void setEnergiaSED(String energiaSED);
	
	public Date getFechaFinalizacion();
	
	public void setFechaFinalizacion(Date fechaFinalizacion);
	
	public Long getIdMotivoCongelacion();
	
	public void setIdMotivoCongelacion(Long idMotivoCongelacion);
	
	public Long getIdMotivoDescongelacion();
	
	public void setIdMotivoDescongelacion(Long idMotivoDescongelacion);
	
	public String getIdTipoCongelacion();
	
	public void setIdTipoCongelacion(String idTipoCongelacion);
	
	public String getUsuarioCreadorDescongelacion();
	
	public void setUsuarioCreadorDescongelacion(String usuarioCreadorDescongelacion);
	
	public String getUsuarioRealizador();
	
	public void setUsuarioRealizador(String usuarioRealizador);
		
}