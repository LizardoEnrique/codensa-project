package com.synapsis.sed.codensa.model.impl;

import java.util.Date;

import com.synapsis.sed.codensa.model.UsuariosIntervinientesOrden;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class UsuariosIntervinientesOrdenImpl extends SynergiaBusinessObjectImpl implements UsuariosIntervinientesOrden {
	private String usuarioInterviniente;
	private Date fechaIntervencion;
	private String accion;
	private String tiemposIntervencion;
	private Long idOrden;
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientes#getAccion()
	 */
	public String getAccion() {
		return accion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientes#setAccion(java.lang.String)
	 */
	public void setAccion(String accion) {
		this.accion = accion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientes#getFechaIntervencion()
	 */
	public Date getFechaIntervencion() {
		return fechaIntervencion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientes#setFechaIntervencion(java.util.Date)
	 */
	public void setFechaIntervencion(Date fechaIntervencion) {
		this.fechaIntervencion = fechaIntervencion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientes#getUsuarioInterviniente()
	 */
	public String getUsuarioInterviniente() {
		return usuarioInterviniente;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientes#setUsuarioInterviniente(java.lang.String)
	 */
	public void setUsuarioInterviniente(String usuarioInterviniente) {
		this.usuarioInterviniente = usuarioInterviniente;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.DatosUsuariosIntervinientes#getTiemposIntervencion()
	 */
	public String getTiemposIntervencion() {
		return tiemposIntervencion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.DatosUsuariosIntervinientes#setTiemposIntervencion(java.lang.String)
	 */
	public void setTiemposIntervencion(String tiemposIntervencion) {
		this.tiemposIntervencion = tiemposIntervencion;
	}
	public Long getIdOrden() {
		return idOrden;
	}
	public void setIdOrden(Long idOrden) {
		this.idOrden = idOrden;
	}

	
}
