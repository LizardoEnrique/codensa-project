/**
 * $Id: ReporteSaldosDisputaImpl.java,v 1.7 2008/07/16 21:45:34 ar26557682 Exp $
 */
package com.synapsis.sed.codensa.model.impl;

import java.util.Date;

import com.synapsis.sed.codensa.model.ReporteSaldosDisputa;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ReporteSaldosDisputaImpl extends SynergiaBusinessObjectImpl implements ReporteSaldosDisputa {
	
	private static final long serialVersionUID = 1L;
	
	
	private Long nroSED;
	private String tipoCongelacion;
	private String cicloFacturacion;
	private Date fechaEntraFacturacion;
	private Long nroCuenta;
	private String nroOrdenSED;
	private String nroAtencion;
	private String usuarioRealizador;
	private String usuarioCreadorDescongelacion;
	private String areaRealizadora;
	private Date fechaRealizacion;
	private Date fechaFinalizacion;
	private Date fechaRealizacionDescongelacion;
	private Date fechaAprobacionCongelacion;
	private String usuarioAprobador;	
	private String estadoSED;
	private String motivoCongelacion;
	private String motivoDescongelacion;
	private String valorSED;
	private String energiaSED;
	private Double valorTotalItemsNoAmortizados;
	private Double energiaItemsDisputadosNoAmortizados;
	private String valorTotalItemsDisputadosAmortizadosAjusteReliq;
	private String energiaItemsAmortizadosAjusteReliq;
	private String valorTotalItemsDisputadosAmortizadosPorPago;	
	private String energiaItemsAmortizadosPorPago;	
	private String tiempoTranscurridoCreacionAprobacionFinal;	
	private Date fechaAmpliacionTerminos;	
	private Date fechaVencimientoCongelacionTransitoria;	
	private String diasAmpliados;
	private Long idAreaSolicitante;
	private String idEstado;
	private String cargoItems;
	private String cargoItemsAmortAjusteReliq;
	private String valoresDisputaAmortizados;
	private String claseServicio;
	private String cantidadSED;
	private Long idUsuarioGestor;
	private Long idMotivoCongelacion;
	private Long idMotivoDescongelacion;
	private String cargoItemsAclaracion;
	private String idTipoCongelacion;
	
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getAreaRealizadora()
	 */
	public String getAreaRealizadora() {
		return areaRealizadora;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setAreaRealizadora(java.lang.String)
	 */
	public void setAreaRealizadora(String areaRealizadora) {
		this.areaRealizadora = areaRealizadora;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getCicloFacturacion()
	 */
	public String getCicloFacturacion() {
		return cicloFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setCicloFacturacion(java.lang.String)
	 */
	public void setCicloFacturacion(String cicloFacturacion) {
		this.cicloFacturacion = cicloFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getDiasAmpliados()
	 */
	public String getDiasAmpliados() {
		return diasAmpliados;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setDiasAmpliados(java.lang.String)
	 */
	public void setDiasAmpliados(String diasAmpliados) {
		this.diasAmpliados = diasAmpliados;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getEnergiaItemsAmortizadosAjusteReliq()
	 */
	public String getEnergiaItemsAmortizadosAjusteReliq() {
		return energiaItemsAmortizadosAjusteReliq;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setEnergiaItemsAmortizadosAjusteReliq(java.lang.String)
	 */
	public void setEnergiaItemsAmortizadosAjusteReliq(String energiaItemsAmortizadosAjusteReliq) {
		this.energiaItemsAmortizadosAjusteReliq = energiaItemsAmortizadosAjusteReliq;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getEnergiaItemsAmortizadosPorPago()
	 */
	public String getEnergiaItemsAmortizadosPorPago() {
		return energiaItemsAmortizadosPorPago;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setEnergiaItemsAmortizadosPorPago(java.lang.String)
	 */
	public void setEnergiaItemsAmortizadosPorPago(String energiaItemsAmortizadosPorPago) {
		this.energiaItemsAmortizadosPorPago = energiaItemsAmortizadosPorPago;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getEnergiaItemsDisputadosNoAmortizados()
	 */
	public Double getEnergiaItemsDisputadosNoAmortizados() {
		return energiaItemsDisputadosNoAmortizados;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setEnergiaItemsDisputadosNoAmortizados(java.lang.String)
	 */
	public void setEnergiaItemsDisputadosNoAmortizados(Double energiaItemsDisputadosNoAmortizados) {
		this.energiaItemsDisputadosNoAmortizados = energiaItemsDisputadosNoAmortizados;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getEnergiaSED()
	 */
	public String getEnergiaSED() {
		return energiaSED;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setEnergiaSED(java.lang.String)
	 */
	public void setEnergiaSED(String energiaSED) {
		this.energiaSED = energiaSED;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getEstadoSED()
	 */
	public String getEstadoSED() {
		return estadoSED;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setEstadoSED(java.lang.String)
	 */
	public void setEstadoSED(String estadoSED) {
		this.estadoSED = estadoSED;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getFechaAmpliacionTerminos()
	 */
	public Date getFechaAmpliacionTerminos() {
		return fechaAmpliacionTerminos;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setFechaAmpliacionTerminos(java.util.Date)
	 */
	public void setFechaAmpliacionTerminos(Date fechaAmpliacionTerminos) {
		this.fechaAmpliacionTerminos = fechaAmpliacionTerminos;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getFechaAprobacionCongelacion()
	 */
	public Date getFechaAprobacionCongelacion() {
		return fechaAprobacionCongelacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setFechaAprobacionCongelacion(java.util.Date)
	 */
	public void setFechaAprobacionCongelacion(Date fechaAprobacionCongelacion) {
		this.fechaAprobacionCongelacion = fechaAprobacionCongelacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getFechaEntraFacturacion()
	 */
	public Date getFechaEntraFacturacion() {
		return fechaEntraFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setFechaEntraFacturacion(java.util.Date)
	 */
	public void setFechaEntraFacturacion(Date fechaEntraFacturacion) {
		this.fechaEntraFacturacion = fechaEntraFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getFechaRealizacion()
	 */
	public Date getFechaRealizacion() {
		return fechaRealizacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setFechaRealizacion(java.util.Date)
	 */
	public void setFechaRealizacion(Date fechaRealizacion) {
		this.fechaRealizacion = fechaRealizacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getFechaRealizacionDescongelacion()
	 */
	public Date getFechaRealizacionDescongelacion() {
		return fechaRealizacionDescongelacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setFechaRealizacionDescongelacion(java.util.Date)
	 */
	public void setFechaRealizacionDescongelacion(
			Date fechaRealizacionDescongelacion) {
		this.fechaRealizacionDescongelacion = fechaRealizacionDescongelacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getFechaVencimientoCongelacionTransitoria()
	 */
	public Date getFechaVencimientoCongelacionTransitoria() {
		return fechaVencimientoCongelacionTransitoria;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setFechaVencimientoCongelacionTransitoria(java.util.Date)
	 */
	public void setFechaVencimientoCongelacionTransitoria(
			Date fechaVencimientoCongelacionTransitoria) {
		this.fechaVencimientoCongelacionTransitoria = fechaVencimientoCongelacionTransitoria;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getMotivoCongelacion()
	 */
	public String getMotivoCongelacion() {
		return motivoCongelacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setMotivoCongelacion(java.lang.String)
	 */
	public void setMotivoCongelacion(String motivoCongelacion) {
		this.motivoCongelacion = motivoCongelacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getMotivoDescongelacion()
	 */
	public String getMotivoDescongelacion() {
		return motivoDescongelacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setMotivoDescongelacion(java.lang.String)
	 */
	public void setMotivoDescongelacion(String motivoDescongelacion) {
		this.motivoDescongelacion = motivoDescongelacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getNroAtencion()
	 */
	public String getNroAtencion() {
		return nroAtencion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setNroAtencion(java.lang.Long)
	 */
	public void setNroAtencion(String nroAtencion) {
		this.nroAtencion = nroAtencion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getNroOrdenSED()
	 */
	public String getNroOrdenSED() {
		return nroOrdenSED;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setNroOrdenSED(java.lang.Long)
	 */
	public void setNroOrdenSED(String nroOrdenSED) {
		this.nroOrdenSED = nroOrdenSED;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getNroSED()
	 */
	public Long getNroSED() {
		return nroSED;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setNroSED(java.lang.Long)
	 */
	public void setNroSED(Long nroSED) {
		this.nroSED = nroSED;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getTiempoTranscurridoCreacionAprobacionFinal()
	 */
	public String getTiempoTranscurridoCreacionAprobacionFinal() {
		return tiempoTranscurridoCreacionAprobacionFinal;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setTiempoTranscurridoCreacionAprobacionFinal(java.lang.String)
	 */
	public void setTiempoTranscurridoCreacionAprobacionFinal(
			String tiempoTranscurridoCreacionAprobacionFinal) {
		this.tiempoTranscurridoCreacionAprobacionFinal = tiempoTranscurridoCreacionAprobacionFinal;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getTipoCongelacion()
	 */
	public String getTipoCongelacion() {
		return tipoCongelacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setTipoCongelacion(java.lang.String)
	 */
	public void setTipoCongelacion(String tipoCongelacion) {
		this.tipoCongelacion = tipoCongelacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getUsuarioAprobador()
	 */
	public String getUsuarioAprobador() {
		return usuarioAprobador;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setUsuarioAprobador(java.lang.String)
	 */
	public void setUsuarioAprobador(String usuarioAprobador) {
		this.usuarioAprobador = usuarioAprobador;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getUsuarioRealizador()
	 */
	public String getUsuarioRealizador() {
		return usuarioRealizador;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setUsuarioRealizador(java.lang.String)
	 */
	public void setUsuarioRealizador(String usuarioRealizador) {
		this.usuarioRealizador = usuarioRealizador;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getValorSED()
	 */
	public String getValorSED() {
		return valorSED;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setValorSED(java.lang.String)
	 */
	public void setValorSED(String valorSED) {
		this.valorSED = valorSED;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getValorTotalItemsDisputadosAmortizadosAjusteReliq()
	 */
	public String getValorTotalItemsDisputadosAmortizadosAjusteReliq() {
		return valorTotalItemsDisputadosAmortizadosAjusteReliq;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setValorTotalItemsDisputadosAmortizadosAjusteReliq(Double)
	 */			
	public void setValorTotalItemsDisputadosAmortizadosAjusteReliq(
			String valorTotalItemsDisputadosAmortizadosAjusteReliq) {
		this.valorTotalItemsDisputadosAmortizadosAjusteReliq = valorTotalItemsDisputadosAmortizadosAjusteReliq;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getValorTotalItemsDisputadosAmortizadosPorPago()
	 */
	public String getValorTotalItemsDisputadosAmortizadosPorPago() {
		return valorTotalItemsDisputadosAmortizadosPorPago;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setValorTotalItemsDisputadosAmortizadosPorPago(Double)
	 */
	public void setValorTotalItemsDisputadosAmortizadosPorPago(
			String valorTotalItemsDisputadosAmortizadosPorPago) {
		this.valorTotalItemsDisputadosAmortizadosPorPago = valorTotalItemsDisputadosAmortizadosPorPago;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#getValorTotalItemsNoAmortizados()
	 */
	public Double getValorTotalItemsNoAmortizados() {
		return valorTotalItemsNoAmortizados;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.ReporteSaldosDisputa#setValorTotalItemsNoAmortizados(Double)
	 */
	public void setValorTotalItemsNoAmortizados(Double valorTotalItemsNoAmortizados) {
		this.valorTotalItemsNoAmortizados = valorTotalItemsNoAmortizados;
	}

	public Long getIdAreaSolicitante() {
		return idAreaSolicitante;
	}
	public void setIdAreaSolicitante(Long idAreaSolicitante) {
		this.idAreaSolicitante = idAreaSolicitante;
	}
	public String getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}
	public String getCantidadSED() {
		return cantidadSED;
	}
	public void setCantidadSED(String cantidadSED) {
		this.cantidadSED = cantidadSED;
	}
	public String getCargoItems() {
		return cargoItems;
	}
	public void setCargoItems(String cargoItems) {
		this.cargoItems = cargoItems;
	}
	public String getCargoItemsAmortAjusteReliq() {
		return cargoItemsAmortAjusteReliq;
	}
	public void setCargoItemsAmortAjusteReliq(String cargoItemsAmortAjusteReliq) {
		this.cargoItemsAmortAjusteReliq = cargoItemsAmortAjusteReliq;
	}
	public String getClaseServicio() {
		return claseServicio;
	}
	public void setClaseServicio(String claseServicio) {
		this.claseServicio = claseServicio;
	}
	public String getValoresDisputaAmortizados() {
		return valoresDisputaAmortizados;
	}
	public void setValoresDisputaAmortizados(String valoresDisputaAmortizados) {
		this.valoresDisputaAmortizados = valoresDisputaAmortizados;
	}
	public Long getIdUsuarioGestor() {
		return idUsuarioGestor;
	}
	public void setIdUsuarioGestor(Long idUsuarioGestor) {
		this.idUsuarioGestor = idUsuarioGestor;
	}
	public Long getIdMotivoCongelacion() {
		return idMotivoCongelacion;
	}
	public void setIdMotivoCongelacion(Long idMotivoCongelacion) {
		this.idMotivoCongelacion = idMotivoCongelacion;
	}
	public String getCargoItemsAclaracion() {
		return cargoItemsAclaracion;
	}
	public void setCargoItemsAclaracion(String cargoItemsAclaracion) {
		this.cargoItemsAclaracion = cargoItemsAclaracion;
	}
	/**
	 * @return the idTipoCongelacion
	 */
	public String getIdTipoCongelacion() {
		return idTipoCongelacion;
	}
	/**
	 * @param idTipoCongelacion the idTipoCongelacion to set
	 */
	public void setIdTipoCongelacion(String idTipoCongelacion) {
		this.idTipoCongelacion = idTipoCongelacion;
	}
	public Date getFechaFinalizacion() {
		return fechaFinalizacion;
	}
	public void setFechaFinalizacion(Date fechaFinalizacion) {
		this.fechaFinalizacion = fechaFinalizacion;
	}
	public Long getIdMotivoDescongelacion() {
		return idMotivoDescongelacion;
	}
	public void setIdMotivoDescongelacion(Long idMotivoDescongelacion) {
		this.idMotivoDescongelacion = idMotivoDescongelacion;
	}
	public String getUsuarioCreadorDescongelacion() {
		return usuarioCreadorDescongelacion;
	}
	public void setUsuarioCreadorDescongelacion(String usuarioCreadorDescongelacion) {
		this.usuarioCreadorDescongelacion = usuarioCreadorDescongelacion;
	}	
}
