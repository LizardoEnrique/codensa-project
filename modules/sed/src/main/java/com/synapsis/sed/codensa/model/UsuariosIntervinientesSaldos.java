package com.synapsis.sed.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface UsuariosIntervinientesSaldos extends SynergiaBusinessObject {

	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientes#getAccion()
	 */
	public abstract String getAccion();

	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientes#setAccion(java.lang.String)
	 */
	public abstract void setAccion(String accion);

	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientes#getFechaIntervencion()
	 */
	public abstract Date getFechaIntervencion();

	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientes#setFechaIntervencion(java.util.Date)
	 */
	public abstract void setFechaIntervencion(Date fechaIntervencion);

	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientes#getUsuarioInterviniente()
	 */
	public abstract String getUsuarioInterviniente();

	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.UsuariosIntervinientes#setUsuarioInterviniente(java.lang.String)
	 */
	public abstract void setUsuarioInterviniente(String usuarioInterviniente);

	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.DatosUsuariosIntervinientes#getTiemposIntervencion()
	 */
	public abstract String getTiemposIntervencion();

	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.DatosUsuariosIntervinientes#setTiemposIntervencion(java.lang.String)
	 */
	public abstract void setTiemposIntervencion(String tiemposIntervencion);

	public Long getIdOrden();
	/**
	 * @param idOrden the idOrden to set
	 */
	public void setIdOrden(Long idOrden);
}