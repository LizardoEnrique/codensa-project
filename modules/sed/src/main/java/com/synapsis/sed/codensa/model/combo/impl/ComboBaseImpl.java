
package com.synapsis.sed.codensa.model.combo.impl;

import com.synapsis.sed.codensa.model.combo.Combo;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;


public class ComboBaseImpl extends SynergiaBusinessObjectImpl
		implements Combo {
	private static final long serialVersionUID = 1L;

	private String codigo;

	private String descripcion;

	public ComboBaseImpl() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.combo.impl.ComboLineaNegocio#getCodigo()
	 */
	public String getCodigo() {
		return codigo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.combo.impl.ComboLineaNegocio#setCodigo(java.lang.String)
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.combo.impl.ComboLineaNegocio#getDescripcion()
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.eco.codensa.model.combo.impl.ComboLineaNegocio#setDescripcion(java.lang.String)
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}