package com.synapsis.sed.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface UsuariosIntervinientesOrden extends SynergiaBusinessObject {

	public abstract String getAccion();

	public abstract void setAccion(String accion);

	public abstract Date getFechaIntervencion();

	public abstract void setFechaIntervencion(Date fechaIntervencion);

	public abstract String getUsuarioInterviniente();

	public abstract void setUsuarioInterviniente(String usuarioInterviniente);

	/**
	 * @return the tiemposIntervencion
	 */
	public abstract String getTiemposIntervencion();

	/**
	 * @param tiemposIntervencion the tiemposIntervencion to set
	 */
	public abstract void setTiemposIntervencion(String tiemposIntervencion);

	public Long getIdOrden();
	public void setIdOrden(Long idOrden);

}