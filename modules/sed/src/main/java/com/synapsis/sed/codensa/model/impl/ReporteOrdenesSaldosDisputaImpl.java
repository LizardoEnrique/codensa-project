/**
 * 
 */
package com.synapsis.sed.codensa.model.impl;

import java.util.Date;

import com.synapsis.sed.codensa.model.ReporteOrdenesSaldosDisputa;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 *
 */
public class ReporteOrdenesSaldosDisputaImpl extends SynergiaBusinessObjectImpl implements ReporteOrdenesSaldosDisputa {

	private static final long serialVersionUID = 1L;
	
	private Long nroOrden;
	private String tipoOrden;
	private String estado; 	
	private String areaSolicitante;	
	private String motivo;	
	private Long nroCuenta;	
	private String cicloFacturacion;	
	private Date fechaCiclo;	
	private Date fechaCreacion;	
	private String usuarioCreador;	
	private Long tiempoTranscurridoCreacionHastaAprobacion;	
	private Long nroSaldoDisputa;	
	private String estadoSed;	
	private Long valorSed;	
	private String energia;
	private Long idAreaSolicitante;
	private Long idAreaAsignada;
	private String idEstado;
	
	
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#getAreaSolicitante()
	 */
	public String getAreaSolicitante() {
		return areaSolicitante;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#setAreaSolicitante(java.lang.String)
	 */
	public void setAreaSolicitante(String areaSolicitante) {
		this.areaSolicitante = areaSolicitante;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#getCicloFacturacion()
	 */
	public String getCicloFacturacion() {
		return cicloFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#setCicloFacturacion(java.lang.String)
	 */
	public void setCicloFacturacion(String cicloFacturacion) {
		this.cicloFacturacion = cicloFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#getEnergia()
	 */
	public String getEnergia() {
		return energia;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#setEnergia(java.lang.String)
	 */
	public void setEnergia(String energia) {
		this.energia = energia;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#getEstado()
	 */
	public String getEstado() {
		return estado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#setEstado(java.lang.String)
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#getEstadoSed()
	 */
	public String getEstadoSed() {
		return estadoSed;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#setEstadoSed(java.lang.String)
	 */
	public void setEstadoSed(String estadoSed) {
		this.estadoSed = estadoSed;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#getFechaCiclo()
	 */
	public Date getFechaCiclo() {
		return fechaCiclo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#setFechaCiclo(java.util.Date)
	 */
	public void setFechaCiclo(Date fechaCiclo) {
		this.fechaCiclo = fechaCiclo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#getFechaCreacion()
	 */
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#setFechaCreacion(java.util.Date)
	 */
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#getMotivo()
	 */
	public String getMotivo() {
		return motivo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#setMotivo(java.lang.String)
	 */
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#getNroOrden()
	 */
	public Long getNroOrden() {
		return nroOrden;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#setNroOrden(java.lang.Long)
	 */
	public void setNroOrden(Long nroOrden) {
		this.nroOrden = nroOrden;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#getNroSaldoDisputa()
	 */
	public Long getNroSaldoDisputa() {
		return nroSaldoDisputa;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#setNroSaldoDisputa(java.lang.Long)
	 */
	public void setNroSaldoDisputa(Long nroSaldoDisputa) {
		this.nroSaldoDisputa = nroSaldoDisputa;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#getTiempoTranscurridoCreacionHastaAprobacion()
	 */
	public Long getTiempoTranscurridoCreacionHastaAprobacion() {
		return tiempoTranscurridoCreacionHastaAprobacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#setTiempoTranscurridoCreacionHastaAprobacion(java.lang.Long)
	 */
	public void setTiempoTranscurridoCreacionHastaAprobacion(
			Long tiempoTranscurridoCreacionHastaAprobacion) {
		this.tiempoTranscurridoCreacionHastaAprobacion = tiempoTranscurridoCreacionHastaAprobacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#getTipoOrden()
	 */
	public String getTipoOrden() {
		return tipoOrden;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#setTipoOrden(java.lang.String)
	 */
	public void setTipoOrden(String tipoOrden) {
		this.tipoOrden = tipoOrden;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#getUsuarioCreador()
	 */
	public String getUsuarioCreador() {
		return usuarioCreador;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#setUsuarioCreador(java.lang.String)
	 */
	public void setUsuarioCreador(String usuarioCreador) {
		this.usuarioCreador = usuarioCreador;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#getValorSed()
	 */
	public Long getValorSed() {
		return valorSed;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.sed.codensa.model.impl.ReporteOrdenesSaldosDisputa#setValorSed(java.lang.Long)
	 */
	public void setValorSed(Long valorSed) {
		this.valorSed = valorSed;
	}
	public Long getIdAreaSolicitante() {
		return idAreaSolicitante;
	}
	public void setIdAreaSolicitante(Long idAreaSolicitante) {
		this.idAreaSolicitante = idAreaSolicitante;
	}
	public String getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}
	public Long getIdAreaAsignada() {
		return idAreaAsignada;
	}
	public void setIdAreaAsignada(Long idAreaAsignada) {
		this.idAreaAsignada = idAreaAsignada;
	}
	
	
	
}