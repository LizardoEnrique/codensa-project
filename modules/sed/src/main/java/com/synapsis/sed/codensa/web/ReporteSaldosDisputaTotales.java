package com.synapsis.sed.codensa.web;

public class ReporteSaldosDisputaTotales {
	private Double valorTotalSed = null;

	private Double energiaTotalSed = null;

	private Double valorTotalItemsNoAmortizados = null;

	private Double energiaTotalItemsNoAmortizados = null;

	private Double valorTotalItemsAmortizadosAjuste = null;

	private Double energiaTotalItemsAmortizadosAjuste = null;

	private Double valorTotalItemsAmortizadosPago = null;

	private Double energiaTotalItemsAmortizadosPago = null;

	private Double totalesCantidadOperacionesSed = null;

	public Double getEnergiaTotalItemsAmortizadosAjuste() {
		return energiaTotalItemsAmortizadosAjuste;
	}

	public void setEnergiaTotalItemsAmortizadosAjuste(
			Double energiaTotalItemsAmortizadosAjuste) {
		this.energiaTotalItemsAmortizadosAjuste = energiaTotalItemsAmortizadosAjuste;
	}

	public Double getEnergiaTotalItemsAmortizadosPago() {
		return energiaTotalItemsAmortizadosPago;
	}

	public void setEnergiaTotalItemsAmortizadosPago(
			Double energiaTotalItemsAmortizadosPago) {
		this.energiaTotalItemsAmortizadosPago = energiaTotalItemsAmortizadosPago;
	}

	public Double getEnergiaTotalItemsNoAmortizados() {
		return energiaTotalItemsNoAmortizados;
	}

	public void setEnergiaTotalItemsNoAmortizados(
			Double energiaTotalItemsNoAmortizados) {
		this.energiaTotalItemsNoAmortizados = energiaTotalItemsNoAmortizados;
	}

	public Double getEnergiaTotalSed() {
		return energiaTotalSed;
	}

	public void setEnergiaTotalSed(Double energiaTotalSed) {
		this.energiaTotalSed = energiaTotalSed;
	}

	public Double getTotalesCantidadOperacionesSed() {
		return totalesCantidadOperacionesSed;
	}

	public void setTotalesCantidadOperacionesSed(
			Double totalesCantidadOperacionesSed) {
		this.totalesCantidadOperacionesSed = totalesCantidadOperacionesSed;
	}

	public Double getValorTotalItemsAmortizadosAjuste() {
		return valorTotalItemsAmortizadosAjuste;
	}

	public void setValorTotalItemsAmortizadosAjuste(
			Double valorTotalItemsAmortizadosAjuste) {
		this.valorTotalItemsAmortizadosAjuste = valorTotalItemsAmortizadosAjuste;
	}

	public Double getValorTotalItemsAmortizadosPago() {
		return valorTotalItemsAmortizadosPago;
	}

	public void setValorTotalItemsAmortizadosPago(
			Double valorTotalItemsAmortizadosPago) {
		this.valorTotalItemsAmortizadosPago = valorTotalItemsAmortizadosPago;
	}

	public Double getValorTotalItemsNoAmortizados() {
		return valorTotalItemsNoAmortizados;
	}

	public void setValorTotalItemsNoAmortizados(
			Double valorTotalItemsNoAmortizados) {
		this.valorTotalItemsNoAmortizados = valorTotalItemsNoAmortizados;
	}

	public Double getValorTotalSed() {
		return valorTotalSed;
	}

	public void setValorTotalSed(Double valorTotalSed) {
		this.valorTotalSed = valorTotalSed;
	}

}
