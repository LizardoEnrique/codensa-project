package com.synapsis.sed.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface DatosUsuariosIntervinientes extends SynergiaBusinessObject {

	/**
	 * @return the accionIntervencion
	 */
	public abstract String getAccionIntervencion();

	/**
	 * @param accionIntervencion the accionIntervencion to set
	 */
	public abstract void setAccionIntervencion(String accionIntervencion);

	/**
	 * @return the fechaIntervencion
	 */
	public abstract Date getFechaIntervencion();

	/**
	 * @param fechaIntervencion the fechaIntervencion to set
	 */
	public abstract void setFechaIntervencion(Date fechaIntervencion);

	/**
	 * @return the tiemposIntervencion
	 */
	public abstract String getTiemposIntervencion();

	/**
	 * @param tiemposIntervencion the tiemposIntervencion to set
	 */
	public abstract void setTiemposIntervencion(String tiemposIntervencion);

	/**
	 * @return the usuariosIntervinientes
	 */
	public abstract String getUsuariosIntervinientes();

	/**
	 * @param usuariosIntervinientes the usuariosIntervinientes to set
	 */
	public abstract void setUsuariosIntervinientes(String usuariosIntervinientes);

}