package com.synapsis.sed.codensa.model.listbox.impl;

import com.synapsis.sed.codensa.model.listbox.ListBox;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ListBoxBaseImpl extends SynergiaBusinessObjectImpl implements
		ListBox {

	private static final long serialVersionUID = 1L;

	private String codigo;

	private String descripcion;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
