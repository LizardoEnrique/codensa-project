/*$Id: SedCodensaTestSuite.java,v 1.5 2007/05/28 15:23:59 ar29261698 Exp $*/
package com.synapsis.sed.codensa;



import junit.framework.Test;
import junit.framework.TestSuite;

import com.synapsis.sed.codensa.service.ReporteOrdenesSaldosDisputaServiceTest;
import com.synapsis.sed.codensa.service.UsuariosIntervinientesOrdenImplServiceTest;
import com.synapsis.sed.codensa.service.UsuariosIntervinientesSaldosServiceTest;

/**
 * @autor: chino
 * @date: 07/03/2007
 */
public class SedCodensaTestSuite {
       public static String SPRING_TEST_CONFIG="META-INF/sed/spring/*.xml";

        public static Test suite(){
		    TestSuite suite = new TestSuite();
			suite.addTestSuite(UsuariosIntervinientesSaldosServiceTest.class);
			suite.addTestSuite(UsuariosIntervinientesOrdenImplServiceTest.class);
			//suite.addTestSuite(ReporteSaldosDisputaServiceTest.class);
			suite.addTestSuite(ReporteOrdenesSaldosDisputaServiceTest.class);
            return suite;
        }
}
