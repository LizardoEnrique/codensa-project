/**
 * 
 */
package com.synapsis.sed.codensa;

import com.suivant.arquitectura.support.tests.service.SpringFinderServiceTestCase;

/**
 * @author ar18799631
 *
 */
public abstract class SedCodensaFinderServiceTest extends SpringFinderServiceTestCase {

	public SedCodensaFinderServiceTest(){
		super(SedCodensaTestSuite.SPRING_TEST_CONFIG);
	}
	
}
