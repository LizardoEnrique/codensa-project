/**
 * $Id: CarteraMorosaImpl.java,v 1.7 2008/05/08 21:19:44 ar32272876 Exp $
 */
package com.synapsis.mor.cca.codensa.model.impl;

import java.util.Date;

import com.synapsis.mor.cca.codensa.model.CarteraMorosa;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * Modela el reporte y actualmente est� mapeado a una Vista
 * que realiza la consulta
 * 
 * @author Paola Attadio
 * @version $Revision: 1.7 $
 * 
 */
public class CarteraMorosaImpl extends SynergiaBusinessObjectImpl implements
		CarteraMorosa {

	private static final long serialVersionUID = 1L;

	private String lineaNegocio;
	private Long idLineaNegocio;// no en la tabla
	private String planServicioFinanciero;
	private Long idPlanServicioFinanciero;// no en la tabla
	private String socioNegocio;
	private Long idSocioNegocio;// no en la tabla
	private String sucursalSocioNegocio;
	private Long nroCuenta;
	private Long idCuenta;// no en la tabla
	private Long nroServicioCompraCartera;
	private Long idTipoIdentifPersonaRelacionada;// no en la tabla
	private String tipoIdentifPersonaRelacionada; // CODIGO: dni, etc ...
	private Long idIdentifPersonaRelacionada;// no en la tabla
	private String identifPersonaRelacionada;
	private String estadoServicioCompra;
	private Date fechaUltimoCambioEstado;
	private Date fechaCompra;
	private Date fechaCreacion;
	private Long valorCredito;
	private Long tasaInteres;
	private Long nroCuotas;
	private Long nroCuotasFacturadas;
	private Long nroCuotasFacturadasNoPagadas;
	private Long nroCuotasFacturadasPagadas;
	private Long valorCapitalTotal;
	private Long valorFacturado;
	private Long valorFacturadoNoPagado;
	private Long valorCapitalPagado;
	private Long interesTotal;
	private Long interesFacturado;
	private Long interesFacturadoNoPagado;
	private Long interesFacturadoPagado;
	private Long interesMoraFacturado;
	private Long interesMoraFacturadoNoPagado;
	private Long interesMoraPagado;
	/* 1er vencimiento de la cuota m�s antigua no pagada - la fecha de hoy */
	private Long diasAtraso;
	private Date fechaCarga;

	public Long getDiasAtraso() {
		return diasAtraso;
	}

	public void setDiasAtraso(Long diasAtraso) {
		this.diasAtraso = diasAtraso;
	}

	public Date getFechaCompra() {
		return fechaCompra;
	}

	public void setFechaCompra(Date fechaCompra) {
		this.fechaCompra = fechaCompra;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaUltimoCambioEstado() {
		return fechaUltimoCambioEstado;
	}

	public void setFechaUltimoCambioEstado(Date fechaUltimoCambioEstado) {
		this.fechaUltimoCambioEstado = fechaUltimoCambioEstado;
	}

	public Long getInteresFacturado() {
		return interesFacturado;
	}

	public void setInteresFacturado(Long interesFacturado) {
		this.interesFacturado = interesFacturado;
	}

	public Long getInteresFacturadoNoPagado() {
		return interesFacturadoNoPagado;
	}

	public void setInteresFacturadoNoPagado(Long interesFacturadoNoPagado) {
		this.interesFacturadoNoPagado = interesFacturadoNoPagado;
	}

	public Long getInteresFacturadoPagado() {
		return interesFacturadoPagado;
	}

	public void setInteresFacturadoPagado(Long interesFacturadoPagado) {
		this.interesFacturadoPagado = interesFacturadoPagado;
	}

	public Long getInteresMoraFacturado() {
		return interesMoraFacturado;
	}

	public void setInteresMoraFacturado(Long interesMoraFacturado) {
		this.interesMoraFacturado = interesMoraFacturado;
	}

	public Long getInteresMoraFacturadoNoPagado() {
		return interesMoraFacturadoNoPagado;
	}

	public void setInteresMoraFacturadoNoPagado(
			Long interesMoraFacturadoNoPagado) {
		this.interesMoraFacturadoNoPagado = interesMoraFacturadoNoPagado;
	}

	public Long getInteresMoraPagado() {
		return interesMoraPagado;
	}

	public void setInteresMoraPagado(Long interesMoraPagado) {
		this.interesMoraPagado = interesMoraPagado;
	}

	public Long getInteresTotal() {
		return interesTotal;
	}

	public void setInteresTotal(Long interesTotal) {
		this.interesTotal = interesTotal;
	}

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public Long getNroCuotas() {
		return nroCuotas;
	}

	public void setNroCuotas(Long nroCuotas) {
		this.nroCuotas = nroCuotas;
	}

	public Long getNroCuotasFacturadas() {
		return nroCuotasFacturadas;
	}

	public void setNroCuotasFacturadas(Long nroCuotasFacturadas) {
		this.nroCuotasFacturadas = nroCuotasFacturadas;
	}

	public Long getNroCuotasFacturadasNoPagadas() {
		return nroCuotasFacturadasNoPagadas;
	}

	public void setNroCuotasFacturadasNoPagadas(
			Long nroCuotasFacturadasNoPagadas) {
		this.nroCuotasFacturadasNoPagadas = nroCuotasFacturadasNoPagadas;
	}

	public Long getNroCuotasFacturadasPagadas() {
		return nroCuotasFacturadasPagadas;
	}

	public void setNroCuotasFacturadasPagadas(Long nroCuotasFacturadasPagadas) {
		this.nroCuotasFacturadasPagadas = nroCuotasFacturadasPagadas;
	}

	public Long getNroServicioCompraCartera() {
		return nroServicioCompraCartera;
	}

	public void setNroServicioCompraCartera(Long nroServicioCompraCartera) {
		this.nroServicioCompraCartera = nroServicioCompraCartera;
	}

	public Long getTasaInteres() {
		return tasaInteres;
	}

	public void setTasaInteres(Long tasaInteres) {
		this.tasaInteres = tasaInteres;
	}

	public Long getValorCapitalPagado() {
		return valorCapitalPagado;
	}

	public void setValorCapitalPagado(Long valorCapitalPagado) {
		this.valorCapitalPagado = valorCapitalPagado;
	}

	public Long getValorCapitalTotal() {
		return valorCapitalTotal;
	}

	public void setValorCapitalTotal(Long valorCapitalTotal) {
		this.valorCapitalTotal = valorCapitalTotal;
	}

	public Long getValorCredito() {
		return valorCredito;
	}

	public void setValorCredito(Long valorCredito) {
		this.valorCredito = valorCredito;
	}

	public Long getValorFacturado() {
		return valorFacturado;
	}

	public void setValorFacturado(Long valorFacturado) {
		this.valorFacturado = valorFacturado;
	}

	public Long getValorFacturadoNoPagado() {
		return valorFacturadoNoPagado;
	}

	public void setValorFacturadoNoPagado(Long valorFacturadoNoPagado) {
		this.valorFacturadoNoPagado = valorFacturadoNoPagado;
	}

	public String getEstadoServicioCompra() {
		return estadoServicioCompra;
	}

	public void setEstadoServicioCompra(String estadoServicioCompra) {
		this.estadoServicioCompra = estadoServicioCompra;
	}

	public String getLineaNegocio() {
		return lineaNegocio;
	}

	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}

	public String getPlanServicioFinanciero() {
		return planServicioFinanciero;
	}

	public void setPlanServicioFinanciero(String planServicioFinanciero) {
		this.planServicioFinanciero = planServicioFinanciero;
	}

	public String getSocioNegocio() {
		return socioNegocio;
	}

	public void setSocioNegocio(String socioNegocio) {
		this.socioNegocio = socioNegocio;
	}

	public String getSucursalSocioNegocio() {
		return sucursalSocioNegocio;
	}

	public void setSucursalSocioNegocio(String sucursalSocioNegocio) {
		this.sucursalSocioNegocio = sucursalSocioNegocio;
	}

	public Long getIdCuenta() {
		return idCuenta;
	}

	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}

	public Long getIdIdentifPersonaRelacionada() {
		return idIdentifPersonaRelacionada;
	}

	public void setIdIdentifPersonaRelacionada(Long idIdentifPersonaRelacionada) {
		this.idIdentifPersonaRelacionada = idIdentifPersonaRelacionada;
	}

	public Long getIdLineaNegocio() {
		return idLineaNegocio;
	}

	public void setIdLineaNegocio(Long idLineaNegocio) {
		this.idLineaNegocio = idLineaNegocio;
	}

	public Long getIdPlanServicioFinanciero() {
		return idPlanServicioFinanciero;
	}

	public void setIdPlanServicioFinanciero(Long idPlanServicioFinanciero) {
		this.idPlanServicioFinanciero = idPlanServicioFinanciero;
	}

	public Long getIdSocioNegocio() {
		return idSocioNegocio;
	}

	public void setIdSocioNegocio(Long idSocioNegocio) {
		this.idSocioNegocio = idSocioNegocio;
	}

	public Long getIdTipoIdentifPersonaRelacionada() {
		return idTipoIdentifPersonaRelacionada;
	}

	public void setIdTipoIdentifPersonaRelacionada(
			Long idTipoIdentifPersonaRelacionada) {
		this.idTipoIdentifPersonaRelacionada = idTipoIdentifPersonaRelacionada;
	}

	public String getTipoIdentifPersonaRelacionada() {
		return tipoIdentifPersonaRelacionada;
	}

	public void setTipoIdentifPersonaRelacionada(
			String tipoIdentifPersonaRelacionada) {
		this.tipoIdentifPersonaRelacionada = tipoIdentifPersonaRelacionada;
	}

	public void setIdentifPersonaRelacionada(String identifPersonaRelacionada) {
		this.identifPersonaRelacionada = identifPersonaRelacionada;
	}

	public String getIdentifPersonaRelacionada() {
		return identifPersonaRelacionada;
	}

	public Date getFechaCarga() {
		return fechaCarga;
	}

	public void setFechaCarga(Date fechaCarga) {
		this.fechaCarga = fechaCarga;
	}
}
