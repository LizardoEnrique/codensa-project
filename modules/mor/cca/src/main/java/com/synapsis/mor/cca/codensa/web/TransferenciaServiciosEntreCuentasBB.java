/**
 * $Id: TransferenciaServiciosEntreCuentasBB.java,v 1.4 2007/07/13 17:22:34 ar28750185 Exp $
 */
package com.synapsis.mor.cca.codensa.web;

import java.util.Collection;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.myfaces.shared_tomahawk.util.MessageUtils;

import com.synapsis.commons.services.ServiceHelper;
import com.synapsis.commons.services.model.ServiceResponseError;
import com.synapsis.components.behaviour.Notifier;
import com.synapsis.mor.cca.codensa.model.ServicioCuenta;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;

/**
 * FIXME: [afalduto] internationalization of error messages ...
 * 
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.4 $ $Date: 2007/07/13 17:22:34 $
 */
public class TransferenciaServiciosEntreCuentasBB {

	/**
	 * @uml.property name="numeroCuentaOrigen"
	 */
	private Long numeroCuentaOrigen;

	/**
	 * @uml.property name="numeroCuentaDestino"
	 */
	private Long numeroCuentaDestino;

	/**
	 * Required by rollback operation.
	 * 
	 * @uml.property name="servicioFinancieros"
	 */
	private Collection servicioFinancieros;

	/**
	 * @uml.property name="servicioFinanciero"
	 */
	private ServicioCuenta servicioFinanciero;

	/**
	 * Se encarga de hacer la ejecucion del servicio
	 * <code>serviciosCuentaService</code>.
	 * 
	 * @param event
	 */
	public void buscarServiciosFinancieros(ActionEvent event) {
		// El sistema obtiene los servicios financieros activos asociados a
		// la cuenta ingresada ...
		this.servicioFinancieros = ServiceHelper.getCollectionResponse(
				SynergiaApplicationContext.getServiceManager(),
				"serviciosCuentaService", "findByCriteria",
				new Object[] { this.numeroCuentaOrigen });
	}

	/**
	 * @return
	 */
	public String nextPage() {
		if (this.servicioFinanciero == null) {
			this.addMessage("El servicio financiero seleccionado es invalido");
			// no navigation required ...
			return NavigationConstants.REFRESH;
		}
		// go to destination page ...
		return NavigationConstants.NEXT;
	}

	/**
	 * FIXME: [afalduto] we need some helper to work with messages, proper
	 * appoach is the MessageUtils.
	 * 
	 * @param message
	 */
	private void addMessage(String message) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(message));
	}

	/**
	 * @return
	 */
	public String previousPage() {
		return NavigationConstants.PREVIOUS;
	}

	/**
	 * @param event
	 */
	public String transferir() {
		try {
			ServiceHelper.invoke(
					SynergiaApplicationContext.getServiceManager(),
					"servicioFinancieroService", "transferir", new Object[] {
							this.numeroCuentaOrigen, this.numeroCuentaDestino,
							this.servicioFinanciero });
		} catch (ServiceResponseError e) { // FIXME: [afalduto] proper ??!!?!?!
			FacesContext.getCurrentInstance().addMessage(
					null,
					MessageUtils.getMessage(e.getCode(), new Object[] { e
							.getMessage() }));
			return NavigationConstants.REFRESH;
		}
		Notifier
				.addMessage("La transferencia del servicio se ha realizado con exito.");
		return NavigationConstants.SUCCESS;
	}

	/**
	 * Getter of the property <tt>numeroCuentaOrigen</tt>
	 * 
	 * @return Returns the numeroCuentaOrigen.
	 * @uml.property name="numeroCuentaOrigen"
	 */
	public Long getNumeroCuentaOrigen() {
		return this.numeroCuentaOrigen;
	}

	/**
	 * Setter of the property <tt>numeroCuentaOrigen</tt>
	 * 
	 * @param numeroCuentaOrigen
	 *            The numeroCuentaOrigen to set.
	 * @uml.property name="numeroCuentaOrigen"
	 */
	public void setNumeroCuentaOrigen(Long numeroCuentaOrigen) {
		this.numeroCuentaOrigen = numeroCuentaOrigen;
	}

	/**
	 * Getter of the property <tt>numeroCuentaDestino</tt>
	 * 
	 * @return Returns the numeroCuentaDestino.
	 * @uml.property name="numeroCuentaDestino"
	 */
	public Long getNumeroCuentaDestino() {
		return this.numeroCuentaDestino;
	}

	/**
	 * Setter of the property <tt>numeroCuentaDestino</tt>
	 * 
	 * @param numeroCuentaDestino
	 *            The numeroCuentaDestino to set.
	 * @uml.property name="numeroCuentaDestino"
	 */
	public void setNumeroCuentaDestino(Long numeroCuentaDestino) {
		this.numeroCuentaDestino = numeroCuentaDestino;
	}

	/**
	 * Getter of the property <tt>servicioFinanciero</tt>
	 * 
	 * @return Returns the servicioFinanciero.
	 * @uml.property name="servicioFinanciero"
	 */
	public ServicioCuenta getServicioFinanciero() {
		return this.servicioFinanciero;
	}

	/**
	 * Setter of the property <tt>servicioFinanciero</tt>
	 * 
	 * @param servicioFinanciero
	 *            The servicioFinanciero to set.
	 * @uml.property name="servicioFinanciero"
	 */
	public void setServicioFinanciero(ServicioCuenta servicioFinanciero) {
		this.servicioFinanciero = servicioFinanciero;
	}

	/**
	 * Getter of the property <tt>servicioFinancieros</tt>
	 * 
	 * @return Returns the servicioFinancieros.
	 * @uml.property name="servicioFinancieros"
	 */
	public Collection getServicioFinancieros() {
		return this.servicioFinancieros;
	}

	/**
	 * Setter of the property <tt>servicioFinancieros</tt>
	 * 
	 * @param servicioFinancieros
	 *            The servicioFinancieros to set.
	 * @uml.property name="servicioFinancieros"
	 */
	public void setServicioFinancieros(Collection servicioFinancieros) {
		this.servicioFinancieros = servicioFinancieros;
	}

}
