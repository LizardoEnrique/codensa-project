/**
 * $Id: DeudaExigibleImpl.java,v 1.1 2007/07/04 20:53:52 ar26557682 Exp $
 */
package com.synapsis.mor.cca.codensa.model.impl;

import com.synapsis.mor.cca.codensa.model.DeudaExigible;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * Modela el reporte y actualmente est� mapeado a una Vista que realiza la
 * consulta
 * 
 * @author Paola Attadio
 * @version $Revision: 1.1 $
 * 
 */
public class DeudaExigibleImpl extends SynergiaBusinessObjectImpl implements
		DeudaExigible {

	private static final long serialVersionUID = 1L;

	private Long idServicio;
	private Long idItemCapital;
	private Double saldoCapital;
	private Long idItemIteres;
	private Double saldoInteres;
	private Long idItemNoAfecto;
	private Double saldoItemNoAfecto;
	private Long idItemDiferenciaCotizacion;
	private Double saldoDiferenciaCotizacion;

	public Long getIdItemCapital() {
		return idItemCapital;
	}

	public void setIdItemCapital(Long idItemCapital) {
		this.idItemCapital = idItemCapital;
	}

	public Long getIdItemDiferenciaCotizacion() {
		return idItemDiferenciaCotizacion;
	}

	public void setIdItemDiferenciaCotizacion(Long idItemDiferenciaCotizacion) {
		this.idItemDiferenciaCotizacion = idItemDiferenciaCotizacion;
	}

	public Long getIdItemIteres() {
		return idItemIteres;
	}

	public void setIdItemIteres(Long idItemIteres) {
		this.idItemIteres = idItemIteres;
	}

	public Long getIdItemNoAfecto() {
		return idItemNoAfecto;
	}

	public void setIdItemNoAfecto(Long idItemNoAfecto) {
		this.idItemNoAfecto = idItemNoAfecto;
	}

	public Long getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}

	public Double getSaldoCapital() {
		return saldoCapital;
	}

	public void setSaldoCapital(Double saldoCapital) {
		this.saldoCapital = saldoCapital;
	}

	public Double getSaldoDiferenciaCotizacion() {
		return saldoDiferenciaCotizacion;
	}

	public void setSaldoDiferenciaCotizacion(Double saldoDiferenciaCotizacion) {
		this.saldoDiferenciaCotizacion = saldoDiferenciaCotizacion;
	}

	public Double getSaldoInteres() {
		return saldoInteres;
	}

	public void setSaldoInteres(Double saldoInteres) {
		this.saldoInteres = saldoInteres;
	}

	public Double getSaldoItemNoAfecto() {
		return saldoItemNoAfecto;
	}

	public void setSaldoItemNoAfecto(Double saldoItemNoAfecto) {
		this.saldoItemNoAfecto = saldoItemNoAfecto;
	}
}
