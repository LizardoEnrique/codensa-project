/**
 * $Id: NavigationConstants.java,v 1.1 2007/07/03 20:19:06 ar28750185 Exp $
 */
package com.synapsis.mor.cca.codensa.web;

/**
 * FIXME: [afalduto] will be moved from here ...
 * 
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.1 $ $Date: 2007/07/03 20:19:06 $
 */
public interface NavigationConstants {

	/**
	 * @uml.property name="REFRESH"
	 */
	public static final String REFRESH = null;

	/**
	 * @uml.property name="SUCCESS"
	 */
	public static final String SUCCESS = "SUCCESS";

	/**
	 * @uml.property name="NEXT"
	 */
	public static final String NEXT = "NEXT";

	/**
	 * @uml.property name="PREVIOUS"
	 */
	public static final String PREVIOUS = "PREVIOUS";

}
