/**
 * 
 */
package com.synapsis.mor.cca.codensa.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.commons.collections.Closure;
import org.apache.commons.collections.CollectionUtils;
import org.apache.myfaces.custom.datatable.ext.HtmlDataTable;
import org.apache.myfaces.custom.service.UIService;
import org.apache.myfaces.shared_impl.util.MessageUtils;

import com.synapsis.commons.services.ServiceHelper;
import com.synapsis.mor.cca.codensa.model.impl.RepTotalCompralImpl;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;

/**
 * CU-CCA006
 * 
 * @author outaTiME (afalduto at gmail dot com)
 * @author Paola Attadio (refactor - impl CU)
 * @version $Revision: 1.10 $ $Date: 2008/07/02 19:16:57 $
 */
public class ReporteComprasTotalesBB {

	/**
	 * property para setear la cantidad de registros para los combos
	 */
	private Integer comboMaxResults = new Integer(100);

	/**
	 * Comment for <code>findByCriteriaService</code> Servicio para realizar
	 * la busqueda
	 */
	private UIService findByCriteriaService;

	/**
	 * Comment for <code>dataTable</code> Tabla en la cual se dejan los
	 * resultados de la busqueda ralizada. FIXME: [dbraccio]necesitamos la tabla
	 * para poder refrescarla
	 */
	private HtmlDataTable dataTable;

	private static final String ESTADO_COMPRA_FALTA_DETALLE = "FaltaDetalle";

	// filtros de busqueda
	private String numeroAutorizacion;
	private Date fechaCreacionDesde;
	private Date fechaCreacionHasta;
	private Date fechaCompraDesde;
	private Date fechaCompraHasta;
	private String estadoCompra;
	private Long idSocioNegocio;
	private Long idSocioSucursal;
	private String tipoIdentifPersonaRelacionada;
	private String identifPersonaRelacionada;
	//lista de combos
	private List estadosComprasCombo;
	private List sociosCombo;
	private List sucursalesCombo;
	private List tipoDocumentosCombo;
	
	// lista donde se dajan las compras seleccionadas
	private List compras = new ArrayList();


	/**
	 * @param event
	 */
	public void eliminarCompras(ActionEvent event) {
		final List id = new ArrayList();

		CollectionUtils.forAllDo(this.compras, new Closure() {
			public void execute(Object input) {
				id.add(((RepTotalCompralImpl) input).getId());
			}
		});

		if (id.size() == 0) {
			FacesContext
					.getCurrentInstance()
					.addMessage(
							null,
							MessageUtils
									.getMessage(
											"servicio.financiero.cca.compras.totales.no.hay.seleccionados",
											null));
			return;
		}

		try {
			ServiceHelper.getResponse(SynergiaApplicationContext
					.getServiceManager(), "comprasTotalesService",
					"removeByCriteria", new Object[] { id });
		} catch (Exception e) {

			FacesContext.getCurrentInstance().addMessage(
					null,
					MessageUtils.getMessage(
							"servicio.financiero.cca.compras.totales.error",
							null));
		}
		this.getFindByCriteriaService().execute(true);
		this.getDataTable().setFirst(0);
	}

	/**
	 * realiza la busqueda
	 */
	public void search() {
		this.getFindByCriteriaService().execute();
		this.getDataTable().setFirst(0);
	}

	/**
	 * @param bo
	 * @return
	 */
	public boolean excludeSelection(RepTotalCompralImpl bo) {
		if (((RepTotalCompralImpl) bo).getEstadoCompra().equalsIgnoreCase(
				ESTADO_COMPRA_FALTA_DETALLE))
			return false;
		return true;
	}

	/**
	 * Getter & Setter
	 */
	public List getCompras() {
		return this.compras;
	}

	public void setCompras(List compras) {
		this.compras = compras;
	}

	public String getEstadoCompra() {
		return estadoCompra;
	}

	public void setEstadoCompra(String estadoCompra) {
		this.estadoCompra = estadoCompra;
	}

	public Date getFechaCompraDesde() {
		return fechaCompraDesde;
	}

	public void setFechaCompraDesde(Date fechaCompraDesde) {
		this.fechaCompraDesde = fechaCompraDesde;
	}

	public Date getFechaCompraHasta() {
		return fechaCompraHasta;
	}

	public void setFechaCompraHasta(Date fechaCompraHasta) {
		this.fechaCompraHasta = fechaCompraHasta;
	}

	public Date getFechaCreacionDesde() {
		return fechaCreacionDesde;
	}

	public void setFechaCreacionDesde(Date fechaCreacionDesde) {
		this.fechaCreacionDesde = fechaCreacionDesde;
	}

	public Date getFechaCreacionHasta() {
		return fechaCreacionHasta;
	}

	public void setFechaCreacionHasta(Date fechaCreacionHasta) {
		this.fechaCreacionHasta = fechaCreacionHasta;
	}

	public String getIdentifPersonaRelacionada() {
		return identifPersonaRelacionada;
	}

	public void setIdentifPersonaRelacionada(String identifPersonaRelacionada) {
		this.identifPersonaRelacionada = identifPersonaRelacionada;
	}

	public String getTipoIdentifPersonaRelacionada() {
		return tipoIdentifPersonaRelacionada;
	}

	public void setTipoIdentifPersonaRelacionada(
			String tipoIdentifPersonaRelacionada) {
		this.tipoIdentifPersonaRelacionada = tipoIdentifPersonaRelacionada;
	}

	public Long getIdSocioNegocio() {
		return idSocioNegocio;
	}

	public void setIdSocioNegocio(Long idSocioNegocio) {
		this.idSocioNegocio = idSocioNegocio;
	}

	public Long getIdSocioSucursal() {
		return idSocioSucursal;
	}

	public void setIdSocioSucursal(Long idSocioSucursal) {
		this.idSocioSucursal = idSocioSucursal;
	}

	public String getNumeroAutorizacion() {
		return numeroAutorizacion;
	}

	public void setNumeroAutorizacion(String numeroAutorizacion) {
		this.numeroAutorizacion = numeroAutorizacion;
	}

	public Integer getComboMaxResults() {
		return comboMaxResults;
	}

	public void setComboMaxResults(Integer comboMaxResults) {
		this.comboMaxResults = comboMaxResults;
	}

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public UIService getFindByCriteriaService() {
		return findByCriteriaService;
	}

	public void setFindByCriteriaService(UIService findByCriteriaService) {
		this.findByCriteriaService = findByCriteriaService;
	}

	public List getEstadosComprasCombo() {
		return estadosComprasCombo;
	}

	public void setEstadosComprasCombo(List estadosComprasCombo) {
		this.estadosComprasCombo = estadosComprasCombo;
	}

	public List getSociosCombo() {
		return sociosCombo;
	}

	public void setSociosCombo(List sociosCombo) {
		this.sociosCombo = sociosCombo;
	}

	public List getSucursalesCombo() {
		return sucursalesCombo;
	}

	public void setSucursalesCombo(List sucursalesCombo) {
		this.sucursalesCombo = sucursalesCombo;
	}

	public List getTipoDocumentosCombo() {
		return tipoDocumentosCombo;
	}

	public void setTipoDocumentosCombo(List tipoDocumentosCombo) {
		this.tipoDocumentosCombo = tipoDocumentosCombo;
	}
	
	
}
