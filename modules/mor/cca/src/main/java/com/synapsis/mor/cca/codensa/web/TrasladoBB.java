/**
 * $Id: TrasladoBB.java,v 1.3 2007/07/04 18:57:42 ar26557682 Exp $
 */
package com.synapsis.mor.cca.codensa.web;

import java.util.Date;

import org.apache.myfaces.custom.datatable.ext.HtmlDataTable;
import org.apache.myfaces.custom.service.UIService;

/**
 * CU-CCA036
 * 
 * @author Paola Attadio
 * @version $Revision: 1.3 $
 */
public class TrasladoBB {
	
	/**
	 * property para setear la cantidad de registros para los combos
	 */
	private Integer comboMaxResults = new Integer(100);
	/**
	 * Comment for <code>findByCriteriaService</code> Servicio para realizar
	 * la busqueda
	 */
	private UIService findByCriteriaService;
	
	/**
	 * Comment for <code>dataTable</code> Tabla en la cual se dejan los
	 * resultados de la busqueda ralizada. FIXME: [dbraccio]necesitamos la tabla
	 * para poder refrescarla
	 */
	private HtmlDataTable dataTable;
	
	private Date fechaDesde;
	private Date fechaHasta;
	private Long nroCuenta;
	//TIPO Y NUMERO DE DOCUMENTO
	private String tipoIdentifPersonaRelacionada; // CODIGO: dni, etc ...
	private String identifPersonaRelacionada;

	private Long idLineaNegocio; 
	private Long idSocioNegocio;
    private Long idPlan;
    private String marcaProducto;

	public void search() {
		this.getFindByCriteriaService().execute();
		this.getDataTable().setFirst(0);
	}

    public Date getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public Date getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public Long getIdLineaNegocio() {
		return idLineaNegocio;
	}
	public void setIdLineaNegocio(Long idLineaNegocio) {
		this.idLineaNegocio = idLineaNegocio;
	}
	public Long getIdPlan() {
		return idPlan;
	}
	public void setIdPlan(Long idPlan) {
		this.idPlan = idPlan;
	}
	public Long getIdSocioNegocio() {
		return idSocioNegocio;
	}
	public void setIdSocioNegocio(Long idSocioNegocio) {
		this.idSocioNegocio = idSocioNegocio;
	}
	public String getMarcaProducto() {
		return marcaProducto;
	}
	public void setMarcaProducto(String marcaProducto) {
		this.marcaProducto = marcaProducto;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public String getIdentifPersonaRelacionada() {
		return identifPersonaRelacionada;
	}
	public void setIdentifPersonaRelacionada(String identifPersonaRelacionada) {
		this.identifPersonaRelacionada = identifPersonaRelacionada;
	}
	public String getTipoIdentifPersonaRelacionada() {
		return tipoIdentifPersonaRelacionada;
	}
	public void setTipoIdentifPersonaRelacionada(
			String tipoIdentifPersonaRelacionada) {
		this.tipoIdentifPersonaRelacionada = tipoIdentifPersonaRelacionada;
	}
	public Integer getComboMaxResults() {
		return comboMaxResults;
	}
	public void setComboMaxResults(Integer comboMaxResults) {
		this.comboMaxResults = comboMaxResults;
	}
	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	public UIService getFindByCriteriaService() {
		return findByCriteriaService;
	}
	public void setFindByCriteriaService(UIService findByCriteriaService) {
		this.findByCriteriaService = findByCriteriaService;
	}
}
