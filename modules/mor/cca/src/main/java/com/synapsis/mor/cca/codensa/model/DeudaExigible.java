/**
 * $Id: DeudaExigible.java,v 1.1 2007/07/04 20:53:52 ar26557682 Exp $
 */
package com.synapsis.mor.cca.codensa.model;

import com.suivant.arquitectura.core.model.BusinessObject;

/**
 * Modela el reporte y actualmente est� mapeado a una Vista que realiza la
 * consulta de compras totales
 * 
 * @author Paola Attadio
 * @version $Revision: 1.1 $
 * 
 */
public interface DeudaExigible extends BusinessObject {

	public Long getIdItemCapital();

	public void setIdItemCapital(Long idItemCapital);

	public Long getIdItemDiferenciaCotizacion();

	public void setIdItemDiferenciaCotizacion(Long idItemDiferenciaCotizacion);

	public Long getIdItemIteres();

	public void setIdItemIteres(Long idItemIteres);

	public Long getIdItemNoAfecto();

	public void setIdItemNoAfecto(Long idItemNoAfecto);

	public Long getIdServicio();

	public void setIdServicio(Long idServicio);

	public Double getSaldoCapital();

	public void setSaldoCapital(Double saldoCapital);

	public Double getSaldoDiferenciaCotizacion();

	public void setSaldoDiferenciaCotizacion(Double saldoDiferenciaCotizacion);

	public Double getSaldoInteres();

	public void setSaldoInteres(Double saldoInteres);

	public Double getSaldoItemNoAfecto();

	public void setSaldoItemNoAfecto(Double saldoItemNoAfecto);
}
