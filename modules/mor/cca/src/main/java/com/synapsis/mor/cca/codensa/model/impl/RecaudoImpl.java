/**
 * $Id: RecaudoImpl.java,v 1.5 2007/07/04 18:54:50 ar26557682 Exp $ 
 */
package com.synapsis.mor.cca.codensa.model.impl;

import java.util.Date;

import com.synapsis.mor.cca.codensa.model.Recaudo;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * Modela el reporte y actualmente est� mapeado a una Vista
 * que realiza la consulta
 * 
 * @author Paola Attadio
 * 
 */
public class RecaudoImpl extends SynergiaBusinessObjectImpl implements Recaudo {

	private static final long serialVersionUID = 1L;

	private String lineaNegocio;
	private String socioNegocio;
	private String sucursalSocioNegocio;
    private String planServicioFinanciero;
	private Long idCuenta;// no en la tabla
	private Long nroCuenta;
	private Long nroServicioCompraCartera;
	private Date fechaCompra;
    private Date fechaCreacion; // pendiente def.
	private String estadoServicioCompraCartera;
	private Long idTipoIdentifPersonaRelacionada;// no en la tabla
	private String tipoIdentifPersonaRelacionada; // CODIGO: dni, etc ...
	private Long idIdentifPersonaRelacionada;// no en la tabla
	private String identifPersonaRelacionada;
	private String producto;
	private Date fechaPago;
	private Date fechaProceso;
	private Date fechaIngresoSistema;
	private Long nroCuotas;
	private Long nroCuotasRecaudadas;
	private String cargoCapitalAmortizado;
	private Long valorCapitalAmortizado;
	private String cargoInteresAmortizado;
	private Long valorInteresAmortizado;
	private String cargoInteresMoraAmortizado;
	private Long valorInteresMoraAmortizado;

	public String getCargoCapitalAmortizado() {
		return cargoCapitalAmortizado;
	}

	public void setCargoCapitalAmortizado(String cargoCapitalAmortizado) {
		this.cargoCapitalAmortizado = cargoCapitalAmortizado;
	}

	public String getCargoInteresAmortizado() {
		return cargoInteresAmortizado;
	}

	public void setCargoInteresAmortizado(String cargoInteresAmortizado) {
		this.cargoInteresAmortizado = cargoInteresAmortizado;
	}

	public String getCargoInteresMoraAmortizado() {
		return cargoInteresMoraAmortizado;
	}

	public void setCargoInteresMoraAmortizado(String cargoInteresMoraAmortizado) {
		this.cargoInteresMoraAmortizado = cargoInteresMoraAmortizado;
	}

	public String getEstadoServicioCompraCartera() {
		return estadoServicioCompraCartera;
	}

	public void setEstadoServicioCompraCartera(
			String estadoServicioCompraCartera) {
		this.estadoServicioCompraCartera = estadoServicioCompraCartera;
	}

	public Date getFechaCompra() {
		return fechaCompra;
	}

	public void setFechaCompra(Date fechaCompra) {
		this.fechaCompra = fechaCompra;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaIngresoSistema() {
		return fechaIngresoSistema;
	}

	public void setFechaIngresoSistema(Date fechaIngresoSistema) {
		this.fechaIngresoSistema = fechaIngresoSistema;
	}

	public Date getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public Date getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public Long getIdCuenta() {
		return idCuenta;
	}

	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}

	public String getIdentifPersonaRelacionada() {
		return identifPersonaRelacionada;
	}

	public void setIdentifPersonaRelacionada(String identifPersonaRelacionada) {
		this.identifPersonaRelacionada = identifPersonaRelacionada;
	}

	public Long getIdIdentifPersonaRelacionada() {
		return idIdentifPersonaRelacionada;
	}

	public void setIdIdentifPersonaRelacionada(Long idIdentifPersonaRelacionada) {
		this.idIdentifPersonaRelacionada = idIdentifPersonaRelacionada;
	}

	public Long getIdTipoIdentifPersonaRelacionada() {
		return idTipoIdentifPersonaRelacionada;
	}

	public void setIdTipoIdentifPersonaRelacionada(
			Long idTipoIdentifPersonaRelacionada) {
		this.idTipoIdentifPersonaRelacionada = idTipoIdentifPersonaRelacionada;
	}

	public String getLineaNegocio() {
		return lineaNegocio;
	}

	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public Long getNroCuotas() {
		return nroCuotas;
	}

	public void setNroCuotas(Long nroCuotas) {
		this.nroCuotas = nroCuotas;
	}

	public Long getNroCuotasRecaudadas() {
		return nroCuotasRecaudadas;
	}

	public void setNroCuotasRecaudadas(Long nroCuotasRecaudadas) {
		this.nroCuotasRecaudadas = nroCuotasRecaudadas;
	}

	public Long getNroServicioCompraCartera() {
		return nroServicioCompraCartera;
	}

	public void setNroServicioCompraCartera(Long nroServicioCompraCartera) {
		this.nroServicioCompraCartera = nroServicioCompraCartera;
	}

	public String getPlanServicioFinanciero() {
		return planServicioFinanciero;
	}

	public void setPlanServicioFinanciero(String planServicioFinanciero) {
		this.planServicioFinanciero = planServicioFinanciero;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getSocioNegocio() {
		return socioNegocio;
	}

	public void setSocioNegocio(String socioNegocio) {
		this.socioNegocio = socioNegocio;
	}

	public String getSucursalSocioNegocio() {
		return sucursalSocioNegocio;
	}

	public void setSucursalSocioNegocio(String sucursalSocioNegocio) {
		this.sucursalSocioNegocio = sucursalSocioNegocio;
	}

	public String getTipoIdentifPersonaRelacionada() {
		return tipoIdentifPersonaRelacionada;
	}

	public void setTipoIdentifPersonaRelacionada(
			String tipoIdentifPersonaRelacionada) {
		this.tipoIdentifPersonaRelacionada = tipoIdentifPersonaRelacionada;
	}

	public Long getValorCapitalAmortizado() {
		return valorCapitalAmortizado;
	}

	public void setValorCapitalAmortizado(Long valorCapitalAmortizado) {
		this.valorCapitalAmortizado = valorCapitalAmortizado;
	}

	public Long getValorInteresAmortizado() {
		return valorInteresAmortizado;
	}

	public void setValorInteresAmortizado(Long valorInteresAmortizado) {
		this.valorInteresAmortizado = valorInteresAmortizado;
	}

	public Long getValorInteresMoraAmortizado() {
		return valorInteresMoraAmortizado;
	}

	public void setValorInteresMoraAmortizado(Long valorInteresMoraAmortizado) {
		this.valorInteresMoraAmortizado = valorInteresMoraAmortizado;
	}
}
