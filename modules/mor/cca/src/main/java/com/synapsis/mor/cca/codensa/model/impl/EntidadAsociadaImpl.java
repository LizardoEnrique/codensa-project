/**
 * $Id: EntidadAsociadaImpl.java,v 1.3 2007/09/10 21:50:19 ar29261698 Exp $
 */
package com.synapsis.mor.cca.codensa.model.impl;

import com.synapsis.mor.cca.codensa.model.EntidadAsociada;
import com.synapsis.synergia.core.model.impl.SynergiaPersistentObjectImpl;

/**
 * 
 * @author Paola Attadio
 * 
 */
public class EntidadAsociadaImpl extends SynergiaPersistentObjectImpl implements
		EntidadAsociada {

	private static final long serialVersionUID = 1L;

	private String codigo;
	private String nombre;
	private char activo;
	private String nit;

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public char getActivo() {
		return this.activo;
	}

	public void setActivo(char activo) {
		this.activo = activo;
	}

	public String getNit() {
		return this.nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public String getEntityName() {
		return "EntidadAsociada";
	}
}
