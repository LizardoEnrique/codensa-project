/**
 * $Id: EntidadAsociada.java,v 1.2 2007/06/29 13:19:06 ar26557682 Exp $
 */
package com.synapsis.mor.cca.codensa.model;

import com.suivant.arquitectura.core.model.PersistentObject;

/**
 * 
 * @author Paola Attadio
 * 
 */
public interface EntidadAsociada extends PersistentObject {

	public String getCodigo();

	public void setCodigo(String codigo);

	public String getNombre();

	public void setNombre(String nombre);

	public char getActivo();

	public void setActivo(char activo);

	public String getNit();

	public void setNit(String nit);
}
