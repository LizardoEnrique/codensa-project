/**
 * $Id: LineaNegocio.java,v 1.2 2007/06/29 13:20:51 ar26557682 Exp $
 */
package com.synapsis.mor.cca.codensa.model;

import java.util.Set;

import com.suivant.arquitectura.core.model.PersistentObject;

/**
 * 
 * @author Paola Attadio
 *
 */
public interface LineaNegocio extends PersistentObject {

	public String getCodigo();
	public void setCodigo(String codigo);
	public String getDescripcion();
	public void setDescripcion(String descripcion);
	public char getActivo();
	public void setActivo(char activo);
	public Set getTipoProductos();
	public void setTipoProductos(Set ccaTipoProductos);
}
