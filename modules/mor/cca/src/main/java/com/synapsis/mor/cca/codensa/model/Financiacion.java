/**
 * 
 */
package com.synapsis.mor.cca.codensa.model;

import java.math.BigDecimal;
import java.util.Set;

import com.suivant.arquitectura.core.model.PersistentObject;

/**
 * FIXME: Corresponde al modulo de financiacion
 * 
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.4 $ $Date: 2007/06/29 13:19:49 $
 */
public interface Financiacion extends PersistentObject {

	public abstract BigDecimal getPorcInteres();

	public abstract void setPorcInteres(BigDecimal porcInteres);

	public abstract String getDiscriminador();

	public abstract void setDiscriminador(String discriminador);

	public abstract Long getIdRefinUsura();

	public abstract void setIdRefinUsura(Long idRefinUsura);

	public abstract Set getServFinancs();

	public abstract void setServFinancs(Set ccaServFinancs);

}