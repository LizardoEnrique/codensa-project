/**
 * $Id: ServicioActivoImpl.java,v 1.1 2007/07/06 15:19:08 ar28750185 Exp $
 */
package com.synapsis.mor.cca.codensa.model.impl;

import com.synapsis.mor.cca.codensa.model.ServicioActivo;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.1 $ $Date: 2007/07/06 15:19:08 $
 */
public class ServicioActivoImpl extends SynergiaBusinessObjectImpl implements
		ServicioActivo {

	/**
	 * @uml.property name="nroCuenta"
	 */
	private Long nroCuenta;

	/**
	 * @uml.property name="idServicio"
	 */
	private Long idServicio;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioActivo#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return this.nroCuenta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioActivo#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioActivo#getIdServicio()
	 */
	public Long getIdServicio() {
		return this.idServicio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioActivo#setIdServicio(java.lang.Long)
	 */
	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}

}
