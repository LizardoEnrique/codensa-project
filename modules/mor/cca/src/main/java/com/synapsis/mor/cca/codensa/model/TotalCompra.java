/**
 * $Id: TotalCompra.java,v 1.1 2007/06/29 13:25:23 ar26557682 Exp $
 */
package com.synapsis.mor.cca.codensa.model;

import java.util.Date;

import com.suivant.arquitectura.core.model.PersistentObject;

/**
 * 
 * @author Paola Attadio
 * @version $Revision: 1.1 $
 * 
 */
public interface TotalCompra extends PersistentObject {

	public Long getCantidadCuotas();

	public void setCantidadCuotas(Long cantidadCuotas);

	public String getEstado();

	public void setEstado(String estado);

	public Date getFechaCarga();

	public void setFechaCarga(Date fechaCarga);

	public Date getFechaCompra();

	public void setFechaCompra(Date fechaCompra);

	public Date getFechaCreacion();

	public void setFechaCreacion(Date fechaCreacion);

	public Long getIdCupoCredito();

	public void setIdCupoCredito(Long idCupoCredito);

	public Long getIdEntidadAsociada();

	public void setIdEntidadAsociada(Long idEntidadAsociada);

	public Long getIdSucursalEntidadAsociada();

	public void setIdSucursalEntidadAsociada(Long idSucursalEntidadAsociada);

	public String getNombreArchivo();

	public void setNombreArchivo(String nombreArchivo);

	public String getNtoAprobacion();

	public void setNtoAprobacion(String ntoAprobacion);

	public Long getValorConsumo();

	public void setValorConsumo(Long valorConsumo);}
