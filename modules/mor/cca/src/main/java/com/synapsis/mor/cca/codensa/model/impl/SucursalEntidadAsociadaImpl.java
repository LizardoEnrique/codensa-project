/**
 * $Id: SucursalEntidadAsociadaImpl.java,v 1.3 2007/09/10 21:50:19 ar29261698 Exp $
 */
package com.synapsis.mor.cca.codensa.model.impl;

import com.synapsis.mor.cca.codensa.model.SucursalEntidadAsociada;
import com.synapsis.synergia.core.model.impl.SynergiaPersistentObjectImpl;

/**
 * 
 * @author Paola Attadio
 * @version $Revision: 1.3 $
 */
public class SucursalEntidadAsociadaImpl extends SynergiaPersistentObjectImpl
		implements SucursalEntidadAsociada {

	private static final long serialVersionUID = 1L;

	private String codigo;
	private String descripcion;
	private Long idEntidadAsociada;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Long getIdEntidadAsociada() {
		return idEntidadAsociada;
	}

	public void setIdEntidadAsociada(Long idEntidadAsociada) {
		this.idEntidadAsociada = idEntidadAsociada;
	}

	public String getEntityName() {
		return "SucursalEntidadAsociada";
	}
}
