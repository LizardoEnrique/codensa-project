/**
 * $Id: LineaNegocioImpl.java,v 1.3 2007/09/10 21:50:19 ar29261698 Exp $
 */
package com.synapsis.mor.cca.codensa.model.impl;

import java.util.HashSet;
import java.util.Set;

import com.synapsis.mor.cca.codensa.model.LineaNegocio;
import com.synapsis.synergia.core.model.impl.SynergiaPersistentObjectImpl;

/**
 * 
 * @author Paola Attadio
 * 
 */
public class LineaNegocioImpl extends SynergiaPersistentObjectImpl implements
		LineaNegocio {

	private static final long serialVersionUID = 1L;

	private String codigo;
	private String descripcion;
	private char activo;
	private Set tipoProductos = new HashSet(0);
	
	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public char getActivo() {
		return this.activo;
	}

	public void setActivo(char activo) {
		this.activo = activo;
	}

	public Set getTipoProductos() {
		return this.tipoProductos;
	}

	public void setTipoProductos(Set tipoProductos) {
		this.tipoProductos = tipoProductos;
	}

	public String getEntityName() {
		return "LineaNegocio";
	}

}
