/**
 * $Id: RepTotalCompralImpl.java,v 1.3 2007/07/04 18:56:04 ar26557682 Exp $
 */
package com.synapsis.mor.cca.codensa.model.impl;

import java.util.Date;

import com.synapsis.mor.cca.codensa.model.RepTotalCompra;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * Modela el reporte y actualmente est� mapeado a una Vista
 * que realiza la consulta de compras totales
 * 
 * @author Paola Attadio
 * @version $Revision: 1.3 $
 *
 */
public class RepTotalCompralImpl extends
		SynergiaBusinessObjectImpl implements RepTotalCompra {

	private static final long serialVersionUID = 1L;

	private Long idTipoIdentifPersonaRelacionada;// no en la tabla
	private String tipoIdentifPersonaRelacionada; // CODIGO: dni, etc ...
	private String identifPersonaRelacionada;
	private String nroAutorizacion;
	private Date fechaCreacion;
	private Date fechaCompra;
	private String socioNegocio;
	private Long idSocioNegocio;
	private String sucursalSocioNegocio;
	private Long idSocioSucursal;

	private Long valorCompra;
	private String estadoCompra;
	
	public String getEstadoCompra() {
		return estadoCompra;
	}
	public void setEstadoCompra(String estadoCompra) {
		this.estadoCompra = estadoCompra;
	}
	public Date getFechaCompra() {
		return fechaCompra;
	}
	public void setFechaCompra(Date fechaCompra) {
		this.fechaCompra = fechaCompra;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getIdentifPersonaRelacionada() {
		return identifPersonaRelacionada;
	}
	public void setIdentifPersonaRelacionada(String identifPersonaRelacionada) {
		this.identifPersonaRelacionada = identifPersonaRelacionada;
	}
	public Long getIdTipoIdentifPersonaRelacionada() {
		return idTipoIdentifPersonaRelacionada;
	}
	public void setIdTipoIdentifPersonaRelacionada(
			Long idTipoIdentifPersonaRelacionada) {
		this.idTipoIdentifPersonaRelacionada = idTipoIdentifPersonaRelacionada;
	}
	public String getSocioNegocio() {
		return socioNegocio;
	}
	public void setSocioNegocio(String socioNegocio) {
		this.socioNegocio = socioNegocio;
	}
	public String getSucursalSocioNegocio() {
		return sucursalSocioNegocio;
	}
	public void setSucursalSocioNegocio(String sucursalSocioNegocio) {
		this.sucursalSocioNegocio = sucursalSocioNegocio;
	}
	public String getTipoIdentifPersonaRelacionada() {
		return tipoIdentifPersonaRelacionada;
	}
	public void setTipoIdentifPersonaRelacionada(
			String tipoIdentifPersonaRelacionada) {
		this.tipoIdentifPersonaRelacionada = tipoIdentifPersonaRelacionada;
	}
	public Long getValorCompra() {
		return valorCompra;
	}
	public void setValorCompra(Long valorCompra) {
		this.valorCompra = valorCompra;
	}
	public String getNroAutorizacion() {
		return nroAutorizacion;
	}
	public void setNroAutorizacion(String nroAutorizacion) {
		this.nroAutorizacion = nroAutorizacion;
	}
	public Long getIdSocioNegocio() {
		return idSocioNegocio;
	}
	public void setIdSocioNegocio(Long idSocioNegocio) {
		this.idSocioNegocio = idSocioNegocio;
	}
	public Long getIdSocioSucursal() {
		return idSocioSucursal;
	}
	public void setIdSocioSucursal(Long idSocioSucursal) {
		this.idSocioSucursal = idSocioSucursal;
	}
}
