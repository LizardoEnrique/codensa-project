/**
 * 
 */
package com.synapsis.mor.cca.codensa.model;

import com.suivant.arquitectura.core.model.PersistentObject;


/**
 * @author outaTiME (afalduto at gmail dot com)
 * @author Paola Attadio (refactor)
 * @version $Revision: 1.2 $ $Date: 2007/06/29 13:18:12 $
 */
public interface CupoCredito extends PersistentObject {

	public Long getCuentaQuePagaServicio();

	public void setCuentaQuePagaServicio(Long cuentaQuePagaServicio);

	public Long getCupoAprobado();

	public void setCupoAprobado(Long cupoAprobado);

	public Long getCupoDisponible();

	public void setCupoDisponible(Long cupoDisponible);

	public Long getNumCuotas();

	public void setNumCuotas(Long numCuotas);

	public String getNumTarjetaCredito();

	public void setNumTarjetaCredito(String numTarjetaCredito);

	public Long getSolicitanteServicioFinanciero();

	public void setSolicitanteServicioFinanciero(
			Long solicitanteServicioFinanciero);
}