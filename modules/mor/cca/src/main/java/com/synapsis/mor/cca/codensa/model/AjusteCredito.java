package com.synapsis.mor.cca.codensa.model;

public interface AjusteCredito {

	/**
	 * Getter of the property <tt>idDocumento</tt>
	 * 
	 * @return Returns the idDocumento.
	 * @uml.property name="idDocumento"
	 */
	public abstract Long getIdDocumento();

	/**
	 * Setter of the property <tt>idDocumento</tt>
	 * 
	 * @param idDocumento
	 *            The idDocumento to set.
	 * @uml.property name="idDocumento"
	 */
	public abstract void setIdDocumento(Long idDocumento);

	/**
	 * Getter of the property <tt>descripcion</tt>
	 * 
	 * @return Returns the descripcion.
	 * @uml.property name="descripcion"
	 */
	public abstract String getDescripcion();

	/**
	 * Setter of the property <tt>descripcion</tt>
	 * 
	 * @param descripcion
	 *            The descripcion to set.
	 * @uml.property name="descripcion"
	 */
	public abstract void setDescripcion(String descripcion);

}