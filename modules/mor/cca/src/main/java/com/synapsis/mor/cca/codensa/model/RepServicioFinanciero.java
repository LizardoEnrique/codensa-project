/**
 * $Id: RepServicioFinanciero.java,v 1.8 2008/06/14 01:29:00 ar32272876 Exp $
 */
package com.synapsis.mor.cca.codensa.model;

import java.util.Date;

import com.suivant.arquitectura.core.model.BusinessObject;

/**
 * Modela el reporte y actualmente est� mapeado a una Vista que realiza la
 * consulta de compras totales
 * 
 * @author Paola Attadio
 * @version $Revision: 1.8 $
 * 
 */
public interface RepServicioFinanciero extends BusinessObject {

	public String getEstadoProceso();

	public void setEstadoProceso(String estadoProceso);

	public Date getFechaCarga();

	public void setFechaCarga(Date fechaCarga);

	public Long getIdPlan();

	public void setIdPlan(Long idPlan);

	public Long getIdSocioNegocio();

	public void setIdSocioNegocio(Long idSocioNegocio);

	public String getLineaNegocio();

	public void setLineaNegocio(String lineaNegocio);

	public String getNroAutorizacion();

	public void setNroAutorizacion(String nroAutorizacion);
	
	public String getNroAutorizacionPago();
	
	public void setNroAutorizacionPago(String nroAutorizacionPago);

	public Long getNroServicio();

	public void setNroServicio(Long nroServicio);

	public String getPdp();

	public void setPdp(String pdp);

	public String getPlan();

	public void setPlan(String plan);

	public String getProducto();

	public void setProducto(String producto);

	public String getSocioNegocio();

	public void setSocioNegocio(String socioNegocio);

	public String getSucursalSocioNegocio();

	public void setSucursalSocioNegocio(String sucursalSocioNegocio);

	public Long getValorFinanciado();

	public void setValorFinanciado(Long valorFinanciado);

	public String getEstadoServicioCompraCartera();

	public void setEstadoServicioCompraCartera(
			String estadoServicioCompraCartera);

	public Long getIdEstadoProceso();

	public void setIdEstadoProceso(Long idEstadoProceso);

	public Long getIdLineaNegocio();

	public void setIdLineaNegocio(Long idLineaNegocio);

	public Long getIdProducto();

	public void setIdProducto(Long idProducto);

	public Long getIdSucursalSocioNegocio();

	public void setIdSucursalSocioNegocio(Long idSucursalSocioNegocio);

	public Long getIdCompraTotal();

	public void setIdCompraTotal(Long idCompraTotal);

	public Date getFechaCompra();

	public void setFechaCompra(Date fechaCompra);
	
	public String getIdentifPersonaRelacionada() ;
	
	public void setIdentifPersonaRelacionada(String identifPersonaRelacionada);
	
	public Long getIdIdentifPersonaRelacionada();
	
	public void setIdIdentifPersonaRelacionada(Long idIdentifPersonaRelacionada);
	
	public Long getIdTipoIdentifPersonaRelacionada();
	
	public void setIdTipoIdentifPersonaRelacionada(
			Long idTipoIdentifPersonaRelacionada);
	
	public String getTipoIdentifPersonaRelacionada();
	
	public void setTipoIdentifPersonaRelacionada(
			String tipoIdentifPersonaRelacionada);
}
