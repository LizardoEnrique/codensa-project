/**
 * $Id: Refactura.java,v 1.3 2007/06/29 13:21:36 ar26557682 Exp $
 */
package com.synapsis.mor.cca.codensa.model;

import java.util.Date;

import com.suivant.arquitectura.core.model.BusinessObject;

/**
 * Modela el reporte y actualmente est� mapeado a una Vista que realiza la
 * consulta de compras totales
 * 
 * @author Paola Attadio
 * 
 */
public interface Refactura extends BusinessObject {

	public Date getFechaAprovado();

	public void setFechaAprovado(Date fechaAprovado);

	public Date getFechaEjecutado();

	public void setFechaEjecutado(Date fechaEjecutado);

	public Long getId_cuenta();

	public void setId_cuenta(Long id_cuenta);

	public String getIdentifPersonaRelacionada();

	public void setIdentifPersonaRelacionada(String identifPersonaRelacionada);

	public Long getIdIdentifPersonaRelacionada();

	public void setIdIdentifPersonaRelacionada(Long idIdentifPersonaRelacionada);

	public Long getIdOrdenRefactura();

	public void setIdOrdenRefactura(Long idOrdenRefactura);

	public Long getIdTipoIdentifPersonaRelacionada();

	public void setIdTipoIdentifPersonaRelacionada(
			Long idTipoIdentifPersonaRelacionada);

	public String getLineaNegocio();

	public void setLineaNegocio(String lineaNegocio);

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public String getNroOrdenRefactura();

	public void setNroOrdenRefactura(String nroOrdenRefactura);

	public Long getNroServicioCompraCartera();

	public void setNroServicioCompraCartera(Long nroServicioCompraCartera);

	public Long getNroSolicitudRefactura();

	public void setNroSolicitudRefactura(Long nroSolicitudRefactura);

	public String getPlanServicioFinanciero();

	public void setPlanServicioFinanciero(String planServicioFinanciero);

	public String getSocioNegocio();

	public void setSocioNegocio(String socioNegocio);

	public String getSucursalSocioNegocio();

	public void setSucursalSocioNegocio(String sucursalSocioNegocio);

	public String getTipoIdentifPersonaRelacionada();

	public void setTipoIdentifPersonaRelacionada(
			String tipoIdentifPersonaRelacionada);

	public String getTipoProducto();

	public void setTipoProducto(String tipoProducto);

	public Long getValorRefacturadoPorIntereses();

	public void setValorRefacturadoPorIntereses(
			Long valorRefacturadoPorIntereses);

	public Long getValorRefacturadoPorOtrosCargos();

	public void setValorRefacturadoPorOtrosCargos(
			Long valorRefacturadoPorOtrosCargos);
}
