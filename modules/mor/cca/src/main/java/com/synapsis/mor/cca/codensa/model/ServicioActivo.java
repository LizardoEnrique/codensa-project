package com.synapsis.mor.cca.codensa.model;

public interface ServicioActivo {

	/**
	 * Getter of the property <tt>nroCuenta</tt>
	 * 
	 * @return Returns the nroCuenta.
	 * @uml.property name="nroCuenta"
	 */
	public abstract Long getNroCuenta();

	/**
	 * Setter of the property <tt>nroCuenta</tt>
	 * 
	 * @param nroCuenta
	 *            The nroCuenta to set.
	 * @uml.property name="nroCuenta"
	 */
	public abstract void setNroCuenta(Long nroCuenta);

	/**
	 * Getter of the property <tt>idServicio</tt>
	 * 
	 * @return Returns the idServicio.
	 * @uml.property name="idServicio"
	 */
	public abstract Long getIdServicio();

	/**
	 * Setter of the property <tt>idServicio</tt>
	 * 
	 * @param idServicio
	 *            The idServicio to set.
	 * @uml.property name="idServicio"
	 */
	public abstract void setIdServicio(Long idServicio);

}