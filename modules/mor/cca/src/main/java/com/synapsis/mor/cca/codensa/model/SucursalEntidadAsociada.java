/**
 * $Id: SucursalEntidadAsociada.java,v 1.2 2007/06/29 13:24:36 ar26557682 Exp $
 */
package com.synapsis.mor.cca.codensa.model;

import com.suivant.arquitectura.core.model.PersistentObject;

/**
 * 
 * @author Paola Attadio
 * @version $Revision: 1.2 $
 *
 */
public interface SucursalEntidadAsociada extends PersistentObject {

	public String getCodigo();

	public void setCodigo(String codigo);

	public String getDescripcion();

	public void setDescripcion(String descripcion);

	public Long getIdEntidadAsociada();

	public void setIdEntidadAsociada(Long idEntidadAsociada);
}
