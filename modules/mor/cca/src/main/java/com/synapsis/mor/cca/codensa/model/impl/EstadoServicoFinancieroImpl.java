/**
 * $Id: EstadoServicoFinancieroImpl.java,v 1.2 2007/06/21 14:28:24 ar26557682 Exp $
 */
package com.synapsis.mor.cca.codensa.model.impl;

import com.synapsis.mor.cca.codensa.model.EstadoServicoFinanciero;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * Modela el reporte y actualmente est� mapeado a una Vista
 * que realiza la consulta
 * 
 * @author Paola Attadio
 *
 */
public class EstadoServicoFinancieroImpl extends SynergiaBusinessObjectImpl
		implements EstadoServicoFinanciero {

	private static final long serialVersionUID = 1L;
	
	private String estado;

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
}
