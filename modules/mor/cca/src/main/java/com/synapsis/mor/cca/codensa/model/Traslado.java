/**
 * $Id: Traslado.java,v 1.4 2007/06/29 13:25:36 ar26557682 Exp $
 */
package com.synapsis.mor.cca.codensa.model;

import java.util.Date;

import com.suivant.arquitectura.core.model.BusinessObject;

/**
 * Modela el reporte y actualmente est� mapeado a una Vista
 * que realiza la consulta 
 * 
 * @author Paola Attadio
 * 
 */
public interface Traslado extends BusinessObject {
	public String getDireccionRepartoCDEctaFinal();

	public void setDireccionRepartoCDEctaFinal(
			String direccionRepartoCDEctaFinal);

	public Date getFechaCompra();

	public void setFechaCompra(Date fechaCompra);

	public Date getFechaCreacion();

	public void setFechaCreacion(Date fechaCreacion);

	public Date getFechaTraslado();

	public void setFechaTraslado(Date fechaTraslado);

	public String getIndicadorDireccionRepartoEspecialCDEctaFinal();

	public void setIndicadorDireccionRepartoEspecialCDEctaFinal(
			String indicadorDireccionRepartoEspecialCDEctaFinal);

	public Long getInteresesPagados();

	public void setInteresesPagados(Long interesesPagados);

	public Long getInteresFacturado();

	public void setInteresFacturado(Long interesFacturado);

	public Long getInteresFacturadoNoPagado();

	public void setInteresFacturadoNoPagado(Long interesFacturadoNoPagado);

	public Long getInteresMoraFacturado();

	public void setInteresMoraFacturado(Long interesMoraFacturado);

	public Long getInteresMoraFacturadoNoPagado();

	public void setInteresMoraFacturadoNoPagado(
			Long interesMoraFacturadoNoPagado);

	public Long getInteresMoraPagado();

	public void setInteresMoraPagado(Long interesMoraPagado);

	public Long getInteresPorFacturar();

	public void setInteresPorFacturar(Long interesPorFacturar);

	public Long getInteresTotal();

	public void setInteresTotal(Long interesTotal);

	public String getLineaNegocio();

	public void setLineaNegocio(String lineaNegocio);

	public Long getNroCuentaFinal();

	public void setNroCuentaFinal(Long nroCuentaFinal);

	public Long getNroCuentaInicial();

	public void setNroCuentaInicial(Long nroCuentaInicial);

	public Long getNroCuotas();

	public void setNroCuotas(Long nroCuotas);

	public Long getNroCuotasFacturadas();

	public void setNroCuotasFacturadas(Long nroCuotasFacturadas);

	public Long getNroCuotasFacturadasNoPagadas();

	public void setNroCuotasFacturadasNoPagadas(
			Long nroCuotasFacturadasNoPagadas);

	public Long getNroCuotasFacturadasPagadas();

	public void setNroCuotasFacturadasPagadas(Long nroCuotasFacturadasPagadas);

	public Long getNroServicioCompraCarteraFinal();

	public void setNroServicioCompraCarteraFinal(
			Long nroServicioCompraCarteraFinal);

	public Long getNroServicioCompraCarteraInicial();

	public void setNroServicioCompraCarteraInicial(
			Long nroServicioCompraCarteraInicial);

	public String getPlanServicioFinanciero();

	public void setPlanServicioFinanciero(String planServicioFinanciero);

	public String getProducto();

	public void setProducto(String producto);

	public String getSocioNegocio();

	public void setSocioNegocio(String socioNegocio);

	public String getSucursalSocioNegocio();

	public void setSucursalSocioNegocio(String sucursalSocioNegocio);

	public Long getTasaInteres();

	public void setTasaInteres(Long tasaInteres);

	public Long getValorCapitalPagado();

	public void setValorCapitalPagado(Long valorCapitalPagado);

	public Long getValorCapitalPorFacturar();

	public void setValorCapitalPorFacturar(Long valorCapitalPorFacturar);

	public Long getValorCapitalTotal();

	public void setValorCapitalTotal(Long valorCapitalTotal);

	public Long getValorCredito();

	public void setValorCredito(Long valorCredito);

	public Long getValorFacturado();

	public void setValorFacturado(Long valorFacturado);

	public Long getValorFacturadoNoPagado();

	public void setValorFacturadoNoPagado(Long valorFacturadoNoPagado);

	public Long getIdCuenta();

	public void setIdCuenta(Long idCuenta);

	public String getIdentifPersonaRelacionada();

	public void setIdentifPersonaRelacionada(String identifPersonaRelacionada);

	public Long getIdIdentifPersonaRelacionada();

	public void setIdIdentifPersonaRelacionada(Long idIdentifPersonaRelacionada);

	public Long getIdLineaNegocio();

	public void setIdLineaNegocio(Long idLineaNegocio);

	public Long getIdPlanServicioFinanciero();

	public void setIdPlanServicioFinanciero(Long idPlanServicioFinanciero);

	public Long getIdProducto();

	public void setIdProducto(Long idProducto);

	public Long getIdServicio();

	public void setIdServicio(Long idServicio);

	public Long getIdSocioNegocio();

	public void setIdSocioNegocio(Long idSocioNegocio);

	public Long getIdSucursalSocioNegocio();

	public void setIdSucursalSocioNegocio(Long idSucursalSocioNegocio);

	public Long getIdTipoIdentifPersonaRelacionada();

	public void setIdTipoIdentifPersonaRelacionada(
			Long idTipoIdentifPersonaRelacionada);

	public String getTipoIdentifPersonaRelacionada();

	public void setTipoIdentifPersonaRelacionada(
			String tipoIdentifPersonaRelacionada);
}
