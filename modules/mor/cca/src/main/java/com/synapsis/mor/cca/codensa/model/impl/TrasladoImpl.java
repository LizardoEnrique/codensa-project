/**
 * $Id: TrasladoImpl.java,v 1.5 2007/07/10 14:46:23 ar26557682 Exp $
 */
package com.synapsis.mor.cca.codensa.model.impl;

import java.util.Date;

import com.synapsis.mor.cca.codensa.model.Traslado;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * Modela el reporte y actualmente est� mapeado a una Vista que realiza la
 * consulta
 * 
 * @author Paola Attadio
 * 
 */
public class TrasladoImpl extends SynergiaBusinessObjectImpl implements
		Traslado {

	private static final long serialVersionUID = 1L;

	private Long idLineaNegocio; // no mapeada en la tabla
	private String lineaNegocio;
	private Long idSocioNegocio; // no mapeada en la tabla
	private String socioNegocio;
	private Long idSucursalSocioNegocio; // no mapeada en la tabla
	private String sucursalSocioNegocio;
	private Long idProducto; // no mapeada en la tabla
	private String producto;
	private Long idPlanServicioFinanciero; // no mapeada en la tabla
	private String planServicioFinanciero;
	private Long idServicio; // no mapeada en la tabla
	private Date fechaTraslado;
	private Date fechaCompra;
	private Date fechaCreacion;
	private Long idTipoIdentifPersonaRelacionada;// no en la tabla
	private String tipoIdentifPersonaRelacionada;
	private Long idIdentifPersonaRelacionada;// no en la tabla
	private String identifPersonaRelacionada;
	private Long idCuenta; // no mapeada en la tabla
	private Long nroCuentaInicial;
	private Long nroCuentaFinal;
	private String direccionRepartoCDEctaFinal;
	private String indicadorDireccionRepartoEspecialCDEctaFinal;
	private Long nroServicioCompraCarteraInicial;
	private Long nroServicioCompraCarteraFinal;
	private Long valorCredito;
	private Long tasaInteres;
	private Long nroCuotas;
	private Long nroCuotasFacturadas;
	private Long nroCuotasFacturadasNoPagadas;
	private Long nroCuotasFacturadasPagadas;
	private Long valorCapitalTotal;
	private Long valorFacturado;
	private Long valorFacturadoNoPagado;
	private Long valorCapitalPagado;
	private Long valorCapitalPorFacturar;
	private Long interesTotal;
	private Long interesFacturado;
	private Long interesFacturadoNoPagado;
	private Long interesPorFacturar;
	private Long interesesPagados;
	private Long interesMoraFacturado;
	private Long interesMoraFacturadoNoPagado;
	private Long interesMoraPagado;

	public String getDireccionRepartoCDEctaFinal() {
		return direccionRepartoCDEctaFinal;
	}

	public void setDireccionRepartoCDEctaFinal(
			String direccionRepartoCDEctaFinal) {
		this.direccionRepartoCDEctaFinal = direccionRepartoCDEctaFinal;
	}

	public Date getFechaCompra() {
		return fechaCompra;
	}

	public void setFechaCompra(Date fechaCompra) {
		this.fechaCompra = fechaCompra;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaTraslado() {
		return fechaTraslado;
	}

	public void setFechaTraslado(Date fechaTraslado) {
		this.fechaTraslado = fechaTraslado;
	}

	public String getIndicadorDireccionRepartoEspecialCDEctaFinal() {
		return indicadorDireccionRepartoEspecialCDEctaFinal;
	}

	public void setIndicadorDireccionRepartoEspecialCDEctaFinal(
			String indicadorDireccionRepartoEspecialCDEctaFinal) {
		this.indicadorDireccionRepartoEspecialCDEctaFinal = indicadorDireccionRepartoEspecialCDEctaFinal;
	}

	public Long getInteresesPagados() {
		return interesesPagados;
	}

	public void setInteresesPagados(Long interesesPagados) {
		this.interesesPagados = interesesPagados;
	}

	public Long getInteresFacturado() {
		return interesFacturado;
	}

	public void setInteresFacturado(Long interesFacturado) {
		this.interesFacturado = interesFacturado;
	}

	public Long getInteresFacturadoNoPagado() {
		return interesFacturadoNoPagado;
	}

	public void setInteresFacturadoNoPagado(Long interesFacturadoNoPagado) {
		this.interesFacturadoNoPagado = interesFacturadoNoPagado;
	}

	public Long getInteresMoraFacturado() {
		return interesMoraFacturado;
	}

	public void setInteresMoraFacturado(Long interesMoraFacturado) {
		this.interesMoraFacturado = interesMoraFacturado;
	}

	public Long getInteresMoraFacturadoNoPagado() {
		return interesMoraFacturadoNoPagado;
	}

	public void setInteresMoraFacturadoNoPagado(
			Long interesMoraFacturadoNoPagado) {
		this.interesMoraFacturadoNoPagado = interesMoraFacturadoNoPagado;
	}

	public Long getInteresMoraPagado() {
		return interesMoraPagado;
	}

	public void setInteresMoraPagado(Long interesMoraPagado) {
		this.interesMoraPagado = interesMoraPagado;
	}

	public Long getInteresPorFacturar() {
		return interesPorFacturar;
	}

	public void setInteresPorFacturar(Long interesPorFacturar) {
		this.interesPorFacturar = interesPorFacturar;
	}

	public Long getInteresTotal() {
		return interesTotal;
	}

	public void setInteresTotal(Long interesTotal) {
		this.interesTotal = interesTotal;
	}

	public String getLineaNegocio() {
		return lineaNegocio;
	}

	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}

	public Long getNroCuentaFinal() {
		return nroCuentaFinal;
	}

	public void setNroCuentaFinal(Long nroCuentaFinal) {
		this.nroCuentaFinal = nroCuentaFinal;
	}

	public Long getNroCuentaInicial() {
		return nroCuentaInicial;
	}

	public void setNroCuentaInicial(Long nroCuentaInicial) {
		this.nroCuentaInicial = nroCuentaInicial;
	}

	public Long getNroCuotas() {
		return nroCuotas;
	}

	public void setNroCuotas(Long nroCuotas) {
		this.nroCuotas = nroCuotas;
	}

	public Long getNroCuotasFacturadas() {
		return nroCuotasFacturadas;
	}

	public void setNroCuotasFacturadas(Long nroCuotasFacturadas) {
		this.nroCuotasFacturadas = nroCuotasFacturadas;
	}

	public Long getNroCuotasFacturadasNoPagadas() {
		return nroCuotasFacturadasNoPagadas;
	}

	public void setNroCuotasFacturadasNoPagadas(
			Long nroCuotasFacturadasNoPagadas) {
		this.nroCuotasFacturadasNoPagadas = nroCuotasFacturadasNoPagadas;
	}

	public Long getNroCuotasFacturadasPagadas() {
		return nroCuotasFacturadasPagadas;
	}

	public void setNroCuotasFacturadasPagadas(Long nroCuotasFacturadasPagadas) {
		this.nroCuotasFacturadasPagadas = nroCuotasFacturadasPagadas;
	}

	public Long getNroServicioCompraCarteraFinal() {
		return nroServicioCompraCarteraFinal;
	}

	public void setNroServicioCompraCarteraFinal(
			Long nroServicioCompraCarteraFinal) {
		this.nroServicioCompraCarteraFinal = nroServicioCompraCarteraFinal;
	}

	public Long getNroServicioCompraCarteraInicial() {
		return nroServicioCompraCarteraInicial;
	}

	public void setNroServicioCompraCarteraInicial(
			Long nroServicioCompraCarteraInicial) {
		this.nroServicioCompraCarteraInicial = nroServicioCompraCarteraInicial;
	}

	public String getPlanServicioFinanciero() {
		return planServicioFinanciero;
	}

	public void setPlanServicioFinanciero(String planServicioFinanciero) {
		this.planServicioFinanciero = planServicioFinanciero;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getSocioNegocio() {
		return socioNegocio;
	}

	public void setSocioNegocio(String socioNegocio) {
		this.socioNegocio = socioNegocio;
	}

	public String getSucursalSocioNegocio() {
		return sucursalSocioNegocio;
	}

	public void setSucursalSocioNegocio(String sucursalSocioNegocio) {
		this.sucursalSocioNegocio = sucursalSocioNegocio;
	}

	public Long getTasaInteres() {
		return tasaInteres;
	}

	public void setTasaInteres(Long tasaInteres) {
		this.tasaInteres = tasaInteres;
	}

	public Long getValorCapitalPagado() {
		return valorCapitalPagado;
	}

	public void setValorCapitalPagado(Long valorCapitalPagado) {
		this.valorCapitalPagado = valorCapitalPagado;
	}

	public Long getValorCapitalPorFacturar() {
		return valorCapitalPorFacturar;
	}

	public void setValorCapitalPorFacturar(Long valorCapitalPorFacturar) {
		this.valorCapitalPorFacturar = valorCapitalPorFacturar;
	}

	public Long getValorCapitalTotal() {
		return valorCapitalTotal;
	}

	public void setValorCapitalTotal(Long valorCapitalTotal) {
		this.valorCapitalTotal = valorCapitalTotal;
	}

	public Long getValorCredito() {
		return valorCredito;
	}

	public void setValorCredito(Long valorCredito) {
		this.valorCredito = valorCredito;
	}

	public Long getValorFacturado() {
		return valorFacturado;
	}

	public void setValorFacturado(Long valorFacturado) {
		this.valorFacturado = valorFacturado;
	}

	public Long getValorFacturadoNoPagado() {
		return valorFacturadoNoPagado;
	}

	public void setValorFacturadoNoPagado(Long valorFacturadoNoPagado) {
		this.valorFacturadoNoPagado = valorFacturadoNoPagado;
	}

	public Long getIdCuenta() {
		return idCuenta;
	}

	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}

	public String getIdentifPersonaRelacionada() {
		return identifPersonaRelacionada;
	}

	public void setIdentifPersonaRelacionada(String identifPersonaRelacionada) {
		this.identifPersonaRelacionada = identifPersonaRelacionada;
	}

	public Long getIdIdentifPersonaRelacionada() {
		return idIdentifPersonaRelacionada;
	}

	public void setIdIdentifPersonaRelacionada(Long idIdentifPersonaRelacionada) {
		this.idIdentifPersonaRelacionada = idIdentifPersonaRelacionada;
	}

	public Long getIdLineaNegocio() {
		return idLineaNegocio;
	}

	public void setIdLineaNegocio(Long idLineaNegocio) {
		this.idLineaNegocio = idLineaNegocio;
	}

	public Long getIdPlanServicioFinanciero() {
		return idPlanServicioFinanciero;
	}

	public void setIdPlanServicioFinanciero(Long idPlanServicioFinanciero) {
		this.idPlanServicioFinanciero = idPlanServicioFinanciero;
	}

	public Long getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}

	public Long getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}

	public Long getIdSocioNegocio() {
		return idSocioNegocio;
	}

	public void setIdSocioNegocio(Long idSocioNegocio) {
		this.idSocioNegocio = idSocioNegocio;
	}

	public Long getIdSucursalSocioNegocio() {
		return idSucursalSocioNegocio;
	}

	public void setIdSucursalSocioNegocio(Long idSucursalSocioNegocio) {
		this.idSucursalSocioNegocio = idSucursalSocioNegocio;
	}

	public Long getIdTipoIdentifPersonaRelacionada() {
		return idTipoIdentifPersonaRelacionada;
	}

	public void setIdTipoIdentifPersonaRelacionada(
			Long idTipoIdentifPersonaRelacionada) {
		this.idTipoIdentifPersonaRelacionada = idTipoIdentifPersonaRelacionada;
	}

	public String getTipoIdentifPersonaRelacionada() {
		return tipoIdentifPersonaRelacionada;
	}

	public void setTipoIdentifPersonaRelacionada(
			String tipoIdentifPersonaRelacionada) {
		this.tipoIdentifPersonaRelacionada = tipoIdentifPersonaRelacionada;
	}
}
