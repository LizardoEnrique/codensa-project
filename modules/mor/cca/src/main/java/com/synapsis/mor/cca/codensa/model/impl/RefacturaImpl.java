/**
 * $Id: RefacturaImpl.java,v 1.3 2007/06/21 14:29:10 ar26557682 Exp $
 */
package com.synapsis.mor.cca.codensa.model.impl;

import java.util.Date;

import com.synapsis.mor.cca.codensa.model.Refactura;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * Modela el reporte y actualmente est� mapeado a una Vista
 * que realiza la consulta
 * 
 * @author Paola Attadio
 * 
 */
public class RefacturaImpl extends SynergiaBusinessObjectImpl implements
		Refactura {

	private static final long serialVersionUID = 1L;

	private Long id_cuenta;// no en la tabla
	private Long nroCuenta;
	private Long nroServicioCompraCartera;
	private String lineaNegocio;
	private String planServicioFinanciero;
	private String socioNegocio;
	private String sucursalSocioNegocio;
	private Long idTipoIdentifPersonaRelacionada;// no en la tabla
	private String tipoIdentifPersonaRelacionada;
	private Long idIdentifPersonaRelacionada;// no en la tabla
	private String identifPersonaRelacionada;
	private String tipoProducto;
	private Long nroSolicitudRefactura;
	private String nroOrdenRefactura;
	private Long idOrdenRefactura;// no en la tabla
	private Date fechaEjecutado;
	private Date fechaAprovado;
	private Long valorRefacturadoPorIntereses;
	private Long valorRefacturadoPorOtrosCargos;

	public Date getFechaAprovado() {
		return fechaAprovado;
	}

	public void setFechaAprovado(Date fechaAprovado) {
		this.fechaAprovado = fechaAprovado;
	}

	public Date getFechaEjecutado() {
		return fechaEjecutado;
	}

	public void setFechaEjecutado(Date fechaEjecutado) {
		this.fechaEjecutado = fechaEjecutado;
	}

	public Long getId_cuenta() {
		return id_cuenta;
	}

	public void setId_cuenta(Long id_cuenta) {
		this.id_cuenta = id_cuenta;
	}

	public String getIdentifPersonaRelacionada() {
		return identifPersonaRelacionada;
	}

	public void setIdentifPersonaRelacionada(String identifPersonaRelacionada) {
		this.identifPersonaRelacionada = identifPersonaRelacionada;
	}

	public Long getIdIdentifPersonaRelacionada() {
		return idIdentifPersonaRelacionada;
	}

	public void setIdIdentifPersonaRelacionada(Long idIdentifPersonaRelacionada) {
		this.idIdentifPersonaRelacionada = idIdentifPersonaRelacionada;
	}

	public Long getIdOrdenRefactura() {
		return idOrdenRefactura;
	}

	public void setIdOrdenRefactura(Long idOrdenRefactura) {
		this.idOrdenRefactura = idOrdenRefactura;
	}

	public Long getIdTipoIdentifPersonaRelacionada() {
		return idTipoIdentifPersonaRelacionada;
	}

	public void setIdTipoIdentifPersonaRelacionada(
			Long idTipoIdentifPersonaRelacionada) {
		this.idTipoIdentifPersonaRelacionada = idTipoIdentifPersonaRelacionada;
	}

	public String getLineaNegocio() {
		return lineaNegocio;
	}

	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getNroOrdenRefactura() {
		return nroOrdenRefactura;
	}

	public void setNroOrdenRefactura(String nroOrdenRefactura) {
		this.nroOrdenRefactura = nroOrdenRefactura;
	}

	public Long getNroServicioCompraCartera() {
		return nroServicioCompraCartera;
	}

	public void setNroServicioCompraCartera(Long nroServicioCompraCartera) {
		this.nroServicioCompraCartera = nroServicioCompraCartera;
	}

	public Long getNroSolicitudRefactura() {
		return nroSolicitudRefactura;
	}

	public void setNroSolicitudRefactura(Long nroSolicitudRefactura) {
		this.nroSolicitudRefactura = nroSolicitudRefactura;
	}

	public String getPlanServicioFinanciero() {
		return planServicioFinanciero;
	}

	public void setPlanServicioFinanciero(String planServicioFinanciero) {
		this.planServicioFinanciero = planServicioFinanciero;
	}

	public String getSocioNegocio() {
		return socioNegocio;
	}

	public void setSocioNegocio(String socioNegocio) {
		this.socioNegocio = socioNegocio;
	}

	public String getSucursalSocioNegocio() {
		return sucursalSocioNegocio;
	}

	public void setSucursalSocioNegocio(String sucursalSocioNegocio) {
		this.sucursalSocioNegocio = sucursalSocioNegocio;
	}

	public String getTipoIdentifPersonaRelacionada() {
		return tipoIdentifPersonaRelacionada;
	}

	public void setTipoIdentifPersonaRelacionada(
			String tipoIdentifPersonaRelacionada) {
		this.tipoIdentifPersonaRelacionada = tipoIdentifPersonaRelacionada;
	}

	public String getTipoProducto() {
		return tipoProducto;
	}

	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}

	public Long getValorRefacturadoPorIntereses() {
		return valorRefacturadoPorIntereses;
	}

	public void setValorRefacturadoPorIntereses(
			Long valorRefacturadoPorIntereses) {
		this.valorRefacturadoPorIntereses = valorRefacturadoPorIntereses;
	}

	public Long getValorRefacturadoPorOtrosCargos() {
		return valorRefacturadoPorOtrosCargos;
	}

	public void setValorRefacturadoPorOtrosCargos(
			Long valorRefacturadoPorOtrosCargos) {
		this.valorRefacturadoPorOtrosCargos = valorRefacturadoPorOtrosCargos;
	}
}