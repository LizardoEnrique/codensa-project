/**
 * $Id: ServicioFinancieroServiceImpl.java,v 1.7 2008/04/25 15:52:11 ar29261698 Exp $
 */
package com.synapsis.mor.cca.codensa.service.impl;

import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.suivant.arquitectura.core.exception.BusinessException;
import com.suivant.arquitectura.core.integration.Module;
import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;
import com.synapsis.mor.cca.codensa.model.ServicioCuenta;
import com.synapsis.mor.cca.codensa.model.impl.AjusteCreditoImpl;
import com.synapsis.mor.cca.codensa.model.impl.RestriccionImpl;
import com.synapsis.mor.cca.codensa.model.impl.ServicioActivoImpl;
import com.synapsis.mor.cca.codensa.service.ServicioFinancieroService;
import com.synapsis.synergia.core.service.impl.SynergiaServiceImpl;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @author paola
 * @author dbraccio
 * @version $Revision: 1.7 $ $Date: 2008/04/25 15:52:11 $
 */
public class ServicioFinancieroServiceImpl extends SynergiaServiceImpl
		implements ServicioFinancieroService {

	// *************************************************************************
	// *********** constructors
	// *************************************************************************
	/**
	 * default constructor
	 */
	public ServicioFinancieroServiceImpl() {
		super();
	}

	/**
	 * constructor with module
	 * 
	 * @param module
	 */
	public ServicioFinancieroServiceImpl(Module module) {
		super(module);
	}

	/**
	 * 
	 * @see com.synapsis.mor.cca.codensa.service.ServicioFinancieroService#transferir(java.lang.Long,
	 *      java.lang.Long, com.synapsis.mor.cca.codensa.model.ServicioCuenta)
	 */
	public void transferir(Long numeroCuentaOrigen, Long numeroCuentaDestino,
			ServicioCuenta servicioCuenta) throws BusinessException {
		// El sistema obtiene la financiación asociada al servicio
		// financiero indicado ...
		// Financiacion financiacion = servicioCuenta.getServicioFinanciero()
		// .getFinanciacion();
		// El sistema verifica que la cuenta ingresada no tenga restricción
		// de creación de servicios financieros para la línea de negocio del
		// servicio financiero indicado ...
		if (this.tieneRestricciones(numeroCuentaDestino).booleanValue()) {
			throw new BusinessException(
					"cca.serviciofinanciero.cuentaConRestricciones");
		}
		// El sistema valida que la cuenta destino tiene asociado un servicio
		// eléctrico activo ...
		if (!(this.tieneServiciosActivos(numeroCuentaDestino).booleanValue())) {
			throw new BusinessException(
					"cca.serviciofinanciero.tieneServiciosActivos");
		}
		// El sistema solicita a Financiación transferir la financiación
		// indicada a la cuenta destino informada ...

		// El sistema verifica que fue caducada la financiación de la cuenta
		// origen y que se generó un ajuste en crédito en la cuenta origen
		// para
		// cancelar la deuda facturada impaga asociada a la financiación ...
		if (!(this.seGeneroAjusteEnCuenta(servicioCuenta
				.getServicioFinanciero().getId()).booleanValue())) {
			throw new BusinessException(
					"cca.serviciofinanciero.cuantaSinAjusteGenerado");
		}
		// El sistema obtiene la financiación creada para la cuenta destino ...

		// El sistema crea un servicio financiero asociándolo a la cuenta
		// destino copiando exactamente los datos del servicio original ...

		// El sistema vincula la financiación obtenida al servicio financiero
		// recién creado ...

		// El sistema actualiza el cupo de crédito del servicio financiero
		// asignándole la cuenta destino ...
	}

	/**
	 * @see com.synapsis.mor.cca.codensa.service.impl.ServicioFinancieroService#consultaRestricciones(java.lang.Long)
	 */
	public List consultaRestricciones(Long numeroCuenta) {
		EqualQueryFilter filter = new EqualQueryFilter("nroCuenta",
				numeroCuenta);
		filter.setQueryFilter(super.getEmpresaQueryFilter());
		return super.getDAO(RestriccionImpl.class).findByCriteria(filter);
	}

	/**
	 * @see com.synapsis.mor.cca.codensa.service.impl.ServicioFinancieroService#tieneRestricciones(java.lang.Long)
	 */
	public Boolean tieneRestricciones(Long numeroCuenta) {
		EqualQueryFilter filter = new EqualQueryFilter("nroCuenta",
				numeroCuenta);
		filter.setQueryFilter(super.getEmpresaQueryFilter());
		return super.getDAO(RestriccionImpl.class).exists(filter);
	}

	/**
	 * @see com.synapsis.mor.cca.codensa.service.impl.ServicioFinancieroService#consultaServiciosActivos(java.lang.Long)
	 */
	public List consultaServiciosActivos(Long numeroCuenta) {
		EqualQueryFilter filter = new EqualQueryFilter("nroCuenta",
				numeroCuenta);
		filter.setQueryFilter(super.getEmpresaQueryFilter());
		return super.getDAO(ServicioActivoImpl.class).findByCriteria(filter);
	}

	/**
	 * @see com.synapsis.mor.cca.codensa.service.impl.ServicioFinancieroService#tieneServiciosActivos(java.lang.Long)
	 */
	public Boolean tieneServiciosActivos(Long numeroCuenta) {
		EqualQueryFilter filter = new EqualQueryFilter("nroCuenta",
				numeroCuenta);
		filter.setQueryFilter(super.getEmpresaQueryFilter());
		return super.getDAO(ServicioActivoImpl.class).exists(filter);
	}

	/**
	 * @see com.synapsis.mor.cca.codensa.service.ServicioFinancieroService#seGeneroAjusteEnCuenta(java.lang.Long)
	 */
	public Boolean seGeneroAjusteEnCuenta(Long idServicio) {
		EqualQueryFilter filter = new EqualQueryFilter("id", idServicio);
		filter.setQueryFilter(super.getEmpresaQueryFilter());
		return super.getDAO(AjusteCreditoImpl.class).exists(filter);
	}


	/**
	 * metodo para generar un codigo unico que luego es usado para identificar
	 * en la tabla los servicios pertenecientes a una operacion
	 * 
	 * @return
	 */
	public synchronized static String getCodigoOperacion() {
		return String.valueOf(Calendar.getInstance().getTimeInMillis());
	}

	/**
	 * arma la respuesta para enviar a presentacion. Si es ejecutada en su
	 * totalidad, retorn'ok', en caso de ser ejecutada en forma parcial, retorna
	 * los ids de los servicios que no se pudieron ilocalizar. 'id1;id2'.
	 * 
	 * @param originalResponse
	 * @return
	 */
	private String parseResponse(String originalResponse) {
		if (originalResponse.trim().toLowerCase().equalsIgnoreCase("ok"))
			return "ok";
		else
			return StringUtils.replace(StringUtils.removeStart(
					originalResponse.trim().toLowerCase(), "parcial").trim(),
					" ", ";");

	}

}
