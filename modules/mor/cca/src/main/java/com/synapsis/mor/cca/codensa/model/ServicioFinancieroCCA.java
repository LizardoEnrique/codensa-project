/**
 * 
 */
package com.synapsis.mor.cca.codensa.model;

import java.math.BigDecimal;
import java.util.Date;

import com.suivant.arquitectura.core.model.PersistentObject;
import com.synapsis.nuc.codensa.model.Servicio;
import com.synapsis.nuc.codensa.model.Workflow;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.5 $ $Date: 2007/07/10 13:55:49 $
 */
public interface ServicioFinancieroCCA extends PersistentObject, Servicio {

	public CupoCredito getCupoCredito();

	public void setCupoCredito(CupoCredito cupoCredito);

	public Date getFechaCompra();

	public void setFechaCompra(Date fechaCompra);

	public Financiacion getFinanciacion();

	public void setFinanciacion(Financiacion financiacion);

	public String getObservaciones();

	public void setObservaciones(String observaciones);

	public Long getSocioNegocio();

	public void setSocioNegocio(Long socioNegocio);

	public Long getSucursalSocioNegocio();

	public void setSucursalSocioNegocio(Long sucursalSocioNegocio);

	public BigDecimal getValorCompra();

	public void setValorCompra(BigDecimal valorCompra);

	public Workflow getWorkFlow();

	public void setWorkFlow(Workflow workFlow);
}