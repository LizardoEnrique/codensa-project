/**
 * $Id: TotalCompraImpl.java,v 1.2 2007/09/10 21:50:19 ar29261698 Exp $
 */
package com.synapsis.mor.cca.codensa.model.impl;

import java.util.Date;

import com.synapsis.mor.cca.codensa.model.TotalCompra;
import com.synapsis.synergia.core.model.impl.SynergiaPersistentObjectImpl;

/**
 * 
 * @author Paola Attadio
 * @version $Revision: 1.2 $
 * 
 */
/**
 * @author Paola Attadio
 * 
 */
/**
 * @author Paola Attadio
 * 
 */
public class TotalCompraImpl extends SynergiaPersistentObjectImpl implements
		TotalCompra {

	private static final long serialVersionUID = 1L;

	private Long idCupoCredito;
	private Long valorConsumo;
	private String estado;
	private Date fechaCompra;
	private Date fechaCreacion;
	private Date fechaCarga;
	private Long idEntidadAsociada;
	private Long idSucursalEntidadAsociada;
	private Long cantidadCuotas;
    private String ntoAprobacion;
	private String nombreArchivo;

	public Long getCantidadCuotas() {
		return cantidadCuotas;
	}

	public void setCantidadCuotas(Long cantidadCuotas) {
		this.cantidadCuotas = cantidadCuotas;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaCarga() {
		return fechaCarga;
	}

	public void setFechaCarga(Date fechaCarga) {
		this.fechaCarga = fechaCarga;
	}

	public Date getFechaCompra() {
		return fechaCompra;
	}

	public void setFechaCompra(Date fechaCompra) {
		this.fechaCompra = fechaCompra;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Long getIdCupoCredito() {
		return idCupoCredito;
	}

	public void setIdCupoCredito(Long idCupoCredito) {
		this.idCupoCredito = idCupoCredito;
	}

	public Long getIdEntidadAsociada() {
		return idEntidadAsociada;
	}

	public void setIdEntidadAsociada(Long idEntidadAsociada) {
		this.idEntidadAsociada = idEntidadAsociada;
	}

	public Long getIdSucursalEntidadAsociada() {
		return idSucursalEntidadAsociada;
	}

	public void setIdSucursalEntidadAsociada(Long idSucursalEntidadAsociada) {
		this.idSucursalEntidadAsociada = idSucursalEntidadAsociada;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getNtoAprobacion() {
		return ntoAprobacion;
	}

	public void setNtoAprobacion(String ntoAprobacion) {
		this.ntoAprobacion = ntoAprobacion;
	}

	public Long getValorConsumo() {
		return valorConsumo;
	}

	public void setValorConsumo(Long valorConsumo) {
		this.valorConsumo = valorConsumo;
	}

	public String getEntityName() {
		return "TotalCompra";
	}
}
