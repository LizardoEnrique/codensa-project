/**
 * $Id: Recaudo.java,v 1.4 2007/06/29 19:27:58 ar26557682 Exp $
 */
package com.synapsis.mor.cca.codensa.model;

import java.util.Date;

import com.suivant.arquitectura.core.model.BusinessObject;

/**
 * Modela el reporte y actualmente est� mapeado a una Vista
 * que realiza la consulta
 * 
 * @author Paola Attadio
 * 
 */
public interface Recaudo extends BusinessObject {

	public String getCargoCapitalAmortizado();

	public void setCargoCapitalAmortizado(String cargoCapitalAmortizado);

	public String getCargoInteresAmortizado();

	public void setCargoInteresAmortizado(String cargoInteresAmortizado);

	public String getCargoInteresMoraAmortizado();

	public void setCargoInteresMoraAmortizado(String cargoInteresMoraAmortizado);

	public String getEstadoServicioCompraCartera();

	public void setEstadoServicioCompraCartera(
			String estadoServicioCompraCartera);

	public Date getFechaCompra();

	public void setFechaCompra(Date fechaCompra);

	public Date getFechaCreacion();

	public void setFechaCreacion(Date fechaCreacion);

	public Date getFechaIngresoSistema();

	public void setFechaIngresoSistema(Date fechaIngresoSistema);

	public Date getFechaPago();

	public void setFechaPago(Date fechaPago);

	public Date getFechaProceso();

	public void setFechaProceso(Date fechaProceso);

	public Long getIdCuenta();

	public void setIdCuenta(Long idCuenta);

	public String getIdentifPersonaRelacionada();

	public void setIdentifPersonaRelacionada(String identifPersonaRelacionada);

	public Long getIdIdentifPersonaRelacionada();

	public void setIdIdentifPersonaRelacionada(Long idIdentifPersonaRelacionada);

	public Long getIdTipoIdentifPersonaRelacionada();

	public void setIdTipoIdentifPersonaRelacionada(
			Long idTipoIdentifPersonaRelacionada);

	public String getLineaNegocio();

	public void setLineaNegocio(String lineaNegocio);

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public Long getNroCuotas();

	public void setNroCuotas(Long nroCuotas);

	public Long getNroCuotasRecaudadas();

	public void setNroCuotasRecaudadas(Long nroCuotasRecaudadas);

	public Long getNroServicioCompraCartera();

	public void setNroServicioCompraCartera(Long nroServicioCompraCartera);

	public String getPlanServicioFinanciero();

	public void setPlanServicioFinanciero(String planServicioFinanciero);

	public String getProducto();

	public void setProducto(String producto);

	public String getSocioNegocio();

	public void setSocioNegocio(String socioNegocio);

	public String getSucursalSocioNegocio();

	public void setSucursalSocioNegocio(String sucursalSocioNegocio);

	public String getTipoIdentifPersonaRelacionada();

	public void setTipoIdentifPersonaRelacionada(
			String tipoIdentifPersonaRelacionada);

	public Long getValorCapitalAmortizado();

	public void setValorCapitalAmortizado(Long valorCapitalAmortizado);

	public Long getValorInteresAmortizado();

	public void setValorInteresAmortizado(Long valorInteresAmortizado);

	public Long getValorInteresMoraAmortizado();

	public void setValorInteresMoraAmortizado(Long valorInteresMoraAmortizado);
}
