/**
 * $Id: PlanServiciosFinancierosImpl.java,v 1.3 2007/09/10 21:50:19 ar29261698 Exp $
 */
package com.synapsis.mor.cca.codensa.model.impl;

import java.util.Date;

import com.synapsis.mor.cca.codensa.model.PlanServiciosFinancieros;
import com.synapsis.synergia.core.model.impl.SynergiaPersistentObjectImpl;

/**
 * 
 * @author Paola Attadio
 * 
 */
public class PlanServiciosFinancierosImpl extends SynergiaPersistentObjectImpl
		implements PlanServiciosFinancieros {

	private static final long serialVersionUID = 1L;

	private String codigo;
	private char activo;
	private String descripcion;
	private Long lineaNegocio;
	private Date fechaInicioVigencia;
	private Date fechaFinVigencia;
	private Long tipoFinanciacion;
	//private Long idConfConvenio; por ahora no lo mapeamos porque en el modelo de KND parece no utilizarse ...

	public char getActivo() {
		return activo;
	}

	public void setActivo(char activo) {
		this.activo = activo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	public Long getLineaNegocio() {
		return lineaNegocio;
	}

	public void setLineaNegocio(Long lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}

	public Long getTipoFinanciacion() {
		return tipoFinanciacion;
	}

	public void setTipoFinanciacion(Long tipoFinanciacion) {
		this.tipoFinanciacion = tipoFinanciacion;
	}

	public String getEntityName() {
		return "PlanServicioFinanciero";
	}
}
