/**
 * $Id: ReporteServicioFinancieroBB.java,v 1.10 2008/01/22 14:28:39 ar26088269 Exp $
 */
package com.synapsis.mor.cca.codensa.web;

import java.util.Date;

import org.apache.myfaces.custom.datatable.ext.HtmlDataTable;
import org.apache.myfaces.custom.service.UIService;


/**
 * CU-CCA007
 * 
 * @author Paola Attadio
 *
 */
public class ReporteServicioFinancieroBB {
	
	/**
	 * property para setear la cantidad de registros para los combos
	 */
	private Integer comboMaxResults = new Integer(100);
	/**
	 * Comment for <code>findByCriteriaService</code> Servicio para realizar
	 * la busqueda
	 */
	private UIService findByCriteriaService;
	
	/**
	 * Comment for <code>dataTable</code> Tabla en la cual se dejan los
	 * resultados de la busqueda ralizada. FIXME: [dbraccio]necesitamos la tabla
	 * para poder refrescarla
	 */
	private HtmlDataTable dataTable;
	
	private Date fechaDesde;
	private Date fechaHasta;
	private String tipoIdentifPersonaRelacionada; // CODIGO: dni, etc ...
	private String identifPersonaRelacionada;
	private Long idSocioNegocio; 
	private Date fechaCompraDesde;
	private Date fechaCompraHasta;
	private Long idSocioSucursal;
	private String nroAutorizacion;
	private String nroAutorizacionPago;
	private String idEstadoServicioFinanciero;
	private String pdp;
	
	public void search() {
		this.getFindByCriteriaService().execute();
		this.getDataTable().setFirst(0);
	}
	
	public Date getFechaCompraDesde() {
		return fechaCompraDesde;
	}
	public void setFechaCompraDesde(Date fechaCompraDesde) {
		this.fechaCompraDesde = fechaCompraDesde;
	}
	public Date getFechaCompraHasta() {
		return fechaCompraHasta;
	}
	public void setFechaCompraHasta(Date fechaCompraHasta) {
		this.fechaCompraHasta = fechaCompraHasta;
	}
	public String getIdEstadoServicioFinanciero() {
		return idEstadoServicioFinanciero;
	}

	public void setIdEstadoServicioFinanciero(String idEstadoServicioFinanciero) {
		this.idEstadoServicioFinanciero = idEstadoServicioFinanciero;
	}
	public String getNroAutorizacion() {
		return nroAutorizacion;
	}

	public void setNroAutorizacion(String nroAutorizacion) {
		this.nroAutorizacion = nroAutorizacion;
	}
	public String getPdp() {
		return pdp;
	}
	public void setPdp(String pdp) {
		this.pdp = pdp;
	}
	public Long getIdSocioSucursal() {
		return idSocioSucursal;
	}
	public void setIdSocioSucursal(Long idSocioSucursal) {
		this.idSocioSucursal = idSocioSucursal;
	}
	public Date getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public Date getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public Long getIdSocioNegocio() {
		return idSocioNegocio;
	}
	public void setIdSocioNegocio(Long idSocioNegocio) {
		this.idSocioNegocio = idSocioNegocio;
	}
	public String getIdentifPersonaRelacionada() {
		return identifPersonaRelacionada;
	}
	public void setIdentifPersonaRelacionada(String identifPersonaRelacionada) {
		this.identifPersonaRelacionada = identifPersonaRelacionada;
	}
	public String getTipoIdentifPersonaRelacionada() {
		return tipoIdentifPersonaRelacionada;
	}
	public void setTipoIdentifPersonaRelacionada(
			String tipoIdentifPersonaRelacionada) {
		this.tipoIdentifPersonaRelacionada = tipoIdentifPersonaRelacionada;
	}
	public Integer getComboMaxResults() {
		return comboMaxResults;
	}
	public void setComboMaxResults(Integer comboMaxResults) {
		this.comboMaxResults = comboMaxResults;
	}
	public HtmlDataTable getDataTable() {
		return dataTable;
	}
	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}
	public UIService getFindByCriteriaService() {
		return findByCriteriaService;
	}
	public void setFindByCriteriaService(UIService findByCriteriaService) {
		this.findByCriteriaService = findByCriteriaService;
	}

	public String getNroAutorizacionPago() {
		return nroAutorizacionPago;
	}

	public void setNroAutorizacionPago(String nroAutorizacionPago) {
		this.nroAutorizacionPago = nroAutorizacionPago;
	}
}
