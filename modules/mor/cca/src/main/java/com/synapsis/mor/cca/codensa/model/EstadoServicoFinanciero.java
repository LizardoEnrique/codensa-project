/**
 * $Id: EstadoServicoFinanciero.java,v 1.2 2007/06/29 13:19:35 ar26557682 Exp $
 */
package com.synapsis.mor.cca.codensa.model;

import com.suivant.arquitectura.core.model.BusinessObject;

/**
 * Modela el reporte y actualmente est� mapeado a una Vista
 * que realiza la consulta
 * 
 * @author Paola Attadio
 *
 */
public interface EstadoServicoFinanciero extends BusinessObject {

	public String getEstado();
	public void setEstado(String estado);
}
