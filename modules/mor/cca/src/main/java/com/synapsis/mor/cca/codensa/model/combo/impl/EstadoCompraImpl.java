/**
 * 
 */
package com.synapsis.mor.cca.codensa.model.combo.impl;

import com.synapsis.mor.cca.codensa.model.combo.EstadoCompra;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author dbraccio - Suivant
 *	02/07/2008
 */
public class EstadoCompraImpl extends SynergiaBusinessObjectImpl implements EstadoCompra  {
	
	/**
	 * ID	NUMBER	
ID_EMPRESA	NUMBER(18,0)	
ESTADO	VARCHAR2(40 Bytes)	
	 */
	
	private String estado;

	/* (non-Javadoc)
	 * @see com.synapsis.mor.cca.codensa.model.combo.impl.EstadoCompra#getEstado()
	 */
	public String getEstado() {
		return estado;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.mor.cca.codensa.model.combo.impl.EstadoCompra#setEstado(java.lang.String)
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

}
