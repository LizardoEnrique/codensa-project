/**
 * $Id: CarteraMorosa.java,v 1.5 2008/05/08 21:19:44 ar32272876 Exp $
 */
package com.synapsis.mor.cca.codensa.model;

import java.util.Date;

import com.suivant.arquitectura.core.model.BusinessObject;

/**
 * Modela el reporte y actualmente est� mapeado a una Vista que realiza la
 * consulta de compras totales
 * 
 * @author Paola Attadio
 * @version $Revision: 1.5 $
 * 
 */
public interface CarteraMorosa extends BusinessObject {

	public Long getDiasAtraso();

	public void setDiasAtraso(Long diasAtraso);

	public Date getFechaCompra();

	public void setFechaCompra(Date fechaCompra);

	public Date getFechaCreacion();

	public void setFechaCreacion(Date fechaCreacion);

	public Date getFechaUltimoCambioEstado();

	public void setFechaUltimoCambioEstado(Date fechaUltimoCambioEstado);

	public Long getInteresFacturado();

	public void setInteresFacturado(Long interesFacturado);

	public Long getInteresFacturadoNoPagado();

	public void setInteresFacturadoNoPagado(Long interesFacturadoNoPagado);

	public Long getInteresFacturadoPagado();

	public void setInteresFacturadoPagado(Long interesFacturadoPagado);

	public Long getInteresMoraFacturado();

	public void setInteresMoraFacturado(Long interesMoraFacturado);

	public Long getInteresMoraFacturadoNoPagado();

	public void setInteresMoraFacturadoNoPagado(
			Long interesMoraFacturadoNoPagado);

	public Long getInteresMoraPagado();

	public void setInteresMoraPagado(Long interesMoraPagado);

	public Long getInteresTotal();

	public void setInteresTotal(Long interesTotal);

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public Long getNroCuotas();

	public void setNroCuotas(Long nroCuotas);

	public Long getNroCuotasFacturadas();

	public void setNroCuotasFacturadas(Long nroCuotasFacturadas);

	public Long getNroCuotasFacturadasNoPagadas();

	public void setNroCuotasFacturadasNoPagadas(
			Long nroCuotasFacturadasNoPagadas);

	public Long getNroCuotasFacturadasPagadas();

	public void setNroCuotasFacturadasPagadas(Long nroCuotasFacturadasPagadas);

	public Long getNroServicioCompraCartera();

	public void setNroServicioCompraCartera(Long nroServicioCompraCartera);

	public Long getTasaInteres();

	public void setTasaInteres(Long tasaInteres);

	public Long getValorCapitalPagado();

	public void setValorCapitalPagado(Long valorCapitalPagado);

	public Long getValorCapitalTotal();

	public void setValorCapitalTotal(Long valorCapitalTotal);

	public Long getValorCredito();

	public void setValorCredito(Long valorCredito);

	public Long getValorFacturado();

	public void setValorFacturado(Long valorFacturado);

	public Long getValorFacturadoNoPagado();

	public void setValorFacturadoNoPagado(Long valorFacturadoNoPagado);

	public String getLineaNegocio();

	public void setLineaNegocio(String lineaNegocio);

	public String getPlanServicioFinanciero();

	public void setPlanServicioFinanciero(String planServicioFinanciero);

	public String getSocioNegocio();

	public void setSocioNegocio(String socioNegocio);

	public String getSucursalSocioNegocio();

	public void setSucursalSocioNegocio(String sucursalSocioNegocio);

	public Long getIdCuenta();

	public void setIdCuenta(Long idCuenta);

	public Long getIdIdentifPersonaRelacionada();

	public void setIdIdentifPersonaRelacionada(Long idIdentifPersonaRelacionada);

	public Long getIdLineaNegocio();

	public void setIdLineaNegocio(Long idLineaNegocio);

	public Long getIdPlanServicioFinanciero();

	public void setIdPlanServicioFinanciero(Long idPlanServicioFinanciero);

	public Long getIdSocioNegocio();

	public void setIdSocioNegocio(Long idSocioNegocio);

	public Long getIdTipoIdentifPersonaRelacionada();

	public void setIdTipoIdentifPersonaRelacionada(
			Long idTipoIdentifPersonaRelacionada);

	public String getTipoIdentifPersonaRelacionada();

	public void setTipoIdentifPersonaRelacionada(
			String tipoIdentifPersonaRelacionada);

	public void setIdentifPersonaRelacionada(String identifPersonaRelacionada);

	public String getIdentifPersonaRelacionada();

	public Date getFechaCarga();

	public void setFechaCarga(Date fechaCarga);
}
