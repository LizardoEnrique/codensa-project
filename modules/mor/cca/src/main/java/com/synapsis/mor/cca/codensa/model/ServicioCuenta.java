/**
 * 
 */
package com.synapsis.mor.cca.codensa.model;

import com.suivant.arquitectura.core.model.BusinessObject;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.3 $ $Date: 2007/06/29 13:22:45 $
 */
public interface ServicioCuenta extends BusinessObject {

	/**
	 * Getter of the property <tt>nroCuenta</tt>
	 * 
	 * @return Returns the nroCuenta.
	 * @uml.property name="nroCuenta"
	 */
	public abstract Long getNroCuenta();

	/**
	 * Setter of the property <tt>nroCuenta</tt>
	 * 
	 * @param nroCuenta
	 *            The nroCuenta to set.
	 * @uml.property name="nroCuenta"
	 */
	public abstract void setNroCuenta(Long nroCuenta);

	/**
	 * Getter of the property <tt>nroServicio</tt>
	 * 
	 * @return Returns the nroServicio.
	 * @uml.property name="nroServicio"
	 */
	public abstract Long getNroServicio();

	/**
	 * Setter of the property <tt>nroServicio</tt>
	 * 
	 * @param nroServicio
	 *            The nroServicio to set.
	 * @uml.property name="nroServicio"
	 */
	public abstract void setNroServicio(Long nroServicio);

	/**
	 * Getter of the property <tt>idPlanServicioFinanciero</tt>
	 * 
	 * @return Returns the idPlanServicioFinanciero.
	 * @uml.property name="idPlanServicioFinanciero"
	 */
	public abstract Long getIdPlanServicioFinanciero();

	/**
	 * Setter of the property <tt>idPlanServicioFinanciero</tt>
	 * 
	 * @param idPlanServicioFinanciero
	 *            The idPlanServicioFinanciero to set.
	 * @uml.property name="idPlanServicioFinanciero"
	 */
	public abstract void setIdPlanServicioFinanciero(
			Long idPlanServicioFinanciero);

	/**
	 * Getter of the property <tt>plan</tt>
	 * 
	 * @return Returns the plan.
	 * @uml.property name="plan"
	 */
	public abstract String getPlan();

	/**
	 * Setter of the property <tt>plan</tt>
	 * 
	 * @param plan
	 *            The plan to set.
	 * @uml.property name="plan"
	 */
	public abstract void setPlan(String plan);

	/**
	 * Getter of the property <tt>idLineaNegocio</tt>
	 * 
	 * @return Returns the idLineaNegocio.
	 * @uml.property name="idLineaNegocio"
	 */
	public abstract Long getIdLineaNegocio();

	/**
	 * Setter of the property <tt>idLineaNegocio</tt>
	 * 
	 * @param idLineaNegocio
	 *            The idLineaNegocio to set.
	 * @uml.property name="idLineaNegocio"
	 */
	public abstract void setIdLineaNegocio(Long idLineaNegocio);

	/**
	 * Getter of the property <tt>linea</tt>
	 * 
	 * @return Returns the linea.
	 * @uml.property name="linea"
	 */
	public abstract String getLinea();

	/**
	 * Setter of the property <tt>linea</tt>
	 * 
	 * @param linea
	 *            The linea to set.
	 * @uml.property name="linea"
	 */
	public abstract void setLinea(String linea);

	/**
	 * Getter of the property <tt>idEntidadAsociada</tt>
	 * 
	 * @return Returns the idEntidadAsociada.
	 * @uml.property name="idEntidadAsociada"
	 */
	public abstract Long getIdEntidadAsociada();

	/**
	 * Setter of the property <tt>idEntidadAsociada</tt>
	 * 
	 * @param idEntidadAsociada
	 *            The idEntidadAsociada to set.
	 * @uml.property name="idEntidadAsociada"
	 */
	public abstract void setIdEntidadAsociada(Long idEntidadAsociada);

	/**
	 * Getter of the property <tt>nombreSocio</tt>
	 * 
	 * @return Returns the nombreSocio.
	 * @uml.property name="nombreSocio"
	 */
	public abstract String getNombreSocio();

	/**
	 * Setter of the property <tt>nombreSocio</tt>
	 * 
	 * @param nombreSocio
	 *            The nombreSocio to set.
	 * @uml.property name="nombreSocio"
	 */
	public abstract void setNombreSocio(String nombreSocio);

	/**
	 * Getter of the property <tt>idMarcaProducto</tt>
	 * 
	 * @return Returns the idMarcaProducto.
	 * @uml.property name="idMarcaProducto"
	 */
	public abstract Long getIdMarcaProducto();

	/**
	 * Setter of the property <tt>idMarcaProducto</tt>
	 * 
	 * @param idMarcaProducto
	 *            The idMarcaProducto to set.
	 * @uml.property name="idMarcaProducto"
	 */
	public abstract void setIdMarcaProducto(Long idMarcaProducto);

	/**
	 * Getter of the property <tt>marcaProducto</tt>
	 * 
	 * @return Returns the marcaProducto.
	 * @uml.property name="marcaProducto"
	 */
	public abstract String getMarcaProducto();

	/**
	 * Setter of the property <tt>marcaProducto</tt>
	 * 
	 * @param marcaProducto
	 *            The marcaProducto to set.
	 * @uml.property name="marcaProducto"
	 */
	public abstract void setMarcaProducto(String marcaProducto);

	/**
	 * Getter of the property <tt>servicioFinanciero</tt>
	 * 
	 * @return Returns the servicioFinanciero.
	 * @uml.property name="servicioFinanciero"
	 */
	public ServicioFinancieroCCA getServicioFinanciero();

	/**
	 * Setter of the property <tt>servicioFinanciero</tt>
	 * 
	 * @param servicioFinanciero
	 *            The servicioFinanciero to set.
	 * @uml.property name="servicioFinanciero"
	 */
	public void setServicioFinanciero(ServicioFinancieroCCA servicioFinanciero);

}