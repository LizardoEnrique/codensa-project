package com.synapsis.mor.cca.codensa.model;

public interface Restriccion {

	/**
	 * Getter of the property <tt>nroCuenta</tt>
	 * 
	 * @return Returns the nroCuenta.
	 * @uml.property name="nroCuenta"
	 */
	public abstract Long getNroCuenta();

	/**
	 * Setter of the property <tt>nroCuenta</tt>
	 * 
	 * @param nroCuenta
	 *            The nroCuenta to set.
	 * @uml.property name="nroCuenta"
	 */
	public abstract void setNroCuenta(Long nroCuenta);

	/**
	 * Getter of the property <tt>idRestriccion</tt>
	 * 
	 * @return Returns the idRestriccion.
	 * @uml.property name="idRestriccion"
	 */
	public abstract Long getIdRestriccion();

	/**
	 * Setter of the property <tt>idRestriccion</tt>
	 * 
	 * @param idRestriccion
	 *            The idRestriccion to set.
	 * @uml.property name="idRestriccion"
	 */
	public abstract void setIdRestriccion(Long idRestriccion);

	/**
	 * Getter of the property <tt>tipoRestriccion</tt>
	 * 
	 * @return Returns the tipoRestriccion.
	 * @uml.property name="tipoRestriccion"
	 */
	public abstract String getTipoRestriccion();

	/**
	 * Setter of the property <tt>tipoRestriccion</tt>
	 * 
	 * @param tipoRestriccion
	 *            The tipoRestriccion to set.
	 * @uml.property name="tipoRestriccion"
	 */
	public abstract void setTipoRestriccion(String tipoRestriccion);

}