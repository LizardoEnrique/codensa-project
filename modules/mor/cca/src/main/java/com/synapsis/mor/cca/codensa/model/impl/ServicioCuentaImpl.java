/**
 * 
 */
package com.synapsis.mor.cca.codensa.model.impl;

import com.synapsis.mor.cca.codensa.model.ServicioCuenta;
import com.synapsis.mor.cca.codensa.model.ServicioFinancieroCCA;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.3 $ $Date: 2007/06/29 13:23:09 $
 */
public class ServicioCuentaImpl extends SynergiaBusinessObjectImpl implements
		ServicioCuenta {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @uml.property name="nroCuenta"
	 */
	private Long nroCuenta;

	/**
	 * @uml.property name="servicioFinanciero"
	 */
	private ServicioFinancieroCCA servicioFinanciero;

	/**
	 * @uml.property name="nroServicio"
	 */
	private Long nroServicio;

	/**
	 * @uml.property name="idPlanServicioFinanciero"
	 */
	private Long idPlanServicioFinanciero;

	/**
	 * @uml.property name="plan"
	 */
	private String plan;

	/**
	 * @uml.property name="idLineaNegocio"
	 */
	private Long idLineaNegocio;

	/**
	 * @uml.property name="linea"
	 */
	private String linea;

	/**
	 * @uml.property name="idEntidadAsociada"
	 */
	private Long idEntidadAsociada;

	/**
	 * @uml.property name="nombreSocio"
	 */
	private String nombreSocio;

	/**
	 * @uml.property name="idMarcaProducto"
	 */
	private Long idMarcaProducto;

	/**
	 * @uml.property name="marcaProducto"
	 */
	private String marcaProducto;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioCuenta#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return this.nroCuenta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioCuenta#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.ServicioCuenta#getServicioFinanciero()
	 */
	public ServicioFinancieroCCA getServicioFinanciero() {
		return this.servicioFinanciero;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.ServicioCuenta#setServicioFinanciero(com.synapsis.mor.cca.codensa.model.ServicioFinancieroCCA)
	 */
	public void setServicioFinanciero(ServicioFinancieroCCA servicioFinanciero) {
		this.servicioFinanciero = servicioFinanciero;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioCuenta#getNroServicio()
	 */
	public Long getNroServicio() {
		return this.nroServicio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioCuenta#setNroServicio(java.lang.Long)
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioCuenta#getIdPlanServicioFinanciero()
	 */
	public Long getIdPlanServicioFinanciero() {
		return this.idPlanServicioFinanciero;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioCuenta#setIdPlanServicioFinanciero(java.lang.Long)
	 */
	public void setIdPlanServicioFinanciero(Long idPlanServicioFinanciero) {
		this.idPlanServicioFinanciero = idPlanServicioFinanciero;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioCuenta#getPlan()
	 */
	public String getPlan() {
		return this.plan;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioCuenta#setPlan(java.lang.String)
	 */
	public void setPlan(String plan) {
		this.plan = plan;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioCuenta#getIdLineaNegocio()
	 */
	public Long getIdLineaNegocio() {
		return this.idLineaNegocio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioCuenta#setIdLineaNegocio(java.lang.Long)
	 */
	public void setIdLineaNegocio(Long idLineaNegocio) {
		this.idLineaNegocio = idLineaNegocio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioCuenta#getLinea()
	 */
	public String getLinea() {
		return this.linea;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioCuenta#setLinea(java.lang.String)
	 */
	public void setLinea(String linea) {
		this.linea = linea;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioCuenta#getIdEntidadAsociada()
	 */
	public Long getIdEntidadAsociada() {
		return this.idEntidadAsociada;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioCuenta#setIdEntidadAsociada(java.lang.Long)
	 */
	public void setIdEntidadAsociada(Long idEntidadAsociada) {
		this.idEntidadAsociada = idEntidadAsociada;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioCuenta#getNombreSocio()
	 */
	public String getNombreSocio() {
		return this.nombreSocio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioCuenta#setNombreSocio(java.lang.String)
	 */
	public void setNombreSocio(String nombreSocio) {
		this.nombreSocio = nombreSocio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioCuenta#getIdMarcaProducto()
	 */
	public Long getIdMarcaProducto() {
		return this.idMarcaProducto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioCuenta#setIdMarcaProducto(java.lang.Long)
	 */
	public void setIdMarcaProducto(Long idMarcaProducto) {
		this.idMarcaProducto = idMarcaProducto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioCuenta#getMarcaProducto()
	 */
	public String getMarcaProducto() {
		return this.marcaProducto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.ServicioCuenta#setMarcaProducto(java.lang.String)
	 */
	public void setMarcaProducto(String marcaProducto) {
		this.marcaProducto = marcaProducto;
	}

}
