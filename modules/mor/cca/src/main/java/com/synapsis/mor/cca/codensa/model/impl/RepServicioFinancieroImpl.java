/**
 * $Id: RepServicioFinancieroImpl.java,v 1.8 2008/06/14 01:28:50 ar32272876 Exp $
 */
package com.synapsis.mor.cca.codensa.model.impl;

import java.util.Date;

import com.synapsis.mor.cca.codensa.model.RepServicioFinanciero;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * Modela el reporte y actualmente est� mapeado a una Vista
 * que realiza la consulta
 * 
 * @author Paola Attadio
 * @version $Revision: 1.8 $
 * 
 */
public class RepServicioFinancieroImpl extends SynergiaBusinessObjectImpl
		implements RepServicioFinanciero {

	private static final long serialVersionUID = 1L;

	private Long nroServicio;
	private Long idCompraTotal; // no en la tabla
	private String nroAutorizacion;
	private String nroAutorizacionPago;
	private Date fechaCarga;
	private Long idLineaNegocio; //no en la tabla 
	private String lineaNegocio;
	private String plan;
	private Long idPlan;// no en la tabla
	private Long idProducto; // no en la tabla
	private String producto;
	private String socioNegocio;
	private Long idSocioNegocio;// no en la tabla
	private Long idSucursalSocioNegocio;// no en la tabla	
	private String sucursalSocioNegocio;
	private String estadoServicioCompraCartera;
	private Long idEstadoProceso; //Estado del registro de detalle
	private String estadoProceso; //Estado del registro de detalle
	private Long valorFinanciado;
	private String pdp;
	private Date fechaCompra;
	private Long idTipoIdentifPersonaRelacionada;// no en la tabla
	private String tipoIdentifPersonaRelacionada; // CODIGO: dni, etc ...
	private Long idIdentifPersonaRelacionada;// no en la tabla
	private String identifPersonaRelacionada;
	

	public String getEstadoProceso() {
		return estadoProceso;
	}
	public void setEstadoProceso(String estadoProceso) {
		this.estadoProceso = estadoProceso;
	}
	public Date getFechaCarga() {
		return fechaCarga;
	}
	public void setFechaCarga(Date fechaCarga) {
		this.fechaCarga = fechaCarga;
	}
	public Long getIdPlan() {
		return idPlan;
	}
	public void setIdPlan(Long idPlan) {
		this.idPlan = idPlan;
	}
	public Long getIdSocioNegocio() {
		return idSocioNegocio;
	}
	public void setIdSocioNegocio(Long idSocioNegocio) {
		this.idSocioNegocio = idSocioNegocio;
	}
	public String getLineaNegocio() {
		return lineaNegocio;
	}
	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}
	public String getNroAutorizacion() {
		return nroAutorizacion;
	}
	public void setNroAutorizacion(String nroAutorizacion) {
		this.nroAutorizacion = nroAutorizacion;
	}
	public Long getNroServicio() {
		return nroServicio;
	}
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	public String getPdp() {
		return pdp;
	}
	public void setPdp(String pdp) {
		this.pdp = pdp;
	}
	public String getPlan() {
		return plan;
	}
	public void setPlan(String plan) {
		this.plan = plan;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getSocioNegocio() {
		return socioNegocio;
	}
	public void setSocioNegocio(String socioNegocio) {
		this.socioNegocio = socioNegocio;
	}
	public String getSucursalSocioNegocio() {
		return sucursalSocioNegocio;
	}
	public void setSucursalSocioNegocio(String sucursalSocioNegocio) {
		this.sucursalSocioNegocio = sucursalSocioNegocio;
	}
	public Long getValorFinanciado() {
		return valorFinanciado;
	}
	public void setValorFinanciado(Long valorFinanciado) {
		this.valorFinanciado = valorFinanciado;
	}
	public String getEstadoServicioCompraCartera() {
		return estadoServicioCompraCartera;
	}
	public void setEstadoServicioCompraCartera(String estadoServicioCompraCartera) {
		this.estadoServicioCompraCartera = estadoServicioCompraCartera;
	}
	public Long getIdEstadoProceso() {
		return idEstadoProceso;
	}
	public void setIdEstadoProceso(Long idEstadoProceso) {
		this.idEstadoProceso = idEstadoProceso;
	}
	public Long getIdLineaNegocio() {
		return idLineaNegocio;
	}
	public void setIdLineaNegocio(Long idLineaNegocio) {
		this.idLineaNegocio = idLineaNegocio;
	}
	public Long getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}
	public Long getIdSucursalSocioNegocio() {
		return idSucursalSocioNegocio;
	}
	public void setIdSucursalSocioNegocio(Long idSucursalSocioNegocio) {
		this.idSucursalSocioNegocio = idSucursalSocioNegocio;
	}
	public Long getIdCompraTotal() {
		return idCompraTotal;
	}
	public void setIdCompraTotal(Long idCompraTotal) {
		this.idCompraTotal = idCompraTotal;
	}
	public Date getFechaCompra() {
		return fechaCompra;
	}
	public void setFechaCompra(Date fechaCompra) {
		this.fechaCompra = fechaCompra;
	}
	public String getNroAutorizacionPago() {
		return nroAutorizacionPago;
	}
	public void setNroAutorizacionPago(String nroAutorizacionPago) {
		this.nroAutorizacionPago = nroAutorizacionPago;
	}
	public String getIdentifPersonaRelacionada() {
		return identifPersonaRelacionada;
	}
	public void setIdentifPersonaRelacionada(String identifPersonaRelacionada) {
		this.identifPersonaRelacionada = identifPersonaRelacionada;
	}
	public Long getIdIdentifPersonaRelacionada() {
		return idIdentifPersonaRelacionada;
	}
	public void setIdIdentifPersonaRelacionada(Long idIdentifPersonaRelacionada) {
		this.idIdentifPersonaRelacionada = idIdentifPersonaRelacionada;
	}
	public Long getIdTipoIdentifPersonaRelacionada() {
		return idTipoIdentifPersonaRelacionada;
	}
	public void setIdTipoIdentifPersonaRelacionada(
			Long idTipoIdentifPersonaRelacionada) {
		this.idTipoIdentifPersonaRelacionada = idTipoIdentifPersonaRelacionada;
	}
	public String getTipoIdentifPersonaRelacionada() {
		return tipoIdentifPersonaRelacionada;
	}
	public void setTipoIdentifPersonaRelacionada(
			String tipoIdentifPersonaRelacionada) {
		this.tipoIdentifPersonaRelacionada = tipoIdentifPersonaRelacionada;
	}
}
