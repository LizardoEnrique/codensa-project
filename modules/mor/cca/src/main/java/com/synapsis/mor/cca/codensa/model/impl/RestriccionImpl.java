/**
 * $Id: RestriccionImpl.java,v 1.1 2007/07/06 15:19:08 ar28750185 Exp $
 */
package com.synapsis.mor.cca.codensa.model.impl;

import com.synapsis.mor.cca.codensa.model.Restriccion;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.1 $ $Date: 2007/07/06 15:19:08 $
 */
public class RestriccionImpl extends SynergiaBusinessObjectImpl implements
		Restriccion {

	/**
	 * @uml.property name="nroCuenta"
	 */
	private Long nroCuenta;

	/**
	 * @uml.property name="idRestriccion"
	 */
	private Long idRestriccion;

	/**
	 * @uml.property name="tipoRestriccion"
	 */
	private String tipoRestriccion;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.Restriccion#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return this.nroCuenta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.Restriccion#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.Restriccion#getIdRestriccion()
	 */
	public Long getIdRestriccion() {
		return this.idRestriccion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.Restriccion#setIdRestriccion(java.lang.Long)
	 */
	public void setIdRestriccion(Long idRestriccion) {
		this.idRestriccion = idRestriccion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.Restriccion#getTipoRestriccion()
	 */
	public String getTipoRestriccion() {
		return this.tipoRestriccion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.mor.cca.codensa.model.impl.Restriccion#setTipoRestriccion(java.lang.String)
	 */
	public void setTipoRestriccion(String tipoRestriccion) {
		this.tipoRestriccion = tipoRestriccion;
	}

}
