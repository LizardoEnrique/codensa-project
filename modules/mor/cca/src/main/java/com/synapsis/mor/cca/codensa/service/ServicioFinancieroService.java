/**
 * $Id: ServicioFinancieroService.java,v 1.6 2008/04/25 15:52:11 ar29261698 Exp $
 */
package com.synapsis.mor.cca.codensa.service;

import java.util.List;

import com.suivant.arquitectura.core.exception.BusinessException;
import com.synapsis.mor.cca.codensa.model.ServicioCuenta;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.6 $ $Date: 2008/04/25 15:52:11 $
 */
public interface ServicioFinancieroService {

	/**
	 * @throws BusinessException
	 */
	public abstract void transferir(Long numeroCuentaOrigen,
			Long numeroCuentaDestino, ServicioCuenta servicioCuenta)
			throws BusinessException;

	/**
	 * @param numeroCuenta
	 * @return
	 */
	public abstract List consultaRestricciones(Long numeroCuenta);

	/**
	 * @param numeroCuenta
	 * @return
	 */
	public abstract Boolean tieneRestricciones(Long numeroCuenta);

	/**
	 * @param numeroCuenta
	 * @return
	 */
	public abstract List consultaServiciosActivos(Long numeroCuenta);

	/**
	 * @param numeroCuenta
	 * @return
	 */
	public abstract Boolean tieneServiciosActivos(Long numeroCuenta);

	/**
	 * @param idServicio
	 * @return
	 */
	public abstract Boolean seGeneroAjusteEnCuenta(Long idServicio);



}