package com.synapsis.mor.cca.codensa.model.combo;

public interface EstadoCompra {

	public abstract String getEstado();

	public abstract void setEstado(String estado);

}