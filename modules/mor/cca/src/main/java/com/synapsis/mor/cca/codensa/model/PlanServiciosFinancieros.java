/**
 * $Id: PlanServiciosFinancieros.java,v 1.2 2007/06/29 13:21:10 ar26557682 Exp $
 */
package com.synapsis.mor.cca.codensa.model;

import java.util.Date;

import com.suivant.arquitectura.core.model.PersistentObject;

/**
 * 
 * @author Paola Attadio
 * 
 */
public interface PlanServiciosFinancieros extends PersistentObject {

	public char getActivo();

	public void setActivo(char activo);

	public String getCodigo();

	public void setCodigo(String codigo);

	public String getDescripcion();

	public void setDescripcion(String descripcion);

	public Date getFechaFinVigencia();

	public void setFechaFinVigencia(Date fechaFinVigencia);

	public Date getFechaInicioVigencia();

	public void setFechaInicioVigencia(Date fechaInicioVigencia);

	public Long getLineaNegocio();

	public void setLineaNegocio(Long lineaNegocio);

	public Long getTipoFinanciacion();

	public void setTipoFinanciacion(Long tipoFinanciacion);
}
