/**
 * $Id: RepTotalCompra.java,v 1.2 2007/07/04 16:13:28 ar26557682 Exp $
 */
package com.synapsis.mor.cca.codensa.model;

import java.util.Date;

import com.suivant.arquitectura.core.model.BusinessObject;

/**
 * Modela el reporte y actualmente est� mapeado a una Vista que realiza la
 * consulta de compras totales
 * 
 * @author Paola Attadio
 * @version $Revision: 1.2 $
 * 
 */
public interface RepTotalCompra extends BusinessObject {

	public String getEstadoCompra();

	public void setEstadoCompra(String estadoCompra);

	public Date getFechaCompra();

	public void setFechaCompra(Date fechaCompra);

	public Date getFechaCreacion();

	public void setFechaCreacion(Date fechaCreacion);

	public String getIdentifPersonaRelacionada();

	public void setIdentifPersonaRelacionada(String identifPersonaRelacionada);

	public Long getIdTipoIdentifPersonaRelacionada();

	public void setIdTipoIdentifPersonaRelacionada(
			Long idTipoIdentifPersonaRelacionada);

	public String getSocioNegocio();

	public void setSocioNegocio(String socioNegocio);

	public String getSucursalSocioNegocio();

	public void setSucursalSocioNegocio(String sucursalSocioNegocio);

	public String getTipoIdentifPersonaRelacionada();

	public void setTipoIdentifPersonaRelacionada(
			String tipoIdentifPersonaRelacionada);

	public Long getValorCompra();

	public void setValorCompra(Long valorCompra);

	public String getNroAutorizacion();

	public void setNroAutorizacion(String nroAutorizacion);

	public Long getIdSocioNegocio();

	public void setIdSocioNegocio(Long idSocioNegocio);

	public Long getIdSocioSucursal();

	public void setIdSocioSucursal(Long idSocioSucursal);
}
