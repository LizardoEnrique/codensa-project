/**
 * Ambiente TESTCOLOMBIA.
 * Script que implementa el KeepAlive de la sesion de KND cuando el usuario 
 * se encuentra trabajando en AME. El mantenimiento de la sesion se realiza 
 * mientras la sesion en AME no alcance el timeout por inactividad. En caso de
 * llegar al timeout se cierran las ventans hijas de AME junto con la principal.	
 *
 * Configuraciones: la variable llamada time debe coincidir con el valor 
 * setedo en el web.xml referente al timeout de sesion.
 * Ese valor se definio como 29 minutos, un minuto menos que el timeput de Kendary.
 	
 * @author Paola Attadio
 * @version $Revision: 1.3 $
 */

var time = 840000; // 60000 = 1 minuto, 840000 = 14 minutos (un minuto menos que el timeout de Kendary)
var URLKND = URLAPP + "/" + KNDcontextRoot + "/testpage.jsp;jsessionid="+SessionID;
var URLNAS = URLAPP + AMEcontextRoot + "/index.jsf;jsessionid=" +SessionID ;

IdKnd =  window.setTimeout("KNDKeepAlive();",time );
IdAME =  window.setTimeout("AMECloseWindow();",time );

if (window.captureEvents){
   window.captureEvents(Event.CLICK);
   window.onclick=reStartTimeout;
} else document.onclick=reStartTimeout;
				
function reStartTimeout() {
   window.clearTimeout(IdAME);
   IdAME = window.setTimeout("AMECloseWindow();",time );
   return true;
}

function KNDKeepAlive() {
    if (window.XMLHttpRequest) {
      kndRequest = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
      kndRequest = new ActiveXObject("Microsoft.XMLHTTP");
    }
    kndRequest.open("GET", URLKND, false);
    kndRequest.send(null);
    //alert("Ya se proceso la ida y vuelta, statusText: " + kndRequest.statusText );
    //seteo otro para los proximos time minutos
    IdKnd =  window.setTimeout("KNDKeepAlive();", time);
}
 
function AMECloseWindow() {
    // si estamos aca significa que la sesion expiro, con lo cual tengo que hacer un par de cositas
    //primero, cierro todas las ventas hijo     
    wManager.closeFromReference(this);
    //si soy el tope entonces me cierro
    //if (instanceWindowManager == true)  {
    window.close();
    //}   
}