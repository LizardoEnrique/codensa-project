/**
 * Script que implementa el KeepAlive de la sesion de KND cuando el usuario 
 * se encuentra en AME. El mantenimiento de la sesion se realiza mientras   
 * la sesion en AME no alcance el timeout por inactividad	
 *	
 * Este script es para el ambiente de DESARROLLO, en donde no debemos tener
 * ninguna politica de KeepAlive y por lo tanto no contiene codigo.	
 *
 *	
 * @author Paola Attadio
 * @version $Revision: 1.2 $
 */
//nada es requerido por estar en desarrollo