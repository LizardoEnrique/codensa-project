<!--

    /**
     * a partir de un id busca el form (en realidad cualquier nodo que tenga un 
     * metodo submit) que lo contiene.
     *  
     * @param {Object} id
     */
    function find_form(id) {
    	var node = document.getElementById(id);
    	if (node != null)
    		while (node != null && !(node.submit))
    			node = node.parentNode;
    
    	return node;
    }
    
    makeUnikeName = function() {
		var foo = new Date();
		return foo.getTime();
	}
    
    /**
     * abre un popup invocando el action del componente de jsf con id especificado 
     * por el argumento item_id. attributes permite sobreescribir las opciones 
     * enviadas a la ventana de popup.
     *  
     * @param {Object} target_form
     * @param {Object} item_id
     * @param {Object} attributes
     */
    function openPopup(target_form, item_id, attributes) {
        if (target_form == null)
        	target_form = find_form(item_id);
    
        if (target_form == null) {
        	alert('Error interno. No se envio el form.');
        	return;
        }
        if (item_id == null) {
        	alert('Error interno. No se envio el item_id.');
        	return;
        }
    	var auxTargetForm = target_form;
    	var auxItemId = item_id;
    	features = attributes + 'width=950,height=600,status=yes,toolbar=no,menubar=no,location=no,resizable=yes,scrollbars=yes';
    
    	var hidden_id = ':hiddenDispatch';
    	var full_form_name = target_form.id + hidden_id;
    	var hidden_name = item_id.replace(/:[^:]+$/, '') + hidden_id;
    	//debe comenzar con una letra y no acepta cualquier caracter, solo un conjunto reducido
    	//ademas si se da el caso de que exista en un popup otro popup con el mismo id quedaria en
    	//la misma ventana.
    	var popup_name = 'a' + makeUnikeName();
    
    	window.open('', popup_name, features);

        target_form.target = popup_name;
    	target_form[target_form.id + ':_link_hidden_'].value = hidden_name;
    	target_form.submit();
    	
    	// Ok, let's fix it.
    	target_form = auxTargetForm;
    	target_form[target_form.id + ':_link_hidden_'].value = "";
    	target_form.target = "_self";
    	item_id = auxItemId;
    }
    
    /**
     * Fires the onClick event.
     * 
     * @param {Object} element
     */
    function fireClickEvent(element) {
        if (document.createEvent) {
            var customClick = document.createEvent('MouseEvents');
            customClick.initEvent('click',0,0);
            element.dispatchEvent(customClick);
        }
        else if (document.createEventObject) { // IE model
            var customClick = document.createEventObject();
            element.click(); // equals to a hardware click
        }
        else {
            /* NOP */
        }
    }
    
    /**
     * Change the opener value.
     * 
     * @param {Object} valueToSet
     */
    function doOpenerSubmit(valueToSet) {
    	var frmParent = window.opener.getObjFrmSearch();
    	var codToChange;
    	for(i=0; i < frmParent.elements.length; i++) {
    		if(frmParent.elements[i].name == "search:codigo") {
    			codToChange = frmParent.elements[i];
    		}
    	}
    	codToChange.value = valueToSet;
        fireClickEvent(window.opener.document.getElementById("search:cmdButton"));
    	window.close();
    	return false;
    }
    
    /**
     * Change the opener value.
     * 
     * @param {Object} valueToSet
     */
    function doOpenerSubmitTab(valueToSet) {
       var el = document.getElementById(valueToSet);   
       fireClickEvent(el.firstChild);
		// finally performs the submit action ...
       el.firstChild.submit();
    }
    
    
	// Retorna un elemento de form buscando por todos los formularios del doc
	function getFormToOperate(sComponentName) {
		var forms 	= document.getElementsByTagName("form");
		var f		= forms.length;
		while(f--) {
			for (var i = 0; i < forms[f].elements.length; i++) {
				if(forms[f].elements[i].name.indexOf(sComponentName) >= 0) {
					return forms[f];							
				}
			}
		}
	}
	// TODO: sacar el selectOneCheckBox y mandarlo a otro js	
	function selectOneCheckBox(obj, bOperate) {
		if(!bOperate) {
    		var formToUse = getFormToOperate(obj.name);
    		for (var i = 0; i < formToUse.elements.length; i++) {
    			if(formToUse.elements[i].type == "checkbox") {
    				if(formToUse.elements[i].value != obj.value)
    					formToUse.elements[i].checked=false;					
    			}
    		}
        }
        return;
	}
	
	// Funciones para la apertura de popup para los CU de KND
	// funciones para popup externos
	function windowOpen(sWhere, sWith, sHeight) {				
			window.open(sWhere,"","location=0,status=1,resizable=yes,scrollbars=1,width="+sWith+",height="+sHeight);
	}
	// Retorna un elemento de form buscando por todos los formularios del doc
	function getInput(sComponentName) {
		var forms 	= document.getElementsByTagName("form");
		var f		= forms.length;
		while(f--) {
			for (var i = 0; i < forms[f].elements.length; i++) {
				if(forms[f].elements[i].name.indexOf(sComponentName) >= 0) {
					return forms[f].elements[i];							
				}
			}
		}
	}
	
	// object to call kns from anywhere
	var KndExecuter = new Object();
	KndExecuter.makeCall = function(sWhere, sWith, sHeight) {
		windowOpen(sWhere, sWith, sHeight);
	}
	
//-->