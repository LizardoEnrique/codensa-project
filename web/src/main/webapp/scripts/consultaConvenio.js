function openPopup(target_form, item_id, attributes)
{
	var found = false;

	var btnTxt = document.getElementById(item_id).value;
	// TODO: comparo con el texto del boton, si cambia en el .properties esto se rompe.
	if ( (btnTxt == 'Certifica Convenio') || (btnTxt == 'Detalle Deuda Actual Exigible')) {
		var elems = target_form.elements;
		for (var i = 0; i < elems.length && !found; i++)
			if (elems[i].type == 'radio' && elems[i].checked)
				found = true;
	} else
		found = true;

	if (!found)
		alert('Seleccione un item');
	else
		old_open_popup(target_form, item_id, attributes);
}


function reimprimir(){
	if (getInput('valueToPost2').value != '')
		windowOpen(getInput('valueToPost2').value, 800, 600)
}