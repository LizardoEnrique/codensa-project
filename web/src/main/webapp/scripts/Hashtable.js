/**
 * @author waab0x
 * @version $Revision: 1.1 $ $Date: 2007/09/14 18:22:50 $
 * 
 */
function Hashtable() {
	Hashtable.prototype.Add=fnAdd;
	Hashtable.prototype.Remove=fnRemove;
	Hashtable.prototype.ContainsKey=fnContainsKey;
	Hashtable.prototype.Item=fnItem;
	Hashtable.prototype.MapOnKey=fnMapOnKey;
	Hashtable.prototype.MapOnItem=fnMapOnItem;
	Hashtable.prototype.Length=fnLength;
	Hashtable.prototype.Keys=fnKeys;
	Hashtable.prototype.Items=fnItems;
	
	this.hsh={};
	this.length=0;
	
	function fnMapOnKey(oFn) {
		for(var sKey in this.hsh) {
			if (this.ContainsKey(sKey)) oFn(sKey);
		}
	}
	function fnMapOnItem(oFn) {
		for(var sItem in this.hsh) {
			if (this.ContainsKey(sItem)) oFn(this.hsh[sItem]);
		}
	}
	function fnAdd(sKey,sValue) {
		if (!this.ContainsKey(sKey)){
			this.hsh[sKey]=sValue;
			this.length++;
		} else {
			alert('key exist ^^');
		}
	}
	function fnContainsKey(sKey) {
		return(this.hsh[sKey]?true:false);		
	}
	function fnItem(sKey) {
		return(this.ContainsKey(sKey)?this.hsh[sKey]:null);
	}	
	function fnRemove(sKey) {
		if(this.ContainsKey(sKey)) {
			this.hsh[sKey]=null;
			this.length--;
		}
	}	
	function fnLength() {
		return(this.length);
	}
	function fnKeys(){
	    var keys = new Array();
	    for (var sKey in this.hsh) {
	        if (this.ContainsKey(sKey)) 
	            keys.push(sKey);
	    }
	    return keys;
	}
	function fnItems(){
	    var items = new Array();
	    for (var sKey in this.hsh) {
	        if (this.ContainsKey(sKey)) 
	            items.push(this.hsh[sKey]);
	    }
	    return items;
	}
}