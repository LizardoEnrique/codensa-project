function openPopup(target_form, item_id, attributes)
{
	var found = false;

	var btnTxt = document.getElementById(item_id).value;
	// TODO: comparo con el texto del boton, si cambia en el .properties esto se rompe.
	if (btnTxt == 'Certifica Pagos') {
		var elems = target_form.elements;
		for (var i = 0; i < elems.length && !found; i++)
			if (elems[i].type == 'checkbox' && elems[i].checked)
				found = true;
	} else
		found = true;

	if (!found)
		alert('Seleccione un item');
	else
		old_open_popup(target_form, item_id, attributes);
}
