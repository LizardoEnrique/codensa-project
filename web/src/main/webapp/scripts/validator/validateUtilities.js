/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
  /*$RCSfile: validateUtilities.js,v $ $Rev: 478676 $ $Date: 2007/02/01 20:19:38 $ */
  /**
  * This is a place holder for common utilities used across the javascript validation
  *
  **/

  /**
   * Retreive the name of the form
   * @param form The form validation is taking place on.
   */
  function jcv_retrieveFormName(form) {

      // Please refer to Bugs 31534, 35127, 35294, 37315 & 38159
      // for the history of the following code

      var formName;

      if (form.getAttributeNode) {
          if (form.getAttributeNode("id") && form.getAttributeNode("id").value) {
              formName = form.getAttributeNode("id").value;
          } else {
              formName = form.getAttributeNode("name").value;
          }
      } else if (form.getAttribute) {
          if (form.getAttribute("id")) {
              formName = form.getAttribute("id");
          } else {
              formName = form.attributes["name"];
          }
      } else {
          if (form.id) {
              formName = form.id;
          } else {
              formName = form.name;
          }
      }

      return formName;

  }  

  /**
   * Handle error messages.
   * @param messages Array of error messages.
   * @param focusField Field to set focus on.
   * @param fields The ids of fields that failed validation.
   *
   * Hace falta definir 4 variables para las modificaciones hechas a jcv_handleErrors:
   *   use_popup: booleano, indica si usa alert o un div para mostrar los errores
   *   errorBoxId: string, id del elemento donde se escribiran los errores
   *   highlight: booleano, si cambia el color de fondo de los campos invalidos.
   *   errorBkgColor: string, color a aplicar a los campos invalidos si highlight es true.
   *   validBkgColor: string, color a aplicar a los campos validos si highlight es true.
   *
   */
  var use_popup = false;
  var errorBoxId = 'validationMessages';
  var highlight = true;
  var errorBkgColor = '#ff0000';
  var validBkgColor = '';
   
  function jcv_handleErrors(messages, focusField, fields, formName) {
      if (focusField && focusField != null) {
          var doFocus = true;
          if (focusField.disabled || focusField.type == 'hidden') {
              doFocus = false;
          }
          if (doFocus && 
              focusField.style && 
              focusField.style.visibility &&
              focusField.style.visibility == 'hidden') {
              doFocus = false;
          }
          if (doFocus) {
              focusField.focus();
          }
      }

	  if (use_popup)
      	alert(messages.join('\n'));
      else {
		  var errDiv = document.getElementById(errorBoxId);
		  if (errDiv)
			  errDiv.innerHTML = messages.join('<br/>');
	  }

		if (highlight)
			syn_highlight(fields, formName);
  }

	/**
	 * Setea un estilo a los campos enviados como argumentos.
	 */
	function syn_highlight(fields, formName) {
		// pasamos los fields invalidos a un hash para el chequeo posterior
		var invalid_fields = new Array();
		if (fields)
			for (var i in fields) invalid_fields[fields[i]] = 1;

		// Los nombres de todas las validaciones posibles. Si se creara otra, hay que
		// agregarla aca.
		var validationType = ['creditCard', 'ByteValidations', 'DateValidations',
			'email', 'FloatValidations', 'floatRange', 'IntegerValidations', 'intRange',
			'mask', 'maxlength', 'minlength', 'ShortValidations', 'required',
			'DateCompareValidations'];

		var form = document.forms[formName];

		for (var i = 0; i < validationType.length; i++) {
			var func = formName +  '_' + validationType[i];
			if (eval('typeof ' + func) == 'undefined') continue;
    	    var v = eval('new ' + func + '()');
	        for (var k in v) {
	        	var field_name = v[k][0];
	        	var elem = form[field_name];
	        	if (invalid_fields[field_name])
					elem.style.background = errorBkgColor;
	        	else
					elem.style.background = validBkgColor;
	        }
	    }
	}

  /**
   * Checks that the array element is a valid
   * Commons Validator element and not one inserted by
   * other JavaScript libraries (for example the
   * prototype library inserts an "extends" into
   * all objects, including Arrays).
   * @param name The element name.
   * @param value The element value.
   */
  function jcv_verifyArrayElement(name, element) {
      if (element && element.length && element.length == 3) {
          return true;
      } else {
          return false;
      }
  }

  /**
   * Checks whether the field is present on the form.
   * @param field The form field.
   */
  function jcv_isFieldPresent(field) {
      var fieldPresent = true;
      if (field == null || (typeof field == 'undefined')) {
          fieldPresent = false;
      } else {
          if (field.disabled) {
              fieldPresent = false;
          }
      }
      return fieldPresent;
  }

  /**
   * Check a value only contains valid numeric digits
   * @param argvalue The value to check.
   */
  function jcv_isAllDigits(argvalue) {
      argvalue = argvalue.toString();
      var validChars = "0123456789";
      var startFrom = 0;
      if (argvalue.substring(0, 2) == "0x") {
         validChars = "0123456789abcdefABCDEF";
         startFrom = 2;
      } else if (argvalue.charAt(0) == "0") {
         validChars = "01234567";
         startFrom = 1;
      } else if (argvalue.charAt(0) == "-") {
          startFrom = 1;
      }

      for (var n = startFrom; n < argvalue.length; n++) {
          if (validChars.indexOf(argvalue.substring(n, n+1)) == -1) return false;
      }
      return true;
  }

  /**
   * Check a value only contains valid decimal digits
   * @param argvalue The value to check.
   */
  function jcv_isDecimalDigits(argvalue) {
      argvalue = argvalue.toString();
      var validChars = "0123456789";

      var startFrom = 0;
      if (argvalue.charAt(0) == "-") {
          startFrom = 1;
      }

      for (var n = startFrom; n < argvalue.length; n++) {
          if (validChars.indexOf(argvalue.substring(n, n+1)) == -1) return false;
      }
      return true;
  }
