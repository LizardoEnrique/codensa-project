var Exception = Class.create();
 
Exception.prototype = {
 
	initialize: function(message, exceptionSumary) {
		this.message = message;
		this.sumary	 = $(exceptionSumary);
	},

	showSumary: function() {
		this.getSummary().innerHTML = this.getMessage();
	},
		
	// getters
	getMessage: function() {
		return this.message;
	},
	
	getSummary: function() {
		return this.sumary;
	}
};