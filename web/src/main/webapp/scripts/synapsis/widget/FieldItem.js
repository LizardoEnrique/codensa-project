/*
	Copyright (c) 2004-2006, The Dojo Foundation
	All Rights Reserved.

	Licensed under the Academic Free License version 2.1 or above OR the
	modified BSD license. For more information on Dojo licensing, see:

		http://dojotoolkit.org/community/licensing.shtml
*/

dojo.provide("synapsis.widget.FieldItem");

dojo.require("dojo.widget.*");

/**
 * FieldItem definition.
 * 
 *  @author outaTiME (afalduto at gmail dot com) 
 *  @version $Revision: 1.2 $ $Date: 2008/01/10 16:16:33 $
 */
dojo.widget.defineWidget(
	"synapsis.widget.FieldItem",
	dojo.widget.HtmlWidget, {        
    
    /**
     * Comment for <code>bind</code>
     */
    bind: "",    
    
    /**
     * Comment for <code>name</code>
     */
    name: "",    
    
    /**
     * Comment for <code>value</code>
     */
    value: "",    
    
	/**
	 * postCreate
	 */
    postCreate: function() {
        // with bind specified ???
        if (!dojo.string.isBlank(this.bind)) {
            var element = dojo.byId(this.bind);
            if(!dojo.dom.isNode(element)){
                dojo.raise("Unabled to bind with the component: " + this.bind);
            }
            // ok ... object find ... will be perform the binding ...
            this.bind = element;
        }
    },
    
    /**
     * getName
     */
    getName: function() {
        if (dojo.string.isBlank(this.name)){
            // break the execution and raise an error ...
            this.assertBindExist();        	
            // use the bind id ...
            return this.bind.id;
        }
        return this.name;                
    },
    
    /**
     * getValue
     */
    getValue: function() {
        if (dojo.string.isBlank(this.value)){
            // break the execution and raise an error ...
            this.assertBindExist();
            // bind type switch ...
            switch(this.bind.type) {
        		case "textarea":
                case "password":                
                case "checkbox":
                case "radio":
        		case "select-one":
                case "hidden":
        		case "text":
                    return this.bind.value;  // string
                // other elements will be here (if value not exist) !!! 
        		default:
        			return dojo.dom.textContent(this.bind); // textContent                       
            }
        }
        return this.value;                
    },    
    
    /**
     * assertBindExist
     */
    assertBindExist: function() {
        if (!dojo.dom.isNode(this.bind)) {
            dojo.raise("The name or bind attribute will required");
        }
    },
    
    /**
     * getField
     */
    getField: function() {
        return {name: this.getName(), value: this.getValue()};
    }

});