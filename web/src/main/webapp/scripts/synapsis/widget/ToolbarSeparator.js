/*
	Copyright (c) 2004-2006, The Dojo Foundation
	All Rights Reserved.

	Licensed under the Academic Free License version 2.1 or above OR the
	modified BSD license. For more information on Dojo licensing, see:

		http://dojotoolkit.org/community/licensing.shtml
*/

dojo.provide("synapsis.widget.ToolbarSeparator");

dojo.require("dojo.io.*");
dojo.require("dojo.widget.*");
dojo.require("dojo.widget.Toolbar");
dojo.require("dojo.event.*");

dojo.widget.defineWidget(
	"synapsis.widget.ToolbarSeparator",
	dojo.widget.ToolbarSeparator, {
    
    /**
     * Comment for <code>widgetType</code>
     */
    widgetType: "ToolbarSeparator"
	
});