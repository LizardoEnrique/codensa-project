/*
Copyright (c) 2004-2006, The Dojo Foundation
All Rights Reserved.

Licensed under the Academic Free License version 2.1 or above OR the
modified BSD license. For more information on Dojo licensing, see:

http://dojotoolkit.org/community/licensing.shtml
*/

dojo.provide("synapsis.widget.StaticToaster");

dojo.require("dojo.widget.*");
dojo.require("dojo.widget.Toaster");

/**
* StaticToaster definition.
*
* @author waabox (waabox at gmail dot com)
* @version $Revision: 1.2 $ $Date: 2008/01/10 16:16:33 $
*/
dojo.widget.defineWidget(
	"synapsis.widget.StaticToaster",
	dojo.widget.Toaster, {
    
		hide: function() {}         
		
});