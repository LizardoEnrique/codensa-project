/*
	Copyright (c) 2004-2006, The Dojo Foundation
	All Rights Reserved.

	Licensed under the Academic Free License version 2.1 or above OR the
	modified BSD license. For more information on Dojo licensing, see:

		http://dojotoolkit.org/community/licensing.shtml
*/

dojo.provide("synapsis.widget.ToolbarSelector");

dojo.require("dojo.widget.*");

/**
 * ToolbarSelector definition.
 */
dojo.widget.defineWidget(
	"synapsis.widget.ToolbarSelector",
	dojo.widget.HtmlWidget, {

    /**
     * Comment for <code>widgetType</code>
     */
    widgetType: "ToolbarSelector",

    /**
     * Event fired when <code>selection</code> event on toggleable item 
     * happended.
     *
     * @param item <code>ToolbarItem</code> reference.
     */
	onSelect: function(item) {},
    
    /**
     * Event fired when <code>deselection</code> event on toggleable item 
     * happended.
     *
     * @param item <code>ToolbarItem</code> reference.
     */
	onDeselect: function(item) {},    

    /**
     * Event fired when <code>click</code> event on button item happended.
     *
     * @param item <code>ToolbarItem</code> reference.
     */
	onClick: function(item) {}
    
});