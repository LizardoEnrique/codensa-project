/*
	Copyright (c) 2004-2006, The Dojo Foundation
	All Rights Reserved.

	Licensed under the Academic Free License version 2.1 or above OR the
	modified BSD license. For more information on Dojo licensing, see:

		http://dojotoolkit.org/community/licensing.shtml
*/

dojo.provide("synapsis.widget.Toolbar");

dojo.require("dojo.io.*");
dojo.require("dojo.widget.*");
dojo.require("dojo.widget.Toolbar");
dojo.require("dojo.event.*");

/**
 * Toolbar definition.
 */
dojo.widget.defineWidget(
	"synapsis.widget.Toolbar",
	dojo.widget.Toolbar,
    
{
    
    /**
     * Comment for <code>widgetType</code>
     */
    widgetType: "Toolbar",

    /**
     * Comment for <code>templateCssPath</code>
     */
    templateCssPath: dojo.uri.dojoUri("../synapsis/widget/templates/Toolbar.css"),

    /**
     * Comment for <code>controller</code>
     */
	controller: "",
    
    /**
     * Comment for <code>selector</code>
     */
    selector: "",
    
    /**
     * Comment for <code>selector</code>
     */
    disabled: false,

    /**
     * Over-rides the widget initialization method. 
     *
     * @param args initialization arguments.
     * @param frag unknown. 
     */   
    initialize: function(args, frag){
        // initialize the selector ...
        this._initializeSelector();        
        // initialize the mock controller ...
        this._initializeController();        
    },

    /**
     * Initialize the selector widget reference.
     */   
    _initializeSelector: function(){
    	if (this.selector) {
    		this.selector = dojo.widget.byId(this.selector);
        }
    },

    /**
     * Initialize the controller widget reference.
     */   
    _initializeController: function(){
    	if (this.controller) {
    		this.controller = dojo.widget.byId(this.controller);
            // perform the invoke operation after event handler linked ... 
            dojo.addOnLoad(this, "_onLoad");
            dojo.event.connect(this, "_onLoad", this, "onLoad");
    	}        
    },

    /**
     * This method will be executed when page be loaded.
     */ 
	_onLoad: function() {
        this.controller.invoke(dojo.lang.hitch(this, this._initWithoutCallback));
	},
    
    /**
     * Mock event for toolbar initialization.
     */
    onLoad: function() {
    },    

    /**
     * Builds empty toolbar that will be completed with its 
     * respective items via RPC call.
     *
     * This invocation prevents the <code>dojo.io.bind</code> calls.
     *
     * @param type Type of response.
     * @param source Response from invocation. 
     * @param e Event related data. 
     */  
    _initWithoutCallback: function(type, source, e){
        this._buildToolbar(source);        
    }, 

    /**
     * Build toolbar from RPC response.
     *
     * @param data RPC data returned by dojo bind callback.
     */   
    _buildToolbar: function(data){
        for(var i = 0; i < data.length; i++) {
            if (data[i].type == "ToolbarButtonGroup") {
                var group = dojo.widget.createWidget(data[i].type, data[i]);
                this.addChild(group);
                for(var j = 0; j <data[i].childItems.length; j++) {
                    this._addButton(data[i].childItems[j], group);
                }
            } else {
                this._addButton(data[i]);
            }
        } 
        this.disableChilds();
    	dojo.lang.forEach(this.getItems(), function(widget) {
    		widget.disable();
       	});
    },  

    /**
     * Adds button to the <code>source</code> specified.
     *
     * @param data button reference descriptor.
     * @param source where the button was be placed.
     */  
    _addButton: function(data, source){
        if (!source) {
            source = this;
        } 
        this._registerEvents(source.addChild(
            dojo.widget.createWidget(data.type, data)));        
    },

    /**
     * Bind events for current the <code>ToolbarItem</code> specified.
     *
     * @param btn <code>ToolbarItem</code> reference.
     */  
    _registerEvents: function(item){
        if(item.type == "synapsis:ToolbarButton") {
            if(item.toggleItem) {
                dojo.event.connect(item, "onSelect", this.selector, "onSelect");
                dojo.event.connect(item, "onDeselect", this.selector, "onDeselect");
                if(item.selected) {
                    item.select();
                }                  
            } else {
                dojo.event.connect(item, "onClick", this.selector, "onClick");
            }
            // has the tooltip identifier ??? 
            /*
			if (item.description) {
                this._setupTooltip(item);
            } 
            */ 
        }       
    },
    
    /**
     * Configure the tooltip for current the <code>ToolbarItem</code> specified.
     *
     * @param btn <code>ToolbarItem</code> reference.
     */  
    _setupTooltip: function(item){
        var addToElmt = document.body;
        var rowNode = item.domNode;
        var ttWidget = dojo.widget.createWidget('Tooltip', { 
            connectId: rowNode.id 
        });
        ttWidget.hide();
        ttWidget.setContent(item.description);
        addToElmt.appendChild(ttWidget.domNode);
    },
    
    /**
     * Fires the disabled action. 
     */
    disable: function() {
    	this.disabled = true;
    }, 
    
    /**
     * Fires the enabled action. 
     */
    enable: function() {
    	dojo.lang.forEach(this.getItems(), function(widget) {
    		widget.enable();
       	});
        dojo.debug("widget id: " + this.widgetId + " enabled.");
    },
    
    disableChilds: function() {
    	dojo.lang.forEach(this.getItems(), function(widget) {
    		widget.disable();
       	});
    }     
        
});