/*
	Copyright (c) 2004-2006, The Dojo Foundation
	All Rights Reserved.

	Licensed under the Academic Free License version 2.1 or above OR the
	modified BSD license. For more information on Dojo licensing, see:

		http://dojotoolkit.org/community/licensing.shtml
*/

dojo.kwCompoundRequire({
	common: ["synapsis.widget.PortletContainer",
			 "synapsis.widget.PortletContainerSinTitle",	
    		 "synapsis.widget.PortletItem",
    		 "synapsis.widget.PortletItemSinTitle",
			 "synapsis.widget.Toolbar",
	         "synapsis.widget.ToolbarButton",
	         "synapsis.widget.ToolbarSeparator",
	         "synapsis.widget.ToolbarSelector",
	         "synapsis.widget.ToolbarLoadingController",
             "synapsis.widget.Menubar",
             "synapsis.widget.FieldCollector",
             "synapsis.widget.FieldItem",
             "synapsis.widget.StaticToaster"]
});
dojo.provide("synapsis.widget.*");
