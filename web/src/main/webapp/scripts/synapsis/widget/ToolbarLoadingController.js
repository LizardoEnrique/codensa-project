/*
	Copyright (c) 2004-2006, The Dojo Foundation
	All Rights Reserved.

	Licensed under the Academic Free License version 2.1 or above OR the
	modified BSD license. For more information on Dojo licensing, see:

		http://dojotoolkit.org/community/licensing.shtml
*/

dojo.provide("synapsis.widget.ToolbarLoadingController");

dojo.require("dojo.io.*");
dojo.require("dojo.widget.*");

/**
 * LoadingController definition.
 */
dojo.widget.defineWidget(
	"synapsis.widget.ToolbarLoadingController",
	dojo.widget.HtmlWidget, {

    /**
     * Comment for <code>widgetType</code>
     */
    widgetType: "ToolbarLoadingController",

    /**
     * Comment for <code>RPCUrl</code>
     */
    RPCUrl: "",
      
	/**
	 * Common RPC error handler (dies).
	 * 
     * @param type Type of response.
     * @param source Response from invocation. 
     * @param e Event related data.
	 */
	errorHandler: function(type, source, evt) {
		alert("RPC Error: " + (source.message||"no message"));
	},    
    
    /**
     * RCP invoker will be delegate the correct invokation to 
     * <code>handler</code>.
     * 
     * @param handler function will be executed.
     */    
    invoke: function(handler) {
        if (this.RPCUrl){
            dojo.io.bind({ 
                url: this.RPCUrl, 
                mimetype: "text/javascript", 
                load: handler,
                error: this.errorHandler
            });
        }        
    }   
});