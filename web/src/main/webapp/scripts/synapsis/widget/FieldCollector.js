/*
	Copyright (c) 2004-2006, The Dojo Foundation
	All Rights Reserved.

	Licensed under the Academic Free License version 2.1 or above OR the
	modified BSD license. For more information on Dojo licensing, see:

		http://dojotoolkit.org/community/licensing.shtml
*/

dojo.provide("synapsis.widget.FieldCollector");

dojo.require("dojo.widget.*");

/**
 * FieldCollector definition.
 * 
 *  @author outaTiME (afalduto at gmail dot com) 
 *  @version $Revision: 1.2 $ $Date: 2008/01/10 16:16:33 $
 */
dojo.widget.defineWidget(
	"synapsis.widget.FieldCollector",
	dojo.widget.HtmlWidget, {
        
    /**
     * Comment for <code>isContainer</code>
     */
    isContainer: true,
    
    /**
     * Comment for <code>fieldItems</code>
     */
    fieldItems: [],

    /**
     * Over-rides the widget initialization method. 
     *
     * @param args initialization arguments.
     * @param frag unknown. 
     */   
    initialize: function(args, frag){
        this.fieldItems = [];        
    },

	/**
	 * postCreate
	 */
    postCreate: function() {
    	dojo.lang.forEach(this.children, function(widget){
            if(widget instanceof synapsis.widget.FieldItem){
                this.fieldItems.push(widget);
            }
    	}, this);
    },
    
    /**
     * Collect the value of each field defined.
     */
    collect: function() {
        var values = [];        
    	dojo.lang.forEach(this.fieldItems, function(widget){
            // push a iface with name and value ...
            values.push(widget.getField());
    	}, this);
        return values;        
    }

});