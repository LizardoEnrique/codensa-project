/*
	Copyright (c) 2004-2006, The Dojo Foundation
	All Rights Reserved.

	Licensed under the Academic Free License version 2.1 or above OR the
	modified BSD license. For more information on Dojo licensing, see:

		http://dojotoolkit.org/community/licensing.shtml
*/

dojo.provide("synapsis.widget.PortletItem");

dojo.require("dojo.html.*");
dojo.require("dojo.widget.*");
dojo.require("dojo.widget.ContentPane");
dojo.require("dojo.event.*");
dojo.require("synapsis.widget.FieldCollector");
dojo.require("synapsis.uri.Uri");

synapsis.widget.PortletItem = function(){
	dojo.widget.ContentPane.call(this);
}

/**
 * Comment for <code>MAX_ITEM_WIDTH</code>
 */
synapsis.widget.PortletItem.MAX_ITEM_WIDTH = 100;

/**
 * Comment for <code>ITEM_AUTO_WIDTH</code>
 */    
synapsis.widget.PortletItem.ITEM_AUTO_WIDTH = "auto";

dojo.inherits(synapsis.widget.PortletItem, dojo.widget.ContentPane);

/**
 * PortletItem definition.
 * 
 *  @author outaTiME (afalduto at gmail dot com)
 *  @version $Revision: 1.7 $ $Date: 2010/02/01 12:35:14 $  
 */
dojo.lang.extend(synapsis.widget.PortletItem, {

    /**
     * Comment for <code>widgetType</code>
     */
    widgetType: "PortletItem",
	
    /**
     * Comment for <code>width</code>
     */
    width: synapsis.widget.PortletItem.ITEM_AUTO_WIDTH,
    
    /**
     * Comment for <code>title</code>
     */
    title: "",
    
    /**
     * Comment for <code>cacheContent</code>
     */
    cacheContent: false,       
       
    /**
     * Comment for <code>refreshable</code>
     */
    refreshable: true,   

    /**
     * Comment for <code>contentUri</code>
     */
    contentUri: "",
    
    /**
     * Comment for <code>contentParameterFieldCollector</code>
     */
    contentParameterFieldCollector: "",
    
    /**
     * Comment for <code>detailUri</code>
     */
    detailUri: "",
    
    /**
     * Comment for <code>detailParameterFieldCollector</code>
     */
    detailParameterFieldCollector: "",
    
    /**
     * Comment for <code>highlight</code>
     */
    highlight: false,
    
    /**
     * Comment for <code>_emptyHeaderTitle</code>
     */
    _emptyHeaderTitle: "",    
    
	// Boolean: Extract visible content from inside of <body> .... </body>
	extractContent: false,

    /**
     * Comment for <code>templatePath</code>
     */
    templatePath: dojo.uri.dojoUri("../synapsis/widget/templates/PortletItem.html"),

    /**
     * Comment for <code>templateCssPath</code>
     */
    templateCssPath: dojo.uri.dojoUri("../synapsis/widget/templates/PortletItem.css"),

	/**
	 * postCreate
	 */
    postCreate: function() {
        // init the contents ...
        this._initializeContents();
        // init the details ...
        this._initializeDetails();
        // attach the highlight styles ...
        this._manageHighlight();
        // store the original headerTitle ...
        this._emptyHeaderTitle = this.headerTitle.firstChild;
        // adjust the widget width ...
        if(this.width != synapsis.widget.PortletItem.ITEM_AUTO_WIDTH) {
            // value has been set ...
            this.width = this.parseWidth(this.width);
        }
        // make the href loading ...
        synapsis.widget.PortletItem.superclass.postCreate.call(this);
    },
    
    /**
     * _initializeContents
     */
    _initializeContents: function() {
        // begin with the contentUri spec.
        if(dojo.string.isBlank(this.contentUri)){
            dojo.raise("PortletItem need the contentUri attribute to performs the operation.");
        }
        // relate with the fieldCollector
    	if (!dojo.string.isBlank(this.contentParameterFieldCollector)) {
            var wg = dojo.widget.byId(this.contentParameterFieldCollector)
            if (!wg) {
                dojo.raise("Can't locate the element for contentParameterFieldCollector attribute.");                    
            }
            this.contentParameterFieldCollector = wg;
        } else {
            // create an empty parameter list ...
            this.contentParameterFieldCollector = dojo.widget.createWidget("synapsis:FieldCollector",{})
        }    
        // fills the href attribute ...
        this.href = new synapsis.uri.Uri(
            this.contentUri, 
            this.contentParameterFieldCollector.collect()).toString();
    },
    
    /**
     * _initializeDetails
     */
    _initializeDetails: function() {
        if (this.isDetailPresent()) {
            // relate with the fieldCollector
        	if (!dojo.string.isBlank(this.detailParameterFieldCollector)) {
                var wg = dojo.widget.byId(this.detailParameterFieldCollector);
                if (!wg) {
                    dojo.raise("Can't locate the element for detailParameterFieldCollector attribute.");                    
                }
        		this.detailParameterFieldCollector = wg;
            } else {
                // create an empty parameter list ...
                this.detailParameterFieldCollector = dojo.widget.createWidget("synapsis:FieldCollector",{})
            }                
        }
    },    

    /**
     * _manageHighlight
     */
    _manageHighlight: function() {
        if(this.highlight) {
            dojo.html.addClass(this.moduleFrame, "moduleFrameHighlight");
            dojo.html.addClass(this.moduleHeader, "moduleHeaderHighlight");
            
        }
    },    

    /**
     * isDetailPresent
     */
    isDetailPresent: function() {
        return !dojo.string.isBlank(this.detailUri);
    },

  	/**
  	 * onDownloadStart
  	 * 
  	 * @param {Object} e
  	 */
    onDownloadStart: function(e){
        // show wait screen ...
        this._displayWaitScreen(e);
        e.returnValue = false;
        // detail present ???
        if (this.isDetailPresent()) {         
            // removes detail data ...
            dojo.dom.replaceChildren(this.headerTitle, this._emptyHeaderTitle);
        }        
    },

  	/**
  	 * onDownloadEnd
  	 * 
  	 * @param {Object} url
  	 * @param {Object} data
  	 */
    onDownloadEnd: function(url, data){
        synapsis.widget.PortletItem.superclass.onDownloadEnd.call(this, url, data);
        // detail present ???
        if (this.isDetailPresent()) {
            // make the link title ...
            var link = document.createElement("A");
            link.setAttribute('href', '#');
            link.appendChild(this._emptyHeaderTitle);
            dojo.event.connect(link, "onclick", this, "onDetail");
            // removes detail data ...
            dojo.dom.replaceChildren(this.headerTitle, link);
        }        
        
        if (this.contentUri == 'datosCuentaBenefTitular.jsf') endTimePortlet(0);
        if (this.contentUri == 'datosEstadoDeuda.jsf') endTimePortlet(1);
        if (this.contentUri == 'datosComerciales.jsf') endTimePortlet(2);
        if (this.contentUri == 'datosTecnicos.jsf') endTimePortlet(3);
        if (this.contentUri == 'datosUltimaFacturacion.jsf') endTimePortlet(4);
        if (this.contentUri == 'datosUltimoPagoRealizado.jsf') endTimePortlet(5);
        if (this.contentUri == 'consultaComponenteMedidor.jsf') endTimePortlet(6);
        if (this.contentUri == 'datosUltimaRefacturacion.jsf') endTimePortlet(7);
        if (this.contentUri == 'consultaCorte.jsf') endTimePortlet(8);
        if (this.contentUri == 'consultaReposicion.jsf') endTimePortlet(9);
        if (this.contentUri == 'consultaUltimasLect.jsf') endTimePortlet(10);

  	},
	
    /**
     * onDownloadError
     * 
     * @param {Object} e
     */
    onDownloadError: function(e){
        // todo
    },

	/**
	 * onHeaderOver event
	 */
    onHeaderOver: function() {
		// summary: callback when someone over the header zone
        if (this.isLoaded) {
            if (this.isDetailPresent()) {
                dojo.html.show(this.headerDetail);
            }
            if (this.refreshable) {
                dojo.html.show(this.headerRefresh);
            }            
        }            
	},
    
	/**
	 * onHeaderOut event
	 */
    onHeaderOut: function() {
		// summary: callback when someone goes out of the header zone
        if (this.isLoaded) {
            if (this.isDetailPresent()) {
                dojo.html.hide(this.headerDetail);
            }
            if (this.refreshable) {
                dojo.html.hide(this.headerRefresh);
            }            
        }   
	},    
    
	/**
	 * onDetail event
	 * 
	 * FIXME: [afalduto] performs window handler.
	 */
    onDetail: function() {
		// summary: callback when someone clicks my detail
        var uri = new synapsis.uri.Uri(this.detailUri, 
            this.detailParameterFieldCollector.collect()).toString()
        var features = "width=950,height=600,status=yes,toolbar=no,menubar=no,location=no,resizable=yes,scrollbars=yes";             
    	// open window ...
        window.open(uri, this.title.replace(/[^\w_\.]/g, '_'), features);            
	},
    
	/**
	 * onRefresh event 
	 */
    onRefresh: function() {
		// summary: callback when someone clicks my refresh
        this.refresh();
	},    

    /**
     * _displayWaitScreen
     * 
     * @param {Object} e
     */
    _displayWaitScreen: function(e){
        var table = document.createElement("TABLE");
        var body = document.createElement("TBODY");
        table.setAttribute("height", "100%");
        table.setAttribute("width", "100%");
        var tr = document.createElement("tr");
        var td = document.createElement("td");
        td.setAttribute("align", "center");
        td.setAttribute("vAlign", "middle");
		var img = document.createElement("img");
        img.style.verticalAlign = "middle";
        img.src = dojo.uri.dojoUri("../synapsis/widget/templates/images/indicator.gif");
        td.appendChild(img);
        tr.appendChild(td);
        table.appendChild(body);
        body.appendChild(tr);
        this._setContent(table);
    },
        
    /**
     * getWidth
     */
    getWidth: function(){
        return new Number(this.width).valueOf();
    },
    
    /**
     * isAutoWidth
     */
    isAutoWidth: function(){
        return this.width == synapsis.widget.PortletItem.ITEM_AUTO_WIDTH;
    },    
    
    /**
     * 
     * @param {Object} width
     */
    resize: function(width){
        this.width = this.parseWidth(width);
    },
    
    /**
     * Make the <code>PortletItem</code> refresh.
     */
    refresh: function(){
        synapsis.widget.PortletItem.superclass.refresh.call(this);
    },
    
    /**
     * 
     * @param {Object} number
     */
    parseWidth: function(width){
        var strWidth = new String(width);
        if(strWidth.indexOf("px")!=-1){
            return new String(synapsis.widget.PortletItem.MAX_ITEM_WIDTH);
        }             
        return strWidth.match(/[0-9]+/)[0];        
    }         
        
});
