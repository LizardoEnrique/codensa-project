/*
	Copyright (c) 2004-2006, The Dojo Foundation
	All Rights Reserved.

	Licensed under the Academic Free License version 2.1 or above OR the
	modified BSD license. For more information on Dojo licensing, see:

		http://dojotoolkit.org/community/licensing.shtml
*/

dojo.provide("synapsis.widget.ToolbarButton");

dojo.require("dojo.widget.*");
dojo.require("dojo.widget.Toolbar");

/**
 * ToolbarButton definition.
 */
dojo.widget.defineWidget(
	"synapsis.widget.ToolbarButton",
	dojo.widget.ToolbarButton, {

    /**
     * Comment for <code>widgetType</code>
     */
    widgetType: "ToolbarButton",
    
	fillInTemplate: function(args, frag) {
		synapsis.widget.ToolbarButton.superclass.fillInTemplate.call(this, args, frag);
        this.domNode.id = this.widgetId;
	}         
    
});