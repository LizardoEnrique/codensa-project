/*
	Copyright (c) 2004-2006, The Dojo Foundation
	All Rights Reserved.

	Licensed under the Academic Free License version 2.1 or above OR the
	modified BSD license. For more information on Dojo licensing, see:

		http://dojotoolkit.org/community/licensing.shtml
*/

dojo.provide("synapsis.widget.PortletContainerSinTitle");

dojo.require("dojo.widget.*");
dojo.require("dojo.html.*");

/**
 * PortletContainer definition.
 * 
 * FIXME: [afalduto] test the aspect ratios of the container.
 * 
 *  @author outaTiME (afalduto at gmail dot com) 
 *  @version $Revision: 1.1 $ $Date: 2014/04/15 17:56:19 $
 */
dojo.widget.defineWidget(
	"synapsis.widget.PortletContainerSinTitle",
	dojo.widget.HtmlWidget, {
        
    /**
     * Comment for <code>isContainer</code>
     */
    isContainer: true,

    /**
     * Comment for <code>templatePath</code>
     */
    templatePath: dojo.uri.dojoUri("../synapsis/widget/templates/PortletContainerSinTitle.html"),

    /**
     * Comment for <code>templateCssPath</code>
     */
    templateCssPath: dojo.uri.dojoUri("../synapsis/widget/templates/PortletContainerSinTitle.css"),

	/**
	 * postCreate
	 */
    postCreate: function() {
        dojo.lang.forEach(this.getItems(), dojo.lang.hitch(this,"addItem"));
    },

	/**
	 * addItem
	 * @param {Object} widget
	 */
    addItem: function(widget){
        // if no dom row created will be create one ...
        if (!PortletContainer.config.lastDomRow) {
            var table = document.createElement("TABLE");
            table.setAttribute("cellpadding", 0);
            table.setAttribute("cellspacing", 0);
            table.setAttribute("width", "100%");
            var body = document.createElement("TBODY");

            table.appendChild(body);
            PortletContainer.config.lastDomRow = document.createElement("tr");
            body.appendChild(PortletContainerSinTitle.config.lastDomRow);
            // add table to the main container ...
			this.containerNode.appendChild(table);
        }
        if(widget.isAutoWidth()){ // auto-widget ...
            widget.resize(synapsis.widget.PortletItemSinTitle.MAX_ITEM_WIDTH - 
                PortletContainerSinTitle.config.lastWidth);
            PortletContainerSinTitle.config.lastWidth = 0;
        }else if(this.isExceededWidth(widget)){ // width exceeded ...
            widget.resize(synapsis.widget.PortletItemSinTitle.MAX_ITEM_WIDTH);
            PortletContainerSinTitle.config.lastWidth = 0;            
        }else{
            PortletContainerSinTitle.config.lastWidth += widget.getWidth();
        }
        // make the td attached to the last row generated ...
        var td = document.createElement("td");
        td.setAttribute("height", "100%");
        td.setAttribute("width", widget.getWidth() + "%");
        // FIXME: [afalduto] ver que onda con los heights :P
        td.setAttribute("vAlign", "top");
        td.appendChild(widget.domNode);
        PortletContainerSinTitle.config.lastDomRow.appendChild(td);
        // cos' row is currently finished im clear the lastDomRow ...
        if (PortletContainerSinTitle.config.lastWidth==0){
            PortletContainerSinTitle.config.lastDomRow = null;            
        }        
    },

    /**
     * 
     * @param {Object} widget
     */
    isExceededWidth: function(widget){
        return (PortletContainerSinTitle.config.lastWidth + widget.getWidth()) >= 
            synapsis.widget.PortletItemSinTitle.MAX_ITEM_WIDTH;
    },

    /**
     * Return the <code>PortletItem</code> widget list related with this 
     * <code>PortletContainer</code>.
     */
    getItems: function() {
        var items = [];
    	dojo.lang.forEach(this.children, function(widget){
            if(widget instanceof synapsis.widget.PortletItem || widget instanceof synapsis.widget.PortletItemSinTitle){
                items.push(widget);
            }
    	}, this);
    	return items;
    },
    
    /**
     * Make the <code>PortletItem</code> contained in the current 
     * <code>PortletContainer</code> be refreshed.
     */
    refresh: function(){
        dojo.lang.forEach(this.getItems(), function(widget){
            widget.refresh();
    	});        
    }    

});

/**
 * Prototyped PortletContainer global object reference.
 */
var PortletContainerSinTitle = {
  /**
   * Comment for <code>config</code>
   */
  config:{
    /**
     * Last <code>PortletContainer</code> width used.
     */
    lastWidth: 0,
    /**
     * Last row created and will be used to be completed by the portlets.
     */
    lastDomRow: null
  }
}
