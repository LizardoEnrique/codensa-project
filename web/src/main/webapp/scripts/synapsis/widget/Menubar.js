/*
	Copyright (c) 2004-2006, The Dojo Foundation
	All Rights Reserved.

	Licensed under the Academic Free License version 2.1 or above OR the
	modified BSD license. For more information on Dojo licensing, see:

		http://dojotoolkit.org/community/licensing.shtml
*/

dojo.provide("synapsis.widget.Menubar");

dojo.require("dojo.widget.*");
dojo.require("dojo.widget.Menu2");

/**
 * Menubar definition.
 */
dojo.widget.defineWidget(
	"synapsis.widget.Menubar",
	dojo.widget.MenuBar2,
{

    /**
     * Fires the disabled action. 
     */
    disable: function() {
        dojo.debug("widget id: " + this.widgetId + " disabled.");
    }, 
    
    /**
     * Fires the enabled action. 
     */
    enable: function() {
        dojo.debug("widget id: " + this.widgetId + " enabled.");
    }     
        
});