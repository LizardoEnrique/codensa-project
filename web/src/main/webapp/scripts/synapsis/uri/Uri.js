/*
	Copyright (c) 2004-2006, The Dojo Foundation
	All Rights Reserved.

	Licensed under the Academic Free License version 2.1 or above OR the
	modified BSD license. For more information on Dojo licensing, see:

		http://dojotoolkit.org/community/licensing.shtml
*/

dojo.provide("synapsis.uri.Uri");

dojo.require("dojo.uri.Uri");

synapsis.uri = new function() {
    
	/**
	 * 
	 * @param {Object} uri
	 * @param {Object} params
	 */
    this.Uri = function (uri, params) {
        // found params ???
        if (params) {
            var pairs = [];    
            dojo.lang.forEach(params, function(prop){
                pairs.push(escape(prop.name) + "=" + escape(prop.value));
    	    }, this);            
            uri += "?" + pairs.join("&");
        }
        // process the entry uri specified ...
        return new dojo.uri.Uri(uri);
    }

};
