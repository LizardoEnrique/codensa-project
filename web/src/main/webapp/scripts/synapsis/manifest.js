/*
	Copyright (c) 2004-2006, The Dojo Foundation
	All Rights Reserved.

	Licensed under the Academic Free License version 2.1 or above OR the
	modified BSD license. For more information on Dojo licensing, see:

		http://dojotoolkit.org/community/licensing.shtml
*/

dojo.provide("synapsis.manifest");
dojo.require("dojo.string.extras");

var synapsisMap = {
    "portletcontainer":"synapsis.widget.PortletContainer",
    "portletcontainersintitle":"synapsis.widget.PortletContainerSinTitle",
    "portletitem":"synapsis.widget.PortletItem",
    "portletitemsintitle":"synapsis.widget.PortletItemSinTitle",
    "toolbar":"synapsis.widget.Toolbar",
    "toolbarbutton":"synapsis.widget.ToolbarButton",
    "toolbarseparator":"synapsis.widget.ToolbarSeparator",
    "toolbarselector":"synapsis.widget.ToolbarSelector",
    "toolbarloadingcontroller":"synapsis.widget.ToolbarLoadingController",
    "menubar":"synapsis.widget.Menubar",
    "fieldCollector":"synapsis.widget.FieldCollector",
    "fieldItem":"synapsis.widget.FieldItem",
    "StaticToaster":"synapsis.widget.StaticToaster"
};

dojo.registerNamespaceResolver("synapsis", 
    function(name) {
        return synapsisMap[name];
    }
); 