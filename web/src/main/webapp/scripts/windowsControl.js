/**
 * @author waab0x
 * @version $Revision: 1.1 $ $Date: 2007/09/14 18:22:50 $
 * 
 */
var WindowsManager = function() {
	this.references = new Hashtable();
};

WindowsManager.prototype = {
	
	notifyOpen: function(references) {
		this.insertReferences(references);
		var instanceRef = references.WindowsManager;
	},
	
	insertReferences: function(tReferences) {
		if(wManager.references.ContainsKey(tReferences.name))	
			wManager.references.Remove(tReferences.name);
		wManager.references.Add(tReferences.name, tReferences);
	},
	
	closeFromReference: function(objReference) {
		try {
			var foo = wManager.references.Items();
			for(i=0; i<=foo.length; i++) {
				try {
					foo[i].close();
				} catch(e) {
					//nothing?
				}
			}
		} catch(e) {
			alert(e);
		}
	} 
};
//check if i've gt a father or i'm the singleton instance xD
if(!window.opener || window.instanceWindowManager) {
	var wManager = new WindowsManager();
} else {
	var wManager = window.opener.wManager;
}