// Cuando se usa el tag sy:selectOneOption, este validador chequea que los input dentro de
// un tag sy:option se hayan ingresado.

// Si se llegara a modificar alguno de esos tags, revisar el validador, que asume cierta
// estructura en el formulario generado.

// Busca los elementos del formulario que corresponderian con la fila de un checkbox. El html
// que genera tiene un fieldset, el radio y luego los input hasta que llega otro fieldset.
// Este metodo genera un arreglo con todos los input que estan luego del radio seleccionado
// y corta cuando llega un nuevo fieldset.
function inputs_to_validate(form) {
	var elems = [];
	var adding = false;
	for (var i = 0; i < form.elements.length; i++) {
		var elem = form.elements[i];
		var what = elem.type;
		if (what == 'radio') {
			adding = (elem.checked);
		} else
		// un fieldset
		if (what == null) {
			if (adding) break;
		} else
			if (adding) elems[elems.length] = elem;
	}

	return elems;
}

var SCRIPT_ID = 'optionValidator_js_reqs';

// agrega el codigo js a la pagina actual.
function add_script(code) {
	script = document.createElement('script');
	script.type = 'text/javascript';
	script.text = code;
	script.id = SCRIPT_ID;
	document.getElementsByTagName('head')[0].appendChild(script); 
}

// Si hubo validacion antes, le quita el color de invalido a los campos y elimina el codigo
// que se hubiera generado anteriormente para las validaciones.
function clear_old_validations(formName) {
	if (highlight)
		syn_highlight([], formName);

	script = document.getElementById(SCRIPT_ID);
	if (script)
		script.parentNode.removeChild(script);
}

// Genera la funcion javascript que necesita shale-validator y la agrega a la pagina actual.
function generate_code(formName, elems) {
	var code = 'function ' + formName + "_required() {\n";

	for (var i = 0; i < elems.length; i++)
		code += 'this[' + i + '] = new Array("' + elems[i].name + '", "Faltan datos requeridos.", ' +
		"new Function(\"x\", \"return {arg:'algo'}[x];\"));\n"

	code += "}\n";

	add_script(code);
}

// Genera el codigo javascript que requiere commons-validator y despues invoca las funciones
// de validacion de shale.
function validateSelecOneRequired(form) {
	var result = false;

	var formName = jcv_retrieveFormName(form);
	var elems = inputs_to_validate(form);

	clear_old_validations(formName);

	if (elems.length > 0) {
		generate_code(formName, elems);

		result = validateRequired(form);
	}

	return result;
}
