package com.synapsis.nuc.auditing;

/**
 * Interfaz que define que objetos de negocio seran cronometrados desde que se los obtiene de la base de datos
 * hasta la creacion del mismo.
 * 
 * @author ccamba
 * 
 */
public interface AuditableTimeElapsed {

	public Long getTimeElapsed();

	public void setTimeElapsed(Long timeElapsed);

}
