package com.synapsis.nuc.codensa.model.impl;

import java.util.Date;

import com.synapsis.nuc.codensa.model.UserAuditAMETimeStamp;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ccamba
 * 
 */
public class UserAuditAMETimeStampImpl extends SynergiaBusinessObjectImpl implements UserAuditAMETimeStamp {
	private String username;
	private Date fechaAlta;
	private Date fechaBaja;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

}
