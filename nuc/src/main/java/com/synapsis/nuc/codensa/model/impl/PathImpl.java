/**
 * 
 */
package com.synapsis.nuc.codensa.model.impl;

import com.synapsis.nuc.codensa.model.Path;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author dbraccio - Assert Solutions 04/06/2008
 */
public class PathImpl extends SynergiaBusinessObjectImpl implements Path {

	private String key;
	private String path;
	private Long idModulo;

	/* (non-Javadoc)
	 * @see com.synapsis.nuc.codensa.model.impl.Path#getKey()
	 */
	public String getKey() {
		return key;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.nuc.codensa.model.impl.Path#setKey(java.lang.String)
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.nuc.codensa.model.impl.Path#getPath()
	 */
	public String getPath() {
		return path;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.nuc.codensa.model.impl.Path#setPath(java.lang.String)
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.nuc.codensa.model.impl.Path#getIdModulo()
	 */
	public Long getIdModulo() {
		return idModulo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.nuc.codensa.model.impl.Path#setIdModulo(java.lang.Long)
	 */
	public void setIdModulo(Long idModulo) {
		this.idModulo = idModulo;
	}

}
