/**
 * 
 */
package com.synapsis.nuc.codensa.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.suivant.arquitectura.core.integration.Module;
import com.synapsis.nuc.codensa.model.impl.TipoServicioImpl;
import com.synapsis.nuc.codensa.service.TiposServicioService;
import com.synapsis.synergia.core.service.impl.SynergiaServiceImpl;

/**
 * 
 * 
 * @author ar18799631
 *
 * @nas-descriptor.service id="tiposServicioService" module-id="nucModule"
 */
public class TiposServicioServiceImpl extends SynergiaServiceImpl implements TiposServicioService {

	public TiposServicioServiceImpl(Module module) {
		super(module);
	}

	
//	combo de tipos de servicio
	/**
	 * @nas-descriptor.method
	 */
	public List tiposServicio() {
		List result = new ArrayList();

		TipoServicioImpl element0 = new TipoServicioImpl();
		element0.setId("Electrico");
		element0.setDescripTipoServicio("Electrico");

		TipoServicioImpl element1 = new TipoServicioImpl();
		element1.setId("Venta");
		element1.setDescripTipoServicio("Venta");

		TipoServicioImpl element2 = new TipoServicioImpl();
		element2.setId("Financiacion");
		element2.setDescripTipoServicio("Financiación");

		TipoServicioImpl element3 = new TipoServicioImpl();
		element3.setId("Convenio");
		element3.setDescripTipoServicio("Convenio");

		TipoServicioImpl element4 = new TipoServicioImpl();
		element4.setId("EncargoCobranza");
		element4.setDescripTipoServicio("Encargo de Cobranza");

		result.add(element0);
		result.add(element1);
		result.add(element2);
		result.add(element3);
		result.add(element4);

		return result;
	}
	

}
