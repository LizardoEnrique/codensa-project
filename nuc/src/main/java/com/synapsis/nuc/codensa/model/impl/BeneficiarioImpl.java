/**
 * 
 */
package com.synapsis.nuc.codensa.model.impl;

import com.synapsis.nuc.codensa.model.Beneficiario;
import com.synapsis.synergia.core.model.impl.SynergiaPersistentObjectImpl;

/**
 * @author luciano tobaldi - assert solutions
 *
 */
public class BeneficiarioImpl extends SynergiaPersistentObjectImpl implements Beneficiario {

	private String telefono;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.suivant.arquitectura.core.model.PersistentObject#getEntityName()
	 */
	/* (non-Javadoc)
	 * @see com.synapsis.nuc.codensa.model.impl.Beneficiario#getEntityName()
	 */
	public String getEntityName() {
		return "Beneficiario";
	}

	// **********************************
	// getters and setters
	// **********************************

	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.Persona#getTelefono()
	 */
	/* (non-Javadoc)
	 * @see com.synapsis.nuc.codensa.model.impl.Beneficiario#getTelefono()
	 */
	public String getTelefono() {
		return telefono;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.Persona#setTelefono(java.lang.String)
	 */
	/* (non-Javadoc)
	 * @see com.synapsis.nuc.codensa.model.impl.Beneficiario#setTelefono(java.lang.String)
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

}
