/**
 * $Id: CuentaImpl.java,v 1.1 2007/06/28 13:06:59 ar28750185 Exp $
 */
package com.synapsis.nuc.codensa.model.impl;

import com.synapsis.nuc.codensa.model.Cuenta;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.1 $ $Date: 2007/06/28 13:06:59 $
 */
public class CuentaImpl extends SynergiaBusinessObjectImpl implements Cuenta {

	/**
	 * @uml.property name="nroCuenta"
	 */
	private long nroCuenta;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.nuc.codensa.model.impl.Cuenta#getNroCuenta()
	 */
	public long getNroCuenta() {
		return this.nroCuenta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.nuc.codensa.model.impl.Cuenta#setNroCuenta(long)
	 */
	public void setNroCuenta(long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

}
