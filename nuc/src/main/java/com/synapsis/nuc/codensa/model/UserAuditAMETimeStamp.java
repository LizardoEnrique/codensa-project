package com.synapsis.nuc.codensa.model;

import java.util.Date;

import com.suivant.arquitectura.core.model.BusinessObject;

/**
 * @author ccamba
 * 
 */
public interface UserAuditAMETimeStamp extends BusinessObject {

	public String getUsername();

	public void setUsername(String username);

	public Date getFechaAlta();

	public void setFechaAlta(Date fechaAlta);

	public Date getFechaBaja();

	public void setFechaBaja(Date fechaBaja);

}
