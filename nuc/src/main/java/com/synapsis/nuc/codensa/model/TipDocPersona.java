/**
 * $Id: TipDocPersona.java,v 1.1 2007/06/06 16:10:47 ar26557682 Exp $
 */
package com.synapsis.nuc.codensa.model;

import com.suivant.arquitectura.core.model.BusinessObject;

/**
 * 
 * @author Paola Attadio
 * 
 */
public interface TipDocPersona extends BusinessObject {
	public String getCodigo();

	public void setCodigo(String codigo);

	public String getDescripcion();

	public void setDescripcion(String descripcion);
}
