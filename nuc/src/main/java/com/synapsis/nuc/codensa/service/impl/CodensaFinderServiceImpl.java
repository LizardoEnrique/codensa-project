package com.synapsis.nuc.codensa.service.impl;

import java.util.List;

import com.suivant.arquitectura.core.dao.DAO;
import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.integration.Module;
import com.suivant.arquitectura.core.model.BusinessObject;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.service.FinderService;
import com.synapsis.nuc.codensa.validation.NumeroCuentaQueryFilterValidation;
import com.synapsis.synergia.core.service.impl.SynergiaServiceImpl;

/**
 * 
 * @author Paola Attadio y Diego Braccio
 * 
 */
public class CodensaFinderServiceImpl extends SynergiaServiceImpl implements
		FinderService {

	/**
	 * Dao que tiene asociado el finderService
	 */
	private DAO dao;

	/**
	 * retorna el dao asociado
	 * 
	 * @return DAO
	 */
	public DAO getDao() {
		return dao;
	}

	/**
	 * setea el DAO asociado que tiene el CrudService
	 * 
	 * @param DAO
	 */
	public void setDao(DAO dao) {
		this.dao = dao;
	}

	public int countAll(QueryFilter filter) {
		NumeroCuentaQueryFilterValidation nc = new NumeroCuentaQueryFilterValidation(
				filter);
		if (nc.execute()) {
			((QueryFilter) filter)
					.setQueryFilter(super.getEmpresaQueryFilter());
			return getDao().countAll(filter);
		} else
			// lanzo una excepcion
			return 0;
	}

	public List findByCriteria(QueryFilter filter) {
		NumeroCuentaQueryFilterValidation nc = new NumeroCuentaQueryFilterValidation(
				filter);
		if (nc.execute()) {

			((QueryFilter) filter)
					.setQueryFilter(super.getEmpresaQueryFilter());
			List results = getDao().findByCriteria(filter);
			// fix the for Scrollable data
			if (results.size() > 0) {
				filter.setLastResult(filter.getPositionStart().intValue()
						+ results.size() - 1);
			}
			return results;
		}
		return null;
	}

	public BusinessObject findByCriteriaUnique(QueryFilter filter)
			throws ObjectNotFoundException {
		NumeroCuentaQueryFilterValidation nc = new NumeroCuentaQueryFilterValidation(
				filter);
		if (nc.execute()) {
			((QueryFilter) filter)
					.setQueryFilter(super.getEmpresaQueryFilter());
			return getDao().findByCriteriaUnique(filter);
		}
		throw new ObjectNotFoundException();
	}

	/**
	 * solo esta puesto para compatibilidad
	 * 
	 * @param module
	 */
	public void setModule(Module module) {
		// do nothing
	}

	public String getBusinessClassName() {
		return getDao().getBusinessClassName();
	}
}
