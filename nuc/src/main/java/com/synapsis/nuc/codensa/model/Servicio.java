/**
 * $Id: Servicio.java,v 1.3 2007/07/04 20:55:30 ar26557682 Exp $
 */
package com.synapsis.nuc.codensa.model;


import java.util.Date;

import com.suivant.arquitectura.core.model.PersistentObject;

/**
 * 
 * @author Paola Attadio
 * @version $Revision: 1.3 $
 *
 */
public interface Servicio extends PersistentObject {
	
	public char getEstado();
	public void setEstado(char estado);
	public Date getFecFinalizacion();
	public void setFecFinalizacion(Date fecFinalizacion);
	public Date getFecInicio();
	public void setFecInicio(Date fecInicio);
	public Long getIdCuenta();
	public void setIdCuenta(Long idCuenta);
	public String getTipo();
	public void setTipo(String tipo);
	public String getTipoServicio();
	public void setTipoServicio(String tipoServicio);
}
