package com.synapsis.nuc.codensa.validation;

import java.util.Iterator;

import org.apache.commons.lang.StringUtils;

import com.suivant.arquitectura.core.queryFilter.CompositeQueryFilter;
import com.suivant.arquitectura.core.queryFilter.NullQueryFilter;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.queryFilter.SimpleQueryFilter;
import com.suivant.arquitectura.core.service.validation.IServiceValidation;

public class NroDocumentoQueryFilterValidator implements IServiceValidation {
	/** estado de la validacion */
	private QueryFilter filter;

	/**
	 * default constructor
	 *
	 * @param filter
	 */
	public NroDocumentoQueryFilterValidator(QueryFilter filter) {
		super();
		this.filter = filter;
	}

	/**
	 * dafault method to execute
	 */
	public boolean execute() {
		if (filter instanceof NullQueryFilter) {
			return true;
		} else if (filter instanceof SimpleQueryFilter) {
			return validateSimpleNumeroCuenta((SimpleQueryFilter) filter);
		} else if (filter instanceof CompositeQueryFilter) {
			return searchNumeroCuenta((CompositeQueryFilter) filter);
		}
		return true;
	}

	private boolean searchNumeroCuenta(CompositeQueryFilter cqf) {
		Iterator it = cqf.getFiltersList().iterator();
		while (it.hasNext()) {
			Object ofilter = it.next();
			try {
				SimpleQueryFilter sqf = (SimpleQueryFilter) ofilter;
				if (isNumeroCuentaQueryFilter(sqf))
					return isNumeroCuentaNull(sqf);
			} catch (ClassCastException cce) {
				CompositeQueryFilter cf = (CompositeQueryFilter) ofilter;
				if (searchNumeroCuenta(cf) == true)
					return true;
			}
		}
		return true;
	}

	private boolean validateSimpleNumeroCuenta(SimpleQueryFilter sqf) {
		if (isNumeroCuentaQueryFilter(sqf))
			return isNumeroCuentaNull(sqf);
		return true;
	}

	private boolean isNumeroCuentaQueryFilter(SimpleQueryFilter sqf) {
		return StringUtils
				.equalsIgnoreCase(sqf.getAttributeName(), "nroDocumento");
	}

	private boolean isNumeroCuentaNull(SimpleQueryFilter sqf) {
		if (sqf.getAttributeValue() == null)
			return false;
		return true;
	}

	/**
	 * key del mensaje a mostrar en caso que la validacion no sea satisfatoria
	 */
	public String getKeyMessage() {
		return "com.synapsis.numero.documento.no.puede.ser.vacio";
	}

}