/**
 * $Id: ItemDocumentoImpl.java,v 1.3 2007/09/10 21:47:28 ar29261698 Exp $
 */
package com.synapsis.nuc.codensa.model.impl;

import com.synapsis.nuc.codensa.model.ItemDocumento;
import com.synapsis.synergia.core.model.impl.SynergiaPersistentObjectImpl;

/**
 * @author Paola Attadio
 * @version $Revision: 1.3 $
 */
public class ItemDocumentoImpl extends SynergiaPersistentObjectImpl implements ItemDocumento {

	private static final long serialVersionUID = 1L;

	private Double saldo;

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public String getEntityName() {
		return "ItemDocumento";
	}
}
