/**
 * 
 */
package com.synapsis.nuc.codensa.model.impl;

import java.io.Serializable;

import com.synapsis.nuc.codensa.model.TipoServicio;

/**
 * @author ar18799631
 *
 */
public class TipoServicioImpl  implements TipoServicio,Serializable {
	private String descripTipoServicio;
	private String id;

	/* (non-Javadoc)
	 * @see com.synapsis.nuc.codensa.model.impl.TipoServicio#getDescripTipoServicio()
	 */
	public String getDescripTipoServicio() {
		return descripTipoServicio;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.nuc.codensa.model.impl.TipoServicio#setDescripTipoServicio(java.lang.String)
	 */
	public void setDescripTipoServicio(String descripTipoServicio) {
		this.descripTipoServicio = descripTipoServicio;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
}
