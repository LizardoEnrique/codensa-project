/**
 * $Id: TipDocPersonaImpl.java,v 1.1 2007/06/06 16:10:47 ar26557682 Exp $
 */
package com.synapsis.nuc.codensa.model.impl;

import com.synapsis.nuc.codensa.model.TipDocPersona;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * 
 * @author Paola Attadio
 * 
 */
public class TipDocPersonaImpl extends SynergiaBusinessObjectImpl implements
		TipDocPersona {

	private static final long serialVersionUID = 1L;

	private String codigo;
	private String descripcion;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
