package com.synapsis.nuc.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface Path extends SynergiaBusinessObject {

	public abstract String getKey();

	public abstract void setKey(String key);

	public abstract String getPath();

	public abstract void setPath(String path);

	public abstract Long getIdModulo();

	public abstract void setIdModulo(Long idModulo);

}