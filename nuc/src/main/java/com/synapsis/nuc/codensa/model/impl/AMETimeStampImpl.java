package com.synapsis.nuc.codensa.model.impl;

import java.util.Date;

import com.synapsis.nuc.codensa.model.AMETimeStamp;
import com.synapsis.synergia.core.model.impl.SynergiaPersistentObjectImpl;
import com.synapsis.synergia.presentation.model.NasServiceHelper;

/**
 * @author ccamba
 * 
 */
public class AMETimeStampImpl extends SynergiaPersistentObjectImpl implements AMETimeStamp {
	private Long nroCuenta;
	private Date fechaInicial;
	private Date fechaFinal;
	private Long milisegundos;
	private String username;
	private String userIP;

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public Long getMilisegundos() {
		return milisegundos;
	}

	public void setMilisegundos(Long milisegundos) {
		this.milisegundos = milisegundos;
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserIP() {
		return userIP;
	}

	public void setUserIP(String userIP) {
		this.userIP = userIP;
	}

	public void save() {
		NasServiceHelper.invoke("ameTimeStampServiceCRUD", "saveOrUpdate", new Object[] { this });
	}

	public String getEntityName() {
		return "AMETimeStamp";
	}

}
