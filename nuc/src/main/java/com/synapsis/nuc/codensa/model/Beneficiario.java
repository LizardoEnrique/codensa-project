package com.synapsis.nuc.codensa.model;

import com.synapsis.synergia.core.model.SynergiaPersistentObject;

public interface Beneficiario extends SynergiaPersistentObject{

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.suivant.arquitectura.core.model.PersistentObject#getEntityName()
	 */
	public abstract String getEntityName();

	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.Persona#getTelefono()
	 */
	public abstract String getTelefono();

	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.Persona#setTelefono(java.lang.String)
	 */
	public abstract void setTelefono(String telefono);

}