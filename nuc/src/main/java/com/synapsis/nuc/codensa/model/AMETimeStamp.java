package com.synapsis.nuc.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaPersistentObject;

/**
 * @author ccamba
 */
public interface AMETimeStamp extends SynergiaPersistentObject {

	public void setNroCuenta(Long nroCuenta);

	public Long getNroCuenta();

	public void setFechaInicial(Date fechaInicial);

	public Date getFechaInicial();

	public void setMilisegundos(Long milisegundos);
	
	public Long getMilisegundos(); 
	
	public void setFechaFinal(Date fechaFinal);

	public Date getFechaFinal();

	public String getUsername();

	public void setUsername(String username);

	public String getUserIP();

	public void setUserIP(String userIP);

	public void save();
}
