/**
 * $Id: ItemDocumento.java,v 1.3 2007/09/10 21:47:28 ar29261698 Exp $
 */
package com.synapsis.nuc.codensa.model;

import com.synapsis.synergia.core.model.SynergiaPersistentObject;

/**
 * 
 * @author Paola Attadio
 * @version $Revision: 1.3 $
 * 
 */
public interface ItemDocumento extends SynergiaPersistentObject {
	public Double getSaldo();

	public void setSaldo(Double saldo);
}