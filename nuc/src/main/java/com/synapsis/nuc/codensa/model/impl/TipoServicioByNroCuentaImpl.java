package com.synapsis.nuc.codensa.model.impl;

import com.synapsis.nuc.codensa.model.TipoServicioByNroCuenta;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class TipoServicioByNroCuentaImpl extends
		SynergiaBusinessObjectImpl implements TipoServicioByNroCuenta{

	private static final long serialVersionUID = 1L;
	
	private Long nroCuenta;
	private String tipoServicio;
	
	public Long getNroCuenta() {
          return this.nroCuenta;
	}

	public String getTipoServicio() {
		return this.tipoServicio;
	}

	public void setNroCuenta(Long nroCuenta) {
	   this.nroCuenta=nroCuenta;
		
	}

	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio=tipoServicio;
		
	}

}
