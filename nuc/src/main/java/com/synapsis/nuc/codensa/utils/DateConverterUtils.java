/**
 * 
 */
package com.synapsis.nuc.codensa.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Utilidad para convertir fechas
 * @author dbraccio - Assert Solutions
 *	02/07/2008
 */
public class DateConverterUtils {

	/**
	 * recibe un date y retorna una nueva con el formato 'dd-MM-yyyy'
	 * @param initialDate
	 * @return
	 */
	public static Date getDateFormatddmmyyyy(Date initialDate){
		try {
			SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
			if (initialDate != null) 
			    return  format.parse(initialDate.getDate() + "-"
					+ (initialDate.getMonth() + 1) + "-"
					+ (initialDate.getYear() + 1900));
			return null;  
		} catch (java.text.ParseException e) {
			return null;
		}
	}
}
