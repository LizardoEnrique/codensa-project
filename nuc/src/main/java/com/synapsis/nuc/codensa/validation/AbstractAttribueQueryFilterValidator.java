package com.synapsis.nuc.codensa.validation;

import java.util.Iterator;

import org.apache.commons.lang.StringUtils;

import com.suivant.arquitectura.core.queryFilter.CompositeQueryFilter;
import com.suivant.arquitectura.core.queryFilter.NullQueryFilter;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.queryFilter.SimpleQueryFilter;
import com.suivant.arquitectura.core.service.validation.IServiceValidation;

/**
 * Clase abstracta para implementar las validaciones de atributos obligatorios para realizar consultas a vistas.
 * 
 * @author egrande
 *
 */

public abstract class AbstractAttribueQueryFilterValidator implements IServiceValidation {

	/** estado de la validacion */
	private QueryFilter filter;

	/**
	 * default constructor
	 *
	 * @param filter
	 */
	public AbstractAttribueQueryFilterValidator(QueryFilter filter) {
		super();
		this.filter = filter;
	}

	/**
	 * dafault method to execute
	 */
	public boolean execute() {
		if (filter instanceof NullQueryFilter) {
			return true;
		} else if (filter instanceof SimpleQueryFilter) {
			return validateSimpleAttribute((SimpleQueryFilter) filter);
		} else if (filter instanceof CompositeQueryFilter) {
			return searchAttribute((CompositeQueryFilter) filter);
		}
		return true;
	}

	protected boolean searchAttribute(CompositeQueryFilter cqf) {
		Iterator it = cqf.getFiltersList().iterator();
		while (it.hasNext()) {
			Object ofilter = it.next();
			try {
				SimpleQueryFilter sqf = (SimpleQueryFilter) ofilter;
				if (isAttributeQueryFilter(sqf))
					return isAttributeNull(sqf);
			} catch (ClassCastException cce) {
				CompositeQueryFilter cf = (CompositeQueryFilter) ofilter;
				if (searchAttribute(cf) == true)
					return true;
			}
		}
		return true;
	}

	protected boolean validateSimpleAttribute(SimpleQueryFilter sqf) {
		if (isAttributeQueryFilter(sqf))
			return isAttributeNull(sqf);
		return true;
	}

	protected boolean isAttributeQueryFilter(SimpleQueryFilter sqf) {
		return StringUtils
				.equalsIgnoreCase(sqf.getAttributeName(), this.getAttributeName());
	}

	protected boolean isAttributeNull(SimpleQueryFilter sqf) {
		if (sqf.getAttributeValue() == null)
			return false;
		return true;
	}

	/**
	 * key del mensaje a mostrar en caso que la validacion no sea satisfatoria
	 */
	public abstract String getKeyMessage();

	public abstract String getAttributeName();
	
}
