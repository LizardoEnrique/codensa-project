/**
 * $Id: Workflow.java,v 1.2 2007/09/10 21:47:28 ar29261698 Exp $
 */
package com.synapsis.nuc.codensa.model;

import com.synapsis.synergia.core.model.SynergiaPersistentObject;

/**
 * 
 * @author Paola Attadio
 * @version $Revision: 1.2 $
 * 
 */
public interface Workflow extends SynergiaPersistentObject {

	public String getIdDescriptor();

	public void setIdDescriptor(String idDescriptor);

	public String getIdOldState();

	public void setIdOldState(String idOldState);

	public String getIdState();

	public void setIdState(String idState);
}
