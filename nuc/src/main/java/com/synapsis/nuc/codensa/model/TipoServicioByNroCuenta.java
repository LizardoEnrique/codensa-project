package com.synapsis.nuc.codensa.model;

import com.suivant.arquitectura.core.model.BusinessObject;

public interface TipoServicioByNroCuenta extends BusinessObject {
	
	public String getTipoServicio();
	public void setTipoServicio(String tipoServicio);
	public Long getNroCuenta();
	public void setNroCuenta(Long nroCuenta);

}
