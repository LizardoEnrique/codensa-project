package com.synapsis.nuc.codensa.model;


public interface TipoServicio  {

	/**
	 * @return the descripTipoServicio
	 */
	public String getDescripTipoServicio();

	/**
	 * @param descripTipoServicio the descripTipoServicio to set
	 */
	public void setDescripTipoServicio(String descripTipoServicio);
	public String getId();

	/**
	 * @param id the id to set
	 */
	public void setId(String id);
	
}