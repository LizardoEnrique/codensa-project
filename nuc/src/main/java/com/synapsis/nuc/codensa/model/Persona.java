package com.synapsis.nuc.codensa.model;

import com.synapsis.synergia.core.model.SynergiaPersistentObject;

/**
 * Interface que representa a la entidad Persona
 * 
 * @author ar29261698
 * 
 */
public interface Persona extends SynergiaPersistentObject{

	/**
	 * @return the telefono
	 */
	public abstract String getTelefono();

	/**
	 * @param telefono
	 *            the telefono to set
	 */
	public abstract void setTelefono(String telefono);

}