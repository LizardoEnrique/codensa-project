/**
 * 
 */
package com.synapsis.nuc.codensa.model.impl;

import com.synapsis.nuc.codensa.model.Persona;
import com.synapsis.synergia.core.model.impl.SynergiaPersistentObjectImpl;

/**
 * @author dbraccio - Suivant
 * 
 */
public class PersonaImpl extends SynergiaPersistentObjectImpl implements Persona {

	private String telefono;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.suivant.arquitectura.core.model.PersistentObject#getEntityName()
	 */
	public String getEntityName() {
		return "Persona";
	}

	// **********************************
	// getters and setters
	// **********************************

	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.Persona#getTelefono()
	 */
	public String getTelefono() {
		return telefono;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.Persona#setTelefono(java.lang.String)
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

}
