/**
 * $Id: WorkflowImpl.java,v 1.2 2007/09/10 21:47:28 ar29261698 Exp $
 */
package com.synapsis.nuc.codensa.model.impl;

import com.synapsis.nuc.codensa.model.Workflow;
import com.synapsis.synergia.core.model.impl.SynergiaPersistentObjectImpl;

/**
 * @author Paola Attadio
 * @version $Revision: 1.2 $
 */
public class WorkflowImpl extends SynergiaPersistentObjectImpl implements
		Workflow {

	private static final long serialVersionUID = 1L;

	private String idDescriptor;
	private String idState;
	private String idOldState;

	public String getIdDescriptor() {
		return idDescriptor;
	}

	public void setIdDescriptor(String idDescriptor) {
		this.idDescriptor = idDescriptor;
	}

	public String getIdOldState() {
		return idOldState;
	}

	public void setIdOldState(String idOldState) {
		this.idOldState = idOldState;
	}

	public String getIdState() {
		return idState;
	}

	public void setIdState(String idState) {
		this.idState = idState;
	}

	public String getEntityName() {
		return "WorkFlow";
	}
}
