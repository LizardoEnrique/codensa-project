package com.synapsis.nuc.codensa.validation;

import com.suivant.arquitectura.core.queryFilter.QueryFilter;

public class NumeroExpedienteQueryFilterValidation extends AbstractAttribueQueryFilterValidator {

	public NumeroExpedienteQueryFilterValidation(QueryFilter filter) {
		super(filter);
	}

	public String getAttributeName() {
		return "numeroExpediente";
	}

	public String getKeyMessage() {
		return "com.synapsis.numero.expediente.no.puede.ser.vacio";
	}

}
