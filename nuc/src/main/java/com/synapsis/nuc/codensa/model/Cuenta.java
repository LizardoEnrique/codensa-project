/**
 * 
 */
package com.synapsis.nuc.codensa.model;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.1 $ $Date: 2007/06/28 13:06:59 $
 */
public interface Cuenta {

	/**
	 * Getter of the property <tt>nroCuenta</tt>
	 * 
	 * @return Returns the nroCuenta.
	 * @uml.property name="nroCuenta"
	 */
	public abstract long getNroCuenta();

	/**
	 * Setter of the property <tt>nroCuenta</tt>
	 * 
	 * @param nroCuenta
	 *            The nroCuenta to set.
	 * @uml.property name="nroCuenta"
	 */
	public abstract void setNroCuenta(long nroCuenta);

}