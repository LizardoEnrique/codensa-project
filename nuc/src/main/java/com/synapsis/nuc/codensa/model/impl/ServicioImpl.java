/**
 * $Id: ServicioImpl.java,v 1.3 2007/07/04 20:55:37 ar26557682 Exp $
 */
package com.synapsis.nuc.codensa.model.impl;

import java.util.Date;

import com.synapsis.nuc.codensa.model.Servicio;
import com.synapsis.synergia.core.model.impl.SynergiaPersistentObjectImpl;

/**
 * 
 * @author Paola Attadio
 * @version $Revision: 1.3 $
 * 
 */
public abstract class ServicioImpl extends SynergiaPersistentObjectImpl implements
		Servicio {

	private Long idCuenta;
	private Date fecInicio;
	private Date fecFinalizacion;
	private char estado;
	private String tipo;
	private String tipoServicio;

	public char getEstado() {
		return estado;
	}

	public void setEstado(char estado) {
		this.estado = estado;
	}

	public Date getFecFinalizacion() {
		return fecFinalizacion;
	}

	public void setFecFinalizacion(Date fecFinalizacion) {
		this.fecFinalizacion = fecFinalizacion;
	}

	public Date getFecInicio() {
		return fecInicio;
	}

	public void setFecInicio(Date fecInicio) {
		this.fecInicio = fecInicio;
	}

	public Long getIdCuenta() {
		return idCuenta;
	}

	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getTipoServicio() {
		return tipoServicio;
	}

	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
}
