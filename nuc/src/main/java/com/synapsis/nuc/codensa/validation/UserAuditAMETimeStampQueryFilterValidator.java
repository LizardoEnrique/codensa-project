package com.synapsis.nuc.codensa.validation;

import com.suivant.arquitectura.core.queryFilter.QueryFilter;

/**
 * @author ccamba
 * 
 */
public class UserAuditAMETimeStampQueryFilterValidator extends AbstractAttribueQueryFilterValidator {

	public UserAuditAMETimeStampQueryFilterValidator(QueryFilter filter) {
		super(filter);
	}

	public String getAttributeName() {
		return "username";
	}

	public String getKeyMessage() {
		return "com.synapsis.username.no.puede.ser.vacio";
	}

}
