/**
 * 
 */
package com.synapsis.nuc.codensa.validation;

import com.suivant.arquitectura.core.queryFilter.CompositeQueryFilter;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.queryFilter.SimpleQueryFilter;

/**
 * @author ccamba
 * 
 */
public class CuentaInfraestructuraQueryFilterValidator extends AbstractAttribueQueryFilterValidator {

	public CuentaInfraestructuraQueryFilterValidator(QueryFilter filter) {
		super(filter);
	}

	public String getAttributeName() {
		return null;
	}

	public String getKeyMessage() {
		return "com.synapsis.numero.expediente.no.puede.ser.vacio";
	}

	protected boolean searchAttribute(CompositeQueryFilter cqf) {
		// Iterator it = cqf.getFiltersList().iterator();
		// boolean allNull = true;
		//
		// while (it.hasNext()) {
		// SimpleQueryFilter sqf = (SimpleQueryFilter) it.next();
		//
		// if (!isAttributeNull(sqf))
		// allNull = false;
		// }
		// return !allNull;
		return true;
	}

	protected boolean isAttributeNull(SimpleQueryFilter sqf) {
		if (sqf.getAttributeValue() == null || sqf.getAttributeValue() == "" || sqf.getAttributeValue().equals(""))
			return true;
		return false;
	}

}
