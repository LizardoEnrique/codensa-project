package com.synapsis.nuc.dao.hibernate;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.MethodUtils;
import org.apache.commons.lang.Validate;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.springframework.orm.hibernate3.HibernateCallback;

import com.suivant.arquitectura.core.context.ApplicationContext;
import com.suivant.arquitectura.core.dao.hibernate.DAOHibernate;
import com.suivant.arquitectura.core.dao.hibernate.QueryFilterResolver;
import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.integration.Module;
import com.suivant.arquitectura.core.logging.Logging;
import com.suivant.arquitectura.core.model.BusinessObject;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.synapsis.nuc.auditing.AuditableTimeElapsed;
import com.synapsis.nuc.utils.Chronometer;
import com.synapsis.synergia.presentation.model.NasServiceHelper;

/**
 * Clase generica de implementacion de DAO usando H3 Los modulos de negocio deben extender esta clase para
 * implementar DAOs
 * 
 * @author ccamba
 */
public class DAOHibernateChronometer extends DAOHibernate {
	private Logging logger = ApplicationContext.getLogger();

	public DAOHibernateChronometer(SessionFactory sessionFactory, Module module) {
		super(sessionFactory, module);
	}

	public DAOHibernateChronometer(SessionFactory sessionFactory, Module module, Class businessObjectClass) {
		super(sessionFactory, module, businessObjectClass);
	}

	/**
	 * retorna la cantidad de PersistentObject filtrada por el queryFilter y la clase asociada
	 * 
	 * @param filter
	 * @return int
	 */
	public int countAll(final QueryFilter filter) {
		logger.debugLog(this.getClass(), "countAll", "comienzo del metodo");
		Validate.notNull(getBusinessObjectClass());
		Integer i = (Integer) getHibernateTemplate().execute(new HibernateCallback() {
			public Object doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria criteria = getSession().createCriteria(getBusinessObjectClass());
				logger.debugLog(this.getClass(), "countAll", "dentro del Callback");
				// le pide a la clase QueryFilterResolver que le genere
				// el criteria
				QueryFilterResolver.generateCriteriaWithoutBasicRestrictions(criteria, filter);
				criteria.setProjection(Projections.rowCount());
				Integer isize = (Integer) criteria.uniqueResult();
				return isize;
			}
		});

		if (i.intValue() == 0) {
			Class[] interfaces = this.getBusinessObjectClass().getInterfaces();
			for (int j = 0; j < interfaces.length; j++) {
				if (interfaces[j].equals(AuditableTimeElapsed.class)) {

					// Es decir, no hay objetos de negocio para mostrar pero como es auditado lo debo
					// cronometrar al frame igual
					Class cls = null;
					Object ame = null;
					try {
						cls = Class.forName("com.synapsis.cns.codensa.utils.AMETimeStampUtils");
						Method method = cls.getMethod("getInstance", new Class[0]);
						Object instance = method.invoke(null, new Object[0]);
						Date fecha = new Date();
						ame = MethodUtils.invokeExactMethod(instance, "getFromSessionContext", new Object[] { null,
							fecha }, new Class[] { Date.class, Date.class });
						if (ame != null) {
							NasServiceHelper.invoke("ameTimeStampServiceCRUD", "saveOrUpdate", new Object[] { ame });
						}
					}
					catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
					catch (NoSuchMethodException e) {
						e.printStackTrace();
					}
					catch (InvocationTargetException e) {
						e.printStackTrace();
					}
					catch (IllegalAccessException e) {
						e.printStackTrace();
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}

		logger.debugLog(this.getClass(), "countAll", "termino de contar");
		return i.intValue();
	}

	/**
	 * retorna la lista de PersistentObject filtrada por el queryFilter y la clase asociada
	 * 
	 * @param filter
	 * @return List
	 */
	public List findByCriteria(final QueryFilter filter) {
		Validate.notNull(getBusinessObjectClass());
		logger.debugLog(this.getClass(), "findByCriteria", "comienzo a buscar");

		final Chronometer chronometer = new Chronometer();
		chronometer.start();

		return (List) getHibernateTemplate().execute(new HibernateCallback() {
			public Object doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria criteria = getSession().createCriteria(getBusinessObjectClass());
				QueryFilterResolver.generateCriteria(criteria, filter);
				List result = criteria.list();
				chronometer.stop();

				if (result != null && !result.isEmpty() && result.get(0) instanceof AuditableTimeElapsed) {
					for (Iterator iter = result.iterator(); iter.hasNext();) {
						AuditableTimeElapsed auditable = (AuditableTimeElapsed) iter.next();
						auditable.setTimeElapsed(new Long(chronometer.timeElapsed()));
					}
				}

				logger.debugLog(this.getClass(), "findByCriteria", "Tiempo Ejecucion: " + chronometer.timeElapsed()
					+ " msecs.");

				return result;
			}
		});

	}

	/**
	 * retorna un PersistentObject filtrada por el queryFilter y la clase asociada
	 * 
	 * @param filter
	 * @return BusinessObject
	 * @throws ObjectNotFoundException
	 */
	public BusinessObject findByCriteriaUnique(final QueryFilter filter) throws ObjectNotFoundException {
		Validate.notNull(getBusinessObjectClass());
		logger.debugLog(this.getClass(), "findByCriteriaUnique", "comienzo a buscar");

		Chronometer chronometer = new Chronometer();
		chronometer.start();

		BusinessObject businessObject = (BusinessObject) getHibernateTemplate().execute(new HibernateCallback() {
			public Object doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria criteria = getSession().createCriteria(getBusinessObjectClass());
				QueryFilterResolver.generateCriteriaWithoutBasicRestrictions(criteria, filter);
				return criteria.uniqueResult();
			}
		});

		chronometer.stop();

		if (businessObject == null)
			throw new ObjectNotFoundException();

		if (businessObject instanceof AuditableTimeElapsed) {
			((AuditableTimeElapsed) businessObject).setTimeElapsed(new Long(chronometer.timeElapsed()));
		}

		logger.debugLog(this.getClass(), "findByCriteriaUnique", "Tiempo Ejecucion: " + chronometer.timeElapsed()
			+ " msecs.");

		return businessObject;
	}

	/**
	 * retorna el resultado de la validacion de la exitencia de instancias con las restricciones del query
	 * filter
	 * 
	 * @param filter
	 * @return FIXME: [dbraccio] ver esta implementacion de pasar a hacerla con exists
	 */
	public Boolean exists(final QueryFilter filter) {
		Validate.notNull(getBusinessObjectClass());
		logger.debugLog(this.getClass(), "exists", "inicia la ejecucion");

		Chronometer chronometer = new Chronometer();
		chronometer.start();

		Boolean b = (Boolean) getHibernateTemplate().execute(new HibernateCallback() {
			public Object doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria criteria = getSession().createCriteria(getBusinessObjectClass());
				QueryFilterResolver.generateCriteriaWithoutBasicRestrictions(criteria, filter);
				criteria.setProjection(Projections.rowCount());
				Integer isize = (Integer) criteria.uniqueResult();
				if (isize.intValue() == 0)
					return new Boolean(false);
				return new Boolean(true);
			}
		});

		chronometer.stop();

		logger.debugLog(this.getClass(), "exists", "Tiempo Ejecucion: " + chronometer.timeElapsed() + " msecs.");
		return b;
	}

}
