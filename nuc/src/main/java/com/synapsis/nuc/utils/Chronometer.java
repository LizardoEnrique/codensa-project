package com.synapsis.nuc.utils;

/**
 * @author ccamba
 * 
 */
public class Chronometer {
	private boolean running;
	private long elapsedTime;
	private long startTime;

	public Chronometer start() {
		this.startTime = System.currentTimeMillis();
		this.elapsedTime = 0;
		this.running = true;
		return this;
	}

	public Chronometer stop() {
		long stopTime = System.currentTimeMillis();
		this.elapsedTime += stopTime - startTime;
		this.running = false;
		return this;
	}
	
	public long timeElapsed() {
		if (this.running) {
			return System.currentTimeMillis() - startTime;
		}
		return this.elapsedTime;
	}

	public boolean isRunning() {
		return this.running;
	}

}
