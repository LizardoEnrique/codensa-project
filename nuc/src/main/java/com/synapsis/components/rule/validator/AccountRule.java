/**
 * 
 */
package com.synapsis.components.rule.validator;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.el.ValueBinding;
import javax.faces.validator.ValidatorException;

import com.synapsis.commons.services.IServiceManager;
import com.synapsis.commons.services.ServiceHelper;
import com.synapsis.components.rule.CustomizableInputRule;
import com.synapsis.components.rule.ValidatorRule;
import com.synapsis.components.rule.converter.LongConverter;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.1 $ $Date: 2007/06/28 21:01:23 $
 */
public class AccountRule extends CustomizableInputRule {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.components.rule.CustomizableInputRule#addRules()
	 */
	protected void addRules() {
		super.addRule(new RequiredRule());
		super.addRule(new LongConverter());
		super.addRule(new ValidatorRule() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see javax.faces.validator.Validator#validate(javax.faces.context.FacesContext,
			 *      javax.faces.component.UIComponent, java.lang.Object)
			 */
			public void validate(FacesContext context, UIComponent component,
					Object value) throws ValidatorException {
				if (!ServiceHelper.getBooleanResponse(this.getServiceManager(),
						"numerosCuentaService", "exists",
						new Object[] { value }).booleanValue()) {
					super.setErrorMessage("La cuenta ingresada es inexistente",
							null, component);
				}
			}

			/**
			 * @return
			 */
			private IServiceManager getServiceManager() {
				ValueBinding valueBinding = FacesContext.getCurrentInstance()
						.getApplication().createValueBinding(
								"#{serviceManager}");
				IServiceManager serviceManager = (IServiceManager) valueBinding
						.getValue(FacesContext.getCurrentInstance());
				return serviceManager;
			}
		});
	}

}
