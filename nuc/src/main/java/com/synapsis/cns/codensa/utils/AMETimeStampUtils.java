package com.synapsis.cns.codensa.utils;

import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.synapsis.nuc.codensa.model.AMETimeStamp;
import com.synapsis.nuc.codensa.model.impl.AMETimeStampImpl;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;
import com.synapsis.synergia.core.model.Empresa;
import com.synapsis.synergia.presentation.model.NasServiceHelper;

/**
 * @author ccamba
 * 
 */
public class AMETimeStampUtils {
	private static final String KEY = "ameTimeStamp";
	private static AMETimeStampUtils instance = new AMETimeStampUtils();

	private AMETimeStampUtils() {
		super();
	}

	public static synchronized AMETimeStampUtils getInstance() {
		return instance;
	}

	/**
	 * Determina si al usuario actual hay que auditarle los tiempos de consulta.
	 */
	public boolean isUserAuditable() {
		Boolean exists = (Boolean) NasServiceHelper.getResponse("userAuditAMETimeStampService", "exists",
			new Object[] { this.getUsername() });

		return exists.booleanValue();
	}

	public AMETimeStamp makeNewInstance(String IP, Long nroCuenta, Empresa empresa) {
		AMETimeStamp ame = new AMETimeStampImpl();
		ame.setNroCuenta(nroCuenta);
		ame.setUsername(getUsername());
		ame.setUserIP(IP);
		ame.setEmpresa(empresa);
		
		return ame;
	}

	/**
	 * Agrega al objeto en el contexto de la session del usuario<br>
	 */
	public void putInSessionContext(AMETimeStamp ame) {
		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		Map map = externalContext.getSessionMap();
		map.put(KEY, ame);
	}

	/**
	 * Quita el objeto del contexto de la session del usuario
	 */
	public void clearSessionContext() {
		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		Map map = externalContext.getSessionMap();
		if (map.containsKey(KEY)) {
			map.remove(KEY);
		}
	}

	/**
	 * Devuelve el usuario logueado en esta session
	 */
	private String getUsername() {
		try {
			return SynergiaApplicationContext.getCurrentUser().getUsername();
		}
		catch (Exception e) {
			return "user_codensa";
		}
	}

}
