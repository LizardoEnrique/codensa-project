/*$Id: IntegrationCodensaTest.java,v 1.8 2007/05/28 16:17:26 ar28750185 Exp $*/
package com.synapsis.integration.codensa;

import com.suivant.arquitectura.support.tests.common.SpringTestCase;

/**
 * @autor: chino
 * @date: 26/02/2007
 */
public abstract class IntegrationCodensaTest extends SpringTestCase {

	
    public IntegrationCodensaTest() {
         super(IntegrationCodensaTestSuite.SPRING_TEST_CONFIG);
    }
    
}
