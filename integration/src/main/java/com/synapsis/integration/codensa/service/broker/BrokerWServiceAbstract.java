package com.synapsis.integration.codensa.service.broker;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.suivant.arquitectura.core.integration.RemoteEnvironment;

public abstract class BrokerWServiceAbstract {
	
	RemoteEnvironment remoteEnvironment;
	
	/**
	 * arma el contexto tomando del bean de Spring que define el contexto de la
	 * otra aplicacion
	 * 
	 * @return
	 */
	private String getUrlEnvironment() {
		FacesContext fc = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) fc
				.getExternalContext().getRequest();
		return new StringBuffer().append(
				this.getRemoteEnvironment().getProtocol()).append("://")
				.append(getServerName(request)).append(":").append(
						getServerPort(request)).append("/").append(
						this.getRemoteEnvironment().getContextRoot()).append(
						"/").append(this.getRemoteEnvironment().getServlet())
				.toString();
	}
	/**
	 * Si esta definido el host en el remote.properties devuelve ese, sino
	 * devuelve el host local donde se encuentra corriendo la aplicacion
	 * 
	 * @param request
	 * @return
	 */
	private String getServerName(HttpServletRequest request) {
		if (this.getRemoteEnvironment().getHost() != null)
			return this.getRemoteEnvironment().getHost();
		else
			return request.getServerName();
	}
	
	/**
	 * Si esta definido el port en el remote.properties devuelve ese, sino
	 * devuelve el port local donde se encuentra corriendo la aplicacion
	 * 
	 * @param request
	 * @return
	 */
	private String getServerPort(HttpServletRequest request) {
		if (this.getRemoteEnvironment().getPort() != null)
			return this.getRemoteEnvironment().getPort();
		else
			return String.valueOf(request.getServerPort());

	}
	
	// *******************************************
	// ************************getters and setters
	// *********************************************
	/**
	 * 
	 * @return
	 */
	public RemoteEnvironment getRemoteEnvironment() {
		return remoteEnvironment;
	}

	/**
	 * 
	 * @param remoteEnvironment
	 */
	public void setRemoteEnvironment(RemoteEnvironment remoteEnvironment) {
		this.remoteEnvironment = remoteEnvironment;
	}

}
