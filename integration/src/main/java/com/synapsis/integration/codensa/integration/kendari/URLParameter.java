/**
 * 
 */
package com.synapsis.integration.codensa.integration.kendari;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.Validate;

/**
 * Clase para armar los parametros para armar la url para navegar a Knd
 * @author dbraccio - Suivant
 * 
 */
public class URLParameter {

	private String parameterName;

	private Object parameterValue;

	/**
	 * constructor unico para pasar el nombre del parametro y el (o los) valores
	 * 
	 * @param parameterName
	 * @param paramValue
	 */
	public URLParameter(String parameterName, Object parameterValue) {
		super();
		Validate.notNull(parameterValue);
		Validate.notNull(parameterName);
		this.parameterName = parameterName;
		this.parameterValue = parameterValue;
	}

	public String getParameter() {
		StringBuffer sb = new StringBuffer().append(parameterName).append('=');
		if (parameterValue instanceof List){
			Iterator it = ((List)parameterValue).iterator();
			while (it.hasNext()){
				Object parameterIt = it.next();
				sb.append(parameterIt.toString());
				if (it.hasNext())
					sb.append(',');
			}
		}else
			sb.append(parameterValue.toString());
		return sb.toString();
	}

}
