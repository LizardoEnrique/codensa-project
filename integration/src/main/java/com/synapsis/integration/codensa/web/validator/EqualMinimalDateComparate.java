/**
 * 
 */
package com.synapsis.integration.codensa.web.validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import com.synapsis.components.rule.FormRule;

/**
 * @author ltobaldi
 *	assert - solutions
 */
public class EqualMinimalDateComparate extends FormRule {

		/*
		 * (non-Javadoc)
		 * 
		 * @see javax.faces.validator.Validator#validate(javax.faces.context.FacesContext,
		 *      javax.faces.component.UIComponent, java.lang.Object)
		 */
		public void validate(FacesContext context, UIComponent component,
				Object value) throws ValidatorException {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			Date maximo = null;
			Date minimo = null;
			try {
				maximo = df.parse((String) super.getValue("max"));
				minimo = df.parse((String) super.getValue("min"));
			} catch (ParseException e) {
				super.setErrorMessage("No ingreso fechas correctas en el formato dd/MM/yyyy", null);
			}
				int compare = maximo.compareTo(minimo);
				switch (compare) {
				case -1:
					super.setErrorMessage("la fecha hasta es menor a la fecha desde", null);
					break;
				}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.synapsis.components.rule.FormRule#getComponents()
		 */
		protected List getComponents() {
			List list = new ArrayList();
			list.add(super.getParameters().get("min"));
			list.add(super.getParameters().get("max"));
			return list;
		}
	}
