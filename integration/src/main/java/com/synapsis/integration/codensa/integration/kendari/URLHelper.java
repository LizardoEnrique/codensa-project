/**
 * 
 */
package com.synapsis.integration.codensa.integration.kendari;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.suivant.arquitectura.core.integration.RemoteEnvironment;

/**
 * 
 * Arma la url para pegarle a una pagina de un ambiente remoto, ejemplo kendari
 * 
 * @author dbraccio - Suivant
 * 
 */
public class URLHelper {

	private RemoteEnvironment remoteEnvironment;

	/**
	 * Setea el parametro para el caso de uso
	 */
	private static final String USE_CASE_PARAM = "_use_case_name";

	/**
	 * Startea un caso de uso de Kendari
	 */
	private static final String KND_START_USE_CASE_PARAM = "_action_id=CreateUseCaseEvent";

	/**
	 * Nombre del parametro extra que se envia a KND
	 */
	private static final String KND_PARAMETER_AUTENTICATION = "isExternal";

	/**
	 * Valor del Parametro extra que enviamos a KND
	 */
	private static final String KND_PARAMETER_AUTENTICATION_VALUE = "1";

	/**
	 * arma la url coompleta para pegarle a la aplicacion
	 * 
	 * @param useCaseName
	 * @param parameters
	 * @return
	 */
	public String getUrlComplete(String useCaseName, URLParameter[] parameters) {
		StringBuffer sb = new StringBuffer();
		sb.append(getUrlEnvironment())
				.append(getUrlFirstParameter(useCaseName));
		sb.append(getUrlApplicationParameter());
		if (parameters != null)
			sb.append(getUrlLastParameters(parameters));
		return sb.toString();
	}

	/**
	 * short cut para armar la url
	 * 
	 * @param useCaseName
	 * @return
	 */
	public String getUrlComplete(String useCaseName) {
		return getUrlComplete(useCaseName, null);
	}

	/**
	 * arma el contexto tomando del bean de Spring que define el contexto de la
	 * otra aplicacion
	 * 
	 * @return
	 */
	private String getUrlEnvironment() {
		FacesContext fc = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) fc
				.getExternalContext().getRequest();
		return new StringBuffer().append(
				this.getRemoteEnvironment().getProtocol()).append("://")
				.append(getServerName(request)).append(":").append(
						getServerPort(request)).append("/").append(
						this.getRemoteEnvironment().getContextRoot()).append(
						"/").append(this.getRemoteEnvironment().getServlet())
				.toString();
	}

	private String getUrlFirstParameter(String useCaseName) {
		return new StringBuffer().append("?").append(KND_START_USE_CASE_PARAM)
				.append("&").append(USE_CASE_PARAM).append("=").append(
						useCaseName).toString();
	}

	private String getUrlApplicationParameter() {
		return new StringBuffer().append("&").append(
				KND_PARAMETER_AUTENTICATION).append("=").append(
				KND_PARAMETER_AUTENTICATION_VALUE).toString();
	}

	private String getUrlLastParameters(URLParameter[] parameters) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < parameters.length; ++i)
			sb.append('&').append(parameters[i].getParameter());
		return sb.toString();
	}

	/**
	 * Si esta definido el host en el remote.properties devuelve ese, sino
	 * devuelve el host local donde se encuentra corriendo la aplicacion
	 * 
	 * @param request
	 * @return
	 */
	private String getServerName(HttpServletRequest request) {
		if (this.getRemoteEnvironment().getHost() != null)
			return this.getRemoteEnvironment().getHost();
		else
			return request.getServerName();
	}

	/**
	 * Si esta definido el port en el remote.properties devuelve ese, sino
	 * devuelve el port local donde se encuentra corriendo la aplicacion
	 * 
	 * @param request
	 * @return
	 */
	private String getServerPort(HttpServletRequest request) {
		if (this.getRemoteEnvironment().getPort() != null)
			return this.getRemoteEnvironment().getPort();
		else
			return String.valueOf(request.getServerPort());

	}

	// *******************************************
	// ************************getters and setters
	/**
	 * 
	 * @return
	 */
	public RemoteEnvironment getRemoteEnvironment() {
		return remoteEnvironment;
	}

	/**
	 * 
	 * @param remoteEnvironment
	 */
	public void setRemoteEnvironment(RemoteEnvironment remoteEnvironment) {
		this.remoteEnvironment = remoteEnvironment;
	}

}
