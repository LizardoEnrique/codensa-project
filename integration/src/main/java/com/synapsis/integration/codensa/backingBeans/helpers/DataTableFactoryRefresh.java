/**
 * 
 */
package com.synapsis.integration.codensa.backingBeans.helpers;

import javax.faces.component.html.HtmlDataTable;

import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.presentation.utils.JSFUtils;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.synapsis.components.common.exporter.IMetaData;
import com.synapsis.synergia.core.components.dataTable.ScrollableList;


/**
 *  
 * Se va a encargar de realizar el refresco de los tablas que deben 
 * refrescarse.
 * 
 * @author ar30557486
 * dbraccio: le saque funcionalidad que estaba siendo mal utilizada y agregue
 * 	otra que si creo que es mas necesaria
 * Importante: esta clase tiene que ser inutizada cuando se agregue el refresco 
 * automatico de la tablas con el componente
 */
public class DataTableFactoryRefresh {
	
	/**
	 * realiza el refresco del value de la tabla
	 * @param componentName nombre del componente
	 * @param filterName expresion EL del filtro
	 * @param serviceName nombre del servicio
	 * @param metadata metadata necesaria para exportar que debe inyectarse a la tabla
	 */
	public static void makeDataTable(String componentName, String filterName,
			String serviceName, IMetaData metadata) {
		
		HtmlDataTable dataTable = (HtmlDataTable) JSFUtils.getComponentFromTree(componentName);
		if (dataTable != null) {
			QueryFilter filter = (QueryFilter)VariableResolverUtils.getObject(filterName);
			dataTable.setValue(new ScrollableList(filter, serviceName, metadata));
			dataTable.setFirst(0);
		}
	}

	/**
	 * 
	 * @param componentName nombre del componente
	 * @param filter filtro
	 * @param serviceName nombre del servicio
	 * @param metadata metadata necesaria para exportar que debe inyectarse a la tabla
	 */
	public static void makeDataTable(String componentName, QueryFilter filter,
			String serviceName, IMetaData metadata) {

		HtmlDataTable dataTable = (HtmlDataTable) JSFUtils.getComponentFromTree(componentName);
		if (dataTable != null) {
			dataTable.setValue(new ScrollableList(filter, serviceName, metadata));
			dataTable.setFirst(0);
		}
	}
		
}