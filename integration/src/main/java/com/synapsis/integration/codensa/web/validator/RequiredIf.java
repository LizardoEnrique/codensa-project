/**
 * 
 */
package com.synapsis.integration.codensa.web.validator;

import java.util.Iterator;
import java.util.Set;

import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import com.synapsis.components.rule.ValidatorRule;

/**
 * @author ar27816695
 * @author dbraccio (el que hizo el copy + paste)
 */
public class RequiredIf extends ValidatorRule {

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.faces.validator.Validator#validate(javax.faces.context.FacesContext,
	 *      javax.faces.component.UIComponent, java.lang.Object)
	 */
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		Set a = super.getParameters().keySet();
		Iterator iterator = a.iterator();
		// String required = (String)this.getValue("required");
		while (iterator.hasNext()) {
			Object thaValue = iterator.next();
			String objectName = thaValue.toString();
			String objectValue = (String) super.getParameters().get(objectName);
			String val = (String) this.getValue(component, objectValue);
			if (val == null || "".equals(val)) {
				super.setErrorMessage("El campo " + objectValue
						+ " es requerido para esta b�squeda", null, component);
				break;
			}
		}
	}

	/**
	 * @param idComponent
	 * @return Value of the uiInput
	 */
	protected Object getValue(UIComponent component, String idComponent) {
		UIInput input = (UIInput) component.findComponent(idComponent);
		if (input.getSubmittedValue() == null) {
			if (input.getConverter() != null) {
				return input.getConverter().getAsString(super.getContext(),
						input, input.getValue());
			}
			return input.getValue().toString();
		}
		return input.getSubmittedValue();
	}

	/**
	 * @param idComponent
	 * @param context
	 * @return
	 */
	private UIInput getUIInput(String idComponent) {
		return (UIInput) super.getContext().getViewRoot().findComponent(
				idComponent);
	}
}