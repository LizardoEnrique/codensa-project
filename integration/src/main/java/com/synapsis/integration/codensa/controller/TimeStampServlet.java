package com.synapsis.integration.codensa.controller;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.math.NumberUtils;

import com.synapsis.nuc.codensa.model.AMETimeStamp;

/**
 * @author ccamba
 * 
 */
public class TimeStampServlet extends HttpServlet {

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.processParameters(req);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.processParameters(req);
	}

	private void processParameters(HttpServletRequest req) {

		if (!this.isValid(req.getParameter("ti")) || !this.isValid(req.getParameter("tf")))
			return;

		long tInicial = Long.valueOf(req.getParameter("ti")).longValue();
		long tFinal = Long.valueOf(req.getParameter("tf")).longValue();

		// PERSISTIR EL REGISTRO AME y NULLEAR EL ATRIBUTO PARA UNA NUEVA BUSQUEDA
		this.persistirTimeStamp(req.getSession(), tInicial, tFinal);
	}

	private Date convertParameterToDate(long parameter) {
		return new Date(new Long(parameter).longValue());
	}

	private boolean isValid(String parameter) {
		return parameter == null || parameter.length() == 0 || !NumberUtils.isDigits(parameter) ? false : true;
	}

	private void persistirTimeStamp(HttpSession session, long tInicial, long tFinal) {
		Date fechaInicial = this.convertParameterToDate(tInicial);
		Date fechaFinal = this.convertParameterToDate(tFinal);

		AMETimeStamp ame = (AMETimeStamp) session.getAttribute("ameTimeStamp");

		if (ame != null) {
			if (fechaInicial != null) {
				ame.setFechaInicial(fechaInicial);
			}

			if (fechaFinal != null) {
				ame.setFechaFinal(fechaFinal);
			}

			if (fechaInicial != null && fechaFinal != null) {
				ame.setMilisegundos(new Long(tFinal - tInicial));
				ame.save();
				// Dado que esta instancia es el fin de la renderizacion se quita del contexto para evitar los
				// refresh
				session.setAttribute("ameTimeStamp", null);
			}
		}

	}
}
